#ifndef CONVERTER_H
#define CONVERTER_H

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClassTable.h"
#include "EventType.h"
#include "EventDict.h"
#include "events.h"


#include <iostream>


class eventControler
{
public: 

private:
	TFile * file;
	TFile * fileEdit;
	TTree * tree;

	bool enCut[64];
	Int_t coinPair[64];


private:
	Int_t Elow[64], Eup[64];
	const char* statLogFile;
	const char* fsource;
	bool enStatLog;


public:
	eventControler(const char* fname);
	~eventControler();

	void setFile(const char* fname);
	void setCut(Int_t ch, Int_t Elow, Int_t Eup);
	void setCut(Int_t ch); // select and enable channels
	void setCoin(Int_t ch1, Int_t ch2);
	void setStatLog(const char* fname);
	//void setFileReport(const char* fname);
	void addHist(const char * tname, Int_t ch, Int_t ch2 = -1);

	Int_t printEntry(const char* treeName, Int_t no);	
	Int_t printEntries(const char* treeName, Int_t n = -1, Int_t step = 1, Int_t start = 1, std::ostream &st = std::cout);

	Int_t run();

private: 
	Int_t getPeriodeCC(stic_data_e * event, stic_data_e * eventPrev);
	Int_t getPeriodeFine(stic_data_e * event, stic_data_e * eventPrev);
	Int_t getDeltaT(stic_data_e * eventA, stic_data_e * eventB);
	Int_t getDeltaTfine(stic_data_e * eventA, stic_data_e * eventB);
	//Int_t startCheck(const char* brName);
	//void setFile(const char* fname);
	
	// Iteration
	// - detect errors
	// - delete them
	// - tree/branch with
	//   - CC errors
	//   - Twin errors
	//   - cleaned
	// - energy and channel cut
	// - extract physics
	//   - Periode
	//   - Coinzidence
	

	//converter(const char* fname);
	//Int_t getEntries(const char* brName);
	//Int_t printEntries(const char* brName, Int_t n = -1, Int_t step = 1, Int_t start = 1, std::ostream &st = std::cout);
	//Int_t printErrors(const char* brName, std::ostream &st = std::cout, Int_t print = 0, Int_t start = 0, Int_t n = -1);
	//Int_t errors(const char* brName, std::ostream &st = std::cout);
	//Int_t rowlingErrors(const char* brName, Int_t length, std::ostream &st = std::cout);

	//void setFile(const char* fname);
	//void save();
	//Int_t removeErrors(const char* brName, const char* newBrName);
	//Int_t selectCH(const char* brName, const char* newBrName, Int_t ch1, Int_t ch2 = -1);
	//Int_t selectE(const char* brName, const char* newBrName, Int_t ch, Int_t lower, Int_t upper);
	//Int_t select2chE(const char* brName, const char* newBrName, Int_t ch1, Int_t lower1, Int_t upper1, Int_t ch2, Int_t lower2, Int_t upper2);
	//Int_t energyCut(TTree * tree, Int_t ch, Int_t lowerCut, Int_t upperCut, TTree * newTree) {
	//Int_t loadCh(TTree * newTree, Int_t ch) ;
	//Int_t loadCoin(TTree * newTree, Int_t ch1, Int_t ch2);


	//Int_t abs(Int_t a);
	//void calcValues(Int_t * channel,Int_t * TMcc,Int_t * TScc,Int_t * Tfine,Int_t * E);
};

#endif
