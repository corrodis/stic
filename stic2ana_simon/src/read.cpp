#include "TROOT.h"
// #include "TCanvas.h"
// #include "TString.h"
#include "eventControler.h"
//#include "TStyle.h"
//#include "TFile.h"
//#include "TTree.h"


#include <iostream>
//#include <fstream>
//#include <string>
//#include <ctime>
//#include <vector>

using namespace std;

int main(int argc, char *argv[])
{
	const char* fname = (argc<2) ? "dump" : strtok(argv[1],".");	
	Int_t ch1 = (argc<3) ? -1 : atoi(argv[2]);
	Int_t Elow1 = (argc<4) ? -1 : atoi(argv[3]);
	Int_t Eup1 = (argc<5) ? -1 : atoi(argv[4]);	
	Int_t ch2 = (argc<6) ? -1 : atoi(argv[5]);
	Int_t Elow2 = (argc<7) ? -1 : atoi(argv[6]);
	Int_t Eup2 = (argc<8) ? -1 : atoi(argv[7]);
	//Int_t ch2 = (argc<4) ? -1 : atoi(argv[3]);


	eventControler c(Form("%s.root",fname));
	//c.printEntries("dump",20);
	//c.printEntry("br",100);
	
	c.setFile(Form("edit/%s.root",fname));
	//c.setFile("test.root");
	if(ch1 != -1)
		c.setCut(ch1,Elow1,Eup1);
	if(ch2 != -1)
	{
		c.setCut(ch2,Elow2,Eup2);
		c.setCoin(ch1,ch2);
	}
	c.run();
	//c.addHist("cut",13);
	// set cuts
	// set cut histos
	//c.addHist("Cleaned",ch1);
	//c.printEntries("Twins",100);
	//c.save();
	//c.printErrors("br");
	//c.errors("br");
	//c.rowlingErrors("br");
	//cout << c.getEntries("br") << endl;
	//c.setFile("wfile.root");
	//cout << c.getEntries("br") << endl;
	//c.selectCH("br","CHsel",7,13);
	//cout << c.getEntries("CHsel") << endl;




	//TTree * tEcut1 = new TTree("Ecut_1"),Form("ch%i energy cut",ch1);
	//TTree * tEcut2 = new TTree("Ecut_2"),Form("ch%i energy cut",ch2);
	//c.entries = c.energyCut(Int_t ch, Int_t lowerCut, Int_t upperCut) {


	//TFile hfile(Form("%s_coin.root",fname),"RECREATE",Form("Coincidence of ch %i and %i frome file %s",ch1,ch2,fname));
	//TTree * tch1 = new TTree(Form("tree_%i",ch1),Form("ch %i",ch1));


	//c.loadCh(tch1,ch1);
	//if(ch2!=-1)
	//{
	//	TTree * tch2 = new TTree(Form("tree_%i",ch2),Form("ch %i",ch2));
	//	TTree * tCoin1 = new TTree(Form("tree_%i-%i",ch1,ch2),Form("Coincidence of ch %i and %i",ch1,ch2));
	//	TTree * tCoin2 = new TTree(Form("tree_%i-%i",ch2,ch1),Form("Coincidence of ch %i and %i",ch1,ch2));
	//	c.loadCh(tch2,ch2);
	//	c.loadCoin(tCoin1,ch1,ch2);
	//	c.loadCoin(tCoin2,ch2,ch1);
	//}
	//newTree->Print();
	//hfile.Write();
	//hfile.Close();

	//Int_t window = (argc<5) ? -2 : atoi(argv[4]);
	//Int_t window2 = (argc<5) ? 2 : atoi(argv[4]);
	//cout << "Window: " << window << endl;
	//const char* logName = (argc<4) ? "read" : strtok(argv[3],".");

	//reader r(Form("%s.root",fname));
	//TCanvas * c1 = new TCanvas("c1", "c1", 1200, 800);
	//TCanvas * c2 = new TCanvas("c2", "c2", 1200, 800);
	//TCanvas * c3 = new TCanvas("c3", "c3", 1200, 800);
	//TCanvas * c4 = new TCanvas("c4", "c4", 1200, 800);
	//TCanvas * c5 = new TCanvas("c5", "c5", 1200, 800);
	//TCanvas * c3 = new TCanvas("c3", "c3", 1200, 800);
	//float freq = 1000; //in kHz



	//gROOT->Reset();
	//gROOT->SetStyle("Plain");
	//gStyle->SetOptFit (0001);
	//gStyle->SetPalette(1);
	//gStyle->SetOptTitle(0);


	//c1->SetGrid();
	//r.getChannels();
	
	//fstream fs;
	//fs.open(Form("%s.log",logName),fstream::in | fstream::out | fstream::app);
	//fs << fname << "\t";

	//r.getChannels();
	//r.getBasics(c1,ch1);
	//r.getBasics(c2,ch2);
	//r.getPeriod(c2,ch1,freq);
	//r.getPeriod(c4,ch2,freq);
	//r.checkTOrder(c3,1);
	//r.readEvents(1,1000,100);
	//r.getDeltaT(c3,ch1,ch2,100000);
	//r.getCoin(c4,ch1,ch2,window,window2);

	//time_t now = time(0);
	//fs << ctime(&now) << "\t";
	//fs << endl;
	//fs.close();

	//c1->Print(Form("%s.pdf[",fname));
	//c1->Print(Form("%s.pdf",fname));
	//c2->Print(Form("%s.pdf",fname));
	//c3->Print(Form("%s.pdf",fname));
	//c4->Print(Form("%s.pdf",fname));
	//c5->Print(Form("%s.pdf",fname));
	//c3->Print(Form("output/%s.pdf",fname));
	//c1->Print(Form("%s.pdf]",fname));
		
	return 0;
}
