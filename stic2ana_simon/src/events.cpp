#include "events.h"

#define VCO_SHIFT 0

stic_data_e::stic_data_e() {
	handleID = 0;
	packetID = 0;
	frame_number = 0;
	channel = 0;
	T_CCM = 0;
	T_CCS = 0;
	T_badhit = 0;
	T_fine = 0;
	E_CCM = 0;
	E_CCS = 0;
	E_badhit = 0;
	RAWerror = 0;

	time = 0;
	energy = 0;
}

void stic_data_e::printHeader(std::ostream &st) {
	st << "handleID" << "\t";
	st << "packetID" << "\t";
	st << "frame_number" << "\t";
	st << "channel" << "\t";
	st << "T_CCM" << "\t";
	st << "T_CCS" << "\t";
	st << "T_badhit" << "\t";
	st << "T_fine" << "\t";
	st << "E_CCM" << "\t";
	st << "E_CCS" << "\t";
	st << "E_badhit" << "\t";
	st << "RAWerror" << "\t";
	st << "time" << "\t";
	st << "energy" << "\t";
}

void stic_data_e::print( const char* delimiter, std::ostream &st) {
	st << handleID << delimiter;
	st << packetID << delimiter;
	st << frame_number << delimiter;
	st << channel << delimiter;
	st << T_CCM << delimiter;
	st << T_CCS << delimiter;
	st << T_badhit << delimiter;
	st << T_fine << delimiter;
	st << E_CCM << delimiter;
	st << E_CCS << delimiter;
	st << E_badhit << delimiter;
	st << RAWerror << delimiter;
	st << time << delimiter;
	st << energy << delimiter;
}

bool stic_data_e::eq(stic_data_e * event) {
	//if(eventA->handleID =! eventB->handleID)
	//	return 0;
	//if(eventA->packetID =! eventB->packetID)
	//	return 0;
	//if(eventA->frame_number =! eventB->frame_number)
	//	return 0;
	
	if(channel =! event->channel)
		return 0;
	if(T_CCM =! event->T_CCM)
		return 0;
	if(T_CCS =! event->T_CCS)
		return 0;
	if(T_badhit =! event->T_badhit)
		return 0;
	if(T_fine =! event->T_fine)
		return 0;
	if(E_CCM =! event->E_CCM)
		return 0;
	if(E_CCS =! event->E_CCS)
		return 0;
	if(E_badhit =! event->E_badhit)
		return 0;
	if(RAWerror =! event->RAWerror)
		return 0;

	return 1;
}

Int_t stic_data_e::getEnergy() {
	Int_t energy = E_CCM-T_CCS;
	if(energy>=0)
	{
		return energy;
	}
	else {
		return energy+(1<<15);
	}
	//return (E_CCM-T_CCS)>=0?(E_CCM-T_CCS):(E_CCS-T_CCS+(1<<15));
}

Int_t stic_data_e::getTcc() {
	return T_CCM;
}

Int_t stic_data_e::getTfine() {
	//return time;
	//Int_t Tfine = 0;

	//if((E_CCM - E_CCS) <= 1 && (T_CCM - T_CCS) <= 1 ) {
		if(T_fine < 2 || (T_fine > 13 && T_fine < 18) || T_fine > 29)
		{
			//Discard finecounter values where the CC was just flipping
			return -100000;
		}
		else if((T_CCM - T_CCS) == 1)
		{
			//Tfine = (T_fine + VCO_SHIFT) % 32;
			//return T_CCM*32;
			return  (T_CCM*0x20 + T_fine);
		}
		else if ((T_CCM - T_CCS) == 0)
		{
			//Tfine = (T_fine + 16 + VCO_SHIFT) % 32;
			//return T_CCM*32;
			return (T_CCS*32 + T_fine);
		}
		else {
			return -100000;
		}
	//}	
	//else {
	//	return -100000;
	//}
}

bool stic_data_e::TCCerror() {
	if(T_CCM == 1<<32 - 1 || T_CCS == 1<<32 - 1)
	{
		// Bad encoding
		return 1;
	}
	else if((T_CCM - T_CCS) != 0 && (T_CCM - T_CCS) != 1 ) {
		//strange T_CC behavior
		return 1;
	}
	else {
		return 0;
	}	
}

bool stic_data_e::ECCerror() {
	if(E_CCM == 1<<32 - 1 || E_CCS == 1<<32 - 1)
	{
		// Bad encoding
		return 1;
	}
	else if((E_CCM - E_CCS) != 0 && (E_CCM - E_CCS) != 1 ) {
		//strange T_CC behavior
		return 1;
	}
	else {
		return 0;
	}	
}
