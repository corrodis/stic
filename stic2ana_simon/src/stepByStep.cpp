#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1I.h"
#include "TString.h"
#include "TBranch.h"
#include "TClassTable.h"
#include "EventType.h"
#include "EventDict.h"
#include "events.h"


#include <iostream>
#include <vector>
#include <algorithm> 
#include <string> 

bool eventChannelSort (stic_data_e eventA, stic_data_e eventB) { return (eventA.channel < eventB.channel); }

int main(int argc, char *argv[]) {
	Int_t optPrint = 0;
	const bool only2perFrame = 0;
	
	Int_t coinWindow = 500; // 50ps units

	const Int_t ch1 = 30;
	const Int_t ch2 = 48;

	std::cout << "Start StepByStep" << std::endl; 
	stic_data_e * event = NULL;

	int lastindex = string(argv[1]).find_last_of("."); 
	string fname = string(argv[1]).substr(0, lastindex); 

	if(argc>=3) {
		coinWindow = int(atof(argv[2])*20);
	}


	TFile * file = new TFile((TString)fname+(TString)".root","READ");
	TTree * tree = (TTree*)file->Get("dump");
	TBranch * branch = tree->GetBranch("br");
	branch->SetAddress(&event);

	TFile * fileCoin = new TFile((TString)fname+(TString)"_coin.root","RECREATE");
	Int_t energy1, energy2, deltaTcc, deltaTfine;
	TTree * treeCoin = new TTree("Coin","Coin");
	treeCoin->Branch("energy1",&energy1,"energy1/I");
	treeCoin->Branch("energy2",&energy2,"energy2/I");
	treeCoin->Branch("deltaTcc",&deltaTcc,"deltaTcc/I");
	treeCoin->Branch("deltaTfine",&deltaTfine,"deltaTfine/I");

	TH1I * statFrames = new TH1I("statFrames","Events per Frame",21,-0.5,20.5);
	//TH1I * deltaT = new TH1I("deltaT","CTR", 2097153,-1048576.5,1048576.5);

	Int_t frame = 0;
	//Int_t cntFrame = 0;
	Int_t time1 = -1;
	Int_t time1cc, time1fine;
	std::vector<stic_data_e> events[2];


	Int_t cntCh = 0;

	if(optPrint>0)
		event->printHeader();
	std::cout << std::endl;
	for(Int_t i=0;i<branch->GetEntries();i++) {
	if(i%1000000==0)
		std::cout << (Double_t)i/branch->GetEntries()*100 << "%" << std::endl;
	branch->GetEntry(i);
	//for(Int_t i=0;i<20;i++) {
		if(i < optPrint) {
			event->print("\t");
			std::cout << std::endl;
		}
		//std::cout << "Event: " << i << "\t Frame: " << frame << " " << event->frame_number << std::endl;
		if(frame<event->frame_number) {
			// new Frame
			//std::cout << "Frame no " << frame << ": " << events.size() << std::endl;
			statFrames->Fill(events[0].size());			
			
			for (std::vector<stic_data_e>::iterator it1 = events[0].begin(); it1 != events[0].end(); ++it1) {
				// check if events are separated at least by coinWindow
				if( (it1 == events[0].begin() || (abs((it1-1)->getTfine() - it1->getTfine())>=coinWindow)) &&
				    (it1 == events[0].end()   || (abs((it1+1)->getTfine() - it1->getTfine())>=coinWindow))   )  {// first event or separated by coinWindow to last 

					deltaTfine = -2097152;
					for (std::vector<stic_data_e>::iterator it2 = events[1].begin(); it2 != events[1].end(); ++it2) {
						if(abs(it1->getTfine()-it2->getTfine()) <= coinWindow) {
							if(deltaTfine != -2097152) {
								// alredy an event in the coin window
								deltaTfine = 2097152;
							} else {
								deltaTfine = it1->getTfine()-it2->getTfine();
								deltaTcc = it1->getTcc()-it2->getTcc();
								energy1 = it1->getEnergy();
								energy2 = it2->getEnergy();
							}
						}

					}
					// if no coin in window deltaTfine -2097152
					// if more then one coin in window deltaTfine 2097152
					if(deltaTfine > -2097152 && deltaTfine < 2097152) {
						treeCoin->Fill();
					}
				}
			}


			//if(!only2perFrame || events.size()==2) {
			//	std::sort (events.begin(), events.end(), eventChannelSort );
			//	for (std::vector<stic_data_e>::iterator it = events.begin(); it != events.end(); ++it) {
			//		if(it->channel == ch1 && time1 < 0) {
			//			time1 = it->time; //it->channel;
			//			time1cc = it->getTcc();
			//			time1fine = it->getTfine();
			//			energy1 = it->getEnergy();
			//		}
			//		if(time1 >= 0 && it->channel == ch2) {
			//			//std::cout << time1-it->time << std::endl;
			//			deltaT->Fill((Int_t)time1-it->time);

			//			deltaTcc = time1cc - it->getTcc();
			//			if(time1fine != -100000 && it->getTfine() != -100000)
			//				deltaTfine = time1fine - it->getTfine();
			//			else 
			//				deltaTfine = -100000;
			//			energy2 = it->getEnergy();
			//			treeCoin->Fill();
			//		}		
			//	} 
			//}

			events[0].clear();
			events[1].clear();
			//cntFrame = 0;
			//time1 = -1;			
		}
		frame = event->frame_number;
		//cntFrame++;
		if(event->channel == ch1)
			events[0].push_back(*event);
		if(event->channel == ch2)
			events[1].push_back(*event);
		
		//std::cout << event-> << "\t" << it->channel << "\t"  << (it-1)->channel << "\t" << it->getTcc()-(it-1)->getTcc()  << "\t" << it->getTfine()  << "\t" << (it-1)->getTfine()  << "\t" << it->getEnergy() << "\t" << (it-1)->getEnergy() << std::endl;	
		//if(event->channel == ch1 || (event->channel == ch2 && 0) && event->TCCerror()) {
		//	cntCh++;
		//	if(frame < event->frame_number && 0) {
				// Start of a new Frame
		//		std::sort (events.begin(), events.end(), eventSorting);
		//		for (std::vector<stic_data_e>::iterator it = events.begin(); it != events.end(); ++it) {
		//			if(std::abs(it->getTcc()-lastTcc)<100) {
		//				std::cout << it->frame_number << "\t" << it->channel << "\t"  << (it-1)->channel << "\t" << it->getTcc()-(it-1)->getTcc()  << "\t" << it->getTfine()  << "\t" << (it-1)->getTfine()  << "\t" << it->getEnergy() << "\t" << (it-1)->getEnergy() << std::endl;
		//			}
		//			lastTcc = it->getTcc();
		//		}
		//		events.clear();
		//		frame = event->frame_number;
		//	}
		//	events.push_back(*event);
			//std::cout << event->channel << "\t" << event->getTcc() << std::endl;
		//	lastTcc = event->getTcc();
	
		//}
	}

	std::cout << "Total: " << branch->GetEntries() << std::endl;
	std::cout << "Coin: " << treeCoin->GetEntries() << std::endl;
	
	treeCoin->Draw("energy1");
	treeCoin->Draw("energy2");
	treeCoin->Draw("energy1:energy2");
	treeCoin->Draw("deltaTcc");
	treeCoin->Draw("deltaTfine");

	
	treeCoin->Write();
	fileCoin->Write();
	fileCoin->Close();
	file->Close();
}
