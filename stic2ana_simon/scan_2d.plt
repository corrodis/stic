set terminal eps color

# palette
set palette defined ( 0.65 '#005A32',\
		      0.7 '#238B45',\
		      0.75'#41AB5D',\
		      0.8 '#74C476',\
		      0.85 '#A1D99B',\
		      0.9 '#C7E9C0',\
    	    	      0.95 '#E5F5E0',\
		      1.0 '#F7FCF5',\
		      1.05 '#FFF5F0',\
    	    	      1.1 '#FEE0D2',\
		      1.15 '#FCBBA1',\
		      1.2 '#FC9272',\
		      1.25 '#FB6A4A',\
		      1.3 '#EF3B2C',\
		      1.35 '#CB181D',\
		      1.4 '#99000D' )

set linetype 1 lc rgb "#0EA857"		lw 1 pt 0
set linetype 2 lc rgb "#0EAD7E"		lw 1 pt 0
set linetype 3 lc rgb "#0EB2A6" 	lw 1 pt 0
set linetype 4 lc rgb "#0D9CB7" 	lw 1 pt 0
set linetype 5 lc rgb "#0D78BC" 	lw 1 pt 0
set linetype 6 lc rgb "#0C52C0" 	lw 1 pt 0
set linetype 7 lc rgb "#0B29C5" 	lw 1 pt 0
set linetype 8 lc rgb "#170BCA" 	lw 1 pt 0
set linetype 9 lc rgb "#440ACF" 	lw 1 pt 0
set linetype 10 lc rgb "#7309D4"	lw 1 pt 0
set linetype 11 lc rgb "#A408D9"	lw 1 pt 0
set linetype 12 lc rgb "#D807D0"	lw 1 pt 0
set linetype 13 lc rgb "#E207B5"	lw 1 pt 0
set linetype 14 lc rgb "#D21A26"	lw 1 pt 0
set linetype 15 lc rgb "#E10F22"	lw 1 pt 0
set linetype 16 lc rgb "#F1041F"	lw 1 pt 0

set cbrange [0.65:1.4]
set title "Parallel TTHRES & TBIAS Scans"
#set pm3d map
set xtics ("5" 17, "6" 18,"7" 19,"8" 20,"9" 21, "5" 23, "6" 24,"7" 25,"8" 26,"9" 27, "5" 29, "6" 30,"7" 31,"8" 32,"9" 33)
set x2tics ("4" 19, "6" 25, "8" 31)
set xlabel "TTHRES ch1"
set x2label "TBIAS" offset 23, -1.4
set ytics ("5" 17, "6" 18,"7" 19,"8" 20,"9" 21, "5" 23, "6" 24,"7" 25,"8" 26,"9" 27, "5" 29, "6" 30,"7" 31,"8" 32,"9" 33)
set y2tics ("4" 19, "6" 25, "8" 31)
set ylabel "TTHRES ch2"
set y2label "TBIAS" offset -2.1,7.3
set xrange [16:34]
set yrange [16:34]
set output "scan_2ch.eps"
plot 'scan.data' u ($6*3+$7):($14*3+$15):($10==31&&$20<0.1?$19/2.34:1/0) w p pt 7 palette notitle,\
     'scan.data' u ($6*3+$7):($14*3+$15):($10==31&&$20>=0.1?$19/2.34:1/0) w p pt 7 lc rgb "#cccccc" notitle

set title "Parallel TTHRES & TBIAS Scans (O_DAC(ch2) 16)"
set output "scan_2ch_DAC16.eps"
plot 'scan.data' u ($6*3+$7):($14*3+$15):($10==16&&$20<0.1?$19/2.34:1/0) w p pt 7 palette notitle,\
     'scan.data' u ($6*3+$7):($14*3+$15):($10==16&&$20>=0.1?$19/2.34:1/0) w p pt 7 lc rgb "#cccccc" notitle

set output "scan_2ch_scan.eps"
set autoscale y
set ytics auto
unset y2label
set ylabel "ctr FWHM [ps]"
set grid x2 y
set xtics nomirror
set xtics scale 0
set xlabel "TTHRES ch1 (ch2)"
set key left
#set x2tics format ""
#set x2tics autofreq 0.5,1
set x2tics ("" 17.5, "4" 18.5,"" 19.5,"" 20.5,"" 21.5, "" 23.5, "6" 24.5,"" 25.5,"" 26.5,"" 27.5,"" 29.5, "8" 30.5,"" 31.5,"" 32.5,"" 33.5)
set x2tics mirror
set title "Parallel TTHRES & TBIAS Scans"
plot 'scan.data' u ($10==31&&$20<0.1&&($6*3+$7)==31?($14*3+$15+0.1):1/0):($19*51*2.35):($20*51*2.35) w yerrorbars title "ch1: TDAC 8-7" ls 1,\
     'scan.data' u ($10==31&&$20<0.1&&($6*3+$7)==32?($14*3+$15-0.1):1/0):($19*51*2.35):($20*51*2.35) w yerrorbars title "ch1: TDAC 8-6" ls 4,\
     'scan.data' u ($10==31&&$20<0.1&&($6*3+$7)==33?($14*3+$15+0.1):1/0):($19*51*2.35):($20*51*2.35) w yerrorbars title "ch1: TDAC 8-9" ls 8,\
     'scan.data' u ($10==31&&$20<0.1&&($14*3+$15)==20?($6*3+$7-0.1):1/0):($19*51*2.35):($20*51*2.35) w yerrorbars title "ch2: TDAC 4-8" ls 16
#plot 'scan.data' u ($10==16&&$20<0.1?($6*3+$7+0.1):1/0):($19*51*2.35) w yerrorbars title "O_DAC(ch2) 16",\
#     'scan.data' u ($10==31&&$20<0.1?($6*3+$7-0.1):1/0):($19*51*2.35) w yerrorbars title "O_DAC(ch2) 31"
