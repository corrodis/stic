#!/bin/bash

# Processes all dump files in a folder: creates coincidence and applies energy cut
# 6.7.2014 Simon Corrodi, corrodis@phys.ethz.ch


ANAPATH=~/Documents/stic/stic2ana_simon
mkdir plots
for f in *[0-9].root
	do
		echo "Processing $f file..."
		if [ -f ${f:0:-5}_coin.root ]
		then
			echo "${f:0:-5}_coin.root is already generated"
		else
			  $ANAPATH/stepByStep $f
		fi
		if [ -f ${f:0:-5}_ana.root ]
		then
			echo "	${f:0:-5}_ana.root is already generated"
		else
			  $ANAPATH/check ${f:0:-5} >> scan.data
		fi

done

gnuplot $ANAPATH/scan_ctr_bias.plt
gnuplot $ANAPATH/scan_ctr_tbias.plt
gnuplot $ANAPATH/scan_ctr_tthres.plt

gnuplot $ANAPATH/scan_E1_bias.plt
gnuplot $ANAPATH/scan_E1_tbias.plt
gnuplot $ANAPATH/scan_E1_tthres.plt

gnuplot $ANAPATH/scan_overflow_bias.plt
gnuplot $ANAPATH/scan_overflow_tbias.plt
gnuplot $ANAPATH/scan_overflow_tthres.plt

gnuplot $ANAPATH/scan_workingpoint.plt
gnuplot $ANAPATH/scan_stability.plt
