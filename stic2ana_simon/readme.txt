To analyse data:
1) run stepByStep dump.root		This produces FILE_coin.root, which contains the coincidence events
2a) root 'check.cxx("dump")'		This is used for the energy cut on the energy 
					If the wrong peak is selected, (if the photo peak is not the biggest), adjust that for p1=, p2=
2b) or checker dump			same as macro, compiled version

ALTERNATIVE
use process.sh				1) change to folder with the dump files
					2) start ......./process.sh
					3) data is saved in scan.data

If you use other channels, you probably have to adjust that in the files. Sorry for that
