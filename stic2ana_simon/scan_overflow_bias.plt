set terminal eps

# Data Format of the scan.data
#  1: RUN - NO
#  2: DAC - BIAS
#  3: DAC - LADDER
#  4: DAC - ICOMP
#  5: DAC - ?
#  6: DAC - TBIAS
#  7: DAC - TTHRESHOLD
#  8: DAC - EBIAS
#  9: DAC - ETHRESHOLD
#  10: BIAS VOLTAGE (nominal 58.14)
# 11: CTR Sigma
# 12: CTR Sigma fiterror
# 13: E1 Position
# 14: E1 Position error
# 15: E2 Position
# 16: E2 Position error
# 17: number of coincidence events after energy cuts 
# 18: number of underflows
# 19: number of overflows

set linetype 1 lc rgb "#0EA857"		lw 1 pt 0
set linetype 2 lc rgb "#0EAD7E"		lw 1 pt 0
set linetype 3 lc rgb "#0EB2A6" 	lw 1 pt 0
set linetype 4 lc rgb "#0D9CB7" 	lw 1 pt 0
set linetype 5 lc rgb "#0D78BC" 	lw 1 pt 0
set linetype 6 lc rgb "#0C52C0" 	lw 1 pt 0
set linetype 7 lc rgb "#0B29C5" 	lw 1 pt 0
set linetype 8 lc rgb "#170BCA" 	lw 1 pt 0
set linetype 9 lc rgb "#440ACF" 	lw 1 pt 0
set linetype 10 lc rgb "#7309D4"	lw 1 pt 0
set linetype 11 lc rgb "#A408D9"	lw 1 pt 0
set linetype 12 lc rgb "#D807D0"	lw 1 pt 0
set linetype 13 lc rgb "#E207B5"	lw 1 pt 0
set linetype 14 lc rgb "#D21A26"	lw 1 pt 0
set linetype 15 lc rgb "#overflow0F22"	lw 1 pt 0
set linetype 16 lc rgb "#F1041F"	lw 1 pt 0


############# overflowvs TTHRES (BIAS) with fixed TB ############################
set xlabel "over-voltag [V]"
set ylabel "overflow [%]"
set xrange [-0.25:4.25]
set x2range [-0.25:4.25]
set mxtics 0.5
set xtics 0,0.5 scale 0
set x2tics -0.25,0.5 mirror format ""
set grid x2tics ytics
set key outside Right

set output "plots/scan_overflow_bias_TBIAS-0.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 0)"
plot "scan.data" using ($6==0&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==0&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==0&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==0&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==0&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==0&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==0&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==0&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==0&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==0&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==0&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==0&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==0&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==0&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==0&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==0&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"


set output "plots/scan_overflow_bias_TBIAS-1.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 1)"
plot "scan.data" using ($6==1&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==1&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==1&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==1&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==1&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==1&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==1&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==1&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==1&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==1&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==1&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==1&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==1&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==1&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==1&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==1&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-2.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 2)"
plot "scan.data" using ($6==2&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==2&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==2&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==2&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==2&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==2&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==2&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==2&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==2&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==2&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==2&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==2&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==2&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==2&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==2&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==2&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-3.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 3)"
plot "scan.data" using ($6==3&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==3&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==3&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==3&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==3&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==3&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==3&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==3&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==3&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==3&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==3&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==3&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==3&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==3&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==3&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==3&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-4.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 4)"
plot "scan.data" using ($6==4&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==4&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==4&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==4&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==4&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==4&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==4&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==4&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==4&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==4&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==4&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==4&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==4&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==4&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==4&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==4&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-5.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 5)"
plot "scan.data" using ($6==5&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==5&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==5&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==5&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==5&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==5&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==5&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==5&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==5&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==5&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==5&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==5&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==5&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==5&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==5&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==5&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-6.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 6)"
plot "scan.data" using ($6==6&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==6&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==6&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==6&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==6&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==6&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==6&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==6&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==6&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==6&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==6&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==6&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==6&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==6&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==6&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==6&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-7.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 7)"
plot "scan.data" using ($6==7&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==7&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==7&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==7&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==7&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==7&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==7&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==7&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==7&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==7&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==7&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==7&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==7&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==7&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==7&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==7&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-8.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 8)"
plot "scan.data" using ($6==8&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==8&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==8&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==8&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==8&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==8&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==8&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==8&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==8&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==8&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==8&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==8&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==8&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==8&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==8&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==8&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-9.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 9)"
plot "scan.data" using ($6==9&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==9&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==9&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==9&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==9&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==9&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==9&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==9&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==9&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==9&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==9&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==9&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==9&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==9&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==9&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==9&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-20.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 10)"
plot "scan.data" using ($6==10&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==10&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==10&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==10&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==10&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==10&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==10&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==10&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==10&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==10&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==10&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==10&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==10&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==10&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==10&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==10&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-11.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 11)"
plot "scan.data" using ($6==11&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==11&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==11&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==11&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==11&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==11&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==11&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==11&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==11&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==11&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==11&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==11&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==11&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==11&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==11&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==11&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-12.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 12)"
plot "scan.data" using ($6==12&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==12&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==12&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==12&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==12&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==12&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==12&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==12&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==12&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==12&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==12&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==12&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==12&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==12&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==12&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==12&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-13.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 13)"
plot "scan.data" using ($6==13&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==13&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==13&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==13&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==13&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==13&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==13&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==13&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==13&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==13&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==13&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==13&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==13&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==13&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==13&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==13&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-14.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 14)"
plot "scan.data" using ($6==14&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==14&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==14&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==14&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==14&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==14&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==14&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==14&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==14&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==14&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==14&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==14&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==14&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==14&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==14&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==14&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TBIAS-28.eps"
set title "Bias Scan, (EDAC 12-13, TBIAS 15)"
plot "scan.data" using ($6==15&&$7==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==15&&$7==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==15&&$7==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==15&&$7==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==15&&$7==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==15&&$7==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==15&&$7==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==15&&$7==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==15&&$7==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==15&&$7==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==15&&$7==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==15&&$7==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==15&&$7==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==15&&$7==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==15&&$7==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==15&&$7==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

############# overflowvs BIAS (TB) with fixed TT ############################
set xlabel "over-voltag [V]"
set ylabel "overflow [%]"
set xrange [-0.25:4.25]
set x2range [-0.25:4.25]
set mxtics 0.5
set xtics 0,0.5 scale 0
set x2tics -0.25,0.5 mirror format ""
set grid x2tics ytics
set key outside Right

set output "plots/scan_overflow_bias_TTHRES-0.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 0)"
plot "scan.data" using ($7==0&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==0&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==0&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==0&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==0&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==0&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==0&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==0&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==0&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==0&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==0&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==0&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==0&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==0&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==0&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==0&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-1.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 1)"
plot "scan.data" using ($7==1&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==1&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==1&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==1&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==1&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==1&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==1&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==1&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==1&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==1&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==1&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==1&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==1&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==1&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==1&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==1&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-2.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 2)"
plot "scan.data" using ($7==2&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==2&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==2&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==2&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==2&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==2&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==2&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==2&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==2&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==2&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==2&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==2&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==2&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==2&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==2&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==2&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-3.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 3)"
plot "scan.data" using ($7==3&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==3&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==3&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==3&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==3&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==3&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==3&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==3&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==3&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==3&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==3&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==3&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==3&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==3&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==3&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==3&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-4.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 4)"
plot "scan.data" using ($7==4&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==4&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==4&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==4&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==4&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==4&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==4&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==4&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==4&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==4&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==4&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==4&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==4&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==4&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==4&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==4&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-5.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 5)"
plot "scan.data" using ($7==5&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==5&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==5&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==5&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==5&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==5&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==5&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==5&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==5&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==5&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==5&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==5&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==5&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==5&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==5&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==5&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-6.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 6)"
plot "scan.data" using ($7==6&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==6&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==6&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==6&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==6&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==6&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==6&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==6&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==6&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==6&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==6&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==6&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==6&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==6&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==6&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==6&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-7.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 7)"
plot "scan.data" using ($7==7&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==7&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==7&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==7&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==7&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==7&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==7&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==7&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==7&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==7&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==7&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==7&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==7&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==7&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==7&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==7&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-8.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 8)"
plot "scan.data" using ($7==8&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==8&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==8&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==8&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==8&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==8&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==8&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==8&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==8&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==8&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==8&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==8&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==8&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==8&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==8&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==8&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-9.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 9)"
plot "scan.data" using ($7==9&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==9&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==9&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==9&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==9&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==9&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==9&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==9&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==9&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==9&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==9&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==9&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==9&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==9&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==9&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==9&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-20.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 10)"
plot "scan.data" using ($7==10&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==10&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==10&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==10&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==10&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==10&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==10&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==10&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==10&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==10&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==10&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==10&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==10&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==10&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==10&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==10&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-11.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 11)"
plot "scan.data" using ($7==11&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==11&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==11&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==11&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==11&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==11&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==11&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==11&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==11&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==11&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==11&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==11&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==11&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==11&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==11&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==11&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-12.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 12)"
plot "scan.data" using ($7==12&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==12&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==12&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==12&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==12&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==12&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==12&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==12&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==12&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==12&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==12&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==12&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==12&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==12&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==12&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==12&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-13.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 13)"
plot "scan.data" using ($7==13&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==13&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==13&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==13&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==13&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==13&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==13&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==13&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==13&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==13&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==13&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==13&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==13&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==13&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==13&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==13&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-14.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 14)"
plot "scan.data" using ($7==14&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==14&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==14&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==14&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==14&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==14&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==14&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==14&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==14&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==14&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==14&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==14&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==14&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==14&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==14&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==14&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_bias_TTHRES-28.eps"
set title "Bias Scan, (EDAC 12-13, TTHRES 15)"
plot "scan.data" using ($7==15&&$6==0&&$8==13&&$9==13?$10-66.7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($7==15&&$6==1&&$8==12&&$9==13?$10-66.7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($7==15&&$6==2&&$8==12&&$9==13?$10-66.7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($7==15&&$6==3&&$8==12&&$9==13?$10-66.7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($7==15&&$6==4&&$8==12&&$9==13?$10-66.7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($7==15&&$6==5&&$8==12&&$9==13?$10-66.7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($7==15&&$6==6&&$8==12&&$9==13?$10-66.7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($7==15&&$6==7&&$8==12&&$9==13?$10-66.7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($7==15&&$6==8&&$8==12&&$9==13?$10-66.7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($7==15&&$6==9&&$8==12&&$9==13?$10-66.7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($7==15&&$6==10&&$8==12&&$9==13?$10-66.7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($7==15&&$6==11&&$8==12&&$9==13?$10-66.7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($7==15&&$6==12&&$8==12&&$9==13?$10-66.7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($7==15&&$6==13&&$8==12&&$9==13?$10-66.7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($7==15&&$6==14&&$8==12&&$9==13?$10-66.7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($7==15&&$6==15&&$8==12&&$9==13?$10-66.7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"



