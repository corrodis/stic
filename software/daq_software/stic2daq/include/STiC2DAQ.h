#ifndef __STIC2DAQ_H
#define __STIC2DAQ_H


#include "TH1.h"
#include "TTree.h"
#include "TLFSRDecode.h"
#include "TMutex.h"
#include "TUSB_iface.h"

#include <list>
#include <mqueue.h>

#include "EventType.h"
//#include "EventDict.h"





class TSTiC2DAQ{

public:

	enum mode{MODE_NONE,MODE_E,MODE_TSINGLE,MODE_T,MODE_TSINGLE_FINE, MODE_TSINGLE_CM,MODE_CHANNEL,MODE_TBADHIT,MODE_PERIOD};
	struct monitor{
		TH1D* hist;
		mode fMode;
		int fChannel1;
		int fChannel2;

		int ch1_ECut_low, ch1_ECut_high;
		int ch2_ECut_low, ch2_ECut_high;
		
		stic_data_t* previous_events;
		/*cut for histogram filling*/
		bool (*cut_method)(stic_data_t*);

		monitor(mode m=MODE_NONE);
		~monitor();
	};


	typedef std::vector<monitor>::iterator monitor_ref;

	TSTiC2DAQ();
	~TSTiC2DAQ();

//monitoring histogram
	monitor_ref 		AddMonitor(monitor mon);
	std::vector<monitor>*	GetMonitorHistograms() {return fmonitors;}
	void			RemoveMonitor(monitor_ref m);

//dump tree
	TBranch*	AddBranch(TTree* tree, const char* treename);
	void		SetBranch(TTree* tree, const char* branchname);
	
//reader thread & configuration


	/*
	*	Read from stream and decoder thread. A kickstarter.
	*/
	static void reader_thread(TSTiC2DAQ* instance){
		printf("STiC2DAQ::reader_thread started\n");
		instance->reader_method();
		printf("STiC2DAQ::reader_thread ended\n");
	}

	void StopReadout(){fRunning=false;};
	int  PauseReadout();
	int  StartReadout();
	int  SetSync(unsigned char sync);
/*
	Returns a pair of sockets to read/write to sc class
	[0]:	read from interface
	[1]:	write to interface
*/
	int SCMessageQueue(const char* qname="sticsc");
	
protected:
//monitoring
	std::vector<monitor>* fmonitors;
	TBranch* fBranch;
	TTree* fTree;
	TUSB_iface* usb_iface;

private:
	

	mqd_t	scqueue_cmd;
	mqd_t	scqueue_reply;

//reader
	bool fRunning;
	char* chunk_buffer; 
	int numbytes;
	stic_data_t* current_event;
	TLFSRDecode decoder;

//mutex for accessing monitor and tree objects
	TMutex rw_mutex;
	/*
	 * internal reader loop
	 * will read the fd until a chunk is full, and deserialize the data.
	 * the event data will be filled into the tree(if available), and fill the histogram(if available)
	*/
	void reader_method();


	/* Decode 12bytes to event struct*/
	void BufferToEvent(unsigned int* const buf_ptr, stic_data_t* const event);
	/* Fill high level pads (time,energy) return -1 on bad reconstruction*/
	short ProcessRawEvent(stic_data_t* const current_event);
	/* Fill Monitor Histograms */
	void ProcessMonitors(stic_data_t* const current_event);
};

#endif
