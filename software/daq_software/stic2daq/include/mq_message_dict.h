

#ifndef MQ_MESSAGE_DICT_H__
#define MQ_MESSAGE_DICT_H__

#define MAX_MQ_MSGLEN 2000 

struct __attribute__((__packed__)) msg_header{
	unsigned char cmd;
	unsigned char handleID;
	unsigned char port;
	unsigned short len;
};

struct __attribute__((__packed__)) msg_struct{
	unsigned char cmd;
	unsigned char handleID;
	unsigned char port;
	unsigned short len;
	char msg[MAX_MQ_MSGLEN];		
};


struct __attribute__((__packed__)) daq_data_header{
	unsigned char cmd;
	unsigned char handleID;
	unsigned char port;
	unsigned short len;
	unsigned short packetID;
};
struct __attribute__((__packed__)) daq_data_struct{
	unsigned char cmd;
	unsigned char handleID;
	unsigned char port;
	unsigned short len;
	unsigned short packetID;
	char payload[MAX_MQ_MSGLEN-2];
};


struct usb_header{
	unsigned cmd : 8;
	unsigned handleID : 7;
	unsigned port : 6;
	unsigned len : 11;
};

struct usb_struct{
	unsigned cmd : 8;
	unsigned handleID : 7;
	unsigned port : 6;
    unsigned len : 11;
	unsigned char payload[1020+1020];
};

#endif
