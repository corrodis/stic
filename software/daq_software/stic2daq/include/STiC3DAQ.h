#ifndef __STIC3DAQ_H
#define __STIC3DAQ_H


#include "TH1.h"
#include "TTree.h"
#include "TLFSRDecode.h"
#include "TMutex.h"
#include "TUSB_iface.h"

#include <list>
#include <mqueue.h>
#include <fstream>
//#include <mutex>

#include "EventType.h"
#include "mq_message_dict.h"

#include <map>




class TSTiC3DAQ{

public:

	enum mode{MODE_NONE,MODE_E,MODE_TSINGLE,MODE_T,MODE_TSINGLE_FINE, MODE_TSINGLE_CM,MODE_CHANNEL,MODE_TBADHIT,MODE_PERIOD,MODE_E_MEAN,MODE_E_RMS,MODE_F_MEAN,MODE_F_RMS,MODE_BEAM_PROFILE};
	struct monitor{
		TH1* hist;
		mode fMode;
		int fChannel1;
		int fChannel2;

		int ch1_ECut_low, ch1_ECut_high;
		int ch2_ECut_low, ch2_ECut_high;
		
		stic3_data_t* previous_events;
		/*cut for histogram filling*/
		bool (*cut_method)(stic3_data_t*);

		monitor(mode m=MODE_NONE);
		~monitor();
	};

	struct tPosition{
		int x;
		int y;
	};



	typedef std::vector<monitor>::iterator monitor_ref;

	//TSTiC3DAQ();
	TSTiC3DAQ(bool offline = false);
	~TSTiC3DAQ();

//monitoring histogram
	monitor_ref 		AddMonitor(monitor mon);
	std::vector<monitor>*	GetMonitorHistograms() {return fmonitors;}
	void			RemoveMonitor(monitor_ref m);
//for the beam profile histogram
	int			GetBeamProfileMapping();
	tPosition		GetChannelPosition(unsigned short channel);

//dump tree
	TBranch*	AddBranch(TTree* tree, const char* treename);
	void		SetBranch(TTree* tree, const char* branchname);
	TTree*		GetTree();
	void		DeleteTree();

//dumpt bin file
    ofstream * binFile;
    void        SetRedoutBinFile(ofstream * fout) {binFile = fout;}

	
//reader thread & configuration


	/*
	*	Read from stream and decoder thread. A kickstarter.
	*/
    static void reader_thread(TSTiC3DAQ* instance){
		printf("STiC3DAQ::reader_thread started\n");
		instance->reader_method();
		printf("STiC3DAQ::reader_thread ended\n");
	}


    void StopReadout();
    //void SetReadoutBin(bool bin) {fbin = bin;}
    int PauseReadout();
    int StartReadout();
    int StartDebugEvents(int no = 300);
    int StopDebugEvents();
    int SetSync(unsigned char sync);
    int Enable2kPackages();
    int Disable2kPackages();
    int GetTemperature();

/*
	Returns a pair of sockets to read/write to sc class
	[0]:	read from interface
	[1]:	write to interface
*/
	int SCMessageQueue(const char* qname="sticsc");

    // USB package statistics
	unsigned int GetUsbPackageNo() {return n_usb_bulk_packages;}
	unsigned int GetUsbDoublePackageNo() {return n_usb_bulk_packages_double;}
	unsigned int GetUsbLostPackageNo() {return n_usb_bulk_packages_lost;}
	void ResetUsbPakageNo();
	
	// Interface to load and process one event in a buffer
	int ProcessBufferToEvent(unsigned int* const buf_ptr, stic3_data_t* const event);
	int ProcessBufferToEvent(unsigned int* const buf_ptr);
	
protected:
//monitoring
	std::vector<monitor>* fmonitors;
	TBranch* fBranch;
	TTree* fTree;
	TUSB_iface* usb_iface;

	std::map<unsigned short,tPosition> fBeamProfileMapping;
	tPosition fBeanPosition[256];

private:
	
	/* USB Package counter */
	unsigned int n_usb_bulk_packages;
	unsigned int n_usb_bulk_packages_double;
	unsigned int n_usb_bulk_packages_lost;

	mqd_t	scqueue_cmd;
	mqd_t	scqueue_reply;
     
     //reader
	bool fRunning;
    bool f2kPackages;

    //bool fbin;
	char* chunk_buffer; 
	int numbytes;
	unsigned short curr_packetID;
	unsigned short last_packetID;
	stic3_data_t* current_event;
	TLFSRDecode decoder;

//mutex for accessing monitor and tree objects
	TMutex rw_mutex;
	/*
	 * internal reader loop
	 * will read the fd until a chunk is full, and deserialize the data.
	 * the event data will be filled into the tree(if available), and fill the histogram(if available)
	*/
	void reader_method();
    void reader_method_bin();


	/* Process data from the message queue interface and send back data if required */
	short ProcessLocalMQ(msg_struct* current_msg);

	/* Process data from the message queue interface and send back data if required */
	short FetchEventData(unsigned short handleID);

	/* Decode 12bytes to event struct*/
	void BufferToEvent(unsigned int* const buf_ptr, stic3_data_t* const event);
    /* Dumps raw usb data to disc */
    void BufferToFile(unsigned int* const buf_ptr, unsigned int len);
	/* Fill high level pads (time,energy) return -1 on bad reconstruction*/
	short ProcessRawEvent(stic3_data_t* const current_event);
	/* Fill Monitor Histograms */
	//void ProcessMonitors(stic3_data_t* const current_event);
	void ProcessMonitors(stic3_data_t* const current_event,bool matrix);
};

#endif
