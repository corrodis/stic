#ifndef MonitorGUI_H_
#define MonitorGUI_H_

#include "TGComboBox.h"
#include "TGNumberEntry.h"
#include <RQ_OBJECT.h>

#include "vector"

#include "STiC3DAQ.h"

class MonitorGUI{
  RQ_OBJECT("MonitorGUI")

public:

	MonitorGUI();
	~MonitorGUI();
	
	void	SetSTiC3DAQ( TSTiC3DAQ* daq ){ stic_daq = daq; };
	
	void	StartGUI();
	int	UseGUIValues();
	void	UpdateECuts();
	void	ResetHistos();

private:  
        
        TSTiC3DAQ*	stic_daq;
  
	TGComboBox*	comboBox_mode[4];
	TGNumberEntry*	entry_ch1_select[4];
	TGNumberEntry*	entry_ch2_select[4];

	TGNumberEntry*	entry_ch1_eCut[2];
	TGNumberEntry*	entry_ch2_eCut[2];

	ClassDef(MonitorGUI,1)
};

#endif /* MonitorGUI_H_ */
