/*
 *	Decoder Class for LFSR Counter values from STiC2 data
 *	
 *	Date: 19.12.2012
 *	Author: Tobias, adopted from Michael Ritzert
 *
 *
 */



#ifndef LFSRDECODE_H_
#define LFSRDECODE_H_

#include <inttypes.h>
#include <assert.h>

class TLFSRDecode
{
	public:

		TLFSRDecode();
		///!< Init and return function for the decoding
		void init_lut();

		///!< Definition of the operator []
		inline int16_t operator[] ( const int16_t _lfsr )
		{
			return m_lut[ _lfsr ];
		} // operator[]

	private:
		///!< The LUT to decode LFSR
		int16_t m_lut[ 1 << 15 ];
};


#endif /* LFSRDECODE_H_ */
