/*
	DAQ class to read the STiC3 data sent over UDP by the FPGA
	Provides Access to basic commands for the FPGA,
	and saves the data in a ROOT Branch. For Monitoring, a ROOT histogram can be filled.

	Author: kbriggl, tharion

*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <arpa/inet.h>
#include <fstream>
#include <iostream>

//SLOW CONTROL
#include <mqueue.h>
#include <string.h>
#include "mq_message_dict.h"

//USB Communication Interface
#include "TUSB_iface.h"



#include "TMutex.h"


#include "STiC3DAQ.h"

//LENGTH OF AN EVENT IN BYTES
#define EVENT_LEN 7

#define BUFSIZE 1024
#define DAQ_PORT 10

//#define DEBUG
//#define DEBUG_USB
//#define DEBUG_VERBOSE
//#define DEBUG_EVENT
//#define DEBUG_MAPPING
//#define DEBUG_FETCH
//#define DEBUG_PACKAGES
#define DECODE

#define VCO_SHIFT 0


TSTiC3DAQ::monitor::monitor(mode m){
	hist=NULL;
	cut_method=NULL;
	fMode=m;
	if(m==MODE_PERIOD)
		previous_events=new stic3_data_t;
	if(m==MODE_T)
		previous_events=new stic3_data_t[2];
}
TSTiC3DAQ::monitor::~monitor(){
	if(!previous_events)
		delete[] previous_events;
}


TSTiC3DAQ::TSTiC3DAQ(bool offline){
    if(!offline)
	    fRunning=true;
    else
        fRunning=false;
    //fbin = false;
    binFile = NULL;
	scqueue_cmd=-1;
	scqueue_reply=-1;


	current_event= NULL; //new stic3_data_t;
	/*net in buffer*/
    chunk_buffer=(char*)malloc(BUFSIZE*2);
	if (chunk_buffer==NULL){
		perror("TSTiC3DAQ: malloc");
		return;
	}
    
	//USB interconnection
    if(!offline) {
	    //No changes are required, the default values read from the Visions SP6 board
	    usb_iface = new TUSB_iface;
	    usb_iface->set_timeout(2000);
	    if (!usb_iface->check_status())
	    {
		    perror("TUSB_iface: Device not found!\n");
	    }

	    fmonitors = new std::vector<monitor>;
    }

	//standard message queues
	SCMessageQueue("DAQ");

	// get the beam profile mapping
    GetBeamProfileMapping();

	// USB counters
	n_usb_bulk_packages = 0;
    n_usb_bulk_packages_double = 0;
    n_usb_bulk_packages_lost = 0;

}

TSTiC3DAQ::~TSTiC3DAQ(){

	if (scqueue_cmd){
		mq_close(scqueue_cmd);
	}
	if (scqueue_reply){
		mq_close(scqueue_reply);
	}
}

TTree*	TSTiC3DAQ::GetTree() {
	return fTree;
}

void	TSTiC3DAQ::DeleteTree(){
	rw_mutex.Lock();
	fTree->Delete("all"); //not working... will quit if call this....
	rw_mutex.UnLock();
}


void	TSTiC3DAQ::SetBranch(TTree* tree, const char* branchname){

	rw_mutex.Lock();
	fTree=tree;
	fBranch=fTree->GetBranch(branchname);
	fBranch->SetAddress(&current_event);
	rw_mutex.UnLock();

}


void    TSTiC3DAQ::ResetUsbPakageNo(){
	//n_usb_bulk_package = 0; 
	//n_usb_bulk_package_double = 0;
}


TBranch* TSTiC3DAQ::AddBranch(TTree* tree, const char* branchname){
	rw_mutex.Lock();
	fTree=tree;
	fBranch=fTree->Branch(branchname,"stic3_data_t", &current_event);
	rw_mutex.UnLock();
	return fBranch;
}



TSTiC3DAQ::monitor_ref TSTiC3DAQ::AddMonitor(monitor mon){
	rw_mutex.Lock();
	fmonitors->push_back(mon);
	rw_mutex.UnLock();
	return fmonitors->end();
}

void TSTiC3DAQ::RemoveMonitor(monitor_ref m){
	rw_mutex.Lock();
	fmonitors->erase(m);
	rw_mutex.UnLock();
}

int TSTiC3DAQ::GetBeamProfileMapping(){
	unsigned short channel;
	std::ifstream inFile;
	const char* fileName = "beam_profile_mapping.txt";
	inFile.open(fileName);
	if(!inFile.good()) {
		printf("GetBeamProfileMapping: Cannot open file: %s", fileName);
		return -1;
	}
	printf("Reading Beam Profile Mapping File: %s\n",fileName);
	int ncount = 0;
	while(!inFile.eof()) {
		inFile >> channel >> fBeanPosition[ncount].x >> fBeanPosition[ncount].y;
#ifdef DEBUG_MAPPING
		printf("channel:%hd; x: %d; y: %d\n",channel,fBeanPosition[ncount].x, fBeanPosition[ncount].y);
#endif
		fBeamProfileMapping.insert(std::make_pair(channel,fBeanPosition[ncount]));
		ncount++;
		//if(ncount>=256) break;
	}
	inFile.close();
	return 0;
}

struct TSTiC3DAQ::tPosition TSTiC3DAQ::GetChannelPosition(unsigned short channel) {
	return fBeamProfileMapping.at(channel);
}

int TSTiC3DAQ::SCMessageQueue(const char* qname){
	if (scqueue_cmd){
//		mq_unlink(scqueue_cmd);
		mq_close(scqueue_cmd);
	}
	if (scqueue_reply){
//		mq_unlink(scqueue_reply);
		mq_close(scqueue_reply);
	}

	struct mq_attr attr;
	attr.mq_maxmsg=10;
	attr.mq_msgsize=sizeof(struct msg_struct);
	attr.mq_flags=0;

	char name[30];
	sprintf(name,"/%s_cmd.0",qname);
	scqueue_cmd=	mq_open(name,O_RDONLY|O_CREAT|O_NONBLOCK, 0664,&attr);
	if(scqueue_cmd < 0){
		printf("DAQ Slow Control connection: Error opening cmd message queue (%d):",errno);
		perror("");
	}
#ifdef DEBUG
	else
		printf("DEBUG: Opened cmd message queue %s | ID: %d \n",name, scqueue_cmd);
#endif

	sprintf(name,"/%s_reply.1",qname);
	scqueue_reply=	mq_open(name,O_WRONLY|O_CREAT, 0664,&attr);
	if(scqueue_reply < 0){
		printf("DAQ Slow Control connection: Error opening reply message queue (%d):",errno);
		perror("");
	}
#ifdef DEBUG
	else
		printf("DEBUG: Opened reply message queue %s | ID: %d \n",name, scqueue_reply);
#endif
}

int TSTiC3DAQ::PauseReadout(){
	daq_data_struct msg;
	msg.cmd='E';
	msg.port=DAQ_PORT;
	msg.handleID = 0; //A handle ID is required!
	msg.len=0;
	if ((numbytes = usb_iface->write_usb((unsigned char*) &msg, sizeof(daq_data_header)))==-1){
		perror("TSTiC3DAQ: sendto");
		return -1;
	}
	return 0;

}
int TSTiC3DAQ::StartReadout(){
	daq_data_struct msg;
	msg.cmd='e';
	msg.handleID = 0; //A handle ID is required!
	msg.port=DAQ_PORT;
	msg.len=0;
	if ((numbytes = usb_iface->write_usb((unsigned char*) &msg, sizeof(daq_data_header)))==-1){
		perror("TSTiC3DAQ: sendto");
		return -1;
	}
	return 0;
}

int TSTiC3DAQ::SetSync(unsigned char sync){
	daq_data_struct msg;
	msg.cmd='s';
	msg.port=DAQ_PORT;
	msg.len=1;
	msg.payload[0]=sync;

	for(int i=0; i<4; i++)
	{
		msg.handleID = i;
		if ((numbytes = usb_iface->write_usb((unsigned char*) &msg, sizeof(daq_data_header) +1 ))==-1){
			perror("TSTiC3DAQ: sendto");
			return -1;
		}
	}
	return 0;
}

int TSTiC3DAQ::StartDebugEvents(int no){
    daq_data_struct msg;
    msg.cmd='b';
    msg.port=DAQ_PORT;
    msg.handleID = 0; //A handle ID is required!
    if(no < 256)
        msg.len = no;
    else
        msg.len= 255;
    printf("msg.len: %d", msg.len);
    if ((numbytes = usb_iface->write_usb((unsigned char*) &msg, sizeof(daq_data_header)))==-1){
        perror("TSTiC3DAQ: sendto");
        return -1;
    }
    return 0;
}

void TSTiC3DAQ::StopReadout() {
    fRunning=false;
    StopDebugEvents();
    Disable2kPackages();
}

int TSTiC3DAQ::StopDebugEvents(){
    daq_data_struct msg;
    msg.cmd='B';
    msg.port=DAQ_PORT;
    msg.handleID = 0; //A handle ID is required!
    msg.len= 0;
    if ((numbytes = usb_iface->write_usb((unsigned char*) &msg, sizeof(daq_data_header)))==-1){
        perror("TSTiC3DAQ: sendto");
        return -1;
    }
    return 0;
}

int TSTiC3DAQ::Enable2kPackages(){
    daq_data_struct msg;
    msg.cmd='s';
    msg.port=DAQ_PORT;
    msg.handleID = 0; //A handle ID is required!
    msg.len= 0;
    if ((numbytes = usb_iface->write_usb((unsigned char*) &msg, sizeof(daq_data_header)))==-1){
        perror("TSTiC3DAQ: sendto");
        return -1;
    }
    f2kPackages = true;
    return 0;
}

int TSTiC3DAQ::Disable2kPackages(){
    daq_data_struct msg;
    msg.cmd='S';
    msg.port=DAQ_PORT;
    msg.handleID = 0; //A handle ID is required!
    msg.len= 0;
    if ((numbytes = usb_iface->write_usb((unsigned char*) &msg, sizeof(daq_data_header)))==-1){
        perror("TSTiC3DAQ: sendto");
        return -1;
    }
    f2kPackages = false;
    return 0;
}


void TSTiC3DAQ::BufferToFile(unsigned int* const buf_ptr, unsigned int len) {
    //std::cout << "BufferToFile:" << binFile << " len: " << len << std::endl;
    binFile->write((const char *)buf_ptr, len);

}


void TSTiC3DAQ::BufferToEvent(unsigned int* const buf_ptr,stic3_data_t* const event){
	/*
	 *	Frame Composition:
	 *
	 *	unsigned int* bin_event has to be provided as: bin_event[0] = [0:-7], bin_event[1] = [7:0] .....
	 *
	 	data.frame := vec(55 downto 48);
		data.channel := vec(47 downto 42);
		data.T_BadHit := vec(41);
		data.TCC := vec(40 downto 26);
		data.T_Fine := vec(25 downto 21);
		data.E_BadHit := vec(20);
		data.ECC := vec(19 downto 5);
		data.E_Fine := vec(4 downto 0);

	 *	bin_event[0]=[7:0]	: {Energy CC[2:0], Energy Fine[4:0]}
	 *	bin_event[1]=[15:8]	: Energy CC[10:3]
	 *	bin_event[2]=[23:16] : {T_fine[2:0], Energy BadHit, Energy CC[14:11]}
	 *	bin_event[3]=[31:24] : {Time CC[5:0], T_fine[4:3]}
	 *	bin_event[4]=[39:32] : {Time CC[13:6]}
	 *	bin_event[5]=[47:40] : {channel[5:0], Time BadHit,Time CC[14]}
	 *	bin_event[6]=[55:48] : Frame Number
	 */


	// Bit shift ruler... : 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
	//The coarse counter values are decoded on the fly....


	event->E_fine =  (unsigned short) 	(( 0x0000001f & buf_ptr[0]));
#ifdef DECODE
	event->E_CC = decoder[ (unsigned int)	(( 0x000fffe0 & buf_ptr[0]) >> 5)	];
#else
	event->E_CC =  (unsigned int)	(( 0x000fffe0 & buf_ptr[0]) >> 5);
#endif
	event->E_badhit=  			(( 0x00100000 & buf_ptr[0]) != 0 )? true:false;

	event->T_fine =		(unsigned short)(( 0x03e00000 & buf_ptr[0]) >> 21);

#ifdef DECODE
	event->T_CC = decoder[	(unsigned int) ((( 0x1FF & buf_ptr[1] ) << 6 ) + (0x3F & (buf_ptr[0]>>26)) ) ];
#else
	event->T_CC = (unsigned int) ((( 0x1FF & buf_ptr[1] ) << 6 ) + (0x3F & (buf_ptr[0]>>26)) );

	printf("TCC DEBUG: Reconstructed value: %03X %02X\n\n",(( 0x1FF & buf_ptr[1] ) << 6 ), (0x3F & (buf_ptr[0]>>26)));
#endif

	event->T_badhit=  			(( 0x00000200 & buf_ptr[1]) != 0 )? true:false;
	event->channel = 	(unsigned short) ((0x0000fc00 & buf_ptr[1]) >> 10);
	event->frame_number =	(unsigned short) ((0x00ff0000 & buf_ptr[1]) >> 16);

#ifdef DEBUG_EVENT

	printf("Event Hex Code: %X %X \n\n",0x00ffffffff&buf_ptr[1], buf_ptr[0]);

	printf("E_fine: %u\n",current_event->E_fine);
	printf("E_CC: %u\n",current_event->E_CC);
	printf("E_BADHIT: %u\n",current_event->E_badhit);
	printf("T_FINE: %u\n",current_event->T_fine);
	printf("T_CC: %u\n",current_event->T_CC);
	printf("T_BADHIT: %u\n",current_event->T_badhit);
	printf("CHANNEL: %u\n",current_event->channel);
	printf("FRAME: %u\n\n",current_event->frame_number);
#endif

}

short TSTiC3DAQ::ProcessRawEvent(stic3_data_t* const current_event)
{


	current_event->time = current_event->T_CC*0x20 + current_event->T_fine;
	current_event->energy = current_event->E_CC - current_event->T_CC;

	return 0;


}

void TSTiC3DAQ::ProcessMonitors( stic3_data_t* const current_event, bool matrix)
{

  if(fmonitors->size() == 0)
      return;

  for(int i_monitor=0;i_monitor<fmonitors->size();i_monitor++)
    {
      if(fmonitors->at(i_monitor).cut_method && !fmonitors->at(i_monitor).cut_method(current_event))
	continue;
      if(fmonitors->at(i_monitor).hist == NULL)
	continue;

      switch (fmonitors->at(i_monitor).fMode) { //switch
      case MODE_E:
	if ((current_event->channel+current_event->handleID*64) == fmonitors->at(i_monitor).fChannel1){
	  fmonitors->at(i_monitor).hist->Fill( current_event->energy);}
	break;

      case MODE_TSINGLE :
	if ((current_event->channel+current_event->handleID*64) == fmonitors->at(i_monitor).fChannel1){
	  fmonitors->at(i_monitor).hist->Fill( current_event->time );}
	break;

      case MODE_TSINGLE_FINE :
	if ((current_event->channel+current_event->handleID*64) == fmonitors->at(i_monitor).fChannel1){
	  fmonitors->at(i_monitor).hist->Fill( current_event->T_fine );}
	break;

      case MODE_TBADHIT :
	if ((current_event->channel+current_event->handleID*64) == fmonitors->at(i_monitor).fChannel1){
	  fmonitors->at(i_monitor).hist->Fill( current_event->T_badhit );}
	break;

      case MODE_TSINGLE_CM :
	if ((current_event->channel+current_event->handleID*64) == fmonitors->at(i_monitor).fChannel1){
	  fmonitors->at(i_monitor).hist->Fill( current_event->T_CC );}
	break;

      case MODE_CHANNEL :
#ifdef DEBUG_MAPPING
	printf("MODE_CHANNEL: channel: %d\n",(current_event->channel+current_event->handleID*64));
#endif
	fmonitors->at(i_monitor).hist->Fill( (current_event->channel+current_event->handleID*64) );
	break;

      case MODE_PERIOD :
	if ((current_event->channel+current_event->handleID*64) == fmonitors->at(i_monitor).fChannel1 && fmonitors->at(i_monitor).previous_events->time != 0 && current_event->time !=0)
	  {
	    fmonitors->at(i_monitor).hist->Fill( current_event->time - fmonitors->at(i_monitor).previous_events->time);
	  }
	if((current_event->channel+current_event->handleID*64) == fmonitors->at(i_monitor).fChannel1) memcpy(fmonitors->at(i_monitor).previous_events,current_event,sizeof(stic3_data_t));
	break;

      case MODE_T :
	if((current_event->channel+current_event->handleID*64) == fmonitors->at(i_monitor).fChannel1){
	  if( abs(fmonitors->at(i_monitor).previous_events[1].frame_number - current_event->frame_number) < 2 \
	      && current_event->time!=0 && fmonitors->at(i_monitor).previous_events[1].time!=0 \
	      && current_event->energy > fmonitors->at(i_monitor).ch1_ECut_low \
	      && current_event->energy < fmonitors->at(i_monitor).ch1_ECut_high \
&& fmonitors->at(i_monitor).previous_events[1].energy > fmonitors->at(i_monitor).ch2_ECut_low \
	    && fmonitors->at(i_monitor).previous_events[1].energy < fmonitors->at(i_monitor).ch2_ECut_high \
	      )
	    {
	      //the saved hit in the other channel was not too long ago.
	      fmonitors->at(i_monitor).hist->Fill( (long int)fmonitors->at(i_monitor).previous_events[1].time - (long int)current_event->time );
	    }
	  //copy the new event
	  memcpy(&fmonitors->at(i_monitor).previous_events[0],&current_event,sizeof(stic3_data_t));

	}else if((current_event->channel+current_event->handleID*64) == fmonitors->at(i_monitor).fChannel2 ){
	  if( abs(fmonitors->at(i_monitor).previous_events[0].frame_number - current_event->frame_number) < 2 \
	      && current_event->time!=0 && fmonitors->at(i_monitor).previous_events[0].time!=0 \
	      && current_event->energy > fmonitors->at(i_monitor).ch2_ECut_low \
	      && current_event->energy < fmonitors->at(i_monitor).ch2_ECut_high \
	&& fmonitors->at(i_monitor).previous_events[0].energy > fmonitors->at(i_monitor).ch1_ECut_low \
	      && fmonitors->at(i_monitor).previous_events[0].energy < fmonitors->at(i_monitor).ch1_ECut_high \
	      )
	    {
	      //the saved hit in the other channel was not too long ago.
	      fmonitors->at(i_monitor).hist->Fill((long int)current_event->time - (long int)fmonitors->at(i_monitor).previous_events[0].time);
	    }
	  //copy the new event
	  memcpy(&fmonitors->at(i_monitor).previous_events[1],this->current_event,sizeof(stic3_data_t));
	}
	break;

	//from here it is not important to do after each event, can be done outside
      case MODE_E_MEAN :
	if(matrix){
	  if(fmonitors->at(i_monitor).hist == NULL){
	    printf("no hist %d------------------------\n",i_monitor);
	    continue;
	  }else{
	    int handle = fmonitors->at(i_monitor).fChannel1;
	    int shift = fmonitors->at(i_monitor).fChannel2;
	    for(int i_ch=1;i_ch<65;i_ch++)
	      {
		//printf("1 %d %d %d %d\n",i_monitor,i_ch,handle,shift);
		double mean = fmonitors->at(shift+handle*64+i_ch).hist->GetMean();
		if(fmonitors->at(shift+handle*64+i_ch).hist->GetEntries() >0) fmonitors->at(i_monitor).hist->SetBinContent(i_ch,mean);
		//printf("2 %d %d %d %d\n",i_monitor,i_ch,handle,shift);
	      }
	  }
	}
	break;

      case MODE_E_RMS :
	if(matrix){
	  if(fmonitors->at(i_monitor).hist == NULL){
	    printf("no hist %d------------------------\n",i_monitor);
	    continue;
	  } else {
	    int handle = fmonitors->at(i_monitor).fChannel1;
	    int shift = fmonitors->at(i_monitor).fChannel2;
	    for(int i_ch=1;i_ch<65;i_ch++)
	      {
		double rms =fmonitors->at(shift+handle*64+i_ch).hist->GetRMS();
		if(fmonitors->at(shift+handle*64+i_ch).hist->GetEntries() >0)fmonitors->at(i_monitor).hist->SetBinContent(i_ch,rms);
	      }
	  }
	}
	break;

      case MODE_F_MEAN :
	if(matrix){
	  if(fmonitors->at(i_monitor).hist == NULL){
	    printf("no hist %d------------------------\n",i_monitor);
	    continue;
	  }else{
	    int handle = fmonitors->at(i_monitor).fChannel1;
	    int shift = fmonitors->at(i_monitor).fChannel2;
	    for(int i_ch=1;i_ch<65;i_ch++)
	      {
		//printf("1 %d %d %d %d\n",i_monitor,i_ch,handle,shift);
		double mean=fmonitors->at(shift+handle*64+i_ch).hist->GetMean()*50.2*1e-12;
		if(mean!=0) mean=1./mean;
		if(fmonitors->at(shift+handle*64+i_ch).hist->GetEntries()>0)fmonitors->at(i_monitor).hist->SetBinContent(i_ch,mean);
		//printf("1b %d %d %d %d\n",i_monitor,i_ch,handle,shift);
	      }
	  }
	}
	break;

      case MODE_F_RMS:
	if(matrix){
	  if(fmonitors->at(i_monitor).hist == NULL){
	    printf("no hist %d------------------------\n",i_monitor);
	    continue;
	  } else {
	    int handle = fmonitors->at(i_monitor).fChannel1;
	    int shift = fmonitors->at(i_monitor).fChannel2;
	    for(int i_ch=1;i_ch<65;i_ch++)
	      {
		//printf("2 %d %d %d %d\n",i_monitor,i_ch,handle,shift);
		double rms =fmonitors->at(shift+handle*64+i_ch).hist->GetRMS()*50.2*1e-12;
		if(rms!=0) rms=1./rms;
		if(fmonitors->at(shift+handle*64+i_ch).hist->GetEntries()>0)fmonitors->at(i_monitor).hist->SetBinContent(i_ch,rms);

	      //printf("2b %d %d %d %d\n",i_monitor,i_ch,handle,shift);
	      }
	  }
	}
	break;

	//for the beam profile
      case MODE_BEAM_PROFILE:
#ifdef DEBUG_MAPPING
	printf("MODE_BEAM_PROFILE: channel: %hd\n", (current_event->channel+current_event->handleID*64));
	printf("MODE_BEAM_PROFILE: position: %d	%d \n",(GetChannelPosition(current_event->channel+current_event->handleID*64).x),(GetChannelPosition(current_event->channel+current_event->handleID*64).y));
#endif
	//if((current_event->channel+current_event->handleID*64)>63){
	fmonitors->at(i_monitor).hist->Fill( (GetChannelPosition(current_event->channel+current_event->handleID*64).x), (GetChannelPosition(current_event->channel+current_event->handleID*64).y));
      //}
	break;

      default :
	printf("%d monitor is not assigned!!!\n");

      } //switch

    } //list iterator
}



short TSTiC3DAQ::FetchEventData(unsigned short handleID){

	usb_struct usb_message;	//USB MESSAGE STRUCT FOR COMMUNICATION WITH THE FPGA
	int numbytes;

	unsigned int* buf_ptr;
	unsigned int* buf_end;
	int n_events = 0;
	bool matrix = false;

	//ISSUE A FETCH PACKET REQUESTING DATA FROM THE DAQ WITH DATA

	usb_message.cmd = 'f';	//FETCH COMMAND TO GET DATA FROM FIFO WITH THE SPECIFIED HANDLEID
	usb_message.handleID = handleID;
	usb_message.len = 0;
	usb_message.port = 0x3F&curr_packetID;	//SET THE CURRENT PACKET ID FOR THE TRANSFER

	//SEND THE PACKET TO REQUEST THE DATA
#ifdef DEBUG_VERBOSE
	printf("TSTiC3DAQ: Sending packet out\n");
	printf("Debug: Sizeof package: %lu bytes\n",sizeof(usb_header) + usb_message.len);
#endif

        #ifdef DEBUG_FETCH	
	printf("send fetch:                              \r");
        #endif

	numbytes = usb_iface->write_usb((unsigned char*)&usb_message,sizeof(usb_header) + usb_message.len);
	if(numbytes < 0) perror("TSTiC3DAQ: USB Send Error\n");

    #ifdef DEBUG_PACKAGES
    printf("send %d, ",usb_message.port);
    #endif

    #ifdef DEBUG_FETCH
	printf("send fetch: ok                           \r");
	#endif

        last_packetID=0x3F &curr_packetID;	//SAVE THE PACKET ID FOR IDENTIFYING THE LAST PACKET TRANSMISSION
	curr_packetID++;		//INCREMENT THE PACKETID FOR THE NEXT TRANSMISSION

    //usleep(5000);
    usleep(40);
    // SIMON: Minimize!
	//THE TIMEOUT SHOULD BE SUFFICIENT TO RECEIVE THE FULL 1024 BYTE DATA PACKET

        #ifdef DEBUG_FETCH
        printf("read usb:                                  \r");
        #endif

    //last_port = ((usb_header*)chunk_buffer)->port;
    if(f2kPackages)
        numbytes=usb_iface->read_usb((unsigned char*)chunk_buffer,BUFSIZE*2);
    else
        numbytes=usb_iface->read_usb((unsigned char*)chunk_buffer,BUFSIZE);

        #ifdef DEBUG_PACKAGES
        printf("got %d %c %d %d", ((usb_header*)chunk_buffer)->port, ((usb_header*)chunk_buffer)->cmd, ((usb_header*)chunk_buffer)->len, numbytes);
        #endif

        #ifdef DEBUG_FETCH
        printf("read usb: ok                               \r");
        #endif



    //if(numbytes > 0 && ((usb_header*)chunk_buffer)->cmd == 'd' && usb_message.port != ((usb_header*)chunk_buffer)->port) {
    if(usb_message.port != ((usb_header*)chunk_buffer)->port) {
        #ifdef DEBUG_PACKAGES
        printf(" offset! \n");
        #endif

        if(f2kPackages) {
            usb_iface->clear_usb();
            n_usb_bulk_packages_lost++;
            return -9;
        }
    }

        if(!f2kPackages){
            while(numbytes==BUFSIZE && ((usb_header*)chunk_buffer)->port != last_packetID)
            //while((((usb_header*)chunk_buffer)->port < last_packetID ) || ((last_packetID == 0) && (((usb_header*)chunk_buffer)->port < last_packetID < 64)))
            {
                if(!fRunning) {
                    printf("DAQ stopped while reading...\n");
                    return -5;
                }

                    #ifdef DEBUG_FETCH
                    printf("wrong header id, read again               \r");
                    #endif
                n_usb_bulk_packages_double++;
                    #ifdef DEBUG_VERBOSE
                    printf("TSTiC3DAQ: Bad Data Packet ID %d expected %d, reading next packet\n",((usb_header*)chunk_buffer)->port,last_packetID);
                    #endif

                //usleep(100000);
                //usleep(10000); //used value
                // starts to fail below 50 (depends on auto save rate!)
                usleep(75);
                    #ifdef DEBUG_FETCH
                    printf("read again:                         \r");
                    #endif
                numbytes=usb_iface->read_usb((unsigned char*)chunk_buffer,BUFSIZE);
                    #ifdef DEBUG_FETCH
                    printf("read again: ok                       \r");
                    #endif
                    #ifdef DEBUG_PACKAGES
                    printf("got %d %c %d %d", ((usb_header*)chunk_buffer)->port, ((usb_header*)chunk_buffer)->cmd, ((usb_header*)chunk_buffer)->len, numbytes);
                    #endif
           }
        }

        #ifdef DEBUG_PACKAGES
        printf("\n");
        #endif

        #ifdef DEBUG_FETCH
        if(((usb_header*)chunk_buffer)->port != last_packetID)
            printf("Package IDs still don't match");
        #endif


#ifdef DEBUG_VERBOSE
	if( numbytes > 0){

		//remote data available
		printf("Remote data.\n");

		printf(" HEADER HEX: %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x\n", chunk_buffer[0], chunk_buffer[1], chunk_buffer[2], chunk_buffer[3]
							, chunk_buffer[4], chunk_buffer[5], chunk_buffer[6], chunk_buffer[7]
							, chunk_buffer[8], chunk_buffer[9], chunk_buffer[10], chunk_buffer[11]
				);


		printf("STiC3DAQ: recv: packet is %d bytes long\n", numbytes);
		//Demux packet
		//TODO: Change this to use the ports defined in the USB packet

		printf("Remote CMD: %c\n",((usb_header*)chunk_buffer)->cmd);
		printf("Remote PACKETID: %d\n",((usb_header*)chunk_buffer)->port);
		printf("Remote LEN: %d\n",((usb_header*)chunk_buffer)->len);
		printf("HANDLE ID: %d\n",((usb_header*)chunk_buffer)->handleID);
	}
#endif

	if(numbytes < 0){
		printf("TSTiC3DAQ: Error usb read: %d\n",numbytes);
        n_usb_bulk_packages_lost++;
		return numbytes;
	}

	if( ((usb_header*)chunk_buffer)->cmd != 'd'){
		printf("Bad command field received: %X\n",((usb_header*)chunk_buffer)->cmd);
		return -3;
	}

    if (!f2kPackages && numbytes > 0 && numbytes != BUFSIZE){
		printf("Did not receive the full amount of data: %d\n", numbytes);
			return -2;
	}

    if (f2kPackages && numbytes > 0 && numbytes != BUFSIZE*2){
        printf("2k) Did not receive the full amount of data: %d\n", numbytes);
        n_usb_bulk_packages_lost++;
        return -2;
    }

	//size check
	if ((numbytes < sizeof(usb_header)) || (numbytes < sizeof(usb_header) + ((usb_header*)chunk_buffer)->len)){
		printf("DAQ: Omitted data packet due to length inconsistency\n");
        n_usb_bulk_packages_lost++;
        return -5;
	}

#ifdef DEBUG_USB
    printf("cmd:\t\t%c\nnumbytes:\t%d\nlen:\t\t%d\nevents:\t%d", ((usb_header*)chunk_buffer)->cmd, numbytes, ((usb_header*)chunk_buffer)->len, ((usb_header*)chunk_buffer)->len/8);
#endif    

    n_usb_bulk_packages++;
    if(binFile == NULL) {

        current_event->handleID=((usb_struct*)chunk_buffer)->handleID;
        current_event->packetID=((usb_struct*)chunk_buffer)->port;
        buf_ptr=(unsigned int*)((usb_struct*)chunk_buffer)->payload;
        buf_end=(unsigned int*)&((usb_struct*)chunk_buffer)->payload[((usb_header*)chunk_buffer)->len];

        #ifdef DEBUG_VERBOSE
            printf("Expected number of events in packet: %d\n",(((usb_header*)chunk_buffer)->len)/8);
        #endif

        while (buf_ptr < buf_end){
            /*extract the event from the buffer*/
            BufferToEvent(buf_ptr, current_event);

    //		ProcessRawEvent(current_event);
            if ( ProcessRawEvent(current_event) == 0)
            {
              n_events++;
                /************************************/
                /*aquire lock to access tree and histograms*/
                rw_mutex.Lock();

                /************************************/
                /*event is done, write to tree*/
                if(fBranch != NULL){
                    //fBranch->Fill();
                    fTree->Fill();
                }

            /************************************/
            /*iterate over monitors*/

                if(n_events % 100 == 0 ) {
                  matrix = true;
    #ifdef DEBUG
                  printf("n_event %d %d\n",n_events,matrix);
    #endif
                }
                ProcessMonitors(current_event, matrix);
                matrix=false;
            }

            /*release lock to access tree and histograms*/
            /************************************/
            rw_mutex.UnLock();


            //Next Event	--FOR THE USB DAQ THE EVENT LENGTH IS 8 BYTES (7 data + 1 padding to 32 bit)
            buf_ptr=&buf_ptr[2];

        }//payload loop
    } else {
	//std::cout << ((usb_header*)chunk_buffer)->len << std::endl;
	#ifdef DEBUG_FETCH
	printf("buffer to file:                           \r");
        #endif
	BufferToFile((unsigned int*)((usb_struct*)chunk_buffer)->payload, ((usb_header*)chunk_buffer)->len);
   	#ifdef DEBUG_FETCH
	printf("buffer to file: ok\r"); 
	#endif
   }

    // printf("Events in package: %i\n",n_events);
}


short TSTiC3DAQ::ProcessLocalMQ(msg_struct* sc_message)
{
	usb_struct usb_message;	//USB MESSAGE STRUCT FOR COMMUNICATION WITH THE FPGA

	//RETURN VALUES
	int numbytes;
	int ret;

	usb_message.cmd = sc_message->cmd;
	usb_message.handleID = sc_message->handleID;
	usb_message.len = sc_message->len;
	usb_message.port = curr_packetID;	//SET THE CURRENT PACKET ID FOR THE TRANSFER
	memcpy(usb_message.payload,sc_message->msg,sc_message->len);


#ifdef DEBUG
	for(int i=511;i>=0;i--)
	{
		printf(" %.1x ",((char*)&usb_message)[i]&0xff);
		if (i%4 == 0) printf("\n");
	}
	printf("\n");
#endif

	//USB PACKET SEND
#ifdef DEBUG
	printf("TSTiC3DAQ: Sending packet out\n");
	printf("Debug: Sizeof package: %lu bytes\n",sizeof(usb_header) + usb_message.len);
#endif
    usb_iface->write_usb((unsigned char*)&usb_message,sizeof(usb_header) + sc_message->len);

	last_packetID=0x3F&curr_packetID;	//SAVE THE PACKET ID FOR IDENTIFYING THE LAST PACKET TRANSMISSION
	curr_packetID++;		//INCREMENT THE PACKETID FOR THE NEXT TRANSMISSION

	if (numbytes==-1)
	{
		perror("TSTiC3DAQ: sendto");
	}

	//usleep(10000);
	usleep(1000);

	if (sc_message->cmd == 'c' || sc_message->cmd=='D'){	//Configuration and Debug expects response from the FPGA
		numbytes=usb_iface->read_usb((unsigned char*)chunk_buffer,BUFSIZE);	//1024 BYTES ARE EXPECTED FOR THE CONFIGURATION

		while(numbytes==BUFSIZE && ((usb_header*)chunk_buffer)->port < last_packetID)
		{
#ifdef DEBUG
			printf("TSTiC3DAQ: Bad Config Packet ID %d expected %d, reading next packet\n",((usb_header*)chunk_buffer)->port,last_packetID);
#endif
			numbytes=usb_iface->read_usb((unsigned char*)chunk_buffer,BUFSIZE);
			//usleep(1000);
			usleep(100); //used value
		}

		if(numbytes == -1){
			perror("TSTiC3DAQ: readusb");
			return -1;
		}
		else
		{

			if(((usb_header*)chunk_buffer)->port != last_packetID) printf("TSTiC3DAQ: ERROR Config: packetID received %d packetID expected: %d\n");
			if(((usb_header*)chunk_buffer)->cmd != 'c' && ((usb_header*)chunk_buffer)->cmd!= 'D' )
				printf("TSTiC3DAQ: ERROR unexpected CMD field '%c'\n",((usb_header*)chunk_buffer)->cmd);

			//check for lenght consistency
			if (numbytes < sizeof(daq_data_header) + ((usb_header*)chunk_buffer)->len){
				printf("DAQ: Omitted configuration reply due to length pad inconsistency\n");
				return -1;
			}

			//SEND THE DATA BACK

			sc_message->cmd = ((usb_struct*)chunk_buffer)->cmd;
			sc_message->handleID = ((usb_struct*)chunk_buffer)->handleID;
			sc_message->len = ((usb_struct*)chunk_buffer)->len;
			sc_message->port=((usb_struct*)chunk_buffer)->port;
			memcpy(sc_message->msg,((usb_struct*)chunk_buffer)->payload,1020);//Copy the rest of the packet to the msgstruct buffer

#ifdef DEBUG
			printf("TSTiC3DAQ: Returning Config/Debug packet to GUI\n");
			printf("TSTiC3DAQ: PACKET CMD: %c\n", sc_message->cmd);
			printf("TSTiC3DAQ: PACKET HANDLEID %d\n", sc_message->handleID);
			printf("TSTiC3DAQ: PACKET LEN %d\n", sc_message->len);
			printf("TSTiC3DAQ: PACKET PORT %d\n", sc_message->port);
#endif

			//send package
			ret=mq_send(scqueue_reply,
				(char*)sc_message,
				sizeof(msg_header) + sc_message->len,
				0
			);
			if(ret != 0){
				printf("ret= %d\n",ret);
				perror("mq_send");
			}
		}
	}

}

void TSTiC3DAQ::reader_method(){

	current_event= new stic3_data_t;

	unsigned int* buf_ptr;
	unsigned int* buf_end;
	msg_struct sc_message;

	usb_struct usb_message;

	fd_set rfds;
	struct timeval tv;
	int retval;
	int maxfd;

	int ret;

	unsigned int num_loop;
	num_loop=0;
    // Clear USB !
    usb_iface->clear_usb();
    //usleep(1000);



      while(fRunning){
	//fRunning_mutex.Lock();
	//printf("status: %d\r",fRunning);

	retval=mq_receive(scqueue_cmd,	(char*)&sc_message,sizeof(msg_struct), NULL);

    /* Simon
     *      If the message queue 'scqueue_cmd' is empty, mq_receive fails with EAGAIN.
     */

	if (retval < 0)
	{
		if (errno != EAGAIN) perror("TSTiC3DAQ: mq_receive");
	}
	else
	{
#ifdef DEBUG
		printf("Local data\n");

		printf("cmd=%.1x \n",sc_message.cmd);
		printf("ID=%.1x \n",sc_message.handleID);
		printf("len=%d \n",sc_message.len);
		printf("port=%d \n",sc_message.port);
#endif
		ProcessLocalMQ(&sc_message);
        	usleep(10);
		printf("Slow Controle: %.1x \n", sc_message.cmd);

	}//MQ READ

    // usleep(10);
	
	FetchEventData(1);	//AT THE MOMENT ONLY FETCH HANDLEID 0
//	FetchEventData(1);	//AT THE MOMENT ONLY FETCH HANDLEID 0
//	FetchEventData(2);	//AT THE MOMENT ONLY FETCH HANDLEID 0
//	FetchEventData(3);	//AT THE MOMENT ONLY FETCH HANDLEID 0
	//ProcessMonitorsMeanRMS( 0);
	
    // It was probably used to ensure autosave
    // usleep(500)ul;

    // Simon: Optimise this line
    // above 1000 and usleep(50) in loop it starts to fail
    //if(num_loop % 2000 == 0) {
   //     std::cout << num_loop << std::endl;
    //    fTree->AutoSave("SaveSelf");
    //}
    //fRunning_mutex.UnLock();
    if(num_loop % 2000 == 0 && binFile == NULL)
        fTree->AutoSave("SaveSelf");
    //
    num_loop++;



     }//while running
}//reader_loop

int TSTiC3DAQ::ProcessBufferToEvent(unsigned int* const buf_ptr, stic3_data_t* const event) {
    BufferToEvent(buf_ptr, event);
    return ProcessRawEvent(event);
}

int TSTiC3DAQ::ProcessBufferToEvent(unsigned int* const buf_ptr) {
    BufferToEvent(buf_ptr, current_event);
    return ProcessRawEvent(current_event);
}


