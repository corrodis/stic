#include "TApplication.h"
#include "TObject.h"
#include "TH1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TThread.h"
#include "TTree.h"
#include "TFile.h"
#include <unistd.h>

#include "STiC3DAQ.h"
#include "STiC3Config.h"
#include "STiC2FPGAConfig.h"
#include "MQInterface.h"
/*
** STIC2DAQ
**
** The Code will intanciate a DAQ class, start the readout and store the data recorded
** in 30 minutes to the dump.root file
*/

// a global canvas for the drawing of histograms
TCanvas *c1;

TSTiC3DAQ* daq_class;
TThread *th_daq;
// a global histogram object, will be accessed both threads
//dump tree
TH1D 	*hist;
TTree	*tree;
TBranch	*branch;
TFile 	*fout;

//SIGINT handler
#include <signal.h>
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	daq_class->StopReadout();
	TThread::Ps();
   	th_daq->Join();
	tree->Write();
	fout->Close();
	TThread::Ps();
	exit(0);
}




int main(int argc, char **argv)
{

	signal(SIGINT,&handler_sigint);

	float sleeptime=1800;

	if(argc<2)
	{
		printf("Usage: dumpto [filename] <record time in s>\n");
		return -1;
	}
	if(argc>2) sleeptime=atoi(argv[2]);

	// Set up configuration interface to reset chip and channel data
	TMQInterface iface("DAQ");
	iface.SetTimeout(0,5000000,0);
	TSTiC3Config *config = new TSTiC3Config(&iface);
	config->ChipReset();
	config->ChannelReset();
	printf("RESET Chip and Channel\n");
	delete config;


	printf("Writing to %s for %f seconds\n",argv[1],sleeptime);


	fout=new TFile(argv[1],"RECREATE");
	tree  = (TTree*)fout->Get("dump");


	daq_class = new TSTiC3DAQ();

 
	if(tree == NULL)
	{
		tree= new TTree("dump","dump");
		daq_class->AddBranch(tree,"br");
	}
	else
	{
		daq_class->SetBranch(tree,"br");
	}

    // Save configurations
    // TODO
    //daq_class->config->

	// start Thread
	// thread function (readout thread)
	th_daq = new TThread("daq", (TThread::VoidRtnFunc_t) &(TSTiC3DAQ::reader_thread), daq_class);
	daq_class->Disable2kPackages();
	th_daq->Run();

	daq_class->StartReadout();

	printf("Sleeping\n");
	TThread::Sleep(sleeptime);
	printf("Finished\n");

	daq_class->PauseReadout();

	daq_class->StopReadout();
	th_daq->Join();
	tree->Write(0,TObject::kOverwrite);
	fout->Close();

	printf("Processed USB Bulk Packages: %d\n", daq_class->GetUsbPackageNo());
	printf("Double Recieved USB Bulk Packages: %d\n", daq_class->GetUsbDoublePackageNo());
	printf("Bandwith loos due to double packages: %.3f\n", (float)daq_class->GetUsbDoublePackageNo()/(daq_class->GetUsbDoublePackageNo() + daq_class->GetUsbPackageNo()));

	printf("The END...\n");

	return 0;
}

