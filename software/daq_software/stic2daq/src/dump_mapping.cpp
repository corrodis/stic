#include "TApplication.h"
#include "TObject.h"
#include "TH1.h"
#include "TH2.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TThread.h"
#include "TTree.h"
#include "TFile.h"
#include "TStyle.h"
#include "TPaveText.h"
#include "TLine.h"
#include "STiC3DAQ.h"
#include "MonitorGUI.h"

#include <iostream>
#include <fstream>


/*
** STIC2DAQ
**
** The Code will intanciate a DAQ class, set appropriate parameters
** and start a separate Thread where the data is read out.
** In the Main thread, a Histogram is Drawn and updated
*/

// a global canvas for the drawing of histograms
TCanvas *c1;
// a global canvas for the beam profile
TCanvas *c2;

TSTiC3DAQ* daq_class;
//Monitor GUI interface
MonitorGUI* mon_gui;

TThread *th_daq;
// a global histogram object, will be accessed both threads
//dump tree
TH1I 	*hist;
TH1I	*hist2;
TH1I	*hist3;
TH1I	*hist4;
TTree	*tree;
TBranch	*branch;
TFile 	*fout;

// a global histogram for the beam profile
TH2I	*hist_map;
TH2I	*hist_map_channel_number;

//SIGINT handler
#include <signal.h>
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	//daq_class->StopReadout();	//TODO: will not cancel the thread if no data is incoming
	TThread::Ps();
   	th_daq->Join();
	tree->Write();
	fout->Close();
	TThread::Ps();
	exit(0);
}



void *top(void *)
{
   TThread::Printf("Start of top");

   for (Int_t i = 0; ;i++) {
      if(c1!=NULL){
//      	c1->cd(1);
        c1->GetPad(1)->Update();
        c1->GetPad(1)->Modified();
//	c1->cd(2);
        c1->GetPad(2)->Update();
        c1->GetPad(2)->Modified();
        c1->GetPad(3)->Update();
        c1->GetPad(3)->Modified();
        c1->GetPad(4)->Update();
        c1->GetPad(4)->Modified();

      }
      if(c2!=NULL) {
	c2->Update();
	c2->Modified();
      }
      if(i % 30 == 0){
      		TThread::Ps();
		printf("Recorded %lld events\n",branch->GetEntries());
//		tree->Scan("","","",100);
//		hist->Clear();
//  		hist->Reset();
//	      	hist2->Clear();
//  		hist2->Reset();
//      		hist3->Clear();
//  		hist3->Reset();
//	      	hist4->Clear();
//		hist4->Reset();
      }
      TThread::Sleep(0.25);
   }
   TThread::Printf("End of top");
   return 0;
}

// get the channel mapping from the file to the histogram
int GetMappingChannelNumber(const char* fileName, TH2* hist)
{
	int channel,x,y;
	std::ifstream inFile;
	inFile.open(fileName);
	if(!inFile.good()) {
		printf("dump_mapping: GetMappingChannelNumber: Cannot open file: %s", fileName);
		return -1;
	}
	printf("dump_mapping: GetMappingChannelNumber: Reading Beam Profile Mapping File: %s\n",fileName);
	while(!inFile.eof()) {
		inFile >> channel >> x >> y;
		if(channel < 64) hist->Fill(x,y,channel);
		else hist->Fill(x,y,channel-64);
	}
	inFile.close();
	return 1;
}


int main(int argc, char **argv)
{

   signal(SIGINT,&handler_sigint);
   gStyle->SetOptStat(111111);
   TApplication theApp("DAQ", &argc, argv);
   // exit on close window


   // a global canvas for the drawing of histograms
   c1 = new TCanvas("c1","Readout Monitor",1900,550,900,450);
   c1->Connect("Closed()","TApplication",&theApp,"Terminate(=0)");
   
   // a global histogram object, which will be accessed both by mhs and mhs1
   hist  = new TH1I("monitor","Bad Hit",(1<<15),-0.5, (1<<15)-0.5);
   hist2  = new TH1I("monitor2","Coarse Counter Values",(1<<15),-0.5, (1<<15)-0.5);
   hist3  = new TH1I("monitor3","Fine Counter",(1<<15),-0.5, (1<<15)-0.5);
   hist4  = new TH1I("monitor4","Energy Measurement",(1<<15),-0.5, (1<<15)-0.5);

   c1->cd();
   c1->Divide(2,2);
   c1->cd(1);
   hist->Draw();
   c1->cd(2);
   hist2->Draw();
   c1->cd(3);
   hist3->Draw();
   c1->cd(4);
   hist4->Draw();

   
   c2 = new TCanvas("c2","Mapping Monitor",1900,10,900,450);
   c2->Connect("Closed()","TApplication",&theApp,"Terminate(=0)");
   hist_map = new TH2I("monitor5","",16,0,16,8,0,8);
   c2->cd();
   c2->SetGrid();
   hist_map->Draw("COLZ");
   //hist_map->Draw("LEGO2Z"); // 3D view
   hist_map->GetXaxis()->SetNdivisions(16);
   hist_map->GetYaxis()->SetNdivisions(8);
   hist_map->SetStats(0);

   hist_map_channel_number = new TH2I("monitor6","mapping channel number",16,0,16,8,0,8);
   const char* fileName = "beam_profile_mapping.txt";
   GetMappingChannelNumber(fileName,hist_map_channel_number);
   hist_map_channel_number->SetMarkerSize(2.0);
   gStyle->SetHistMinimumZero(); 		// plot the empty cells, "0"
   hist_map_channel_number->Draw("TEXT SAME");

   TPaveText *pt1 = new TPaveText(3,8,5,9,"NB");
   pt1->AddText("Chip #1");
   pt1->Draw("SAME");
   TPaveText *pt2 = new TPaveText(11,8,13,9,"NB");
   pt2->AddText("Chip #0");
   pt2->Draw("SAME");

   TLine *l1 = new TLine(8,0,8,8);
   l1->SetLineWidth(3);
   l1->Draw("SAME");
   c2->Update();



   fout=new TFile("dump.root","RECREATE");

   tree  = new TTree("dump","dump");

   //DAQ Configuration
   daq_class = new TSTiC3DAQ();

   mon_gui=new MonitorGUI;
   mon_gui->SetSTiC3DAQ(daq_class);



   

   TSTiC3DAQ::monitor mon(TSTiC3DAQ::MODE_PERIOD);
   mon.hist = hist;
   mon.fChannel1=47;
   mon.fChannel2=0;
   //mon.fMode=TSTiC3DAQ::MODE_PERIOD;

   daq_class->AddMonitor(mon);

   mon.hist = hist2;
   mon.fChannel1=15;
   mon.fChannel2=0;
   mon.fMode=TSTiC3DAQ::MODE_TSINGLE_CM;

   daq_class->AddMonitor(mon);

   mon.hist = hist3;
   mon.fChannel1=15;
   mon.fChannel2=0;
   mon.fMode=TSTiC3DAQ::MODE_TSINGLE_FINE;

   daq_class->AddMonitor(mon);

   mon.hist = hist4;
   mon.fChannel1=15;
   mon.fChannel2=0;
   mon.fMode=TSTiC3DAQ::MODE_E;

   daq_class->AddMonitor(mon);


   mon.hist = hist_map;
   mon.fChannel1=15;
   mon.fChannel2=0;
   mon.fMode=TSTiC3DAQ::MODE_BEAM_PROFILE;

   daq_class->AddMonitor(mon);

   branch=daq_class->AddBranch(tree,"br");

   // start Thread
   // thread function (readout thread)
   th_daq = new TThread("daq", (TThread::VoidRtnFunc_t) &(TSTiC3DAQ::reader_thread), daq_class);
   // top thread (just ps and update)
   TThread *th_ps = new TThread("top",top);
   daq_class->Disable2kPackages();
   th_daq->Run();
   th_ps->Run();

   daq_class->StartReadout();
   theApp.Run(kTRUE);


   c1=NULL;	//not completely thread save, but it's a start.
   c2=NULL;
   th_ps->SetCancelAsynchronous();
   th_ps->Kill();

   daq_class->StopReadout();
   th_daq->Join();
   tree->Write();
   fout->Close();


   printf("The END...\n");

   return 0;
}

