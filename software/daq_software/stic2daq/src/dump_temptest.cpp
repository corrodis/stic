#include "TApplication.h"
#include "TObject.h"
#include "TH1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TThread.h"
#include "TTree.h"
#include "TFile.h"
#include <unistd.h>
#include <iostream>

#include "STiC3DAQ.h"
#include "STiC3Config.h"
#include "STiC2FPGAConfig.h"
#include "MQInterface.h"


// a global canvas for the drawing of histograms
//TCanvas *c1;

TSTiC3DAQ* daq_class;
TThread *th_daq;
// a global histogram object, will be accessed both threads
//dump tree
//TH1D 	*hist;
//TTree	*tree;
//TBranch	*branch;
//TFile 	*fout;
ofstream *fout;

//SIGINT handler
#include <signal.h>
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	daq_class->StopReadout();
	TThread::Ps();
   	th_daq->Join();
//	tree->Write();
//	fout->Close();
    fout->close();
    TThread::Ps();
	exit(0);
}




int main(int argc, char **argv)
{

	signal(SIGINT,&handler_sigint);

	float sleeptime=1800;

	if(argc<2)
	{
		printf("Usage: dumpto [filename] <record time in s>\n");
		return -1;
	}
	if(argc>2) sleeptime=atoi(argv[2]);

	// Set up configuration interface to reset chip and channel data
        TMQInterface iface("DAQ");
        iface.SetTimeout(0,5000000,0);
        TSTiC3Config *config = new TSTiC3Config(&iface);
        config->ChipReset();
        config->ChannelReset();
        printf("RESET Chip and Channel\n");
        delete config;



	printf("Writing to %s for %f seconds\n",argv[1],sleeptime);



//	fout=new TFile(argv[1],"RECREATE");
//	tree  = (TTree*)fout->Get("dump");
    //std::cout << fout << std::endl;
    fout = new ofstream(argv[1], std::ios::out | std::ios::binary);
    //std::cout << fout << std::endl;


	daq_class = new TSTiC3DAQ();

 
//	if(tree == NULL)
//	{
//		tree= new TTree("dump","dump");
//		daq_class->AddBranch(tree,"br");
//	}
//	else
//	{
//		daq_class->SetBranch(tree,"br");
//	}

	// start Thread
	// thread function (readout thread)

    //Set Readout to bin mode
    daq_class->SetRedoutBinFile(fout);
    //daq_class->SetRedoutBin(true);

    th_daq = new TThread("daq", (TThread::VoidRtnFunc_t) &(TSTiC3DAQ::reader_thread), daq_class);
    //th_daq->Run();

    //daq_class->GetTemperature();

    //daq_class->Enable2kPackages();
    //daq_class->StartDebugEvents(500);
    //daq_class->StartReadout();

    printf("Sleeping\n");
	TThread::Sleep(sleeptime);
	printf("Finished\n");



    //daq_class->PauseReadout();


    //daq_class->StopReadout();


    //th_daq->Join();
    //printf("Thread joined\n");
	// TODO: Fix that, why does it hang if Join or Exit?
	//th_daq->Delete();
	//printf("Exit Thread");	

//	tree->Write(0,TObject::kOverwrite);
//	fout->Close();
        fout->close();
	printf("File closed\n");


        printf("Processed USB Bulk Packages: %d\n", daq_class->GetUsbPackageNo());
        printf("Double Recieved USB Bulk Packages: %d\n", daq_class->GetUsbDoublePackageNo());
        printf("Bandwith loos due to double packages: %.3f\n", (float)daq_class->GetUsbDoublePackageNo()/(daq_class->GetUsbDoublePackageNo() + daq_class->GetUsbPackageNo()));


    printf("Processed USB Bulk Packages: %d\n", daq_class->GetUsbPackageNo());
    printf("Lost USB Bulk Packages: %d (%f)\n", daq_class->GetUsbLostPackageNo(), 1.0*daq_class->GetUsbLostPackageNo()/daq_class->GetUsbPackageNo());
    printf("Double USB Bulk Packages: %d (%f)\n", daq_class->GetUsbDoublePackageNo(), 1.0*daq_class->GetUsbDoublePackageNo()/daq_class->GetUsbPackageNo());
    printf("The END...\n");

	return 0;
}

