/*
	DAQ class to read the STiC2 data sent over UDP by the FPGA
	Provides Access to basic commands for the FPGA,
	and saves the data in a ROOT Branch. For Monitoring, a ROOT histogram can be filled.

	Author: kbriggl, tharion

*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <arpa/inet.h>

//SLOW CONTROL
#include <mqueue.h>
#include <string.h>
#include "mq_message_dict.h"

//USB Communication Interface
#include "TUSB_iface.h"



#include "TMutex.h"


#include "STiC2DAQ.h"

//LENGTH OF AN EVENT IN BYTES
#define EVENT_LEN 12

#define BUFSIZE 1024
#define DAQ_PORT 10

//#define DEBUG

#define VCO_SHIFT 0


TSTiC2DAQ::monitor::monitor(mode m){
	hist=NULL;
	cut_method=NULL;
	fMode=m;
	if(m==MODE_PERIOD)
		previous_events=new stic_data_t;
	if(m==MODE_T)
		previous_events=new stic_data_t[2];
}
TSTiC2DAQ::monitor::~monitor(){
	if(!previous_events)
		delete[] previous_events;
}


TSTiC2DAQ::TSTiC2DAQ(){
	fRunning=true;
	scqueue_cmd=-1;
	scqueue_reply=-1;


	current_event= NULL; //new stic_data_t;
	/*net in buffer*/
	chunk_buffer=(char*)malloc(BUFSIZE);
	if (chunk_buffer==NULL){
		perror("TSTiC2DAQ: malloc");
		return;
	}

	//USB interconnection
	//No changes are required, the default values read from the Visions SP6 board
	usb_iface = new TUSB_iface;
	if (!usb_iface->check_status())
	{
		perror("TUSB_iface: Device not found!\n");
	}

	fmonitors = new std::vector<monitor>;


	//standard message queues
	SCMessageQueue("DAQ");
}

TSTiC2DAQ::~TSTiC2DAQ(){

	if (scqueue_cmd){
		mq_close(scqueue_cmd);
	}
	if (scqueue_reply){
		mq_close(scqueue_reply);
	}
}

void	TSTiC2DAQ::SetBranch(TTree* tree, const char* branchname){

	rw_mutex.Lock();
	fTree=tree;
	fBranch=fTree->GetBranch(branchname);
	fBranch->SetAddress(&current_event);
	rw_mutex.Lock();

}



TBranch* TSTiC2DAQ::AddBranch(TTree* tree, const char* branchname){
	rw_mutex.Lock();
	fTree=tree;
	fBranch=fTree->Branch(branchname,"stic_data_t", &current_event);
	rw_mutex.UnLock();
	return fBranch;
}



TSTiC2DAQ::monitor_ref TSTiC2DAQ::AddMonitor(monitor mon){
	rw_mutex.Lock();
	fmonitors->push_back(mon);
	rw_mutex.UnLock();
	return fmonitors->end();
}

void TSTiC2DAQ::RemoveMonitor(monitor_ref m){
	rw_mutex.Lock();
	fmonitors->erase(m);
	rw_mutex.UnLock();
}

int TSTiC2DAQ::SCMessageQueue(const char* qname){
	if (scqueue_cmd){
//		mq_unlink(scqueue_cmd);
		mq_close(scqueue_cmd);
	}
	if (scqueue_reply){
//		mq_unlink(scqueue_reply);
		mq_close(scqueue_reply);
	}

	struct mq_attr attr;
	attr.mq_maxmsg=10;
	attr.mq_msgsize=sizeof(struct msg_struct);
	attr.mq_flags=0;

	char name[30];
	sprintf(name,"/%s_cmd.0",qname);
	scqueue_cmd=	mq_open(name,O_RDONLY|O_CREAT|O_NONBLOCK, 0664,&attr);
	if(scqueue_cmd < 0){
		printf("DAQ Slow Control connection: Error opening cmd message queue (%d):",errno);
		perror("");
	}
	else
		printf("DEBUG: Opened cmd message queue %s | ID: %d \n",name, scqueue_cmd);

	sprintf(name,"/%s_reply.1",qname);
	scqueue_reply=	mq_open(name,O_WRONLY|O_CREAT, 0664,&attr);
	if(scqueue_reply < 0){
		printf("DAQ Slow Control connection: Error opening reply message queue (%d):",errno);
		perror("");
	}
	else
		printf("DEBUG: Opened reply message queue %s | ID: %d \n",name, scqueue_reply);
}

int TSTiC2DAQ::PauseReadout(){
	daq_data_struct msg;
	msg.cmd='E';
	msg.port=DAQ_PORT;
	msg.handleID = 0; //A handle ID is required!
	msg.len=0;
	if ((numbytes = usb_iface->write_usb((unsigned char*) &msg, sizeof(daq_data_header)))==-1){
		perror("TSTiC2DAQ: sendto");
		return -1;
	}
	return 0;

}
int TSTiC2DAQ::StartReadout(){
	daq_data_struct msg;
	msg.cmd='e';
	msg.handleID = 0; //A handle ID is required!
	msg.port=DAQ_PORT;
	msg.len=0;
	if ((numbytes = usb_iface->write_usb((unsigned char*) &msg, sizeof(daq_data_header)))==-1){
		perror("TSTiC2DAQ: sendto");
		return -1;
	}
	return 0;
}

int TSTiC2DAQ::SetSync(unsigned char sync){
	daq_data_struct msg;
	msg.cmd='s';
	msg.port=DAQ_PORT;
	msg.len=1;
	msg.payload[0]=sync;

	for(int i=0; i<4; i++)
	{
		msg.handleID = i;
		if ((numbytes = usb_iface->write_usb((unsigned char*) &msg, sizeof(daq_data_header) +1 ))==-1){
			perror("TSTiC2DAQ: sendto");
			return -1;
		}
	}
	return 0;
}


void TSTiC2DAQ::BufferToEvent(unsigned int* const buf_ptr,stic_data_t* const event){
	/*
	 *	Frame Composition:
	 *
	 *	unsigned int* bin_event has to be provided as: bin_event[0] = [0:-7], bin_event[1] = [7:0] .....
	 *
	 *	bin_event[0]=[0:-7]	: Frame Number 		(Leading byte)
	 *	bin_event[1]=[7:0]	: Energy CC[7:0]
	 *	bin_event[2]=[15:8]	: Energy CC[15:8]
	 *	bin_event[3]=[23:16] : Energy CC[23:16]
	 *	bin_event[4]=[31:24] : {T_fine[0], E_badhit, Energy CC[29:24]}
	 *	bin_event[5]=[39:32] : {Time CC [3:0], T_fine[4:1]}
	 *	bin_event[6]=[47:40] : Time CC [11:4]
	 *	bin_event[7]=[55:48] : Time CC [19:12]
	 *	bin_event[8]=[63:56] : Time CC [27:20]
	 *	bin_event[9]=[71:64] : {0, channel [3:0] Time_badhit, Time CC[29:28]}
	 *
	 *	Time and Energy CC contain the Master and Slave registers
	 *	[14:0] = Master, [29:15] = Slave
	 */


	// Bit shift ruler... : 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
	//In God we trust..... no comments for the bit shifts (sorry). Most important: take care without brakets
	//The coarse counter values are decoded on the fly....
	event->E_CCM = decoder[ (unsigned int)	(  0x00007fff & buf_ptr[0]) 	];
	event->E_CCS = decoder[ (unsigned int)	(( 0x3fff8000 & buf_ptr[0]) >> 15)];
	event->E_badhit =  			( 0x40000000 & buf_ptr[0]) >> 30;

	event->T_fine =		(unsigned short)((( 0x0000000f & buf_ptr[1]) << 1) + ((0x80000000 & buf_ptr[0]) >> 31));

	event->T_CCM = decoder[	(unsigned int) 	(( 0x0007fff0 & buf_ptr[1]) >> 4 ) ];
	event->T_CCS = decoder[	(unsigned int) 	(( 0x00000003 & buf_ptr[2])<< 13 | ( 0xfff80000 & buf_ptr[1]) >> 19) ];


	event->T_badhit=  			(( 0x00000004 & buf_ptr[2]) != 0 )? true:false;

	event->channel = 	(unsigned short) ((0x000000f8 & buf_ptr[2]) >> 3) + event->handleID*16;
	event->frame_number =	(unsigned short) ((0x0000ff00 & buf_ptr[2]) >> 8);

//	printf("E_CCM: %u\n",current_event->E_CCM);
//	printf("E_CCS: %u\n",current_event->E_CCS);
//	printf("E_BADHIT: %u\n",current_event->E_badhit);
//	printf("T_FINE: %u\n",current_event->T_fine);
//	printf("T_CCM: %u\n",current_event->T_CCM);
//	printf("T_CCS: %u\n",current_event->T_CCS);
//	printf("T_BADHIT: %u\n",current_event->T_badhit);
//	printf("CHANNEL: %u\n",current_event->channel);
//	printf("FRAME: %u\n\n",current_event->frame_number);

}

short TSTiC2DAQ::ProcessRawEvent(stic_data_t* const current_event)
{


	current_event->time = 0;

	//RECONSTRUCT THE FINECOUNTER MSB AND DISCARD BAD VALUES

	if (current_event->T_CCM == 1<<32 - 1 || current_event->T_CCS == 1<<32 - 1)
	{
		//Bad decoding: get the next event
		return -1;
	}
	else if(current_event->T_CCM-current_event->T_CCS >1)
	{
		//ONE OF THE COARSE COUNTERS WAS JUST FLIPPING, WHO KNOWS WHAT WE SHOULD RECONSTRUCT???
		return -1;
	}
	else if(current_event->T_fine>15)
	{
		printf("Finecounter magically working again!!!Channel:%d Value: %d\n",current_event->channel,current_event->T_fine);
		return -1;
	}
	else if(current_event->T_fine < 2 || current_event->T_fine > 13)
	{
		//Discard finecounter values where the CC was just flipping
		//printf("Continue because fine value is close to CC change\n");
		return -1;
	}
	else if(current_event->T_CCM - current_event->T_CCS == 1)
	{
		current_event->T_fine = (current_event->T_fine + VCO_SHIFT) % 32;
		current_event->time =    current_event->T_CCM*0x20 + current_event->T_fine;
	}
	else if (current_event->T_CCM - current_event->T_CCS == 0)
	{
		current_event->T_fine = (current_event->T_fine + 16 + VCO_SHIFT) % 32;
		current_event->time=    current_event->T_CCS*0x20 + current_event->T_fine;
	}
	else
	{
		//One counter was just flipping: get the next event
		return -1;
	}


	if ( (current_event->E_CCM - current_event->E_CCS) <= 1 && (current_event->T_CCM - current_event->T_CCS) <= 1 ) current_event->energy=current_event->E_CCS - current_event->T_CCS;
	else return -1;


	return 0;


}

void TSTiC2DAQ::ProcessMonitors( stic_data_t* const current_event)
{


			for(int i_monitor=0;i_monitor<fmonitors->size();i_monitor++)
			{
				if(fmonitors->at(i_monitor).cut_method && !fmonitors->at(i_monitor).cut_method(current_event))
					continue;
				if(fmonitors->at(i_monitor).hist == NULL)
					continue;

				switch (fmonitors->at(i_monitor).fMode){
					case MODE_E :{
						if (current_event->channel == fmonitors->at(i_monitor).fChannel1)
							fmonitors->at(i_monitor).hist->Fill( current_event->energy);
						break;
					}
					case MODE_TSINGLE :{
						if (current_event->channel == fmonitors->at(i_monitor).fChannel1)
							fmonitors->at(i_monitor).hist->Fill( current_event->time );
						break;
					}
					case MODE_TSINGLE_FINE :{
						if (current_event->channel == fmonitors->at(i_monitor).fChannel1)
							fmonitors->at(i_monitor).hist->Fill( current_event->T_fine );
						break;
					}
					case MODE_TBADHIT :{
						if (current_event->channel == fmonitors->at(i_monitor).fChannel1)
							fmonitors->at(i_monitor).hist->Fill( current_event->T_badhit );
						break;
					}
					case MODE_TSINGLE_CM :{
						if (current_event->channel == fmonitors->at(i_monitor).fChannel1)
							fmonitors->at(i_monitor).hist->Fill( current_event->T_CCM );
						break;
					}
					case MODE_CHANNEL :{
						fmonitors->at(i_monitor).hist->Fill( current_event->channel );
						break;
					}
					case MODE_PERIOD :{
						if (current_event->channel == fmonitors->at(i_monitor).fChannel1 && fmonitors->at(i_monitor).previous_events->time != 0 && current_event->time !=0)
						{
							fmonitors->at(i_monitor).hist->Fill( current_event->time - fmonitors->at(i_monitor).previous_events->time);
						}
						if(current_event->channel == fmonitors->at(i_monitor).fChannel1) memcpy(fmonitors->at(i_monitor).previous_events,current_event,sizeof(stic_data_t));
						break;
					}
					case MODE_T :{
						if(current_event->channel == fmonitors->at(i_monitor).fChannel1){
							if( abs(fmonitors->at(i_monitor).previous_events[1].frame_number - current_event->frame_number) < 2 \
								&& current_event->time!=0 && fmonitors->at(i_monitor).previous_events[1].time!=0 \
								&& current_event->energy > fmonitors->at(i_monitor).ch1_ECut_low \
								&& current_event->energy < fmonitors->at(i_monitor).ch1_ECut_high \
								&& fmonitors->at(i_monitor).previous_events[1].energy > fmonitors->at(i_monitor).ch2_ECut_low \
								&& fmonitors->at(i_monitor).previous_events[1].energy < fmonitors->at(i_monitor).ch2_ECut_high \
								)
							{
								//the saved hit in the other channel was not too long ago.
								fmonitors->at(i_monitor).hist->Fill( (long int)fmonitors->at(i_monitor).previous_events[1].time - (long int)current_event->time );
							}
							//copy the new event
							memcpy(&fmonitors->at(i_monitor).previous_events[0],&current_event,sizeof(stic_data_t));

						}else if(current_event->channel == fmonitors->at(i_monitor).fChannel2 ){
							if( abs(fmonitors->at(i_monitor).previous_events[0].frame_number - current_event->frame_number) < 2 \
								&& current_event->time!=0 && fmonitors->at(i_monitor).previous_events[0].time!=0 \
								&& current_event->energy > fmonitors->at(i_monitor).ch2_ECut_low \
								&& current_event->energy < fmonitors->at(i_monitor).ch2_ECut_high \
								&& fmonitors->at(i_monitor).previous_events[0].energy > fmonitors->at(i_monitor).ch1_ECut_low \
								&& fmonitors->at(i_monitor).previous_events[0].energy < fmonitors->at(i_monitor).ch1_ECut_high \
								)
							{
								//the saved hit in the other channel was not too long ago.
								fmonitors->at(i_monitor).hist->Fill((long int)current_event->time - (long int)fmonitors->at(i_monitor).previous_events[0].time);
							}
							//copy the new event
							memcpy(&fmonitors->at(i_monitor).previous_events[1],this->current_event,sizeof(stic_data_t));
						}
						break;
					}
				}//switch
			} //list iterator
}


void TSTiC2DAQ::reader_method(){

	current_event= new stic_data_t;

	unsigned int* buf_ptr;
	unsigned int* buf_end;
	msg_struct sc_message;

	usb_struct usb_message;

	fd_set rfds;
	struct timeval tv;
	int retval;
	int maxfd;

	int ret;

      while(fRunning){


	retval=mq_receive (scqueue_cmd,	(char*)&sc_message,sizeof(msg_struct), NULL);
	if (retval < 0)
	{
		if (errno != EAGAIN) perror("TSTiC2DAQ: mq_receive");
	}
	else
	{
		printf("Local data\n");

		printf("cmd=%.1x \n",sc_message.cmd);
		printf("ID=%.1x \n",sc_message.handleID);
		printf("len=%d \n",sc_message.len);
		printf("port=%d \n",sc_message.port);

		usb_message.cmd = sc_message.cmd;
		usb_message.handleID = sc_message.handleID;
		usb_message.len = sc_message.len;
		usb_message.port = sc_message.port;
		memcpy(usb_message.payload,sc_message.msg,sc_message.len);

#ifdef DEBUG
		for(int i=511;i>=0;i--)
		{
			printf(" %.1x ",((char*)&usb_message)[i]&0xff);
			if (i%4 == 0) printf("\n");
		}
		printf("\n");
#endif

		printf("TSTiC2DAQ: Sending packet out\n");
		printf("Debug: Sizeof package: %lu bytes\n",sizeof(msg_header) + sc_message.len);
		numbytes = usb_iface->write_usb((unsigned char*)&usb_message,sizeof(msg_header) + sc_message.len);

		if (numbytes==-1)
		{
			perror("TSTiC2DAQ: sendto");
			continue;
		}
	}//MQ READ

	numbytes=usb_iface->read_usb((unsigned char*)chunk_buffer,BUFSIZE);
//	printf("Received %d bytes\n",numbytes);
	if(numbytes>0)
	{

#ifdef DEBUG
		//remote data available
		printf("Remote data.\n");

		printf(" HEADER HEX: %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x\n", chunk_buffer[0], chunk_buffer[1], chunk_buffer[2], chunk_buffer[3]
							, chunk_buffer[4], chunk_buffer[5], chunk_buffer[6], chunk_buffer[7]
							, chunk_buffer[8], chunk_buffer[9], chunk_buffer[10], chunk_buffer[11]
				);


		printf("STiC2DAQ: recv: packet is %d bytes long\n", numbytes);
		//Demux packet
		//TODO: Change this to use the ports defined in the USB packet

		printf("Remote CMD: %c\n",((usb_header*)chunk_buffer)->cmd);
		printf("Remote PORT: %d\n",((usb_header*)chunk_buffer)->port);
		printf("Remote LEN: %d\n",((usb_header*)chunk_buffer)->len);
		printf("HANDLE ID: %d\n",((usb_header*)chunk_buffer)->handleID);
#endif


		switch(((usb_header*)chunk_buffer)->cmd){
			case 'c':{
				printf("DAQ: remote SC reply\n");
				//got a slow control reply, containing a bitpattern
				//this has to be passed to the message queue

				//check for lenght consistency
//				printf("nb=%d len=%lu\n",numbytes,sizeof(daq_data_header) + ((daq_data_struct*)chunk_buffer)->len);
				if (numbytes < sizeof(daq_data_header) + ((usb_header*)chunk_buffer)->len){
					printf("DAQ: Omitted configuration reply due to length pad inconsistency\n");
					break;
				}



				sc_message.cmd = ((usb_struct*)chunk_buffer)->cmd;
				sc_message.handleID = ((usb_struct*)chunk_buffer)->handleID;
				sc_message.len = ((usb_struct*)chunk_buffer)->len;
				sc_message.port = ((usb_struct*)chunk_buffer)->port;
				memcpy(sc_message.msg,((usb_struct*)chunk_buffer)->payload,508);

				//send package
				ret=mq_send(scqueue_reply,
					(char*)&sc_message,
					sizeof(usb_header) + ((usb_header*)chunk_buffer)->len,
					0
				);

				if(ret != 0){
					printf("ret= %d\n",ret);
					perror("mq_send");
				}

				break;

			case 'D':
				printf("DAQ: remote DEBUG reply\n");
				//got a debug control reply, containing a bitpattern
				//this has to be passed to the message queue

				//check for lenght consistency
//				printf("nb=%d len=%lu\n",numbytes,sizeof(daq_data_header) + ((daq_data_struct*)chunk_buffer)->len);
				if (numbytes < sizeof(daq_data_header) + ((usb_header*)chunk_buffer)->len){
					printf("DAQ: Omitted configuration reply due to length pad inconsistency\n");
					break;
				}



				sc_message.cmd = ((usb_struct*)chunk_buffer)->cmd;
				sc_message.handleID = ((usb_struct*)chunk_buffer)->handleID;
				sc_message.len = ((usb_struct*)chunk_buffer)->len;
				sc_message.port = ((usb_struct*)chunk_buffer)->port;
				memcpy(sc_message.msg,((usb_struct*)chunk_buffer)->payload,508);

				//send package
				ret=mq_send(scqueue_reply,
					(char*)&sc_message,
					sizeof(usb_header) + ((usb_header*)chunk_buffer)->len,
					0
				);

				if(ret != 0){
					printf("ret= %d\n",ret);
					perror("mq_send");
				}

				break;

			}
			case 'd':{
				//daq data packet
				//size check
//				printf("nb=%d len=%lu\n",numbytes,sizeof(usb_header) + ((usb_struct*)chunk_buffer)->len);
				if ((numbytes < sizeof(usb_header)) || (numbytes < sizeof(usb_header) + ((usb_struct*)chunk_buffer)->len)){
					printf("DAQ: Omitted data packet due to length inconsistency\n");
					break;
				}

				current_event->handleID=((usb_struct*)chunk_buffer)->handleID;
//				current_event->packetID=((usb_struct*)chunk_buffer)->packetID;
				current_event->packetID=1;
				buf_ptr=(unsigned int*)((usb_struct*)chunk_buffer)->payload;
				buf_end=(unsigned int*)&((usb_struct*)chunk_buffer)->payload[((usb_header*)chunk_buffer)->len-4];

				while (buf_ptr < buf_end){
					BufferToEvent(buf_ptr, current_event);
//					ProcessRawEvent(current_event);
					if ( ProcessRawEvent(current_event) == 0)
					{

						/************************************/
						/*aquire lock to access tree and histograms*/
						rw_mutex.Lock();

						/************************************/
						/*event is done, write to tree*/
						if(fBranch != NULL){
							//fBranch->Fill();
							fTree->Fill();
						}

					/************************************/
					/*iterate over monitors*/
						ProcessMonitors(current_event);
					}

					/*release lock to access tree and histograms*/
					/************************************/
					rw_mutex.UnLock();


					//Next Event	--FOR THE USB DAQ THE EVENT LENGTH IS 12 BYTES
					buf_ptr=(&buf_ptr[3]);

				}//payload loop
			}//daq packet case
		}//remote read demultiplexing switch

	}//if (FD_ISSET(sockfd,&rfds))


     }//while running
}//reader_loop

