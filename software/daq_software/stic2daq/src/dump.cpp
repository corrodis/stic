#include "TApplication.h"
#include "TObject.h"
#include "TH1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TThread.h"
#include "TTree.h"
#include "TFile.h"
#include "TStyle.h"
#include "STiC3DAQ.h"
#include "MonitorGUI.h"

#include <iostream>


/*
** STIC2DAQ
**
** The Code will intanciate a DAQ class, set appropriate parameters
** and start a separate Thread where the data is read out.
** In the Main thread, a Histogram is Drawn and updated
*/

// a global canvas for the drawing of histograms
TCanvas *c1;

TSTiC3DAQ* daq_class;
//Monitor GUI interface
MonitorGUI* mon_gui;

TThread *th_daq;
// a global histogram object, will be accessed both threads
//dump tree
TH1I 	*hist;
TH1I	*hist2;
TH1I	*hist3;
TH1I	*hist4;
TTree	*tree;
TBranch	*branch;
TFile 	*fout;

//SIGINT handler
#include <signal.h>
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	//daq_class->StopReadout();	//TODO: will not cancel the thread if no data is incoming
	TThread::Ps();
   	th_daq->Join();
	tree->Write();
	fout->Close();
	TThread::Ps();
	exit(0);
}



void *top(void *)
{
   TThread::Printf("Start of top");

   for (Int_t i = 0; ;i++) {
      if(c1!=NULL){
//      	c1->cd(1);
        c1->GetPad(1)->Update();
        c1->GetPad(1)->Modified();
//	c1->cd(2);
        c1->GetPad(2)->Update();
        c1->GetPad(2)->Modified();
        c1->GetPad(3)->Update();
        c1->GetPad(3)->Modified();
        c1->GetPad(4)->Update();
        c1->GetPad(4)->Modified();
      }
      if(i % 30 == 0){
      		TThread::Ps();
		printf("Recorded %lld events\n",branch->GetEntries());
//		tree->Scan("","","",100);
//		hist->Clear();
//  		hist->Reset();
//	      	hist2->Clear();
//  		hist2->Reset();
//      		hist3->Clear();
//  		hist3->Reset();
//	      	hist4->Clear();
//		hist4->Reset();
      }
      TThread::Sleep(0.25);
   }
   TThread::Printf("End of top");
   return 0;
}

int main(int argc, char **argv)
{

   signal(SIGINT,&handler_sigint);
   gStyle->SetOptStat(111111);
   TApplication theApp("DAQ", &argc, argv);
   // exit on close window


   // a global canvas for the drawing of histograms
   c1 = new TCanvas("c1","Readout Monitor",800,400);
   c1->Connect("Closed()","TApplication",&theApp,"Terminate(=0)");
   
   // a global histogram object, which will be accessed both by mhs and mhs1
   hist  = new TH1I("monitor","Bad Hit",(1<<15),-0.5, (1<<15)-0.5);
   hist2  = new TH1I("monitor2","Coarse Counter Values",(1<<15),-0.5, (1<<15)-0.5);
   hist3  = new TH1I("monitor3","Fine Counter",(1<<15),-0.5, (1<<15)-0.5);
   hist4  = new TH1I("monitor4","Energy Measurement",(1<<15),-0.5, (1<<15)-0.5);




   c1->cd();
   c1->Divide(2,2);
   c1->cd(1);
   hist->Draw();
   c1->cd(2);
   hist2->Draw();
   c1->cd(3);
   hist3->Draw();
   c1->cd(4);
   hist4->Draw();
   
   fout=new TFile("dump.root","RECREATE");

   tree  = new TTree("dump","dump");

   //DAQ Configuration
   daq_class = new TSTiC3DAQ();

   mon_gui=new MonitorGUI;
   mon_gui->SetSTiC3DAQ(daq_class);



   

   TSTiC3DAQ::monitor mon(TSTiC3DAQ::MODE_PERIOD);
   mon.hist = hist;
   mon.fChannel1=47;
   mon.fChannel2=0;
   //mon.fMode=TSTiC3DAQ::MODE_PERIOD;

   daq_class->AddMonitor(mon);

   mon.hist = hist2;
   mon.fChannel1=15;
   mon.fChannel2=0;
   mon.fMode=TSTiC3DAQ::MODE_TSINGLE_CM;

   daq_class->AddMonitor(mon);

   mon.hist = hist3;
   mon.fChannel1=15;
   mon.fChannel2=0;
   mon.fMode=TSTiC3DAQ::MODE_TSINGLE_FINE;

   daq_class->AddMonitor(mon);

   mon.hist = hist4;
   mon.fChannel1=15;
   mon.fChannel2=0;
   mon.fMode=TSTiC3DAQ::MODE_E;

   daq_class->AddMonitor(mon);



   branch=daq_class->AddBranch(tree,"br");

   // start Thread
   // thread function (readout thread)
   th_daq = new TThread("daq", (TThread::VoidRtnFunc_t) &(TSTiC3DAQ::reader_thread), daq_class);
   // top thread (just ps and update)
   TThread *th_ps = new TThread("top",top);
   daq_class->Disable2kPackages();
   th_daq->Run();
   th_ps->Run();

   daq_class->StartReadout();
   theApp.Run(kTRUE);


   c1=NULL;	//not completely thread save, but it's a start.
   th_ps->SetCancelAsynchronous();
   th_ps->Kill();

   daq_class->StopReadout();
   th_daq->Join();
   tree->Write();
   fout->Close();


   printf("The END...\n");

   return 0;
}

