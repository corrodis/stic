#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TF1.h"
#include "TFile.h"
#include "stdio.h"
#include "TSystem.h"
#include "EventDict.h"
#include "EventType.h"
#include "TFile.h"
#include "TKDE.h"
#include "TMath.h"
#include "Math/RootFinder.h"
#include "Math/WrappedTF1.h"



#define CH_10_E_LOW 190 
#define CH_10_E_HIGH 208 
#define CH_15_E_LOW 175 
#define CH_15_E_HIGH 190 

//#define CH_10_E_LOW 130 
//#define CH_10_E_HIGH 147 
//#define CH_15_E_LOW 120 
//#define CH_15_E_HIGH 132 



TF1* func;

Double_t sub_func(Double_t *x, Double_t *par)
{

	return (func->Eval(x[0]) - par[0]/2.0);
}

double get_FWHM()
{
	double function_max;
	function_max=func->GetMaximum();

	double values[2];

	TF1* fwhm_func= new TF1("fwhmfunction",sub_func,-70,70,1);
	fwhm_func->SetParameter(0,function_max);

	printf("Sanity check: value at x_min: %f function maximum/2.0: %f\n",fwhm_func->Eval(-7.0), function_max/2.0);
	printf("Value of unmodified function: %f \n",func->Eval(-7.0));

	ROOT::Math::WrappedTF1 wf1(*fwhm_func);
	ROOT::Math::RootFinder brf;

	brf.SetFunction(wf1,-10.0,0.0);
	brf.Solve();
	
	printf("Found Root: %f, status: %d, Iterations: \n",brf.Root(), (int)brf.Status());
	values[0]=brf.Root();
	brf.SetFunction(wf1,0,10.0);
	brf.Solve();
	values[1]=brf.Root();

	printf("Found Root: %f, status: %d, Iterations: \n",brf.Root(),brf.Status());
	printf("FWHM: %f TDC Bins = %f ps FWHM\n",values[1]-values[0],(values[1]-values[0])*50.2 );
	return 0;

}



int main(int argc, char *argv[]){
	if (argc<3){
		printf("use: %s file treename\n",argv[0]);
		return -1;
	}




	TFile f(argv[1]);
	TTree *tree=(TTree*)f.Get(argv[2]);

	if (tree==NULL)
		return -1;


	TObjArray *branches=tree->GetListOfBranches();

	for(int i=0; i<branches->GetEntries(); i++){
		TBranch * thisBranch=(TBranch*)(*branches)[i];
		printf("branch \"%s\" has %lld entries\n",thisBranch->GetName(),thisBranch->GetEntries());
	}


	TCanvas *c1 = new TCanvas("c1","Canvas",800,400);

	TH1F *tot_hist_ch1 = new TH1F("tot1","ToT Spectrum 1",15000,0,15000);
	TH1F *tot_hist_ch2 = new TH1F("tot2","ToT Spectrum 2",15000,0,15000);
	TH1F *timediff_hist = new TH1F("timediff","Timedifference",1e10,0,1e10);



	printf("%lld\n",tree->GetEntries());
	stic_data_t* event=NULL;
	stic_data_t last_event_ch10;
	stic_data_t last_event_ch15;

	tree->SetBranchAddress("br",&event);


	double* data = new double[tree->GetEntries()]; //Array containing the coincidences
	unsigned int n;				//number of found coincidences
	n=0;


	//Coincidence Search:
	//	printf("%d has CCM=%d, CCS=%d, ch=%u\n",i,event->T_CCM,event->T_CCS,event->channel);
	for (int i=0; i<tree->GetEntries(); i++){

		tree->GetEntry(i);
		//Remove entries with invalid CCS or CCM measurements (time=-1);
		if(event->time == ((0x1 << 32) - 1) )
		{
			//printf("Invalid event found \n");
	//		if (event->channel == 10) last_event_ch10.time = ((0x1<<32 - 1)); //Notice that the last event was invalid
	//		if (event->channel == 15) last_event_ch15.time = ((0x1<<32 - 1)); //Notice that the last event was invalid
			continue;
			printf("This should never occur\n");
		}

		if(event->T_fine <= 3 || (event->T_fine >= 13 && event->T_fine <= 18) || event->T_fine > 28)
		{
			continue;
		}
		
		//Fill the energy spect of channel 10 and search for coincidence with ch15
		if(event->channel==10)
		{
			if(event->energy != ((0x1 << 32)-1) ) tot_hist_ch1->Fill(event->energy);
			if (event->energy < CH_10_E_HIGH && event->energy > CH_10_E_LOW && event->time != 0)
			{
				
				last_event_ch10.time = event->time;	//SAVE THE EVENT AS THE LAST 511 keV event 
				last_event_ch10.energy = event->energy;	//SAVE THE EVENT AS THE LAST 511 keV event 
				last_event_ch10.T_fine = event->T_fine;
				last_event_ch10.T_CCM = event->T_CCM;
				last_event_ch10.T_CCS = event->T_CCS;

	//				printf("Coincidence found ch10 %lld %lld diff: %d\n", event->time , last_event_ch15.time, (int)(event->time) - (int)(last_event_ch15.time));
//				data[n]=(int)(event->time) - (int)(last_event_ch10.time);
//				n++;
	//			timediff_hist->Fill((int)(event->time) - (int)(last_event_ch10.time));
			}
		}

		if(event->channel==15)
		{
			if(event->energy != ((0x1 << 32)-1) ) tot_hist_ch2->Fill(event->energy);
			if (event->energy < CH_15_E_HIGH && event->energy > CH_15_E_LOW && event->time != 0)
			{
				
				last_event_ch15.time = event->time;	//SAVE THE EVENT AS THE LAST 511 keV event 
				last_event_ch15.T_fine = event->T_fine;
				last_event_ch15.T_CCM = event->T_CCM;
				last_event_ch15.T_CCS = event->T_CCS;
				last_event_ch15.energy = event->energy;	//SAVE THE EVENT AS THE LAST 511 keV event 


					data[n]=(int)(last_event_ch15.time) - (int)(event->time);
					n++;
					timediff_hist->Fill( (int)(last_event_ch15.time) - (int)(event->time) );
			}
		}


	}
	printf("filled the timediff histogram\n");


//	TKDE* kde = new TKDE(n,&data[0],-300,300,"",1.0);
//	printf("KDE Sigma: %f\n",kde->GetSigma());
//
//	//TF1 *func;
//	func = kde->GetFunction(600,-300,300);
//
//
//	double HM = func->GetMaximum()/2.0;
//
//	//Now try to find the FWHM
//
//
//
//
//	get_FWHM();

	//c1->Draw();
	c1->cd();
	timediff_hist->Draw("ALP");
//	kde->Draw("SAME");
	TFile* fout=new TFile("coincidence_result.root","RECREATE");
	timediff_hist->Write();
	tot_hist_ch1->Write();
	tot_hist_ch2->Write();
//	func->Write("func");

	fout->Close();

	char c;
	scanf("%c",&c);

	return 0;
}
