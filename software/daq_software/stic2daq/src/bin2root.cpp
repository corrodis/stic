#include "TApplication.h"
#include "TObject.h"
#include "TH1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TThread.h"
#include "TTree.h"
#include "TFile.h"
//#include <unistd.h>
#include <iostream>
#include <string>

#include "STiC3DAQ.h"

#define USBEVENTSIZE 8

//#define DEBUG_EVENT


//TCanvas *c1;

TSTiC3DAQ* daq_class;
//TThread *th_daq;
// a global histogram object, will be accessed both threads
//dump tree
//TH1D 	*hist;
TTree	*tree;
//TBranch	*branch;
TFile 	*fout;

ifstream *fin;

//stic3_data_t* current_event;


int main(int argc, char **argv) {
    if(argc<2) {
        printf("Usage: bin2root [input filename] <output filename>\n");
		return -1;
	}


    fin = new ifstream(argv[1], std::ios::in | std::ios::binary);

 //   tree  = (TTree*)fout->Get("dump");
    if(argc>2)
        fout= new TFile(argv[2],"RECREATE");
    else
        fout= new TFile(std::string(argv[1]).substr(0,std::string(argv[1]).find_last_of(".")).append(".root").c_str(),"RECREATE");

//	tree  = (TTree*)fout->Get("dump");


	daq_class = new TSTiC3DAQ(1);
 
    if(tree == NULL) {
        tree= new TTree("dump","dump");
        daq_class->AddBranch(tree,"br");
    }
    else {
        daq_class->SetBranch(tree,"br");
    }


    //current_event = new stic3_data_t;
    unsigned int* buffer = (unsigned int*)malloc(USBEVENTSIZE);

    // Read from binary file
    unsigned int nevent = 0;
    while(fin->read((char *)buffer, USBEVENTSIZE)){
        nevent++;
        if(nevent % 100000 == 0)
                printf("read event %i\n",nevent);
        //if(nevent<10) {
        if(daq_class->ProcessBufferToEvent(buffer) == 0) {
#ifdef DEBUG_EVENT
        
	        printf("Event Hex Code: %X %X \n\n", 0x00ffffffff&buffer[1], buffer[0]);

	        printf("E_fine: %u\n",current_event->E_fine);
	        printf("E_CC: %u\n",current_event->E_CC);
	        printf("E_BADHIT: %u\n",current_event->E_badhit);
	        printf("T_FINE: %u\n",current_event->T_fine);
	        printf("T_CC: %u\n",current_event->T_CC);
	        printf("T_BADHIT: %u\n",current_event->T_badhit);
	        printf("CHANNEL: %u\n",current_event->channel);
	        printf("FRAME: %u\n\n",current_event->frame_number); 
#endif
            tree->Fill();
            //}
        }
    }
    std::cout << "Events: " << nevent << std::endl;
    fin->close();

    tree->Write(0,TObject::kOverwrite);
    fout->Close();


	printf("The END...\n");

	return 0;
}
