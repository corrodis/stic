/*
 * Wrapper Class for readout of a USB device with libusb libraries
 *
 * Date: Thu Apr 18 18:42:43 CEST 2013
 * Author tharion
 *
 */

#include <stdint.h>
#include <iomanip>
#include <libusb.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include "TUSB_iface.h"

//#define DEBUG

TUSB_iface::TUSB_iface()
{
	ctx=NULL;
	devh = NULL;	//set the device handle to NULL for safety
	//Set the default values for the vendor and product id (cypress usb controller)
	vid = 0x04b4;
	pid = 0x1003;


	int r;

	r = libusb_init(NULL);	//Init the libusb driver
	if (r < 0) {
		fprintf(stderr, "failed to initialise libusb\n");
	}
	r=open_usb();

	if(r < 0) device_open = false;
		else device_open = true;

	timeout = 200;	//Default timeout 500ms -> 2000, new 50ms
	ep_read = 6;	//The in and out endpoints for reading and writing
	ep_write = 2;
}


TUSB_iface::~TUSB_iface()
{
	close_usb();
	libusb_exit(NULL);
}


int TUSB_iface::open_usb()
{
	int r;
	devh = libusb_open_device_with_vid_pid(NULL, vid, pid);	//Try to open the device
	r = libusb_claim_interface(devh, 0);	//The device interface is 1
	if (r < 0) {
		fprintf(stderr, "usb_claim_interface error %d\n", r);
	}

	return devh ? 0 : -EIO;	//Return open failed if unsuccessfull
}


void TUSB_iface::close_usb()
{
	if(device_open)
	{
		libusb_release_interface(devh, 0);
		libusb_close(devh);
	}
	libusb_exit(NULL);
}


int TUSB_iface::write_usb(unsigned char* buffer, int len)
{
	int transferred;
	int r;


	//Check if the device is open to avoid segmentation violations
	if(!device_open)
	{
		perror("TUSB_iface::write_usb : No USB device opened!!!\n");
		return -1;
	}

#ifdef DEBUG
	printf("TUSB_Iface DEBUG: PACKAGE OF LEN %d, Content: \n\n", len);
	for(int i=0;i<len;i++)
	{
		printf("%2X ",buffer[i]);
	}
	printf("\n\n");
#endif

	r = libusb_bulk_transfer(devh,((0x0f&ep_write) | LIBUSB_ENDPOINT_OUT),buffer, len ,&transferred,timeout);

	if (r < 0) return r;
	else return transferred;
}



int TUSB_iface::read_usb(unsigned char* buffer, int len)
{

	int transferred;
	int r;

	//Check if the device is open to avoid segmentation violations
	if(!device_open)
	{
		perror("TUSB_iface::read_usb : No USB device opened!!!\n");
		return -1;
	}

	r = libusb_bulk_transfer(devh,(0x80 | (0x0f&ep_read) | LIBUSB_ENDPOINT_IN),buffer,len,&transferred,timeout);

#ifdef DEBUG
	printf("\n TUSB_iface usb_read(): Bytes transferred: %d\n",transferred);
#endif

	if (transferred == 0) return r;
	else return transferred;
}

void TUSB_iface::clear_usb(int bufsize)
{

    int transferred;
    int r;

    unsigned char* chunk_buffer=(unsigned char*)malloc(bufsize);
    if (chunk_buffer==NULL){
        perror("TSTiC3DAQ: malloc");
        return;
    }

    //Check if the device is open to avoid segmentation violations
    if(!device_open)
    {
        perror("TUSB_iface::read_usb : No USB device opened!!!\n");
    }

    libusb_bulk_transfer(devh,(0x80 | (0x0f&ep_read) | LIBUSB_ENDPOINT_IN),chunk_buffer,bufsize,&transferred,1);

    free(chunk_buffer);
}

void TUSB_iface::set_ep_rw(unsigned short read, unsigned short write)
{
	ep_read = read;
	ep_write = write;
}

void TUSB_iface::set_timeout(unsigned int time)
{
	timeout=time;
}

void TUSB_iface::set_vid_pid(unsigned int vendor_id, unsigned int product_id)
{
	vid=vendor_id;
	pid=product_id;
}
