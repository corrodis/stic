#include "TApplication.h"
#include "TObject.h"
#include "TH1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TThread.h"
#include "TTree.h"
#include "TFile.h"
//#include <unistd.h>
//#include <iostream>
//#include <string>

#include "STiC3DAQ.h"

#define USBEVENTSIZE 8


TCanvas *c1;

TSTiC3DAQ* daq_class;
TThread *th_daq;
// a global histogram object, will be accessed both threads
//dump tree
TH1D 	*hist;
TTree	*tree;
TBranch	*branch;
TFile 	*fout;

ifstream *fin;

stic3_data_t* current_event;


int main(int argc, char **argv) {
    if(argc<2) {
        printf("Usage: bin2root [input filename] <output filename>\n");
		return -1;
	}


    fin = new ifstream(argv[1], std::ios::in | std::ios::binary);

 //   fout=new TFile(argv[1],"RECREATE");
 //   tree  = (TTree*)fout->Get("dump");
//    if(argc>2)
//        fin = new ifstream(argv[2], std::ios::in | std::ios::binary);
//    else
//        fin = new ifstream(std::string(argv[1]).substr(0,std::string(argv[1]).find_last_of(".")).append(".root").c_str(), std::ios::in | std::ios::binary);

	fout=new TFile(argv[1],"RECREATE");
	tree  = (TTree*)fout->Get("dump");


	daq_class = new TSTiC3DAQ();
 
//    if(tree == NULL)
//    {
//        tree= new TTree("dump","dump");
//        daq_class->AddBranch(tree,"br");
//    }
//    else
//    {
//        daq_class->SetBranch(tree,"br");
//    }


//    current_event = new stic3_data_t;
//    char * buffer = (char*)malloc(USBEVENTSIZE);

    // Read from binary file
//    unsigned int nevent = 0;
/*    while(fin->read(buffer, USBEVENTSIZE)){
        //std::cout << std::dec << *buffer << std::endl;
//        printf("%x ",(unsigned int)*buffer);
        nevent++;
        if(nevent % 10000 == 0)
//                printf("\n");
                printf("read event %i\n",nevent);
//        if(daq_class->ProcessBufferEvent((unsigned int* const)&buffer, current_event) == 0) {
//            //if(fBranch != NULL){
//            tree->Fill();
//            //}
//        }
    }
    fin->close();

//    tree->Write(0,TObject::kOverwrite);
//    fout->Close();


	printf("The END...\n");
*/
	return 0;
}
