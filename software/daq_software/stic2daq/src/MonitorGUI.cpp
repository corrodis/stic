/*
	DAQ class to read the STiC3 data sent over UDP by the FPGA
	Provides Access to basic commands for the FPGA,
	and saves the data in a ROOT Branch. For Monitoring, a ROOT histogram can be filled.

	Author: kbriggl, tharion

*/

#include "MonitorGUI.h"

#include "iostream"

#include "TGFrame.h"
#include "TGTab.h"
#include "TGLabel.h"
#include "TGProgressBar.h"


ClassImp (MonitorGUI);

MonitorGUI::MonitorGUI()
:stic_daq(0)
{

  StartGUI();
}

MonitorGUI::~MonitorGUI(){


}

void MonitorGUI::ResetHistos()
{
  std::vector<TSTiC3DAQ::monitor>* v_mon = stic_daq->GetMonitorHistograms();

  for(int i=0;i<v_mon->size();i++)
  {
    v_mon->at(i).hist->Reset();
  }
}


void MonitorGUI::StartGUI(){

	TGLayoutHints* layout1 = new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 0, 0, 4, 4);

	TGMainFrame *mainFrame = new TGMainFrame(gClient->GetRoot(), 100, 100, kMainFrame | kVerticalFrame);
	mainFrame->SetWindowName("Monitor GUI");
	mainFrame->SetIconName("Monitor GUI");
	mainFrame->Move(1600,850);
	mainFrame->SetWMPosition(1600,850);

	TGGroupFrame *group_frame = new TGGroupFrame(mainFrame, "Monitor GUI", kVerticalFrame);
	mainFrame->AddFrame(group_frame, new TGLayoutHints(kLHintsTop | kLHintsRight, 2, 2, 2, 2));



	TGHorizontalFrame *frame_label = new TGHorizontalFrame(group_frame, 0, 0);
	group_frame->AddFrame(frame_label, new TGLayoutHints(kLHintsRight | kLHintsTop, 0, 0, 8, 0));

	TGLabel* label_mode = new TGLabel(frame_label,"MODE");
	frame_label->AddFrame(label_mode, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 40, 60, 4, 4));
	TGLabel* label_ch1 = new TGLabel(frame_label,"CH 1");
	frame_label->AddFrame(label_ch1, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 0, 0, 4, 4));
	TGLabel* label_ch2 = new TGLabel(frame_label,"CH 2");
	frame_label->AddFrame(label_ch2, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 35, 20, 4, 4));

	TGCompositeFrame *frame2[4];

	for(int n=0;n<4;n++)
	{
		frame2[n] = new TGHorizontalFrame(group_frame, 0, 0);
		group_frame->AddFrame(frame2[n], new TGLayoutHints(kLHintsRight | kLHintsTop, 0, 0, 8, 0));

		comboBox_mode[n] = new TGComboBox(frame2[n],-1, kHorizontalFrame | kSunkenFrame | kDoubleBorder | kOwnBackground);
		frame2[n]->AddFrame(comboBox_mode[n], layout1);

		comboBox_mode[n]->AddEntry("E",1);
		comboBox_mode[n]->AddEntry("TSINGLE",2);
		comboBox_mode[n]->AddEntry("TSINGLE_FINE",3);
		comboBox_mode[n]->AddEntry("TSINGLE_CM",4);
		comboBox_mode[n]->AddEntry("CHANNEL",5);
		comboBox_mode[n]->AddEntry("TBADHIT",6);
		comboBox_mode[n]->AddEntry("PERIOD",7);
		comboBox_mode[n]->AddEntry("T",8);
		comboBox_mode[n]->Resize(120,22);
		if(n==0) comboBox_mode[n]->Select(5);
		else if(n==1) comboBox_mode[n]->Select(4);
		else if(n==2) comboBox_mode[n]->Select(3);
		else if(n==3) comboBox_mode[n]->Select(1);
		else comboBox_mode[n]->Select(1);

		comboBox_mode[n]->Connect("Selected(Int_t)", "MonitorGUI", this, "UseGUIValues()");

		entry_ch1_select[n] = new TGNumberEntry(frame2[n],10,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,256);
		frame2[n]->AddFrame(entry_ch1_select[n], layout1);
		entry_ch1_select[n]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UseGUIValues()");

		entry_ch2_select[n] = new TGNumberEntry(frame2[n],15,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,256);
		frame2[n]->AddFrame(entry_ch2_select[n], layout1);
		entry_ch2_select[n]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UseGUIValues()");

	}

	//Reset Button
	TGTextButton *button_reset = new TGTextButton(mainFrame," Reset ");
	mainFrame->AddFrame(button_reset, new TGLayoutHints(kLHintsCenterX | kLHintsTop, 2, 2, 5, 5));
	button_reset->Connect("Clicked()", "MonitorGUI", this, "ResetHistos()");

	TGGroupFrame *energy_frame = new TGGroupFrame(mainFrame, "Monitor GUI", kVerticalFrame);
	mainFrame->AddFrame(energy_frame, new TGLayoutHints(kLHintsTop | kLHintsRight, 2, 2, 2, 2));

	TGHorizontalFrame *frame_Elabel = new TGHorizontalFrame(energy_frame, 0);
	energy_frame->AddFrame(frame_Elabel, new TGLayoutHints(kLHintsRight | kLHintsTop, 0, 0, 8, 0));

	TGLabel* label_Ech1 = new TGLabel(frame_Elabel,"CH 1 ECUT");
	frame_Elabel->AddFrame(label_Ech1, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 0, 0, 4, 4));
	TGLabel* label_Ech2 = new TGLabel(frame_Elabel,"CH 2 ECUT");
	frame_Elabel->AddFrame(label_Ech2, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 35, 20, 4, 4));


	//Energy Cuts for T Monitor
	TGHorizontalFrame *frame_ecuts = new TGHorizontalFrame(energy_frame, 0, 0);

	entry_ch1_eCut[0] = new TGNumberEntry(frame_ecuts,1,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,600);
	frame_ecuts->AddFrame(entry_ch1_eCut[0], layout1);
	entry_ch1_eCut[0]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UpdateECuts()");

	entry_ch1_eCut[1] = new TGNumberEntry(frame_ecuts,600,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,600);
	frame_ecuts->AddFrame(entry_ch1_eCut[1], layout1);
	entry_ch1_eCut[1]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UpdateECuts()");



	entry_ch2_eCut[0] = new TGNumberEntry(frame_ecuts,0,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,600);
	frame_ecuts->AddFrame(entry_ch2_eCut[0], layout1);
	entry_ch2_eCut[0]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UpdateECuts()");

	entry_ch2_eCut[1] = new TGNumberEntry(frame_ecuts,600,6,-1,TGNumberFormat::kNESInteger,TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,600);
	frame_ecuts->AddFrame(entry_ch2_eCut[1], layout1);
	entry_ch2_eCut[1]->Connect("ValueSet(Long_t)", "MonitorGUI", this, "UpdateECuts()");

	energy_frame->AddFrame(frame_ecuts, new TGLayoutHints(kLHintsRight | kLHintsTop, 0, 0, 8, 0));

	UseGUIValues();
	UpdateECuts();

	mainFrame->SetMWMHints(kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
	mainFrame->MapSubwindows();
	mainFrame->Resize(mainFrame->GetDefaultSize());
	mainFrame->MapWindow();
}

int MonitorGUI::UseGUIValues(){

  if(stic_daq==0)
  {
    printf("MonitorGUI: No STiC3DAQ set.");
    return -1;
  }



  printf("updating values\n");
  for(int n=0;n<4;n++)
  {

  printf("updating channels\n");
    stic_daq->GetMonitorHistograms()->at(n).fChannel1 = entry_ch1_select[n]->GetNumber();
    stic_daq->GetMonitorHistograms()->at(n).fChannel2 = entry_ch2_select[n]->GetNumber();


    printf("updating monitors\n");
    if(comboBox_mode[n]->GetSelected()==1)
    {
      stic_daq->GetMonitorHistograms()->at(n).fMode = TSTiC3DAQ::MODE_E;
      stic_daq->GetMonitorHistograms()->at(n).hist->SetTitle("E");
      stic_daq->GetMonitorHistograms()->at(n).hist->SetBins((1<<15),0,(1<<15));
      stic_daq->GetMonitorHistograms()->at(n).hist->GetXaxis()->SetRangeUser(0,500);
    }
    if(comboBox_mode[n]->GetSelected()==2)
    {
      stic_daq->GetMonitorHistograms()->at(n).fMode = TSTiC3DAQ::MODE_TSINGLE;
      stic_daq->GetMonitorHistograms()->at(n).hist->SetTitle("TSINGLE");
      stic_daq->GetMonitorHistograms()->at(n).hist->SetBins((1<<17),0,(1<<17));
    }
    if(comboBox_mode[n]->GetSelected()==3)
    {
      stic_daq->GetMonitorHistograms()->at(n).fMode = TSTiC3DAQ::MODE_TSINGLE_FINE;
      stic_daq->GetMonitorHistograms()->at(n).hist->SetTitle("TSINGLE_FINE");
      stic_daq->GetMonitorHistograms()->at(n).hist->SetBins(32,0,32);
    }
    if(comboBox_mode[n]->GetSelected()==4)
    {
      stic_daq->GetMonitorHistograms()->at(n).fMode = TSTiC3DAQ::MODE_TSINGLE_CM;
      stic_daq->GetMonitorHistograms()->at(n).hist->SetTitle("TSINGLE_CM");
      stic_daq->GetMonitorHistograms()->at(n).hist->SetBins((1<<15),0,(1<<15));
    }
    if(comboBox_mode[n]->GetSelected()==5)
    {
      stic_daq->GetMonitorHistograms()->at(n).fMode = TSTiC3DAQ::MODE_CHANNEL;
      stic_daq->GetMonitorHistograms()->at(n).hist->SetTitle("CHANNEL");
      stic_daq->GetMonitorHistograms()->at(n).hist->SetBins(256,0,256);
    }
    if(comboBox_mode[n]->GetSelected()==6)
    {
      stic_daq->GetMonitorHistograms()->at(n).fMode = TSTiC3DAQ::MODE_TBADHIT;
      stic_daq->GetMonitorHistograms()->at(n).hist->SetTitle("TBADHIT");
      stic_daq->GetMonitorHistograms()->at(n).hist->SetBins(2,0,2);
    }
    if(comboBox_mode[n]->GetSelected()==7)
    {
      stic_daq->GetMonitorHistograms()->at(n).fMode = TSTiC3DAQ::MODE_PERIOD;
      stic_daq->GetMonitorHistograms()->at(n).hist->SetTitle("PERIOD");
      stic_daq->GetMonitorHistograms()->at(n).hist->SetBins((1<<18),0,(1<<18));
    }
    if(comboBox_mode[n]->GetSelected()==8)
    {
      stic_daq->GetMonitorHistograms()->at(n).fMode = TSTiC3DAQ::MODE_T;
      stic_daq->GetMonitorHistograms()->at(n).hist->SetTitle("T");
      stic_daq->GetMonitorHistograms()->at(n).hist->SetBins((1<<17),-(1<<17),(1<<17));
    }

  }
}


void MonitorGUI::UpdateECuts()
{

	if(stic_daq==0) return;


	for(int n=0;n<4;n++)
	{
		printf("Changed Ch1 ECut to %f %f \n",entry_ch1_eCut[0]->GetNumber(),entry_ch1_eCut[1]->GetNumber());
		stic_daq->GetMonitorHistograms()->at(n).ch1_ECut_low = entry_ch1_eCut[0]->GetNumber();
		stic_daq->GetMonitorHistograms()->at(n).ch1_ECut_high = entry_ch1_eCut[1]->GetNumber();

		printf("Changed Ch2 ECut to %f %f \n",entry_ch2_eCut[0]->GetNumber(),entry_ch2_eCut[1]->GetNumber());
		stic_daq->GetMonitorHistograms()->at(n).ch2_ECut_low = entry_ch2_eCut[0]->GetNumber();
		stic_daq->GetMonitorHistograms()->at(n).ch2_ECut_high = entry_ch2_eCut[1]->GetNumber();
	}
}
