#include "TApplication.h"
#include "TObject.h"
#include "TH1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TThread.h"
#include "TTree.h"
#include "TFile.h"
#include "TLine.h"

#include "STiC3DAQ.h"
#include "MonitorGUI.h"

#include <iostream>



/*
** STIC2DAQ
**
** The Code will intanciate a DAQ class, set appropriate parameters
** and start a separate Thread where the data is read out.
** In the Main thread, a Histogram is Drawn and updated
*/

// a global canvas for the drawing of histograms
TCanvas *c1,*c2, *c3, *c4, *c5;

TSTiC3DAQ* daq_class;
//Monitor GUI interface
MonitorGUI* mon_gui;

TThread *th_daq;
// a global histogram object, will be accessed both threads
//dump tree

TH1I 	*hist[4][64]; //[handleID][channel]
TH1I 	*hist_f[4][64]; //[handleID][channel]
TH1I	*hist1;
TH1I	*hist2;
TH1I	*hist3;
TH1I	*hist4;
   
TH1I	*hist_e_mean[4];
TH1I	*hist_e_rms[4];
TH1I	*hist_f_mean[4];
TH1I	*hist_f_rms[4];

TLine *line[3][8];
TLine *line_f[3][8];

TTree	*tree;
TBranch	*branch;
TFile 	*fout;

//SIGINT handler
#include <signal.h>
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	//daq_class->StopReadout();	//TODO: will not cancel the thread if no data is incoming
	TThread::Ps();
   	th_daq->Join();
	tree->Write();
	fout->Close();
	TThread::Ps();
	exit(0);
}



void *top(void *)
{
   TThread::Printf("Start of top");

   for (Int_t i = 0; ;i++) {
     
     //if(i % 100000 == 0){
     if(c1!=NULL){
	 for(int j=0;j<4;j++){
	   c1->GetPad(j+1)->Update();
	   c1->GetPad(j+1)->Modified();
	 }
       }
     
     for(int j=0;j<8;j++){
       c4->GetPad(j+1)->Update();
       c4->GetPad(j+1)->Modified();
       //double max = c4->cd(j+1)->GetUymax();
       //if(i==2 || i % 1 == 0 )
       //{
       //for(int l=0;l<3;l++){
       //  //line[l][j]->SetY1(c4->cd(j+1)->GetUymin());
       //  line[l][j]->SetY2(max);
       //  line[l][j]->Draw("SAME");
       //}
       //c4->GetPad(j+1)->Update();
       //c4->GetPad(j+1)->Modified();
       //line[3][j]->Draw("SAME");
       //line[4][j]->Draw("SAME");
       //line[5][j]->Draw("SAME");
       //  }
     }
     
     for(int j=0;j<8;j++){
       //for(int j=0;j<2;j++){
       c5->GetPad(j+1)->Update();
       c5->GetPad(j+1)->Modified();
       double max = c5->cd(j+1)->GetUymax();
       
       for(int l=0;l<3;l++){
	 line_f[l][j]->SetY2(max);
	 line_f[l][j]->Draw("SAME");
       }
       c5->GetPad(j+1)->Update();
       c5->GetPad(j+1)->Modified();
     }
     if(i % 30 == 0){
       TThread::Ps();
       printf("Recorded %lld events\n",branch->GetEntries());
     }
     TThread::Sleep(1);
     //TThread::Sleep(0.25);
     //TThread::Sleep(10);
     //}
   }
   TThread::Printf("End of top");
   return 0;
}

int main(int argc, char **argv)
{

   signal(SIGINT,&handler_sigint);

   TApplication theApp("DAQ", &argc, argv);
   // exit on close window

   // a global canvas for the drawing of histograms
   c1 = new TCanvas("c1","Readout Monitor",800,400);
   c1->Connect("Closed()","TApplication",&theApp,"Terminate(=0)");

   c4 = new TCanvas("c4","Energy Mean & RMS Monitor",1000,600);
   c4->Connect("Closed()","TApplication",&theApp,"Terminate(=0)");
   c5 = new TCanvas("c5","Period Mean & RMS Monitor",1000,600);
   c5->Connect("Closed()","TApplication",&theApp,"Terminate(=0)");
   
   // a global histogram object, which will be accessed both by mhs and mhs1
   hist1  = new TH1I("monitor","Bad Hit1",(1<<15),-0.5, (1<<15)-0.5);
   hist2  = new TH1I("monitor2","Coarse Counter Values",(1<<15),-0.5, (1<<15)-0.5);
   hist3  = new TH1I("monitor3","Fine Counter",(1<<15),-0.5, (1<<15)-0.5);
   hist4  = new TH1I("monitor4","Energy Measurement",(1<<15),-0.5, (1<<15)-0.5);

   c1->cd();
   c1->Divide(2,2);
   c1->cd(1);
   hist1->Draw();
   c1->cd(2);
   hist2->Draw();
   c1->cd(3);
   hist3->Draw();
   c1->cd(4);
   hist4->Draw();
   
   double max_mean=600.;
   double max_rms=50.;
   
   for(int i_chip =0;i_chip<4;i_chip++){
     //for(int i_chip =0;i_chip<1;i_chip++){
     for(int j=0;j<64;j++){
       hist[i_chip][j]  = new TH1I(Form("monitor_e_%d_%d",j+1,i_chip),Form("Energy Measurement ch_%d_%d",j,i_chip),(1<<15),-0.5, (1<<15)-0.5);
       //hist[i_chip][j]->GetXaxis()->SetRangeUser(0,max_mean);
       hist_f[i_chip][j]  = new TH1I(Form("monitor_period_%d_%d",j+1,i_chip),Form("Period Measurement ch_%d_%d",j,i_chip),(1<<18),0, (1<<18));
     }
   }
   
   c4->cd();
   c4->Divide(2,4);
   
   for(int j=0;j<4;j++){
     hist_e_mean[j]  = new TH1I(Form("energy_mean_%d",j+1),Form("Energy Mean chip_%d",j),64,0, 64);
     hist_e_rms[j]  = new TH1I(Form("energy_rms_%d",j+1),Form("Energy rms chip_%d",j),64,0, 64);
     hist_e_mean[j]->GetYaxis()->SetRangeUser(0,max_mean);
     hist_e_rms[j]->GetYaxis()->SetRangeUser(0,max_rms);
     
     c4->cd(2*j+1);
     hist_e_mean[j]->Draw();
     //line[0][2*j] = new TLine(16,c4->cd(2*j+1)->GetUymin(),16,c4->cd(2*j+1)->GetUymax());
     //line[1][2*j] = new TLine(32,c4->cd(2*j+1)->GetUymin(),32,c4->cd(2*j+1)->GetUymax());
     //line[2][2*j] = new TLine(48,c4->cd(2*j+1)->GetUymin(),48,c4->cd(2*j+1)->GetUymax());
     line[0][2*j] = new TLine(16,0,16,max_mean);
     line[1][2*j] = new TLine(32,0,32,max_mean);
     line[2][2*j] = new TLine(48,0,48,max_mean);
     line[0][2*j]->SetLineColor(kRed);
     line[1][2*j]->SetLineColor(kRed);
     line[2][2*j]->SetLineColor(kRed);
     line[0][2*j]->Draw("SAME");
     line[1][2*j]->Draw("SAME");
     line[2][2*j]->Draw("SAME");
     c4->cd(2*j+2);
     hist_e_rms[j]->Draw();
     //line[0][2*j+1] = new TLine(16,c4->cd(2*j+2)->GetUymin(),16,c4->cd(2*j+2)->GetUymax());
     //line[1][2*j+1] = new TLine(32,c4->cd(2*j+2)->GetUymin(),32,c4->cd(2*j+2)->GetUymax());
     //line[2][2*j+1] = new TLine(48,c4->cd(2*j+2)->GetUymin(),48,c4->cd(2*j+2)->GetUymax());
     line[0][2*j+1] = new TLine(16,0,16,max_rms);
     line[1][2*j+1] = new TLine(32,0,32,max_rms);
     line[2][2*j+1] = new TLine(48,0,48,max_rms);
     line[0][2*j+1]->SetLineColor(kRed);
     line[1][2*j+1]->SetLineColor(kRed);
     line[2][2*j+1]->SetLineColor(kRed);
     line[0][2*j+1]->Draw("SAME");
     line[1][2*j+1]->Draw("SAME");
     line[2][2*j+1]->Draw("SAME");
   }


   c5->cd();
   c5->Divide(2,4);
   
   for(int j=0;j<4;j++){
     hist_f_mean[j]  = new TH1I(Form("period_mean_%d",j+1),Form("Period Mean chip_%d",j),64,0, 64);
     hist_f_rms[j]  = new TH1I(Form("period_rms_%d",j+1),Form("Period rms chip_%d",j),64,0, 64);
     //hist_f_mean[j]->GetYaxis()->SetRangeUser(0,max_mean);
     //hist_f_rms[j]->GetYaxis()->SetRangeUser(0,max_rms);
     
     c5->cd(2*j+1);
     hist_f_mean[j]->Draw();
     //line[0][2*j] = new TLine(16,c4->cd(2*j+1)->GetUymin(),16,c4->cd(2*j+1)->GetUymax());
     //line[1][2*j] = new TLine(32,c4->cd(2*j+1)->GetUymin(),32,c4->cd(2*j+1)->GetUymax());
     //line[2][2*j] = new TLine(48,c4->cd(2*j+1)->GetUymin(),48,c4->cd(2*j+1)->GetUymax());
     line_f[0][2*j] = new TLine(16,0,16,max_mean);
     line_f[1][2*j] = new TLine(32,0,32,max_mean);
     line_f[2][2*j] = new TLine(48,0,48,max_mean);
     line_f[0][2*j]->SetLineColor(kRed);
     line_f[1][2*j]->SetLineColor(kRed);
     line_f[2][2*j]->SetLineColor(kRed);
     line_f[0][2*j]->Draw("SAME");
     line_f[1][2*j]->Draw("SAME");
     line_f[2][2*j]->Draw("SAME");
     c5->cd(2*j+2);
     hist_f_rms[j]->Draw();
     //line[0][2*j+1] = new TLine(16,c4->cd(2*j+2)->GetUymin(),16,c4->cd(2*j+2)->GetUymax());
     //line[1][2*j+1] = new TLine(32,c4->cd(2*j+2)->GetUymin(),32,c4->cd(2*j+2)->GetUymax());
     //line[2][2*j+1] = new TLine(48,c4->cd(2*j+2)->GetUymin(),48,c4->cd(2*j+2)->GetUymax());
     line_f[0][2*j+1] = new TLine(16,0,16,max_rms);
     line_f[1][2*j+1] = new TLine(32,0,32,max_rms);
     line_f[2][2*j+1] = new TLine(48,0,48,max_rms);
     line_f[0][2*j+1]->SetLineColor(kRed);
     line_f[1][2*j+1]->SetLineColor(kRed);
     line_f[2][2*j+1]->SetLineColor(kRed);
     line_f[0][2*j+1]->Draw("SAME");
     line_f[1][2*j+1]->Draw("SAME");
     line_f[2][2*j+1]->Draw("SAME");
   }


   c1->Update();
   //c2->Update();
   //c3->Update();
   c4->Update();
   c5->Update();
   
   fout=new TFile("dump.root","RECREATE");

   tree  = new TTree("dump","dump");
   int size =0;
   //DAQ Configuration
   daq_class = new TSTiC3DAQ();

   mon_gui=new MonitorGUI;
   mon_gui->SetSTiC3DAQ(daq_class);

   //old gui
   TSTiC3DAQ::monitor mon(TSTiC3DAQ::MODE_PERIOD);
   mon.hist = hist1;
   mon.fChannel1=47;
   mon.fChannel2=0;
   //mon.fMode=TSTiC3DAQ::MODE_PERIOD;

   daq_class->AddMonitor(mon);
   size++;
   mon.hist = hist2;
   mon.fChannel1=15;
   mon.fChannel2=0;
   mon.fMode=TSTiC3DAQ::MODE_TSINGLE_CM;

   daq_class->AddMonitor(mon);
   size++;
   mon.hist = hist3;
   mon.fChannel1=15;
   mon.fChannel2=0;
   mon.fMode=TSTiC3DAQ::MODE_TSINGLE_FINE;

   daq_class->AddMonitor(mon);
   size++;
   mon.hist = hist4;
   mon.fChannel1=15;
   mon.fChannel2=0;
   mon.fMode=TSTiC3DAQ::MODE_E;

   daq_class->AddMonitor(mon);
   size++;

   for(int i_chip =0;i_chip<4;i_chip++){
     //for(int i_chip =0;i_chip<1;i_chip++){
     for(int j=0;j<64;j++){
       TSTiC3DAQ::monitor mon(TSTiC3DAQ::MODE_E);
       mon.hist = hist[i_chip][j];
       mon.fChannel1=j+i_chip*64;
       mon.fChannel2=j+i_chip*64;
       daq_class->AddMonitor(mon);  
       size++;
     }
   }

   for(int i_chip =0;i_chip<4;i_chip++){
     for(int j=0;j<64;j++){
       //     int i_chip=0;  
       TSTiC3DAQ::monitor mon(TSTiC3DAQ::MODE_PERIOD);
       mon.hist = hist_f[i_chip][j];
       mon.fChannel1=j+i_chip*64;//CHANNEL
       mon.fChannel2=j+i_chip*64;
       daq_class->AddMonitor(mon);  
     }
   }

   for(int j=0;j<4;j++){
     TSTiC3DAQ::monitor mon(TSTiC3DAQ::MODE_E_MEAN);
     mon.hist = hist_e_mean[j];
     mon.fChannel1=j; //wiil be used for handleID
     mon.fChannel2=3;//wiil be used for shift
     daq_class->AddMonitor(mon);  
   }
   for(int j=0;j<4;j++){
     TSTiC3DAQ::monitor mon(TSTiC3DAQ::MODE_E_RMS);
     mon.hist = hist_e_rms[j];
     mon.fChannel1=j;//wiil be used for handleID
     mon.fChannel2=3;//wiil be used for shift
     daq_class->AddMonitor(mon);  
   }

   for(int j=0;j<4;j++){
     //for(int j=0;j<1;j++){
     TSTiC3DAQ::monitor mon(TSTiC3DAQ::MODE_F_MEAN);
     mon.hist = hist_f_mean[j];
     mon.fChannel1=j; //wiil be used for handleID
     mon.fChannel2=size; //wiil be used for shift
     daq_class->AddMonitor(mon);  
   }
   for(int j=0;j<4;j++){
     //for(int j=0;j<1;j++){
     TSTiC3DAQ::monitor mon(TSTiC3DAQ::MODE_F_RMS);
     mon.hist = hist_f_rms[j];
     mon.fChannel1=j;//wiil be used for handleID
     mon.fChannel2=size;//wiil be used for shift
     daq_class->AddMonitor(mon);  
   }

   branch=daq_class->AddBranch(tree,"br");

   // start Thread
   // thread function (readout thread)
   th_daq = new TThread("daq", (TThread::VoidRtnFunc_t) &(TSTiC3DAQ::reader_thread), daq_class);
   // top thread (just ps and update)
   TThread *th_ps = new TThread("top",top);
   th_daq->Run();
   th_ps->Run();

   daq_class->StartReadout();
   theApp.Run(kTRUE);


   c1=NULL;	//not completely thread save, but it's a start.
   th_ps->SetCancelAsynchronous();
   th_ps->Kill();

   daq_class->StopReadout();
   th_daq->Join();
   tree->Write();
   fout->Close();


   printf("The END...\n");

   return 0;
}

