/*
 * VirtualConfig.cpp
 *
 *  Created on: 29.09.2011
 *      Author: Konrad
 */

//#define DEBUG1


#include <string.h>
#include <stdio.h>
#include <iostream>
#include <fstream>

#include "../include/VirtualConfig.h"
#include "../include/VirtualInterface.h"

#define DEBUG

using namespace std;



TVirtualConfig::TVirtualConfig(TVirtualInterface* Interface,
                               /*const members*/const void* pars, const short int nParameters, const short int patternlength):
    parameters((parameter*)pars),
    nParameters(nParameters),
    patternlength(patternlength)
{

    Iface=Interface;
    ifaceHandID=Iface->AttachHandle();
    printf("Config : Attached Handle ID = %d\n",ifaceHandID);
}



TVirtualConfig::~TVirtualConfig()
{
//	printf("VC destruct\n");

    if (Iface->DetachHandle(ifaceHandID)<0)
    {
        printf("Iface: is Detached\n");
    }
}




/** @brief Set Parameter in Pattern
  *
  * (documentation goes here)
  */
void TVirtualConfig::SetInPattern(char * const pattern, int N,unsigned long long const& value ) const
{
//	printf("Parameter %d set to %llu\n",N,value);
    	int pos;
    	unsigned long long mask=1;
	if (parameters[N].endianess==false){
		pos=parameters[N].offset;
		while (pos<parameters[N+1].offset) //loop over bits in parameter
		{
			if ((value & mask)!=0)    //bit is set in parameter
				pattern[pos/8]|= (0x01<<(pos%8));
			else
				pattern[pos/8]&=~(0x01<<(pos%8));
			pos++;
			mask<<=1;
		}
	}else{
		pos=parameters[N+1].offset-1;
		while (pos>=parameters[N].offset) //loop over bits in parameter
		{
			if ((value & mask)!=0)    //bit is set in parameter
				pattern[pos/8]|= (0x01<<(pos%8));
			else
				pattern[pos/8]&=~(0x01<<(pos%8));
			pos--;
			mask<<=1;
		}

	}


};


/** @brief Get Parameter in Pattern
  *
  * (documentation goes here)
  */



void TVirtualConfig::GetFromPattern(char * const pattern, int N, unsigned long long &value) const
{
    unsigned long long temp=0;//temporary value read from pattern
    int pos;
	pos=parameters[N].offset;
	while (pos<parameters[N+1].offset) //loop over bits in parameter
	{
	    if ((pattern[pos/8] & ( 1<< (pos%8))) !=0 ){
		if (parameters[N].endianess==false)
			temp+=(1<<(pos-parameters[N].offset));
		else
			temp+=(1<<(parameters[N+1].offset-1-pos));
	    }
	    pos++;
	}
	value=temp;
};

short int TVirtualConfig::GetParID(const char *name) const
{
    //suche parameter-ID mit namen
    int ID=-1;
    for (int npar=0; npar<nParameters; npar++)
    {
        if(strcmp(name,parameters[npar].name)==0)
        {
            ID=npar;
            break;
        }
    }
    return ID;
}

const char* TVirtualConfig::GetParName(short int n) const
{
    if ((n>=0)&&(n<nParameters))
        return parameters[n].name;
    else	return NULL;
}



const int TVirtualConfig::ChannelFromID(short int n) const{
	int pos=strlen(parameters[n].name);
	const char* name=parameters[n].name;
	int chan;

        while ((pos>=0)&&(name[pos]!='_')){pos--;};
        if (pos<0){
                return -1;
        }
        if(sscanf(&name[pos],"_CH%d",&chan)!=1){
                return -1;
        }
	return chan;
}



int TVirtualConfig::UpdateConfig()
{
    int status;
#ifdef DEBUG
    printf("Bitpattern wr: ");
    int nch = ((patternlength-1)/8)*8;
    printf("\n %05X: ",nch);
    for (nch=patternlength-1; nch>=0; nch--)
    {
	if(nch % 8 == 0) printf("\n %05X: ",nch);
	else
		if(nch % 4 == 0) printf("    ",nch);

        printf("%02X ",0xFF&bitpattern_write[nch]);
    }
    printf("\n");
#endif
    status =Iface->Command(ifaceHandID,'c', patternlength,bitpattern_write,  patternlength,bitpattern_read);
    if (status<0)
	return status;

#ifdef DEBUG
    printf("Bitpattern rd: ");
    for (int nch=patternlength-1; nch>=0; nch--)
    {
        printf("%02X ",0xFF&bitpattern_read[nch]);
    }
    printf("\n");
#endif


    /*check bitpattern*/
    //take care of the MSB wich might not be alligned
    unsigned short bitmask=(parameters[patternlength].offset%8);
    nch=patternlength-1;
    if( (bitmask&bitpattern_write[nch]) != (bitmask&bitpattern_read[nch])){
		    printf("Difference found for byte %d: %02X recv: %02X \n",nch,bitmask&bitpattern_write[nch],bitmask&bitpattern_read[nch]);
	    return -1;
    }

    for (int nch=patternlength-2; nch>=0; nch--)
    {
	    if( (0xff&bitpattern_write[nch]) != (0xff&bitpattern_read[nch])){
		    printf("Difference found for byte %d: %02X recv: %02X \n",nch,0xff&bitpattern_write[nch],0xff&bitpattern_read[nch]);
		    return -1;
	    }
    }

    return 0;
}




/** @brief Get Parameter (read)
  * Return: number of parameter, -1 if not in range/found/RW-error
  */
int TVirtualConfig::GetParValueRD(const char *name, unsigned long long &value) const
{
    return GetParValueRD(GetParID(name), value);
}
/** @brief Get Parameter (read)
  * Return: number of parameter, -1 if not in range/found/RW-error
  */
int TVirtualConfig::GetParValueRD(short int npar, unsigned long long &value) const
{
    if ( (npar<0) | (npar>=nParameters) )
        return -1;
    GetFromPattern(bitpattern_read, npar, value);
    return npar;
}



/** @brief Get Parameter (to be written)
  * Return: number of parameter, -1 if not in range/found
  */
int TVirtualConfig::GetParValueWR(short int npar, unsigned long long &value) const
{
    if ( (npar<0) | (npar>=nParameters) )
        return -1;
    GetFromPattern(bitpattern_write, npar, value);
    return npar;
}



/** @brief Get Parameter (to be written)
  * Return: number of parameter, -1 if not in range/found
  */
int TVirtualConfig::GetParValueWR(const char *name, unsigned long long &value) const
{
    return GetParValueWR(GetParID(name), value);
}





int TVirtualConfig::SetParValue(const char *name, unsigned long long value)
{
    return SetParValue(GetParID(name), value);
}


int TVirtualConfig::SetParValue(short int npar,unsigned long long value)
{
    if ( (npar<0) | (npar>=nParameters) )
        return -1;
    SetInPattern(bitpattern_write,npar,value);

    return npar;
}



int TVirtualConfig::ReadFromFile(const char *fname,bool write)
{
    if (fname==NULL)
	return 0;
    ifstream ifile(fname);
    string line;
    if (!ifile.is_open())
    {
        printf("Configuration File \"%s\" could not be opened. Leaving Standard Values.\n",fname);
        return -1;
    }

    short int nRead=0; //Anzahl gelesener Parameter
    char parname[MAX_PARLENGTH];
    unsigned long long value;
    while (getline(ifile,line,'\n').good())
    {
	if(line.c_str()[0]=='#')
		continue;
        if(sscanf(line.c_str(),"%s = %llX",parname,&value)!=2)
        {
            printf("TVirtualConfig::ReadFromFile() - Error in syntax.\n\t Line: %s\n",line.c_str());
            continue;
        }
        if (SetParValue(parname,value)>=0)
            nRead++;
        else
        {
            printf("TVirtualConfig::ReadFromFile() - Parameter \"%s\" not recognized.\n",parname);
        }

#ifdef DEBUG1
        printf("\t parname %s value=%llX\n%s\n",parname,value,line.c_str());
#endif
    }

    return 1;
}

void TVirtualConfig::Print(bool batch,const char* ofilename) const
{
    unsigned long long value_rd,value_wr;
    FILE * outfile=NULL;
    if (ofilename!=NULL){
  	outfile = fopen (ofilename,"w");
	if (outfile==NULL){
		perror("ERROR: Could not open file in write mode");
		return;
	}
    }
    else
	outfile=stdout;

    if(!batch)
    {
        fprintf(outfile,"Configuration values: \n\n");
        fprintf(outfile,"parname\t sent\tread\n");
    }
    for (int npar=0; npar<nParameters; npar++)
    {
        GetFromPattern(bitpattern_read,npar,value_rd);
        GetFromPattern(bitpattern_write,npar,value_wr);
        if(batch)
            fprintf(outfile,"%s = %llx\n",GetParName(npar),value_wr);
        else
            fprintf(outfile,"%s =\t%llx\t%llx\n",GetParName(npar),value_wr,value_rd);


    }

    if (ofilename!=NULL && outfile!=NULL)
    	fclose(outfile);
}
