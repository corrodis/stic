/*
 * STiC2Config.cpp
 *
 *  Created on: 29.09.2011
 *      Author: Konrad
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/STiC2FPGAConfig.h"
#include "../include/VirtualInterface.h"


TSTiC2FPGAConfig::TSTiC2FPGAConfig(TVirtualInterface* Interface)
{
		deser_mask = 0x00;		//by default do not enable any deserializer (helps when starting the readout)
		Iface = Interface;
};

TSTiC2FPGAConfig::~TSTiC2FPGAConfig()
{
		printf("STiC2FPGAConfiguration destroyed\n");
};


void TSTiC2FPGAConfig::UpdateDAQConfig()
{
    Iface->Command((unsigned int)deser_mask,'M',0, NULL);	//Update the deserializer
}

bool TSTiC2FPGAConfig::GetActive(unsigned short deser)
{
		if ((deser_mask & (0x1 << deser)) == 0) return false;
		else return true;
}


void TSTiC2FPGAConfig::UpdateDeserEnable(unsigned short deser,unsigned short value)
{
		deser_mask = (~(0x1 << deser))&deser_mask;	//Reset the value to 0
		deser_mask = deser_mask | ((0xff&value) << deser);	//Reset the value to 1 if it should be enabled
		printf("New deserializer mask: %02X \n",deser_mask);

		UpdateDAQConfig();
}
