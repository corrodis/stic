/*
 * klaus2Config.cpp
 *
 *  Created on: 29.09.2011
 *      Author: Konrad
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/Klaus2Config.h"
#include "../include/VirtualConfig.h"
#include "../include/VirtualInterface.h"

#define NPARS		25
#define PATLEN		19


TKlaus2Config::TKlaus2Config(TVirtualInterface* Interface)
    :TVirtualConfig(Interface,par_descriptor,NPARS,PATLEN)
{
    bitpattern_read =  new char[PATLEN];
    bitpattern_write = new char[PATLEN];


    memset(bitpattern_read,0,sizeof(char) * PATLEN);
    memset(bitpattern_read,0,sizeof(char) * PATLEN);
};

TKlaus2Config::~TKlaus2Config()
{
    delete [] bitpattern_read;
    delete [] bitpattern_write;
};
//


TVirtualConfig::parameter const TKlaus2Config::par_descriptor[]=
{
	{"O_DAC0",0,	0},\
	{"O_DAC1",8,	0},\
	{"O_DAC2",16,	0},\
	{"O_DAC3",24,	0},\
	{"O_DAC4",32,	0},\
	{"O_DAC5",40,	0},\
	{"O_DAC6",48,	0},\
	{"O_DAC7",56,	0},\
	{"O_DAC8",64,	0},\
	{"O_DAC9",72,	0},\
	{"O_DAC10",80,	0},\
	{"O_DAC11",88,	0},\
	{"O_SHAPING",96,	0},\
	{"O_SCALING",99,	0},\
	{"O_POWERGATE_BIT",102,	0},\
	{"O_GAIN_SW",113,	0},\
	{"O_SW_B_DAC_H",114,	0},\
	{"O_SW_B_INPOWCOM_H",115,	0},\
	{"O_SW_SD_ISG_I",116,	0},\
	{"O_PDSTL_PROBE_H",117,	0},\
	{"O_CSCDE_PROBE_H",118,	0},\
	{"O_CSCDE_B_PROBE_H",119,	0},\
	{"O_CSCDE_CMP_PROBE_H",120,	0},\
	{"O_CH_CHARGE_PROBE_H",121,	0},\
	{"O_CH_DAC_PROBE_H",133,	0},\
	{"",145,0}
};


