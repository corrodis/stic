int argc;
char** argv;


const char* help_screen =
    "Syntax: chip-interface [OPTIONS]"
    "		  [-h] [--help]\n"
    "             [-devname=path] : Name of interface device.\n"
    "             [-iftype=type] : Type of interface. Can be:\n"

///INTERFACES
#ifndef NO_SERIAL
    "		tty: FPGA-board with usb connector\n"
    "		device will be /dev/ttyUSB1 if not specified in -devname\n"
#endif
#ifndef NO_UDP
    "		: UDP packetized interface\n"
    "		host will be 192.168.0.2 if not specified in -devname\n"
#endif

//CHIPS
    "             [-chip=type] : Type of configuration Pattern. Can be:\n"
#ifndef NO_KLAUS2
    "               klaus2"
#endif
    "\n"

#ifndef NO_STIC2
    "               stic2"
#endif

//OTHER
    "\n"

    "             [-Nrwop=n] : Write n times before printing the bitpattern read last.\n"
    "		  	       Typically 1 or 2, more for debug purposes.\n"
    "             [-b] : Print out Values read from chip as configuration file output\n"
    "             [-if=filename] : File to be read other than standard_tokens.txt in current directory\n\n";

#include <string.h>
#include <stdio.h>

#include "../include/VirtualInterface.h"
#include "../include/VirtualConfig.h"
#ifndef NO_SERIAL
#include "../include/SerialInterface.h"
#endif
#ifndef NO_UDP
#include "../include/UDPInterface.h"
#endif


#ifndef NO_KLAUS2
#include "../include/Klaus2Config.h"
#endif
#ifndef NO_STIC2
#include "../include/STiC2Config.h"
#endif






//lese parameter aus argv
const char* GetCmdParam(const char* argv_name, bool value_required)
{
    int argv_name_len = strlen(argv_name);
    for (int i=1; i<argc; i++)
    {
        if (strncmp(argv_name, argv[i], argv_name_len)==0)
        {
            if (argv[i][argv_name_len]==0)
            {
                if (value_required)
                {
                    printf("Parameter %s is incomplete", argv[i]);
                    return NULL;
                }
                return &argv[i][argv_name_len];
            }
            if (argv[i][argv_name_len]=='=')
            {
                return &argv[i][argv_name_len+1];
            }
        }
    }
    return NULL;
}


int main(int _argc, char* _argv[])
{
    argc=_argc;
    argv=_argv;
//teste command line parameter durch
    if (argc==1)
    {
//  printf("%s\n", help_screen);
//  exit(0);
    }

    if (GetCmdParam("-h", false) || GetCmdParam("--help", false))
    {
        printf("%s\n", help_screen);
        exit(0);
    }

//printout mode
    bool batchmode=false;
    if (GetCmdParam("-b",false))
        batchmode=true;

    const char *value;
    const char *devname;

    TVirtualInterface *iface=NULL;
    TVirtualConfig *chip=NULL;

//device name
    devname=GetCmdParam("-devname",true);
    if (!batchmode)
    {
        if (devname)
            printf("Using device %s\n",devname);
        else printf("Using standard device\n");
    }

///interface type
    if ((value=GetCmdParam("-iftype",true)))
    {
	#ifndef NO_SERIAL
        if (strcmp(value,"usb")==0)
        {
            printf("Using tty interface\n");
            iface=new TSerialInterface(devname);
        }
	#endif
	
	#ifndef NO_UDP
        if (strcmp(value,"udp")==0)
        {
            printf("Using udp interface\n");
            iface=new TUDPInterface(devname);
        }
	#endif
    }
    else
    {
	#ifndef NO_SERIAL
        printf("Using USB interface\n");
        iface=new TSerialInterface(devname);
	#endif
    }

//interface ready?
    if (!iface)
    {
        printf("No interface selected, Exiting.\n");
        exit(-1);
    }
	
    if (iface && !iface->IsReady())
    {
        printf("Interface is not Ready, Exiting.\n");
        delete iface;
        exit(-1);
    }


//configuration type
    if ((value=GetCmdParam("-chip",true)))
    {
#ifndef NO_KLAUS2
        if (strcmp(value,"klaus2")==0)
        {
            chip=new TKlaus2Config(iface);
        }
#endif
#ifndef NO_STIC2
        if (strcmp(value,"stic2")==0)
        {
            chip=new TSTiC2Config(iface);
        }
#endif
    }
   else
    {
	#ifndef NO_STIC2
        printf("Using STiC2 Pattern set\n");
        chip=new TSTiC2Config(iface);
	#endif
    }


    if(!chip){
	printf("No chip type selected, exiting");
    }

//input file name
    const char *ifilename;
    if ((ifilename=GetCmdParam("-if",true)))
        chip->ReadFromFile(ifilename,false);
    else
        chip->ReadFromFile("standard_tokens.txt",false);



    int rwop=1;
    if ((value=GetCmdParam("-Nrwop",true)))
        sscanf(value,"%d",&rwop);

//just do writing

        while(rwop-- > 0)
            chip->UpdateConfig();


        chip->Print(batchmode);

    delete chip;
    delete iface;
    return 100;
}
