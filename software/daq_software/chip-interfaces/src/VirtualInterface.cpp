
#include <stdio.h>
#include "string.h"
#include "VirtualInterface.h"


TVirtualInterface::TVirtualInterface(int nAttacheable)
    {
	fd=-1;
        isReady = 0;
        nAttached = 0;
	nAttable =nAttacheable;
	attachedHandles=new bool[nAttacheable];
	for (int i=0;i<=nAttacheable;i++)
		attachedHandles[i]=false; 
	tv_timeout.tv_sec=0;
	tv_timeout.tv_usec=500000;
	retries=2;
    }
    ;

TVirtualInterface::~TVirtualInterface()
    {
        //printf("VI destruct\n");
        if (nAttached != 0)
            printf(
                "Warnung: Interface wird gelöscht, aber es sind noch TConfigurations verbunden.\n");
	delete [] attachedHandles;
    }
    ;

int TVirtualInterface::AttachHandle()
    {
	//find first multiplexing handle that is free
	for (int i=0; i<nAttable;i++)
		if (attachedHandles[i]==false){
			attachedHandles[i]=true;
			nAttached++;
			return i;
		}
	
        return -1;
    }
    ;

int TVirtualInterface::DetachHandle(int ifaceHandID)
    {
	if ((ifaceHandID < nAttable) && (ifaceHandID>0))
		attachedHandles[ifaceHandID]=false;
        return --nAttached;
    }
    ;


int TVirtualInterface::SetTimeout(long tv_sec,long tv_usec, int Nretry){
	tv_timeout.tv_sec=tv_sec;
	tv_timeout.tv_usec=tv_usec;
	retries=Nretry;
	return 0;
    };


/*
    virtual int read_Reply(unsigned int ifaceHandID,int reply_len, char* reply)=0;
    virtual int send_cmd(int cmd, unsigned int ifaceHandID,int len, char* data)=0;
*/

int TVirtualInterface::Command(unsigned int ifaceHandID, const char cmd, int len, char* data, int reply_len, char* reply){
	int op_left=retries;
	if(reply_len<0)		//no reply expected
		op_left=-1;



	struct timeval tv;	
	fd_set rfds;
	int retval;
	
	/*send a command*/
	if (send_cmd(cmd,ifaceHandID,len,data) <0)
		return -2;	//writing command failed

	if (op_left<0)
		return 0;	//no reply

	/*wait for reply*/
	while(1){
		memcpy(&tv, &tv_timeout,sizeof(struct timeval));
		FD_ZERO(&rfds);
		FD_SET(fd, &rfds);
		retval = select(fd+1, &rfds, NULL, NULL, &tv);
		if (retval == -1){
			perror("VirtualInterface::ReplyCmd::select()");
			return -2;
		

		/*Timout, retry*/
		}else if(retval==0){
			if(op_left < 0){
				printf("VirtualInterface::ReplyCmd: Final Timeout\n");
				return -1;
			}else{
				printf("VirtualInterface::ReplyCmd: Timeout + Retry (%d)\n",op_left);
				if (send_cmd(cmd,ifaceHandID,len,data) <0)
					return -2;	//writing command failed
			}

		
		/*got reply*/
		}else if( retval &&(FD_ISSET(fd,&rfds)) ){
    			retval = read_Reply(ifaceHandID,reply_len,reply);
			if(retval>=0){
				return 0;
			}else if (retval==-2)
				return -2;
			//else: read_Reply not for this handle
		}
		op_left--;
	}
	return -1;
}


