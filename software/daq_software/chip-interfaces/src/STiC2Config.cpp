/*
 * STiC2Config.cpp
 *
 *  Created on: 29.09.2011
 *      Author: Konrad
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/STiC2Config.h"
#include "../include/VirtualConfig.h"
#include "../include/VirtualInterface.h"

TSTiC2Config::TSTiC2Config(TVirtualInterface* Interface)
    :TVirtualConfig(Interface,par_descriptor,203,111)
{
    bitpattern_read =  new char[111];
    bitpattern_write = new char[111];
    memset(bitpattern_read,0,sizeof(char) * 111);
    memset(bitpattern_read,0,sizeof(char) * 111);
};

TSTiC2Config::~TSTiC2Config()
{
    delete [] bitpattern_read;
    delete [] bitpattern_write;
};
//
void TSTiC2Config::ChipReset(){
    Iface->Command(ifaceHandID,'R',0, NULL);
}
void TSTiC2Config::ChannelReset(){
    Iface->Command(ifaceHandID,'r',0, NULL);
}


TVirtualConfig::parameter const TSTiC2Config::par_descriptor[]=
{

	{"O_GEN_IDLE_SIGNAL",0,	1},\
	{"TEMP12_BIT_DAC",1,	0},\
	{"O_VMON_CTL_CH0",13,	1},\
	{"O_IMON_CTL_CH0",17,	1},\
	{"O_DAC_CH0",19,	1},\
	{"O_DAC_TTHRESH_CH0",25,	1},\
	{"O_DAC_TBIAS_CH0",29,	1},\
	{"O_DAC_EBIAS_CH0",33,	1},\
	{"O_DAC_ETHRESH_CH0",37,	1},\
	{"O_LADDER_DAC_CH0",41,	1},\
	{"O_LADDER_INPUTBIAS_CH0",47,	1},\
	{"O_LADDER_ICOMP_CH0",53,	1},\
	{"O_CHANNEL_MASK_CH0",59,	1},\
	{"O_VMON_CTL_CH1",60,	1},\
	{"O_IMON_CTL_CH1",64,	1},\
	{"O_DAC_CH1",66,	1},\
	{"O_DAC_TTHRESH_CH1",72,	1},\
	{"O_DAC_TBIAS_CH1",76,	1},\
	{"O_DAC_EBIAS_CH1",80,	1},\
	{"O_DAC_ETHRESH_CH1",84,	1},\
	{"O_LADDER_DAC_CH1",88,	1},\
	{"O_LADDER_INPUTBIAS_CH1",94,	1},\
	{"O_LADDER_ICOMP_CH1",100,	1},\
	{"O_CHANNEL_MASK_CH1",106,	1},\
	{"O_VMON_CTL_CH2",107,	1},\
	{"O_IMON_CTL_CH2",111,	1},\
	{"O_DAC_CH2",113,	1},\
	{"O_DAC_TTHRESH_CH2",119,	1},\
	{"O_DAC_TBIAS_CH2",123,	1},\
	{"O_DAC_EBIAS_CH2",127,	1},\
	{"O_DAC_ETHRESH_CH2",131,	1},\
	{"O_LADDER_DAC_CH2",135,	1},\
	{"O_LADDER_INPUTBIAS_CH2",141,	1},\
	{"O_LADDER_ICOMP_CH2",147,	1},\
	{"O_CHANNEL_MASK_CH2",153,	1},\
	{"O_VMON_CTL_CH3",154,	1},\
	{"O_IMON_CTL_CH3",158,	1},\
	{"O_DAC_CH3",160,	1},\
	{"O_DAC_TTHRESH_CH3",166,	1},\
	{"O_DAC_TBIAS_CH3",170,	1},\
	{"O_DAC_EBIAS_CH3",174,	1},\
	{"O_DAC_ETHRESH_CH3",178,	1},\
	{"O_LADDER_DAC_CH3",182,	1},\
	{"O_LADDER_INPUTBIAS_CH3",188,	1},\
	{"O_LADDER_ICOMP_CH3",194,	1},\
	{"O_CHANNEL_MASK_CH3",200,	1},\
	{"O_VMON_CTL_CH4",201,	1},\
	{"O_IMON_CTL_CH4",205,	1},\
	{"O_DAC_CH4",207,	1},\
	{"O_DAC_TTHRESH_CH4",213,	1},\
	{"O_DAC_TBIAS_CH4",217,	1},\
	{"O_DAC_EBIAS_CH4",221,	1},\
	{"O_DAC_ETHRESH_CH4",225,	1},\
	{"O_LADDER_DAC_CH4",229,	1},\
	{"O_LADDER_INPUTBIAS_CH4",235,	1},\
	{"O_LADDER_ICOMP_CH4",241,	1},\
	{"O_CHANNEL_MASK_CH4",247,	1},\
	{"O_VMON_CTL_CH5",248,	1},\
	{"O_IMON_CTL_CH5",252,	1},\
	{"O_DAC_CH5",254,	1},\
	{"O_DAC_TTHRESH_CH5",260,	1},\
	{"O_DAC_TBIAS_CH5",264,	1},\
	{"O_DAC_EBIAS_CH5",268,	1},\
	{"O_DAC_ETHRESH_CH5",272,	1},\
	{"O_LADDER_DAC_CH5",276,	1},\
	{"O_LADDER_INPUTBIAS_CH5",282,	1},\
	{"O_LADDER_ICOMP_CH5",288,	1},\
	{"O_CHANNEL_MASK_CH5",294,	1},\
	{"O_VMON_CTL_CH6",295,	1},\
	{"O_IMON_CTL_CH6",299,	1},\
	{"O_DAC_CH6",301,	1},\
	{"O_DAC_TTHRESH_CH6",307,	1},\
	{"O_DAC_TBIAS_CH6",311,	1},\
	{"O_DAC_EBIAS_CH6",315,	1},\
	{"O_DAC_ETHRESH_CH6",319,	1},\
	{"O_LADDER_DAC_CH6",323,	1},\
	{"O_LADDER_INPUTBIAS_CH6",329,	1},\
	{"O_LADDER_ICOMP_CH6",335,	1},\
	{"O_CHANNEL_MASK_CH6",341,	1},\
	{"O_VMON_CTL_CH7",342,	1},\
	{"O_IMON_CTL_CH7",346,	1},\
	{"O_DAC_CH7",348,	1},\
	{"O_DAC_TTHRESH_CH7",354,	1},\
	{"O_DAC_TBIAS_CH7",358,	1},\
	{"O_DAC_EBIAS_CH7",362,	1},\
	{"O_DAC_ETHRESH_CH7",366,	1},\
	{"O_LADDER_DAC_CH7",370,	1},\
	{"O_LADDER_INPUTBIAS_CH7",376,	1},\
	{"O_LADDER_ICOMP_CH7",382,	1},\
	{"O_CHANNEL_MASK_CH7",388,	1},\
	{"O_INDAC",389,	1},\
	{"O_CML_DAC",395,	1},\
	{"O_DISABLE_COARSE",401,	1},\
	{"O_VMON_CTL_CH8",402,	1},\
	{"O_IMON_CTL_CH8",406,	1},\
	{"O_DAC_CH8",408,	1},\
	{"O_DAC_TTHRESH_CH8",414,	1},\
	{"O_DAC_TBIAS_CH8",418,	1},\
	{"O_DAC_EBIAS_CH8",422,	1},\
	{"O_DAC_ETHRESH_CH8",426,	1},\
	{"O_LADDER_DAC_CH8",430,	1},\
	{"O_LADDER_INPUTBIAS_CH8",436,	1},\
	{"O_LADDER_ICOMP_CH8",442,	1},\
	{"O_CHANNEL_MASK_CH8",448,	1},\
	{"O_VMON_CTL_CH9",449,	1},\
	{"O_IMON_CTL_CH9",453,	1},\
	{"O_DAC_CH9",455,	1},\
	{"O_DAC_TTHRESH_CH9",461,	1},\
	{"O_DAC_TBIAS_CH9",465,	1},\
	{"O_DAC_EBIAS_CH9",469,	1},\
	{"O_DAC_ETHRESH_CH9",473,	1},\
	{"O_LADDER_DAC_CH9",477,	1},\
	{"O_LADDER_INPUTBIAS_CH9",483,	1},\
	{"O_LADDER_ICOMP_CH9",489,	1},\
	{"O_CHANNEL_MASK_CH9",495,	1},\
	{"O_VMON_CTL_CH10",496,	1},\
	{"O_IMON_CTL_CH10",500,	1},\
	{"O_DAC_CH10",502,	1},\
	{"O_DAC_TTHRESH_CH10",508,	1},\
	{"O_DAC_TBIAS_CH10",512,	1},\
	{"O_DAC_EBIAS_CH10",516,	1},\
	{"O_DAC_ETHRESH_CH10",520,	1},\
	{"O_LADDER_DAC_CH10",524,	1},\
	{"O_LADDER_INPUTBIAS_CH10",530,	1},\
	{"O_LADDER_ICOMP_CH10",536,	1},\
	{"O_CHANNEL_MASK_CH10",542,	1},\
	{"O_VMON_CTL_CH11",543,	1},\
	{"O_IMON_CTL_CH11",547,	1},\
	{"O_DAC_CH11",549,	1},\
	{"O_DAC_TTHRESH_CH11",555,	1},\
	{"O_DAC_TBIAS_CH11",559,	1},\
	{"O_DAC_EBIAS_CH11",563,	1},\
	{"O_DAC_ETHRESH_CH11",567,	1},\
	{"O_LADDER_DAC_CH11",571,	1},\
	{"O_LADDER_INPUTBIAS_CH11",577,	1},\
	{"O_LADDER_ICOMP_CH11",583,	1},\
	{"O_CHANNEL_MASK_CH11",589,	1},\
	{"O_VMON_CTL_CH12",590,	1},\
	{"O_IMON_CTL_CH12",594,	1},\
	{"O_DAC_CH12",596,	1},\
	{"O_DAC_TTHRESH_CH12",602,	1},\
	{"O_DAC_TBIAS_CH12",606,	1},\
	{"O_DAC_EBIAS_CH12",610,	1},\
	{"O_DAC_ETHRESH_CH12",614,	1},\
	{"O_LADDER_DAC_CH12",618,	1},\
	{"O_LADDER_INPUTBIAS_CH12",624,	1},\
	{"O_LADDER_ICOMP_CH12",630,	1},\
	{"O_CHANNEL_MASK_CH12",636,	1},\
	{"O_VMON_CTL_CH13",637,	1},\
	{"O_IMON_CTL_CH13",641,	1},\
	{"O_DAC_CH13",643,	1},\
	{"O_DAC_TTHRESH_CH13",649,	1},\
	{"O_DAC_TBIAS_CH13",653,	1},\
	{"O_DAC_EBIAS_CH13",657,	1},\
	{"O_DAC_ETHRESH_CH13",661,	1},\
	{"O_LADDER_DAC_CH13",665,	1},\
	{"O_LADDER_INPUTBIAS_CH13",671,	1},\
	{"O_LADDER_ICOMP_CH13",677,	1},\
	{"O_CHANNEL_MASK_CH13",683,	1},\
	{"O_VMON_CTL_CH14",684,	1},\
	{"O_IMON_CTL_CH14",688,	1},\
	{"O_DAC_CH14",690,	1},\
	{"O_DAC_TTHRESH_CH14",696,	1},\
	{"O_DAC_TBIAS_CH14",700,	1},\
	{"O_DAC_EBIAS_CH14",704,	1},\
	{"O_DAC_ETHRESH_CH14",708,	1},\
	{"O_LADDER_DAC_CH14",712,	1},\
	{"O_LADDER_INPUTBIAS_CH14",718,	1},\
	{"O_LADDER_ICOMP_CH14",724,	1},\
	{"O_CHANNEL_MASK_CH14",730,	1},\
	{"O_VMON_CTL_CH15",731,	1},\
	{"O_IMON_CTL_CH15",735,	1},\
	{"O_DAC_CH15",737,	1},\
	{"O_DAC_TTHRESH_CH15",743,	1},\
	{"O_DAC_TBIAS_CH15",747,	1},\
	{"O_DAC_EBIAS_CH15",751,	1},\
	{"O_DAC_ETHRESH_CH15",755,	1},\
	{"O_LADDER_DAC_CH15",759,	1},\
	{"O_LADDER_INPUTBIAS_CH15",765,	1},\
	{"O_LADDER_ICOMP_CH15",771,	1},\
	{"O_CHANNEL_MASK_CH15",777,	1},\
	{"O_VMON_CTL_CH16",778,	1},\
	{"O_IMON_CTL_CH16",782,	1},\
	{"O_DAC_CH16",784,	1},\
	{"O_DAC_TTHRESH_CH16",790,	1},\
	{"O_DAC_TBIAS_CH16",794,	1},\
	{"O_DAC_EBIAS_CH16",798,	1},\
	{"O_DAC_ETHRESH_CH16",802,	1},\
	{"O_LADDER_DAC_CH16",806,	1},\
	{"O_LADDER_INPUTBIAS_CH16",812,	1},\
	{"O_LADDER_ICOMP_CH16",818,	1},\
	{"O_CHANNEL_MASK_CH16",824,	1},\
	{"O_TESTCML_DAC",825,	1},\
	{"O_SELECT_TDCTEST",831,	1},\
	{"O_VN_D2C",832,		1},\
	{"O_VN_StampLat",838,		1},\
	{"O_VNCntBuffer",844,		1},\
	{"O_VNCnt",850,			1},\
	{"O_VPCP",856,			1},\
	{"O_VNVCODelay",862,		1},\
	{"O_VNVCOBuffer",868,		1},\
	{"O_VNHitLogic",874,		1},\
	{"O_DAC_PFC",880,		1},\
	{"",886,0}
};


