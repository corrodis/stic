/*
 * klaus2Config.h
 *
 *  Created on: 29.09.2011
 *      Author: Konrad
 */

#include "VirtualConfig.h"
#include "VirtualInterface.h"
#include "SerialBridge.h"

TSerialBridge::TSerialBridge(TVirtualInterface* Interface)
:TVirtualConfig(Interface,NULL,0,0){}

int TSerialBridge::Read(char* data, int len){
    return Iface->Command(ifaceHandID,'s',len, data, len, data);
}
int TSerialBridge::Write(char* data, int len){
    return Iface->Command(ifaceHandID,'s',len, data, len, data);
}
