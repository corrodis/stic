/*g
 Beschreibungsklasse fuer Interface mit einem SiPM-Chip (KlAus, StiC)
 rein virtuell, Bitpattern einlese, auslese je nach typ
 */

#ifndef __VIRTUAL_CONFIG_H
#define __VIRTUAL_CONFIG_H
#include <stdio.h>
#include <stdlib.h>


#include "VirtualInterface.h"

#define MAX_PARLENGTH 50

class TVirtualConfig
{

protected:
    struct parameter
    {
        const char *name;
        int offset;
	bool endianess; //0: little endian
			//1: big endian
        //parameter(char * const n,int o):name(n),offset(o){};
    };

    //static data members
    parameter * parameters;
    const short int nParameters;
    const short int patternlength;
    //pattern read / to write
    char *bitpattern_read;
    char *bitpattern_write;


private:


//verbindung zu bitpattern
    void GetFromPattern(char * const pattern , int N,unsigned long long& value) const;
    void SetInPattern(char * const pattern , int N,unsigned long long const& value) const;

protected:
    //Interface
    TVirtualInterface *Iface;
    unsigned int ifaceHandID;

    //instanziieren mit der richtigen anzahl an parametern, *_conf sind erzeugt.
    //ist protected um instanziieren zu verhindern.
    TVirtualConfig(TVirtualInterface* Interface,
                   const void* pars, const short int nParameters, const short int patternlength);


public:

    virtual ~TVirtualConfig();

    int UpdateConfig();
    /*
	Possible returns:
	1:  all good
	0:  command ok, but returned bitpattern not correct
	-1: communication timeout
	-2: communication error

    */

    //parametername
    const char* GetParName(short int n) const;
    const short int GetNParameters()const
    {
        return nParameters;
    } ;
    short int GetParID(const char* name) const;

    const int ChannelFromID(short int n) const;
//Parameter Setters+Getters
///////////////////////////


    /** @brief Get Parameter (read)
      * Return: number of parameter, -1 if not in range/found
      */
    int GetParValueRD(short int npar,unsigned long long &value) const;
    /** @brief Get Parameter (read)
      * Return: number of parameter, -1 if not in range/found
      */
    int GetParValueRD(const char *name,unsigned long long &value) const;

    /** @brief Get Parameter (to be written)
      * Return: number of parameter, -1 if not in range/found
      */
    int GetParValueWR(short int npar,unsigned long long &value) const;
    /** @brief Get Parameter (to be written)
      * Return: number of parameter, -1 if not in range/found
      */
    int GetParValueWR(const char *name,unsigned long long &value) const;




    /** @brief Set Parameter (read)
      * Return: number of parameter, -1 if not in range/found
      */
    int SetParValue(short int npar,unsigned long long value);
    /** @brief Set Parameter in current_conf (read)
      * Return: number of parameter, -1 if not in range/found
      */
    int SetParValue(const char *name,unsigned long long value);

    //lese datei, schreibe in new_conf
    int ReadFromFile(const char *fname,bool write=false);

    virtual void Print(bool batch,const char* ofilename=NULL) const;

};

#endif
