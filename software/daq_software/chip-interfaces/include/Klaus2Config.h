/*
 * klaus2Config.h
 *
 *  Created on: 29.09.2011
 *      Author: Konrad
 */
#ifndef KLAUS2CONFIG_H__
#define KLAUS2CONFIG_H__


#include "VirtualConfig.h"
#include "VirtualInterface.h"



class TKlaus2Config: public TVirtualConfig
{
private:
//konfiguration
    static parameter const par_descriptor[];



public:
    TKlaus2Config(TVirtualInterface* Interface);
    ~TKlaus2Config();

};


#endif
