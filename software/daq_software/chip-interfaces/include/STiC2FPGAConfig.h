/*
 * STiC2FPGAConfig.h
 *
 *  Created on: 
 *	Wed Nov  6 17:59:26 CET 2013
 *      Author: Tobias
 *      
 *      Class that handles the configuration of the FPGA, keeping it seperate from a Chip configuration
 */
#ifndef STIC2FPGACONFIG_H__
#define STIC2FPGACONFIG_H__


#include "VirtualConfig.h"
#include "VirtualInterface.h"



class TSTiC2FPGAConfig
{
private:
//konfiguration
	char deser_mask;				//Bit pattern masking the individual deserializers for the readout. 

protected:
    //Interface
    TVirtualInterface *Iface;

public:
    TSTiC2FPGAConfig(TVirtualInterface* Interface);
    ~TSTiC2FPGAConfig();
	void UpdateDeserEnable(unsigned short deser, unsigned short value);
	void UpdateDAQConfig();
	bool GetActive(unsigned short deser);

};

#endif
