/*
 * klaus2Config.h
 *
 *  Created on: 29.09.2011
 *      Author: Konrad
 */
#ifndef STIC2CONFIG_H__
#define STIC2CONFIG_H__


#include "VirtualConfig.h"
#include "VirtualInterface.h"



class TSTiC2Config: public TVirtualConfig
{
private:
//konfiguration
    static parameter const par_descriptor[];



public:
    TSTiC2Config(TVirtualInterface* Interface);
    ~TSTiC2Config();
    void ChipReset();
    void ChannelReset();

};

#endif
