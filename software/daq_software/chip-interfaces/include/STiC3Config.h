/*
 * STiC3Config.h
 *
 *  Created on: Fri Jan  3 14:47:45 CET 2014
 *      Author: Tobias
 */
#ifndef STIC2CONFIG_H__
#define STIC2CONFIG_H__


#include "VirtualConfig.h"
#include "VirtualInterface.h"



class TSTiC3Config: public TVirtualConfig
{
private:
//konfiguration
    static parameter const par_descriptor[];



public:
    TSTiC3Config(TVirtualInterface* Interface);
    ~TSTiC3Config();
    void ChipReset();
    void ChannelReset();

};

#endif
