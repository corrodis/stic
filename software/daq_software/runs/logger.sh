#!/bin/bash

# Script that manages all the logging for the STiC-Ge setup
# Usage: ./logger.sh [log interval] [log time] 
# If it is run withoaut an argument, it goes on forever
# log interval: time interval between two loggs
# log time: over all time that the logger should run
# 
# Simon Corrodi, corrodis@phys.ethz.ch, June 2015

# Loop
start=$( date +%s )
while true;
    do	
	# Read Voltage
	volts=$( ssh pceth126 "sudo expect ~/Documents/gpib/gpib.exp" | grep "received string: '" | sed -e "s/received string: '+//" | sed -e 's/E/e/' )

	# Output
	echo $(date +%s) $(( $(date +%s) - start )) $volts >> logger.log
	echo $(date +%s) $(( $(date +%s) - start )) $volts 

	if [ $# -gt 0 ]
	   then 
		sleep $1
	   else
		sleep 30
	fi
	
	if [ $# -gt 1 ]
	    then
		if [ $(( $(date +%s) - start )) -gt $2 ]
	    	    then
			break
		fi
	fi
done
