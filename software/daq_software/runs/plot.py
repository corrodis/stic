# coding: utf-8
import numpy as np
m = np.loadtxt("logger.log")
import datetime
t = [datetime.datetime.fromtimestamp(ts) for ts in m[:,0]]
import matplotlib.pyplot as plt
plt.plot(t,m[:,2])
plt.grid()
plt.xlabel("time")
plt.ylabel("Voltage [V]")
plt.show()
