/*
* GUI class to handle the signals from the main window, changing the underlying TVirtualConfig parameter set.
* it can control several VirtualConfig instances, by using a "Chip-Channel-Scope" to adress the different asics.
*
* Also includes some tools to copy configuration data from one channel to another, and communication with a parent-like window 
* (e.g. a System overview window for a combined ctrl&daq framework)
*
* Author: kbriggl
* Date:   Feb/13
*
*/


#include <gtk/gtk.h>
#include <string.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "STiC2Gui.h"
#include "expand.h"

#include "resources.h"
#include "HandlerProxies.h"
//using namespace std;



//gui contructor
TSTiC2gui::TSTiC2gui(int nChips):
nChips(nChips)
{
	builder = gtk_builder_new ();
	//gtk_builder_add_from_file (builder, "stic2_gui.gtk", NULL);
	gtk_builder_add_from_resource (builder, "/stic2/configuration/glade_xml/config_window.gtk", NULL);
	window = 		GTK_WIDGET (	gtk_builder_get_object (builder, "config_window"));
	GtkAdjustment * adj = 	GTK_ADJUSTMENT( gtk_builder_get_object (builder, "adj_chip"));
	gtk_adjustment_set_upper(adj,nChips-1);

	gtk_builder_connect_signals_full( builder , TSTiC2gui::connection_mapper, (gpointer)this);

	scope.chip=0;
	scope.channel=-1;
	block_val_changed=false;
	config=new TSTiC2Config* [nChips];
	for(int i=0;i<nChips;i++)
		config[i]=NULL;
};


//gui wnd destructor
TSTiC2gui::~TSTiC2gui(){
	delete [] config;
};


/* connect the signals with the class' member function. 
 * This method has to be static in order to be callable by a standard c function pointer.
 * The this pointer is passed over the user data parameter
 */
void TSTiC2gui::connection_mapper (GtkBuilder *builder, GObject *object,
        const gchar *signal_name, const gchar *handler_name,
        GObject *connect_object, GConnectFlags flags, gpointer user_data)
{	
	gpointer this__=user_data;
	if(g_strcmp0 (handler_name, "on_window_destroy") == 0){
		g_signal_connect(object,signal_name,
    				G_MEMBER_CALLBACK1( TSTiC2gui, on_window_close, GObject*),  this__);
		return;
	}

	if(g_strcmp0 (handler_name, "on_btn_clicked") == 0){
		g_signal_connect(object,signal_name,
    				G_MEMBER_CALLBACK1(TSTiC2gui, on_btn_clicked , GObject*),  this__);
		return;
	}
	if(g_strcmp0 (handler_name, "on_val_changed") == 0){
		g_signal_connect(object,signal_name,
    				G_MEMBER_CALLBACK1(TSTiC2gui, on_val_changed, GObject*),  this__);
		return;
	}
	if(g_strcmp0 (handler_name, "on_scope_changed") == 0){
		g_signal_connect(object,signal_name,
    				G_MEMBER_CALLBACK1(TSTiC2gui, on_scope_changed , GObject* ),  this__);
		return;
	}
	printf("!! Unhandled signal: %s to %s\n",signal_name, handler_name);
}



void TSTiC2gui::CopyChanInfo(PattExp::copyset &cset){
	char cpy[255];
	char* pos;

	unsigned long long val;
	PattExp::copyset::iterator it;

	TVirtualConfig *thisConfig=config[scope.chip];
	for (short int srcparN=0; srcparN<thisConfig->GetNParameters(); srcparN++){
		strcpy(cpy,thisConfig->GetParName(srcparN));
		pos=strrchr(cpy,'_');

		if(pos==NULL)
			continue;
		
		int curr_chan=thisConfig->ChannelFromID(srcparN);
		if(curr_chan>scope.channel)
			break;
		if(curr_chan==scope.channel){
		//	printf("src=%s\n",cpy);
			for (it=cset.begin(); it!=cset.end(); ++it){	
				//sprintf(pos,"_CH%d\0",it->channel);
				sprintf(pos,"_CH%d",it->channel);
		//		printf("copy to %s\n",cpy);
				thisConfig->GetParValueWR(srcparN,val);
				config[it->chip]->SetParValue(cpy,val);
			}
		}
	}
}




/* (GUI-)User wants to select a different chip/channel or the general config page*/
void TSTiC2gui::on_scope_changed (GObject *object){
	std::string obj_name=(char*)gtk_buildable_get_name((GtkBuildable*)object);
	int channel=scope.channel;
	int chip=scope.chip;

	if(obj_name.compare("sc_chip")==0){
		//chip changed
		chip=  	gtk_range_get_value( (GtkRange*) object);
	}
	else
       	if(obj_name.compare("sel_config_gen")==0){
		//general chip parameters page selected
		if(gtk_toggle_button_get_active((GtkToggleButton*)object)==true)
			channel=-1;
		else return;
	}else 
       	if(obj_name.compare("sel_config_chan")==0){
		//channel parameters page selected
		if(gtk_toggle_button_get_active((GtkToggleButton*)object)==true){
			GtkRange* rang=(GtkRange*)gtk_builder_get_object(builder,"sc_chan");
			channel=  gtk_range_get_value( (GtkRange*) rang);	
		}else return;
	}else 
	if(obj_name.compare("sc_chan")==0){
                //channel changed
		channel=  gtk_range_get_value( (GtkRange*) object);	
        }
	printf("change scope to %d %d\n",chip,channel);
	ChangeScope(chip,channel);
}


/* Select a different chip/channel or the general config page, update all parameters
   update all widgets to show the current channel's chip's settings and old values
   after the scope has been changed by the user
*/
void TSTiC2gui::ChangeScope(int chip, int channel){
	scope.chip=chip;
	scope.channel=channel;
	UpdateScope();
}


void TSTiC2gui::UpdateScope(){
	//update selection widgets
	GtkRange* rang=(GtkRange*)gtk_builder_get_object(builder,"sc_chip");
	gtk_range_set_value( (GtkRange*) rang, scope.chip);
	
	if(config[scope.chip]==NULL){
		printf("configuration is NULL!");
		return;
	}
	
	if(scope.channel==-1){
		GtkToggleButton* btn=(GtkToggleButton*)gtk_builder_get_object(builder,"sel_config_gen");
		gtk_toggle_button_set_active(btn,true);
	}else{
		GtkToggleButton* btn=(GtkToggleButton*)gtk_builder_get_object(builder,"sel_config_chan");
		gtk_toggle_button_set_active(btn,true);
		GtkRange* rang=(GtkRange*)gtk_builder_get_object(builder,"sc_chan");
		gtk_range_set_value( (GtkRange*) rang, scope.channel);	
		//change copy_src label
		GtkLabel* lab=(GtkLabel*)gtk_builder_get_object(builder,"cbox_srclabel");
		char ltext[10];
		sprintf(ltext,"%d:%d",scope.chip,scope.channel);
		gtk_label_set_text(lab,ltext);
	}
	//update page widgets
	unsigned long long value;
	std::string obj_name;
	char valuestr[20];
	char* object_type_name;
	GObject* obj=NULL;

	block_val_changed=true;//block the value_changed signal

	for (short int npar=0; npar<config[scope.chip]->GetNParameters(); npar++){
		if(config[scope.chip]->ChannelFromID(npar) != scope.channel)
			continue;
		//get object name
		obj_name=config[scope.chip]->GetParName(npar);
		if (scope.channel!=-1){//remove channel number from parameter name
			obj_name=config[scope.chip]->GetParName(npar);
			obj_name.erase(obj_name.find_last_of('_')+3);		//TODO: change in xml, remove "_CH" 
		}

		obj=gtk_builder_get_object(builder,obj_name.c_str());
		if (obj==NULL){
			printf("ERROR: could not find object for parameter %s! (%s)\n",config[scope.chip]->GetParName(npar), obj_name.c_str());
			continue;
		}
		//get object type to set their value
		config[scope.chip]->GetParValueWR(npar,value);
		object_type_name=(char*)gtk_widget_get_name((GtkWidget*)obj);
		if (strcmp(object_type_name,"GtkCheckButton")==0){
			gtk_toggle_button_set_active((GtkToggleButton*)obj,value);
		}else 
    		if (strcmp(object_type_name,"GtkComboBoxText")==0){
			gtk_combo_box_set_active((GtkComboBox*)obj,(value==0)?0:log2(value));
		}else	
		if (strcmp(object_type_name,"GtkSpinButton")==0){
			gtk_spin_button_set_value((GtkSpinButton*)obj,value);
		}else{
			printf("ERROR: unrecognized type of GtkObject (%s)\n",object_type_name);
		}

		//get&set old val label
		obj_name.insert(0,"old_");
		obj=gtk_builder_get_object(builder,obj_name.c_str());
		config[scope.chip]->GetParValueRD(npar,value);
		sprintf(valuestr,"Old: %lld",value);
		if (obj==NULL)
			printf("ERROR: could not find label for parameter %s!\n",config[scope.chip]->GetParName(npar));
		else
			   gtk_label_set_text((GtkLabel*) obj,valuestr);
	}

	//Check the state of the deserializer in the fpga daq config
	obj=gtk_builder_get_object(builder,"btn_ena_readout");
	printf("Value of the deserializer active: %u\n",daq_config->GetActive(scope.chip));
	gtk_toggle_button_set_active((GtkToggleButton*)obj,daq_config->GetActive(scope.chip));

		
	//get notebook
	GtkNotebook *nb=(GtkNotebook*)gtk_builder_get_object(builder,"config_notebook");	
	gtk_notebook_set_current_page(nb, scope.channel==-1 ? 0:1);
	block_val_changed=false;//unblock the value_changed signal

	return;
}

//signal handlers
void TSTiC2gui::on_btn_clicked (GObject *object){
	//printf("TSTiC2gui::on_btn_clicked\n");
	//what button?	
	unsigned long long value;
	std::string btn_name=(char*)gtk_buildable_get_name((GtkBuildable*)object);

	if (btn_name.compare("btn_open")==0){	
	//"SAVE" button
		GtkWidget *dialog = gtk_file_chooser_dialog_new ("Open File",
						(GtkWindow*)window,
						GTK_FILE_CHOOSER_ACTION_OPEN,
						GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
						GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
						NULL);
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), "./");
		if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
		{
			char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
			config[scope.chip]->ReadFromFile(filename);
			g_free (filename);
			UpdateScope();	
		}
		gtk_widget_destroy (dialog);
	}else
	if (btn_name.compare("btn_save")==0){	
	//"OPEN" button
		GtkWidget *dialog = gtk_file_chooser_dialog_new ("Save File",
						(GtkWindow*)this->window,
						GTK_FILE_CHOOSER_ACTION_SAVE,
						GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
						GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
						NULL);
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), "./");
		char fname[30]; sprintf(fname,"STiC2Config_CHIP%d.txt",scope.chip);
		gtk_file_chooser_set_current_name   (GTK_FILE_CHOOSER (dialog), fname);
		if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
		{
			char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
			config[scope.chip]->Print(true,filename);
			g_free (filename);
		}
		gtk_widget_destroy (dialog);
	}else
	if (btn_name.compare("btn_update")==0){	
	//"UPDATE" button
		config[scope.chip]->UpdateConfig();
		config[scope.chip]->UpdateConfig();
		UpdateScope();	
	}else
	if (btn_name.compare("btn_update_all")==0){
	//"UPDATE ALL" button
		for (int c=0;c<nChips;c++){
			if(config[c]!=NULL){
				printf("____Configuring Chip #%d____\n",c);
				config[c]->UpdateConfig();
				config[c]->UpdateConfig();
			}
		}
		UpdateScope();
	}else
	if (btn_name.compare("btn_chipres")==0){	
	//"CHIP RESET" button
		printf("____Resetting Chip #%d\n",scope.chip);
		config[scope.chip]->ChipReset();
	}else
	if (btn_name.compare("btn_chanres")==0){	
	//"CHANNEL RESET" button
		printf("____Resetting Channels of Chip #%d\n",scope.chip);
		config[scope.chip]->ChannelReset();
	}else
	if (btn_name.compare("btn_copy")==0){	
	//"COPY" button
		GtkEntry* entr_dst=(GtkEntry*)gtk_builder_get_object(builder,"cbox_dst");
		if (entr_dst==NULL){
			printf("Error: could not find copy-dst entry\n");
			return;
		}
		gchar *strval;
		g_object_get (entr_dst,
		       "text", &strval,
		       NULL);
		PattExp::copyset cset;
		int ret = PattExp::Expand(strval,cset,scope.chip);
		if(ret<0){
			//error in parsing dst string occured
			char errpos[strlen(strval)];
			memset(errpos,' ',-ret-1);
			errpos[-ret]='^';
			errpos[-ret+1]='\0';
 			GtkWidget* dialog = gtk_message_dialog_new (GTK_WINDOW(this->window),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_ERROR,
				GTK_BUTTONS_OK,
				"Error Parsing string:\n%s\n%s",
				strval,errpos);
			gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_destroy (dialog);


			gtk_editable_select_region ((GtkEditable*) entr_dst,0,-1);
			return;
		}
		if( (cset.begin()->chip<0) || (cset.rbegin()->chip>nChips))
			return;
		printf("____Copy from %d:%d to:\n",scope.chip,scope.channel);
		PattExp::copyset::iterator it;
		for (it=cset.begin(); it!=cset.end(); ++it){
			printf("%d:%d\n",it->chip,it->channel);
		}
		//copy information
		CopyChanInfo(cset);
		
	}else
	if (btn_name.compare("btn_ena_readout")==0){
		value=gtk_toggle_button_get_active((GtkToggleButton*)object);
		printf("____Configuring Deserializer #%d____\n",scope.chip);
		daq_config->UpdateDeserEnable((unsigned short)scope.chip,(unsigned short) value);
	}
	else
		printf("configuration: unknown btn_clicked sender!\n");
	
};









void TSTiC2gui::on_val_changed (GObject *object)
{
	if(block_val_changed)
		return;

	char* object_type_name=(char*)gtk_widget_get_name((GtkWidget*)object);
	char name[30];

	if(scope.channel!=-1)
		sprintf(name,"%s%d",(char*)gtk_buildable_get_name((GtkBuildable*)object),scope.channel);
	else
		sprintf(name,"%s",(char*)gtk_buildable_get_name((GtkBuildable*)object));

	unsigned long long value;
	if (strcmp(object_type_name,"GtkCheckButton")==0){
		value=gtk_toggle_button_get_active((GtkToggleButton*)object);
	}else
	if (strcmp(object_type_name,"GtkComboBoxText")==0)
	{
		//currently, the combo boxes have the number in plain text as first charakter
		if (gtk_combo_box_text_get_active_text((GtkComboBoxText*)object)!=NULL)
			value=gtk_combo_box_text_get_active_text((GtkComboBoxText*)object)[0]-'0';
		else 
			value=0;
	}else
		if (strcmp(object_type_name,"GtkSpinButton")==0)
		{
			value=gtk_spin_button_get_value_as_int((GtkSpinButton*)object);
		}else{
			printf("ERROR: unrecognized type of GtkObject\n");
			return;
		}
		
		printf("new setting of Parameter %s = %llu\n",name,value);

		if (config[scope.chip]->SetParValue(name,value) < 0)
		{
			printf("ERROR: did not find Parameter \"%s\" in configuration\n",name);
		}
	
	
	
};


void TSTiC2gui::on_window_close(GObject *object){
	char filename[200];
	for (int c=0;c<nChips;c++){
		sprintf(filename,".lastquit_config_chip%d",c);
		config[c]->Print(true,filename);
	}
	gtk_main_quit();
}
