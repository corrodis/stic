

#include "STiC3Gui.h"
#include "STiC3Config.h"
#include "UDPInterface.h"
#include "MQInterface.h"

#include <gtk/gtk.h>

#define NCHIPS 2

int main (int argc, char *argv[])
{

	gtk_init (&argc, &argv);


	//builder->add_from_file("glade_xml/config_window.gtk");
	TSTiC2gui *gui_wnd = new TSTiC2gui(NCHIPS);

	TUDPInterface *interfaces[NCHIPS];
	TSTiC2Config *configurations[NCHIPS];



	interfaces[0]=new TUDPInterface("192.168.0.2");
	configurations[0]=new TSTiC2Config(interfaces[0]);
	printf("Chip1 on 192.168.0.2\n");


	interfaces[1]=new TUDPInterface("192.168.0.3");
	configurations[1]=new TSTiC2Config(interfaces[1]);
	printf("Chip1 on 192.168.0.3\n");



	interfaces[0]->SetTimeout(0,200000,2);
	interfaces[1]->SetTimeout(0,200000,2);

	configurations[0]->ReadFromFile("STiC2Config_CHIP0.txt");
	configurations[1]->ReadFromFile("STiC2Config_CHIP0.txt");


	gui_wnd->SetConfiguration(0, configurations[0]);
	gui_wnd->SetConfiguration(1, configurations[1]);

	gtk_widget_show (gui_wnd->GetWindow());
	gtk_main();
	


	delete configurations[0];
	delete configurations[1];
	delete interfaces[0];
	delete interfaces[1];


	//Shows the window and returns when it is closed.
	return 0;
}
