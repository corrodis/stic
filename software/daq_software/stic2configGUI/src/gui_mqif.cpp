

#include "STiC3Gui.h"
#include "STiC3Config.h"
#include "MQInterface.h"

#include <gtk/gtk.h>

#define NCHIPS 4

int main (int argc, char *argv[])
{

	gtk_init (&argc, &argv);


	//builder->add_from_file("glade_xml/config_window.gtk");
	TSTiC3gui *gui_wnd = new TSTiC3gui(NCHIPS);

	TMQInterface iface("DAQ");
	iface.SetTimeout(1,1000000,0);
	TSTiC3Config *configurations[NCHIPS];


	char filename[30];
	for (int i=0;i<NCHIPS;i++){
		configurations[i]=new TSTiC3Config(&iface);

		sprintf(filename,"STiC3Config_CHIP%d.txt",i);
		configurations[i]->ReadFromFile(filename);

		gui_wnd->SetConfiguration(i, configurations[i]);
	}


	//Set the interface for FPGA configurations
	gui_wnd->InitFPGAInterface(&iface);

	gtk_widget_show (gui_wnd->GetWindow());
	gtk_main();
	for (int i=0;i<NCHIPS;i++){
		delete configurations[i];
	}

	//Shows the window and returns when it is closed.
	return 0;
}
