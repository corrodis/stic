//SMALL PROGRAM CONFIGURING THE ASICs


#include "STiC3Config.h"
#include "STiC2FPGAConfig.h"
#include "MQInterface.h"
#include "STiC3DAQ.h"
#include "TThread.h"



#include <unistd.h>
#include <iostream>


#define NCHIPS 4


#ifdef DEBUG_MTRACE
#include <mcheck.h>
#endif

// Used for communication with STiC/FPGA
TSTiC3DAQ* daq_class;
TThread *th_daq;

int main (int argc, char *argv[])
{

#ifdef DEBUG_MTRACE
	mtrace();
#endif

	if (argc<2){
		std::cout << "Usage: ./cmdline_config <ConfigBaseFilename> [Chip Number]\n";
		return -1;
	}

	// Load DAQ for communication with the chip
	daq_class = new TSTiC3DAQ();
	TTree * tree= new TTree("dump","dump");
	daq_class->AddBranch(tree,"br");

	th_daq = new TThread("daq", (TThread::VoidRtnFunc_t) &(TSTiC3DAQ::reader_thread), daq_class);
	th_daq->Run();

	usleep(10000);
	std::cout << "Communication started" << std::endl;


	TMQInterface iface("DAQ");
	iface.SetTimeout(0,5000000,0);
	TSTiC3Config *configurations[NCHIPS];

	//The chip that should be configured
	int chip;
	if (argc == 3)
		chip=atoi(argv[2]);
	else
		chip=0;

	char filename[30];
	for (int i=0;i<NCHIPS;i++){
		configurations[i]=new TSTiC3Config(&iface);
		sprintf(filename,"%s%d.txt",argv[1],i);
		configurations[i]->ReadFromFile(filename);

	}

	int t = 0;

	if ( chip >= NCHIPS )
		std::cout << "The specified chip does not exist in the current setup!" << std::endl;
	else
		while(configurations[chip]->UpdateConfig()<0 && t < 10) {
			std::cout << "readback failed, re-tray no: " << t << std::endl;
			t++;
		}



	usleep(2000000);
//	configurations[0]->UpdateConfig();
//	usleep(2000000);

//	configurations[2]->UpdateConfig();
//	configurations[2]->UpdateConfig();
//	usleep(50000);

	configurations[0]->ChipReset();
	configurations[0]->ChannelReset();

//	TSTiC2FPGAConfig* fpga_config = new TSTiC2FPGAConfig(&iface);
//	fpga_config->UpdateDeserEnable(0,0);
//	fpga_config->UpdateDeserEnable(1,1);
//	fpga_config->UpdateDeserEnable(2,1);
//	fpga_config->UpdateDeserEnable(3,1);


	for (int i=0;i<NCHIPS;i++){
		delete configurations[i];
	}
	daq_class->PauseReadout();
	daq_class->StopReadout();
	th_daq->Join();
#ifdef DEBUG_MTRACE
	muntrace();
#endif

	//Shows the window and returns when it is closed.
	
	//usleep(1000000);
	return 0;
}
