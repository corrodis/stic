//SMALL PROGRAM RESETTING THE ASICs


#include "STiC2Config.h"
#include "STiC2FPGAConfig.h"
#include "MQInterface.h"

#define NCHIPS 1

int main (int argc, char *argv[])
{



	TMQInterface iface("DAQ");
	iface.SetTimeout(0,5000000,0);
	TSTiC2Config *configurations[NCHIPS];


	char filename[30];
	for (int i=0;i<NCHIPS;i++){
		configurations[i]=new TSTiC2Config(&iface);
		sprintf(filename,"STiC2Config_CHIP%d.txt",i);
		configurations[i]->ReadFromFile(filename);

	}

	//TWICE FOR SAFETY REASONS :)

	configurations[0]->ChipReset();
	configurations[0]->ChannelReset();

	for (int i=0;i<NCHIPS;i++){
		delete configurations[i];
	}


	//Shows the window and returns when it is closed.
	return 0;
}
