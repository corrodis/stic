/*
* GUI class to handle the signals from the main window, changing the underlying TVirtualConfig parameter set.
* it can control several VirtualConfig instances, by using a "Chip-Channel-Scope" to adress the different asics.
*
* Also includes some tools to copy configuration data from one channel to another, and communication with a parent-like window 
* (e.g. a System overview window for a combined ctrl&daq framework)
*
* Basically written in gtk+, although the class is derived from a gtkmm window widget.
* 
* Author: kbriggl
* Date:   Feb/13
*
*/




#ifndef  STIC2GUI_H__
#define  STIC2GUI_H__

#include <gtk/gtk.h>
#include <map>

//configuration class
#include "STiC2Config.h"
#include "STiC2FPGAConfig.h"
#include "expand.h"

class TSTiC2gui
{

public:
	TSTiC2gui(int nChips);

	virtual ~TSTiC2gui();


	static void connection_mapper (GtkBuilder *builder, GObject *object,
		const gchar *signal_name, const gchar *handler_name,
		GObject *connect_object, GConnectFlags flags, gpointer user_data);

	

	void SetConfiguration(int ID, TSTiC2Config* conf){
		if(ID>=0 && ID<nChips)
			config[ID]=conf;
	}

	void SetDAQConfiguration(TSTiC2FPGAConfig* daqconf){
		daq_config = daqconf;
	}

	/* Update the labels and settings in the newly selected scope. Called by on_scope_changed or parent */
	virtual void ChangeScope(int chip, int channel);



//signal handlers
	virtual void on_btn_clicked (GObject *object);
	virtual void on_val_changed (GObject *object  );
	virtual void on_scope_changed (GObject *object);
	virtual void on_window_close(GObject *object);
	
	GtkWidget* GetWindow(){ return GTK_WIDGET(window);};

protected:
	//copy channel configuration data
	void CopyChanInfo(PattExp::copyset &cset);

	virtual void UpdateScope();


	TSTiC2Config** config;
	TSTiC2FPGAConfig *daq_config;


	GtkBuilder      *builder; 
	GtkWidget	*window;
	struct{
		int chip;
		int channel;
	}scope;
	int nChips;
	bool block_val_changed;
};




#endif // STIC2GUI_H__
