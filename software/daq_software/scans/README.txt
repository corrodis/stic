######### How to performe proper CTR scans ##########

# Rate Measurments #
- Prepare Setup, MPPC without crystals
- power on StiC and HV
- run 0_setup.sh (switch everything off)
- run 1_scandark.sh CHNO FOLDERNAME VOLTAGE  of all channels you like to acquire dark rates 
- data is saved at Documents/stic/data/scans/FOLDERNAME/VoltVOLTAGE/scan_1-chCHNO.bin
- at the end the overview plots are saved in scan_1-chCHNO.root

# CTR Scans #
- Setup with Crystals and Source
- run 2_scan-ctr.sh CHNO FIXEDCH FOLDERNAME VOLTAGE 
- saved in same destination as above as scan_2-fixchFIXEDCH-chCHNO   .bin
- result is saved in scan_2-fixchFIXEDCH.root


