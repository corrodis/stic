#!/bin/bash
# Script for STiC3 scan used for dark rate scan
# From scan_1-chXX*.bin creates  scan_1-chXX.data and scan_1-chXX.root


## USE LIKE THIS : 
## 2) run locally on single channel : ./onechann.sh CH
## 3) example : for i in {16..47}; do ./onechann.sh $i; done
CH=$1
DATAPATH="/home/mri-pet/Documents/stic/data/scans/Matrix_260_261/Volt67V7_Ch52_54/"
STiC2PATH="/home/mri-pet/Documents/stic/software/daq_software/"

eval cd $DATAPATH

ls scan_*-ch$CH*.bin -l > scan_1-ch$CH.data
sed -i 's/.* 1 mri-pet mri-pet *//g' scan_1-ch$CH.data
sed -i 's/ ... .. ..:.. scan_/\t/g' scan_1-ch$CH.data
sed -i 's/-ch/\t/g' scan_1-ch$CH.data
sed -i 's/-/\t/g' scan_1-ch$CH.data
sed -i 's/s.bin//g' scan_1-ch$CH.data
sed -i 's/.bin//g' scan_1-ch$CH.data

eval "ipython "$STiC2PATH"scans/plot-rate.py scan_1-ch"$CH".data"
