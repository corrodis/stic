#!/bin/bash
# Script for STiC3 scan used for dark rate scan

PWD=$(pwd)
echo $PWD

fname="scantthresh" #located in STiC2PATH 
DATAPATH="/home/mri-pet/Documents/stic/data/scans/"
mkdir $DATAPATH$2
mkdir $DATAPATH$2"/Volt"$3
DATAPATH="/home/mri-pet/Documents/stic/data/scans/"$2"/Volt"$3"/"



STiC2PATH="/home/mri-pet/Documents/stic/software/daq_software/"

ACCTIME=15
CH=$1
#CH2=$2
#CHSTART=16
#C#HSTOP=48


#ANODE_FLAG_CH1 = 1	# 1
#CATHODE_FLAG_CH1 = 1   # 1
#S_SWITCH_CH1 = 1	# 1
#SORD_CH1 = 1		# 1
#SORD_NOT_CH1 = 0	# 1
#EDGE_CH1 = 1		# 1
#EDGE_CML_CH1 = 0	# 1
#DAC_CMLSCALE_CH1 = 0	# 1 
#DMON_ENA_CH1 = 0	# 
#DMON_SW_CH1 = 0	# 
#TDCTEST_CH1 = 1	# 1 (disable)
#AMON_CTRL_CH1 = 0	# 7
#COMP_SPI_CH1 = 0	# 3
#DAC_SIPM_SC_CH1 = 0	# 1
#DAC_SIPM_CH1 = f	# 63
#DAC_TTHRESH_SC_CH1 = 0	# 7
#DAC_TTHRESH_CH1 = 0	# 7
#DAC_AMPCOM_SC_CH1 = 2	# 3
#DAC_AMPCOM_CH1 = 0	# 63
#DAC_INPUTBIAS_SC_CH1 = 0
#DAC_INPUTBIAS_CH1 = 5
#DAC_ETHRESH_CH1 = 64	# 255
#DAC_POLE_SC_CH1 = 0	# 63
#DAC_POLE_CH1 = 3f	# 1
#DAC_CML_CH1 = 8	# 15
#DAC_DELAY_CH1 = 1	# 1
#DAC_DELAY_BIT1_CH1 = 0	# 1


DAC6="DAC_CMLSCALE"
START6=0
END6=1
STEP6=1

DAC7="DAC_CML"
START7=0
END7=15
STEP7=1

DAC8="DAC_DELAY"
START8=0
END8=1
STEP8=1

DAC9="DAC_DELAY_BIT1"
START9=0
END9=1
STEP9=1

DAC10="DAC_SIPM"
START10=0
END10=63
STEP10=1

DAC11="DAC_SIPM_SC"
START11=0
END11=1
STEP11=1

DAC12="DAC_AMPCOM"
START12=0
END12=63
STEP12=1

DAC13="DAC_AMPCOM_SC"
START13=0
END13=3
STEP13=1

DAC14="DAC_POLE"
START14=0
END14=63
STEP14=1

DAC15="DAC_POLE_SC"
START15=0
END15=1
STEP15=1

DAC5="ANODE_FLAG"
START5=0
END5=1
STEP5=1
DAC4="ANODE_FLAG"
START4=0
END4=1
STEP4=1

DAC1="DAC_TTHRESH" 
START1=0
END1=25 #63
STEP1=1
DAC2="DAC_TTHRESH_SC"
START2=1
END2=1	#7
STEP2=1
#OTHER=1 # TScale of the fixed channel
DAC3="DAC_ETHRESH"
START3=100
END3=125 #63
STEP3=1


NOSTART=3
NORUNS=1

NUM=0

if [ 1 == 1 ]; then
mkdir $DATAPATH
cd $STiC2PATH

cp $fname".txt" $fname"0.txt"
cp $fname.".txt" $DATAPATH"scan_"$run"-ch"$CH1"_settings.txt"
echo $START1" "$END1"\n"$DAC2" "$START2" "$END2"\n"$DAC3" "$START3" "$END3"\n"$DAC4" "$START4" "$END4"\n"$DAC5" "$START5" "$END5 >> $DATAPATH"scan_"$run"-ch"$CH1"_scan_settings.txt"


for run in $(seq $NOSTART 1 $(($NORUNS+$NOSTART-1)))
    do
        #for CH in $(seq $CHSTART 1 $CHSTOP)
	    #do
	    cd $STiC2PATH
            # Mask all channels	
   	    sed -i "s/\("DAC_CHANNEL_MASK_.*" =\) .*/\1 "1"/g" $fname"0.txt"

	    # Unmask the current tested channel
	    sed -i "s/\("DAC_CHANNEL_MASK_CH"$CH =\) .*/\1 0/g" $fname"0.txt"
	    #sed -i "s/\("DAC_CHANNEL_MASK_CH"$CH2 =\) .*/\1 0/g" $fname"0.txt"


	    COUNT=0

      	    for i4 in $(seq $START4 $STEP4 $END4) 
	  	do
	    	    cd $STiC2PATH
	    	    sed -i "s/\($DAC4"_CH"$CH =\) .*/\1 $(echo "obase=16; $i4" | bc)/g" $fname"0.txt"
	    	    #sed -i "s/\($DAC1"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname"0.txt"

      	    for i5 in $(seq $START5 $STEP5 $END5) 
	  	do
	    	    cd $STiC2PATH
	    	    sed -i "s/\($DAC5"_CH"$CH =\) .*/\1 $(echo "obase=16; $i5" | bc)/g" $fname"0.txt"
	    	    #sed -i "s/\($DAC1"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname"0.txt"
#      	    for i6 in $(seq $START12 $STEP12 $END12) 
#	  	do
#	    	    cd $STiC2PATH
#	    	    sed -i "s/\($DAC12"_CH"$CH =\) .*/\1 $(echo "obase=16; $i6" | bc)/g" $fname"0.txt"
#	    	    #sed -i "s/\($DAC1"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname"0.txt"
#      	    for i7 in $(seq $START13 $STEP13 $END13) 
#	  	do
#	    	    cd $STiC2PATH
#	    	    sed -i "s/\($DAC13"_CH"$CH =\) .*/\1 $(echo "obase=16; $i7" | bc)/g" $fname"0.txt"
#	    	    #sed -i "s/\($DAC1"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname"0.txt"
      	    for i1 in $(seq $START1 $STEP1 $END1) 
	  	do
	    	    cd $STiC2PATH
	    	    sed -i "s/\($DAC1"_CH"$CH =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname"0.txt"
	    	    #sed -i "s/\($DAC1"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname"0.txt"
		    for i2 in $(seq $START2 $STEP2 $END2) 
		    	do
		    	    cd $STiC2PATH
		    	    sed -i "s/\($DAC2"_CH"$CH =\) .*/\1 $(echo "obase=16; $i2" | bc)/g" $fname"0.txt"
		    	    #sed -i "s/\($DAC2"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $OTHER" | bc)/g" $fname"0.txt"
			    for i3 in $(seq $START3 $STEP3 $END3) 
			         do
			    	    cd $STiC2PATH
			    	    sed -i "s/\($DAC3"_CH"$CH =\) .*/\1 $(echo "obase=16; $i3" | bc)/g" $fname"0.txt"
			    	    #sed -i "s/\($DAC3"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i3" | bc)/g" $fname"0.txt"
				    
                                    # Start Run
				    COUNT=$(( $COUNT + 1))
				    #echo "-- -- -- -- -- -- -- -- -- "$CH"-"$i1"-"$i2"-"$i3" -- -- -- -- -- -- -- -- -- --"  >> $DATAPATH/"scan.log"
				    echo "-- -- -- -- -- -- -- -- -- "$CH"-"$i1"-"$i2"-"$i3"-"$i4"-"$i5" -- -- -- -- -- -- -- -- -- --"


				    # Config

        			    eval cd $STiC2PATH"stic2configGUI"
				    ./cmdline_config "../"$fname 0 
				
				    # Dump
				    eval cd $STiC2PATH"stic2daq"
				    ./dumpto_bin $DATAPATH"scan_"$run"-ch"$CH"-"$i1"-"$i2"-"$i3"-"$i4"-"$i5"-"$ACCTIME"s.bin" $ACCTIME 
				done 
			done    
		done  
	#done
#	done
#	done	
	done
	done
done

cd $STiC2PATH
rm $fname"0.txt"
echo "Scan done. This is the end."
fi

eval cd $DATAPATH
eval rm scan_2-fixch$CH2.data
mkdir plots
for f in scan_2-fixch$CH2*.bin
    do
	echo $f
        ../../../../software/daq_software/stic2daq/bin2root $f
        #../../../../stic3ana_simon/stepByStep ${f:0:-4} $CH2 $CH
        #../../../../stic3ana_simon/check ${f:0:-4} >> 'scan_2-fixch'$CH2'.data'
done

