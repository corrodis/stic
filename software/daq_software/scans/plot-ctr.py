import matplotlib.pyplot as plt
import numpy as np
import ROOT
import sys
import os

if len(sys.argv) < 1:
	print "Usage: plot-ctr.py filename"

m = np.loadtxt(sys.argv[1])
#print m
path = os.path.dirname(sys.argv[1])
f = ROOT.TFile(path+os.path.splitext(sys.argv[1])[0]+".root","RECREATE")


x = np.array(m[:,5])
y = np.array(m[:,7]*50*2.35)
Ey = np.array(m[:,8]*50*2.35)
Ex = np.zeros(len(x))
num = np.array(m[:,9])

g = ROOT.TGraphErrors(len(x),x,y,Ex,Ey)
g.GetXaxis().SetTitle("TTHRESH [DAC]")
g.GetYaxis().SetTitle("CTR FWHM [ps]")
g.SetName("tthresh-ctr")
#g.Draw("AL")
ROOT.gDirectory.Append(g)

n = ROOT.TGraph(len(x),x,num)
n.GetXaxis().SetTitle("TTHRESH [DAC]")
n.GetYaxis().SetTitle("events in photo peak")
n.SetName("tthresh-num")
#g.Draw("AL")
ROOT.gDirectory.Append(n)



f.Write()
f.Close()

