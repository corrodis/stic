#!/bin/bash
# Script for STiC3 scan used for dark rate scan

PWD=$(pwd)
echo $PWD


fname="scandark" #located in STiC2PATH 
DATAPATH="/home/mri-pet/Documents/stic/data/scans/"
mkdir $DATAPATH$2
mkdir $DATAPATH$2"/Volt"$3
DATAPATH="/home/mri-pet/Documents/stic/data/scans/"$2"/Volt"$3"/"
CHIPNO=1
echo "Data path: "$DATAPATH

STiC2PATH="/home/mri-pet/Documents/stic/software/daq_software/"

ACCTIME=1
#CH=$1 #here
CHSTART=(19 22 35 38)
CHS="19 22 35 38"
#CHS="0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63"
DAC3="DAC_TTHRESH" 
START3=0
END3=32 #32
STEP3=1
DAC2="DAC_TTHRESH_SC"
#START2=0
#END2=2
#STEP2=1
#scales
STEPS2="1"
DAC1="DAC_ETHRESH"
START1=0
END1=0
STEP1=255

NOSTART=1
NORUNS=1

NUM=0
mkdir $DATAPATH
cd $STiC2PATH
cp $fname".txt" $fname$CHIPNO".txt"
# Save the base settings
cp $fname".txt" $DATAPATH"scan_settings.txt"
echo "CH: "$CHSTART[0]" to "$CHSTART[3]"\n"$DAC1" "$START1" "$END1"\n"$DAC2" "$START2" "$END2"\n"$DAC3" "$START3" "$END3 > $DATAPATH"scan_settings.txt"


if true; then
for run in $(seq $NOSTART 1 $(($NORUNS+$NOSTART-1)))
    do
	echo "ok"
#        for CH in $(seq $CHSTART 1 $CHSTOP) #here
        #for (( j=0; j<4; ++j )) #here
	for CH in $CHS
   	    do                              #here
	    #CH = CHSTART[$j]
	    cd $STiC2PATH
            # Mask all channels	
   	    sed -i "s/\("DAC_CHANNEL_MASK_.*" =\) .*/\1 "1"/g" $fname$CHIPNO".txt"

	    # Unmask the current tested channel
	    sed -i "s/\("DAC_CHANNEL_MASK_CH"$CH =\) .*/\1 0/g" $fname$CHIPNO".txt"
	    #sed -i "s/\("DAC_CHANNEL_MASK"$CH2 =\) .*/\1 0/g" $fname$CHIPNO".txt"


	    COUNT=0

      	    for i1 in $(seq $START1 $STEP1 $END1) 
	  	do
	    	    cd $STiC2PATH
	    	    sed -i "s/\($DAC1"_CH"$CH =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname$CHIPNO".txt"
	    	    #sed -i "s/\($DAC1"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname$CHIPNO".txt"
		    #for i2 in $(seq $START2 $STEP2 $END2)
		    for i2 in $STEPS2 
		    	do
		    	    cd $STiC2PATH
		    	    sed -i "s/\($DAC2"_CH"$CH =\) .*/\1 $(echo "obase=16; $i2" | bc)/g" $fname$CHIPNO".txt"
		    	    #sed -i "s/\($DAC2"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i2" | bc)/g" $fname$CHIPNO".txt"
			    for i3 in $(seq $START3 $STEP3 $END3) 
			         do
			    	    cd $STiC2PATH
			    	    sed -i "s/\($DAC3"_CH"$CH =\) .*/\1 $(echo "obase=16; $i3" | bc)/g" $fname$CHIPNO".txt"
			    	    #sed -i "s/\($DAC3"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i3" | bc)/g" $fname$CHIPNO".txt"
				    
                                    # Start Run
				    COUNT=$(( $COUNT + 1))
				    #echo "-- -- -- -- -- -- -- -- -- "$CH"-"$i1"-"$i2"-"$i3" -- -- -- -- -- -- -- -- -- --"  >> $DATAPATH/"scan.log"
				    echo "-- -- -- -- -- -- -- -- -- "$CH"-"$i1"-"$i2"-"$i3" -- -- -- -- -- -- -- -- -- --"


				    # Config

        			    eval cd $STiC2PATH"stic2configGUI"
				    ./cmdline_config "../"$fname $CHIPNO 
				
				    # Dump
				    eval cd $STiC2PATH"stic2daq"
				    ./dumpto_bin $DATAPATH"scan_"$run"-ch"$CH"-"$i1"-"$i2"-"$i3"-"$ACCTIME"s.bin" $ACCTIME 
				done 
			done    
		done  
	done #here
done


cd $STiC2PATH
rm $fname$CHIPNO".txt"
echo "Scan done. This is the end."

fi

if true; then
eval cd $DATAPATH
echo "cd "$DATAPATH
rm rates.data
ls scan_*-ch*.bin -l > rates.data
sed -i 's/.* 1 mri-pet mri-pet *//g' rates.data
sed -i 's/.* 1 root    root *//g' rates.data
sed -i 's/.* 1 root root *//g' rates.data
sed -i 's/ ... .. ..:.. scan_/\t/g' rates.data
sed -i 's/-ch/\t/g' rates.data
sed -i 's/-/\t/g' rates.data
sed -i 's/s.bin//g' rates.data
sed -i 's/.bin//g' rates.data
fi

echo "pwd: " $(pwd)
if true; then
for f in $(echo *.bin)
	do
		echo $(pwd)/$f
		if [ ! -f $(pwd)/${f:0:-3}root ]
		then
			echo "bin2root $f"
			eval $STiC2PATH'stic2daq/bin2root '$f
		fi
		
		if [ ! -e  ${f:0:-4}_s.root ] 
		then
			echo "stic2single ${f:0:-3}root"
	 		eval $STiC2PATH'../../stic3ana_simon/stic2single' ${f:0:-3}root
		fi
done
fi
			
if true; then
sed -i 's/scan_.*-ch[0-9]*-//g' rates-fit.data
sed -i 's/-'$ACCTIME's//g' rates-fit.data
sed -i 's/\([0-9]*\)-\([0-9]*\)-\([0-9]*\)/\1\t\2\t\3/g' rates-fit.data
#sed -i 's/-/\t/g' rates-fit.data
sed -i 's/\([0-9]\)\.\([0-9]*e+0[0-9]\)/\1\2/g' rates-fit.data
sed -i 's/e+06/0/g' rates-fit.data
sed -i 's/e+07/00/g' rates-fit.data
sed -i 's/e+08/000/g' rates-fit.data
fi

if true; then
#for CH in $(seq $CHSTART 1 $CHSTOP)
#    do
	eval "ipython "$STiC2PATH"scans/plot-rate.py rates.data rates-fit.data"
#done
fi
