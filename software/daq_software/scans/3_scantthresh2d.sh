#!/bin/bash
# Script for STiC3 scan used for dark rate scan

PWD=$(pwd)
echo $PWD


fname="scantthresh" #located in STiC2PATH 
DATAPATH="/home/mri-pet/Documents/stic/data/scans/SIPATCa2223/Volt59_6/"
#mkdir $DATAPATH$3
#mkdir $DATAPATH$3"/Volt"$4
#DATAPATH="/home/mri-pet/Documents/stic/data/scans/"$3"/Volt"$4"/"



STiC2PATH="/home/mri-pet/Documents/stic/software/daq_software/"

ACCTIME=120
CH1=14
CH2=48
#CHSTART=16
#CHSTOP=48



DAC3="DAC_TTHRESH" 
START3=0
END3=32 #63
STEP3=1
DAC2="DAC_TTHRESH_SC"
START2=0
END2=7	#7
STEP2=1
DAC1="DAC_ETHRESH"
START1=100
END1=100 #63
STEP1=255

NOSTART=3
NORUNS=1

NUM=0

if [ 1 == 1 ]; then
mkdir $DATAPATH
cd $STiC2PATH

cp $fname".txt" $fname"0.txt"
cp $fname.".txt" $DATAPATH"scan_"$run"-ch"$CH1"-ch"$CH2"_settings.txt"
echo $START1" "$END1"\n"$DAC2" "$START2" "$END2"\n"$DAC3" "$START3" "$END3 >> $DATAPATH"scan_"$run"-ch"$CH2"-ch"$CH"_scan_settings.txt"


for run in $(seq $NOSTART 1 $(($NORUNS+$NOSTART-1)))
    do
        #for CH in $(seq $CHSTART 1 $CHSTOP)
	    #do
	    cd $STiC2PATH
            # Mask all channels	
   	    sed -i "s/\("DAC_CHANNEL_MASK_.*" =\) .*/\1 "1"/g" $fname"0.txt"

	    # Unmask the current tested channel
	    sed -i "s/\("DAC_CHANNEL_MASK_CH"$CH1 =\) .*/\1 0/g" $fname"0.txt"
	    sed -i "s/\("DAC_CHANNEL_MASK_CH"$CH2 =\) .*/\1 0/g" $fname"0.txt"


	    COUNT=0

      	    for i1 in $(seq $START1 $STEP1 $END1) 
	  	do
	    	    cd $STiC2PATH
	    	    sed -i "s/\($DAC1"_CH"$CH1 =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname"0.txt"
		    for i2 in $(seq $START2 $STEP2 $END2) 
		    	do
		    	    cd $STiC2PATH
		    	    sed -i "s/\($DAC2"_CH"$CH1 =\) .*/\1 $(echo "obase=16; $i2" | bc)/g" $fname"0.txt"
			    for i3 in $(seq $START3 $STEP3 $END3) 
			         do
			    	    cd $STiC2PATH
			    	    sed -i "s/\($DAC3"_CH"$CH1 =\) .*/\1 $(echo "obase=16; $i3" | bc)/g" $fname"0.txt"
				    for i1_2 in $(seq $START1 $STEP1 $END1)
                			do
                    			    cd $STiC2PATH
                    			    sed -i "s/\($DAC1"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i1_2" | bc)/g" $fname"0.txt"
                    			    for i2_2 in $(seq $START2 $STEP2 $END2)
                        		        do
                            			    cd $STiC2PATH
                            			    sed -i "s/\($DAC2"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i2_2" | bc)/g" $fname"0.txt"
                            			    for i3_2 in $(seq $START3 $STEP3 $END3)
                                 			do
                                    			    cd $STiC2PATH
                                    			    sed -i "s/\($DAC3"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i3_2" | bc)/g" $fname"0.txt"
 
                                    			    # Start Run
				    			    COUNT=$(( $COUNT + 1))
				    			    #echo "-- -- -- -- -- -- -- -- -- "$CH"-"$i1"-"$i2"-"$i3" -- -- -- -- -- -- -- -- -- --"  >> $DATAPATH/"scan.log"
				    			    echo "-- -- -- -- -- -- -- -- -- "$CH1"-"$i1"-"$i2"-"$i3" -- -- -- -- -- -- -- -- -- --"


				    			    # Config

        			    			    eval cd $STiC2PATH"stic2configGUI"
				    			    ./cmdline_config "../"$fname 0 
				
				    			    # Dump
				    			    eval cd $STiC2PATH"stic2daq"
				    			    ./dumpto_bin $DATAPATH"scan_"$run"-ch"$CH1"-"$i1"-"$i2"-"$i3"-ch"$CH2"-"$i1_2"-"$i2_2"-"$i3_2"-"$ACCTIME"s.bin" $ACCTIME
							done
						done
					done 
				done 
			done    
		done  
	#done	
done

cd $STiC2PATH
rm $fname"0.txt"
echo "Scan done. This is the end."
fi

eval cd $DATAPATH
eval rm scan_3-fixch$CH2.data
mkdir plots
for f in scan_3-fixch$CH2*.bin
    do
	echo $f
        ../../../../software/daq_software/stic2daq/bin2root $f
        ../../../../stic3ana_simon/stepByStep ${f:0:-4} $CH2 $CH
        ../../../../stic3ana_simon/check ${f:0:-4} >> 'scan_2-fixch'$CH2'.data'
done

sed -i 's/scan_3-ch//g' 'scan_3-ch'$CH2'.data'
#sed -i 's/-ch/\t/g' 'scan_3-ch'$CH2'.data'
sed -i 's/ch//g' 'scan_2-ch'$CH2'.data'
sed -i 's/-/\t/g' 'scan_2-ch'$CH2'.data'
sed -i 's/s//g' 'scan_2-ch'$CH2'.data'
#eval "ipython "$STiC2PATH"scans/plot-ctr.py scan_2-fixch"$CH2".data"
