#!/bin/bash
# Script for STiC3 scan used for dark rate scan

PWD=$(pwd)
echo $PWD


fname="scantthresh" #located in STiC2PATH 
DATAPATH="/home/mri-pet/Documents/stic/data/scans/"
mkdir $DATAPATH$3
mkdir $DATAPATH$3"/Volt"$4
DATAPATH="/home/mri-pet/Documents/stic/data/scans/"$3"/Volt"$4"/"
CHIPNO=1


STiC2PATH="/home/mri-pet/Documents/stic/software/daq_software/"

ACCTIME=120
CH=$1
CH2=$2
#CHSTART=16
#CHSTOP=48



DAC3="DAC_TTHRESH" 
START3=0
END3=32 #63
STEP3=1
DAC2="DAC_TTHRESH_SC"
START2=6
END2=6	#7
STEP2=1
OTHER=1 # TScale of the fixed channel
DAC1="DAC_ETHRESH"
START1=100
END1=100 #63
STEP1=255

NOSTART=2
NORUNS=1

NUM=0

if [ 1 == 1 ]; then
mkdir $DATAPATH
cd $STiC2PATH

cp $fname".txt" $fname{$CHIPNO}".txt"
cp $fname.".txt" $DATAPATH"scan_"$run"_fixch"$CH2"-ch"$CH"_settings.txt"
echo $START1" "$END1"\n"$DAC2" "$START2" "$END2" OTHER: "$OTHER"\n"$DAC3" "$START3" "$END3 >> $DATAPATH"scan_"$run"_fixch"$CH2"-ch"$CH"_scan_settings.txt"


for run in $(seq $NOSTART 1 $(($NORUNS+$NOSTART-1)))
    do
        #for CH in $(seq $CHSTART 1 $CHSTOP)
	    #do
	    cd $STiC2PATH
            # Mask all channels	
   	    sed -i "s/\("DAC_CHANNEL_MASK_.*" =\) .*/\1 "1"/g" $fname{$CHIPNO}".txt"

	    # Unmask the current tested channel
	    sed -i "s/\("DAC_CHANNEL_MASK_CH"$CH =\) .*/\1 0/g" $fname{$CHIPNO}".txt"
	    sed -i "s/\("DAC_CHANNEL_MASK_CH"$CH2 =\) .*/\1 0/g" $fname{$CHIPNO}".txt"


	    COUNT=0

      	    for i1 in $(seq $START1 $STEP1 $END1) 
	  	do
	    	    cd $STiC2PATH
	    	    sed -i "s/\($DAC1"_CH"$CH =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname{$CHIPNO}".txt"
	    	    #sed -i "s/\($DAC1"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i1" | bc)/g" $fname{$CHIPNO}".txt"
		    for i2 in $(seq $START2 $STEP2 $END2) 
		    	do
		    	    cd $STiC2PATH
		    	    sed -i "s/\($DAC2"_CH"$CH =\) .*/\1 $(echo "obase=16; $i2" | bc)/g" $fname{$CHIPNO}".txt"
		    	    sed -i "s/\($DAC2"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $OTHER" | bc)/g" $fname{$CHIPNO}".txt"
			    for i3 in $(seq $START3 $STEP3 $END3) 
			         do
			    	    cd $STiC2PATH
			    	    sed -i "s/\($DAC3"_CH"$CH =\) .*/\1 $(echo "obase=16; $i3" | bc)/g" $fname{$CHIPNO}".txt"
			    	    #sed -i "s/\($DAC3"_CH"$CH2 =\) .*/\1 $(echo "obase=16; $i3" | bc)/g" $fname{$CHIPNO}".txt"
				    
                                    # Start Run
				    COUNT=$(( $COUNT + 1))
				    #echo "-- -- -- -- -- -- -- -- -- "$CH"-"$i1"-"$i2"-"$i3" -- -- -- -- -- -- -- -- -- --"  >> $DATAPATH/"scan.log"
				    echo "-- -- -- -- -- -- -- -- -- "$CH"-"$i1"-"$i2"-"$i3" -- -- -- -- -- -- -- -- -- --"


				    # Config

        			    eval cd $STiC2PATH"stic2configGUI"
				    ./cmdline_config "../"$fname {$CHIPNO} 
				
				    # Dump
				    eval cd $STiC2PATH"stic2daq"
				    ./dumpto_bin{$CHIPNO} $DATAPATH"scan_"$run"-fixch"$CH2"-ch"$CH"-"$i1"-"$i2"-"$i3"-"$ACCTIME"s.bin" $ACCTIME 
				done 
			done    
		done  
	#done	
done

cd $STiC2PATH
rm $fname{$CHIPNO}".txt"
echo "Scan done. This is the end."
fi

eval cd $DATAPATH
eval rm scan_2-fixch$CH2.data
mkdir plots
for f in scan_2-fixch$CH2*.bin
    do
	echo $f
        ../../../../software/daq_software/stic2daq/bin2root $f
        ../../../../stic3ana_simon/stepByStep ${f:0:-4} $CH2 $CH
        ../../../../stic3ana_simon/check ${f:0:-4} >> 'scan_2-fixch'$CH2'.data'
done

sed -i 's/scan_2-fixch//g' 'scan_2-fixch'$CH2'.data'
sed -i 's/fixch/\t/g' 'scan_2-fixch'$CH2'.data'
sed -i 's/ch//g' 'scan_2-fixch'$CH2'.data'
sed -i 's/-/\t/g' 'scan_2-fixch'$CH2'.data'
sed -i 's/s//g' 'scan_2-fixch'$CH2'.data'
eval "ipython "$STiC2PATH"scans/plot-ctr.py scan_2-fixch"$CH2".data"
