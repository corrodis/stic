import matplotlib.pyplot as plt
import numpy as np
import ROOT
import sys
import os

if len(sys.argv) < 1:
	print "Usage: plot.py filename"

if(sys.argv[1]=="null"):	
	path = os.path.dirname(sys.argv[2])
	f = ROOT.TFile(path+os.path.splitext(sys.argv[2])[0]+".root","RECREATE")
else:
	m = np.loadtxt(sys.argv[1])
	path = os.path.dirname(sys.argv[1])
	f = ROOT.TFile(path+os.path.splitext(sys.argv[1])[0]+".root","RECREATE")


for ch in range(0, 64):
    for scale in range(8):
        s = (m[:,4]==scale)&(m[:,2]==ch)&(m[:,1]==1)      
#        
        sort = np.argsort(m[s,5])
        x = m[s,5][sort]
        y = m[s,0][sort]/8000
        
        #print x.sort()
	if len(m[s,5]) > 0:
	        g = ROOT.TGraph(32, x, y)
	        g.SetName("ch"+str(ch)+"_scale"+str(scale))
	        g.SetTitle("ch: "+str(ch)+", scale: "+str(scale))
	        g.GetXaxis().SetTitle("tthres [DAC]")
	        g.GetYaxis().SetTitle("rate [kHz]")
        #g.Draw("A*")
	        ROOT.gDirectory.Append(g);
        #plt.plot(x,y,"x-", label='scale '+str(scale))
        #s = (m[:,4]==scale)&(m[:,2]==ch)&(m[:,1]==2)
        #sort = np.argsort(m[s,5])
        #x = m[s,5][sort]
        #y = m[s,0][sort]/80000
        #plt.plot(x, y,"x-", label='scale '+str(scale))
        #plt.title("ch: "+str(ch)+", scale: "+str(scale))
        #plt.axis([0, 31, 50, 750])
        #if max(m[s,0]) > 0:
        #plt.gca().set_yscale('log')
    #plt.grid()
    #plt.legend()
    #plt.show()


if len(sys.argv) > 2:
        m2 = np.loadtxt(sys.argv[2])
	#print m2

	#chi2 cut
	m2[m2[:,6]>100,4] = -1
        
        for ch in range(0, 64):
                for scale in range(8):
                        s2 = (m2[:,1]==scale)&(m2[:,3]==ch)
			#print ch, scale
#       

                        sort = np.argsort(m2[s2,2])
                        x = m2[s2,2][sort]
                        y = m2[s2,4][sort]/1000
			ex = np.zeros(33)
			ey = m2[s2,5][sort]/1000

                        #print x.sort()
                        if len(m2[s2,2]) > 10:
				#print "ok"
                                g = ROOT.TGraphErrors(33, x, y, ex, ey)
                                g.SetName("fit_ch"+str(ch)+"_scale"+str(scale))
                                g.SetTitle("ch: "+str(ch)+", scale: "+str(scale)+" (fit)")
                                g.GetXaxis().SetTitle("tthres [DAC]")
                                g.GetYaxis().SetTitle("rate [kHz]")
                                #g.Draw("A*")
                                ROOT.gDirectory.Append(g);
        

f.Write()
f.Close()

