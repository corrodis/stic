#!/bin/bash
# Script for STiC3 scan used for dark rate scan

PWD=$(pwd)
echo $PWD


fname="scandark" #located in STiC2PATH 
DATAPATH="/home/mri-pet/Documents/stic/data/scans/"
mkdir $DATAPATH$2
mkdir $DATAPATH$2"/Volt"$3
DATAPATH="/home/mri-pet/Documents/stic/data/scans/"$2"/Volt"$3"/"
CHIPNO=0
echo "Data path: "$DATAPATH

STiC2PATH="/home/mri-pet/Documents/stic/software/daq_software/"

ACCTIME=1
#CH=$1 #here
CHSTART=14
CHSTOP=14
#CHSTART=52
#CHSTOP=52 #63


DAC3="DAC_TTHRESH" 
START3=0
END3=32 #63
STEP3=1
DAC2="DAC_TTHRESH_SC"
START2=0
#END2=0	#63
END2=7	#63
STEP2=1
DAC1="DAC_ETHRESH"
START1=0
END1=0 #63
STEP1=255

NOSTART=1
NORUNS=1

NUM=0

if true; then
eval cd $DATAPATH
echo "cd "$DATAPATH
fi

if true; then
sed -i 's/scan_.*-ch[0-9]*-//g' rates-fit.data
sed -i 's/-'$ACCTIME's//g' rates-fit.data
sed -i 's/\([0-9]*\)-\([0-9]*\)-\([0-9]*\)/\1\t\2\t\3/g' rates-fit.data
#sed -i 's/-/\t/g' rates-fit.data
sed -i 's/\([0-9]\)\.\([0-9]*e+0[0-9]\)/\1\2/g' rates-fit.data
sed -i 's/e+06/0/g' rates-fit.data
sed -i 's/e+07/00/g' rates-fit.data
sed -i 's/e+08/000/g' rates-fit.data

for CH in $(seq $CHSTART 1 $CHSTOP)
    do
	eval "ipython "$STiC2PATH"scans/plot-rate.py scan_1-ch"$CH".data rates-fit.data"
done
fi
