#include <errno.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <libusb.h>
#include <fstream>
#include <iomanip>

#include <time.h>
#include <math.h>
#include <stdint.h>





using namespace std;

static struct libusb_device_handle *devh = NULL;

int close_lusb()
{
	int r=0;
	libusb_close(devh);
	libusb_exit(NULL);
	if (r < 0) {
		fprintf(stderr, "Could not find/open device\n");
		close_lusb();
		exit(r);
	}
	return r;
}


int init_lusb()
{
	int r;
	r = libusb_init(NULL);
	if (r < 0) {
		fprintf(stderr, "failed to initialise libusb\n");
		exit(1);
	}
	return r;
}



static int find_device()	//open the CYPRESS USB with vendorID:productID 05e3:0608
{
	devh = libusb_open_device_with_vid_pid(NULL, 0j04b4, 0x1003);
	return devh ? 0 : -EIO;
}


static int dump_read()
{
	int r=0;
	int i;
	int transferred=0;
//	unsigned char *buffer = new unsigned char[2048];

	uint32_t *buffer=new uint32_t[512];

	clock_t t;
	
	
	for (i = 0; i < 512; i++)
	{
		buffer[i]=0xffff&(i);
	}
	buffer[0]= 0x00001e00;
	r = libusb_bulk_transfer(devh,(0x02 | LIBUSB_ENDPOINT_OUT),(unsigned char*)buffer,512,&transferred,0);
//	r = libusb_bulk_transfer(devh,(0x02 | LIBUSB_ENDPOINT_OUT),buffer,512,&transferred,0);
//	r = libusb_bulk_transfer(devh,(0x02 | LIBUSB_ENDPOINT_OUT),buffer,512,&transferred,0);
//	r = libusb_bulk_transfer(devh,(0x02 | LIBUSB_ENDPOINT_OUT),buffer,512,&transferred,0);

//	bzero(buffer, sizeof(buffer));
//
	printf("Sending done\n");


	for (i = 0; i < 512; i++)
	{
		buffer[i]=0x00;
	}



	t = clock(); 


	r = libusb_bulk_transfer(devh,(0x86 | LIBUSB_ENDPOINT_IN),(unsigned char*)buffer,512,&transferred,0);
	if (r < 0)
	{
		printf("Error when reading from device: %d\n", r);
	}
	else printf("Number of bytes read: %d\n",transferred);

	t = clock() - t;

	printf("Returned data: \n");
	for(i=63;i>=0;i--)
	{
		printf("%d: %08X \n ",i*4,buffer[i]);
	}

	printf("\n\n");

	return r;
}


int main(int argc, char** argv)
{
	//initialize the libusb
	int r;
	r = init_lusb();

	r = find_device();
	if (r < 0) {
		fprintf(stderr, "Could not find/open device\n");
		close_lusb();
		exit(r);
	}
	else
	{
		fprintf(stderr, "Device found and opened\n");
	}

	r = libusb_claim_interface(devh, 0);	//The device interface is 1
	if (r < 0) {
		fprintf(stderr, "usb_claim_interface error %d\n", r);
		exit(r);
	}
	else
	{
		printf("claimed interface\n");
	}



	r=dump_read();

	libusb_release_interface(devh, 0);
	libusb_close(devh);
	libusb_exit(NULL);

	return 0;
}
