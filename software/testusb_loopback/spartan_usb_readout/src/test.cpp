#include "TUSB_readout.h"
#include <stdio.h>
#include "usb_msg_struct.h"
#include <unistd.h>



int main()
{
	usb_header packet;
	unsigned char* buffer;	//the payload
	int i;

	TUSB_readout *uread = new TUSB_readout;
	if(!uread->check_status()) printf("Device could not be opened!!\n");
	
	uread->set_timeout(2000);

		packet.cmd='M';
		packet.handleID=0x0E;
		packet.port=0x0F;
		packet.len=0x00;

		
		for(i=0;i<508;i++)
		{
			packet.payload[i]=i*2;
		}
		packet.payload[i]=0x00;
		buffer = (unsigned char*)(&packet);

		for(i=0;i<512;i++)
		{
			printf("%2X ",buffer[i]);
		}
		
		printf("\n");

//		printf("Header written: %c %X %X %X\n\n", ((usb_header*)buffer)->cmd, ((usb_header*)buffer)->handleID, ((usb_header*)buffer)->port, ((usb_header*)buffer)->len); 

		i=uread->write_usb((unsigned char*)(&packet),512);
	//	i=uread->write_usb(buffer,512);
		printf("%d bytes have been written \n\r",i);
		i=uread->read_usb(buffer,512);
		
		printf("%d bytes have been returned: \n\n\n",i);
		if(i > 0){
			for(i=0;i<512;i++)
			{
				printf("%02X ",buffer[511-i]);
				if (i%4 == 3) printf("\n");
			}
		}
		printf("\n");


		usleep(50000);
	return 0;
}
