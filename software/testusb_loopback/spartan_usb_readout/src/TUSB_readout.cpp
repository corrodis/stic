/*
 * Wrapper Class for readout of a USB device with libusb libraries
 *
 * Date: Thu Apr 18 18:42:43 CEST 2013
 * Author tharion
 *
 */

#include <stdint.h>
#include <iomanip>
#include <libusb.h>
#include <errno.h>
#include <stdio.h>
#include "TUSB_readout.h"
#include "usb_msg_struct.h"


TUSB_readout::TUSB_readout()
{
	devh = NULL;	//set the device handle to NULL for safety
	//Set the default values for the vendor and product id (cypress usb controller)
	vid = 0x04b4;
	pid = 0x1003;


	int r;

	r = libusb_init(NULL);	//Init the libusb driver
	if (r < 0) {
		fprintf(stderr, "failed to initialise libusb\n");
	}
	r=open_usb();

	if(r < 0) device_open = false;
		else device_open = true;

	timeout = 1000;	//Default timeout 1s
	ep_read = 6;	//The in and out endpoints for reading and writing
	ep_write = 2;	
}


TUSB_readout::~TUSB_readout()
{
	close_usb();
	libusb_exit(NULL);
}


int TUSB_readout::open_usb()
{
	devh = libusb_open_device_with_vid_pid(NULL, vid, pid);	//Try to open the device
	return devh ? 0 : -EIO;	//Return open failed if unsuccessfull
}


void TUSB_readout::close_usb()
{
	if(device_open)
	{
		libusb_release_interface(devh, 0);
		libusb_close(devh);
	}
}


unsigned int TUSB_readout::write_usb(unsigned char* buffer, int len)
{
	int transferred;
	int r;
	r = libusb_bulk_transfer(devh,((0x0f&ep_write) | LIBUSB_ENDPOINT_OUT),buffer, len ,&transferred,timeout);

	if (r < 0) 
	{
		printf("Error while writing: %d\n\r",r);
		return 0;
	}
	else return transferred;
}



unsigned int TUSB_readout::read_usb(unsigned char* buffer, int len)
{

	int transferred;
	int r;

	r = libusb_bulk_transfer(devh,(0x80 | (0x0f&ep_read) | LIBUSB_ENDPOINT_IN),buffer,len,&transferred,timeout);

	printf("Header returned: %c %X %X %X\n\n", ((usb_header*)buffer)->cmd, ((usb_header*)buffer)->handleID, ((usb_header*)buffer)->port, ((usb_header*)buffer)->len); 
	if (r < 0) return 0;
	else return transferred;
}



void TUSB_readout::set_ep_rw(unsigned short read, unsigned short write)
{
	ep_read = read;
	ep_write = write;
}

void TUSB_readout::set_timeout(unsigned int time)
{
	timeout=time;
}

void TUSB_readout::set_vid_pid(unsigned int vendor_id, unsigned int product_id)
{
	vid=vendor_id;
	pid=product_id;
}
