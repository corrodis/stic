#ifndef USB_MSG_STRUCT_H
#define USB_MSG_STRUCT_H 


struct usb_header{
	unsigned cmd : 8;
	unsigned handleID : 8;
	unsigned port : 7;
	unsigned len : 9;
	unsigned char payload[508];
};



#endif //USB_MSG_STRUCT_H
