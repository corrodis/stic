/*
 * Wrapper Class for readout of a USB device with libusb libraries
 *
 * Date: Thu Apr 18 18:42:43 CEST 2013
 * Author tharion
 *
 */

#ifndef TUSB_READOUT_H
#define TUSB_READOUT_H


#include <stdint.h>
//#include <iomanip>
#include <libusb.h>
#include "usb_msg_struct.h"


class TUSB_readout{
	public:
		/*
		 * Constructor that tries to open the default device and initializes the values
		 */

		TUSB_readout(); 
		/*
		 * Destructor closing the USB device
		 */
		~TUSB_readout(); 

		/*
		 * Function to find and open the USB device. Uses the configured VID and PID to identify the device
		 */
		int open_usb();


		/*
		 * Function for closing the usb device
		 */
		void close_usb();
		


		/*
		 * Write to the usb port
		 *
		 * @param buffer is a pointer to the data buffer
		 * @param len is the length of the data in the buffer
		 *
		 * returns the value of bytes written 
		 */
		unsigned int write_usb(unsigned char* buffer, int len);



		/*
		 * Read from the usb port
		 *
		 * @param buffer is a pointer to the data buffer
		 * @param len is the length of the maximum number of bytes to read
		 *
		 * returns the value of bytes read
		 */
		unsigned int read_usb(unsigned char* buffer, int len);

		/*
		 * Change the default vendor and product id of the usb device
		 */
		void set_vid_pid(unsigned int vid, unsigned int pid);


		/*
		 * Set the endpoint numbers for reading and writing
		 *
		 * @param : read is the endpoint number for reading, write the ep number for writing
		 */
		void set_ep_rw(unsigned short read, unsigned short write);



		/*
		 * Change the timeout of read and write operations
		 *
		 * @param time is the timeout value in ms
		 */
		void set_timeout(unsigned int time);


		/*
		 * Check if a device has been opened
		 */
		bool check_status(){return device_open;}


	private:
		struct libusb_device_handle *devh;	//!< usb device handle for libusb

		unsigned int vid;		//!< Vendor ID of the USB device we want to open
		unsigned int pid;		//!< Product ID of the USB device we want to open
		unsigned int timeout;		//!< Timeout in ms for read and write operations

		unsigned short ep_read;		//!< the endpoint number for reading
		unsigned short ep_write;	//!< the endpoint number for writing

		bool device_open;		//!< indicator if a device is open

};



#endif	//TUSB_READOUT_H
