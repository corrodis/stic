/* Debug interface class for the digital part of STiC3
 *
 * The interface uses the serial port to write and read the debug commands.
 * It is written for the standalone version of the debug module.
 * Modifications will be required if the debug module is integrated as an AXI
 * component in a MicroBlaze processor core.
 *
 * Author : Tobias Harion
 *
 * Date: Fri Feb 14 11:55:17 CET 2014
 *
 * Version: 0.1
 */

//Serial Interface included in the chip_interface folder
//#include "SerialInterface.h"
#include "MQInterface.h"
#include "mq_message_dict.h"

#include <iostream>
#include <string>


//COMMAND DEFINITIONS
//DEBUGGING COMMANDS: MONITORING CONTROL
#define CMD_SET_MONITOR1  0X01;	//SET THE MONITOR VALUE FOR MUX 1
#define CMD_SET_MONITOR2  0X03;	//SET THE MONITOR VALUE FOR MUX 2

#define CMD_EN_MONITOR1  0X02;
#define CMD_EN_MONITOR2  0X04;

#define CMD_DIS_MONITOR  0X00;

//DEBUGGING COMMANDS: DATA INJECTION
#define CMD_WRITE_L1  0X10;	//WRITE EVENT DATA TO A LEVEL 1 BUFFER

//DEBUGGING COMMANDS: DATA READING
//THE GROUP NUMBER SO READ BACK IS GIVEN IN THE PAYLOAD
#define CMD_READ_DSTORING_IN  0X30; 	//"00110000" READ THE DATA TO THE L1 STORING
#define CMD_READ_L1_FIFO_IN  0X40; 	//"01000000"; READ THE DATA TO THE L1 FIFO
#define CMD_READ_L1_FIFO_OUT  0X50; 	//"01010000"; READ THE DATA TO THE GROUP SELECT
#define CMD_READ_GROUP_SEL_OUT  0X60; 	//"01100000"; READ THE DATA TO THE MS SELECT
#define CMD_READ_MS_SEL_OUT  0X70;	//"01110000"; READ THE DATA TO THE L2 FIFO
#define CMD_READ_L2_FIFO_OUT  0X80;	//"01110000"; READ THE DATA FROM THE L2 FIFO

#define CMD_DIS_DATA_FLOW_MON 0XF0; 	//"11110001"; SWITCH OFF THE DATA FLOW MONITOR
#define CMD_EN_DATA_FLOW_MON 0XF1; 	//"11110001"; SWITCH ON THE DATA FLOW MONITOR

//FPGA DEBUG CONTROL COMMANDS
#define CMD_STEP_DBG 0x11;		//"00010001" STEP THE SPI CLOCK FOR THE DEBUG CONTROL LOGIC
#define CMD_STEP_STIC 0x14;		//"00010100" STEP THE SYSTEM CLOCK OF THE CHIP

#define CMD_EN_SYS_CLOCK 0x15;		//"00010001" STEP THE SPI CLOCK FOR THE DEBUG CONTROL LOGIC
#define CMD_DIS_SYS_CLOCK 0x16;		//"00010100" STEP THE SYSTEM CLOCK OF THE CHIP




enum read_cmd {
	READ_DSTORING_IN,\
	READ_L1_IN,\
	READ_L1_OUT,\
	READ_GROUP_SEL_OUT,\
	READ_MS_SEL_OUT,\
	READ_L2_FIFO_OUT
};


enum mon_mux {
       		BYTE_CLK,\
		L1_DREADY0,\
		L1_DREADY1,\
		L1_DREADY2,\
		L1_DREADY3,\
		L1_FIFO_EMPTY0,\
		L1_FIFO_EMPTY1,\
		L1_FIFO_EMPTY2,\
		L1_FIFO_EMPTY3,\
		L1_FIFO_FULL0,\
		L1_FIFO_FULL1,\
		L1_FIFO_FULL2,\
		L1_FIFO_FULL3,\
		L2_FIFO_EMPTY,\
		L2_FIFO_FULL,\
		INIT_TRANS
};

class STiC3_Debug {
	public:
		//! Constructor with the TSerialInterface class as parameter
		/*!
		 * \param dev_name : path of the serial interface device
		 */
		STiC3_Debug(char* queue_id);

		//! Create one clock cycle for the debug logic
		void step_debug_logic();

		//! Create one clock cycle for the digital core logic
		void step_core_logic();



		//! Program digital monitor multiplexer
		/*!
		 *\param monitor : digital monitor to program (0 or 1)
		 *\param selection : which signal to look at this output
		 */
		void set_dig_mon_mux(unsigned short monitor, mon_mux selection);


		//! Enable the specified digital monitor output
		/*!
		 *\param monitor : digital monitor to enable (0 or 1)
		 */
		void ena_dig_mon(unsigned short monitor);

		//! Disable the digital monitors
		void dis_dig_mon();

		//! Enable data flow monitoring
		void en_dflow_mon();

		//! Disable data flow monitoring
		void dis_dflow_mon();

		//! Enable or disable the system clock
		void set_en_sysclk(unsigned short enable);

		//! Write an event to the L1 buffer
		/*!
		 * \param buffer : Number of the buffer to write the data to
		 * \param event : L1_data struct containing the event data to be written to the L1_buffer
		 */
		void inject_l1_data(unsigned short buffer);

		//! Read the current data flow value
		/*!
		 * \param buffer : Number of the buffer to write the data to
		 * \param command : Which part of the chip data flow to read back
		 */
		void read_data_flow(unsigned short buffer, read_cmd command);

		//L1 Buffer event data used for the data injection
		unsigned short channel;
		unsigned short T_BadHit;
		unsigned short TCCM;
		unsigned short TCCS;
		unsigned short T_Fine;
		unsigned short E_BadHit;
		unsigned short ECCM;
		unsigned short ECCS;
		unsigned short E_Fine;

		//Return message public for read in python
		std::string return_string;


		//Run a complete data flow monitoring run with the currently injected data
		void run_dflow(unsigned short buffer, const char* out_filename);

	private:

		unsigned short get_bit(char* buffer, unsigned short bit_num);

		//Debug message containing the debugging command as payload in the sc_message --> 11+4 byte
		char dbg_msg[11];	//!< Buffer of the debug command message
		char return_msg[15];	//!< Buffer for the returned command value with sc_message header

		msg_struct sc_message;	//!< Message queue struct for communication with the STiC3DAQ

		TMQInterface* iface;	//!< The serial interface to the FPGA



};
