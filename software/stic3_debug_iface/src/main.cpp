#include "STiC3_Debug.h"
#include <stdio.h>



int main()
{
	STiC3_Debug *debug = new STiC3_Debug("/dev/ttyUSB0");
	debug->step_debug_logic();
	debug->ena_dig_mon(1);
	debug->set_dig_mon_mux(1,L1_DREADY0);
	delete debug;
	return 0;
}
