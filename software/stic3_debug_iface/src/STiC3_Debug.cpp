#include "STiC3_Debug.h"
#include <stdio.h>
#include <strings.h>
#include <boost/python.hpp>
#include <boost/python/class.hpp>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <unistd.h>


#define DEBUG


//!Constructor with the device name as parameter
STiC3_Debug::STiC3_Debug(char* dev_name)
{
	//Create a new serial interface
	iface = new TMQInterface(dev_name);
	return;
}

void STiC3_Debug::set_en_sysclk(unsigned short enable)
{
	bzero(dbg_msg,11); //CLEAR THE COMMAND MESSAGE
	if (enable== 1)
	{
		dbg_msg[10]=CMD_EN_SYS_CLOCK;
	}
	else
	{
		dbg_msg[10]=CMD_DIS_SYS_CLOCK;
	}

#ifdef DEBUG
	printf("Writing debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",dbg_msg[i]);
	printf("\n");
#endif

	iface->Command(0,'D',15,dbg_msg,-1,NULL);
}

void STiC3_Debug::step_debug_logic()
{
	bzero(dbg_msg,11); //CLEAR THE COMMAND MESSAGE
	dbg_msg[10]=CMD_STEP_DBG;

#ifdef DEBUG
	printf("Writing debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",dbg_msg[i]);
	printf("\n");
#endif

	iface->Command(0,'D',15,dbg_msg,-1,NULL);
}


void STiC3_Debug::step_core_logic()
{
	bzero(dbg_msg,11); //CLEAR THE COMMAND MESSAGE
	dbg_msg[10]=CMD_STEP_STIC;

#ifdef DEBUG
	printf("Writing debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",dbg_msg[i]);
	printf("\n");
#endif
	iface->Command(0,'D',15,dbg_msg,-1,NULL);
}


void STiC3_Debug::set_dig_mon_mux(unsigned short monitor, mon_mux selection)
{
	bzero(dbg_msg,11); //CLEAR THE COMMAND MESSAGE
	if(monitor > 1) return; //THERE IS ONLY MONITOR 0 OR 1....

	if(monitor == 0)
	{
		dbg_msg[10]=CMD_SET_MONITOR1;
	}
	else if(monitor == 1) dbg_msg[10]=CMD_SET_MONITOR2;

	switch (selection)
	{
		case BYTE_CLK:
			dbg_msg[0]=0x00;
			break;
		case L1_DREADY0:
			dbg_msg[0]=0x01;
			break;
		case L1_DREADY1:
			dbg_msg[0]=0x02;
			break;
		case L1_DREADY2:
			dbg_msg[0]=0x03;
			break;
		case L1_DREADY3:
			dbg_msg[0]=0x04;
			break;
		case L1_FIFO_EMPTY0:
			dbg_msg[0]=0x05;
			break;
		case L1_FIFO_EMPTY1:
			dbg_msg[0]=0x06;
			break;
		case L1_FIFO_EMPTY2:
			dbg_msg[0]=0x07;
			break;
		case L1_FIFO_EMPTY3:
			dbg_msg[0]=0x08;
			break;
		case L1_FIFO_FULL0:
			dbg_msg[0]=0x09;
			break;
		case L1_FIFO_FULL1:
			dbg_msg[0]=0x0a;
			break;
		case L1_FIFO_FULL2:
			dbg_msg[0]=0x0b;
			break;
		case L1_FIFO_FULL3:
			dbg_msg[0]=0x0c;
			break;
		case L2_FIFO_EMPTY:
			dbg_msg[0]=0x0d;
			break;
		case L2_FIFO_FULL:
			dbg_msg[0]=0x0e;
			break;
		case INIT_TRANS:
			dbg_msg[0]=0x0f;
			break;
		default: dbg_msg[0]=0x00;
			 break;
	}


#ifdef DEBUG
	printf("Writing debug message: ");
	for(int i=11;i>=0;i--) printf("%02X ",dbg_msg[i]);
	printf("\n");
#endif
	iface->Command(0,'D',15,dbg_msg,15,return_msg);

#ifdef DEBUG
	printf("Returned debug message: ");
	for(int i=11;i>=0;i--) printf("%02X ",return_msg[i]);
	printf("\n");
#endif
}


void STiC3_Debug::ena_dig_mon(unsigned short monitor)
{
	bzero(dbg_msg,11); //CLEAR THE COMMAND MESSAGE
	if(monitor > 1) return; //THERE IS ONLY MONITOR 0 OR 1....

	if(monitor == 0)
	{
		dbg_msg[10]=CMD_EN_MONITOR1;
	}
	else if(monitor == 1) dbg_msg[10]=CMD_EN_MONITOR2;


#ifdef DEBUG
	printf("Writing debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",dbg_msg[i]);
	printf("\n");
#endif
	iface->Command(0,'D',15,dbg_msg,15,return_msg);

#ifdef DEBUG
	printf("Returned debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",return_msg[i]);
	printf("\n");
#endif
}

void STiC3_Debug::dis_dig_mon()
{
	bzero(dbg_msg,11); //CLEAR THE COMMAND MESSAGE
	dbg_msg[10] = CMD_DIS_MONITOR;

#ifdef DEBUG
	printf("Writing debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",dbg_msg[i]);
	printf("\n");
#endif
	iface->Command(0,'D',15,dbg_msg,15,return_msg);

#ifdef DEBUG
	printf("Returned debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",return_msg[i]);
	printf("\n");
#endif

}

void STiC3_Debug::en_dflow_mon()
{
	bzero(dbg_msg,11); //CLEAR THE COMMAND MESSAGE
	dbg_msg[10]=CMD_EN_DATA_FLOW_MON;

#ifdef DEBUG
	printf("Writing debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",dbg_msg[i]);
	printf("\n");
#endif
	iface->Command(0,'D',15,dbg_msg,15,return_msg);

#ifdef DEBUG
	printf("Returned debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",return_msg[i]);
	printf("\n");
#endif
}


void STiC3_Debug::dis_dflow_mon()
{
	bzero(dbg_msg,11); //CLEAR THE COMMAND MESSAGE
	dbg_msg[10]=CMD_DIS_DATA_FLOW_MON;

#ifdef DEBUG
	printf("Writing debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",dbg_msg[i]);
	printf("\n");
#endif
	iface->Command(0,'D',15,dbg_msg,15,return_msg);

#ifdef DEBUG
	printf("Returned debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",return_msg[i]);
	printf("\n");
#endif
}


void STiC3_Debug::inject_l1_data(unsigned short buffer)
{

	bzero(dbg_msg,11); //CLEAR THE COMMAND MESSAGE

	dbg_msg[10]=CMD_WRITE_L1;
	dbg_msg[9]= 0xFF&(((0x0003 & buffer) << 6) | (0x003f & channel));
	dbg_msg[8]= 0xFF&(((0x3f00 & TCCM) >> 8) | (( 0x0001 & T_BadHit) << 7));
	dbg_msg[7]= 0xFF&((0x00ff & TCCM));
	dbg_msg[6]= 0xFF&(((0x7f80 & TCCS) >> 7));
	dbg_msg[5]= 0xFF&(((0x0010 & T_Fine) >> 4) | ((0x007f & TCCS) << 1));
	dbg_msg[4]= 0xFF&(((0x7000 & ECCM) >> 12) | ((0x0001 & E_BadHit) << 3) | ((0x000f & T_Fine) << 4));
	dbg_msg[3]= 0xFF&(((0x0FF0 & ECCM) >> 4));
	dbg_msg[2]= 0xFF&(((0x7800 & ECCS) >> 11) | ((0x000f & ECCM) << 4));
	dbg_msg[1]= 0xFF&(((0x07f8 & ECCS) >> 3));
	dbg_msg[0]= 0xFF&((0x1f & E_Fine) | (0x0007 & ECCS) << 5);


#ifdef DEBUG
	printf("Writing debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",dbg_msg[i]);
	printf("\n");
#endif
	iface->Command(0,'D',15,dbg_msg,15,return_msg);

#ifdef DEBUG
	printf("Returned debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",return_msg[i]);
	printf("\n");
#endif
}


void STiC3_Debug::read_data_flow(unsigned short buffer, read_cmd command)
{
	bzero(dbg_msg,11);
	int ret_val;

	switch (command)
	{
		case READ_DSTORING_IN:
			dbg_msg[10]=CMD_READ_DSTORING_IN;
			break;
		case READ_L1_IN:
			dbg_msg[10]=CMD_READ_L1_FIFO_IN;
			break;
		case READ_L1_OUT:
			dbg_msg[10]=CMD_READ_L1_FIFO_OUT;
			break;
		case READ_GROUP_SEL_OUT:
			dbg_msg[10]=CMD_READ_GROUP_SEL_OUT;
			break;
		case READ_MS_SEL_OUT:
			dbg_msg[10]=CMD_READ_MS_SEL_OUT;
			break;
		case READ_L2_FIFO_OUT:
			dbg_msg[10]=CMD_READ_L2_FIFO_OUT;
			break;

		default: dbg_msg[10]=0x00;
			 break;
	}

	dbg_msg[0]=0x03&buffer;



#ifdef DEBUG
	printf("Writing debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",dbg_msg[i]);
	printf("\n");
#endif
	ret_val=iface->Command(0,'D',15,dbg_msg,15,return_msg);
	if (ret_val<0){
		printf("Retry of the command\n");
		ret_val=iface->Command(0,'D',15,dbg_msg,15,return_msg);
		if (ret_val<0) printf("Failed\n");
	}


#ifdef DEBUG
	printf("Returned debug message: ");
	for(int i=10;i>=0;i--) printf("%02X ",return_msg[i]);
	printf("\n");
#endif

	return_string.clear();
	return_string=return_msg;

	return;

}


unsigned short STiC3_Debug::get_bit(char* buffer, unsigned short bit_num)
{

	unsigned short byte_number;
	unsigned short bit;

	char bit_shift;

	byte_number = bit_num/8;
	bit = bit_num % 8;


	bit_shift=0x1&(buffer[byte_number]>>bit_num);

//	printf("Result of get_bit(bit_number %u): byte_num: %u, bit: %u, result: %u\n",bit_num,byte_number,bit,bit_shift);
	return bit_shift;
}


void STiC3_Debug::run_dflow(unsigned short buffer, const char *out_filename)
{
	//MAKE SURE THE DATA FLOW MONITORING IS ENABLED
	int i,j;

	en_dflow_mon();
	for(i=0;i<10;i++) step_debug_logic();

	//INJECT THE DATA
	inject_l1_data(buffer);
	for(i=0;i<4;i++) step_debug_logic();
	for(i=0;i<2;i++) step_core_logic();

	step_debug_logic();

	FILE *f;
	f= fopen(out_filename,"w");





	fprintf(f,"$timescale 1ps $end\n");
	fprintf(f,"$scope dflow logic $end\n");
	//L1 FIFO OUT
	fprintf(f,"$var wire 1 # L1_fifo_empty $end\n");
	fprintf(f,"$var wire 1 $ L1_fifo_ren $end\n");
	fprintf(f,"$var wire 78 & L1_fifo_dout $end\n");
	//L1 FIFO IN
	fprintf(f,"$var wire 78 ' L1_fifo_din $end\n");   //MUXOUTEXTENDED_S
	fprintf(f,"$var wire 1 ) L1_fifo_wen $end\n");
	fprintf(f,"$var wire 1 ( L1_any_dready $end\n");
	//GS_OUT
	fprintf(f,"$var wire 78 ! GroupSelect_dout $end\n");
	fprintf(f,"$var wire 1 [ GroupSelect_MS_ready $end\n");
	fprintf(f,"$var wire 1 %% GroupSelect_dready $end\n");
	fprintf(f,"$var wire 48 ^ MS_dout $end\n");
	fprintf(f,"$var wire 1 ~ MS_wen $end\n");
	fprintf(f,"$var wire 48 @ L2_fifo_dout $end\n");
	fprintf(f,"$var wire 1 ] L2_fifo_ren $end\n");
	fprintf(f,"$upscope $end\n");
	fprintf(f,"$enddefinitions $end\n");



	for(i=0;i<1024;i++)
	{
		fprintf(f,"#%u\n",i*625);

		//READ THE VALUE AT THE L1 BUFFER INPUT
		read_data_flow(buffer,READ_L1_IN);
		for(j=0;j<10;j++) step_debug_logic();
		read_data_flow(buffer,READ_L1_IN);
		for(j=0;j<10;j++) step_debug_logic();


		//WRITE THE L1_DATA IN VALUES 
		fprintf(f,"%u(\n",get_bit(return_msg,79));
		fprintf(f,"%u)\n",get_bit(return_msg,78));
		fprintf(f,"b");
		for(j=77;j>=0;j--) fprintf(f,"%u",get_bit(return_msg,(unsigned short)j));
		fprintf(f," '\n");

		//READ THE VALUE AT THE L1 BUFFER OUTPUT
		read_data_flow(buffer,READ_L1_OUT);
		for(j=0;j<10;j++) step_debug_logic();
		read_data_flow(buffer,READ_L1_OUT);
		for(j=0;j<10;j++) step_debug_logic();

		//WRITE THE L1 DATA OUT VALUES
		fprintf(f,"%u#\n",get_bit(return_msg,79));
		fprintf(f,"%u$\n",get_bit(return_msg,78));
		fprintf(f,"b");
		for(j=77;j>=0;j--) fprintf(f,"%u",get_bit(return_msg,j));
		fprintf(f," &\n");

		//READ THE VALUE AT THE GROUP SELECT OUTPUT
		read_data_flow(buffer,READ_GROUP_SEL_OUT);
		for(j=0;j<10;j++) step_debug_logic();
		read_data_flow(buffer,READ_GROUP_SEL_OUT);
		for(j=0;j<10;j++) step_debug_logic();

		//WRITE THE GROUP SELECT DATA OUT VALUES
		fprintf(f,"%u[\n",get_bit(return_msg,79));
		fprintf(f,"%u%%\n",get_bit(return_msg,78));
		fprintf(f,"b");
		for(j=77;j>=0;j--) fprintf(f,"%u",get_bit(return_msg,j));
		fprintf(f," !\n");


		//READ THE VALUE AT THE MS SELECT OUTPUT
		read_data_flow(buffer,READ_MS_SEL_OUT);
		for(j=0;j<10;j++) step_debug_logic();
		read_data_flow(buffer,READ_MS_SEL_OUT);
		for(j=0;j<10;j++) step_debug_logic();

		//WRITE THE MS SELECT DATA OUT VALUES
		fprintf(f,"%u~\n",get_bit(return_msg,48));
		fprintf(f,"b");
		for(j=47;j>=0;j--) fprintf(f,"%u",get_bit(return_msg,j));
		fprintf(f," ^\n");


		//READ THE VALUE AT THE L2 BUFFER OUTPUT
		read_data_flow(buffer,READ_MS_SEL_OUT);
		for(j=0;j<10;j++) step_debug_logic();
		read_data_flow(buffer,READ_MS_SEL_OUT);
		for(j=0;j<10;j++) step_debug_logic();

		//WRITE THE L2 BUFFER DATA OUT VALUES
		fprintf(f,"%u]\n",get_bit(return_msg,48));
		fprintf(f,"b");
		for(j=47;j>=0;j--) fprintf(f,"%u",get_bit(return_msg,j));
		fprintf(f," @\n");


		step_core_logic();
		step_debug_logic();


	}


	fclose(f);

	return;

}


//PYTHON WRAPPER FOR THE MODULE
BOOST_PYTHON_MODULE(STiC3_Debug)
{
	using namespace boost::python;
	class_<STiC3_Debug>("STiC3_Debug", init<char*>())
		.def("step_debug_logic",&STiC3_Debug::step_debug_logic)
		.def("step_core_logic",&STiC3_Debug::step_core_logic)
		.def("set_en_sysclk",&STiC3_Debug::set_en_sysclk)
		.def("set_dig_mon_mux",&STiC3_Debug::set_dig_mon_mux)
		.def("ena_dig_mon",&STiC3_Debug::ena_dig_mon)
		.def("dis_dig_mon",&STiC3_Debug::dis_dig_mon)
		.def("en_dflow_mon",&STiC3_Debug::en_dflow_mon)
		.def("dis_dflow_mon",&STiC3_Debug::dis_dflow_mon)
		.def("inject_l1_data",&STiC3_Debug::inject_l1_data)
		.def("read_data_flow",&STiC3_Debug::read_data_flow)
		.def("run_dflow",&STiC3_Debug::run_dflow)
		.def_readonly("return_string",&STiC3_Debug::return_string)
		.def_readwrite("channel",&STiC3_Debug::channel)
		.def_readwrite("T_BadHit",&STiC3_Debug::T_BadHit)
		.def_readwrite("TCCM",&STiC3_Debug::TCCM)
		.def_readwrite("TCCS",&STiC3_Debug::TCCS)
		.def_readwrite("T_Fine",&STiC3_Debug::T_Fine)
		.def_readwrite("E_BadHit",&STiC3_Debug::E_BadHit)
		.def_readwrite("ECCM",&STiC3_Debug::ECCM)
		.def_readwrite("ECCS",&STiC3_Debug::ECCS)
		.def_readwrite("E_Fine",&STiC3_Debug::E_Fine)
		;
	enum_<mon_mux>("mon_mux")
		.value("BYTE_CLK", BYTE_CLK)
		.value("L1_DREADY0",L1_DREADY0)
		.value("L1_DREADY1",L1_DREADY1)
		.value("L1_DREADY2",L1_DREADY2)
		.value("L1_DREADY3",L1_DREADY3)
		.value("L1_FIFO_EMPTY0",L1_FIFO_EMPTY0)
		.value("L1_FIFO_EMPTY1",L1_FIFO_EMPTY1)
		.value("L1_FIFO_EMPTY2",L1_FIFO_EMPTY2)
		.value("L1_FIFO_EMPTY3",L1_FIFO_EMPTY3)
		.value("L1_FIFO_FULL0",L1_FIFO_FULL0)
		.value("L1_FIFO_FULL1",L1_FIFO_FULL1)
		.value("L1_FIFO_FULL2",L1_FIFO_FULL2)
		.value("L1_FIFO_FULL3",L1_FIFO_FULL3)
		.value("L2_FIFO_EMPTY",L2_FIFO_EMPTY)
		.value("L2_FIFO_FULL",L2_FIFO_FULL)
		.value("INIT_TRANS",INIT_TRANS)
		;

	enum_<read_cmd>("read_cmd")
		.value("READ_DSTORING_IN",READ_DSTORING_IN)
		.value("READ_L1_IN",READ_L1_IN)
		.value("READ_L1_OUT",READ_L1_OUT)
		.value("READ_GROUP_SEL_OUT",READ_GROUP_SEL_OUT)
		.value("READ_MS_SEL_OUT",READ_MS_SEL_OUT)
		.value("READ_L2_FIFO_OUT",READ_L2_FIFO_OUT)
		;
}


