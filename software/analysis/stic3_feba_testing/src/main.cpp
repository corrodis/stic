#include <iostream>
#include "TSTiC3_Ana.h"

//Root Includes
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TH1F.h"



int main(int argc, char *argv[])
{

	if (argc<3){
		printf("use: %s file channel\n",argv[0]);
		return -1;
	}




//	TH1F* hist_user;

	unsigned int channel=atoi(argv[2]);
	unsigned int entries;

	TFile f(argv[1]);
	TTree *tree=(TTree*)f.Get("dump");

	if(channel<64) entries=tree->GetEntries(Form("handleID==0 && channel==%d",channel));
	else if(channel<128) entries=tree->GetEntries(Form("handleID==1 && channel==%d",channel-64));

	if (tree==NULL)
		return -1;

	TSTiC3_Ana *analysis = new TSTiC3_Ana(tree);

	analysis->gen_hist_energy();

	analysis->find_peak(channel);


//	analysis->print_einfo();

	energy_info einf = analysis->get_energy_info(channel);
	printf("energy mean : %f\n",einf.sigma);



	//TFile* fout=new TFile("coincidence_result.root","RECREATE");

	analysis->add_coincidence_channel(channel,0);
	analysis->gen_period();

	TH1F* current_hist;

	unsigned int max_bin=0;
	current_hist=analysis->get_period(channel);
	if(current_hist != NULL) max_bin=current_hist->GetMaximumBin();
	else printf("Error: period measurement returned NULL pointer!\n");

	printf("Maximum period bin: %f ns\n",max_bin*1.0/622.08e6/32.0*1e9);

	f.Close();


	std::ofstream outfile;
	outfile.open("characteristics.dat");

	outfile << channel << " " << entries << " " << einf.mean << " " << einf.sigma << " " << max_bin*1.0/622.08e6/32.0*1e9 << std::endl;
	outfile.close();



	return 0;
}
