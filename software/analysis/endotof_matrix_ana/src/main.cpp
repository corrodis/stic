#include <iostream>
#include "TSTiC2_Ana.h"

//Root Includes
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TH1F.h"


void* my_eval( map<unsigned int, list<stic_data_t> > *map_events)	//Example for evaluation of mapped event data
{
	list<stic_data_t> l_ev; //for abbreviation in the find statement

	list<stic_data_t>::iterator it;
	list<stic_data_t>::iterator findit;

	unsigned int time0;

	unsigned short ch1,ch2;
	ch1=1;
	ch2=35;

	TH1F* h1=new TH1F("usergen_hist","userhist",4001,-2000,2000); //allocate the new histogram

	map<unsigned int, list<stic_data_t> >::iterator it_event_seq;

	for(it_event_seq=map_events->begin(); it_event_seq!=map_events->end(); ++it_event_seq)
	{
		l_ev=it_event_seq->second;

		if (l_ev.size()>1)
		{

			//Check if the channels are in the list and get the coincidence time
			if (find(l_ev.begin(),l_ev.end(),ch1) != l_ev.end() && (find(l_ev.begin(),l_ev.end(),ch2) != l_ev.end()))
			{
				findit=find(it_event_seq->second.begin(),it_event_seq->second.end(),ch1);
				if(findit->energy>135+10 || findit->energy<135-10) continue;

				time0=findit->time;
				findit=find(it_event_seq->second.begin(),it_event_seq->second.end(),ch2);
				if(findit->energy>180+10 || findit->energy<180-10) continue;
				h1->Fill((int)time0 - (int)findit->time);

			}
		}
	}

	return h1;
}



int main(int argc, char *argv[])
{

	if (argc<3){
		printf("use: %s file treename\n",argv[0]);
		return -1;
	}


	TH1F* hist_user;

	TFile f(argv[1]);
	TTree *tree=(TTree*)f.Get(argv[2]);

	if (tree==NULL)
		return -1;

	TSTiC2_Ana *analysis = new TSTiC2_Ana(tree);

//	f.Close();
	analysis->gen_hist_energy();

//	for(int i=0;i<16;i++) analysis->find_peak(i);
//	for(int i=32;i<48;i++) analysis->find_peak(i);

	analysis->find_peak(40);
	analysis->find_peak(33);

	analysis->print_einfo();



	TFile* fout=new TFile("coincidence_result.root","RECREATE");


//	for(int i=0;i<16;i++) analysis->add_coincidence_channel(i,0);
//	for(int i=32;i<48;i++) analysis->add_coincidence_channel(i,1);

	analysis->add_coincidence_channel(33,0);
	analysis->add_coincidence_channel(40,1);

	analysis->search_coincidence();
	analysis->fit_cspects();

	int mapping[16];

	mapping[0]=11;
	mapping[1]=13;
	mapping[2]=15;
	mapping[3]=9;
	mapping[4]=10;
	mapping[5]=8;
	mapping[6]=12;
	mapping[7]=14;
	mapping[8]=4;
	mapping[9]=6;
	mapping[10]=2;
	mapping[11]=0;
	mapping[12]=5;
	mapping[13]=3;
	mapping[14]=1;
	mapping[15]=7;


	TH1F* current_hist;
	TCanvas *c_energy = new TCanvas("energy_m1","Matrix1");
	c_energy->Divide(4,4);
	for(int i=0;i<16;i++)
	{
		c_energy->cd(i+1);
		current_hist=analysis->get_hist_energy(mapping[i]);
		if(current_hist != NULL)
		{
			current_hist->Draw();
			current_hist->Write();
		}
	}

	c_energy->Write();
	delete c_energy;

	c_energy = new TCanvas("energy_m2","Matrix2");
	c_energy->Divide(4,4);
	for(int i=32;i<48;i++)
	{
		c_energy->cd(i+1-32);
		current_hist=analysis->get_hist_energy(mapping[i-32]+32);
		if(current_hist != NULL)
		{
			current_hist->Draw();
			current_hist->Write();
		}
	}


	for(int i=0;i<16;i++)
	{
		for(int j=32;j<48;j++)
		{
			current_hist=analysis->get_hist_cspect(i,j);
			if(current_hist != NULL) current_hist->Write();
		}
	}
	current_hist=analysis->get_hist_cspect(33,40);
	if(current_hist != NULL) current_hist->Write();

	c_energy->Write();
//	hist_user->Write();

	fout->Close();

	f.Close();


	return 0;
}
