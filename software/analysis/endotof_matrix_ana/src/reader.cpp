#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TF1.h"
#include "TFile.h"
#include "stdio.h"
#include "TSystem.h"
#include "EventDict.h"
#include "EventType.h"
#include "TFile.h"
#include "TKDE.h"
#include "TMath.h"
#include "Math/RootFinder.h"
#include "Math/WrappedTF1.h"
#include <list>
#include <map>
#include <sstream>
#include <iostream>

using namespace std;


/*
 * Comparison function between stic events which channel is larger
 */
bool compare_channel(stic_data_t& first, stic_data_t& second)
{
	return first.channel < second.channel;	
}

/* Function to fill a map with events in the same frame number
 * Allowing to search more efficiently for coincidences
 */
void fill_map(TTree* tree, map<unsigned int, list<stic_data_t> > *event_seq)
{

	stic_data_t* event=NULL;
	list<stic_data_t> empty_list; //TEMPORARY LIST FOR INSERTING IN THE MAP

	tree->SetBranchAddress("br",&event);

	unsigned int last_frame, frame_seq;
	frame_seq=0;
	last_frame=0;

	for (int i=0; i<tree->GetEntries(); i++)
	{

		if (last_frame != event->frame_number)
		{
			frame_seq++;	//INCREMENT THE FRAME SEQUENCE IF NEEDED
			(*event_seq)[frame_seq]=empty_list;
		}
		last_frame = event->frame_number;

		tree->GetEntry(i);
		(*event_seq)[frame_seq].push_back(*event);	//COPY THE EVENT TO THE LIST
//		cout << event->channel << endl;
	}
}

int find_channel(list<stic_data_t> ev_list, int channel)
{
	list<stic_data_t>::iterator it;
	for(it=ev_list.begin(); it!=ev_list.end();++it)
	{
		if(it->channel==channel) return 1;
	}
	return -1;
}


int main(int argc, char *argv[]){

	if (argc<3){
		printf("use: %s file treename\n",argv[0]);
		return -1;
	}


	TFile f(argv[1]);
	TTree *tree=(TTree*)f.Get(argv[2]);

	if (tree==NULL)
		return -1;


	vector<TH1F*> hist_vec(16);

	//map<int, map<int, TH1F*> > timediff_map;

	map<unsigned int, list<stic_data_t> > event_seq; 
	map<unsigned int, list<stic_data_t> >::iterator event_it; 



	TObjArray *branches=tree->GetListOfBranches();

	for(int i=0; i<branches->GetEntries(); i++){
		TBranch * thisBranch=(TBranch*)(*branches)[i];
		printf("branch \"%s\" has %lld entries\n",thisBranch->GetName(),thisBranch->GetEntries());
	}


	TH1F *energy_spect;
	TH1F *timespect = new TH1F("t_diff","Timedifference Spectrum",501,-250,250);

	ostringstream hist_name;
	ostringstream hist_title;

	//Create histograms
	for(int i=0;i<16;i++)
	{
		hist_name.clear();
		hist_name.str("");
		hist_name << "ToT" << i;

		hist_title.clear();
		hist_title.str("");
		hist_title << "ToT Spectrum Ch" << i;

		energy_spect = new TH1F(hist_name.str().c_str(),hist_title.str().c_str(),400,0,400);
		energy_spect->GetXaxis()->SetTitle("ToT [TDC Bins]");
		energy_spect->GetYaxis()->SetTitle("Entries");
		hist_vec[i]=energy_spect;
	}



	fill_map(tree, &event_seq);
	list<stic_data_t>::iterator it;

	int time0;

	for(event_it=event_seq.begin(); event_it!=event_seq.end(); ++event_it)
	{

		for(it = event_it->second.begin(); it!=event_it->second.end(); ++it)
		{
			//cout << it->channel << endl;
			hist_vec[it->channel]->Fill(it->energy);
		}

		if( event_it->second.size() > 1 ) 
		{
			event_it->second.sort(compare_channel); //SORT TO HAVE THE LOWEST CHANNEL FIRST

			it=event_it->second.begin();
			if(it->channel==7 )
			{
				time0=it->time;
				while(it!=event_it->second.end()) 
				{
					it++;
					if(it->channel==15)
					{
						timespect->Fill((int)time0 - (int)it->time);
						cout << time0 << " - " << it->time << " = "; 
						cout << (int)time0-(int)it->time << endl;
					}
				}
			}
		}

	}

	printf("filled the ToT histogram\n");


	TFile* fout=new TFile("coincidence_result.root","RECREATE");

	TCanvas *c_energy = new TCanvas;
	c_energy->Divide(4,4);
	for(int i=0;i<16;i++)
	{
		c_energy->cd(i+1);
		hist_vec[i]->Draw();
		hist_vec[i]->Write();
	}


	timespect->Write();

	c_energy->Write();

	fout->Close();

	return 0;
}
