for i in ~/measurement_data/STiC2/Coicidence_measurements/EndoTOF_Matrizen/22012014/Bias_Scan0/*.root; do
	if [ -f $i.fitlog ]; then
		echo "Results for ${i} already existing"
	else
		echo "Evaluating file ${i}"
		 ./main $i dump;
		 mv coincidence_result.root `echo $i | sed -e 's/scan_/scan_eval_/g'`;
		 mv cspect_fit.log $i.fitlog;
	fi;
done
