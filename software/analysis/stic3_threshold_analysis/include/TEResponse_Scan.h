//CLASS TO EVALUATE THE PERIOD JITTER AS A FUNCTION OF A SCAN PARAMETER #ifndef __TJITTER_SCAN_H__
#ifndef __TERESPONSE_SCAN_H__
#define __TERESPONSE_SCAN_H__

#include "TVirtual_Scan.h"
#include <string>

using namespace std;

class TEResponse_Scan: public TVirtual_Scan
{
	public:
		TEResponse_Scan(char* outfile);
		~TEResponse_Scan();

		/*
		 * Function to record the jitter values for each parameter setting
		 */
		void find_energy();


	private:
		double create_Eresponse(string dac_setting);
		TF1* measure_energy(string file, string hist_name);
};

#endif
