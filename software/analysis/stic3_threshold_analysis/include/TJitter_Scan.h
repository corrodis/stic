//CLASS TO EVALUATE THE PERIOD JITTER AS A FUNCTION OF A SCAN PARAMETER #ifndef __TJITTER_SCAN_H__
#ifndef __TJITTER_SCAN_H__
#define __TJITTER_SCAN_H__

#include "TVirtual_Scan.h"
#include <string>

using namespace std;

class TJitter_Scan: public TVirtual_Scan
{
	public:
		TJitter_Scan(char* outfile);
		~TJitter_Scan();
		/*
		 * Function to record the jitter values for each parameter setting
		 */
		void find_jitter();

	private:
		double get_min_jitter(string dac_setting);
		double measure_period(string file, string hist_name);

};

#endif
