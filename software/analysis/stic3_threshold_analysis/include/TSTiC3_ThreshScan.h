/*
 * Data analysis class for STiC3 readout data
 *
 */

#ifndef __TSTIC3_THRESHSCAN_H_
#define __TSTIC3_THRESHSCAN_H_


#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TF1.h"
#include "TFile.h"
#include "stdio.h"
#include "TSystem.h"

#include "EventType.h"

//#ifndef __CINT__
//#include "EventDict.h"
//#endif

#include <stdlib.h>
#include <unistd.h>
#include <list>
#include <map>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>

#include "TVirtual_Scan.h"


using namespace std;

class TSTiC3_ThreshScan : public TVirtual_Scan
{
	public:
		//Functions

		TSTiC3_ThreshScan(const char* out);
		~TSTiC3_ThreshScan(); //!< Destructor, removing the allocated root histograms

		/*
		 * Create an efficiency plot from the files in the given folder. The graph is written to the root output file
		 * \param record_folder: folder containing the recorded files
		 */
		double create_efficiency_graph(string dac_setting);


		/*
		 * Walk through the found DAC settings and create the efficiency plots for each setting
		 * Saves the created Graphs and a S-Curve fit in a subdirectory of the specified root file
		 */
		void find_thresholds();

	private:
		stic3_data_t* current_event;


};

#endif
