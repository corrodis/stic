/*
 * Data analysis class for STiC3 readout data
 *
 */

#ifndef __TVIRTUAL_SCAN_H_
#define __TVIRTUAL_SCAN_H_


#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TF1.h"
#include "TFile.h"
#include "stdio.h"
#include "TSystem.h"

#include "EventType.h"

//#ifndef __CINT__
//#include "EventDict.h"
//#endif

#include <stdlib.h>
#include <unistd.h>
#include <list>
#include <map>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <sys/stat.h>
#include <dirent.h>




using namespace std;

class TVirtual_Scan
{
	public:
		//Functions

		TVirtual_Scan(const char* out);
		~TVirtual_Scan(); //!< Destructor, removing the allocated root histograms

		/*
		 * Set the directory containing the recorded files
		 */
		void set_file_directory(const char* directory);

		void set_delimiter(const char* file_pattern,unsigned short n_delimiter);

		void set_output_file(const char* output);

		/*
		 * Create a map of sub_directories to a setting value.
		 * The setting is enclosed in the folder name between the specified delimiter (default: '_')
		 */
		void create_directory_map();

		/*
		 * Create a map of files in the given folder to a setting. The value of the setting is extracted from
		 * the filename between the delimiter and the '.' suffix
		 */
		void create_file_map(string folder);

		/*
		 * Create an efficiency plot from the files in the given folder. The graph is written to the root output file
		 * \param record_folder: folder containing the recorded files
		 */
		double create_efficiency_graph(string dac_setting);


		/*
		 * Walk through the found DAC settings and create the efficiency plots for each setting
		 * Saves the created Graphs and a S-Curve fit in a subdirectory of the specified root file
		 */
		void find_thresholds();



		//Containers public to make inheritance easier

		//Container mapping a setting to a subfolder
		std::map<double, string> map_recorddirs;

		//Container mapping a setting to a file
		std::map<double, string> map_recordfiles;

		string outfile;
		TFile *root_file;

		stic3_data_t* current_event;

	private:





		//Containers and variables needed for the evaluation
		std::string filepath;
		std::string dir_name;
		std::string f_name;
		std::string delim;

		//n_delim might be used in later implementations
		unsigned short n_delim;


};

#endif
