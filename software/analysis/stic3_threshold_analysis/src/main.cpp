#include "TSTiC3_ThreshScan.h"
#include "TJitter_Scan.h"
#include "TEResponse_Scan.h"

int main(int argc, char *argv[])
{

	if (argc<2){
		printf("use: %s directory\n",argv[0]);
		return -1;
	}

	TSTiC3_ThreshScan* ana_thresh= new TSTiC3_ThreshScan((char*)"out_thresh.root");
	ana_thresh->set_file_directory(argv[1]);
	ana_thresh->set_output_file("./out_thresh.root");
	ana_thresh->create_directory_map();
	ana_thresh->find_thresholds();

	TJitter_Scan* ana_jitter= new TJitter_Scan((char*)"out_jitter.root");
	ana_jitter->set_file_directory(argv[1]);
	ana_jitter->set_output_file("./out_jitter.root");
	ana_jitter->create_directory_map();
	ana_jitter->find_jitter();

	TEResponse_Scan* ana_Eres= new TEResponse_Scan((char*)"out_Eresponse.root");
	ana_Eres->set_file_directory(argv[1]);
	ana_Eres->set_output_file("./out_eresponse.root");
	ana_Eres->create_directory_map();
	ana_Eres->find_energy();

	return 0;
}
