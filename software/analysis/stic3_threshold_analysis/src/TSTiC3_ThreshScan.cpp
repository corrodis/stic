#include "TSTiC3_ThreshScan.h"
#include <sstream>
#include <progressbar.h>

//#define DEBUG
//Constructor
TSTiC3_ThreshScan::TSTiC3_ThreshScan(const char* out):TVirtual_Scan(out)
{
}


TSTiC3_ThreshScan::~TSTiC3_ThreshScan()
{
}


void TSTiC3_ThreshScan::find_thresholds()
{

	map< double, ::string >::iterator it_recorddirs;

#ifdef DEBUG
	cout << "Creating the S-Curves from the threshold scan\n";
#endif

	ProgressBar* pbar = new ProgressBar();


	stringstream s;
	s.str("");

	TGraph *threshold_graph = new TGraph();
	threshold_graph->SetName("threshold_values");

	unsigned int i=0;
	double threshold;

	pbar->start();

	for(it_recorddirs=map_recorddirs.begin();it_recorddirs!=map_recorddirs.end();++it_recorddirs)
	{
		create_file_map(it_recorddirs->second);
		s.str("");
		s << "DAC_VALUE_" << it_recorddirs->first;
#ifdef DEBUG
		cout << "Creating S-Curve for DAC Setting: " << it_recorddirs->first << " in " << it_recorddirs->second << endl;
#endif
		threshold = create_efficiency_graph(s.str());
		if(threshold >= 0){
			threshold_graph->SetPoint(i,it_recorddirs->first,threshold);
			i++;
		}
		pbar->status(i,map_recorddirs.size());
	}

	root_file->cd();
	threshold_graph->GetXaxis()->SetTitle("TThresh Setting [DAC Value]");
	threshold_graph->GetYaxis()->SetTitle("Threshold Level [mV]");
	threshold_graph->Write();


	return;
}


double TSTiC3_ThreshScan::create_efficiency_graph(string dac_setting)
{

	//Sanity check
	if(root_file==NULL){
		cout << "ERROR: No root output file open for writing the graphs!" << endl;
		return -1;
	}

	int i;

	stringstream function_name;
	string file_location;


	//a root file for reading the recorded data and a ttree pointer to get the number of events
	TFile *r_file;
	TTree *t;

	//allocate the TGraph and set an appropriate name
	TGraph *gr = new TGraph();
	gr->SetName(dac_setting.c_str());

	//variables used to insert into the graph
	unsigned int entries;
	unsigned int n_pos=0;



	//loop over the record_files and add the values to the TGraph


	map< double, ::string >::iterator it_records;
	
	for(it_records=map_recordfiles.begin();it_records!=map_recordfiles.end();++it_records)
	{
		file_location=it_records->second;

		//Get entries and add them to the TGraph
		r_file=new TFile(file_location.c_str());
		t=(TTree*)r_file->Get("dump");
		if(t==NULL) continue;   //safety measure
		entries=t->GetEntries();
		gr->SetPoint(n_pos,it_records->first,((double)entries)/1000.0);
		n_pos++;
		r_file->Close();
		delete r_file;
#ifdef DEBUG
		cout << "Added values from file for setting value: " << it_records->first << " with " << entries << " entries" <<endl;
#endif
	}

	function_name.str("");
	function_name << dac_setting << "_fit";
	TF1 *func = new TF1(function_name.str().c_str(),"0.5*(1+TMath::Erf((x-[0])/[1]))",0,300);

	func->SetParameter(0,gr->GetMean(1));
	func->SetParameter(1,10);
	gr->Fit(func,"QRW");

	function_name.str("");
	function_name << "TThresh_" << dac_setting;

	root_file->cd();
	root_file->mkdir(function_name.str().c_str(),"update");
	root_file->cd(function_name.str().c_str());

	gr->GetXaxis()->SetTitle("Pulse Height [mV]");
	gr->GetYaxis()->SetTitle("Efficiency");
	gr->Write();
	func->Write();

	double threshold_value = func->GetParameter(0);

	delete gr;
	delete func;

	return threshold_value;

}
