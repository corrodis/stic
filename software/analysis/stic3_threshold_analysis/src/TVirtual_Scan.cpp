#include "TVirtual_Scan.h"
#include <sstream>

//#define DEBUG
//Constructor
TVirtual_Scan::TVirtual_Scan(const char* out)
{
	root_file=NULL;
	outfile = out;
	delim="_";
}

TVirtual_Scan::~TVirtual_Scan()
{
	if(root_file != NULL){
		root_file->Close();
		delete root_file;
	}
}



void TVirtual_Scan::set_file_directory(const char* directory)
{
	dir_name=directory;
}

void TVirtual_Scan::set_delimiter(const char* delimiter, unsigned short n_delimiters)
{
	delim=delimiter;
	n_delim = n_delimiters;
}


void TVirtual_Scan::set_output_file(const char* output)
{
	outfile = output;
	cout << "New output file " << outfile << endl;
	if (root_file == NULL){
		root_file=new TFile(outfile.c_str(),"RECREATE");
	}
	else
	{
		root_file->Close();
		delete root_file;
		root_file=new TFile(outfile.c_str(),"RECREATE");
	}
}


void TVirtual_Scan::create_file_map(string folder)
{


	if(folder.length()==0){
		cout << "You have to specify a folder!" << endl;
	}

	//Clear the recordfiles map
	map_recordfiles.clear();

	//File structure classes for browsing the directory
	DIR* dp;
	struct dirent *dir_entries;
	struct stat filestat;

	string filename;

	//delimiter positions in the file names to extract the recording parameter from the filename
	std::size_t delim_pos1, delim_pos2;

	//variables used to insert into the graph
	double setting_val;

	//go to the directory containing the measurement files
	dp = opendir(folder.c_str());
	if ( dp==NULL){
		cout << "Error: when opening " << folder;
		return;
	}

	while ((dir_entries = readdir( dp )))
	{
		filepath = folder + "/" + dir_entries->d_name;

		// If the file is a directory (or is in some way invalid) we'll skip it
		if (stat( filepath.c_str(), &filestat ) < 0){
			perror("Error: ");
			continue;
		}
		if (S_ISDIR( filestat.st_mode )){
			continue;
		}


		delim_pos1 = filepath.find_last_of("/");

		filename=filepath.substr(delim_pos1+1);
#ifdef DEBUG
		cout << "Checking file " << filename << endl;
#endif

		delim_pos1 = filename.find(delim.c_str());

		if(delim_pos1 == std::string::npos){
			cout << "No delimiter found in " << f_name << endl;
			continue;
		}
		//FIND THE SUFFIX
		delim_pos2 = filename.find(".root",delim_pos1+1);
		setting_val=atof((filename.substr(delim_pos1+1,(delim_pos2 -delim_pos1)-1)).c_str());

		// Add the setting and filepath to the recordfile map
		map_recordfiles[setting_val]=filepath;
#ifdef DEBUG
		cout << "Found data file for setting value: " << setting_val << " at " << filepath << endl;
#endif
	}

	closedir( dp );

}


void TVirtual_Scan::create_directory_map()
{

	DIR* dp;

	struct dirent *dir_entries;
	struct stat filestat;

	dp = opendir(dir_name.c_str());
	double dac_val;
	std::size_t delim_pos1, delim_pos2;

#ifdef DEBUG
	cout << "Looking for subfolders in directory " << dir_name << endl;
#endif

	if ( dp==NULL){
		cout << "Error: when opening " << dir_name;
		return;
	}


	while ((dir_entries = readdir( dp )))
	{
		filepath = dir_name + "/" + dir_entries->d_name;

		// If the file is a directory (or is in some way invalid) we'll skip it
		if (stat( filepath.c_str(), &filestat )) continue;
		if (S_ISDIR( filestat.st_mode )){
#ifdef DEBUG
			cout << filepath << endl;
#endif
		}
		else continue;	//Continue if the file is not a directory

		delim_pos1 = filepath.find_last_of("/");

		f_name=filepath.substr(delim_pos1+1);

		delim_pos1 = f_name.find(delim.c_str());
		if(delim_pos1 == std::string::npos){
#ifdef DEBUG
			cout << "No delimiter found in " << f_name << endl;
#endif
			continue;
		}
		delim_pos2 = f_name.find(delim.c_str(),delim_pos1+1);
		dac_val=atof((f_name.substr(delim_pos1+1,(delim_pos2 -delim_pos1)-1)).c_str());
#ifdef DEBUG
		cout << "Found scan for value: " << dac_val << " in path " << filepath << endl;
#endif

		map_recorddirs[dac_val]=filepath;

	}

	closedir( dp );
}
