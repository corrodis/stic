#include "TJitter_Scan.h"
#include "progressbar.h"
#include <map>


//#define DEBUG

TJitter_Scan::TJitter_Scan(char* outfile):TVirtual_Scan(outfile)
{
}

TJitter_Scan::~TJitter_Scan()
{
}

void TJitter_Scan::find_jitter()
{
	map< double, ::string >::iterator it_recorddirs;

#ifdef DEBUG
	cout << "Measuring the periods and determining the minimal jitter\n";
#endif

	root_file->mkdir("period_measurements");

	ProgressBar* pbar = new ProgressBar();


	stringstream s;
	s.str("");

	TGraph *jitter_graph = new TGraph();
	jitter_graph->SetName("Minimal Jitter_values");

	unsigned int i=0;
	double jitter;

	pbar->start();

	for(it_recorddirs=map_recorddirs.begin();it_recorddirs!=map_recorddirs.end();++it_recorddirs)
	{
		create_file_map(it_recorddirs->second);
		s.str("");
		s << "DAC_VALUE_" << it_recorddirs->first;
#ifdef DEBUG
		cout << "Measuring jitter values for DAC Setting: " << it_recorddirs->first << " in " << it_recorddirs->second << endl;
#endif
		jitter = get_min_jitter(s.str());
		if(jitter >= 0){
			jitter_graph->SetPoint(i,it_recorddirs->first,jitter);
			i++;
		}
		pbar->status(i,map_recorddirs.size());
	}

	root_file->cd();
	jitter_graph->Write();


	return;
}

double TJitter_Scan::measure_period(string file, string hist_name)
{


#ifdef DEBUG
	cout << "Opening root-file " << file << endl;
#endif

	stic3_data_t* event=NULL;
	stic3_data_t* last_event= new stic3_data_t;

	TFile *f = new TFile(file.c_str());

	TTree* tree=(TTree*) f->Get("dump"); //Get the TTree from the given file
	if(tree==NULL){
		cout << "TTree could not be resolved" << endl;
		return -1;
	}
	
	TH1F* h1 = new TH1F(hist_name.c_str(),"Period Measurement",1<<20,0,1<<20);

	
#ifdef DEBUG
	cout << "Root file opened and tree returned" << endl;
#endif

	tree->SetBranchAddress("br",&event);

	unsigned int entries;

	unsigned long int timediff;

	entries=tree->GetEntries();
	if(entries < 2) return -1;

#ifdef DEBUG
	cout << "TTree contains " << entries << " entries. Creating period measurement" << endl;
#endif

	tree->GetEntry(0);	//Get the first entry and copy it to last event, to start off

	memcpy(last_event,event,sizeof(stic3_data_t));	//copy the last event

	for (int i=1; i<tree->GetEntries(); i++)
	{

		tree->GetEntry(i);
		timediff=event->time - last_event->time;
#ifdef DEBUG
		cout << "Timedifference: " << event->time << " - " << last_event->time << " = " << timediff << endl;
#endif
		h1->Fill(timediff);

		memcpy(last_event,event,sizeof(stic3_data_t));	//copy the last event
	}


	//TODO: Closing the file causes a SEGFAULT, have to find out why...
//	f->Close();
//	delete f;

	unsigned long int max_bin;
	double jitter_value;
	max_bin=h1->GetMaximumBin();

	if(h1->GetBinContent(max_bin) > 50){
		h1->Fit("gaus","QM","",max_bin-20,max_bin+20);
		TF1 *fitfunc=h1->GetFunction("gaus");
		jitter_value=fitfunc->GetParameter(2);
	}
	else jitter_value=-1.0;
	
	root_file->cd("/period_measurements");
	h1->Write();

#ifdef DEBUG
	cout << "Histograms written, deleting objects" << endl;
#endif
	delete h1;

	delete last_event;

#ifdef DEBUG
	cout << "Done calculating period for " << hist_name <<  endl;
#endif

	//TODO: return the actual jitter value of a gaus fit to the period peak
	return jitter_value;

}


double TJitter_Scan::get_min_jitter(string dac_setting)
{
	//Sanity check
	if(root_file==NULL){
		cout << "ERROR: No root output file open for writing the graphs!" << endl;
		return -1;
	}

	int i;

	stringstream function_name;
	string file_location;


	//Allocate a TGraph that shows the returned jitter values for each point
	TGraph *gr = new TGraph;
	gr->SetName(dac_setting.c_str());

	//variables used to insert into the graph
	unsigned int n_pos=0;
	double jitter_value;
	double jitter_min=100;



	//loop over the record_files and add the values to the TGraph


	map< double, ::string >::iterator it_records;
	
	for(it_records=map_recordfiles.begin();it_records!=map_recordfiles.end();++it_records)
	{
		file_location=it_records->second;

		function_name.str("");
		function_name << dac_setting << "_" << it_records->first;

		//Get entries and add them to the TGraph
		jitter_value=measure_period(file_location,function_name.str());

		//If there was an error continue
		if(jitter_value < 0) continue;

		gr->SetPoint(n_pos,it_records->first,jitter_value);

		if(jitter_value < jitter_min) jitter_min=jitter_value;
		n_pos++;
#ifdef DEBUG
		cout << "Added values from file for setting value: " << it_records->first << " with " << jitter_value <<" jitter" <<endl;
#endif
	}


	function_name.str("");
	function_name << "TThresh_" << dac_setting;

	root_file->cd();
	root_file->mkdir(function_name.str().c_str(),"update");
	root_file->cd(function_name.str().c_str());

	gr->Write();
	delete gr;

	//TODO: return here the minimal value of the jitter measurements
	return jitter_min;

}
