#include "TEResponse_Scan.h"
#include "progressbar.h"
#include <map>


//#define DEBUG

TEResponse_Scan::TEResponse_Scan(char* outfile):TVirtual_Scan(outfile)
{
}

TEResponse_Scan::~TEResponse_Scan()
{
}


//ENERGY SCANS
TF1* TEResponse_Scan::measure_energy(string file, string hist_name)
{


#ifdef DEBUG
	cout << "Opening root-file " << file << endl;
#endif

	stic3_data_t* event=NULL;
	TFile *f = new TFile(file.c_str());

	TTree* tree=(TTree*) f->Get("dump"); //Get the TTree from the given file
	if(tree==NULL){
		cout << "TTree could not be resolved" << endl;
		return NULL;
	}
	
	TH1F* h1 = new TH1F(hist_name.c_str(),"Energy Measurement",1<<15,0,1<<15);

	
#ifdef DEBUG
	cout << "Root file opened and tree returned" << endl;
#endif

	tree->SetBranchAddress("br",&event);

	unsigned int entries;

	entries=tree->GetEntries();
	if(entries < 2) return NULL;

#ifdef DEBUG
	cout << "TTree contains " << entries << " entries. Creating period measurement" << endl;
#endif

	for (int i=0; i<tree->GetEntries(); i++)
	{

		tree->GetEntry(i);
#ifdef DEBUG_ENERGY
		cout << "Energy: " << event->energy << endl;
#endif
		h1->Fill(event->energy);

	}


	//TODO: Closing the file causes a SEGFAULT, have to find out why...
//	f->Close();
//	delete f;

	unsigned long int max_bin;
	double energy_mean;
	max_bin=h1->GetMaximumBin();
	TF1* fitfunc = NULL;

	if(h1->GetBinContent(max_bin) > 50){
		h1->Fit("gaus","QM","",max_bin-20,max_bin+20);
		fitfunc=(TF1*)h1->GetFunction("gaus")->Clone("returned_fit");
	}
	
	root_file->cd("/energy_measurements/");
	h1->Write();

#ifdef DEBUG
	cout << "Histograms written, deleting objects" << endl;
#endif
	delete h1;

#ifdef DEBUG
	cout << "Done measuring energy for " << hist_name <<  endl;
#endif

	//TODO: return the actual jitter value of a gaus fit to the period peak
	return fitfunc;

}


void TEResponse_Scan::find_energy()
{
	map< double, ::string >::iterator it_recorddirs;

#ifdef DEBUG
	cout << "Measuring the periods and determining the minimal jitter\n";
#endif

	root_file->mkdir("energy_measurements");

	ProgressBar* pbar = new ProgressBar();


	stringstream s;
	s.str("");

	double e_result;
	unsigned int i=0;

	pbar->start();

	for(it_recorddirs=map_recorddirs.begin();it_recorddirs!=map_recorddirs.end();++it_recorddirs)
	{
		create_file_map(it_recorddirs->second);
		s.str("");
		s << "DAC_VALUE_" << it_recorddirs->first;
#ifdef DEBUG
		cout << "Measuring energy values for DAC Setting: " << it_recorddirs->first << " in " << it_recorddirs->second << endl;
#endif
		e_result = create_Eresponse(s.str());
		pbar->status(i,map_recorddirs.size());
		i++;
	}

	return;
}

double TEResponse_Scan::create_Eresponse(string dac_setting)
{
	//Sanity check
	if(root_file==NULL){
		cout << "ERROR: No root output file open for writing the graphs!" << endl;
		return -1;
	}

	int i;

	stringstream function_name;
	string file_location;

	TCanvas *c1=new TCanvas();

	//Allocate a TGraph that shows the returned jitter values for each point
	TF1 *fit_function = NULL;
	TGraph *gr_emean = new TGraph;
	function_name.str("");
	function_name << dac_setting << "_EMean";
	gr_emean->SetName(function_name.str().c_str());

	gr_emean->GetXaxis()->SetTitle("Pulse Amplitude [mV]");
	gr_emean->GetYaxis()->SetTitle("Energy value [TDC Coarse-Bins]");

	function_name.str("");
	function_name << dac_setting << "_ESigma";
	TGraph *gr_esigma = new TGraph;
	gr_esigma->SetName(function_name.str().c_str());
	gr_esigma->GetXaxis()->SetTitle("Pulse Amplitude [mV]");
	gr_esigma->GetYaxis()->SetTitle("Energy resolution [TDC Coarse-Bins]");

	//variables used to insert into the graph
	unsigned int n_pos=0;
	double energy_mean;
	double energy_sigma;

	//loop over the record_files and add the values to the TGraph


	map< double, ::string >::iterator it_records;
	
	for(it_records=map_recordfiles.begin();it_records!=map_recordfiles.end();++it_records)
	{
		file_location=it_records->second;

		function_name.str("");
		function_name << dac_setting << "_" << it_records->first;

		//Get entries and add them to the TGraph
		fit_function=measure_energy(file_location,function_name.str());

		//If there was an error continue
		if(fit_function == NULL) continue;

		energy_mean=fit_function->GetParameter(1);
		energy_sigma=fit_function->GetParameter(2);
		gr_emean->SetPoint(n_pos,it_records->first,energy_mean);
		gr_esigma->SetPoint(n_pos,it_records->first,energy_sigma);
		n_pos++;
#ifdef DEBUG
		cout << "Added values from file for setting value: " << it_records->first << " with " << energy_mean <<" mean energy" <<endl;
#endif
	}


	function_name.str("");
	function_name << "Thresh_" << dac_setting;
	root_file->cd("energy_measurements");
	root_file->mkdir(function_name.str().c_str(),"update");
	root_file->cd(function_name.str().c_str());

	c1->cd();

	gr_emean->Write();
	gr_esigma->Write();
	delete gr_emean;

	return 0;

}
