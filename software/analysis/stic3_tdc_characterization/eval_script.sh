for i in ~/measurement_data/stic3_tdc/13042014/period_measurement_ch3/*.root; do
	new_file=`echo $i | sed -e 's/_period/_period_eval/g'`;
	if [ -f new_file ]; then
		echo "Results for ${i} already existing"
	else
		echo "Evaluating file ${i}"
		 ./main $i dump;
		 mv coincidence_result.root $new_file
#		 echo "mv cspect_fit.log $i.fitlog;"
	fi;
done
