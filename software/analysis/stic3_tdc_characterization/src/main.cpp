#include <iostream>
#include "TSTiC3_Ana.h"

//Root Includes
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TH1F.h"


void* my_eval( map<unsigned int, list<stic3_data_t> > *map_events)	//Example for evaluation of mapped event data
{
	list<stic3_data_t> l_ev; //for abbreviation in the find statement

	list<stic3_data_t>::iterator it;
	list<stic3_data_t>::iterator findit;

	unsigned int time0;

	unsigned short ch1,ch2;
	ch1=1;
	ch2=35;

	TH1F* h1=new TH1F("usergen_hist","userhist",4001,-2000,2000); //allocate the new histogram

	map<unsigned int, list<stic3_data_t> >::iterator it_event_seq;

	for(it_event_seq=map_events->begin(); it_event_seq!=map_events->end(); ++it_event_seq)
	{
		l_ev=it_event_seq->second;

		if (l_ev.size()>1)
		{

			//Check if the channels are in the list and get the coincidence time
			if (find(l_ev.begin(),l_ev.end(),ch1) != l_ev.end() && (find(l_ev.begin(),l_ev.end(),ch2) != l_ev.end()))
			{
				findit=find(it_event_seq->second.begin(),it_event_seq->second.end(),ch1);
				if(findit->energy>135+10 || findit->energy<135-10) continue;

				time0=findit->time;
				findit=find(it_event_seq->second.begin(),it_event_seq->second.end(),ch2);
				if(findit->energy>180+10 || findit->energy<180-10) continue;
				h1->Fill((int)time0 - (int)findit->time);

			}
		}
	}

	return h1;
}



int main(int argc, char *argv[])
{

	if (argc<3){
		printf("use: %s file treename\n",argv[0]);
		return -1;
	}


	TH1F* hist_user;

	TFile f(argv[1]);
	TTree *tree=(TTree*)f.Get(argv[2]);

	if (tree==NULL)
		return -1;

	TSTiC3_Ana *analysis = new TSTiC3_Ana(tree);

//	f.Close();
	analysis->gen_hist_energy();

//	for(int i=0;i<16;i++) analysis->find_peak(i);
//	for(int i=0;i<31;i++) analysis->find_peak(i);

	analysis->find_peak(22);
	

	analysis->print_einfo();



	TFile* fout=new TFile("coincidence_result.root","RECREATE");

//	analysis->add_coincidence_channel(22,0);
	for(int i=0;i<31;i++) analysis->add_coincidence_channel(i,0);
	analysis->gen_period();

//	analysis->search_coincidence();
	//analysis->fit_cspects();

	analysis->gen_dnl();
	analysis->gen_dnl_corrected();


	TH1F* current_hist;

	for(int i=0;i<32;i++){
		current_hist=analysis->get_hist_energy(i);
		if(current_hist != NULL)
		{
			current_hist->Write();
		}
	}


	for(int i=0;i<32;i++)
	{
		current_hist=analysis->get_hist_cspect(0,i);
		if(current_hist != NULL) current_hist->Write();
	}


	for(int i=0;i<32;i++){
		current_hist=analysis->get_period(i);
		if(current_hist != NULL) current_hist->Write();


		current_hist=analysis->get_dnl(i);
		if(current_hist != NULL) current_hist->Write();

		current_hist=analysis->get_inl(i);
		if(current_hist != NULL) current_hist->Write();

		current_hist=analysis->get_dnl_corrected(i);
		if(current_hist != NULL) current_hist->Write();

		current_hist=analysis->get_inl_corrected(i);
		if(current_hist != NULL) current_hist->Write();

		current_hist=analysis->get_cdt(i);
		if(current_hist != NULL) current_hist->Write();

		current_hist=analysis->get_cdt_corrected(i);
		if(current_hist != NULL) current_hist->Write();
	}



//	current_hist=analysis->get_dnl_corrected(8);
//	if(current_hist != NULL) current_hist->Write();
//
//	current_hist=analysis->get_inl_corrected(8);
//	if(current_hist != NULL) current_hist->Write();
//
//	current_hist=analysis->get_cdt(8);
//	if(current_hist != NULL) current_hist->Write();
//
//	current_hist=analysis->get_cdt_corrected(8);
//	if(current_hist != NULL) current_hist->Write();


	fout->Close();

	f.Close();


	return 0;
}
