set terminal eps

# Data Format of the scan.data
#  1: RUN - NO
#  2: DAC - BIAS
#  3: DAC - LADDER
#  4: DAC - ICOMP
#  5: DAC - ?
#  6: DAC - TBIAS
#  7: DAC - TTHRESHOLD
#  8: DAC - EBIAS
#  9: DAC - ETHRESHOLD
#  10: BIAS VOLTAGE (nominal 58.14)
# 11: CTR Sigma
# 12: CTR Sigma fiterror
# 13: E1 Position
# 14: E1 Position error
# 15: E2 Position
# 16: E2 Position error
# 17: number of coincidence events after energy cuts 
# 18: number of underflows
# 19: number of overflows

set linetype 1 lc rgb "#0EA857"		lw 1 pt 0
set linetype 2 lc rgb "#0EAD7E"		lw 1 pt 0
set linetype 3 lc rgb "#0EB2A6" 	lw 1 pt 0
set linetype 4 lc rgb "#0D9CB7" 	lw 1 pt 0
set linetype 5 lc rgb "#0D78BC" 	lw 1 pt 0
set linetype 6 lc rgb "#0C52C0" 	lw 1 pt 0
set linetype 7 lc rgb "#0B29C5" 	lw 1 pt 0
set linetype 8 lc rgb "#170BCA" 	lw 1 pt 0
set linetype 9 lc rgb "#440ACF" 	lw 1 pt 0
set linetype 10 lc rgb "#7309D4"	lw 1 pt 0
set linetype 11 lc rgb "#A408D9"	lw 1 pt 0
set linetype 12 lc rgb "#D807D0"	lw 1 pt 0
set linetype 13 lc rgb "#E207B5"	lw 1 pt 0
set linetype 14 lc rgb "#D21A26"	lw 1 pt 0
set linetype 15 lc rgb "#overflow0F22"	lw 1 pt 0
set linetype 16 lc rgb "#F1041F"	lw 1 pt 0


############# overflow vs TTHRES (BIAS) with fixed TB ############################
set xlabel "TTHRESH"
set ylabel "overflow [%]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
set key outside Right

set output "plots/scan_overflow_tthres_TBIAS-0.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 0)"
plot 	"scan.data" using ($6==0&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==0&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==0&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==0&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==0&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==0&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==0&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==0&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==0&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-1.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 1)"
plot 	"scan.data" using ($6==1&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==1&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==1&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==1&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==1&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==1&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==1&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==1&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==1&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-2.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 2)"
plot 	"scan.data" using ($6==2&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==2&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==2&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==2&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==2&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==2&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==2&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==2&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==2&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-3.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 3)"
plot 	"scan.data" using ($6==3&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==3&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==3&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==3&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==3&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==3&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==3&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==3&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==3&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-4.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 4)"
plot 	"scan.data" using ($6==4&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==4&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==4&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==4&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==4&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==4&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==4&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==4&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==4&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-5.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 5)"
plot 	"scan.data" using ($6==5&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==5&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==5&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==5&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==5&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==5&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==5&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==5&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==5&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-6.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 6)"
plot 	"scan.data" using ($6==6&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==6&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==6&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==6&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==6&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==6&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==6&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==6&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==6&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-7.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 7)"
plot 	"scan.data" using ($6==7&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==7&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==7&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==7&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==7&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==7&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==7&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==7&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==7&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-8.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 8)"
plot 	"scan.data" using ($6==8&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==8&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==8&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==8&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==8&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==8&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==8&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==8&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==8&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-9.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 9)"
plot 	"scan.data" using ($6==9&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==9&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==9&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==9&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==9&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==9&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==9&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==9&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==9&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-20.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 10)"
plot 	"scan.data" using ($6==10&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==10&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==10&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==10&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==10&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==10&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==10&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==10&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==10&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-11.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 11)"
plot 	"scan.data" using ($6==11&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==11&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==11&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==11&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==11&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==11&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==11&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==11&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==11&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-12.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 12)"
plot 	"scan.data" using ($6==12&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==12&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==12&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==12&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==12&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==12&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==12&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==12&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==12&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-13.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 13)"
plot 	"scan.data" using ($6==13&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==13&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==13&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==13&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==13&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==13&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==13&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==13&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==13&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-14.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 14)"
plot 	"scan.data" using ($6==14&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==14&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==14&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==14&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==14&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==14&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==14&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==14&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==14&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_TBIAS-28.eps"
set title "TThreshold Scan, (EDAC 12-13, TBIAS 15)"
plot 	"scan.data" using ($6==15&&$10==67.1&&$8==13&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($6==15&&$10==67.9&&$8==13&&$9==13?$7-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($6==15&&$10==68.7&&$8==13&&$9==13?$7-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==15&&$10==69.5&&$8==13&&$9==13?$7-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($6==15&&$10==60.14&&$8==13&&$9==13?$7     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($6==15&&$10==60.64&&$8==13&&$9==13?$7+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($6==15&&$10==61.14&&$8==13&&$9==13?$7+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($6==15&&$10==61.64&&$8==13&&$9==13?$7+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($6==15&&$10==62.14&&$8==13&&$9==13?$7+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"







############# overflowPosition vs TTHRES (TBIAS) with fixed BIAS ############################
set xlabel "TTHRESH"
set ylabel "overflow [%]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
set key outside Right

set output "plots/scan_overflow_tthres_BIAS-04.eps"
set title "TThreshold Scan (EDAC 12-13, BIAS +0.4V)"
plot 	"scan.data" using ($10==67.1&&$6==0&&$8==13&&$9==13?$7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($10==67.1&&$6==1&&$8==12&&$9==13?$7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($10==67.1&&$6==2&&$8==12&&$9==13?$7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($10==67.1&&$6==3&&$8==12&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($10==67.1&&$6==4&&$8==12&&$9==13?$7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($10==67.1&&$6==5&&$8==12&&$9==13?$7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($10==67.1&&$6==6&&$8==12&&$9==13?$7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($10==67.1&&$6==7&&$8==12&&$9==13?$7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($10==67.1&&$6==8&&$8==12&&$9==13?$7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($10==67.1&&$6==9&&$8==12&&$9==13?$7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($10==67.1&&$6==10&&$8==12&&$9==13?$7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($10==67.1&&$6==11&&$8==12&&$9==13?$7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($10==67.1&&$6==12&&$8==12&&$9==13?$7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($10==67.1&&$6==13&&$8==12&&$9==13?$7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($10==67.1&&$6==14&&$8==12&&$9==13?$7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($10==67.1&&$6==15&&$8==12&&$9==13?$7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_BIAS-12.eps"
set title "TThreshold Scan (EDAC 12-13, BIAS +1.2V)"
plot "scan.data" using ($10==67.9&&$6==0&&$8==13&&$9==13?$7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($10==67.9&&$6==1&&$8==12&&$9==13?$7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($10==67.9&&$6==2&&$8==12&&$9==13?$7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($10==67.9&&$6==3&&$8==12&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($10==67.9&&$6==4&&$8==12&&$9==13?$7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($10==67.9&&$6==5&&$8==12&&$9==13?$7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($10==67.9&&$6==6&&$8==12&&$9==13?$7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($10==67.9&&$6==7&&$8==12&&$9==13?$7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($10==67.9&&$6==8&&$8==12&&$9==13?$7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($10==67.9&&$6==9&&$8==12&&$9==13?$7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($10==67.9&&$6==10&&$8==12&&$9==13?$7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($10==67.9&&$6==11&&$8==12&&$9==13?$7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($10==67.9&&$6==12&&$8==12&&$9==13?$7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($10==67.9&&$6==13&&$8==12&&$9==13?$7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($10==67.9&&$6==14&&$8==12&&$9==13?$7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($10==67.9&&$6==15&&$8==12&&$9==13?$7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_BIAS-20.eps"
set title "TThreshold Scan (EDAC 12-13, BIAS +2.0V)"
plot "scan.data" using ($10==68.7&&$6==0&&$8==13&&$9==13?$7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($10==68.7&&$6==1&&$8==12&&$9==13?$7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($10==68.7&&$6==2&&$8==12&&$9==13?$7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($10==68.7&&$6==3&&$8==12&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($10==68.7&&$6==4&&$8==12&&$9==13?$7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($10==68.7&&$6==5&&$8==12&&$9==13?$7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($10==68.7&&$6==6&&$8==12&&$9==13?$7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($10==68.7&&$6==7&&$8==12&&$9==13?$7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($10==68.7&&$6==8&&$8==12&&$9==13?$7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($10==68.7&&$6==9&&$8==12&&$9==13?$7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($10==68.7&&$6==10&&$8==12&&$9==13?$7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($10==68.7&&$6==11&&$8==12&&$9==13?$7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($10==68.7&&$6==12&&$8==12&&$9==13?$7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($10==68.7&&$6==13&&$8==12&&$9==13?$7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($10==68.7&&$6==14&&$8==12&&$9==13?$7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($10==68.7&&$6==15&&$8==12&&$9==13?$7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_BIAS-28.eps"
set title "TThreshold Scan (EDAC 12-13, BIAS +2.8V)"
plot "scan.data" using ($10==69.5&&$6==0&&$8==13&&$9==13?$7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($10==69.5&&$6==1&&$8==12&&$9==13?$7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($10==69.5&&$6==2&&$8==12&&$9==13?$7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($10==69.5&&$6==3&&$8==12&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($10==69.5&&$6==4&&$8==12&&$9==13?$7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($10==69.5&&$6==5&&$8==12&&$9==13?$7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($10==69.5&&$6==6&&$8==12&&$9==13?$7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($10==69.5&&$6==7&&$8==12&&$9==13?$7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($10==69.5&&$6==8&&$8==12&&$9==13?$7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($10==69.5&&$6==9&&$8==12&&$9==13?$7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($10==69.5&&$6==10&&$8==12&&$9==13?$7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($10==69.5&&$6==11&&$8==12&&$9==13?$7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($10==69.5&&$6==12&&$8==12&&$9==13?$7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($10==69.5&&$6==13&&$8==12&&$9==13?$7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($10==69.5&&$6==14&&$8==12&&$9==13?$7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($10==69.5&&$6==15&&$8==12&&$9==13?$7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_BIAS-20.eps"
set title "TThreshold Scan (EDAC 12-13, BIAS +2.0V)"
plot "scan.data" using ($10==60.14&&$6==0&&$8==13&&$9==13?$7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($10==60.14&&$6==1&&$8==12&&$9==13?$7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($10==60.14&&$6==2&&$8==12&&$9==13?$7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($10==60.14&&$6==3&&$8==12&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($10==60.14&&$6==4&&$8==12&&$9==13?$7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($10==60.14&&$6==5&&$8==12&&$9==13?$7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($10==60.14&&$6==6&&$8==12&&$9==13?$7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($10==60.14&&$6==7&&$8==12&&$9==13?$7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($10==60.14&&$6==8&&$8==12&&$9==13?$7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($10==60.14&&$6==9&&$8==12&&$9==13?$7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($10==60.14&&$6==10&&$8==12&&$9==13?$7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($10==60.14&&$6==11&&$8==12&&$9==13?$7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($10==60.14&&$6==12&&$8==12&&$9==13?$7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($10==60.14&&$6==13&&$8==12&&$9==13?$7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($10==60.14&&$6==14&&$8==12&&$9==13?$7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($10==60.14&&$6==15&&$8==12&&$9==13?$7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_BIAS-25.eps"
set title "TThreshold Scan (EDAC 12-13, BIAS +2.5V)"
plot "scan.data" using ($10==60.64&&$6==0&&$8==13&&$9==13?$7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($10==60.64&&$6==1&&$8==12&&$9==13?$7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($10==60.64&&$6==2&&$8==12&&$9==13?$7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($10==60.64&&$6==3&&$8==12&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($10==60.64&&$6==4&&$8==12&&$9==13?$7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($10==60.64&&$6==5&&$8==12&&$9==13?$7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($10==60.64&&$6==6&&$8==12&&$9==13?$7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($10==60.64&&$6==7&&$8==12&&$9==13?$7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($10==60.64&&$6==8&&$8==12&&$9==13?$7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($10==60.64&&$6==9&&$8==12&&$9==13?$7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($10==60.64&&$6==10&&$8==12&&$9==13?$7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($10==60.64&&$6==11&&$8==12&&$9==13?$7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($10==60.64&&$6==12&&$8==12&&$9==13?$7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($10==60.64&&$6==13&&$8==12&&$9==13?$7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($10==60.64&&$6==14&&$8==12&&$9==13?$7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($10==60.64&&$6==15&&$8==12&&$9==13?$7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_BIAS-30.eps"
set title "TThreshold Scan (EDAC 12-13, BIAS +3.0V)"
plot "scan.data" using ($10==61.14&&$6==0&&$8==13&&$9==13?$7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($10==61.14&&$6==1&&$8==12&&$9==13?$7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($10==61.14&&$6==2&&$8==12&&$9==13?$7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($10==61.14&&$6==3&&$8==12&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($10==61.14&&$6==4&&$8==12&&$9==13?$7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($10==61.14&&$6==5&&$8==12&&$9==13?$7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($10==61.14&&$6==6&&$8==12&&$9==13?$7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($10==61.14&&$6==7&&$8==12&&$9==13?$7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($10==61.14&&$6==8&&$8==12&&$9==13?$7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($10==61.14&&$6==9&&$8==12&&$9==13?$7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($10==61.14&&$6==10&&$8==12&&$9==13?$7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($10==61.14&&$6==11&&$8==12&&$9==13?$7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($10==61.14&&$6==12&&$8==12&&$9==13?$7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($10==61.14&&$6==13&&$8==12&&$9==13?$7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($10==61.14&&$6==14&&$8==12&&$9==13?$7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($10==61.14&&$6==15&&$8==12&&$9==13?$7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_BIAS-35.eps"
set title "TThreshold Scan (EDAC 12-13, BIAS +3.5V)"
plot "scan.data" using ($10==61.64&&$6==0&&$8==13&&$9==13?$7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($10==61.64&&$6==1&&$8==12&&$9==13?$7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($10==61.64&&$6==2&&$8==12&&$9==13?$7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($10==61.64&&$6==3&&$8==12&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($10==61.64&&$6==4&&$8==12&&$9==13?$7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($10==61.64&&$6==5&&$8==12&&$9==13?$7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($10==61.64&&$6==6&&$8==12&&$9==13?$7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($10==61.64&&$6==7&&$8==12&&$9==13?$7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($10==61.64&&$6==8&&$8==12&&$9==13?$7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($10==61.64&&$6==9&&$8==12&&$9==13?$7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($10==61.64&&$6==10&&$8==12&&$9==13?$7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($10==61.64&&$6==11&&$8==12&&$9==13?$7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($10==61.64&&$6==12&&$8==12&&$9==13?$7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($10==61.64&&$6==13&&$8==12&&$9==13?$7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($10==61.64&&$6==14&&$8==12&&$9==13?$7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($10==61.64&&$6==15&&$8==12&&$9==13?$7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tthres_BIAS-40.eps"
set title "TThreshold Scan  (EDAC 12-13, BIAS +4.0V)"
plot "scan.data" using ($10==62.14&&$6==0&&$8==13&&$9==13?$7-0.15:1/0):(($18+$19)/$17*100) with points title "TB  0" ,\
	"scan.data" using ($10==62.14&&$6==1&&$8==12&&$9==13?$7-0.13:1/0):(($18+$19)/$17*100) with points title "TB  1",\
	"scan.data" using ($10==62.14&&$6==2&&$8==12&&$9==13?$7-0.11:1/0):(($18+$19)/$17*100) with points title "TB  2",\
	"scan.data" using ($10==62.14&&$6==3&&$8==12&&$9==13?$7-0.09:1/0):(($18+$19)/$17*100) with points title "TB  3",\
	"scan.data" using ($10==62.14&&$6==4&&$8==12&&$9==13?$7-0.07:1/0):(($18+$19)/$17*100) with points title "TB  4",\
	"scan.data" using ($10==62.14&&$6==5&&$8==12&&$9==13?$7-0.05:1/0):(($18+$19)/$17*100) with points title "TB  5",\
	"scan.data" using ($10==62.14&&$6==6&&$8==12&&$9==13?$7-0.03:1/0):(($18+$19)/$17*100) with points title "TB  6",\
	"scan.data" using ($10==62.14&&$6==7&&$8==12&&$9==13?$7-0.01:1/0):(($18+$19)/$17*100) with points title "TB  7",\
	"scan.data" using ($10==62.14&&$6==8&&$8==12&&$9==13?$7+0.01:1/0):(($18+$19)/$17*100) with points title "TB  8",\
	"scan.data" using ($10==62.14&&$6==9&&$8==12&&$9==13?$7+0.03:1/0):(($18+$19)/$17*100) with points title "TB  9",\
	"scan.data" using ($10==62.14&&$6==10&&$8==12&&$9==13?$7+0.05:1/0):(($18+$19)/$17*100) with points title "TB 10",\
	"scan.data" using ($10==62.14&&$6==11&&$8==12&&$9==13?$7+0.07:1/0):(($18+$19)/$17*100) with points title "TB 11",\
	"scan.data" using ($10==62.14&&$6==12&&$8==12&&$9==13?$7+0.09:1/0):(($18+$19)/$17*100) with points title "TB 12",\
	"scan.data" using ($10==62.14&&$6==13&&$8==12&&$9==13?$7+0.11:1/0):(($18+$19)/$17*100) with points title "TB 13",\
	"scan.data" using ($10==62.14&&$6==14&&$8==12&&$9==13?$7+0.13:1/0):(($18+$19)/$17*100) with points title "TB 14",\
	"scan.data" using ($10==62.14&&$6==15&&$8==12&&$9==13?$7+0.15:1/0):(($18+$19)/$17*100) with points title "TB 15",\
	100 title "" lc rgb "white"




