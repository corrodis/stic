set terminal eps

# Data Format of the scan.data
#  1: RUN - NO
#  2: DAC - BIAS
#  3: DAC - LADDER
#  4: DAC - ICOMP
#  5: DAC - ?
#  6: DAC - TBIAS
#  7: DAC - TTHRESHOLD
#  8: DAC - EBIAS
#  9: DAC - ETHRESHOLD
#  10: BIAS VOLTAGE (nominal 58.14)
# 11: CTR Sigma
# 12: CTR Sigma fiterror
# 13: E1 Position
# 14: E1 Position error
# 15: E2 Position
# 16: E2 Position error
# 17: number of coincidence events after energy cuts 
# 18: number of underflows
# 19: number of overflows

set linetype 1 lc rgb "#0EA857"		lw 1 pt 0
set linetype 2 lc rgb "#0EAD7E"		lw 1 pt 0
set linetype 3 lc rgb "#0EB2A6" 	lw 1 pt 0
set linetype 4 lc rgb "#0D9CB7" 	lw 1 pt 0
set linetype 5 lc rgb "#0D78BC" 	lw 1 pt 0
set linetype 6 lc rgb "#0C52C0" 	lw 1 pt 0
set linetype 7 lc rgb "#0B29C5" 	lw 1 pt 0
set linetype 8 lc rgb "#170BCA" 	lw 1 pt 0
set linetype 9 lc rgb "#440ACF" 	lw 1 pt 0
set linetype 10 lc rgb "#7309D4"	lw 1 pt 0
set linetype 11 lc rgb "#A408D9"	lw 1 pt 0
set linetype 12 lc rgb "#D807D0"	lw 1 pt 0
set linetype 13 lc rgb "#E207B5"	lw 1 pt 0
set linetype 14 lc rgb "#D21A26"	lw 1 pt 0
set linetype 15 lc rgb "#overflow0F22"	lw 1 pt 0
set linetype 16 lc rgb "#F1041F"	lw 1 pt 0


############# overflowvs TBIAS (BIAS) with fixed TT ############################
set xlabel "TBIAS"
set ylabel "overflow [%]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
set key outside Right

set output "plots/scan_overflow_tbias_TTHRES-0.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 0)"
plot 	"scan.data" using ($7==0&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==0&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==0&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==0&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==0&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==0&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==0&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==0&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==0&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-1.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 1)"
plot 	"scan.data" using ($7==1&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==1&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==1&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==1&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==1&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==1&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==1&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==1&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==1&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-2.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 2)"
plot 	"scan.data" using ($7==2&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==2&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==2&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==2&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==2&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==2&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==2&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==2&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==2&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-3.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 3)"
plot 	"scan.data" using ($7==3&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==3&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==3&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==3&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==3&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==3&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==3&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==3&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==3&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-4.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 4)"
plot 	"scan.data" using ($7==4&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==4&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==4&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==4&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==4&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==4&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==4&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==4&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==4&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-5.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 5)"
plot 	"scan.data" using ($7==5&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==5&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==5&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==5&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==5&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==5&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==5&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==5&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==5&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-6.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 6)"
plot 	"scan.data" using ($7==6&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==6&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==6&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==6&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==6&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==6&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==6&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==6&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==6&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-7.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 7)"
plot 	"scan.data" using ($7==7&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==7&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==7&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==7&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==7&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==7&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==7&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==7&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==7&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-8.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 8)"
plot 	"scan.data" using ($7==8&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==8&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==8&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==8&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==8&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==8&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==8&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==8&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==8&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-9.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 9)"
plot 	"scan.data" using ($7==9&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==9&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==9&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==9&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==9&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==9&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==9&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==9&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==9&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-20.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 10)"
plot 	"scan.data" using ($7==10&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==10&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==10&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==10&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==10&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==10&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==10&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==10&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==10&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-11.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 11)"
plot 	"scan.data" using ($7==11&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==11&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==11&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==11&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==11&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==11&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==11&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==11&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==11&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-12.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 12)"
plot 	"scan.data" using ($7==12&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==12&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==12&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==12&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==12&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==12&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==12&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==12&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==12&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-13.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 13)"
plot 	"scan.data" using ($7==13&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==13&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==13&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==13&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==13&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==13&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==13&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==13&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==13&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-14.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 14)"
plot 	"scan.data" using ($7==14&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==14&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==14&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==14&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==14&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==14&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==14&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==14&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==14&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_TTHRES-28.eps"
set title "TBias Scan, (EDAC 12-13, TTHRES 15)"
plot 	"scan.data" using ($7==15&&$10==67.1&&$8==13&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "+0.4 V" ,\
	"scan.data" using ($7==15&&$10==67.9&&$8==13&&$9==13?$6-0.06:1/0):(($18+$19)/$17*100) with points title "+1.2 V" ,\
	"scan.data" using ($7==15&&$10==68.7&&$8==13&&$9==13?$6-0.04:1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==15&&$10==69.5&&$8==13&&$9==13?$6-0.02:1/0):(($18+$19)/$17*100) with points title "+2.8 V" ,\
	"scan.data" using ($7==15&&$10==60.14&&$8==13&&$9==13?$6     :1/0):(($18+$19)/$17*100) with points title "+2.0 V" ,\
	"scan.data" using ($7==15&&$10==60.64&&$8==13&&$9==13?$6+0.02:1/0):(($18+$19)/$17*100) with points title "+2.5 V" ,\
	"scan.data" using ($7==15&&$10==61.14&&$8==13&&$9==13?$6+0.04:1/0):(($18+$19)/$17*100) with points title "+3.0 V" ,\
	"scan.data" using ($7==15&&$10==61.64&&$8==13&&$9==13?$6+0.06:1/0):(($18+$19)/$17*100) with points title "+3.5 V" ,\
	"scan.data" using ($7==15&&$10==62.14&&$8==13&&$9==13?$6+0.08:1/0):(($18+$19)/$17*100) with points title "+4.0 V",\
	100 title "" lc rgb "white"







############# overflowvs TBIAS (TTHRES) with fixed BIAS ############################
set xlabel "TBIAS"
set ylabel "overflow [%]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
set key outside Right

set output "plots/scan_overflow_tbias_BIAS-04.eps"
set title "TBias Scan, (EDAC 12-13, +0.4V)"
plot "scan.data" using ($10==67.1&&$7==0&&$8==13&&$9==13?$6-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($10==67.1&&$7==1&&$8==12&&$9==13?$6-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($10==67.1&&$7==2&&$8==12&&$9==13?$6-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($10==67.1&&$7==3&&$8==12&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($10==67.1&&$7==4&&$8==12&&$9==13?$6-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($10==67.1&&$7==5&&$8==12&&$9==13?$6-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($10==67.1&&$7==6&&$8==12&&$9==13?$6-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($10==67.1&&$7==7&&$8==12&&$9==13?$6-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($10==67.1&&$7==8&&$8==12&&$9==13?$6+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($10==67.1&&$7==9&&$8==12&&$9==13?$6+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($10==67.1&&$7==10&&$8==12&&$9==13?$6+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($10==67.1&&$7==11&&$8==12&&$9==13?$6+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($10==67.1&&$7==12&&$8==12&&$9==13?$6+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($10==67.1&&$7==13&&$8==12&&$9==13?$6+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($10==67.1&&$7==14&&$8==12&&$9==13?$6+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($10==67.1&&$7==15&&$8==12&&$9==13?$6+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
100 title "" lc rgb "white"


set output "plots/scan_overflow_tbias_BIAS-12.eps"
set title "TBias Scan, (EDAC 12-13, +1.2V)"
plot "scan.data" using ($10==67.9&&$7==0&&$8==13&&$9==13?$6-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($10==67.9&&$7==1&&$8==12&&$9==13?$6-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($10==67.9&&$7==2&&$8==12&&$9==13?$6-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($10==67.9&&$7==3&&$8==12&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($10==67.9&&$7==4&&$8==12&&$9==13?$6-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($10==67.9&&$7==5&&$8==12&&$9==13?$6-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($10==67.9&&$7==6&&$8==12&&$9==13?$6-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($10==67.9&&$7==7&&$8==12&&$9==13?$6-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($10==67.9&&$7==8&&$8==12&&$9==13?$6+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($10==67.9&&$7==9&&$8==12&&$9==13?$6+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($10==67.9&&$7==10&&$8==12&&$9==13?$6+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($10==67.9&&$7==11&&$8==12&&$9==13?$6+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($10==67.9&&$7==12&&$8==12&&$9==13?$6+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($10==67.9&&$7==13&&$8==12&&$9==13?$6+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($10==67.9&&$7==14&&$8==12&&$9==13?$6+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($10==67.9&&$7==15&&$8==12&&$9==13?$6+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_BIAS-20.eps"
set title "TBias Scan, (EDAC 12-13, +2.0V)"
plot "scan.data" using ($10==68.7&&$7==0&&$8==13&&$9==13?$6-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($10==68.7&&$7==1&&$8==12&&$9==13?$6-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($10==68.7&&$7==2&&$8==12&&$9==13?$6-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($10==68.7&&$7==3&&$8==12&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($10==68.7&&$7==4&&$8==12&&$9==13?$6-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($10==68.7&&$7==5&&$8==12&&$9==13?$6-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($10==68.7&&$7==6&&$8==12&&$9==13?$6-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($10==68.7&&$7==7&&$8==12&&$9==13?$6-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($10==68.7&&$7==8&&$8==12&&$9==13?$6+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($10==68.7&&$7==9&&$8==12&&$9==13?$6+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($10==68.7&&$7==10&&$8==12&&$9==13?$6+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($10==68.7&&$7==11&&$8==12&&$9==13?$6+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($10==68.7&&$7==12&&$8==12&&$9==13?$6+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($10==68.7&&$7==13&&$8==12&&$9==13?$6+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($10==68.7&&$7==14&&$8==12&&$9==13?$6+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($10==68.7&&$7==15&&$8==12&&$9==13?$6+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_BIAS-28.eps"
set title "TBias Scan, (EDAC 12-13, +2.8V)"
plot "scan.data" using ($10==69.5&&$7==0&&$8==13&&$9==13?$6-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($10==69.5&&$7==1&&$8==12&&$9==13?$6-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($10==69.5&&$7==2&&$8==12&&$9==13?$6-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($10==69.5&&$7==3&&$8==12&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($10==69.5&&$7==4&&$8==12&&$9==13?$6-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($10==69.5&&$7==5&&$8==12&&$9==13?$6-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($10==69.5&&$7==6&&$8==12&&$9==13?$6-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($10==69.5&&$7==7&&$8==12&&$9==13?$6-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($10==69.5&&$7==8&&$8==12&&$9==13?$6+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($10==69.5&&$7==9&&$8==12&&$9==13?$6+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($10==69.5&&$7==10&&$8==12&&$9==13?$6+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($10==69.5&&$7==11&&$8==12&&$9==13?$6+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($10==69.5&&$7==12&&$8==12&&$9==13?$6+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($10==69.5&&$7==13&&$8==12&&$9==13?$6+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($10==69.5&&$7==14&&$8==12&&$9==13?$6+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($10==69.5&&$7==15&&$8==12&&$9==13?$6+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_BIAS-20.eps"
set title "TBias Scan, (EDAC 12-13, +2.0V)"
plot "scan.data" using ($10==60.14&&$7==0&&$8==13&&$9==13?$6-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($10==60.14&&$7==1&&$8==12&&$9==13?$6-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($10==60.14&&$7==2&&$8==12&&$9==13?$6-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($10==60.14&&$7==3&&$8==12&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($10==60.14&&$7==4&&$8==12&&$9==13?$6-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($10==60.14&&$7==5&&$8==12&&$9==13?$6-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($10==60.14&&$7==6&&$8==12&&$9==13?$6-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($10==60.14&&$7==7&&$8==12&&$9==13?$6-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($10==60.14&&$7==8&&$8==12&&$9==13?$6+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($10==60.14&&$7==9&&$8==12&&$9==13?$6+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($10==60.14&&$7==10&&$8==12&&$9==13?$6+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($10==60.14&&$7==11&&$8==12&&$9==13?$6+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($10==60.14&&$7==12&&$8==12&&$9==13?$6+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($10==60.14&&$7==13&&$8==12&&$9==13?$6+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($10==60.14&&$7==14&&$8==12&&$9==13?$6+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($10==60.14&&$7==15&&$8==12&&$9==13?$6+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_BIAS-25.eps"
set title "TBias Scan, (EDAC 12-13, +2.5V)"
plot "scan.data" using ($10==60.64&&$7==0&&$8==13&&$9==13?$6-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($10==60.64&&$7==1&&$8==12&&$9==13?$6-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($10==60.64&&$7==2&&$8==12&&$9==13?$6-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($10==60.64&&$7==3&&$8==12&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($10==60.64&&$7==4&&$8==12&&$9==13?$6-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($10==60.64&&$7==5&&$8==12&&$9==13?$6-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($10==60.64&&$7==6&&$8==12&&$9==13?$6-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($10==60.64&&$7==7&&$8==12&&$9==13?$6-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($10==60.64&&$7==8&&$8==12&&$9==13?$6+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($10==60.64&&$7==9&&$8==12&&$9==13?$6+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($10==60.64&&$7==10&&$8==12&&$9==13?$6+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($10==60.64&&$7==11&&$8==12&&$9==13?$6+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($10==60.64&&$7==12&&$8==12&&$9==13?$6+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($10==60.64&&$7==13&&$8==12&&$9==13?$6+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($10==60.64&&$7==14&&$8==12&&$9==13?$6+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($10==60.64&&$7==15&&$8==12&&$9==13?$6+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_BIAS-30.eps"
set title "TBias Scan, (EDAC 12-13, +3.0V)"
plot "scan.data" using ($10==61.14&&$7==0&&$8==13&&$9==13?$6-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($10==61.14&&$7==1&&$8==12&&$9==13?$6-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($10==61.14&&$7==2&&$8==12&&$9==13?$6-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($10==61.14&&$7==3&&$8==12&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($10==61.14&&$7==4&&$8==12&&$9==13?$6-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($10==61.14&&$7==5&&$8==12&&$9==13?$6-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($10==61.14&&$7==6&&$8==12&&$9==13?$6-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($10==61.14&&$7==7&&$8==12&&$9==13?$6-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($10==61.14&&$7==8&&$8==12&&$9==13?$6+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($10==61.14&&$7==9&&$8==12&&$9==13?$6+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($10==61.14&&$7==10&&$8==12&&$9==13?$6+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($10==61.14&&$7==11&&$8==12&&$9==13?$6+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($10==61.14&&$7==12&&$8==12&&$9==13?$6+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($10==61.14&&$7==13&&$8==12&&$9==13?$6+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($10==61.14&&$7==14&&$8==12&&$9==13?$6+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($10==61.14&&$7==15&&$8==12&&$9==13?$6+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_BIAS-35.eps"
set title "TBias Scan, (EDAC 12-13, +3.5V)"
plot "scan.data" using ($10==61.64&&$7==0&&$8==13&&$9==13?$6-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($10==61.64&&$7==1&&$8==12&&$9==13?$6-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($10==61.64&&$7==2&&$8==12&&$9==13?$6-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($10==61.64&&$7==3&&$8==12&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($10==61.64&&$7==4&&$8==12&&$9==13?$6-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($10==61.64&&$7==5&&$8==12&&$9==13?$6-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($10==61.64&&$7==6&&$8==12&&$9==13?$6-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($10==61.64&&$7==7&&$8==12&&$9==13?$6-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($10==61.64&&$7==8&&$8==12&&$9==13?$6+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($10==61.64&&$7==9&&$8==12&&$9==13?$6+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($10==61.64&&$7==10&&$8==12&&$9==13?$6+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($10==61.64&&$7==11&&$8==12&&$9==13?$6+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($10==61.64&&$7==12&&$8==12&&$9==13?$6+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($10==61.64&&$7==13&&$8==12&&$9==13?$6+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($10==61.64&&$7==14&&$8==12&&$9==13?$6+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($10==61.64&&$7==15&&$8==12&&$9==13?$6+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
	100 title "" lc rgb "white"

set output "plots/scan_overflow_tbias_BIAS-40.eps"
set title "TBias Scan, (EDAC 12-13, +4.0V)"
plot "scan.data" using ($6==8&&$7==0&&$8==13&&$9==13?$6-0.15:1/0):(($18+$19)/$17*100) with points title "TT  0" ,\
	"scan.data" using ($6==8&&$7==1&&$8==12&&$9==13?$6-0.13:1/0):(($18+$19)/$17*100) with points title "TT  1",\
	"scan.data" using ($6==8&&$7==2&&$8==12&&$9==13?$6-0.11:1/0):(($18+$19)/$17*100) with points title "TT  2",\
	"scan.data" using ($6==8&&$7==3&&$8==12&&$9==13?$6-0.09:1/0):(($18+$19)/$17*100) with points title "TT  3",\
	"scan.data" using ($6==8&&$7==4&&$8==12&&$9==13?$6-0.07:1/0):(($18+$19)/$17*100) with points title "TT  4",\
	"scan.data" using ($6==8&&$7==5&&$8==12&&$9==13?$6-0.05:1/0):(($18+$19)/$17*100) with points title "TT  5",\
	"scan.data" using ($6==8&&$7==6&&$8==12&&$9==13?$6-0.03:1/0):(($18+$19)/$17*100) with points title "TT  6",\
	"scan.data" using ($6==8&&$7==7&&$8==12&&$9==13?$6-0.01:1/0):(($18+$19)/$17*100) with points title "TT  7",\
	"scan.data" using ($6==8&&$7==8&&$8==12&&$9==13?$6+0.01:1/0):(($18+$19)/$17*100) with points title "TT  8",\
	"scan.data" using ($6==8&&$7==9&&$8==12&&$9==13?$6+0.03:1/0):(($18+$19)/$17*100) with points title "TT  9",\
	"scan.data" using ($6==8&&$7==10&&$8==12&&$9==13?$6+0.05:1/0):(($18+$19)/$17*100) with points title "TT 10",\
	"scan.data" using ($6==8&&$7==11&&$8==12&&$9==13?$6+0.07:1/0):(($18+$19)/$17*100) with points title "TT 11",\
	"scan.data" using ($6==8&&$7==12&&$8==12&&$9==13?$6+0.09:1/0):(($18+$19)/$17*100) with points title "TT 12",\
	"scan.data" using ($6==8&&$7==13&&$8==12&&$9==13?$6+0.11:1/0):(($18+$19)/$17*100) with points title "TT 13",\
	"scan.data" using ($6==8&&$7==14&&$8==12&&$9==13?$6+0.13:1/0):(($18+$19)/$17*100) with points title "TT 14",\
	"scan.data" using ($6==8&&$7==15&&$8==12&&$9==13?$6+0.15:1/0):(($18+$19)/$17*100) with points title "TT 15",\
100 title "" lc rgb "white"
