set terminal eps

# USAGE: gnuplot -e "PREFIX='';TB='?';TT='?';EB='?';ET='?';BIAS='?'" scan_stability.plt
# Data Format of the scan.data
#  1: RUN - NO
#  2: DAC - BIAS
#  3: DAC - LADDER
#  4: DAC - ICOMP
#  5: DAC - ?
#  6: DAC - TBIAS
#  7: DAC - TTHRESHOLD
#  8: DAC - EBIAS
#  9: DAC - ETHRESHOLD
# 10: BIAS VOLTAGE
# 11: overflowPosition Sigma
# 12: overflowPosition Sigma fiterror
# 13: overflowPosition
# 14: overflowPosition error
# 15: E2 Position
# 16: E2 Position error
# 17: number of coincidence events after energy cuts 
# 18: number of underflows
# 19: number of overflows

set linetype 1 lc rgb "#0EA857"		lw 1 pt 0
set linetype 2 lc rgb "#0EAD7E"		lw 1 pt 0
set linetype 3 lc rgb "#0EB2A6" 	lw 1 pt 0
set linetype 4 lc rgb "#0D9CB7" 	lw 1 pt 0
set linetype 5 lc rgb "#0D78BC" 	lw 1 pt 0
set linetype 6 lc rgb "#0C52C0" 	lw 1 pt 0
set linetype 7 lc rgb "#0B29C5" 	lw 1 pt 0
set linetype 8 lc rgb "#170BCA" 	lw 1 pt 0
set linetype 9 lc rgb "#440ACF" 	lw 1 pt 0
set linetype 10 lc rgb "#7309D4"	lw 1 pt 0
set linetype 11 lc rgb "#A408D9"	lw 1 pt 0
set linetype 12 lc rgb "#D807D0"	lw 1 pt 0
set linetype 13 lc rgb "#E207B5"	lw 1 pt 0
set linetype 14 lc rgb "#D21A26"	lw 1 pt 0
set linetype 15 lc rgb "#overflow0F22"	lw 1 pt 0
set linetype 16 lc rgb "#F1041F"	lw 1 pt 0



#set multiplot layout 3,1
set title "Stability Tests"

set xlabel "time [min]"
set ylabel "ctr FWHM [ps]"
#set yrange [180:350]
#set xrange [0:60]
set grid
#set xtics 0,5
set key horizontal
set output "plots/scan_".PREFIX."_stability_ctr.eps"
plot "scan.data" using ($1>9&&$6==TB&&$7==TT&&$8==EB&&$9==ET&&$10==BIAS?($1-10)*5-2.5:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "scan"



set ylabel "E1 position ToT [1.6ns]"
set output "plots/scan_".PREFIX."_stability_E1.eps"
plot "scan.data" using ($1>9&&$6==TB&&$7==TT&&$8==EB&&$9==ET&&$10==BIAS?($1-10)*5-2.5:1/0):13:14 with yerrorbars title "scan"


set ylabel "E2 position ToT [1.6ns]"
set output "plots/scan_".PREFIX."_stability_E2.eps"
plot "scan.data" using ($1>9&&$6==TB&&$7==TT&&$8==EB&&$9==ET&&$10==BIAS?($1-10)*5-2.5:1/0):15:16 with yerrorbars title "scan"

