#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1I.h"
#include "TString.h"
#include "TBranch.h"
#include "TClassTable.h"
#include "EventType.h"
#include "EventDict.h"
#include "events.h"

#include <map>
#include <iostream>
#include <vector>
#include <algorithm> 
#include <string> 

#define CHANNELS 64

bool eventChannelSort (stic_data_e eventA, stic_data_e eventB) { return (eventA.channel < eventB.channel); }

int main(int argc, char *argv[]) {
    if(argc <= 1) {
	std::cout << "Usage: coincBuilder filename [coincwindow=10ns]" << std::endl;
	return 0;
	}
    Int_t optPrint = 0;
	
    Int_t coinWindow = 10000/50; // 50ps units, default 10ns

    if(argc > 2)
	coinWindow = atof(argv[2])*1000/50;


    //Int_t ch1 = atoi(argv[2]);
    //Int_t ch2 = atoi(argv[3]);

    // Used for periode
    //std::map<Int_t, Int_t> lastTime;

    //std::cout << "Start StepByStep" << std::endl;
	stic_data_e * event = NULL;

	int lastindex = string(argv[1]).find_last_of("."); 
	string fname = string(argv[1]).substr(0, lastindex);

    //std::cout << "test" << std::endl;

    //if(argc >= 4) {
//	ch1 = atoi(argv[2]);
//	ch2 = atoi(argv[3]);
//    } 
//    else {
//	std::cout << "Usage: stepByStep filename ch1 ch2 [coincwindow=10ns]" << std::endl;
 //   }
    //string suffix = string("");

    //if(argc >= 4)
    //    suffix = string(argv[4]);

    //if(argc>=3) {
    //    coinWindow = int(atof(argv[2])*19.92);
    //}

    // Add possibility to apply E-cuts for single construction
    Float_t Ecut[2][2] = {-1, -1, -1, -1};
    /*if(argc>=8) {
        Ecut[0][0] = atof(argv[4]);
        Ecut[0][1] = atof(argv[5]);
        Ecut[1][0] = atof(argv[6]);
        Ecut[1][1] = atof(argv[7]);
    }*/


    TFile * file = new TFile((TString)fname+(TString)".root","READ");
	TTree * tree = (TTree*)file->Get("dump");
	TBranch * branch = tree->GetBranch("br");
	branch->SetAddress(&event);

    TFile * fileCoin = new TFile((TString)fname+(TString)"_coin"+(TString)".root","RECREATE");
    Int_t energy1, energy2, deltaTcc, deltaT, deltaTfine;
    Int_t pair;
	TTree * treeCoin = new TTree("Coin","Coin");
	treeCoin->Branch("energy1",&energy1,"energy1/I");
	treeCoin->Branch("energy2",&energy2,"energy2/I");
	treeCoin->Branch("deltaTcc",&deltaTcc,"deltaTcc/I");
	treeCoin->Branch("deltaTfine",&deltaTfine,"deltaTfine/I");
	treeCoin->Branch("pair",&pair,"pair/I");
    	treeCoin->Branch("deltaT",&deltaT,"deltaT/I");

	TH1I * statFrames = new TH1I("statFrames","Events per Frame",21,-0.5,20.5);
	//TH1I * deltaT = new TH1I("deltaT","CTR", 2097153,-1048576.5,1048576.5);
    //std::map<Int_t,TH1I *> h_period;
    //for(Int_t i = 0; i < 64; ++i) {
    //    h_period[i] = new TH1I(Form("h_periode_ch%i",i),"Periode",(1 << 20),-0.5,(1 << 20) + 0.5);
    //}

	Int_t frame = 0;
    std::vector<stic_data_e> events[CHANNELS]; // alternating (0, 1), (2, 3) (4, 5) current, prev, prevprev events
    //Int_t curIndex = 0;
    //Int_t prevIndex = 0;
    //Int_t prevprevIndex = 0;


	Int_t cntCh = 0;

	if(optPrint>0)
		event->printHeader();
    //std::cout << std::endl;
	for(Int_t i=0;i<branch->GetEntries();i++) {
    if(i%100000==0)
    	std::cout << (Double_t)i/branch->GetEntries()*100 << "%" << std::endl;
    branch->GetEntry(i);
    // skip first 3 entries (easiest)
	//for(Int_t i=0;i<20;i++) {
		if(i < optPrint) {
			event->print("\t");
			std::cout << std::endl;
		}



        // std::cout << "Event: " << i << "\t Frame: " << frame << " " << event->frame_number << std::endl;
        if((frame >> 0) < (event->frame_number >> 0)) {
			// new Frame
            //if(events[prevIndex+0].size() >= 1 && events[prevIndex+1].size() >= 1)
            //    std::cout << "Frame no " << frame << ": " << events[prevIndex+0].size() << "\t" << events[prevIndex+1].size() << std::endl;
            //statFrames->Fill(events[prevIndex+0].size());

            if(i > 0) {
		for (int ch1 = 0; ch1<(CHANNELS/2);++ch1) {
                	for (std::vector<stic_data_e>::iterator it1 = events[ch1].begin(); it1 != events[ch1].end(); ++it1) {
                    // check if events are separated at least by coinWindow


                    //if( (it1 == events[prevIndex].begin() || (abs((it1-1)->getTime() - it1->getTime()) >= coinWindow)) &&
                    //    (it1 == events[prevIndex].end()   || (abs((it1+1)->getTime() - it1->getTime()) >= coinWindow))   ) {
                    /*if(   events[prevIndex+0].size() > 0 &&
                          (
                                (it1 == events[prevIndex+0].begin()) &&
                                    (
                                        (events[prevprevIndex+0].size() == 0 || abs(it1->getTime() - (events[prevprevIndex+0].end()-1)->getTime() >= coinWindow)) &&
                                        (
                                             (events[prevIndex+0].size() > 1 && abs((it1+1)->getTime() - it1->getTime()) >= coinWindow) ||
                                             (events[prevIndex+0].size() == 1 && (events[curIndex+0].size() == 0 || (abs(it1->getTime() - events[curIndex+0].begin()->getTime()) >= coinWindow)))
                                        )
                                    )
                             ) || (((it1 != events[prevIndex+0].begin() && it1 != (events[prevIndex+0].end()-1))) &&
                                     (
                                       (abs((it1-1)->getTime() - it1->getTime()) >= coinWindow) &&
                                       (abs((it1+1)->getTime() - it1->getTime()) >= coinWindow)
                                     )
                             ) || (it1 == (events[prevIndex+0].end()-1) &&
                                    (
                                       (events[prevIndex+0].size() > 1 && abs((it1-1)->getTime() - it1->getTime()) >= coinWindow) &&
                                       (events[curIndex+0].size() == 0 || abs(it1->getTime() - events[curIndex+0].begin()->getTime()) >= coinWindow)
                                    )
                             )
                        ) {*/
		   if(true) {
                        // first event or separated by coinWindow to last
                        // check also distance to last hit in previous frame


                        deltaTfine = -2097152;
			for(int ch2 = CHANNELS/2+1; ch2 < CHANNELS; ++ch2) {
                        	for (std::vector<stic_data_e>::iterator it2 = events[ch2].begin(); it2 != events[ch2].end(); ++it2) {
                           		if(abs(it1->getTime()-it2->getTime()) <= coinWindow) {
                                //if(deltaTfine != -2097152) {
                                    // alredy an event in the coin window
                                //    deltaTfine = 2097152;
                                //} else {
                                    	deltaTfine  = it1->getTfine()-it2->getTfine();
                                    	deltaTcc    = it1->getTcc()-it2->getTcc();
                                    	deltaT      = it1->getTime()-it2->getTime();
                                    	energy1     = it1->getEnergy();
                                    	energy2     = it2->getEnergy();
				    	pair	= (ch1 << 6) + ch2;
				    	treeCoin->Fill();
				    	++cntCh;
				}
                                //}
                            }
                        }
                        // Check in addtion last event from prev and first of nex frame
                        /*if(events[prevprevIndex+1].size() > 0 && abs(it1->getTime()-(events[prevprevIndex+1].end()-1)->getTime()) <= coinWindow) {
                            if(deltaTfine != -2097152) {
                                // alredy an event in the coin window
                                deltaTfine = 2097152;
                            } else {
                                deltaTfine  = it1->getTfine()   - (events[prevprevIndex+1].end()-1)->getTfine();
                                deltaTcc    = it1->getTcc()     - (events[prevprevIndex+1].end()-1)->getTcc();
                                deltaT      = it1->getTime()    - (events[prevprevIndex+1].end()-1)->getTime();
                                energy1     = it1->getEnergy();
                                energy2     = (events[prevprevIndex+1].end()-1)->getEnergy();
                            }
                        }
                        if(events[curIndex+1].size() > 0 && abs(it1->getTime()-events[curIndex+1].begin()->getTime()) <= coinWindow) {
                            if(deltaTfine != -2097152) {
                                // alredy an event in the coin window
                                deltaTfine = 2097152;
                            } else {
                                deltaTfine  = it1->getTfine()   - events[curIndex+1].begin()->getTfine();
                                deltaTcc    = it1->getTcc()     - events[curIndex+1].begin()->getTcc();
                                deltaT      = it1->getTime()    - events[curIndex+1].begin()->getTime();
                                energy1     = it1->getEnergy();
                                energy2     = events[curIndex+1].begin()->getEnergy();
                            }
                        }*/
                        // if no coin in window deltaTfine -2097152
                        // if more then one coin in window deltaTfine 2097152
                        //if(deltaTfine > -2097152 && deltaTfine < 2097152) {
                        //    treeCoin->Fill();
                        //    cntCh++;
                        //}
                    }
                }


                //if(!only2perFrame || events.size()==2) {
                //	std::sort (events.begin(), events.end(), eventChannelSort );
                //	for (std::vector<stic_data_e>::iterator it = events.begin(); it != events.end(); ++it) {
                //		if(it->channel == ch1 && time1 < 0) {
                //			time1 = it->time; //it->channel;
                //			time1cc = it->getTcc();
                //			time1fine = it->getTfine();
                //			energy1 = it->getEnergy();
                //		}
                //		if(time1 >= 0 && it->channel == ch2) {
                //			//std::cout << time1-it->time << std::endl;
                //			deltaT->Fill((Int_t)time1-it->time);

                //			deltaTcc = time1cc - it->getTcc();
                //			if(time1fine != -100000 && it->getTfine() != -100000)
                //				deltaTfine = time1fine - it->getTfine();
                //			else
                //				deltaTfine = -100000;
                //			energy2 = it->getEnergy();
                //			treeCoin->Fill();
                //		}
                //	}
                //}
		}
            }
            // clear prev events
	    for(int i = 0; i < CHANNELS; ++i) {
            	events[i].clear();
	    }
            //events[prevprevIndex+1].clear();
            //prevprevIndex = prevIndex;
            //prevIndex = curIndex;
            //curIndex = (curIndex + 2) % 6;
	    //curIndex = 0;


            //cntFrame = 0;
			//time1 = -1;			
		}
		frame = event->frame_number;
		//cntFrame++;
		//if(event->channel == ch1)
            //if((Ecut[0][0] < 0 || event->getEnergy() >= Ecut[0][0]) && (Ecut[0][1] < 0 || event->getEnergy() <= Ecut[0][1]))
                //events[curIndex+0].push_back(*event);
		//if(event->channel == ch2)
            //if((Ecut[1][0] < 0 || event->getEnergy() >= Ecut[1][0]) && (Ecut[1][1] < 0 || event->getEnergy() <= Ecut[1][1]))
                events[event->channel].push_back(*event);

        // Periode
        //h_period[event->channel]->Fill((event->getTime() - lastTime[event->channel]));
        //lastTime[event->channel] =  event->getTime();
		
		//std::cout << event-> << "\t" << it->channel << "\t"  << (it-1)->channel << "\t" << it->getTcc()-(it-1)->getTcc()  << "\t" << it->getTfine()  << "\t" << (it-1)->getTfine()  << "\t" << it->getEnergy() << "\t" << (it-1)->getEnergy() << std::endl;	
		//if(event->channel == ch1 || (event->channel == ch2 && 0) && event->TCCerror()) {
		//	cntCh++;
		//	if(frame < event->frame_number && 0) {
				// Start of a new Frame
		//		std::sort (events.begin(), events.end(), eventSorting);
		//		for (std::vector<stic_data_e>::iterator it = events.begin(); it != events.end(); ++it) {
		//			if(std::abs(it->getTcc()-lastTcc)<100) {
		//				std::cout << it->frame_number << "\t" << it->channel << "\t"  << (it-1)->channel << "\t" << it->getTcc()-(it-1)->getTcc()  << "\t" << it->getTfine()  << "\t" << (it-1)->getTfine()  << "\t" << it->getEnergy() << "\t" << (it-1)->getEnergy() << std::endl;
		//			}
		//			lastTcc = it->getTcc();
		//		}
		//		events.clear();
		//		frame = event->frame_number;
		//	}
		//	events.push_back(*event);
			//std::cout << event->channel << "\t" << event->getTcc() << std::endl;
		//	lastTcc = event->getTcc();
	
		//}
	}

    //std::cout << "Total: " << branch->GetEntries() << std::endl;
    //std::cout << "Coinc Events: " << cntCh << std::endl;
	
    //std::cout << ch1 << "\t" << ch2 << "\t" << branch->GetEntries() << "\t" << cntCh << std::endl;

    //treeCoin->Draw("energy1>>h_energy1(701,0,700)");
    //gDirectory->Get("h_energy1")->Write();
    //treeCoin->Draw("energy2>>h_energy2(701,0,700)");
    //gDirectory->Get("h_energy2")->Write();
    //treeCoin->Draw("energy1:energy2>>h_energy(701,0,700,701,0,700)");
    //gDirectory->Get("h_energy")->Write();
    //treeCoin->Draw("deltaTcc");
    //treeCoin->Draw("deltaT");
    //treeCoin->Draw("deltaTfine>>h_deltaT");
    //gDirectory->Get("h_deltaT")->Write();
    //for(Int_t i = 0; i < 15; ++i) {
    //    h_period[i]->Write();
    //}

	
	treeCoin->Write();
	fileCoin->Write();
	fileCoin->Close();
	file->Close();
}

