#include "TROOT.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TH1I.h"
#include "TString.h"
#include "TBranch.h"
#include "TClassTable.h"
#include "EventType.h"
#include "EventDict.h"
#include "events.h"
#include "TSpectrum.h"

#include <map>
#include <iostream>
#include <vector>
#include <algorithm> 
#include <string> 


int main(int argc, char *argv[]) {

    Int_t optPrint = 0;

    // Used for periode
    std::map<Int_t, Int_t> lastTime;

    std::cout << "Start correctFine" << std::endl;
	stic_data_e * event = NULL;

	int lastindex = string(argv[1]).find_last_of("."); 
	string fname = string(argv[1]).substr(0, lastindex); 
    string suffix = "";
    if(argc >= 3)
        suffix = string(argv[2]);


    TFile * file = new TFile((TString)fname+(TString)".root","READ");
	TTree * tree = (TTree*)file->Get("dump");
	TBranch * branch = tree->GetBranch("br");
	branch->SetAddress(&event);

    TFile * fileCorrected = new TFile((TString)fname+(TString)suffix+(TString)"_corr"+(TString)".root","RECREATE");
    //TTree * treeCorr = tree->Copy(0);

    std::map<Int_t,TH1I *> h_fine;
    std::map<Int_t,TH1F *> h_fine_new;

    std::map<Int_t, std::map<Int_t, Float_t> > fineBinW;
    std::map<Int_t, std::map<Int_t, Float_t> > fineOffset;

    Int_t N;

    for(Int_t i = 0; i < 64; ++i) {
        N = tree->Draw(Form("T_fine>>h_fine_ch%i(32,0,32)",i), Form("channel==%i",i));
        h_fine[i] = (TH1I*)gDirectory->Get(Form("h_fine_ch%i",i));
        cout << i << "\tN:\t" << N << endl;
        for(Int_t bin = 0; bin < 32; ++bin) {
            fineBinW[i][bin] = 32 * (Float_t)h_fine[i]->GetBinContent(bin+1)/N;
            if(bin == 0)
                fineOffset[i][bin] = fineBinW[i][bin]/2;
            else
                fineOffset[i][bin] = fineOffset[i][bin-1] + fineBinW[i][bin-1]/2 + fineBinW[i][bin]/2;
            cout << bin << " " << fineOffset[i][bin] << endl;
        }
        cout << "32" << " " << fineOffset[i][31]+fineBinW[i][31]/2 << endl;
        cout << endl;

        h_fine_new[i] = new TH1F(Form("h_fine_new_ch%i",i), Form("h_fine_new_ch%i",i),16,0,32);
    }
	if(optPrint>0)
		event->printHeader();
	std::cout << std::endl;


    Float_t time;
	for(Int_t i=0;i<branch->GetEntries();i++) {
        if(i%100000==0)
            std::cout << (Double_t)i/branch->GetEntries()*100 << "%" << std::endl;
        branch->GetEntry(i);
        if(i < optPrint) {
            event->print("\t");
            std::cout << std::endl;
        }
        time = event->getTcc() + fineOffset[event->channel][event->getTfine()];
        //time = event->getTcc() + event->getTfine() + 0.5;
        //cout << "correced time: " << time << endl;
        h_fine_new[event->channel]->Fill((time - event->getTcc()));
	}

	std::cout << "Total: " << branch->GetEntries() << std::endl;

    for(Int_t i = 0; i < 64; ++i) {
        h_fine[i]->Write();
        h_fine_new[i]->Write();
    }

    fileCorrected->Close();
	file->Close();
}
