#include "ethzSticAna.h"
#include "boost/program_options.hpp"

#include "TError.h"

#include <sys/stat.h>
#include <iostream>

using std::cout;
using std::endl;

namespace po = boost::program_options;

int main(int argc, char *argv[]) {
    gErrorIgnoreLevel=1;

    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("input-file,i", po::value<std::string>(), "input file")
        ("output-file,o", po::value<std::string>()->default_value("out.root"), "output file")
        ("energy-min,e", po::value<double>()->default_value(150), "minimum energy to find photo peak")
        ("stat-cut,s", po::value<double>()->default_value(500), "cut on statistics")
        ("discard-multi,d", "discard hits with multiplicity > 2")
        ("no-dnl,n", "no dnl correction (speed up, only for STiC) ")
    ;

    po::positional_options_description p;
    p.add("input-file", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
              options(desc).positional(p).run(), vm);
    po::notify(vm);

    if(argc<2) {
            cout << "useage: ethzAna FILE -output-file=out.tree [options]" << endl;
            cout << desc << "\n";
        return -1;
    }


    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }

    bool realtime = true;
    if(vm.count("no-dnl"))
        realtime = false;

    std::string fname = vm["input-file"].as<std::string>();

    // Check if there maybe is a already sorte file
    struct stat buffer;
    std::string fnameSort = fname.substr(0,fname.length()-5)+"_sort.root";
    if(stat (fnameSort.c_str(), &buffer) == 0) {
        cout << "Found already sorted file " << fnameSort << endl;
        fname = fnameSort;
    }

    cout << "Load ethzSticAna with file " << fname << endl;
    ethzSticAna * ana = new ethzSticAna(fname.c_str());

    cout << "Load all energy histograms (energyMin: " << vm["energy-min"].as<double>() << ")" << endl;
    unsigned int noChannels = ana->loadEnergy(vm["energy-min"].as<double>());
    cout << "-------------------------------------------------------" << endl;
    cout << noChannels << " channels found" << endl << endl;



    if(realtime) {
        cout << "Load DNL look-up table" << endl;
        ana->loadDNLTable();
    }
    //cout << "Load Alex DNL look-up table" << endl;
    //ana->loadDNLTableAlex();
    // cout << "Load NO DNL look-up table" << endl;
    // ana->loadNoDNLTable();
    cout << endl;


    cout << "added all " << ana->addAllPairs() << " pairs." << endl << endl;

    cout << "build coincidences...." << endl;
    int noCoinc = ana->buildCoinc(vm.count("discard-multi"),        // first argument: discardMulti
                                  realtime);                        // second argument: real
    //int noCoinc = ana->buildCoinc(true);
    cout << "total of " << noCoinc << " coincidences found." << endl;
    cout << "-------------------------------------------------------" << endl;
    cout << endl;

    cout << "Calculates CTRs:" << endl;
    ana->getCTRs();
    cout << endl;

    std::string ofname = "out.root";
    if (vm.count("output-file")) {
        ofname = vm["output-file"].as<std::string>();
    }


    cout << "Create output file: " << ofname << endl;
    TFile * ofile = new TFile(ofname.c_str(),"RECREATE");

    //cout << "check DNL tables..." << endl;
    //ana->checkDNLTable();

    cout << "Save histograms..." << endl;
    ana->writeChiaraPlots(vm["stat-cut"].as<double>());
    ana->writeOverview(vm["stat-cut"].as<double>());
    ana->writeHistos(true);


    ofile->Close();
    cout << "END" << endl;
    return 0;
}
