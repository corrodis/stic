#include "events.h"

#define VCO_SHIFT 0

stic_data_e::stic_data_e() {
	handleID = 0;
	packetID = 0;
	frame_number = 0;
	channel = 0;
    T_CC = 0;
	T_badhit = 0;
	T_fine = 0;
    E_CC = 0;
	E_badhit = 0;
    E_fine = 0;

	time = 0;
	energy = 0;
}

void stic_data_e::printHeader(std::ostream &st) {
	st << "handleID" << "\t";
	st << "packetID" << "\t";
	st << "frame_number" << "\t";
	st << "channel" << "\t";
    st << "T_CC" << "\t";
    st << "T_fine" << "\t";
	st << "T_badhit" << "\t";
    st << "E_CC" << "\t";
    st << "E_fine" << "\t";
	st << "E_badhit" << "\t";
	st << "time" << "\t";
	st << "energy" << "\t";
}

void stic_data_e::print( const char* delimiter, std::ostream &st) {
	st << handleID << delimiter;
	st << packetID << delimiter;
	st << frame_number << delimiter;
	st << channel << delimiter;
    st << T_CC << delimiter;
    st << T_fine << delimiter;
	st << T_badhit << delimiter;
    st << E_CC << delimiter;
    st << E_fine << delimiter;
	st << E_badhit << delimiter;
	st << time << delimiter;
	st << energy << delimiter;
}

bool stic_data_e::eq(stic_data_e * event) {
	//if(eventA->handleID =! eventB->handleID)
	//	return 0;
	//if(eventA->packetID =! eventB->packetID)
	//	return 0;
	//if(eventA->frame_number =! eventB->frame_number)
	//	return 0;
	
	if(channel =! event->channel)
		return 0;
    if(T_CC =! event->T_CC)
		return 0;
    if(T_fine =! event->T_fine)
		return 0;
	if(T_badhit =! event->T_badhit)
		return 0;
    if(E_CC =! event->E_CC)
		return 0;
    if(E_fine =! event->E_fine)
		return 0;
	if(E_badhit =! event->E_badhit)
		return 0;

	return 1;
}

Int_t stic_data_e::getEnergy() {
    return energy;

    /*Int_t energy = E_CC-T_CC;
	if(energy>=0)
	{
		return energy;
	}
	else {
		return energy+(1<<15);
    }*/

}

Int_t stic_data_e::getTcc() {
    return T_CC;
}

Int_t stic_data_e::getTime() {
    return time;
}

Int_t stic_data_e::getTfine() {
    return T_fine;
    //return time;
	//Int_t Tfine = 0;

	//if((E_CCM - E_CCS) <= 1 && (T_CCM - T_CCS) <= 1 ) {
        /*if(T_fine < 2 || (T_fine > 13 && T_fine < 18) || T_fine > 29)
		{
			//Discard finecounter values where the CC was just flipping
			return -100000;
		}
		else if((T_CCM - T_CCS) == 1)
		{
			//Tfine = (T_fine + VCO_SHIFT) % 32;
			//return T_CCM*32;
			return  (T_CCM*0x20 + T_fine);
		}
		else if ((T_CCM - T_CCS) == 0)
		{
			//Tfine = (T_fine + 16 + VCO_SHIFT) % 32;
			//return T_CCM*32;
			return (T_CCS*32 + T_fine);
		}
		else {
			return -100000;
        }*/
	//}	
	//else {
	//	return -100000;
	//}
}
