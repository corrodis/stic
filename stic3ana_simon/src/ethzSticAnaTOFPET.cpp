#include "ethzSticAna.h"

#include "TMath.h"
#include "TCanvas.h"
#include <iomanip>

using std::cout;
using std::endl;
using std::to_string;

int ethzSticAna::buildCoincTOFPET(bool discardMulti) {
    bool fast = !discardMulti;
    if(!_energyLoadeded()) {
        std::cerr << "ethzSticAna::buildCoinc please load first the energies with loadEnergy" << std::endl;
        return -1;
    }

    if(discardMulti && fast) {
        std::cerr << "Its not possible to discard multiplicities and use fast mode at the same time" << std::endl;
    }
    std::map<chPair, unsigned int> coincNo;
    initDeltaTHistos();

    TTree * sortedTree;
    // If file ist called _sort.root, assume it is already sorted
    if((std::string(_fname).substr(std::string(_fname).length()-9, 4) == "sort"))
        sortedTree = _itree;
    else
        sortedTree = sortTOFPET();
    cout << "build coincidences..." << endl;

    // Used variables in TOFPET Tree lmData
    UShort_t channel = 0;
    Long64_t time = 0;
    Float_t energy = 0;
    sortedTree->SetBranchAddress("channel", &channel);
    sortedTree->SetBranchAddress("tot", &energy);
    sortedTree->SetBranchAddress("time", &time);

    // Loop over all events
    double lastFrameTime = 0; // used to slice global time into frames
    double windowStart = 0;      // used to check multiplicities in coinc windows
    //int multiCountOffset = 0; // used for offset in multiplicity counts if one element is already removed but opens up the new window
    //std::vector<unsigned int> multiCount;   // store the all channels in which the current coinc window has events
    std::vector<unsigned int> windowHits;
    unsigned int windowHitsOffset = 0;
    std::vector<double> multiHits;          // used to store all hits of one pair

    chPair pair;

    std::map<unsigned int, std::vector<Long64_t>> eventTimes;
    for(unsigned int i = 0; i < sortedTree->GetEntries(); ++i) {
        if(i%1000000==0)
            std::cout << "\r" << std::fixed << std::setprecision(2) << (float)i/_itree->GetEntries()*100 << "%" << std::flush;// << std::endl;
        sortedTree->GetEntry(i);

        //std::cout << "Event " << i << ": ";
        //std::cout << "channel "<< channel << ", ";
        //std::cout << "tot "<< energy << ", ";
        //std::cout << "time "<< time << std::endl;

        // Check if pseudo frame boarder, if yes process data
        if(time > (lastFrameTime + frameLength * (1 << coincBuilderFrames))) { // frameLength is in ps
            if(i > 0) {
                for(auto ch1 = eventTimes.begin(); ch1 != eventTimes.end(); ++ch1) {
                    for(auto ch2 = std::next(ch1); ch2 != eventTimes.end(); ++ch2) {
                        //std::cout << "debug: " << ch1->first << ", " << ch2->first << std::endl;
                        if(ch1->first < ch2->first)
                            pair = std::make_pair(ch1->first, ch2->first);
                        else
                            pair = std::make_pair(ch2->first, ch1->first);
                        if(std::find(_coincPairs.begin(), _coincPairs.end(), pair) == _coincPairs.end())
                            continue;
                        for(auto hit1 = eventTimes[pair.first].begin(); hit1 != eventTimes[pair.first].end(); ++hit1) {
                            for(auto hit2 = eventTimes[pair.second].begin(); hit2 != eventTimes[pair.second].end(); ++hit2) {
                                double deltaT = (*hit1%(frameLength)-*hit2%(frameLength));
                                if(abs(deltaT) <= coincWindow*1e3) {
                                        multiHits.push_back(deltaT);
                                        //coincNo[pair]++;
                                        //_hDeltaT[pair]->Fill(deltaT);
                                }
                            }
                            hMultiplicity->Fill(multiHits.size());
                            for(auto it = multiHits.begin(); it != multiHits.end(); it++) {
                                if(discardMulti && multiHits.size() > 1) break;
                                coincNo[pair]++;
                                _hDeltaT[pair]->Fill(*it);
                            }
                            multiHits.clear();
                        }
                    }
                }
            }
            eventTimes.clear();
            //for(auto it = eventTimes.begin(); it != eventTimes.end(); it++) (it->second).clear(); // clear events
            lastFrameTime = time;
            //if(!fast) {
                windowHits.clear();
                windowHitsOffset = 0;
            //}
        }

        // Collect events within one pseudo frame
        if(energy > _eCuts[channel].first && energy < _eCuts[channel].second) {
            int windowMulti = windowHits.size() + windowHitsOffset;
            if(windowMulti==0) {
                windowStart = time; // start new window
                windowHitsOffset = 0;
            } else {
                if(time > windowStart + coincWindow*1e3) {
                    hGlobalMultiplicity->Fill(windowMulti);
                    if(windowMulti < 2) {
                        windowHits.clear();
                        windowStart = time;
                        windowHitsOffset = 0;
                    } else if(windowMulti >= 2) {
                        unsigned int lastChannel = windowHits.back();
                        //windowHits.push_back(lastChannel);
                        windowStart = eventTimes[lastChannel].back();
                        windowHitsOffset = 1;

                        if(discardMulti && windowMulti > 2) {
                            for(auto & hit : windowHits) {
                                eventTimes[hit].pop_back();
                            }
                        }

                        windowHits.clear();
                    }
                }
            }
            windowHits.push_back(channel);
            eventTimes[channel].push_back(time);
        }
    }
    std::cout << "\r100.00%" << std::endl;

    int coincNoSum = 0;
    for(auto it = coincNo.begin(); it != coincNo.end(); it++) {
        hCoincStatistics->SetBinContent(hCoincStatistics->FindBin(getPairId((*it).first)), it->second);
        coincNoSum += it->second;
    }
    return coincNoSum;
}

TTree * ethzSticAna::sortTOFPET() {
    Int_t n = _itree->GetEntries();
    Int_t * index = new Int_t[n];
    for(Int_t i = 0; i < n; ++i) {
        index[i] = i;
    }

    cout << "sorting... " << endl;
    TMath::Sort(n, _times, index, kFALSE);

    // Load events sorted into the new tree
    TFile * tmpFile = new TFile((std::string(_fname).substr(0, std::string(_fname).length()-5)+"_sort.root").c_str(), "RECREATE");
    TTree * sortedTree = (TTree*)_itree->CloneTree(0);
    cout << "creat sorted tree... " << endl;
    for (Int_t i = 0; i < n; ++i) {
        _itree->GetEntry(index[i]);
        sortedTree->Fill();
        if(i%100000 == 0)
            cout << " " << (float)i/n*100 << " %" << "\r" << std::flush;
    }
    cout << "100.0000 %" << endl;

    //tmpFile->Close();
    //_ifile->Close();
    //_ifile = new TFile((std::string(_fname).substr(0, std::string(_fname).length()-5)+"_sort.root").c_str(), "READ");
    return (TTree*)_ifile->Get("lmData");
}

int ethzSticAna::loadEnergyTOFPET(double energyMin) {
    Int_t n = _itree->GetEntries();
    _times = new Long64_t[n];

    UShort_t channel = 0;
    Long64_t time = 0;
    Float_t energy = 0;

    _itree->SetBranchAddress("channel", &channel);
    _itree->SetBranchAddress("tot", &energy);
    if(needSorting()) _itree->SetBranchAddress("time", &time);

    int noChannels = initEnergyHistos();

    //cout << "Load energies: " << endl;
    for(Int_t i = 0; i < n; ++i) {
        _itree->GetEntry(i);
        if(i%1000000 == 0)
            cout << " " << (float)i/n*100 << " %" << "\r" << std::flush;
        // store all times if a global sorting is required later
        if(needSorting())  _times[i] = time;
        _hEnergy[channel]->Fill(energy);
    }
    cout <<  "100.0000 %" << endl;

    setEnergyCuts(energyMin);
    return noChannels;
}

bool ethzSticAna::needSorting() {return (std::string(_fname).substr(std::string(_fname).length()-9, 4) != "sort");}
