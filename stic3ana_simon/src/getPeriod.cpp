#include "TROOT.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TH1I.h"
#include "TString.h"
#include "TBranch.h"
#include "TClassTable.h"
#include "EventType.h"
#include "EventDict.h"
#include "events.h"
#include "TSpectrum.h"

#include <map>
#include <iostream>
#include <vector>
#include <algorithm> 
#include <string> 


int main(int argc, char *argv[]) {

    Int_t optPrint = 0;

    // Used for periode
    std::map<Int_t, Int_t> lastTime;

    std::cout << "Start getPeriod" << std::endl;
	stic_data_e * event = NULL;

	int lastindex = string(argv[1]).find_last_of("."); 
	string fname = string(argv[1]).substr(0, lastindex); 
    string suffix = "";
    if(argc >= 3)
        suffix = string(argv[2]);

    // Add possibility to apply E-cuts for single construction
    Float_t Ecut[2] = {-1, -1};
    if(argc>=5) {
        Ecut[0] = atof(argv[3]);
        Ecut[1] = atof(argv[4]);
    }


    TFile * file = new TFile((TString)fname+(TString)".root","READ");
	TTree * tree = (TTree*)file->Get("dump");
	TBranch * branch = tree->GetBranch("br");
	branch->SetAddress(&event);

    TFile * filePeriod = new TFile((TString)fname+(TString)suffix+(TString)"_period"+(TString)".root","RECREATE");

    std::map<Int_t,TH1I *> h_period;
    for(Int_t i = 0; i < 64; ++i) {
        h_period[i] = new TH1I(Form("h_period_%i",i),Form("Period ch%i",i),(1<<20),-0.5,(1<<20)-0.5);
    }
	if(optPrint>0)
		event->printHeader();
	std::cout << std::endl;


	for(Int_t i=0;i<branch->GetEntries();i++) {
        if(i%100000==0)
            std::cout << (Double_t)i/branch->GetEntries()*100 << "%" << std::endl;
        branch->GetEntry(i);
        if(i < optPrint) {
            event->print("\t");
            std::cout << std::endl;
        }

        // Periode
        Int_t period;
        if((Ecut[0] < 0 || event->getEnergy() >= Ecut[0]) && (Ecut[1] < 0 || event->getEnergy() <= Ecut[1])) {
            period = (event->getTime() - lastTime[event->channel]);
            if(period < 0)
                period += (1 << 20);
            h_period[event->channel]->Fill(period);
            lastTime[event->channel] =  event->getTime();
        }
	}

	std::cout << "Total: " << branch->GetEntries() << std::endl;
	
    // Fit
    TSpectrum *s = new TSpectrum();
    s->Search(h_period[16]);
    Float_t *xpeaks = s->GetPositionX();
    TF1 * period = new TF1("period","gaus",xpeaks[0]-2000,xpeaks[0]+2000);

    for(Int_t i = 16; i < 32; ++i) {
        h_period[i]->Fit("period","RQ");
        cout << i << "\t";
        cout << period->GetParameter("Mean") << "\t" << period->GetParError(period->GetParNumber("Mean")) << "\t";
        cout << period->GetParameter("Sigma") << "\t" << period->GetParError(period->GetParNumber("Sigma")) << "\t";
        cout << endl;
        h_period[i]->Write();
    }

    filePeriod->Close();
	file->Close();
}
