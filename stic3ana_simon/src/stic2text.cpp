#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1I.h"
#include "TString.h"
#include "TBranch.h"
#include "TClassTable.h"
#include "EventType.h"
#include "EventDict.h"
#include "events.h"


#include <iostream>
#include <vector>
#include <algorithm> 
#include <string> 

#include <fstream>	


int main(int argc, char *argv[]) {


    std::cout << "Start stic2text" << std::endl;
	stic_data_e * event = NULL;
	stic_data_e * prev_event = new stic_data_e();

	int lastindex = string(argv[1]).find_last_of("."); 
	string fname = string(argv[1]).substr(0, lastindex); 
	std::cout << "File: " << fname << std::endl;


    //Int_t mainChannel = 30;
    //if(argc >=2 )
    //    mainChannel = atoi(argv[2]);

    std::cout << "open file: " << (TString)fname+(TString)".root" << std::endl;
        TFile * file = new TFile((TString)fname+(TString)".root","READ");
	std::cout << "opened file" << std::endl;
	TTree * tree = (TTree*)file->Get("dump");
	TBranch * branch = tree->GetBranch("br");
	branch->SetAddress(&event);

	std::ofstream fTextFile;
	fTextFile.open((TString)fname+(TString)"_s.txt");
	// dummies to get to the exact same structure as TOFPET
	Float_t step1, step2, tqT, tqE = 0;
	UShort_t tac = 0;
	Double_t tacIdleTimel, channelIdleTime = 0;
	// actualy used variables
	Long64_t time;
	UShort_t channel;
	Float_t tot;



	Long64_t overflow = 0;
	short last_frame = 0;

    std::map<unsigned int, Long64_t> lastTime;
    unsigned int lastTimeStampAnyChannel;
    std::map<unsigned int, Int_t> lastTimeStamp;
    std::map<unsigned int, Int_t> lastFrame;

    std::cout << "start loop" << std::endl;
    int dublicated = 0;
    for(Int_t i=0;i<branch->GetEntries();i++) {
        if(i%100000==0)
            std::cout << (Double_t)i/branch->GetEntries()*100 << "%" << std::endl;
        *prev_event = *event;
        branch->GetEntry(i);

        channel = event->channel;
	//	if(channel > 32) channel += 64; // hack to make 2 matrices appear on 2 chips
	if(channel > 32) channel += 128; // hack to make 2 matrices appear on 2 chips
        tot     = event->getEnergy();
	if(tot < 0) tot += 32768; // wrap around for case where energy timing counter is less than timing (as it exceeded 15-bits)
        
        // Check for double events
        if(prev_event->channel == event->channel && prev_event->getEnergy() == event->getEnergy() && prev_event->getTime() ==  event->getTime()){
            dublicated++;
            if(0) {
                std::cout << "doublicated event found: " << i << std::endl;
                event->printHeader();
                std::cout << std::endl;
                prev_event->print();
                std::cout << std::endl;
                event->print();
                std::cout << std::endl << std::endl;
            }
        }
        else {
            
            
	  if(event->frame_number < last_frame) {
                overflow++;
	  }

            // last_frame = event->frame_number;

            // In Single the summed up time from run start is used
            // thats important since a global sort is applied in Chiaras Analysis
            //if(event->channel == mainChannel && ((event->getTime() + (overflow << 20)) * 50) - time < (500 * 50)) {
            //        //cout << time << "\t" << (event->getTime() + (overflow << 20)) << "\t" << ((event->getTime() + (overflow << 20)) - time) << "\t";
            //        overflow++;
            //        //cout << ((event->getTime() + (overflow << 20)) - time) << "\t" << endl;
            //}
            

	    // int real_frame = event->frame_number >> 3;
	    // fTextFile << overflow << "\t" << real_frame << "\t" << ((event->time) * 50) << "\t" << tot << "\t" << channel << std::endl;

			time    =  ((overflow << 25) + (((event->frame_number >> 3) & 0x1f)  << 20) + (event->time)) * 50;
			//	    time    =  ((overflow << 25) + ((event->frame_number >> 3)  << 20) + (event->time)) * 50;
			fTextFile << (event->time) * 50 << "\t" << event->frame_number << "\t" << tot << "\t" << channel << std::endl;
            		last_frame = event->frame_number;

        }
	}

	std::cout << "Total: " << branch->GetEntries() << " (" << ((float)dublicated/branch->GetEntries())*100 << "% doublicated)" << std::endl;

	fTextFile.close();
	
	file->Close();
}
