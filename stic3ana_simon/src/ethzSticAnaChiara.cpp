#include "ethzSticAna.h"

#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TStyle.h"

#include <iostream>

using std::to_string;

void ethzSticAna::writeChiaraPlots(int statCut, bool nicer) {
    if(!_coincBuilt()) {
        std::cerr << "ethzSticAna::writeChiaraPlots please build first coincidences with coincBuild() and create ctrs with getCTRs()" << std::endl;
        return;
    }
    if(!_ctrBuilt()) {
        std::cerr << "ethzSticAna::writeChiaraPlots please create ctrs with getCTR()" << std::endl;
        return;
    }

    TCanvas * cChiara = new TCanvas("cChiara");
    cChiara->Divide(3, 2);

    Double_t stat[_coincPairs.size()];
    Double_t ctr[_coincPairs.size()];
    Double_t pairId[_coincPairs.size()];
    Double_t mean[_coincPairs.size()];
    TH1F * hMeanSum = new TH1F("hMeanSum", "Mean", 100, -2000, 2000);
    if(nicer) {
        hMeanSum->GetXaxis()->SetTitle("offset [ps]");
        hMeanSum->SetTitle("Offset Distribution");
    }
    TH1F * hDeltaTSum = new TH1F("hDeltaTSum", ("DeltaTime all (Stat>"+to_string(statCut)   +")").c_str(),
                               ctrBinNumber,
                               -(int)((ctrBinNumber/2)*fineBinWidth),
                               (int)((ctrBinNumber/2)*fineBinWidth));
    hDeltaTSum->GetXaxis()->SetTitle("deltaT [ps]");

    int i = 0;
    for(auto pair = _coincPairs.begin(); pair != _coincPairs.end(); pair++) {
        stat[i] = getCoincStat(*pair);
        ctr[i] = _ctr[*pair].first;
        pairId[i] = getPairId(*pair);
        mean[i] = _mean[*pair].first;
        hMeanSum->Fill(_mean[*pair].first);
        i++;
    }

    // pair vs stat
    cChiara->cd(1);
    cChiara->GetPad(1)->SetGrid();
    //hCoincStatistics->Draw();
    TGraph * hPairVsStat = new TGraph(_coincPairs.size(), pairId, stat);
    hPairVsStat->GetXaxis()->SetTitle(("pairID = (ch1 - "+to_string(_minPairId.first)+") x 16 + (ch2 - "+to_string(_minPairId.second)+")").c_str());
    if(nicer) hPairVsStat->GetYaxis()->SetTitle("# of coincidences");
    hPairVsStat->SetMarkerStyle(20);
    if(nicer) hPairVsStat->SetTitle("Number of Coincidences");
    else hPairVsStat->SetTitle("DeltaTime statistics");
    hPairVsStat->Draw("AP");

    // pair vs mean
    cChiara->cd(2);
    cChiara->GetPad(2)->SetGrid();
    TGraph * hPairVsMean = new TGraph(_coincPairs.size(), pairId, mean);
    hPairVsMean->GetXaxis()->SetTitle(("pairID = (ch1 - "+to_string(_minPairId.first)+") x 16 + (ch2 - "+to_string(_minPairId.second)+")").c_str());
    if(nicer) hPairVsMean->GetYaxis()->SetTitle("mean [ps]");
    hPairVsMean->SetMarkerStyle(20);
    if(nicer) hPairVsMean->SetTitle("Offset");
    else hPairVsMean->SetTitle("DeltaTime mean value [ps]");
    //hPairVsMean->GetYaxis()->SetUserRange(-8000, 8000);
    hPairVsMean->Draw("AP");

    // pair vs ctr
    cChiara->cd(3);
    cChiara->GetPad(3)->SetGrid();
    TGraph * hPairVsCtr = new TGraph(_coincPairs.size(), pairId, ctr);
    hPairVsCtr->GetXaxis()->SetTitle(("pairID = (ch1 - "+to_string(_minPairId.first)+") x 16 + (ch2 - "+to_string(_minPairId.second)+")").c_str());
    if(nicer) hPairVsCtr->GetYaxis()->SetTitle("ctr sigma [ps]");
    hPairVsCtr->SetMarkerStyle(20);
    if(nicer) hPairVsCtr->SetTitle("CTR");
    else hPairVsCtr->SetTitle("DeltaTime sigma [ps] = CTR");
    hPairVsCtr->Draw("AP");

    // Statistics vs Mean
    cChiara->cd(4);
    cChiara->GetPad(4)->SetGrid();
    TGraph * gStatVsCtr = new TGraph(_coincPairs.size(), stat, ctr);
    gStatVsCtr->GetXaxis()->SetTitle("Statistics");
    gStatVsCtr->GetYaxis()->SetTitle("ctr sigma [ps]");
    gStatVsCtr->SetMarkerStyle(20);
    gStatVsCtr->SetTitle("CTR vs Stat");
    gStatVsCtr->Draw("AP");
    gStyle->SetOptFit(11111);
    TF1 * fMeanCtr = new TF1("fMeanCtr", "pol0", statCut, gStatVsCtr->GetXaxis()->GetXmax());
    gStatVsCtr->Fit("fMeanCtr", "qR");

    // Mean sum
    cChiara->cd(5);
    gStyle->SetOptFit(11);
    gStyle->SetOptStat(1110);
    //cChiara->GetPad(5)->SetGrid();
    hMeanSum->Fit("gaus","q");
    hMeanSum->Draw();

    // CTR sum
    cChiara->cd(6);
    gStyle->SetOptFit(1111111);
    cChiara->GetPad(6)->SetLogy();
    for(auto hDeltaT = _hDeltaT.begin(); hDeltaT != _hDeltaT.end(); hDeltaT++) {
        if(getCoincStat(hDeltaT->first) > statCut) {
            hDeltaTSum->Add(hDeltaT->second);
        }
    }
    Double_t par[4];
    TF1 * fGaus = new TF1("fGaus", "gaus", -(int)((ctrBinNumber/2)*fineBinWidth), (int)((ctrBinNumber/2)*fineBinWidth));
    TF1 * fConst = new TF1("fConst", "pol0", -(int)((ctrBinNumber/2)*fineBinWidth), (int)((ctrBinNumber/2)*fineBinWidth));
    hDeltaTSum->Fit("fGaus","qR");
    hDeltaTSum->Fit("fConst","qR+");
    TF1 * fComb = new TF1("fComb", "gaus(0)+pol0(3)", -(int)((ctrBinNumber/2)*fineBinWidth), (int)((ctrBinNumber/2)*fineBinWidth));
    fGaus->GetParameters(&par[0]);
    fConst->GetParameters(&par[3]);
    fComb->SetParameters(par);
    hDeltaTSum->Fit("fComb","qR");
    gStyle->SetOptFit(111111);
    hDeltaTSum->Draw();

    cChiara->Write();
}
