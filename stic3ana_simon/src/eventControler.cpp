#include "eventControler.h"
#include "TH1I.h"
#include "TROOT.h"

#include <fstream>
#include <string>

eventControler::eventControler(const char* fname) {
	file = new TFile(fname,"READ");
	fsource = fname;
	fileEdit = new TFile("edit.root","RECREATE");
	tree = (TTree*)file->Get("dump");

	//enCoin = 0;
	for(Int_t j=0;j<64;j++)
	{
		enCut[j] = 0;
		Elow[j] = -1; 
		Eup[j] = -1;
		coinPair[j] = -1;
	}

	enStatLog = 0;
	//stReport = std::cout;
}

eventControler::~eventControler() {
	fileEdit->Write();
}

//void eventControler::setFileReport(const char* fname) {
	//fstream fs;
	//stReport.open(fname,fstream::in | fstream::out | fstream::app);
	//stReport = fs;
//}

Int_t eventControler::printEntry(const char* treeName, Int_t n) {
	printEntries(treeName, 1, 1, n);
}

void eventControler::setStatLog(const char* fname) {
	eventControler::enStatLog = 1;
	eventControler::statLogFile = fname;
}

void eventControler::setCut(Int_t ch) {
	eventControler::enCut[ch] = 1;
	eventControler::Elow[ch] = -1;
	eventControler::Eup[ch] = -1;
}

void eventControler::setCut(Int_t ch, Int_t Elow, Int_t Eup) {
	eventControler::enCut[ch] = 1;
	eventControler::Elow[ch] = Elow;
	eventControler::Eup[ch] = Eup;
	//cout << "Set Cut of ch" << ch << " to " << eventControler::Elow[ch] << " - " << eventControler::Eup[ch] << std::endl;
}

Int_t eventControler::printEntries(const char* treeName, Int_t n, Int_t step, Int_t start, std::ostream &st) {	
	Int_t count = 0;
	if(treeName == "dump")
		TTree * tree = (TTree*)file->Get("dump");
	else
		TTree * tree = (TTree*)fileEdit->Get(treeName);
	TBranch * branch;


	// if std branch "br"
	//if(brName == "br") {
		stic_data_e * event = NULL;

	//std::cout << tree << std::endl;
	//return 0; 

		branch = tree->GetBranch("br");
		branch->SetAddress(&event);

		// Output headers
		st << "no" << "\t";
		event->printHeader(st);
		st << std::endl;

		Int_t nn;
		if(n==-1)
			nn = tree->GetEntries();
		else
			nn = n+start;  

	//std::cout << branch << " " << nn << " " << step << " " << std::endl;
	//return 0;

		for(Int_t i=start;i<nn;i=i+step) {
			branch->GetEntry(i);
			st << i << "\t";
			event->print("\t",st);
			st << std::endl;

			count++;
		}
	return count;	
	//}
	//else {
		// TODO
	//}

}

Int_t eventControler::run() {
	Int_t count[64] = {0};

	stic_data_e * event = NULL;
	stic_data_e * eventCurrent = new stic_data_e; 
	stic_data_e * eventPrev = new stic_data_e; 
	stic_data_e * eventTrue = new stic_data_e; 
	stic_data_e * eventTruePrev[64]; // used for Period calculation
	stic_data_e * eventTrueCutPrev[64]; // used for Period calculation

	for(Int_t j=0;j<64;j++) {
		eventTruePrev[j] = new stic_data_e; 
		eventTrueCutPrev[j] = new stic_data_e; 
	}

	TBranch * branch = tree->GetBranch("br");
	branch->SetAddress(&event);


	// New Trees for different selections
	Int_t number;
	TTree * treeTwins = new TTree("Twins","Twins");
	treeTwins->Branch("br","stic_data_t", event);
	treeTwins->Branch("number",&number,"number/I");
	TTree * treeCC = new TTree("CCerrors","CCerrorsC");
	treeCC->Branch("br","stic_data_t", event);
	treeCC->Branch("number",&number,"number/I");

	Int_t channel, energy, timeCC, timeFine, fine;
	Int_t periodCC, periodFine;
	
	TTree * treeClean = new TTree("Cleaned","Cleaned");
	//treeClean->Branch("br",eventCurrent,"stic_data_t");
	treeClean->Branch("number",&number,"number/I");
	treeClean->Branch("channel",&channel,"channel/I");
	treeClean->Branch("energy",&energy,"energy/I");
	treeClean->Branch("timeCC",&timeCC,"timeCC/I");
	treeClean->Branch("timeFine",&timeFine,"timeFine/I");
	treeClean->Branch("fine",&fine,"fine/I");
	treeClean->Branch("periodCC",&periodCC,"periodCC/I");
	treeClean->Branch("periodFine",&periodFine,"periodFine/I");

	Int_t d_time, d_energy;
	treeClean->Branch("debug_energy",&d_energy,"debug_energy/I");
	treeClean->Branch("debug_time",&d_time,"debug_time/I");

	TTree * treeCleanR = new TTree("CleanedR","CleanedR");
	//treeCleanR->Branch("br","stic_data_t", event);
	treeCleanR->Branch("number",&number,"number/I");
	treeCleanR->Branch("channel",&channel,"channel/I");
	treeCleanR->Branch("energy",&energy,"energy/I");
	treeCleanR->Branch("timeCC",&timeCC,"timeCC/I");
	treeCleanR->Branch("timeFine",&timeFine,"timeFine/I");
	treeCleanR->Branch("fine",&fine,"fine/I");

	treeCleanR->Branch("debug_energy",&d_energy,"debug_energy/I");
	treeCleanR->Branch("debug_time",&d_time,"debug_time/I");

	TTree * treeCut = new TTree("Cut","Cut");
	//treeCut->Branch("br","stic_data_t", event);
	treeCut->Branch("number",&number,"number/I");
	treeCut->Branch("channel",&channel,"channel/I");
	treeCut->Branch("energy",&energy,"energy/I");
	treeCut->Branch("timeCC",&timeCC,"timeCC/I");
	treeCut->Branch("timeFine",&timeFine,"timeFine/I");
	treeCut->Branch("fine",&fine,"fine/I");
	treeCut->Branch("periodCC",&periodCC,"periodCC/I");
	treeCut->Branch("periodFine",&periodFine,"periodFine/I");

	Int_t channel2, energy2, deltaTcc, deltaTfine;
	TTree * treeCoin = new TTree("Coin","Coin");
	//treeCut->Branch("br","stic_data_t", event);
	//treeCut->Branch("number",&number,"number/I");
	treeCoin->Branch("channel1",&channel,"channel1/I");
	treeCoin->Branch("channel2",&channel2,"channel2/I");
	treeCoin->Branch("energy1",&energy,"energy1/I");
	treeCoin->Branch("energy2",&energy2,"energy2/I");
	treeCoin->Branch("deltaTcc",&deltaTcc,"deltaTcc/I");
	treeCoin->Branch("deltaTfine",&deltaTfine,"deltaTfine/I");


	Int_t ch1_true, ch2_true, energy1_true,energy2_true, deltaTcc_true, deltaTfine_true, no_true;
	Int_t timeCC1_true, timeFine1_true;
	TTree * treeCoin_true = new TTree("CoinTrue","CoinTrue");
	//treeCut->Branch("br","stic_data_t", event);
	//treeCut->Branch("number",&number,"number/I");
	//treeCoin_true->Branch("channel1",&ch1_true,"ch1_true/I");
	//treeCoin_true->Branch("channel2",&ch2_true,"ch2_true/I");
	treeCoin_true->Branch("energy1",&energy1_true,"energy1_true/I");
	treeCoin_true->Branch("energy2",&energy2_true,"energy2_true/I");
	treeCoin_true->Branch("deltaTcc",&deltaTcc_true,"deltaTcc_true/I");
	treeCoin_true->Branch("deltaTfine",&deltaTfine_true,"deltaTfine_true/I");
	treeCoin_true->Branch("no",&no_true,"no_ture/I");


	// Error Counters
	Int_t countTwin[64] = {0}; 
	Int_t countNTwin[64] = {0};
	Int_t countTCC[64] = {0};
	Int_t countECC[64] = {0};
	Int_t countCC[64] = {0};
	Int_t countRAW[64] = {0};
	Int_t countGood[64] = {0};

	// Coincidence maps
	//map<Int_t, stic_data_e> coinMap[64];
	//Int_t lastTcc[64] = {0};

	// Flags
	bool lastEventOk[64] = {0};
	bool lastCutEventOk[64] = {0};

	//std::cout << branch->GetEntries();
	//std::cout << "ok";

	for(Int_t i=0;i<branch->GetEntries();i++) {
	//for(Int_t i=0;i<47000000;i++) {
	//for(Int_t i=0;i<1000000;i++) {
		if(i%100000==0)
			std::cout << i << "\t(" << ((float)i/branch->GetEntries()*100) << "%)" << std::endl;
		number = i;
		if(i>0)
		{
			*eventPrev = *event;
		}

		branch->GetEntry(i);
		channel =  event->channel;
		if(channel > 64)
			continue; // some date sets seem to be corrupt, ignore these data here
		energy =   event->getEnergy();
		timeCC =   event->getTcc();
		timeFine = event->getTfine();
		fine = event->T_fine;
		d_time =   event->time - timeFine;
		d_energy = event->energy - energy;
		//std::cout << event->channel << std::endl;

		if(event->eq(eventPrev)) 
		{
			countTwin[channel]++;
			treeTwins->Fill();

		}
		else
		{
			// Twins removed
			countNTwin[channel]++;

            /*if(event->TCCerror() || event->ECCerror())
			{
				// CC Error
				countCC[channel]++; 
				treeCC->Fill();
				if(event->TCCerror())
					countTCC[channel]++;
				if(event->ECCerror())
					countECC[channel]++;

				// If the last events was corrupted, period calculation is not possible
				// Set it therefore to -1
				periodCC = -10000;
				periodFine = -10000;
				lastEventOk[channel] = 0;
				lastCutEventOk[channel] = 0;
            }*/
            //else {

				// No CC error, good events
			treeTwins->Fill();
				// Calculate period of two events if
				//   - last event was good as well
				// otherwise set it to -1
				if(lastEventOk[channel] == 1)
				{
					periodCC = eventControler::getPeriodeCC(event,eventTruePrev[channel]);
					periodFine = eventControler::getPeriodeFine(event,eventTruePrev[channel]);
				}
				else {
					// Last event of this channel was corrupt, no period calculation possible
					// Set it to -1
					periodCC = -10000;
					periodFine = -10000;
				}
				lastEventOk[channel] = 1;
				*eventTruePrev[channel] = *event;


                //if(event->RAWerror) {
                //	countRAW[channel]++;
                //}
                //else {
					treeCleanR->Fill();
                //}

		
				// Good Events (all)
				// Save them
				countGood[channel]++;
				//std::cout << "Fill:" << channel << endl;
				treeClean->Fill();

				// Cut good events
				// -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

				// create new coin events, according to Chiara, called CoinTrue
				if(coinPair[channel] >=0 && channel < coinPair[channel] && 0) {
					// save event properties
					*eventTrue = *event;
					//ch1_true = channel;
					//ch2_true = coinPair[channel];
					//energy1_true = energy;
					//timeCC1_true = timeCC;
					//timeFine1_true  = timeFine;
					Int_t restorEvent = i;
					no_true = 0;

					// search for event in the same CC counter in the coin Channel
					// Forward
					Int_t cntDebug = 0;
					do {
						i++;
						cntDebug++;
						branch->GetEntry(i);
						if(event->channel==coinPair[eventTrue->channel]  && event->getTcc()==eventTrue->getTcc()){
							std::cout << "ping" << std::endl;
							no_true++;
							energy1_true = eventTrue->getEnergy();
							energy2_true = event->getEnergy();
							deltaTcc_true = eventControler::getDeltaT(event, eventTrue);
							deltaTfine_true = eventControler::getDeltaTfine(event, eventTrue);;
						} 
					} while(cntDebug < 100 && i < branch->GetEntries());
					// Backward
					i = restorEvent;
					cntDebug = 0

;
					do {
						i--;
						cntDebug++;
						branch->GetEntry(i);
						if(event->channel==coinPair[eventTrue->channel]  && event->getTcc()==eventTrue->getTcc()){
							no_true++;
							energy1_true = eventTrue->getEnergy();
							energy2_true = event->getEnergy();
							deltaTcc_true = eventControler::getDeltaT(event, eventTrue);
							deltaTfine_true = eventControler::getDeltaTfine(event, eventTrue);;
						} 
					} while(cntDebug < 100 && i > 0);
					if(no_true>0)
						treeCoin_true->Fill();

					// restore event
					i = restorEvent;
					branch->GetEntry(i);
					energy =   event->getEnergy();
					timeCC =   event->getTcc();
					timeFine = event->getTfine();
					fine = event->T_fine;
					d_time =   event->time - timeFine;
					d_energy = event->energy - energy;

					
				}
				// 1) channel
				if(eventControler::enCut[channel]==1 && channel < 64)
				{
					// std::cout << channel << " " << enCut[channel] << std::endl;
					// 2) Energy
					if((eventControler::Elow[channel] == -1 || (energy >= eventControler::Elow[channel])) && (eventControler::Eup[channel] == -1 || (energy <= eventControler::Eup[channel])))
					{
						
						// Recalculate Periode
						if(lastCutEventOk[channel] == 1)
						{
							periodCC = eventControler::getPeriodeCC(event,eventTrueCutPrev[channel]);
							periodFine = eventControler::getPeriodeFine(event,eventTrueCutPrev[channel]);
						}
						else {
							periodCC = -10000;
							periodFine = -10000;
						}
					

						treeCut->Fill();

						// Coin search
						if(coinPair[channel]!=-1 && lastCutEventOk[coinPair[channel]] == 1 && eventTrueCutPrev[coinPair[channel]]->getEnergy() > 0)
						//if(coinPair[channel]!=-1 && lastCutEventOk[coinPair[channel]] == 1)
						{
							//std::cout << channel << " " << coinPair[channel] << " " << energy << " " << eventTrueCutPrev[coinPair[channel]]->getEnergy() << " "  << timeCC << " " << eventTrueCutPrev[coinPair[channel]]->getTfine() << std::endl;

							channel2 = coinPair[channel];
							energy2 = eventTrueCutPrev[coinPair[channel]]->getEnergy();
							// Always subtract lower ch
							if(coinPair[channel] > channel)
							{
								deltaTcc = eventControler::getDeltaT(event, eventTrueCutPrev[coinPair[channel]]);
								deltaTfine = eventControler::getDeltaTfine(event, eventTrueCutPrev[coinPair[channel]]);
								energy2 = eventTrueCutPrev[coinPair[channel]]->getEnergy();
							}
							else {
								deltaTcc = eventControler::getDeltaT(eventTrueCutPrev[coinPair[channel]],event);
								deltaTfine = eventControler::getDeltaTfine(eventTrueCutPrev[coinPair[channel]],event);
								energy2 = energy;
								energy = eventTrueCutPrev[coinPair[channel]]->getEnergy();
							}
							// If a cut window is needed, insert that here
							//std::cout << energy << "\t" << energy2<< "\t" << deltaTcc << "\t" << deltaTfine << std::endl;
							treeCoin->Fill();

			
							// Save events of one CC period (52mus) in two map containers
							//if(event->getTcc()<lastTcc[channel])
							//{
							//	// Iterate through this map, finding coincidence in other channels
							//	for(std::map<Int_t,stic_data_e>::iterator it=coinMap[channel].begin();it!=coinMap[channel].end(); ++it)
							//	{
							//		it->
							//	}

							//	coinMap[channel].clear();
							//}
							//else {
								
							//}
							//coinMap[channel][event->getTcc()] = *event;
							//lastTcc[channel] = event->getTcc();

							

						} 

						lastCutEventOk[channel] = 1;
						*eventTrueCutPrev[channel] = *event;

					}
					else {
						// outside of E cut
						lastCutEventOk[channel] = 0;
					}
				}
			}
        //}
		//std::cout << channel << " " << count[channel] << std::endl;
		count[channel]++;
		
		
	}

	fileEdit->Write();
	std::cout << "start creating outputs" << std::endl;
	// Print summary
	ofstream myfile;
	myfile.open("edit/log.data", ios::in | ios::ate);
	const char * dac = strstr(fsource, "scan_");
	//std::cout << "dac: " << dac << std::endl;
	string dacString;
	if(dac!=NULL)
	{
		dacString = string(dac+5);
		replace(dacString.begin(), dacString.end(), '-', '\t');
		replace(dacString.begin(), dacString.end(), '.', '\t');
	}


	std::cout << "-- -- -- -- -- -- file " << fsource << " -- -- -- -- -- --" << std::endl;
	for(Int_t j=0;j<64;j++)
	{
		if(count[j]>0)
		{
			//if(enStatLog==0)
			//{
		
				std::cout << "-- -- -- -- -- -- channel " << j << " -- -- -- -- -- --" << std::endl;
				std::cout << "Events:\t\t" << count[j] << endl;
				std::cout << "Twins:\t\t" << countTwin[j] << "\t(" << float(countTwin[j])/count[j] << ")" << std::endl;
				std::cout << "True Events:\t" << countNTwin[j] << endl;
				std::cout << "Error T_CC:\t" << countTCC[j] << "\t(" << float(countTCC[j])/countNTwin[j] << ")" << std::endl;
				std::cout << "Error E_CC:\t" << countECC[j] << "\t(" << float(countECC[j])/countNTwin[j] << ")" << std::endl;
				std::cout << "Error CC:\t" << countCC[j] << "\t(" << float(countCC[j])/countNTwin[j] << ")" << std::endl;
				std::cout << "Good Events:\t" << countGood[j] << "\t(" << float(countGood[j])/countNTwin[j] << ")" << std::endl;
				std::cout << "RAW Error:\t" << countRAW[j] << "\t(" << float(countRAW[j])/countNTwin[j] << ")" << std::endl;
				std::cout << std::endl;
			//}
			//else {
			//}
			
			// Create histograms for these channels
			addHist("Cleaned",j);
			if(enCut[j]==1)
			{
				addHist("Cut",j);
			}
			if(coinPair[j]!=-1)
			{
				if(coinPair[j] > j)
				{
					addHist("Coin",j,coinPair[j]);
					addHist("CoinTrue",j,coinPair[j]);
				}
			}
		}
		if(dac==NULL)
		{
			myfile << fsource << "\t";
		}
			else {
			myfile << dacString << "\t";
		}
		myfile << j << "\t";
		myfile << count[j] << "\t";
		myfile << countTwin[j] << "\t";
		myfile << countTCC[j] << "\t";
		myfile << countECC[j] << "\t";
		myfile << countCC[j] << "\t";
		myfile << countGood[j] << "\t";
		myfile << countRAW[j] << std::endl;
	}
	myfile.close();
	

	return count[0];	
}

void eventControler::setCoin(Int_t ch1, Int_t ch2) {
	eventControler::coinPair[ch1] = ch2;
	eventControler::coinPair[ch2] = ch1;
	eventControler::enCut[ch1] = 1;
	eventControler::enCut[ch2] = 1;
	//for(Int_t j=0;j<64;++j)
	//	std::cout << j << " " << enCut[j] << std::endl; 
}

void eventControler::setFile(const char* fname) {
	fileEdit = new TFile(fname,"RECREATE");
	//TFile * oldfile = file;
	//TTree * oldtree = tree;
	//stic_data_t * event;
	//oldtree->SetBranchStatus("br",1);
	
	//file = new TFile(fname,"RECREATE");
	//tree = oldtree->CloneTree(-1,"fast");

	//file->Write();
	//delete oldfile;
	//delete oldtree;
}

void eventControler::addHist(const char * tname, Int_t ch, Int_t ch2) {
	tree = (TTree*)fileEdit->Get(tname);
	if(tname == "Coin")
	{
		Int_t coinLow = -32767;
		Int_t coinUp = 32767;
		tree->Draw(Form("energy1>>%s_energy_ch%i(32767,-0.5,32767.5)",tname,ch),Form("deltaTcc>%i&&deltaTcc<%i",coinLow,coinUp));
		
		tree->Draw(Form("energy2>>%s_energy_ch%i(32767,-0.5,32767.5)",tname,ch2),Form("deltaTcc>%i&&deltaTcc<%i",coinLow,coinUp));
		//tree->Draw(Form("energy1:energy2>>%s_energy",tname),Form("deltaTcc>%i&&deltaTcc<%i",coinLow,coinUp),"COLZ");
		tree->Draw(Form("deltaTcc>>%s_detltaTcc(65535,-32767.5,32767.5)",tname),Form("deltaTcc>%i&&deltaTcc<%i",coinLow,coinUp));
		//TH1I *histo = (TH1I*)gROOT->FindObject(Form("deltaTcc>>%s_detltaTcc(65535,-32767.5,32767.5)",tname));
		//histo->SetTitle("Coincidence (CC)");
		//histo->GetXaxis()->SetTitle("time [1.64ns]");

		tree->Draw(Form("deltaTfine>>%s_detltaTfine(2097515,-1048575.5,1048575.5)",tname),Form("deltaTcc>%i&&deltaTcc<%i",coinLow,coinUp));
		//histo = (TH1I*)gROOT->FindObject(Form("deltaTfine>>%s_detltaTfine(2097515,-1048575.5,1048575.5)",tname));
		//histo->SetTitle("Coincidence (fine)");
		//histo->GetXaxis()->SetTitle("time [51ps]");
	}
	else if(tname == "CoinTrue") {
		std::cout << "create coinTrue histograms" << std::endl;
		tree->Draw(Form("energy1>>%s_energy_ch%i(32767,-0.5,32767.5)",tname,ch));
		tree->Draw(Form("energy2>>%s_energy_ch%i(32767,-0.5,32767.5)",tname,ch2));
		//tree->Draw(Form("energy1:energy2>>%s_energy_(32767,-0.5,32767.5,32767,-0.5,32767.5)",tname));
		tree->Draw(Form("deltaTcc>>%s_detltaTcc(65535,-32767.5,32767.5)",tname));
		tree->Draw(Form("deltaTfine>>%s_detltaTFine(65535,-32767.5,32767.5)",tname));
		tree->Draw(Form("no>>%s_no(11,-0.5,10.5)",tname));

	} else {
		// Energy
		//tree->Draw(Form("energy>>ch%i_energy",ch),Form("channel==%i&&energy>0&&energy<200",ch));
		tree->Draw(Form("fine>>%s_ch%i_fine(32,-0.5,32.5)",tname,ch),Form("channel==%i",ch));
		//TH1I *histo = (TH1I*)gROOT->FindObject(Form("fine>>%s_ch%i_fine(32,-0.5,32.5)",tname,ch));
		//histo->SetTitle(Form("Fine Counter ch%i",ch));
		//histo->GetXaxis()->SetTitle("fine counter [1/32]");

		tree->Draw(Form("energy>>%s_ch%i_energy(32767,-0.5,32767.5)",tname,ch),Form("channel==%i",ch));
		//histo = (TH1I*)gROOT->FindObject(Form("energy>>%s_ch%i_energy(32767,-0.5,32767.5)",tname,ch));
		//histo->SetTitle(Form("Energy ch%i",ch));
		//histo->GetXaxis()->SetTitle("energy [ToT]");

		tree->Draw(Form("periodCC>>%s_ch%i_periodCC(32767,-0.5,32767.5)",tname,ch),Form("channel==%i&&periodCC>=0",ch));
		//histo = (TH1I*)gROOT->FindObject(Form("periodCC>>%s_ch%i_periodCC(32767,-0.5,32767.5)",tname,ch));
		//histo->SetTitle(Form("Periode (CC) ch%i",ch));
		//histo->GetXaxis()->SetTitle("time [1.64ns]");

		tree->Draw(Form("periodFine>>%s_ch%i_periodFine(1048575,-0.5,1048575.5)",tname,ch),Form("channel==%i&&periodFine>=0",ch));
		//histo = (TH1I*)gROOT->FindObject(Form("periodFine>>%s_ch%i_periodFine(1048575,-0.5,1048575.5)",tname,ch));
		//histo->SetTitle(Form("Periode (fine) ch%i",ch));
		//histo->GetXaxis()->SetTitle("time [51ps]");
		//fileEdit->Write();
	}
	
}

Int_t eventControler::getPeriodeCC(stic_data_e * event, stic_data_e * eventPrev) {
	Int_t periodCC;
	periodCC = event->getTcc() - eventPrev->getTcc();
	if(periodCC < 0)
	{
		periodCC = periodCC + (1<<15);
	}
	return periodCC;
}

Int_t eventControler::getPeriodeFine(stic_data_e * event, stic_data_e * eventPrev) {
	Int_t periodFine;
	// only calculat periodFine if both fine counters were working properly
	if(event->getTfine() >= 0 && eventPrev->getTfine() >= 0)
	{
		periodFine = event->getTfine() - eventPrev->getTfine();
		if(periodFine < 0)
		{
			periodFine = periodFine + (1<<20);
		}
	}		
	else {
		periodFine = -10000;
	}

	return periodFine;
}

Int_t eventControler::getDeltaT(stic_data_e * eventA, stic_data_e * eventB) {
	return eventA->getTcc() - eventB->getTcc();
}

Int_t eventControler::getDeltaTfine(stic_data_e * eventA, stic_data_e * eventB) {
	if(eventA->getTfine() >= 0 && eventB->getTfine() >= 0)
	{
		return eventA->getTfine() - eventB->getTfine();
	}
	else
	{
		return -10000;
	}
}
//void eventControler::save() {
//	file->Write();
//}
