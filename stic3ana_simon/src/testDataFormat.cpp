#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1I.h"
#include "TString.h"
#include "TBranch.h"
#include "TClassTable.h"
#include "EventType.h"
#include "EventDict.h"
#include "events.h"


#include <iostream>
#include <vector>
#include <algorithm> 
#include <string> 

bool eventChannelSort (stic_data_e eventA, stic_data_e eventB) { return (eventA.channel < eventB.channel); }

int main(int argc, char *argv[]) {
	stic_data_e * event = NULL;

	int lastindex = string(argv[1]).find_last_of("."); 
	string fname = string(argv[1]).substr(0, lastindex); 


	TFile * file = new TFile((TString)fname+(TString)".root","READ");
	TTree * tree = (TTree*)file->Get("dump");
	TBranch * branch = tree->GetBranch("br");
	branch->SetAddress(&event);


	for(Int_t i=0;i<branch->GetEntries();i++) {
		if(i%1000000==0)
			std::cout << (Double_t)i/branch->GetEntries()*100 << "%" << std::endl;
		branch->GetEntry(i);
		//std::cout << event->T_fine << std::endl;


		//if(event->time-event->getTfine() != 0 && event->getTfine() != -100000) {
		if(event->time-event->getTfine() != 0) {
            std::cout << event->time << "\t" << event->getTfine() << "\t" << event->T_fine <<  "\t" << event->T_CC << "\t" << event->T_fine << "\t" << event->E_CC << "\t" << event->E_fine << std::endl;
		}
	
	}

	file->Close();
}
