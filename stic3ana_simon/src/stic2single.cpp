#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1I.h"
#include "TF1.h"
#include "TH2I.h"
#include "TString.h"
#include "TBranch.h"
#include "TClassTable.h"
#include "EventType.h"
#include "EventDict.h"
#include "events.h"
 

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm> 
#include <string> 

int main(int argc, char *argv[]) {


    	std::cout << "Start stic2single" << std::endl;
    	std::map<unsigned int, TH1I *> hDeltaT;
    	std::map<unsigned int, TH1I *> hDeltaTNew;
    	//TH2I * hTest = new TH2I("hTest","hTest",5000,5000,15000,100,0,500);
	stic_data_e * event = NULL;
	stic_data_e * prev_event = new stic_data_e();

	int lastindex = string(argv[1]).find_last_of("."); 
	string fname = string(argv[1]).substr(0, lastindex); 
	std::cout << "File: " << fname << std::endl;


    //Int_t mainChannel = 30;
    //if(argc >=2 )
    //    mainChannel = atoi(argv[2]);

    	std::cout << "open file: " << (TString)fname+(TString)".root" << std::endl;
	TFile * file = new TFile((TString)fname+(TString)".root","READ");
	std::cout << "opened file" << std::endl;
	TTree * tree = (TTree*)file->Get("dump");
	TBranch * branch = tree->GetBranch("br");
	branch->SetAddress(&event);

    	TFile * fileSingles = new TFile((TString)fname+(TString)"_s.root","RECREATE");
    	// dummies to get to the exact same structure as TOFPET
    	Float_t step1, step2, tqT, tqE = 0;
    	UShort_t tac = 0;
    	Double_t tacIdleTimel, channelIdleTime = 0;
    // actualy used variables
    Long64_t time;
    UShort_t channel;
    Float_t tot;


    TH1I * hDeltaTChip = new TH1I("hDeltaTChip","hDelatTChip", 32768, 0, 1048576);
    TH1I * hDeltaTChipAny = new TH1I("hDeltaTChipAny","hDelatTChipAny", 32768, 0, 1048576);

    TTree * treeSingles = new TTree("lmData","lmData");
    treeSingles->Branch("step1",&step1,"step1/F");
    treeSingles->Branch("step2",&step2,"step2/F");
    treeSingles->Branch("time",&time,"time/L");
    treeSingles->Branch("channel",&channel,"channel/s");
    treeSingles->Branch("tot",&tot,"tot/F");
    treeSingles->Branch("tac",&tac,"tac/s");
    treeSingles->Branch("channelIdleTime",&channelIdleTime,"channelIdleTime/D");
    treeSingles->Branch("tacIdleTimel",&tacIdleTimel,"tacIdleTimel/D");
    treeSingles->Branch("tqT",&tqT,"tqT/F");
    treeSingles->Branch("tqE",&tqE,"tqE/F");

    Long64_t overflow = 0;
    short last_frame = 0;

    std::map<unsigned int, Long64_t> lastTime;
    unsigned int lastTimeStampAnyChannel;
    std::map<unsigned int, Int_t> lastTimeStamp;
    std::map<unsigned int, Int_t> lastFrame;
    
    std::cout << "start loop" << std::endl;
    int dublicated = 0;
	for(Int_t i=0;i<branch->GetEntries();i++) {
        	if(i%100000==0)
		//if(i%10==0)
            	 	std::cout << (Double_t)i/branch->GetEntries()*100 << "%" << std::endl;
        	*prev_event = *event;
        	branch->GetEntry(i);

        	channel = event->channel;
		//		if(channel > 32) channel += 64; // hack to make 2 matrices appear on 2 chips
		if(channel > 32) channel += 128; // hack to make 2 matrices appear on 2 chips
        	tot     = event->getEnergy();
		if(tot < 0) tot += 32768; // wrap around for case where energy timing counter is less than timing (as it exceeded 15-bits)
        	// Check for double events
        	if(prev_event->channel == event->channel && prev_event->getEnergy() == event->getEnergy() && prev_event->getTime() ==  event->getTime()){
            		dublicated++;
            		if(0) {
                		std::cout << "doublicated event found: " << i << std::endl;
                		event->printHeader();
                		std::cout << std::endl;
                		prev_event->print();
                		std::cout << std::endl;
                		event->print();
               			 std::cout << std::endl << std::endl;
            		}
        	}
        	else {
            
            
            		if(event->frame_number < last_frame) {
                		overflow++;
			}
			if(event->frame_number < lastFrame[event->channel]) {
			// every (1<<25) * 50ps, calculate absolute time, to ensure global alignment
				time    =  ((overflow << 25) + (((event->frame_number >> 3) & 0x1f)  << 20) + (event->time)) * 50;
				//std::cout << std::endl << std::endl;
			} else {
			// Within (1<<25) * 50ps, calculate times based on delta Frames and last event!
            			Int_t dFrame = event->frame_number - lastFrame[event->channel];
				if(dFrame < 0) {
					std::cout << "dFrame < 0" << std::endl;
				}
				time = ((lastTime[event->channel]/50) >> 20) + (dFrame >> 3);
				time = ((time << 20) + (event->time)) * 50;
				//std::cout << "(" << dFrame << " " << (1<<3) << " " << event->time << " " << lastTimeStamp[event->channel] << ") ";
				if((dFrame) < (1<<3) && (event->time < lastTimeStamp[event->channel])) {
					//std::cout << "cor" << dFrame << " ";
					time = time + (1<<20) * 50;
					//std::cout << "dTimeStamp < 0 && dFrame = 0" << std::endl;
				}
			}
			//time    =  ((overflow << 25) + (((event->frame_number >> 3) & 0x1f)  << 20) + (event->time)) * 50;


			/*if(time < lastTime[event->channel]) {
				std::cout << "time < lastTime:\t" << time << " (" << lastTime[event->channel] << ")\t" << event->frame_number << " (" << lastFrame[event->channel] << ")\t";
				std::cout << event->time << " (" << lastTimeStamp[event->channel] << ")\t" << std::endl;
			}*/
            		last_frame = event->frame_number;
            		// In Single the summed up time from run start is used
            		// thats important since a global sort is applied in Chiaras Analysis
            		//if(event->channel == mainChannel && ((event->getTime() + (overflow << 20)) * 50) - time < (500 * 50)) {
            		//        //cout << time << "\t" << (event->getTime() + (overflow << 20)) << "\t" << ((event->getTime() + (overflow << 20)) - time) << "\t";
            		//        overflow++;
            		//        //cout << ((event->getTime() + (overflow << 20)) - time) << "\t" << endl;
            		//}
            
            
	    		//std::cout << event->time << "\t" << event->frame_number << "\t" << overflow << std::endl;
			///if(time < lastTime[event->channel]){
			//	time = time + (1<<20) * 50;
			//}
			//time = (((event->frame_number >> 3) << 20) + (event->time)) * 50;	    

	    		// Fill Histos
			//if(event->channel == 15) {
	    		if(hDeltaT.count(event->channel) == 0)
				// hDeltaT[event->channel] = new TH1I(("hDeltaT-ch"+std::to_string(event->channel)).c_str(), ("deltaT ch "+std::to_string(event->channel)).c_str(), 2000, -1e6, 1e6);
				hDeltaT[event->channel] = new TH1I(("hDeltaT-ch"+std::to_string(event->channel)).c_str(), ("deltaT ch "+std::to_string(event->channel)).c_str(), 32768, 0, 1048576);
				hDeltaT[event->channel]->GetXaxis()->SetTitle("deltaT [50ps]");
			if(int(event->time) >= lastTimeStamp[event->channel]) {
	    			hDeltaT[event->channel]->Fill(int(event->time) - int(lastTimeStamp[event->channel]));
			
				hDeltaTChip->Fill(int(event->time) - int(lastTimeStamp[event->channel]));
			} else {
				hDeltaT[event->channel]->Fill(int(event->time) - int(lastTimeStamp[event->channel])+(1<<20));
				hDeltaTChip->Fill(int(event->time) - int(lastTimeStamp[event->channel])+(1<<20));
			}
			if(int(event->time) >= int(lastTimeStampAnyChannel)){
				hDeltaTChipAny->Fill(int(event->time)-int(lastTimeStampAnyChannel));
			} else {
				hDeltaTChipAny->Fill(int(event->time)-int(lastTimeStampAnyChannel)+(1<<20));
			}
	    		hDeltaTNew[event->channel]->Fill(time - lastTime[event->channel]);
			//}
					
			//if(event->channel == 15)
			//	std::cout << event->time << "\t" << event->frame_number << "\t" << (event->frame_number >> 3) << "\t" << overflow << "\t" << time  << "\t" << event->energy << std::endl;
	    		
	    		//if(event->channel == 15 && (int(event->time) - int(lastTimeStamp[event->channel])) > 7000 && int(event->time) - int((lastTimeStamp[event->channel])) < 10000 ) {
			//	std::cout << "!!!!! Event TimeStamp peak" << std::endl;			
			//}

	    		//if(event->channel == 15 && (time - lastTime[event->channel]) > 200000 && (time - lastTime[event->channel]) < 600000 ) {
			//	std::cout << "!!!!! Event Time peak" << std::endl;			
			//}
			lastTime[event->channel] = time;	
	    		lastTimeStamp[event->channel] = event->time;
			lastFrame[event->channel] = event->frame_number;
			lastTimeStampAnyChannel = event->time;
            		treeSingles->Fill();
        	}
	}

	std::cout << "Total: " << branch->GetEntries() << " (" << ((float)dublicated/branch->GetEntries())*100 << "% doublicated)" << std::endl;
	std::cout << "New: " << treeSingles->GetEntries() << std::endl;

	for(std::map<unsigned int, TH1I *>::iterator it = hDeltaT.begin(); it != hDeltaT.end(); it++) {	
		it->second->Write();
		delete it->second;
	}

    TF1 * r = new TF1("r", "expo",3e5,1.3e6);
    std::ofstream ofs;
    ofs.open ("rates-fit.data", std::ofstream::out | std::ofstream::app);
	for(std::map<unsigned int, TH1I *>::iterator it = hDeltaTNew.begin(); it != hDeltaTNew.end(); it++) {	
	    ofs << fname << "\t" << (it->first) << "\t";
	    if(it->second->GetEntries()>10) {
		    it->second->Fit(r, "RQ");
		    ofs << -(r->GetParameter("Slope")*1e12) << "\t" << (r->GetParError(r->GetParNumber("Slope"))*1e12) << "\t" << (r->GetChisquare()/r->GetNDF()) << std::endl;
		} else {
		    ofs << -1 << "\t" << -1 << "\t" << -1 << std::endl;	
		}
		it->second->Write();
		delete it->second;
	}
	ofs.close();
	
	//hTest->Write();

    	fileSingles->Write();
    	fileSingles->Close();
	file->Close();
}
