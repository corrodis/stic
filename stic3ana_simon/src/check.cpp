#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1I.h"
#include "TStyle.h"
#include "TError.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TLine.h"
#include "TPaveText.h"
#include "TChain.h"
#include "TSpectrum.h"
#include "TString.h"
#include "TBranch.h"
#include "TClassTable.h"
#include "EventType.h"
#include "EventDict.h"
#include "events.h"


#include <iostream>
#include <vector>
#include <algorithm> 
#include <string> 

#define CHANNELS 64

int main(int argc, char *argv[]) {
	gErrorIgnoreLevel=1;
	if(argc<2) {
        	std::cout << "useage: check FILE [ch1 ch2]" << std::endl;
		return -1;
} 
	const char* fname = argv[1];
    Float_t EcutSigmas = 1;
    Int_t thispair = -1;
    if(argc >= 3) {
	if(argc == 3) {
		std::cout << "ch1 and ch2 required" << std::endl;
		return -1;
	}
        if(argv[2] >= argv[3]){
		std::cout << "need: ch1 < ch2" << std::endl;
        }
	thispair = (atoi(argv[2]) << 6) + atoi(argv[3]);
    }

	// not used any longer, as long as TSpectrum works fine
	Double_t p1 = 0;
	Double_t p2 = 0;
	Double_t xl,xu, yl, yu;

   	TChain * chain = new TChain("Coin");

	chain->Add(Form("%s_coin.root",fname));

	//if(fname=="setup10_ch12_ch13") {
		//chain->Add(Form("%s_coin.root",fname));
		//chain->Add(Form("%s2_coin.root",fname));
		//chain->Add(Form("%s3_coin.root",fname));
		//chain->Add(Form("%s4_coin.root",fname));
	//}
	//else {
   	//	chain->Add(Form("%s.root",fname));
	//}
   	//chain->Add("setup10_ch12_ch13_sdaq2.root");
   	//chain->Add("setup10_ch12_ch13_sdaq3.root");
	//chain->Add("setup10_ch12_ch13_sdaq4.root");
	Int_t curpair;
	TFile * file = new TFile(Form("%s_ana.root",fname),"RECREATE");
	
	TCanvas * c = new TCanvas("c", "c", 1000, 800);		
	TCanvas * c2 = new TCanvas("c2", "c2", 1000, 800);
	TCanvas * c4 = new TCanvas("c4", "c4", 800, 600);
	
	// Define Histograms
	TH1F * hE1 = new TH1F("hE1","Energy ch1",801,-0.5,800.5);
	TH1F * hE2 = new TH1F("hE2","Energy ch2",801,-0.5,800.5);
	TH2F * hE = new TH2F("hE","Energy Correlation",801,-0.5,800.5,801,-0.5,800.5);
	TH2F * hEs = new TH2F("hEs","Energy Correlation: Cut on dT",801,-0.5,800.5,801,-0.5,800.5);
	TH1F * hdTall = new TH1F("hdTall","Delta Time All",51,-2525,2525);
	TH1F * hdT1 = new TH1F("hdT1","Delta Time: Energy Cut only ch1",51,-2525,2525);
	TH1F * hdT2 = new TH1F("hdT2","Delta Time: Energy Cut only ch2",51,-2525,2525);
	TH1F * hdTfinal = new TH1F("hdTfinal","Delta Time: Energy Cut on both Channels",51,-2525,2525);
	c->SaveAs(Form("%s.pdf[",fname));
	//c->Delete();


    	Int_t energy1, energy2, deltaT, pair;
	TBranch * bpair = chain->GetBranch("pair");
	TBranch * benergy1 = chain->GetBranch("energy1");
	TBranch * benergy2 = chain->GetBranch("energy2");
    	TBranch * bdeltaT = chain->GetBranch("deltaT");
	benergy1->SetAddress(&energy1);
	benergy2->SetAddress(&energy2);
    	bdeltaT->SetAddress(&deltaT);
	bpair->SetAddress(&pair);
	
	for(Int_t ch1 = 0; ch1 < CHANNELS/2; ++ch1) {
		for(Int_t ch2 = CHANNELS/2; ch2 < CHANNELS; ++ch2) {
			curpair = (ch1 << 6) + ch2;
	//TFile * file = new TFile(Form("%s_%i-%i_ana.root",(curpair >> 5), (curpair & 0x1f),fname),"RECREATE");
	//TChain chain("T");
	//TTree * tree = (TTree*)file->Get("Coin;14");


	//bpair->Draw(">>eventlist",("pair=="+TString(curpair)).Data(),"goff"); 
   	//Int-t N = eventlist->GetN();	
	//TH1 * eventlist = (TH1*)gDirectory->Get("eventlist");
	//if(eventlist->GetEntries()==0){
	//std::cout << "pair: " << curpair << " (ch1: " << (curpair >> 5) << ", ch2: " << (curpair & 0x1f) << ") pairs: " << chain->GetEntries(("pair=="+to_string(curpair)).c_str()) << " " << std::endl; 
	if(chain->GetEntries(("pair=="+to_string(curpair)).c_str())==0) {
		//std::cout << "pair: " << curpair << " is not present." << std::endl;
		continue;
	}
	if(thispair != -1 && curpair != thispair)
		continue;

	hE1->SetTitle(("Energy ch1 ("+to_string(ch1)+", "+to_string(ch2)+")").c_str());
        hE2->SetTitle(("Energy ch2 ("+to_string(ch1)+", "+to_string(ch2)+")").c_str());
        hE->SetTitle(("Energy Correlation ("+to_string(ch1)+", "+to_string(ch2)+")").c_str());
        hEs->SetTitle(("Energy Correlation: Cut on dT ("+to_string(ch1)+", "+to_string(ch2)+")").c_str());
        hdTall->SetTitle(("Delta Time All ("+to_string(ch1)+", "+to_string(ch2)+")").c_str());
        hdT1->SetTitle(("Delta Time: Energy Cut only ch1 ("+to_string(ch1)+", "+to_string(ch2)+")").c_str());
        hdT2->SetTitle(("Delta Time: Energy Cut only ch2 ("+to_string(ch1)+", "+to_string(ch2)+")").c_str());
        hdTfinal->SetTitle(("Delta Time: Energy Cut on both Channels ("+to_string(ch1)+", "+to_string(ch2)+")").c_str());


	// loop through events
	//std::cout << "events: " << benergy1->GetEntries() << std::endl;
	for(Int_t k=0;k<benergy1->GetEntries();k++) {
		bpair->GetEntry(k);
		//std::cout << "pair: " << pair << std::endl;
		if(pair != curpair)
			continue;
		//std::cout << "ok" << std::endl;
		benergy1->GetEntry(k);
		benergy2->GetEntry(k);
		bdeltaT->GetEntry(k);
		hE1->Fill(energy1);
		hE2->Fill(energy2);
		// std::cout << k << "\t" << energy1 << std::endl; 
		hE->Fill(energy1,energy2);
        //if(deltaTfine>-100&&deltaTfine<100)
        //	hEs->Fill(energy1,energy2);
	}


	//TCanvas * c = new TCanvas("c", "c"+TString(curpair), 1000, 800);	
	c->Divide(2,2);

	c->cd(1);
	//chain->Draw("energy1>>hE1(801,-0.5,800.5)");
	hE1->GetXaxis()->SetTitle("ToT [1.6ns]");
	hE1->Draw();	
	c->cd(2);
	//chain->Draw("energy2>>hE2(801,-0.5,800.5)");
	hE2->GetXaxis()->SetTitle("ToT [1.6ns]");
	hE2->Draw();

	TSpectrum *s = new TSpectrum();
	s->Search(hE1,20,"");
	Float_t *xpeaks = s->GetPositionX();
	for(Int_t l=0;l<3;l++) {
		p1 = xpeaks[l];
		if(p1>150)
			break;
	}


	TSpectrum *s2 = new TSpectrum();
	s2->Search(hE2,20,"");
	Float_t *xpeaks2 = s2->GetPositionX();
	for(Int_t l=0;l<3;l++) {
		p2 = xpeaks2[l];
		if(p2>150)
			break;
	}

	c->cd(3);
	//chain->Draw("energy1:energy2>>hE(801,-0.5,800.5,801,-0.5,800.5)","","COLZ");
	hE->GetXaxis()->SetTitle("ToT [1.6ns]");
	hE->GetYaxis()->SetTitle("ToT [1.6ns]");
	hE->Draw("COLZ");	
	//tree->Draw("deltaTfine>>hTcc2(100001,-50000.5,50000.5)");
	c->cd(4);
	hEs->GetXaxis()->SetTitle("ToT [1.6ns]");
	hEs->GetYaxis()->SetTitle("ToT [1.6ns]");
	hEs->Draw("COLZ");
	//chain->Draw("energy1:energy2>>hEs(801,-0.5,800.5,801,-0.5,800.5)","deltaTfine>-100&&deltaTfine<100","COLZ");
	//chain->Draw("energy1:energy2>>hEs(101,-0.5,500.5,501,-0.5,500.5)","deltaTfine>-45&&deltaTfine<-20","COLZ");
		//tree->Draw("deltaTfine>>hTcc(101,-50.5,50.5)","deltaTcc==0");


	c->cd(1);
	TF1 *photo1 = new TF1("photo1","gaus", 0, 1000);
	photo1->SetParameter("Mean",p1);
	photo1->SetRange((p1-p1*0.2),(p1+p1*0.2));
	hE1->Fit("photo1","qR");
	photo1->SetRange((p1-p1*0.07),(p1+p1*0.07));
	hE1->Fit("photo1","qR");
	c->cd(2);
	TF1 *photo2 = new TF1("photo2","gaus", 0, 1000);
	photo2->SetParameter("Mean",p2);
	photo2->SetRange((p2-p2*0.1),(p2+p2*0.1));
	hE2->Fit("photo2","qR");
	photo2->SetRange((p2-p2*0.07),(p2+p2*0.07));
	hE2->Fit("photo2","qR");
	
    xl = photo1->GetParameter("Mean")-photo1->GetParameter("Sigma")*EcutSigmas;
    xu = photo1->GetParameter("Mean")+photo1->GetParameter("Sigma")*EcutSigmas;
    yl = photo2->GetParameter("Mean")-photo2->GetParameter("Sigma")*EcutSigmas;
    yu = photo2->GetParameter("Mean")+photo2->GetParameter("Sigma")*EcutSigmas;

	c->cd(3);
	TLine * linex1 = new TLine(xl,0,xl,800);
	//TLine * linex1 = new TLine(100,0,100,1000);
	TLine * linex2 = new TLine(xu,0,xu,800);
	TLine * liney1 = new TLine(0,yl,800,yl);
	TLine * liney2 = new TLine(0,yu,800,yu);

  	linex1->SetLineWidth(1);
  	linex1->SetLineColor(2);
  	linex2->SetLineWidth(1);
  	linex2->SetLineColor(2);
  	liney1->SetLineWidth(1);
  	liney1->SetLineColor(2);
  	liney2->SetLineWidth(1);
  	liney2->SetLineColor(2);
  	linex1->Draw();
	linex2->Draw();
	liney1->Draw();
	liney2->Draw();

	c->cd(4);
  	linex1->Draw();
	linex2->Draw();
	liney1->Draw();
	liney2->Draw();

	// fill detlaT Histos
	for(Int_t k=0;k<benergy1->GetEntries();k++) {
		bpair->GetEntry(k);
		if(pair != curpair)
			continue;
		benergy1->GetEntry(k);
		benergy2->GetEntry(k);
		bdeltaT->GetEntry(k);
        hdTall->Fill(deltaT*50);
		if(energy1>xl&&energy1<xu)
            hdT1->Fill(deltaT*50);
		if(energy2>yl&&energy2<yu)
            hdT2->Fill(deltaT*50);
		if(energy1>xl&&energy1<xu &&energy2>yl&&energy2<yu)
            hdTfinal->Fill(deltaT*50);
	}

	
    TF1 *deltaT1 = new TF1("deltaT1","gaus", -150, 150);
    TF1 *deltaTall = new TF1("deltaTall","gaus", -150, 150);
	gPad->SetGrid();
	c2->SetGrid();
	gStyle->SetOptFit(1111);	
	c2->Divide(2,2);
	c2->cd(1);
	hdTall->Draw();	
	//chain->Draw("deltaTfine>>hdTall(401,-200.5,200.5)");
    hdTall->GetXaxis()->SetTitle("fine [ps]");
    hdTall->Fit("deltaTall","q","",-2000,2000);
	c2->cd(2);	
	hdT1->Draw();
	//chain->Draw("deltaTfine>>hdT1(401,-200.5,200.5)",Form("energy1>%f&&energy1<%f",xl,xu));
    hdT1->GetXaxis()->SetTitle("fine [ps]");
    hdT1->Fit("deltaT1","q","",-2000,2000);
	c2->cd(3);
	hdT2->Draw();
	//chain->Draw("deltaTfine>>hdT2(401,-200.5,200.5)",Form("energy2>%f&&energy2<%f",yl,yu));	
	//chain->Draw("deltaTfine>>hdTfinal2(10001,-5000.5,5000.5)",Form("energy1>%f&&energy1<%f&&energy2>%f&&energy2<%f",xl,xu,yl,yu));
    hdT2->GetXaxis()->SetTitle("fine [ps]");
    hdT2->Fit("deltaT1","q","",-2000,2000);
	c2->cd(4);
	hdTfinal->Draw();	
	//chain->Draw("deltaTfine>>hdTfinal(401,-200.5,200.5)",Form("energy1>%f&&energy1<%f&&energy2>%f&&energy2<%f",xl,xu,yl,yu));	
    hdTfinal->GetXaxis()->SetTitle("fine [ps]");
	//TH1D * hdTfinalClone = (TH1D*)(hdTfinal->Clone());
    hdTfinal->Fit("deltaT1","q","",-2000,2000);

	if(0) {
		TCanvas * c3 = new TCanvas("c3", "c3", 800, 600);
		c3->SetGrid();
		gStyle->SetOptFit(0);
		TF1 *deltaT2 = new TF1("deltaT2","gaus", -40, -24);
		hdTfinal->SetTitle("only photo peak");	
		hdTfinal->Draw();
		deltaT2->SetLineColor(8);
		hdTfinal->Fit("deltaT2","qR+","sames");

		// Add FWHM
		TPaveText *pt = new TPaveText(30,700,180,900);
		pt->SetFillColor(kWhite);
        pt->AddText(Form("FWHM (small) = %.0f ps",deltaT2->GetParameter("Sigma")*2.35*50.2));
        pt->AddText(Form("FWHM (large) = %.0f ps",deltaT1->GetParameter("Sigma")*2.35*50.2));
		pt->Draw();
	}
	
	// save canvas to pdf
	gErrorIgnoreLevel = kWarning;
	c->SaveAs(Form("%s.pdf",fname));
	c2->SaveAs(Form("%s.pdf",fname));
	
	// Save histograms
	if(1){
		hE1->Write();
		hE1->Draw();
		c4->SaveAs(Form("plots/%s_%i-%i-E1.pdf",fname,ch1,ch2));
        c4->SaveAs(Form("plots/%s_%i-%i-E1.root",fname,ch1,ch2));
		hE2->Write();		
		hE2->Draw();
		c4->SaveAs(Form("plots/%s_%i-%i-E2.pdf",fname,ch1,ch2));
        c4->SaveAs(Form("plots/%s_%i-%i-E2.root",fname,ch1,ch2));
		hE->Write();
		hE->Draw();
		c4->SaveAs(Form("plots/%s_%i-%i-E1E2.pdf",fname,ch1,ch2));
        c4->SaveAs(Form("plots/%s_%i-%i-E1E2.root",fname,ch1,ch2));
		hE2->Write();
		hEs->Draw();
		c4->SaveAs(Form("plots/%s_%i-%i-E1E2s.pdf",fname,ch1,ch2));
        c4->SaveAs(Form("plots/%s_%i-%i-E1E2s.root",fname,ch1,ch2));
		hdTall->Write();
		hdTall->Draw();
		c4->SaveAs(Form("plots/%s_%i-%i-Tall.pdf",fname,ch1,ch2));
        c4->SaveAs(Form("plots/%s_%i-%i-Tall.root",fname,ch1,ch2));
		hdT1->Write();
		hdT1->Draw();
		c4->SaveAs(Form("plots/%s_%i-%i-T1.pdf",fname,ch1,ch2));
        c4->SaveAs(Form("plots/%s_%i-%i-T1.root",fname,ch1,ch2));
		hdT2->Write();
		hdT2->Draw();
		c4->SaveAs(Form("plots/%s_%i-%i--T2.pdf",fname,ch1,ch2));
        c4->SaveAs(Form("plots/%s_%i-%i-T2.root",fname,ch1,ch2));
		hdTfinal->Write();
		hdTfinal->Draw();
		c4->SaveAs(Form("plots/%s_%i-%i-T.pdf",fname,ch1,ch2));
        c4->SaveAs(Form("plots/%s_%i-%i-T.root",fname,ch1,ch2));
	}

	// Output interesting quantities
	// 8 DACS, bias voltage
	// coin fit, error
	// E1 pos and error, E2 pos and error
	// number of entries, under- and overflow

    std::string sname = string(fname+5);
    //std::replace( sname.begin(), sname.end(), '-', '\t');
    //std::cout << sname << "\t";
    std::cout << ch1 << "\t" << ch2 << "\t";	
    std::cout << deltaT1->GetParameter("Mean") << "\t" << deltaT1->GetParError(deltaT1->GetParNumber("Mean")) << "\t";
    std::cout << deltaT1->GetParameter("Sigma") << "\t" << deltaT1->GetParError(deltaT1->GetParNumber("Sigma")) << "\t";
    std::cout << deltaT1->GetChisquare() << "\t" << deltaT1->GetNDF() << "\t";
    std::cout << hdTall->GetEntries() << "\t" << hdTfinal->GetEntries() << "\t";//  << hdTfinal->GetBinContent(0) << "\t" << hdTfinal->GetBinContent(hdTfinal->GetNbinsX()+1) << "\t";
    //std::cout << hdTfinal->GetEntries() - hdTfinal->GetBinContent(0) - hdTfinal->GetBinContent(hdTfinal->GetNbinsX()+1) << "\t";
    //std::cout << hdTfinal->Integral(hdTfinal->FindBin(deltaT1->GetParameter("Mean") - 3 * deltaT1->GetParameter("Sigma")), hdTfinal->FindBin(deltaT1->GetParameter("Mean") + 3 * deltaT1->GetParameter("Sigma")));
    //<< "\t" << hdTfinal->GetBinContent(0) << "\t" << hdTfinal->GetBinContent(hdTfinal->GetNbinsX()+1) << "\t";

    std::cout << std::endl;

    xl = photo1->GetParameter("Mean")-photo1->GetParameter("Sigma")*EcutSigmas;
    xu = photo1->GetParameter("Mean")+photo1->GetParameter("Sigma")*EcutSigmas;
    yl = photo2->GetParameter("Mean")-photo2->GetParameter("Sigma")*EcutSigmas;
    yu = photo2->GetParameter("Mean")+photo2->GetParameter("Sigma")*EcutSigmas;

    // std::cout << xl << " " << xu << " " << yl << " " << yu << std::endl;

    // std::cout << deltaTall->GetParameter("Sigma") << "\t" << deltaTall->GetParError(deltaT1->GetParNumber("Sigma")) << std::endl;
    // std::cout << hdTall->Integral(hdTall->FindBin(deltaTall->GetParameter("Mean") - 3 * deltaTall->GetParameter("Sigma")), hdTall->FindBin(deltaTall->GetParameter("Mean") + 3 * deltaTall->GetParameter("Sigma"))) << std::endl;

	//file->Write();
	//file->Close();
	c->Clear();
	c2->Clear();


	hE1->Reset();
	hE2->Reset();
	hE->Reset();
	hEs->Reset();
	hdTall->Reset();
	hdT1->Reset();
	hdT2->Reset();
	hdTfinal->Reset();
	//std::cout << "test (0): " << hdTfinal->GetEntries() << std::endl;


	}
	}

	//TCanvas * c2 = new TCanvas("c2", "c2", 1000, 800); 	
	c2->SaveAs(Form("%s.pdf]",fname));
}
