#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1I.h"
#include "TString.h"
#include "TBranch.h"
#include "TClassTable.h"
#include "EventType.h"
#include "EventDict.h"
#include "events.h"


#include <iostream>
#include <vector>
#include <algorithm> 
#include <string> 


int main(int argc, char *argv[]) {


    std::cout << "Start stic2single" << std::endl;
	stic_data_e * event = NULL;

	int lastindex = string(argv[1]).find_last_of("."); 
	string fname = string(argv[1]).substr(0, lastindex); 
	std::cout << "File: " << fname << std::endl;


    Int_t mainChannel = 30;
    //if(argc >=2 )
    //    mainChannel = atoi(argv[2]);

    std::cout << "open file: " << (TString)fname+(TString)".root" << std::endl;
	TFile * file = new TFile((TString)fname+(TString)".root","READ");
	std::cout << "opened file" << std::endl;
	TTree * tree = (TTree*)file->Get("dump");
	TBranch * branch = tree->GetBranch("br");
	branch->SetAddress(&event);

    TFile * fileSingles = new TFile((TString)fname+(TString)"_s.root","RECREATE");
    // dummies to get to the exact same structure as TOFPET
    Float_t step1, step2, tqT, tqE = 0;
    UShort_t tac = 0;
    Double_t tacIdleTimel, channelIdleTime = 0;
    // actualy used variables
    Long64_t time;
    UShort_t channel;
    Float_t tot;



    TTree * treeSingles = new TTree("dump","dump");
    //treeSingles->Branch("step1",&step1,"step1/F");
    treeSingles->Branch("br", "stic3_data_t", &event);
    //treeSingles->Branch("step2",&step2,"step2/F");
    //treeSingles->Branch("time",&time,"time/L");
    //treeSingles->Branch("channel",&channel,"channel/s");
    //treeSingles->Branch("tot",&tot,"tot/F");
    //treeSingles->Branch("tac",&tac,"tac/s");
    //treeSingles->Branch("channelIdleTime",&channelIdleTime,"channelIdleTime/D");
    //treeSingles->Branch("tacIdleTimel",&tacIdleTimel,"tacIdleTimel/D");
    //treeSingles->Branch("tqT",&tqT,"tqT/F");
    //treeSingles->Branch("tqE",&tqE,"tqE/F");

    Long64_t overflow = 0;
    short last_frame = 0;


    std::cout << "start loop" << std::endl;
	for(Int_t i=0;i<branch->GetEntries();i++) {
        if(i%100000==0)
            std::cout << (Double_t)i/branch->GetEntries()*100 << "%" << std::endl;
        branch->GetEntry(i);

        //channel = event->channel;
        //tot     = event->getEnergy();
        if(event->frame_number < last_frame)
            overflow++;
        last_frame = event->frame_number;
        // In Single the summed up time from run start is used
        // thats important since a global sort is applied in Chiaras Analysis
        //if(event->channel == mainChannel && ((event->getTime() + (overflow << 20)) * 50) - time < (500 * 50)) {
        //        //cout << time << "\t" << (event->getTime() + (overflow << 20)) << "\t" << ((event->getTime() + (overflow << 20)) - time) << "\t";
        //        overflow++;
        //        //cout << ((event->getTime() + (overflow << 20)) - time) << "\t" << endl;
        //}
        
        
        
        time    =  ((overflow << 25) + ((event->frame_number & 0xf8)  << 20) + (event->time));// * 50;
        event->time = time;
        treeSingles->Fill();
	}

	std::cout << "Total: " << branch->GetEntries() << std::endl;
	
    fileSingles->Write();
    fileSingles->Close();
	file->Close();
}
