#include "ethzSticAna.h"

#include "TTree.h"
#include "TSpectrum.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraphErrors.h"
#include "TStyle.h"

#include <iomanip>
#include <math.h>
#include <algorithm>
#include <string>

#include "TRandom3.h"

//#include "TClassTable.h"
//#include "EventType.h"
//#include "EventDict.h"



using std::to_string;

ethzSticAna::ethzSticAna(const char *fname) {
    _fname = fname;
    _ifile = new TFile(_fname, "READ");
    _itree = (TTree*)_ifile->Get("dump"); // STiC
    if(_itree == 0) {
        _itree = (TTree*)_ifile->Get("lmData");
        _isTOFPET = true;
        CHS = CHSTOFPET; // Adjust number of used channels for TOFPET
        //energyMin = energyMinTOFPET;
        energyMax = energyMaxTOFPET;
        if(_itree == 0)
            std::cerr << "ethzSticAna " << _fname << " has no branch 'dump'(STiC) or 'lmData' (TOFPET)." << std::endl;
        else
            std::cout << "TOFPET file detected" << std::endl;
        // adjust coincWindow to ctr histogram plots
    }
    coincWindow = fineBinWidth/1e3 * (ctrBinNumber/2);
    std::cout << "Use coincidence window of " << coincWindow << "ns";
    std::cout << ", adjusted to " << ctrBinNumber << " bins ("<< fineBinWidth << "ps) of ctr histos." << std::endl;

    hMultiplicity = new TH1F("hMultiplicity", ("Multiplicity per channel pair ("+std::to_string(coincWindow)+"ns window)").c_str(), 8, -0.5, 7.5);
    hMultiplicity->GetXaxis()->SetTitle("channel pair multiplicity");

    hGlobalMultiplicity = new TH1F("hGlobalMultiplicity", ("Multiplicity per coincidence window ("+std::to_string(coincWindow)+"ns)").c_str(), 8, -0.5, 7.5);
    hGlobalMultiplicity->GetXaxis()->SetTitle("multiplicity");

}

void ethzSticAna::checkDNLTable() {

     std::map<unsigned int, TH1F *> hFine;
     std::map<unsigned int, TH1F *> hFineDNL;
     std::map<unsigned int, TH1F *> hRealTime;
     std::map<unsigned int, TH1F *> hRealTimeErr;

     TH1I * hChannels = getChannelsHisto();
     for(unsigned int i = 1; i <= CHS; ++i) {
         if(hChannels->GetBinContent(i) > 0) {
             unsigned int ch = hChannels->GetXaxis()->GetBinCenter(i);
             hFine[ch] = new TH1F(("hFine"+std::to_string(ch)).c_str(),
                                              (std::string("fine counter ch ")+std::to_string(ch)).c_str(),
                                              32,0,32);
             hFine[ch]->GetXaxis()->SetTitle("fine counter");
             hFineDNL[ch] = new TH1F(("hFineDNL"+std::to_string(ch)).c_str(),
                                              ("corrected fine counter ch "+std::to_string(ch)).c_str(),
                                              32,0,32);
             hFineDNL[ch]->GetXaxis()->SetTitle("fine counter");

             hRealTime[ch] = new TH1F(("hRealTime"+std::to_string(ch)).c_str(),
                                       ("fine counter real time ch "+std::to_string(ch)).c_str(),
                                       32,0,32);
             hRealTime[ch]->GetXaxis()->SetTitle("fine counter");
             hRealTimeErr[ch] = new TH1F(("hRealTimeErr"+std::to_string(ch)).c_str(),
                                       ("fine counter real time uncertainty ch "+std::to_string(ch)).c_str(),
                                       32,0,32);
             hRealTimeErr[ch]->GetXaxis()->SetTitle("fine counter");

             for(unsigned int j = 0; j < 32; ++j) {
                 hRealTime[ch]->SetBinContent(j+1, _DNLtable[ch][j]);
                 hRealTimeErr[ch]->SetBinContent(j+1, _uncertaintyTable[ch][j]);
             }


         }
     }


     stic_data_e * event = NULL;
     TBranch * branch = _itree->GetBranch("br");
     branch->SetAddress(&event);
     for(unsigned int i = 0; i < branch->GetEntries() ; ++i) {
       if(i%100000==0)
         std::cout << "\r" << std::fixed << std::setprecision(2) << (float)i/branch->GetEntries()*100 << "%" << std::flush;// << std::endl;
       branch->GetEntry(i);

       hFine[event->channel]->Fill(event->T_fine);
       hFineDNL[event->channel]->Fill(fmod(getRealTime(event),(32.0 * fineBinWidth))/fineBinWidth);
     }

     for(auto it = hFine.begin(); it != hFine.end(); ++it)
         (it->second)->Write();
     for(auto it = hFineDNL.begin(); it != hFineDNL.end(); ++it)
         (it->second)->Write();
     for(auto it = hRealTime.begin(); it != hRealTime.end(); ++it)
         (it->second)->Write();
     for(auto it = hRealTimeErr.begin(); it != hRealTimeErr.end(); ++it)
         (it->second)->Write();
}

void ethzSticAna::loadDNLTable() {
    if(_isTOFPET) {
        std::cout << "no DNL correction needed for TOFPET data." << std::endl;
        return;
    }
    TCanvas * c1 = new TCanvas("cDNL");
    TH1I * hChannels = getChannelsHisto();
    for(unsigned int i = 1; i <= CHS; ++i) {
        if(hChannels->GetBinContent(i) > 0) {
            double binWidthIntegral = 0;
            unsigned int ch = hChannels->GetXaxis()->GetBinCenter(i);
            std::cout << "ch " << i << "\r" << std::flush;
            //_itree->Draw("T_fine>>hFine(32,0,32)", ("channel=="+to_string(ch)).c_str());
            TH1F * hFine = (TH1F*)gDirectory->Get(("hFine"+to_string(ch)).c_str());
            double mean = hFine->Integral()/32;
            for(unsigned int j = 1; j <= 32; j++) {
                double binWidth = hFine->GetBinContent(j)/mean * fineBinWidth;
                _uncertaintyTable[ch][j-1] = binWidth/sqrt(12);
                //std::cout << "debug: fineCounter" << j << " - " << binWidthIntegral + 0.5 * binWidth << std::endl;
                _DNLtable[ch][j-1] = binWidthIntegral;// + 0.5 * binWidth;
                binWidthIntegral += binWidth;
            }
            _DNLtable[ch][32] = binWidthIntegral;
        //std::cout << "debug: binWidthIntergal: " << binWidthIntegral << std::endl;
        }
    }
    std::cout << std::endl;
}

void ethzSticAna::loadNoDNLTable() {
    if(_isTOFPET) {
        std::cout << "no DNL correction needed for TOFPET data." << std::endl;
        return;
    }
    for(unsigned int ch = 1; ch <= CHS; ++ch) {
        for(unsigned int j = 1; j <= 32; j++) {
            _DNLtable[ch][j-1] = fineBinWidth*(j);
            _uncertaintyTable[ch][j-1] = fineBinWidth/sqrt(12);
    //if(ch == 17) std::cout << " _DNLtable: " << _DNLtable[ch][j-1] << std::endl;
        }
      //std::cout << "debug: binWidthIntergal: " << binWidthIntegral << std::endl;
    }
}


void ethzSticAna::loadDNLTableAlex() {
    if(_isTOFPET) {
        std::cout << "no DNL correction needed for TOFPET data." << std::endl;
        return;
    }
    std::map<Int_t,TH1I *> h_fine;
    std::map<Int_t,TH1I *> h_fineInt;
    stic_data_e * event = NULL;

    for(Int_t ch=0; ch<CHS; ++ch) {
      h_fine[ch] = new TH1I(Form("h_fine_ch%i",ch), Form("h_fine_ch%i",ch),32,0,32);
      h_fineInt[ch] = new TH1I(Form("h_fine_int_ch%i",ch), Form("h_fine_int_ch%i",ch),32,0,32);
    }

    TBranch * branch = _itree->GetBranch("br");
    branch->SetAddress(&event);

    //TH1I * statFrames = new TH1I("statFrames","Events per Frame",21,-0.5,20.5);
    Int_t frame = 0;
    std::map<unsigned int, std::vector<stic_data_e>> events;
    for(unsigned int i = 0; i < branch->GetEntries() ; ++i) {
      if(i%100000==0)
        std::cout << "\r" << std::fixed << std::setprecision(2) << (float)i/branch->GetEntries()*100 << "%" << std::flush;// << std::endl;
      branch->GetEntry(i);
      h_fine[event->channel]->Fill(event->getTfine(),1.);
    }

    for(Int_t ch=0; ch<64; ++ch) {
      int integral = 0;
      for(Int_t i=0;i<32; ++i) {
        integral += h_fine[event->channel]->GetBinContent(i+1);
        h_fineInt[ch]->Fill(i,integral);
      }

      double correctFine = -999.;
      for(int i=0; i<32; ++i) {
        // correctFine = 32*(h_fineInt[ch]->GetBinContent(i+1))/integral;
        correctFine = 32*(h_fineInt[ch]->GetBinContent(i+1))/integral;
        _DNLtable[ch][i]=correctFine*fineBinWidth;
    _uncertaintyTable[ch][i] = (h_fine[event->channel]->GetBinContent(i+1)/(integral/32.0)) * fineBinWidth / sqrt(12);
        if(ch == 17) std::cout << " Alex _DNLtable for channel 17: " << _DNLtable[ch][i] << std::endl;
      }
    }

    //   correctFine = 0.5*32*(h_fineInt[ch]->GetBinContent(1))/integral;
    //   _DNLtable[ch][0]=correctFine*50;
    //   for(int i=1; i<32; ++i) {
    // 	correctFine = 32*(h_fineInt[ch]->GetBinContent(i+1))/integral;
    // 	// hFineCorr17->Fill(correctFine17,1.);
    // 	_DNLtable[ch][i]=0.5*(correctFine*50 - _DNLtable[ch][i-1]) + _DNLtable[ch][i-1];
    // 	if(ch == 17) std::cout << " _DNLtable: " << _DNLtable[ch][i] << std::endl;
    //   }
    // }


                // _DNLtable[ch][j-1] = binWidthIntegral + 0.5 * binWidth;
}


int ethzSticAna::loadEnergy(double energyMin) {
    TCanvas * c1 = new TCanvas("cE");
    std::cout << "Total of " << _itree->GetEntries() << " events." << std::endl;
    if(_isTOFPET) {return loadEnergyTOFPET(energyMin);}

    stic_data_e * event = NULL;
    TBranch * branch = _itree->GetBranch("br");
    branch->SetAddress(&event);

    int noChannels = initEnergyHistos();

    // easier to create all hFines
    TH1F * hFine[CHS];
    TH1I * hChannels = getChannelsHisto();
    for(unsigned int ch = 0; ch < CHS; ++ch) {
        if(hChannels->GetBinContent(ch+1) > 0) {
            hFine[ch] = new TH1F(("hFine"+to_string(ch)).c_str(), ("fine time ch "+to_string(ch)).c_str(), 32, 0, 32);
        }
    }

    Int_t n = _itree->GetEntries();
    for(unsigned int i = 1; i < n; ++i) {
        branch->GetEntry(i);
        if(i%100000 == 0)
            std::cout << "\r" << std::fixed << std::setprecision(2) << (float)i/n*100 << "%" << std::flush;
        _hEnergy[event->channel]->Fill(event->energy);
        hFine[event->channel]->Fill(event->T_fine);
    }
    std::cout << std::endl;
    setEnergyCuts(energyMin);

    return noChannels;
}

void ethzSticAna::addPair(unsigned int ch1, unsigned int ch2) {
    _coincPairs.push_back(std::make_pair(ch1, ch2));
    std::cout << "add coinc pair: " << ch1 << "/" << ch2 << std::endl;
    if(ch1 < _minPairId.first || _minPairId.first < 0)  _minPairId.first = ch1;
    if(ch2 < _minPairId.second || _minPairId.second < 0)  _minPairId.second = ch2;
}

int ethzSticAna::addAllPairs() {
    TH1I * hChannels = getChannelsHisto();
    std::vector<unsigned int> left;
    std::vector<unsigned int> right;
    for(unsigned int i = 1; i <= CHS; ++i) {
        if(hChannels->GetBinContent(i) > 0) {
            unsigned int ch = hChannels->GetXaxis()->GetBinCenter(i);
            if(ch<CHS/2)
                left.push_back(ch);
            else
                right.push_back(ch);
        }
    }
    for(auto itl = left.begin(); itl != left.end(); itl++) {
        for(auto itr = right.begin(); itr != right.end(); itr++) {
            addPair(*itl, *itr);
        }
    }
    return _coincPairs.size();
}

TH1I* ethzSticAna::getChannelsHisto() {
    if(!gDirectory->Get("hChannel")) {
        //std::cout << "debug: tree name " << _itree->GetName() << std::endl;
        //TODO: TOFPET
        _itree->Draw(("channel>>hChannel("+to_string(CHS)+",-0.5,"+to_string(CHS-0.5)+")").c_str());
        TH1I * hChannels = (TH1I*)gDirectory->Get("hChannel");
        hChannels->GetXaxis()->SetTitle("channel");
    }
    return (TH1I*)gDirectory->Get("hChannel");
}

int ethzSticAna::initEnergyHistos() {
    int noChannels = 0;
    TH1I * hChannels = getChannelsHisto();
    for(unsigned int i = 1; i <= CHS; ++i) {
        if(hChannels->GetBinContent(i) > 0) {
            noChannels++;
            unsigned int ch = hChannels->GetXaxis()->GetBinCenter(i);
            _hEnergy[ch] = new TH1F(("hEnergy"+to_string(ch)).c_str(),
                                    ("hEnergy"+to_string(ch)).c_str(),
                                    energyMaxTOFPET, 0, energyMaxTOFPET);
            _hEnergy[ch]->GetXaxis()->SetTitle("energy (tot)");
            _hEnergy[ch]->SetTitle(("Energy ch "+to_string(ch)).c_str());
        }
    }
    return noChannels;
}

void ethzSticAna::initDeltaTHistos() {
    for(auto coincPair = _coincPairs.begin(); coincPair != _coincPairs.end(); coincPair++) {
        _hDeltaT[*coincPair] = new TH1F(("hDeltaT"+to_string(coincPair->first)+"-"+to_string(coincPair->second)).c_str(),
                                                  ("delta time ch "+to_string(coincPair->first)+" and "+to_string(coincPair->second)).c_str(),
                                                  ctrBinNumber,
                                                  -(int)((ctrBinNumber/2)*fineBinWidth),
                                                  (int)((ctrBinNumber/2)*fineBinWidth));
        _hDeltaT[*coincPair]->GetXaxis()->SetTitle("deltaT [ps]");
        _hDeltaT[*coincPair]->Sumw2();
    }

    //hCoincStatistics = new TH1F("hCoincStatistics", "Coincidences",CHS*(CHS+1), -0.5, CHS*(CHS+1)-0.5);
    hCoincStatistics = new TH1F("hCoincStatistics", "Coincidences",256, -0.5, 256-0.5);
    hCoincStatistics->GetXaxis()->SetTitle(("pairID = ch1 x "+to_string(CHS)+" + ch2").c_str());
    hCoincStatistics->GetYaxis()->SetTitle("# of coincidences");
    hCoincStatistics->GetYaxis()->SetTitleOffset(1.4);
}

void ethzSticAna::initDeltaTPBHistos() {
    //std::cout << "debug: init per bin histos" << std::endl;
    if(!_dnlLoaded()) {
        std::cerr << "Per Bin Histograms (_hDeltaT_pb) can only be loaded after DNL is loaded with loadDNLtable()" << std::endl;
        return;
    }
    for(auto coincPair = _coincPairs.begin(); coincPair != _coincPairs.end(); coincPair++) {
        for(unsigned int bin = 0; bin < 32; ++bin) {
            _hDeltaT_pb[*coincPair].push_back(new TH1F(("hDeltaT_pb"+to_string(bin)+"_pair"+to_string(coincPair->first)+"-"+to_string(coincPair->second)).c_str(),
                                                  ("Correlation ch "+to_string(coincPair->first)+" bin "+to_string(bin)+" and all bins of ch "+to_string(coincPair->second)).c_str(),
                                                  32, _DNLtable[coincPair->second])
                                              );
            _hDeltaT_pb[*coincPair].back()->GetXaxis()->SetTitle("deltaT [ps]");
        }
    }


}

int ethzSticAna::buildCoinc(bool discardMulti, bool realtime) {
    if(!_energyLoadeded()) {
        std::cerr << "ethzSticAna::buildCoinc please load first the energies with loadEnergy" << std::endl;
        return -1;
    }

    if(discardMulti) std::cout << "Hits with multiplicity > 1 are discarded." << std::endl;
    if(_isTOFPET) {
        if(discardMulti) std::cout << "Hits with global multiplicity > 2 are discarded." << std::endl;
        return buildCoincTOFPET(discardMulti);
    }

    stic_data_e * event = NULL;
    std::map<chPair, unsigned int> coincNo;
    initDeltaTHistos();
    if(realtime)
        initDeltaTPBHistos();

    TBranch * branch = _itree->GetBranch("br");
    branch->SetAddress(&event);

    std::vector<std::pair<double,std::pair<double, double>>> multiHits;
    std::vector<std::pair<unsigned int, unsigned int>> multiHitsFine;
    std::vector<std::pair<unsigned int, unsigned int>> multiHitsCoars;
    //std::vector<std::pair<double, double>> multiHitsFineErr;
    double windowStart = 0;      // used to check multiplicities in coinc windows
    std::vector<unsigned int> windowHits;
    unsigned int windowHitsOffset = 0;

    chPair pair;

    //TH1I * statFrames = new TH1I("statFrames","Events per Frame",21,-0.5,20.5);
    Int_t frame = 0;
    std::map<unsigned int, std::vector<stic_data_e>> events;
    for(unsigned int i = 0; i < branch->GetEntries() ; ++i) {
        if(i%100000==0)
            std::cout << "\r" << std::fixed << std::setprecision(2) << (float)i/branch->GetEntries()*100 << "%" << std::flush;// << std::endl;
        branch->GetEntry(i);
        if((frame >> coincBuilderFrames) < (event->frame_number >> coincBuilderFrames)) {
            if(i > 0) {
                for(auto ch1 = events.begin(); ch1 != events.end(); ++ch1) {
                    for(auto ch2 = std::next(ch1); ch2 != events.end(); ++ch2) {
                        //std::cout << "debug: " << ch1->first << ", " << ch2->first << std::endl;
                        if(ch1->first < ch2->first)
                            pair = std::make_pair(ch1->first, ch2->first);
                        else
                            pair = std::make_pair(ch2->first, ch1->first);
                        if(std::find(_coincPairs.begin(), _coincPairs.end(), pair) == _coincPairs.end())
                            continue;
                        for(auto hit1 = events[pair.first].begin(); hit1 != events[pair.first].end(); ++hit1) {
                            for(auto hit2 = events[pair.second].begin(); hit2 != events[pair.second].end(); ++hit2) {
                                if(realtime) {
                                    double uncertainty1, uncertainty2;
                                    double realTimeDelta = getRealTime(&(*hit1), &uncertainty1) - getRealTime(&(*hit2), &uncertainty2);
                                    //double uncertainty = sqrt(uncertainty1*uncertainty1 + uncertainty2*uncertainty2);
                                     //_hDeltaT[*coincPair]->Fill(realTimeDelta, 1/(uncertainty));
                                    if(abs(realTimeDelta) <= coincWindow*1e3) {
                                        //multiHits.push_back(std::make_pair(realTimeDelta, uncertainty));
                                        multiHits.push_back(std::make_pair(realTimeDelta, std::make_pair(uncertainty1, uncertainty2)));
                                        multiHitsFine.push_back(std::make_pair(hit1->T_fine, hit2->T_fine));
                                        multiHitsCoars.push_back(std::make_pair(hit1->T_CC, hit2->T_CC));
                                    }
                                } else {
                                    double deltaT = (hit1->getTime()-hit2->getTime())*fineBinWidth;
                                    if(abs(deltaT) <= coincWindow*1e3) {
                                        //multiHits.push_back(std::make_pair(deltaT, -1)); //_hDeltaT[*coincPair]->Fill((it1->getTime()-it2->getTime())*fineBinWidth);
                                        multiHits.push_back(std::make_pair(deltaT, std::make_pair(-1,-1)));
                                        multiHitsFine.push_back(std::make_pair(hit1->T_fine, hit2->T_fine));
                                        multiHitsCoars.push_back(std::make_pair(hit1->T_CC, hit2->T_CC));
                                    }
                                }
                            }
                            hMultiplicity->Fill(multiHits.size());
                            unsigned int j = 0;
                            for(auto it = multiHits.begin(); it != multiHits.end(); it++) {
                                if(discardMulti && multiHits.size() > 1) break;
                                coincNo[pair]++;
                                if(realtime) {
                                    unsigned int kk = 1000;
                                    for(unsigned int k = 0; k < kk; ++k) {
                                        double rx;
                                        do{
                                            double flat = sqrt(12)* std::abs((it->second).first - (it->second).second);
                                            double lin = sqrt(12) * std::min((it->second).first, (it->second).second);
                                            rx = generator.Rndm() * (2*lin + flat) ;
                                            double ry = generator.Rndm();
                                            if(abs(rx) <= flat)
                                                break; // take it anyway
                                            if((0.5*flat + lin) - abs(rx) >= ry)
                                                break; // take it if its in the right half
                                        }while(true);
                                        _hDeltaT[pair]->Fill(it->first + rx,1.0/kk);//, 1.0/100);
                                        //_hDeltaT[pair]->Fill(generator.Gaus(it->first, it->second),1.0/100);//, 1.0/100);
                                    }

                                    // Development
                                    //double binStart = _DNLtable[pair.second][multiHitsFine[j].second];
                                    //double binEnd = _DNLtable[pair.second][multiHitsFine[j].second+1];
                                    //double ch1Time = _DNLtable[pair.first][multiHitsFine[j].first];
                                    //double ch2Time = _DNLtable[pair.second][multiHitsFine[j].second];
                                    //ch2Time += (multiHitsCoars[j].second - multiHitsCoars[j].first) * 32 * fineBinWidth;
                                    //_hDeltaT_pb[pair][multiHitsFine[j].first]->Fill(deltaT, 1);
                                    if(abs(it->first) < (32/2 * fineBinWidth))
                                        _hDeltaT_pb[pair][multiHitsFine[j].first]->Fill(multiHitsFine[j].second * fineBinWidth);


                                } else {
                                    _hDeltaT[pair]->Fill(it->first);
                                }
                                ++j;
                            }
                            multiHits.clear();
                            multiHitsFine.clear();
                            multiHitsCoars.clear();
                        }
                    }
                }
            }
            events.clear();
            windowHits.clear();
            windowHitsOffset = 0;
            //for(auto it = events.begin(); it != events.end(); it++) (it->second).clear(); // clear events
        }
        frame = event->frame_number;
        if(event->getEnergy() > _eCuts[event->channel].first && event->getEnergy() < _eCuts[event->channel].second) {
            int windowMulti = windowHits.size() + windowHitsOffset;
            if(windowMulti==0) {
                windowStart = event->getTime() * fineBinWidth; // start new window
                windowHitsOffset = 0;
            } else {
                if(event->getTime() * fineBinWidth > windowStart + coincWindow*1e3) {
                    hGlobalMultiplicity->Fill(windowMulti);
                    if(windowMulti < 2) {
                        windowHits.clear();
                        windowStart = event->getTime() * fineBinWidth;
                        windowHitsOffset = 0;
                    } else if(windowMulti >= 2) {
                        unsigned int lastChannel = windowHits.back();
                        //windowHits.push_back(lastChannel);
                        windowStart = events[lastChannel].back().getTime() * fineBinWidth;
                        windowHitsOffset = 1;

                        if(discardMulti && windowMulti > 2) {
                            for(auto & hit : windowHits) {
                                events[hit].pop_back();
                            }
                        }

                        windowHits.clear();
                    }
                }
            }
            windowHits.push_back(event->channel);
            events[event->channel].push_back(*event);
        }
    }
    std::cout << "\r100.00%" << std::endl;

    int coincNoSum = 0;
    for(auto it = coincNo.begin(); it != coincNo.end(); it++) {
        hCoincStatistics->SetBinContent(hCoincStatistics->FindBin(getPairId((*it).first)), it->second);
        //std::cout << "debug: " << " " << getPairId(it->first) << " " << hCoincStatistics->FindBin(getPairId((*it).first)) << " " << it->second << std::endl;
        coincNoSum += it->second;
    }
    return coincNoSum;
}

void ethzSticAna::setEnergyCuts(double energyMin) {
    float p1 = 0;
    for(auto it = _hEnergy.begin(); it != _hEnergy.end(); it++) {
        std::cout << "Set energy cuts channel " << it->first << ": ";
        TF1 *photoPeak = new TF1("photoPeak","gaus", 0, 1000);
        TSpectrum *s = new TSpectrum();
        s->Search(it->second,20,"");
        float *xpeaks = s->GetPositionX();
        for(Int_t l=0; l<3; l++) {
            p1 = xpeaks[l];
            if(p1 > energyMin)
                break;
        }

        photoPeak->SetParameter("Mean",p1);
        photoPeak->SetRange((p1-p1*0.2),(p1+p1*0.2));
        (it->second)->Fit("photoPeak","qR");
        photoPeak->SetRange((p1-p1*0.04),(p1+p1*0.04));
        (it->second)->Fit("photoPeak","qR");

        _eCuts[it->first] = std::make_pair(photoPeak->GetParameter("Mean") - photoPeak->GetParameter("Sigma")*energySigmas,
                                           photoPeak->GetParameter("Mean") + photoPeak->GetParameter("Sigma")*energySigmas);
        std::cout << photoPeak->GetParameter("Mean") << " +/- " << photoPeak->GetParameter("Sigma") << "*" << energySigmas << std::endl;
    }
    //return photoPeak->GetParameter("Mean");
}

double ethzSticAna::getRealTime(stic_data_e * event, double * uncertainty) {
    if(!_dnlLoaded()) {
        std::cerr << "ethzSticAna::getRealTime please loade DNL table with loadDNLTable()" << std::endl;
        return event->getTime();
    }

    if(uncertainty != nullptr) *uncertainty = _uncertaintyTable[event->channel][event->T_fine];
    double lower = _DNLtable[event->channel][event->T_fine];
    double upper = _DNLtable[event->channel][event->T_fine+1];
    //double fineT = lower + generator.Rndm()*(upper-lower);
    double fineT = lower + 0.5*(upper-lower);
    //return fineT; //generator.Rndm() * (32.0 * fineBinWidth);
    if(uncertainty != nullptr) *uncertainty = (upper-lower)/sqrt(12);
    return event->T_CC * (32.0 * fineBinWidth) + fineT;
    //return event->T_CC * (32.0 * fineBinWidth) + _DNLtable[event->channel][event->T_fine];
}

void ethzSticAna::writeHistos(bool all) {
    // Root stuff
    gStyle->SetOptFit(1111111);
    if(all) hCoincStatistics->Write();
    if(all) hMultiplicity->Write();
    hGlobalMultiplicity->Write();
    writeEnergyHistos(all);
    writeDeltaTHistos(all);
}

void ethzSticAna::writeEnergyHistos(bool all, bool stat) {
    TCanvas * c1 = new TCanvas("energies I");
    c1->Divide(4,4);
    TCanvas * c2 = new TCanvas("energies II");
    c2->Divide(4,4);
    unsigned int histNoSide1 = 0;
    unsigned int histNoSide2 = 0;

    for(auto it = _hEnergy.begin(); it != _hEnergy.end(); it++) {
        if(it->first<CHS/2) c1->cd(++histNoSide1); else c2->cd(++histNoSide2);
        (it->second)->Draw();
        if(all) (it->second)->Write();
    }
    c1->Write();
    c2->Write();

    // Per Channel overview
    if(stat) {
        TCanvas * cEnergyStat = new TCanvas("cEnergyStat");
        const int chNo = _eCuts.size();
        Double_t ch[chNo];
        Double_t peak[chNo];
        Double_t width[chNo];

        int i = 0;
        for(auto it = _eCuts.begin(); it != _eCuts.end(); it++) {
            ch[i] = it->first;
            fitValues  photoPeak = _photoPeak(it->first);
            peak[i] =  photoPeak.first;
            width[i] = photoPeak.second;
            i++;
        }
        TGraphErrors * gPhotopeak = new TGraphErrors(chNo, ch, peak, 0, width);
        gPhotopeak->SetTitle("Photopeaks");
        gPhotopeak->GetXaxis()->SetTitle("channel");
        gPhotopeak->GetYaxis()->SetTitle("position [1.6ns]");
        gPhotopeak->SetMarkerStyle(20);
        gPhotopeak->Draw("AP");
        cEnergyStat->Write();
    }
}

void ethzSticAna::writeDeltaTHistos(bool perbin) {
    for(auto it = _hDeltaT.begin(); it != _hDeltaT.end(); it++) (it->second)->Write();
    if(perbin) {
        TH1F * hCorrected;
        for(auto it = _hDeltaT_pb.begin(); it != _hDeltaT_pb.end(); it++) {
            for(auto h = (it->second).begin(); h != (it->second).end(); h++) {
                (*h)->Write();
                hCorrected = (TH1F*)(*h)->Clone();
                hCorrected->SetName((std::string((*h)->GetName())+"cor").c_str());
                for(unsigned int bin = 1; bin <= 32; ++bin) {
                    double binWidth = _DNLtable[(it->first).second][bin] - _DNLtable[(it->first).second][bin-1];
                    hCorrected->SetBinContent(bin, (*h)->GetBinContent(bin)/binWidth);
                    hCorrected->SetBinError(bin, (*h)->GetBinError(bin)/binWidth);

                }

                hCorrected->Write();
                hCorrected->Delete();
            }
        }
    }
}

fitValues ethzSticAna::getCTR(chPair pair, fitValues * mean, double * chi2) {
    TF1 *deltaT = new TF1("deltaT","gaus",
                          -(int)((ctrBinNumber/2)*fineBinWidth),
                          (int)((ctrBinNumber/2)*fineBinWidth));
    TF1 *deltaTOffset = new TF1("deltaTOffset","gaus(0)+pol0(3)",
                          -(int)((ctrBinNumber/2)*fineBinWidth),
                          (int)((ctrBinNumber/2)*fineBinWidth));
    _hDeltaT[pair]->Fit("deltaT", "qR");
    deltaTOffset->SetRange(deltaT->GetParameter("Mean") - deltaT->GetParameter("Sigma") * ctrFitSigmas,
                     deltaT->GetParameter("Mean") + deltaT->GetParameter("Sigma") * ctrFitSigmas);
    deltaTOffset->SetParameter(0, deltaT->GetParameter("Constant"));
    deltaTOffset->SetParameter(1, deltaT->GetParameter("Mean"));
    deltaTOffset->SetParameter(2, deltaT->GetParameter("Sigma"));

    // Adjust the histogram errors for kernel density
    for(unsigned int m = 1; m <= _hDeltaT[pair]->GetNbinsX(); ++m) {
        _hDeltaT[pair]->SetBinError(m, sqrt(_hDeltaT[pair]->GetBinContent(m)));
    }

    _hDeltaT[pair]->Fit("deltaTOffset", "qR"); // "L" if likelihood
    std::cout << "Channel pair " << pair.first << "/" << pair.second << ": sigma " << deltaTOffset->GetParameter(2);
    std::cout << "ps (+/- " << deltaTOffset->GetParError(2) << "ps)";
    std::cout << ", offset " << deltaTOffset->GetParameter(1) << "ps";
    std::cout << ", chi2 "<< deltaTOffset->GetChisquare() << "/" << deltaTOffset->GetNDF();
    std::cout << " (" << (deltaTOffset->GetChisquare()/deltaTOffset->GetNDF()) << ")" << std::endl;

    if(mean != nullptr) *mean = std::make_pair(deltaTOffset->GetParameter(1), deltaTOffset->GetParError(1));
    if(chi2 != nullptr) *chi2 = (deltaTOffset->GetChisquare()/deltaTOffset->GetNDF());
    return std::make_pair(deltaTOffset->GetParameter(2), deltaTOffset->GetParError(2));
}

fitValues ethzSticAna::getMean(chPair pair) {
    fitValues * mean;
    getCTR(pair, mean);
    return *mean;
}

void ethzSticAna::getCTRs() {
    if(!_coincBuilt()) {
        std::cerr << "ethzSticAna::getCTRs please build first coincidences with buildCoinc()" << std::endl;
        return;
    }
    for(auto pair = _coincPairs.begin(); pair != _coincPairs.end(); pair++) {
        _ctr[*pair] = getCTR(*pair, &(_mean[*pair]), &(_chi2[*pair]));
    }
}


unsigned int ethzSticAna::getCoincStat(chPair pair ) {
    return hCoincStatistics->GetBinContent(hCoincStatistics->FindBin(getPairId(pair)));
}

//unsigned int ethzSticAna::getPairId(chPair pair) {return pair.first * CHS + pair.second;}

unsigned int ethzSticAna::getPairId(chPair pair) {
    return (pair.first - _minPairId.first) * 16 + (pair.second-_minPairId.second);
}

fitValues ethzSticAna::_photoPeak(unsigned int ch) {
    int peak = (_eCuts[ch].first + _eCuts[ch].second)/2;
    int width =(_eCuts[ch].second - _eCuts[ch].first)/(2.0*energySigmas);
    return std::make_pair(peak, width);
}
