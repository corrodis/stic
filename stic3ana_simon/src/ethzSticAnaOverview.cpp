#include "ethzSticAna.h"

#include "TCanvas.h"
#include "TF1.h"
#include "TGraphErrors.h"
#include "TLine.h"
#include "TStyle.h"

#include <iostream>
#include <cmath>

using std::to_string;

void ethzSticAna::writeOverview(int statCut, int chi2Cut) {
    if(!_coincBuilt()) {
        std::cerr << "ethzSticAna::writeChiaraPlots please build first coincidences with coincBuild() and create ctrs with getCTRs()" << std::endl;
        return;
    }
    if(!_ctrBuilt()) {
        std::cerr << "ethzSticAna::writeChiaraPlots please create ctrs with getCTR()" << std::endl;
        return;
    }

    TCanvas * cOverview = new TCanvas("cOverview");
    cOverview->Divide(4, 2);

    Double_t stat[_coincPairs.size()];
    Double_t ctr[_coincPairs.size()];
    Double_t ctr_err[_coincPairs.size()];
    Double_t pairId[_coincPairs.size()];
    Double_t mean[_coincPairs.size()];
    Double_t mean_err[_coincPairs.size()];
    Double_t chi2[_coincPairs.size()];

    TH1F * hCTRs = new TH1F("hCTRs", ("CTR sigma (# of coinc > "+to_string(statCut)+")").c_str(), 32, 90, 250);
    hCTRs->GetXaxis()->SetTitle("ctr sigma [ps]");

    TH1F * hCTRs_chi2 = new TH1F("hCTRs_chi2", ("CTR sigma (# of coinc > "+to_string(statCut)+", chi2 < "+to_string(chi2Cut)+")").c_str(), 32, 90, 250);
    hCTRs->GetXaxis()->SetTitle("ctr sigma [ps]");

    TH1F * hChi2 = new TH1F("hChi2","Chi2",100,0,10);
    TH1F * hChi2_sel = new TH1F("hChi2_sel",("Chi2 (# of coinc > "+to_string(statCut)+")").c_str(),100,0,10);

    int i = 0;
    for(auto pair = _coincPairs.begin(); pair != _coincPairs.end(); pair++) {
        stat[i] = getCoincStat(*pair);
        ctr[i] = _ctr[*pair].first;
        ctr_err[i] = _ctr[*pair].second;
        pairId[i] = getPairId(*pair);
        mean[i] = _mean[*pair].first;
        mean_err[i] = _mean[*pair].second;
        if(!isnan(_chi2[*pair]) && !isinf(_chi2[*pair])) {
            chi2[i] = _chi2[*pair];
            hChi2->Fill(_chi2[*pair]);
            if(stat[i] > statCut)
                hChi2_sel->Fill(_chi2[*pair]);
        } else {
            chi2[i] = 0;
        }
        if(stat[i] > statCut) hCTRs->Fill(_ctr[*pair].first);
        if(stat[i] > statCut && chi2[i] < chi2Cut) hCTRs_chi2->Fill(_ctr[*pair].first);

        i++;
    }

    // Statistics
    cOverview->cd(1);
    //cOverview->GetPad(1)->SetGrid();
    hCoincStatistics->Draw();

    // Mean
    cOverview->cd(2);
    cOverview->GetPad(2)->SetGrid();
    TGraphErrors * hPairVsMean = new TGraphErrors(_coincPairs.size(), pairId, mean, 0, mean_err);
    hPairVsMean->GetXaxis()->SetTitle(("pairID = ch1 x "+to_string(CHS)+" + ch2").c_str());
    hPairVsMean->GetYaxis()->SetTitle("mean [ps]");
    hPairVsMean->GetYaxis()->SetTitleOffset(1.4);
    //hPairVsMean->SetMarkerStyle(20);
    hPairVsMean->SetTitle("Offset");
    hPairVsMean->Draw("AP");

    // pair vs ctr
    cOverview->cd(3);
    cOverview->GetPad(3)->SetGrid();
    TGraphErrors * hPairVsCtr = new TGraphErrors(_coincPairs.size(), pairId, ctr, 0, ctr_err);
    hPairVsCtr->GetXaxis()->SetTitle(("pairID = ch1 x "+to_string(CHS)+" + ch2").c_str());
    hPairVsCtr->GetYaxis()->SetTitle("ctr sigma [ps]");
    hPairVsCtr->GetYaxis()->SetTitleOffset(1.4);
    //hPairVsCtr->SetMarkerStyle(20);
    hPairVsCtr->SetTitle("CTR");
    hPairVsCtr->Draw("AP");

    // pair vs chi2
    cOverview->cd(4);
    cOverview->GetPad(4)->SetGrid();
    TGraph * hPairVsChi2 = new TGraph(_coincPairs.size(), pairId, chi2);
    hPairVsChi2->GetXaxis()->SetTitle(("pairID = ch1 x "+to_string(CHS)+" + ch2").c_str());
    hPairVsChi2->GetYaxis()->SetTitle("chi2/dnf");
    //hPairVsCtr->GetYaxis()->SetTitleOffset(1.4);
    hPairVsChi2->SetMarkerStyle(20);
    hPairVsChi2->SetTitle("Chi2");
    hPairVsChi2->Draw("AP");

    // Statistics vs Mean
    cOverview->cd(5);
    cOverview->GetPad(5)->SetGrid();
    TGraphErrors * gStatVsCtr = new TGraphErrors(_coincPairs.size(), stat, ctr, 0, ctr_err);
    gStatVsCtr->GetXaxis()->SetTitle("Statistics");
    gStatVsCtr->GetYaxis()->SetTitle("ctr sigma [ps]");
    gStatVsCtr->GetYaxis()->SetTitleOffset(1.4);
    //gStatVsCtr->SetMarkerStyle(20);
    gStatVsCtr->SetTitle("CTR vs Stat");
    gStatVsCtr->Draw("AP");
    TLine * cutLine = new TLine(statCut, gStatVsCtr->GetXaxis()->GetXmin(), statCut, gStatVsCtr->GetXaxis()->GetXmax());
    cutLine->SetLineColor(kRed);
    cutLine->Draw();

    // Statistics vs chi2
    cOverview->cd(6);
    cOverview->GetPad(6)->SetGrid();
    TGraph * gStatVsChi2 = new TGraph(_coincPairs.size(), stat, chi2);
    gStatVsChi2->GetXaxis()->SetTitle("Statistics");
    gStatVsChi2->GetYaxis()->SetTitle("chi2/ndf");
    //gStatVsChi2->GetYaxis()->SetTitleOffset(1.4);
    gStatVsChi2->SetMarkerStyle(20);
    gStatVsChi2->SetTitle("chi2 vs Stat");
    gStatVsChi2->Draw("AP");
    TLine * cutLine2 = new TLine(0, chi2Cut, hCoincStatistics->GetMaximum(), chi2Cut);
    cutLine2->SetLineColor(kRed);
    cutLine2->Draw();

    // CTR distribution
    cOverview->cd(7);
    //cOverview->GetPad(5)->SetGrid();
    hCTRs->Draw();
    TF1 * fGaus = new TF1("fGaus","gaus",50,400);
    gStyle->SetOptFit(1111);
    hCTRs->Fit("fGaus","qR");

    // CTR distribution
    cOverview->cd(8);
    //cOverview->GetPad(5)->SetGrid();
    hCTRs_chi2->Draw();
    TF1 * fGaus_chi2 = new TF1("fGaus_chi2","gaus",50,400);
    gStyle->SetOptFit(1111);
    hCTRs_chi2->Fit("fGaus_chi2","qR");
    std::cout << "The ONE number: " << fGaus_chi2->GetParameter("Mean") << "ps sigma." << std::endl;

    cOverview->Write();
    hChi2->Write();
    hChi2_sel->Write();
}

