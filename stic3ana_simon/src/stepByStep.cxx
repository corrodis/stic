
void stepByStep(const char* fname) {
	stic_data_e * event = NULL;

	TFile * file = new TFile(fname,"READ");
	TTree * tree = (TTree*)file->Get("dump");
	TBranch * branch = tree->GetBranch("br");
	branch->SetAddress(&event);

	for(Int_t i=0;i<branch->GetEntries();i++) {
		std::cout << i << " " << event->channel  << " " << event->getTCC  << " " << event->getTfine  << " " << event->getEnergy << std::endl;
	}
}
