set terminal eps

# USAGE:  gnuplot -e "PREFIX='';EB='?';ET='?';BIAS='?'" scan_ctr_bias.plt

# Data Format of the scan.data
#  1: RUN - NO
#  2: DAC - BIAS
#  3: DAC - LADDER
#  4: DAC - ICOMP
#  5: DAC - ?
#  6: DAC - TBIAS
#  7: DAC - TTHRESHOLD
#  8: DAC - EBIAS
#  9: DAC - ETHRESHOLD
#  10: BIAS VOLTAGE (nominal 58.14)
# 11: CTR Sigma
# 12: CTR Sigma fiTTrror
# 13: E1 Position
# 14: E1 Position error
# 15: E2 Position
# 16: E2 Position error
# 17: number of coincidence events afTTr energy cuts 
# 18: number of underflows
# 19: number of overflows

set linetype 1 lc rgb "#0EA857"		lw 1 pt 0
set linetype 2 lc rgb "#0EAD7E"		lw 1 pt 0
set linetype 3 lc rgb "#0EB2A6" 	lw 1 pt 0
set linetype 4 lc rgb "#0D9CB7" 	lw 1 pt 0
set linetype 5 lc rgb "#0D78BC" 	lw 1 pt 0
set linetype 6 lc rgb "#0C52C0" 	lw 1 pt 0
set linetype 7 lc rgb "#0B29C5" 	lw 1 pt 0
set linetype 8 lc rgb "#170BCA" 	lw 1 pt 0
set linetype 9 lc rgb "#440ACF" 	lw 1 pt 0
set linetype 10 lc rgb "#7309D4"	lw 1 pt 0
set linetype 11 lc rgb "#A408D9"	lw 1 pt 0
set linetype 12 lc rgb "#D807D0"	lw 1 pt 0
set linetype 13 lc rgb "#E207B5"	lw 1 pt 0
set linetype 14 lc rgb "#D21A26"	lw 1 pt 0
set linetype 15 lc rgb "#E10F22"	lw 1 pt 0
set linetype 16 lc rgb "#F1041F"	lw 1 pt 0


############# E1 vs TTHRES (BIAS) with fixed TB ############################
set autoscale y
set xlabel "over-voltag [V]"
set ylabel "E1 Position ToT [1.6ns]"
set xrange [-0.25:4.25]
set x2range [-0.25:4.25]
set mxtics 0.5
set xtics 0,0.5 scale 0
set x2tics -0.25,0.5 mirror format ""
set grid x2tics ytics
set key outside Right

#set output "plots/test.eps"
do for [TB=0:15] {
	set output "plots/scan_".PREFIX."_E1_TBIAS-".TB.".eps"
	set title "Bias Scan, (EDAC".EB."-".ET.", TBIAS ".TB.")"
	plot 266 notitle lc rgb "white",\
		"scan.data" using ($6==TB&&$7==1&&$8==EB&&$9==ET?$10-BIAS-0.13:1/0):13:14 with yerrorbars title "TT  0",\
		"scan.data" using ($6==TB&&$7==1&&$8==EB&&$9==ET?$10-BIAS-0.13:1/0):13:14 with yerrorbars title "TT  1",\
		"scan.data" using ($6==TB&&$7==2&&$8==EB&&$9==ET?$10-BIAS-0.11:1/0):13:14 with yerrorbars title "TT  2",\
		"scan.data" using ($6==TB&&$7==3&&$8==EB&&$9==ET?$10-BIAS-0.09:1/0):13:14 with yerrorbars title "TT  3",\
		"scan.data" using ($6==TB&&$7==4&&$8==EB&&$9==ET?$10-BIAS-0.07:1/0):13:14 with yerrorbars title "TT  4",\
		"scan.data" using ($6==TB&&$7==5&&$8==EB&&$9==ET?$10-BIAS-0.05:1/0):13:14 with yerrorbars title "TT  5",\
		"scan.data" using ($6==TB&&$7==6&&$8==EB&&$9==ET?$10-BIAS-0.03:1/0):13:14 with yerrorbars title "TT  6",\
		"scan.data" using ($6==TB&&$7==7&&$8==EB&&$9==ET?$10-BIAS-0.01:1/0):13:14 with yerrorbars title "TT  7",\
		"scan.data" using ($6==TB&&$7==8&&$8==EB&&$9==ET?$10-BIAS+0.01:1/0):13:14 with yerrorbars title "TT  8",\
		"scan.data" using ($6==TB&&$7==9&&$8==EB&&$9==ET?$10-BIAS+0.03:1/0):13:14 with yerrorbars title "TT  9",\
		"scan.data" using ($6==TB&&$7==10&&$8==EB&&$9==ET?$10-BIAS+0.05:1/0):13:14 with yerrorbars title "TT 10",\
		"scan.data" using ($6==TB&&$7==11&&$8==EB&&$9==ET?$10-BIAS+0.07:1/0):13:14 with yerrorbars title "TT 11",\
		"scan.data" using ($6==TB&&$7==12&&$8==EB&&$9==ET?$10-BIAS+0.09:1/0):13:14 with yerrorbars title "TT 12",\
		"scan.data" using ($6==TB&&$7==13&&$8==EB&&$9==ET?$10-BIAS+0.11:1/0):13:14 with yerrorbars title "TT 13",\
		"scan.data" using ($6==TB&&$7==14&&$8==EB&&$9==ET?$10-BIAS+0.13:1/0):13:14 with yerrorbars title "TT 14",\
		"scan.data" using ($6==TB&&$7==15&&$8==EB&&$9==ET?$10-BIAS+0.15:1/0):13:14 with yerrorbars title "TT 15"
}


############# E1 vs BIAS (TB) with fixed TT ############################
set xlabel "over-voltag [V]"
set ylabel "E1 Position ToT [1.6ns]"
set xrange [-0.25:4.25]
set x2range [-0.25:4.25]
set mxtics 0.5
set xtics 0,0.5 scale 0
set x2tics -0.25,0.5 mirror format ""
set grid x2tics ytics
set key outside Right

do for [TT=0:15] {
	set output "plots/scan_".PREFIX."_E1_bias_TTHRES-".TT.".eps"
	set title "Bias Scan, (EDAC".EB."-".ET.", TTHRES ".TT.")"
	plot 266 notitle lc rgb "white",\
		"scan.data" using ($7==TT&&$6==0&&$8==EB&&$9==ET?$10-BIAS-0.15:1/0):13:14 with yerrorbars title "TB  0" ,\
		"scan.data" using ($7==TT&&$6==1&&$8==EB&&$9==ET?$10-BIAS-0.13:1/0):13:14 with yerrorbars title "TB  1",\
		"scan.data" using ($7==TT&&$6==2&&$8==EB&&$9==ET?$10-BIAS-0.11:1/0):13:14 with yerrorbars title "TB  2",\
		"scan.data" using ($7==TT&&$6==3&&$8==EB&&$9==ET?$10-BIAS-0.09:1/0):13:14 with yerrorbars title "TB  3",\
		"scan.data" using ($7==TT&&$6==4&&$8==EB&&$9==ET?$10-BIAS-0.07:1/0):13:14 with yerrorbars title "TB  4",\
		"scan.data" using ($7==TT&&$6==5&&$8==EB&&$9==ET?$10-BIAS-0.05:1/0):13:14 with yerrorbars title "TB  5",\
		"scan.data" using ($7==TT&&$6==6&&$8==EB&&$9==ET?$10-BIAS-0.03:1/0):13:14 with yerrorbars title "TB  6",\
		"scan.data" using ($7==TT&&$6==7&&$8==EB&&$9==ET?$10-BIAS-0.01:1/0):13:14 with yerrorbars title "TB  7",\
		"scan.data" using ($7==TT&&$6==8&&$8==EB&&$9==ET?$10-BIAS+0.01:1/0):13:14 with yerrorbars title "TB  8",\
		"scan.data" using ($7==TT&&$6==9&&$8==EB&&$9==ET?$10-BIAS+0.03:1/0):13:14 with yerrorbars title "TB  9",\
		"scan.data" using ($7==TT&&$6==10&&$8==EB&&$9==ET?$10-BIAS+0.05:1/0):13:14 with yerrorbars title "TB 10",\
		"scan.data" using ($7==TT&&$6==11&&$8==EB&&$9==ET?$10-BIAS+0.07:1/0):13:14 with yerrorbars title "TB 11",\
		"scan.data" using ($7==TT&&$6==12&&$8==EB&&$9==ET?$10-BIAS+0.09:1/0):13:14 with yerrorbars title "TB 12",\
		"scan.data" using ($7==TT&&$6==13&&$8==EB&&$9==ET?$10-BIAS+0.11:1/0):13:14 with yerrorbars title "TB 13",\
		"scan.data" using ($7==TT&&$6==14&&$8==EB&&$9==ET?$10-BIAS+0.13:1/0):13:14 with yerrorbars title "TB 14",\
		"scan.data" using ($7==TT&&$6==15&&$8==EB&&$9==ET?$10-BIAS+0.15:1/0):13:14 with yerrorbars title "TB 15"
}

############# E2 vs TTHRES (BIAS) with fixed TB ############################
set xlabel "over-voltag [V]"
set ylabel "E2 Position ToT [1.6ns]"
set xrange [-0.25:4.25]
set x2range [-0.25:4.25]
set mxtics 0.5
set xtics 0,0.5 scale 0
set x2tics -0.25,0.5 mirror format ""
set grid x2tics ytics
set key outside Right

do for [TB=0:15] {
	set output "plots/scan_".PREFIX."_ctr_E2_TBIAS-".TB.".eps"
	set title "Bias Scan, (EDAC".EB."-".ET.", TBIAS ".TB.")"
	plot 266 notitle lc rgb "white",\
		"scan.data" using ($6==0&&$7==0&&$8==EB&&$9==ET?$10-BIAS-0.15:1/0):15:16 with yerrorbars title "TT  0" ,\
		"scan.data" using ($6==TB&&$7==1&&$8==EB&&$9==ET?$10-BIAS-0.13:1/0):15:16 with yerrorbars title "TT  1",\
		"scan.data" using ($6==TB&&$7==2&&$8==EB&&$9==ET?$10-BIAS-0.11:1/0):15:16 with yerrorbars title "TT  2",\
		"scan.data" using ($6==TB&&$7==3&&$8==EB&&$9==ET?$10-BIAS-0.09:1/0):15:16 with yerrorbars title "TT  3",\
		"scan.data" using ($6==TB&&$7==4&&$8==EB&&$9==ET?$10-BIAS-0.07:1/0):15:16 with yerrorbars title "TT  4",\
		"scan.data" using ($6==TB&&$7==5&&$8==EB&&$9==ET?$10-BIAS-0.05:1/0):15:16 with yerrorbars title "TT  5",\
		"scan.data" using ($6==TB&&$7==6&&$8==EB&&$9==ET?$10-BIAS-0.03:1/0):15:16 with yerrorbars title "TT  6",\
		"scan.data" using ($6==TB&&$7==7&&$8==EB&&$9==ET?$10-BIAS-0.01:1/0):15:16 with yerrorbars title "TT  7",\
		"scan.data" using ($6==TB&&$7==8&&$8==EB&&$9==ET?$10-BIAS+0.01:1/0):15:16 with yerrorbars title "TT  8",\
		"scan.data" using ($6==TB&&$7==9&&$8==EB&&$9==ET?$10-BIAS+0.03:1/0):15:16 with yerrorbars title "TT  9",\
		"scan.data" using ($6==TB&&$7==10&&$8==EB&&$9==ET?$10-BIAS+0.05:1/0):15:16 with yerrorbars title "TT 10",\
		"scan.data" using ($6==TB&&$7==11&&$8==EB&&$9==ET?$10-BIAS+0.07:1/0):15:16 with yerrorbars title "TT 11",\
		"scan.data" using ($6==TB&&$7==12&&$8==EB&&$9==ET?$10-BIAS+0.09:1/0):15:16 with yerrorbars title "TT 12",\
		"scan.data" using ($6==TB&&$7==13&&$8==EB&&$9==ET?$10-BIAS+0.11:1/0):15:16 with yerrorbars title "TT 13",\
		"scan.data" using ($6==TB&&$7==14&&$8==EB&&$9==ET?$10-BIAS+0.13:1/0):15:16 with yerrorbars title "TT 14",\
		"scan.data" using ($6==TB&&$7==15&&$8==EB&&$9==ET?$10-BIAS+0.15:1/0):15:16 with yerrorbars title "TT 15"

}

############# E2 vs BIAS (TB) with fixed TT ############################
set xlabel "over-voltag [V]"
set ylabel "E2 Position ToT [1.6ns]"
set xrange [-0.25:4.25]
set x2range [-0.25:4.25]
set mxtics 0.5
set xtics 0,0.5 scale 0
set x2tics -0.25,0.5 mirror format ""
set grid x2tics ytics
set key outside Right

do for [TT=0:15] {
	set output "plots/scan_".PREFIX."_E2_bias_TTHRES-".TT.".eps"
	set title "Bias Scan, (EDAC".EB."-".ET.", TTHRES ".TT.")"
	plot 266 notitle lc rgb "white",\
		"scan.data" using ($7==TT&&$6==0&&$8==EB&&$9==ET?$10-BIAS-0.15:1/0):15:16 with yerrorbars title "TB  0" ,\
		"scan.data" using ($7==TT&&$6==1&&$8==EB&&$9==ET?$10-BIAS-0.13:1/0):15:16 with yerrorbars title "TB  1",\
		"scan.data" using ($7==TT&&$6==2&&$8==EB&&$9==ET?$10-BIAS-0.11:1/0):15:16 with yerrorbars title "TB  2",\
		"scan.data" using ($7==TT&&$6==3&&$8==EB&&$9==ET?$10-BIAS-0.09:1/0):15:16 with yerrorbars title "TB  3",\
		"scan.data" using ($7==TT&&$6==4&&$8==EB&&$9==ET?$10-BIAS-0.07:1/0):15:16 with yerrorbars title "TB  4",\
		"scan.data" using ($7==TT&&$6==5&&$8==EB&&$9==ET?$10-BIAS-0.05:1/0):15:16 with yerrorbars title "TB  5",\
		"scan.data" using ($7==TT&&$6==6&&$8==EB&&$9==ET?$10-BIAS-0.03:1/0):15:16 with yerrorbars title "TB  6",\
		"scan.data" using ($7==TT&&$6==7&&$8==EB&&$9==ET?$10-BIAS-0.01:1/0):15:16 with yerrorbars title "TB  7",\
		"scan.data" using ($7==TT&&$6==8&&$8==EB&&$9==ET?$10-BIAS+0.01:1/0):15:16 with yerrorbars title "TB  8",\
		"scan.data" using ($7==TT&&$6==9&&$8==EB&&$9==ET?$10-BIAS+0.03:1/0):15:16 with yerrorbars title "TB  9",\
		"scan.data" using ($7==TT&&$6==10&&$8==EB&&$9==ET?$10-BIAS+0.05:1/0):15:16 with yerrorbars title "TB 10",\
		"scan.data" using ($7==TT&&$6==11&&$8==EB&&$9==ET?$10-BIAS+0.07:1/0):15:16 with yerrorbars title "TB 11",\
		"scan.data" using ($7==TT&&$6==12&&$8==EB&&$9==ET?$10-BIAS+0.09:1/0):15:16 with yerrorbars title "TB 12",\
		"scan.data" using ($7==TT&&$6==13&&$8==EB&&$9==ET?$10-BIAS+0.11:1/0):15:16 with yerrorbars title "TB 13",\
		"scan.data" using ($7==TT&&$6==14&&$8==EB&&$9==ET?$10-BIAS+0.13:1/0):15:16 with yerrorbars title "TB 14",\
		"scan.data" using ($7==TT&&$6==15&&$8==EB&&$9==ET?$10-BIAS+0.15:1/0):15:16 with yerrorbars title "TB 15"
}





