set terminal pdf

# USAGE: gnuplot -e -e "PREFIX='';TB='?';TT='?';EB='?';ET='?';BIAS='?';NOMBIAS='?'" scan_workingpoint.plt

# Data Format of the scan.data
#  1: RUN - NO
#  2: DAC - BIAS
#  3: DAC - LADDER
#  4: DAC - ICOMP
#  5: DAC - ?
#  6: DAC - TBIAS
#  7: DAC - TTHRESHOLD
#  8: DAC - EBIAS
#  9: DAC - ETHRESHOLD
# 10: BIAS VOLTAGE (nominal 58.14)
# 11: overflowPosition Sigma
# 12: overflowPosition Sigma fiterror
# 13: overflowPosition
# 14: overflowPosition error
# 15: E2 Position
# 16: E2 Position error
# 17: number of coincidence events after energy cuts 
# 18: number of underflows
# 19: number of overflows

set linetype 1 lc rgb "#0EA857"		lw 1 pt 2
set linetype 2 lc rgb "#0EAD7E"		lw 1 pt 0
set linetype 3 lc rgb "#0EB2A6" 	lw 1 pt 0
set linetype 4 lc rgb "#0D9CB7" 	lw 1 pt 0
set linetype 5 lc rgb "#0D78BC" 	lw 1 pt 0
set linetype 6 lc rgb "#0C52C0" 	lw 1 pt 0
set linetype 7 lc rgb "#0B29C5" 	lw 1 pt 0
set linetype 8 lc rgb "#170BCA" 	lw 1 pt 0
set linetype 9 lc rgb "#440ACF" 	lw 1 pt 0
set linetype 10 lc rgb "#7309D4"	lw 1 pt 0
set linetype 11 lc rgb "#A408D9"	lw 1 pt 0
set linetype 12 lc rgb "#D807D0"	lw 1 pt 0
set linetype 13 lc rgb "#E207B5"	lw 1 pt 0
set linetype 14 lc rgb "#D21A26"	lw 1 pt 0
set linetype 15 lc rgb "#overflow0F22"	lw 1 pt 0
set linetype 16 lc rgb "#F1041F"	lw 1 pt 0



set output "plots/scan_".PREFIX."_workingpoint.pdf"

#set multiplot layout 3,1
set title "Working Point TBIAS ".TB.", TTHRES ".TT.", ".sprintf("+%.1fV",((BIAS + 0)-(NOMBIAS + 0)))
############# CTR vs TTHRES ############################
set xlabel "over-voltag [V]"
set ylabel "ctr FWHM [ps]"
set xrange [-0.25:4.25]
set x2range [-0.25:4.25]
set yrange [180:350]
set mxtics 0.5
set xtics 0,0.5 scale 0
set x2tics -0.25,0.5 mirror format ""
set grid x2tics ytics
plot "scan.data" using ($6==TB&&$7==TT&&$8==EB&&$9==ET?$10-NOMBIAS:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars notitle

############# CTR vs TBIAS  ############################
set xlabel "TBIAS"
set ylabel "ctr FWHM [ps]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set yrange [180:350]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
plot 	"scan.data" using ($7==TT&&$10==BIAS&&$8==EB&&$9==ET?$6:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars notitle

############# CTR vs TTHRES ############################
set xlabel "TTHRESH"
set ylabel "ctr FWHM [ps]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set yrange [180:350]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
plot 	"scan.data" using ($6==TB&&$10==BIAS&&$8==EB&&$9==ET?$7:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars notitle




set terminal eps

set title "Working Point TBIAS 6, TTHRES 7, +2V"
############# CTR vs TTHRES ############################
set output "plots/scan_".PREFIX."_workingpoint_bias.eps"
set xlabel "over-voltag [V]"
set ylabel "ctr FWHM [ps]"
set xrange [-0.25:4.25]
set x2range [-0.25:4.25]
set yrange [180:350]
set mxtics 0.5
set xtics 0,0.5 scale 0
set x2tics -0.25,0.5 mirror format ""
set grid x2tics ytics
plot "scan.data" using ($6==TB&&$7==TT&&$8==EB&&$9==ET?$10-NOMBIAS:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars notitle

############# CTR vs TBIAS  ############################
set output "plots/scan_".PREFIX."_workingpoint_tbias.eps"
set xlabel "TBIAS"
set ylabel "ctr FWHM [ps]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set yrange [180:350]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
plot 	"scan.data" using ($7==TT&&$10==BIAS&&$8==EB&&$9==ET?$6:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars notitle

############# CTR vs TTHRES ############################
set output "plots/scan_".PREFIX."_workingpoint_tthres.eps"
set xlabel "TTHRESH"
set ylabel "ctr FWHM [ps]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set yrange [180:350]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
plot 	"scan.data" using ($6==TB&&$10==BIAS&&$8==EB&&$9==ET?$7:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars notitle

############# CTR vs EBIAS ############################
set output "plots/scan_".PREFIX."_workingpoint_ebias.eps"
set xlabel "EBIAS"
set ylabel "ctr FWHM [ps]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set yrange [180:350]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
plot 	"scan.data" using ($6==TB&&$7==TT&&$10==BIAS&&$9==ET?$8:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars notitle

############# CTR vs ETHRESH ############################
set output "plots/scan_".PREFIX."_workingpoint_ethres.eps"
set xlabel "ETHRES"
set ylabel "ctr FWHM [ps]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set yrange [180:350]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
plot 	"scan.data" using ($6==TB&&$7==TT&&$10==BIAS&&$8==EB?$9:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars notitle


