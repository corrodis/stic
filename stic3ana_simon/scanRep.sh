#!/bin/bash
# Script for STiC2 scan in combination with AVTECH puls generator

fname="start"
DATAPATH="/home/mri-pet/Documents/stic/data/"
STiC2PATH="/home/mri-pet/Documents/stic/"
#GBPIBDEV="/dev/gpib004"

ACCTIME=300
CH1=12
CH2=13

DAC1="O_DAC" 
START1=31
END1=31 #63
STEP1=1
DAC2="O_LADDER_DAC"
START2=56
END2=56	#63
STEP2=1
DAC3="O_LADDER_INPUTBIAS"
START3=20
END3=20 #63
STEP3=1
DAC4="O_LADDER_ICOMP"
START4=43
END4=43 #63
STEP4=1
DAC7="O_DAC_TBIAS"
START7=3
END7=3 #15
STEP7=1
DAC8="O_DAC_TTHRESH"
START8=7
END8=7 #15
STEP8=1
DAC5="O_DAC_EBIAS"
START5=3
END5=3 #15
STEP5=1
DAC6="O_DAC_ETHRESH"
START6=
END6=15 #15
STEP6=1

#VOLTSV="0 10 44 48 50 52 54 56 58 59 60 61 62 64 68 70 73 76 79 82 85"
STARTV=0
ENDV=0 
STEPV=1


#NUM=$(( (($END1-$START1)/$STEP1+1) * (($END2-$START2)/$STEP2+1) * (($END3-$START3)/$STEP3+1) * (($END4-$START4)/$STEP4+1) * (($END5-$START5)/$STEP5+1) * (($END6-$START6)/$STEP6+1) * (($END7-$START7)/$STEP7+1) * (($END8-$START8)/$STEP8+1) ) * (($ENDV-$STARTV)/$STEPV+1) )

NUM=0
#mkdir $DATAPATH
#cp $fname".txt" $fname"0.txt"


#eval "GPIB::16::INSTR> "$GBPIBDEV
#eval "*RST;*CLS> "$GBPIBDEV
COUNT=0
   
for i1 in $(seq $START1 $STEP1 $END1) 
	  do
	    echo "-- -- -- -- -- -- -- -- -- $DAC1 $i1 - $(echo "obase=16; $i1" | bc) -- -- -- -- -- -- -- -- -- --" f
		for i2 in $(seq $START2 $STEP2 $END2) 
		  do
		    echo "-- -- -- -- -- -- -- -- -- $DAC2 $i2 - $(echo "obase=16; $i2" | bc) -- -- -- -- -- -- -- -- -- --" 
			for i3 in $(seq $START3 $STEP3 $END3) 
			  do
			    echo "-- -- -- -- -- -- -- -- -- $DAC3 $i3 - $(echo "obase=16; $i3" | bc) -- -- -- -- -- -- -- -- -- --" 
				for i4 in $(seq $START4 $STEP4 $END4) 
				  do
				    echo "-- -- -- -- -- -- -- -- -- $DAC4 $i4 - $(echo "obase=16; $i4" | bc) -- -- -- -- -- -- -- -- -- --" 
					for i5 in $(seq $START5 $STEP5 $END5) 
					  do
					    echo "-- -- -- -- -- -- -- -- -- $DAC5 $i5 - $(echo "obase=16; $i5" | bc) -- -- -- -- -- -- -- -- -- --"  
						for i6 in $(seq $START6 $STEP6 $END6) 
						  do
						    echo "-- -- -- -- -- -- -- -- -- $DAC6 $i6 - $(echo "obase=16; $i6" | bc) -- -- -- -- -- -- -- -- -- --"  
							for i7 in $(seq $START7 $STEP7 $END7) 
							  do
							    echo "-- -- -- -- -- -- -- -- -- $DAC7 $i7 - $(echo "obase=16; $i7" | bc) -- -- -- -- -- -- -- -- -- --" 
								for i8 in $(seq $START8 $STEP8 $END8) 
								  do
								    echo "-- -- -- -- -- -- -- -- -- $DAC8 $i8 - $(echo "obase=16; $i8" | bc) -- -- -- -- -- -- -- -- -- --" 

									COUNT=$(( $COUNT + 1))
									echo "-- -- -- -- -- -- -- -- -- "$COUNT"/"$NUM" :: "$i1"-"$i2"-"$i3"-"$i4"-"$i5"-"$i6"-"$i7"-"$i8" -- -- -- -- -- -- -- -- -- --"  
									echo "-- -- -- -- -- -- -- -- -- "$COUNT" /  "$NUM"::"$i1"-"$i2"-"$i3"-"$i4"-"$i5"-"$i6"-"$i7"-"$i8" -- -- -- -- -- -- -- -- -- --"


									# eval cd $STiC2PATH"stic2daq"
									#./dump &
									#/dumpto dummy.root 1 &

									# eval cd $STiC2PATH"stic2configGUI"
									#./cmdline_config $DATAPATH"/"$fname 
									#./cmdline_config $DIR"/"$fname
									#./cmdline_config $DIR"/"$fname
									#killall dump
									#killall dumpto
									#sleep 0.1
									#eval cd $STiC2PATH"stic2daq"

									#for volts in $VOLTSV
									#for volts in $(seq $STARTV $STEPV $ENDV) 
  									  #do
										DIR=$DATAPATH
										#echo "-- -- -- -- -- -- -- -- -- $volts V -- -- -- -- -- -- -- -- -- --" 

										# WRITE volts to GPIB
										#eval "SOUR1:VOLT "$volts" > "$GBPIBDEV
										#eval "SOUR1:VOLT? > "$GBPIBDEV 
										#eval "OUTP1 ON > "$GBPIBDEV

										#sleep 0.2
										./stepByStep $(echo $DIR)"scan_"$i1"-"$i2"-"$i3"-"$i4"-"$i5"-"$i6"-"$i7"-"$i8"-"$volts".root"
										#echo $(check.cxx($(echo $DIR)"scan_"$i1"-"$i2"-"$i3"-"$i4"-"$i5"-"$i6"-"$i7"-"$i8"-"$volts"))
#done

									#eval "./read $(echo $DATAPATH)"/scan_"$i1"-"$i2"-"$i3"-"$i4"-"$i5"-"$i6"-"$i7"-"$i8".root" "$CH" "$DATAPATH"/scan.data"
									#eval "./read_energy $(echo $DATAPATH)"/scan_"$i1"-"$i2"-"$i3"-"$i4"-"$i5"-"$i6"-"$i7"-"$i8".root" "$CH
								done
							done
						done
					done
				done
			done    
		done  
	done	



#cd $DATAPATH
#rm $fname"0.txt"
#echo "Scan done. This is the end."
#gnuplot scan.plot



#mkdir $DAC
#cp scan.txt $DAC/
#cp scan.png $DAC/
#cp $fname"0.txt" $DAC/

#gwenview scan.png

