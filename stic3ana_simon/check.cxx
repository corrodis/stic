
//int check(const char* fname, Int_t p1 = 0, In_t p2 = 0) {
//int check(const char* fname) {
int check(const char* fname) {
	//const cahr * fname = "";

	Int_t ch1 = 12;
	Int_t ch2 = 13;

	// run 0046
	 Double_t p1 = 410;
	 Double_t p2 = 370;
	Double_t xl,xu, yl, yu;

   	TChain * chain = new TChain("Coin");

	chain->Add(Form("%s_coin.root",fname));

	//if(fname=="setup10_ch12_ch13") {
		//chain->Add(Form("%s_coin.root",fname));
		//chain->Add(Form("%s2_coin.root",fname));
		//chain->Add(Form("%s3_coin.root",fname));
		//chain->Add(Form("%s4_coin.root",fname));
	//}
	//else {
   	//	chain->Add(Form("%s.root",fname));
	//}
   	//chain->Add("setup10_ch12_ch13_sdaq2.root");
   	//chain->Add("setup10_ch12_ch13_sdaq3.root");
	//chain->Add("setup10_ch12_ch13_sdaq4.root");
	TFile * file = new TFile(Form("%s_ana.root",fname),"RECREATE");
	//TChain chain("T");
	//TTree * tree = (TTree*)file->Get("Coin;14");
	if(1) {
	TCanvas * c = new TCanvas("c", "c", 1000, 800);	
	c->Divide(2,2);

	c->cd(1);
	chain->Draw("energy1>>hE1(1001,-0.5,1000.5)");
	c->cd(2);
	chain->Draw("energy2>>hE2(1001,-0.5,1000.5)");

	TSpectrum *s = new TSpectrum();
	s->Search(hE1,20,"");
	Float_t *xpeaks = s->GetPositionX();
	p1 = xpeaks[0];

	TSpectrum *s2 = new TSpectrum();
	s2->Search(hE2,20,"");
	Float_t *xpeaks2 = s2->GetPositionX();
	p2 = xpeaks2[0];


	c->cd(3);
	chain->Draw("energy1:energy2>>hE(1001,-0.5,1000.5,1001,-0.5,1000.5)","","COLZ");
	//tree->Draw("deltaTfine>>hTcc2(100001,-50000.5,50000.5)");
	c->cd(4);
	chain->Draw("energy1:energy2>>hEs(1001,-0.5,1000.5,1001,-0.5,1000.5)","deltaTfine>-100&&deltaTfine<100","COLZ");
	//chain->Draw("energy1:energy2>>hEs(101,-0.5,500.5,501,-0.5,500.5)","deltaTfine>-45&&deltaTfine<-20","COLZ");
		//tree->Draw("deltaTfine>>hTcc(101,-50.5,50.5)","deltaTcc==0");


	c->cd(1);
	TF1 *photo1 = new TF1("photo1","gaus", 0, 1000);
	photo1->SetParameter("Mean",p1);
	photo1->SetRange((p1-p1*0.2),(p1+p1*0.2));
	hE1->Fit("photo1","R");
	photo1->SetRange((p1-p1*0.07),(p1+p1*0.07));
	hE1->Fit("photo1","R");
	c->cd(2);
	TF1 *photo2 = new TF1("photo2","gaus", 0, 1000);
	photo2->SetParameter("Mean",p2);
	photo2->SetRange((p2-p2*0.1),(p2+p2*0.1));
	hE2->Fit("photo2","R");
	photo2->SetRange((p2-p2*0.07),(p2+p2*0.07));
	hE2->Fit("photo2","R");
	
	xl = photo1->GetParameter("Mean")-photo1->GetParameter("Sigma")*1.5;
	xu = photo1->GetParameter("Mean")+photo1->GetParameter("Sigma")*1.5;
	yl = photo2->GetParameter("Mean")-photo2->GetParameter("Sigma")*1.5;
	yu = photo2->GetParameter("Mean")+photo2->GetParameter("Sigma")*1.5;

	c->cd(3);
	TLine * linex1 = new TLine(0,xl,1000,xl);
	//TLine * linex1 = new TLine(100,0,100,1000);
	TLine * linex2 = new TLine(0,xu,1000,xu);
	TLine * liney1 = new TLine(yl,0,yl,1000);
	TLine * liney2 = new TLine(yu,0,yu,1000);

  	linex1->SetLineWidth(1);
  	linex1->SetLineColor(2);
  	linex2->SetLineWidth(1);
  	linex2->SetLineColor(2);
  	liney1->SetLineWidth(1);
  	liney1->SetLineColor(2);
  	liney2->SetLineWidth(1);
  	liney2->SetLineColor(2);
  	linex1->Draw();
	linex2->Draw();
	liney1->Draw();
	liney2->Draw();

	c->cd(4);
  	linex1->Draw();
	linex2->Draw();
	liney1->Draw();
	liney2->Draw();
	
	}
	TF1 *deltaT = new TF1("deltaT","gaus", -150, 150);
	TCanvas * c2 = new TCanvas("c2", "c2", 1000, 800);
	gPad->SetGrid();
	c2->SetGrid();
	gStyle->SetOptFit(0111);	
	c2->Divide(2,2);
	c2->cd(1);	
	chain->Draw("deltaTfine>>hdTall(401,-200.5,200.5)");
	hdTall->GetXaxis()->SetTitle("fine [51ps]");
	hdTall->Fit("deltaT","","",-10,10);
	c2->cd(2);	
	chain->Draw("deltaTfine>>hdT1(401,-200.5,200.5)",Form("energy1>%f&&energy1<%f",xl,xu));
	hdT1->GetXaxis()->SetTitle("fine [51ps]");
	hdT1->Fit("deltaT","","",-10,10);
	c2->cd(3);
	chain->Draw("deltaTfine>>hdT2(401,-200.5,200.5)",Form("energy2>%f&&energy2<%f",yl,yu));	
	//chain->Draw("deltaTfine>>hdTfinal2(10001,-5000.5,5000.5)",Form("energy1>%f&&energy1<%f&&energy2>%f&&energy2<%f",xl,xu,yl,yu));
	hdT2->GetXaxis()->SetTitle("fine [51ps]");
	hdT2->Fit("deltaT","","",-10,10);
	c2->cd(4);	
	chain->Draw("deltaTfine>>hdTfinal(401,-200.5,200.5)",Form("energy1>%f&&energy1<%f&&energy2>%f&&energy2<%f",xl,xu,yl,yu));	
	hdTfinal->GetXaxis()->SetTitle("fine [51ps]");
	//TH1D * hdTfinalClone = (TH1D*)(hdTfinal->Clone());
	hdTfinal->Fit("deltaT","","",-10,10);

	if(1) {
		TCanvas * c3 = new TCanvas("c3", "c3", 800, 600);
		c3->SetGrid();
		c3->Divide(1,2);
		c3->cd(1);
		gPad-> SetLogy();
		gPad-> SetGrid();
		chain->Draw("deltaTfine>>hdTfinalExt(2097152,-1048576.5,1048576.5)");
		hdTfinalExt->GetXaxis()->SetTitle("deltaT [51ps]");
		hdTfinalExt->SetTitle("deltaT no Cuts");
		c3->cd(2);
		gPad-> SetLogy();
		gPad-> SetGrid();
		chain->Draw("deltaTfine>>hdTfinalExt2(2097152,-1048576.5,1048576.5)",Form("energy1>%f&&energy1<%f&&energy2>%f&&energy2<%f",xl,xu,yl,yu));
		hdTfinalExt2->GetXaxis()->SetTitle("deltaT [51ps]");
		hdTfinalExt2->SetTitle("deltaT Energy Cuts");

	}
	//hdTfinalClone->Fit("deltaT2","R+","sames");
	//hdTfinalClone->Draw("Same Hist");
	
	// save canvas to pdf
	c->Print(Form("%s.pdf[",fname));
	c2->Print(Form("%s.pdf]",fname));
	
	// Save histograms
	if(0){
		TCanvas * c4 = new TCanvas("c4", "c4", 800, 600);
		hE1->Draw();
		c4->SaveAs(Form("plots/%s-E1.pdf",fname));
		hE2->Draw();
		c4->SaveAs(Form("plots/%s-E2.pdf",fname));
		hE->Draw();
		c4->SaveAs(Form("plots/%s-E1E2.pdf",fname));
		hEs->Draw();
		c4->SaveAs(Form("plots/%s-E1E2s.pdf",fname));
		hdTall->Draw();
		c4->SaveAs(Form("plots/%s-Tall.pdf",fname));
		hdT1->Draw();
		c4->SaveAs(Form("plots/%s-T1.pdf",fname));
		hdT2->Draw();
		c4->SaveAs(Form("plots/%s-T2.pdf",fname));
		hdTfinal->Draw();
		c4->SaveAs(Form("plots/%s-T.pdf",fname));
	}

	std::cout << (deltaT->GetParameter("Sigma")) << "\t" << std::endl;

	//file->Write();
	//file->Close();
	}
