set terminal eps

# USAGE: $ gnuplot -e "PREFIX='';EB='?';ET='?';BIAS='?'" scan_ctr_tbias.plt

# Data Format of the scan.data
#  1: RUN - NO
#  2: DAC - BIAS
#  3: DAC - LADDER
#  4: DAC - ICOMP
#  5: DAC - ?
#  6: DAC - TBIAS
#  7: DAC - TTHRESHOLD
#  8: DAC - EBIAS
#  9: DAC - ETHRESHOLD
#  10: BIAS VOLTAGE (nominal 58.14)
# 11: CTR Sigma
# 12: CTR Sigma fiterror
# 13: E1 Position
# 14: E1 Position error
# 15: E2 Position
# 16: E2 Position error
# 17: number of coincidence events after energy cuts 
# 18: number of underflows
# 19: number of overflows

set linetype 1 lc rgb "#0EA857"		lw 1 pt 0
set linetype 2 lc rgb "#0EAD7E"		lw 1 pt 0
set linetype 3 lc rgb "#0EB2A6" 	lw 1 pt 0
set linetype 4 lc rgb "#0D9CB7" 	lw 1 pt 0
set linetype 5 lc rgb "#0D78BC" 	lw 1 pt 0
set linetype 6 lc rgb "#0C52C0" 	lw 1 pt 0
set linetype 7 lc rgb "#0B29C5" 	lw 1 pt 0
set linetype 8 lc rgb "#170BCA" 	lw 1 pt 0
set linetype 9 lc rgb "#440ACF" 	lw 1 pt 0
set linetype 10 lc rgb "#7309D4"	lw 1 pt 0
set linetype 11 lc rgb "#A408D9"	lw 1 pt 0
set linetype 12 lc rgb "#D807D0"	lw 1 pt 0
set linetype 13 lc rgb "#E207B5"	lw 1 pt 0
set linetype 14 lc rgb "#D21A26"	lw 1 pt 0
set linetype 15 lc rgb "#E10F22"	lw 1 pt 0
set linetype 16 lc rgb "#F1041F"	lw 1 pt 0


############# CTR vs TBIAS (BIAS) with fixed TT ############################
set xlabel "TBIAS"
set ylabel "ctr FWHM [ps]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set yrange [180:350]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
set key outside Right

do for [TT=0:15] {
	set output "plots/scan_".PREFIX."_ctr_tbias_TTHRES-".TT.".eps"
	set title "TBias Scan, (EDAC ".EB."-".ET.", TTHRES ".TT.")"	
	plot 	"scan.data" using ($7==TT&&$10==BIAS-0.5&&$8==EB&&$9==ET?$6-0.09:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "-0.5 V" ,\
		"scan.data" using ($7==TT&&$10==BIAS+0.0&&$8==EB&&$9==ET?$6-0.07:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "+0.0 V" ,\
		"scan.data" using ($7==TT&&$10==BIAS+0.5&&$8==EB&&$9==ET?$6-0.05:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "+0.5 V" ,\
		"scan.data" using ($7==TT&&$10==BIAS+1.0&&$8==EB&&$9==ET?$6-0.03:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "+1.0 V" ,\
		"scan.data" using ($7==TT&&$10==BIAS+1.5&&$8==EB&&$9==ET?$6-0.01:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "+1.5 V" ,\
		"scan.data" using ($7==TT&&$10==BIAS+2.0&&$8==EB&&$9==ET?$6+0.01:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "+2.0 V" ,\
		"scan.data" using ($7==TT&&$10==BIAS+2.5&&$8==EB&&$9==ET?$6+0.03:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "+2.5 V" ,\
		"scan.data" using ($7==TT&&$10==BIAS+3.0&&$8==EB&&$9==ET?$6+0.05:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "+3.0 V" ,\
		"scan.data" using ($7==TT&&$10==BIAS+3.5&&$8==EB&&$9==ET?$6+0.07:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "+3.5 V" ,\
		"scan.data" using ($7==TT&&$10==BIAS+4.0&&$8==EB&&$9==ET?$6+0.09:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "+4.0 V" 
}







############# CTR vs TBIAS (TTHRES) with fixed BIAS ############################
set xlabel "TBIAS"
set ylabel "ctr FWHM [ps]"
set xrange [-0.5:15.5]
set x2range [-0.5:15.5]
set yrange [180:350]
set mxtics 1
set xtics 0,1 scale 0
set x2tics -0.5,1 mirror format ""
set grid x2tics ytics
set key outside Right

do for [B=-5:40:5] {
	set output "plots/scan_".PREFIX."_ctr_tbias_BIAS_".B.".eps"
	set title "TBias Scan (EDAC ".EB."-".ET.", BIAS +".(B/10).".".abs(B-(B/10)*10)."V)"
	plot "scan.data" using ($10==BIAS+B/10&&$7==0&&$8==13&&$9==ET?$6-0.15:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT  0" ,\
		"scan.data" using ($10==BIAS+B/10.0&&$7==1&&$8==EB&&$9==ET?$6-0.13:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT  1",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==2&&$8==EB&&$9==ET?$6-0.11:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT  2",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==3&&$8==EB&&$9==ET?$6-0.09:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT  3",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==4&&$8==EB&&$9==ET?$6-0.07:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT  4",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==5&&$8==EB&&$9==ET?$6-0.05:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT  5",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==6&&$8==EB&&$9==ET?$6-0.03:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT  6",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==7&&$8==EB&&$9==ET?$6-0.01:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT  7",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==8&&$8==EB&&$9==ET?$6+0.01:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT  8",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==9&&$8==EB&&$9==ET?$6+0.03:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT  9",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==10&&$8==EB&&$9==ET?$6+0.05:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT 10",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==11&&$8==EB&&$9==ET?$6+0.07:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT 11",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==12&&$8==EB&&$9==ET?$6+0.09:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT 12",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==13&&$8==EB&&$9==ET?$6+0.11:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT 13",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==14&&$8==EB&&$9==ET?$6+0.13:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT 14",\
		"scan.data" using ($10==BIAS+B/10.0&&$7==15&&$8==EB&&$9==ET?$6+0.15:1/0):($11*2.35*50):($12*2.35*50) with yerrorbars title "TT 15"
}

