#ifndef ETHZSTICANA_H
#define ETHZSTICANA_H

#include <map>
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TRandom3.h"

#include "events.h"

typedef std::pair<unsigned int, unsigned int> chPair;
typedef std::pair<double, double> fitValues;

class ethzSticAna {
public:
    ethzSticAna(const char * fname);

    /*!
     * \brief loadEnergy populates energy histograms for all present channels and sets propriate energy cut values
     * \param energyMin minimum energy to find photo peak
     * \return number channels with entries
     * If TOFPET data is used, it stores all times for later sorting in the same loop
     */
    int loadEnergy(double energyMin = 150);

    /*!
     * \brief loadDNLTable creates dnl look-up table to calculate the realTime
     * Only in use for STiC data
     */
    void loadDNLTable();
    void loadDNLTableAlex();
    void loadNoDNLTable();

    /*!
     * \brief buildCoinc builds all coincidences within timewindow on a frame base approche
     * \param realtime = true, if true dnl correction is applied (only STiC data)
     * \return total number (summed over all channels) of all found coincidences
     * Run after energies are loaded with loadEnergy()
     */
    int buildCoinc(bool discardMulti = false, bool realtime = true);

    /*!
     * \brief addAllPairs addes all pairs with present data to coincPairs. Combinations between channels [0, CHS/2] and [CHS/2, CHS]
     * \return total number of added pairs
     */
    int addAllPairs();


    /*!
     * \brief getCTRs performes CTR fits
     * Run after coincidences are built
     */
    void getCTRs();

    /*!
     * \brief writeHistos save histograms to currently open file
     * \param all = false, write all histograms (at the moment: also single energy histos)
     */
    void writeHistos(bool all = false);

    /*!
     * \brief writeChiaraPlots generates a canvas with the std plots from Chiaras analysis
     * \param statCut lower limit for statistics
     * \param nicer changed naming
     * Run after coincidences are built and calculated ctrs
     */
    void writeChiaraPlots(int statCut = 300, bool nicer = true);

    /*!
     * \brief writeOverview generates a ctr overview
     * \param statCut lower limit for statistics
     * \param chi2Cut upper limit for chi2
     * Run after coincidences are built and calculated ctrs
     */
    void writeOverview(int statCut = 300, int chi2Cut = 2);

    void checkDNLTable();

protected:
    /*!
     * \brief setEnergyCuts uses the populated energy histograms to determine the appropiate energy cuts per present channel
     * \param energyMin minimum energy to look at to find photo peak
     * Used at the end of loadEnergy
     */
    void setEnergyCuts(double energyMin);

    /*!
     * \brief getCTR performes ctr fit of given channel pair
     * \param pair
     * \param [mean] used to store the mean position of the deltaT
     * \param [chi2] used to store the chi2 of the deltaT fit
     * \return sigma ctr in ps (value, error)
     */
    fitValues getCTR(chPair pair, fitValues * mean = nullptr, double * chi2 = nullptr);
    fitValues getMean(chPair pair);             ///< Returns mean of deltaT peak, it uses getCTR(pair, &mean) and returns mean

    /*!
     * \brief getRealTime returns the real time (dnl corrected) of an event
     * \param event
     * \param uncertainty = (fine counter bind width)/sqrt(12)
     * \return real time of event
     * DNLTable has to be loaded with loadDNLTable()
     */
    double getRealTime(stic_data_e * event, double * uncertainty = nullptr);

private:

    // Settinngs
    const double       energySigmas = 1.5;              ///< used energy range, HD 1.5, Chiara: 1.0
    const double       ctrFitSigmas = 5;                ///< range for ctr fit, second step to determine chi2/ndf

    // STiC
    unsigned int CHS = 64;                              ///< amixmal number of channels
    const float fineBinWidth = 50.201;                  ///< in ps, locked PLL at 622.5 -> 50.2ps
    unsigned int energyMax = 800;                       ///< upper edge of energy histos in bins [1.6ns]
    //unsigned int energyMin = 150;                       ///< minimum position of photon peak [1.6ns], needed if Ethresh is very, very lowz

    // TOFPET
    bool _isTOFPET = false;                             ///< Indicates if loaded file is a TOFPET file
    const unsigned int CHSTOFPET = 256;                 ///< mixmal number of channels in TOFPET configuration (ASIC2 starts at 64?)
    const unsigned int energyMaxTOFPET = 500;           ///< upper edge of TOFPET energy histos in bins [ns]
    //const unsigned int energyMinTOFPET = energyMin;     ///< minimum position of TOFPET photon peak [1ns]
    const Long64_t frameLength = 6.4 * 1e6;
    //const double frameLength = 1024 * 32 * fineBinWidth;    ///< TOFPET frame length in ps 1024 * coarsCounter in code
    Long64_t * _times;                                  ///< array to store all times from TOFPET for global sorting, not loaded if file ends with *sort.root
    bool needSorting();

    int buildCoincTOFPET(bool discardMulti = false);                             ///< methode to fine coincidences in TOFPET files, is called by buildCoincidences
    int loadEnergyTOFPET(double energyMin);                             ///< methode to load TOFPET energies and set cuts, called by loadEnergy()
    TTree * sortTOFPET();                               ///< methode for global sorting of TOFPET data if times are already stored in _times (loadEnergyTOFPET), if needed (fname doesnt end with sort.root) called by buildCoincTOFPET

    const unsigned int coincBuilderFrames = 0;          ///< in 2^coincBuilderFrames frames coincidences are searched for
    const unsigned int ctrBinNumber = 401;              ///< number of bins used in ctr histos range: (-/+ 0.5 * ctrBinNumber * fineBinWidth )
    double coincWindow;                                 ///< Coincidence window in ns

    // Histo Stuff
    TH1I * getChannelsHisto();                          ///< Histogram used to find present channels
    void initDeltaTHistos();                            ///< init ctr (deltaT) histos
    void initDeltaTPBHistos();
    int initEnergyHistos();                             ///< init energy histos
    std::map<unsigned int, TH1F *> _hEnergy;            ///< Energy histograms per channel
    std::map<chPair, TH1F *> _hDeltaT;                  ///< DeltaT histograms
    std::map<chPair, std::vector<TH1F *>> _hDeltaT_pb;
    TH1F * hCoincStatistics;                            ///< Histogram used to store coincidence statistics per pair
    TH1F * hMultiplicity;
    TH1F * hGlobalMultiplicity;
    unsigned int getCoincStat(chPair pair);             ///< returns number of coincidences of given pair
    unsigned int getPairId(chPair pair);                ///< get pairID for a given pair


    // Storage for easier and faster access
    std::map<chPair, fitValues> _ctr;                   ///< store ctr value of pair
    std::map<chPair, fitValues> _mean;                  ///< store ctr mean position of pair
    fitValues _photoPeak(unsigned int ch);              ///< function with similar behavior as _ctr, _mean for energies: returns photopeak position of channel
    std::map<chPair, double> _chi2;

    // Saving
    void writeEnergyHistos(bool all = false, bool stat = true);   ///< write energy histograms to disk. if all, write single histos
    void writeDeltaTHistos(bool perpin = false);                                     ///< write ctr (deltaT) histos to disk

    // DNL
    std::map<unsigned int, double[33]> _DNLtable;                 ///< dnl look-up table to calculate the real time of events
    std::map<unsigned int, double[33]> _uncertaintyTable;         ///< look-up table fine counter uncertainty (bin width)/sqrt(12))

    // Files
    const char * _fname;                                ///< input file name
    TFile * _ifile;                                     ///< input file
    TTree * _itree;                                     ///< input tree

    // Cuts
    std::map<unsigned int, std::pair<int, int>> _eCuts;     ///< energy cuts
    void addPair(unsigned int ch1, unsigned int ch2);
    std::vector<chPair> _coincPairs;                        ///< used coincidence pairs
    chPair _minPairId = std::make_pair(-1, -1);

    // Flaggs
    inline bool _energyLoadeded() {if(_hEnergy.size() > 0) return true; else false;}    ///< indicator if energies are loaded
    inline bool _coincBuilt() {if(_hDeltaT.size() > 0) return true; else false;}        ///< indicator if coincidences are built
    inline bool _ctrBuilt() {if(_ctr.size() > 0) return true; else false;}              ///< indicator if ctrs are calculated
    inline bool _dnlLoaded() {if(_DNLtable.size() > 0) return true; else false;}        ///< indicator if dnl table is present

      TRandom3 generator;
};

#endif // ETHZSTICANA_H
