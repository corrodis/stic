#ifndef EVENT_TYPE_E__
#define EVENT_TYPE_E__

#include "EventType.h"
#include <iostream>

class stic_data_e : public stic3_data_t
{
public: 
	Int_t number;

	//ClassDef(stic_data_e,1);

	stic_data_e();

	void printHeader(std::ostream &st = std::cout);
	void print(const char* delimiter = "\t",std::ostream &st = std::cout);
	
	bool eq(stic_data_e * event);

	Int_t getEnergy();
	Int_t getTcc();
	Int_t getTfine();
    Int_t getTime();
	
};

#endif
