library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;


entity tb_encoder is
end entity tb_encoder;




architecture a  of tb_encoder is



component encoder_module  is			--{{{
	port (
				i_clk_s	: in std_logic;		--SLOW CLOCK
				i_rst	: in std_logic;		--ASYNC RESET
				i_byte	: in std_logic_vector(7 downto 0);
				i_k		: in std_logic;
				o_enc_ready : out std_logic;
				o_enc_data : out std_logic_vector(9 downto 0)
		 );
	end component encoder_module ;		--}}}


signal rst,k :std_logic;
signal clock :std_logic := '0';
signal enc_ready : std_logic;
signal i_byte : std_logic_vector(7 downto 0);
signal enc_data : std_logic_vector(9 downto 0);

begin

	clock <= not clock after 31.25 ns;

	enc_mod: encoder_module
	port map(
				i_clk_s => clock,
				i_rst => rst,
				i_byte => i_byte,
				i_k => k,
				o_enc_data => enc_data,
				o_enc_ready => enc_ready
			);


	sim : process
	begin
		rst <= '1';
		i_byte <= conv_std_logic_vector(28,8);	--DURING RESET KEEP THE IDLE BYTE FOR CONVERSION
		k <= '1';
		wait until rising_edge(clock);
		rst <= '0';
		wait until enc_ready='1';
		wait until rising_edge(clock);
		wait for 6 ns;
		k<= '0';
		i_byte <= conv_std_logic_vector(16,8);
		wait until rising_edge(clock);
		wait for 6 ns;
		i_byte <= conv_std_logic_vector(117,8);
		wait until rising_edge(clock);
		wait for 6 ns;
		i_byte <= conv_std_logic_vector(19,8);
		wait until rising_edge(clock);
		wait for 6 ns;
		i_byte <= conv_std_logic_vector(215,8);
		wait until rising_edge(clock);
		wait for 6 ns;
		i_byte <= (others => '0');
		wait;



	end process sim;
end architecture a ;	
