onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_encoder/rst
add wave -noupdate /tb_encoder/k
add wave -noupdate /tb_encoder/JO
add wave -noupdate /tb_encoder/IO
add wave -noupdate /tb_encoder/i_byte
add wave -noupdate /tb_encoder/HO
add wave -noupdate /tb_encoder/GO
add wave -noupdate /tb_encoder/FO
add wave -noupdate /tb_encoder/EO
add wave -noupdate /tb_encoder/enc_data
add wave -noupdate /tb_encoder/DO
add wave -noupdate /tb_encoder/CO
add wave -noupdate /tb_encoder/clock
add wave -noupdate /tb_encoder/BO
add wave -noupdate /tb_encoder/AO
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {109 ns} 0}
configure wave -namecolwidth 184
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {105 ns}
