--=============================================================
--
--encoder_module.vhdl 
--controls the 8b/10b encoder module
--data generation is sync to a byte clock with the freq of 160MHz/10
--enable and reset are also handled here
--if the module is disabled (en = '0') the 8b/10b encoder won't receive a 
--clock signal and the running disparity won't change
--
--Version Date: 10.06.2011
--
--Author: Tobias Harion
--
--
--	|HEADER IDENTIFIER|FRAMECOUNTER|DATA...|TRAIL IDENTIFIER|NUMBER OF HITS|
--
--=============================================================




library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;



entity encoder_module  is			--{{{
	port (
				i_clk_s	: in std_logic;		--SLOW CLOCK
				i_rst	: in std_logic;		--ASYNC RESET
				i_byte	: in std_logic_vector(7 downto 0);
				i_k		: in std_logic;
				o_enc_ready : out std_logic;
				o_enc_data : out std_logic_vector(9 downto 0)
		 );
	end entity encoder_module ;		--}}}



architecture a  of encoder_module  is	--{{{1


	component enc_8b10b is 			--{{{2
		port(
			RESET : in std_logic ;		-- Global asynchronous reset (active high) 
			SBYTECLK : in std_logic ;	-- Master synchronous send byte clock
			KI : in std_logic ;			-- Control (K) input(active high)
			AI, BI, CI, DI, EI, FI, GI, HI : in std_logic ;	-- Unencoded input data
			JO, HO, GO, FO, IO, EO, DO, CO, BO, AO : out std_logic 	-- Encoded out 
			);
	end component enc_8b10b; --}}}


--SIGNAL DECLARATIONS --{{{2

	signal eb_rst : std_logic;
	signal next_eb_rst : std_logic;
	signal next_o_enc_ready : std_logic;


	--THE TWO RESET STATES GET THE ENCODER INTO A RECEPTABLE STATE
	type enc_state is (S_RESET0,S_WAIT_RESET,S_RUNNING);		--THE STATES OF THE ENCODER
	signal present_state, next_state : enc_state;	

	--}}}



begin


	enc: enc_8b10b	-- 8b/10b ENCODER DATA IS VALID AFTER ONE RISING + FALLING EDGE CLOCK CYCLE DATA IS INVALID AFTER THE RISING EDGE{{{2
	port map(
				RESET => eb_rst,
				SBYTECLK => i_clk_s,
				KI => i_k,
				AI => i_byte(0),
				BI => i_byte(1),
				CI => i_byte(2),
				DI => i_byte(3),
				EI => i_byte(4),
				FI => i_byte(5),
				GI => i_byte(6),
				HI => i_byte(7),

				JO => o_enc_data(9),
				HO => o_enc_data(8),
				GO => o_enc_data(7),
				FO => o_enc_data(6),
				IO => o_enc_data(5),
				EO => o_enc_data(4),
				DO => o_enc_data(3),
				CO => o_enc_data(2),
				BO => o_enc_data(1),
				AO => o_enc_data(0)

			); 						--}}}


	e_syn_sm : process (i_clk_s,i_rst)		--{{{2
	begin
		if rising_edge(i_clk_s) then
			if i_rst = '1' then

				present_state <= S_RESET0;
				o_enc_ready <= '0';
				eb_rst <= '1';
			else
				present_state <= next_state;
				eb_rst <= next_eb_rst;
				o_enc_ready <= next_o_enc_ready;
			end if;

		end if;								
	end process e_syn_sm;--}}}

	e_comb_sm : process (present_state,i_rst)
	begin

		next_o_enc_ready <= '0';
		next_state <= present_state;
		next_eb_rst <= eb_rst;	--TO AVOID LATCHES

		case present_state is
			when S_RESET0 =>
				next_eb_rst <= '1';
				if i_rst = '0' then
					next_eb_rst <= '0';
					next_state <= S_WAIT_RESET;
				end if;
			when S_WAIT_RESET =>
				next_eb_rst <= '0';
				next_state <= S_RUNNING;

			when S_RUNNING => 
				next_o_enc_ready <= '1';

			when others => null;
		end case;
	end process e_comb_sm;



End architecture a ;	--}}}
