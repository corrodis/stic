Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity eval_cmd is
	port(
			i_sys_clk : in std_logic;
			i_rst : in std_logic;

			--FIFO EMULATION SIGNALS
			o_fifo_data : out std_logic_vector(47 downto 0);
			o_fifo_empty : out std_logic;
			o_fifo_count : out std_logic_vector(6 downto 0);

			--control signals
			o_start_trans : out std_logic;
			o_gen_idle : out std_logic;


			--UART INTERFACE SIGNALS
			i_uart_rx : in std_logic
	    );
end eval_cmd;




architecture behaviour of eval_cmd is

	component r_data is --{{{
	port(
		data : out std_logic_vector(7 downto 0);
	--USES 160 MHZ CLOCK INPUT
		clk : in std_logic;
		rst_n: in std_logic;
		d_ready : out std_logic;
		RX_BIT : in std_logic
	);
	end component; --}}}


	signal s_uart_data : std_logic_vector(7 downto 0);
	signal s_rst_n,n_rst_n : std_logic;
	signal s_init_trans, n_init_trans : std_logic;
	signal s_dready : std_logic;

	signal s_fifo_count, n_fifo_count : std_logic_vector(6 downto 0);


	type FSM_STATES is (S_IDLE, S_EVAL_CMD,S_INIT_TRANSMISSION);
	signal p_state, n_state : FSM_STATES;

begin


	--THE EMULATED FIFO HAS ALWAYS DATA
	o_fifo_empty <= '0';
	o_fifo_count <= s_fifo_count;
	--FIXED DATA FOR THE TIME BEING
	o_fifo_data <= X"00000a0a0a0a";

	o_gen_idle <= '0';
	o_start_trans <= s_init_trans;

	u_uart_rdata : r_data --{{{
	port map(
				data => s_uart_data,
				clk => i_sys_clk,
				rst_n => s_rst_n,
				d_ready => s_dready,
				RX_BIT => i_uart_rx
			); --}}}


	p_comb : process (s_uart_data,s_dready,p_state,s_fifo_count) --{{{
	begin
		n_fifo_count <= s_fifo_count;
		n_rst_n <= '1';
		n_init_trans <= '0';

		case p_state is
			when S_IDLE =>
				if s_dready = '1' then
					n_state <= S_EVAL_CMD;
				end if;
			when S_EVAL_CMD =>
				n_fifo_count <= s_uart_data(6 downto 0);
				n_state <= S_INIT_TRANSMISSION;

			when S_INIT_TRANSMISSION =>
				n_rst_n <= '0';
				n_init_trans <= '1';
				n_state <= S_IDLE;

			when others => null;
		end case;

	end process p_comb; --}}}


	p_sync : process (i_rst,i_sys_clk)
	begin
		if rising_edge(i_sys_clk) then

			if i_rst = '1' then
				s_rst_n <= '0';
				s_init_trans <= '0';
				p_state <= S_IDLE;
				s_fifo_count <= (others => '0');
			else
				s_rst_n <= n_rst_n;
				s_init_trans <= n_init_trans;
				s_fifo_count <= n_fifo_count;
				p_state <= n_state;
			end if;

		end if;
	end process p_sync;


end;
