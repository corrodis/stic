--=============================================================
--
--clock_divider.vhdl 
--
--Divide the input clock by a given number 
--
--
--
--Version Date: 10.06.2011
--
--Author: Tobias Harion
--Kirchhoff-Institut Heidelberg
--
--
--
--=============================================================


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity clock_divider is	--{{{
	generic(Nd : Integer := 10);
PORT(
		i_clk 		  : IN  STD_LOGIC;
		o_div_clk_delay : out std_logic;
        o_div_clk     : OUT STD_LOGIC);
end clock_divider;	--}}}

architecture counter of clock_divider is --{{{1

signal q,next_q  : STD_LOGIC;
signal q_delay,next_q_delay : STD_LOGIC;
signal decrement : std_logic_vector(3 downto 0);
signal cnt : std_logic_vector(3 downto 0);

begin

	build_decrement : process (cnt,q,q_delay)	--{{{2
	begin
		if cnt = "0000" then
			decrement <= conv_std_logic_vector(Nd-1,4);
		else
			decrement <= conv_std_logic_vector(conv_integer(unsigned(cnt))-1,4);
		end if;

		next_q_delay <= q_delay;
		next_q <= q;
		
				--GENERATE THE DELAYED SIGNAL
		if conv_integer(unsigned(cnt)) = ND - 1 - 1 then
			next_q_delay <= '0';
		end if;
		if conv_integer(unsigned(cnt)) = ND/2 - 2 then
			next_q_delay <= '1';
		end if;

			--GENERATE THE NORMAL CLOCK SIGNAL 
		if conv_integer(unsigned(cnt))=0 then
			next_q <= '0';
		end if;
		if conv_integer(unsigned(cnt)) = Nd/2 then
			next_q <= '1';
		end if;

	end process build_decrement;	--}}}


divN: process(i_clk,decrement,next_q,next_q_delay) -- frequency divider	--{{{2
    begin
		if rising_edge(i_clk) then
			q <= next_q;
			q_delay <= next_q_delay;
			cnt <= decrement;
		end if;
    end process;	--}}}

o_div_clk <= q;
o_div_clk_delay <= q_delay;

end architecture counter;	--}}}
