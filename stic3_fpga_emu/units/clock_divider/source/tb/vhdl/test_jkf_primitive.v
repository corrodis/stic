///////////////////////////////////////////////
// Cell Name : jkffrsb_udp
// Cell Type : Primitive
// Version   : 1.3
// Date      : 11/02 2001
// Reason    :
//      1.3  : modify protection method.
//      1.2  : Deal with unknown ck.
//      1.1  : Initial.
///////////////////////////////////////////////


module JKFN(Q, J, K, SB, RB, CK);
   reg flag; // Notifier flag
   output Q;
   input J, K, CK, RB, SB;


//Function Block
   jkffrsb_udp g2(Q,  J,  K,  CK,  RB,  SB,  flag );
endmodule



primitive   jkffrsb_udp (q, j, k, ck, rb, sb, flag);

// JK FLIP FLOP, WITH RB/SB /STANDARD DRIVE
   output q;
   reg q;
   input j,k,ck,rb,sb,flag;


`protect
   table
//     J     K    CK    RB    SB  FLAG :   Qtn : Qtn+1
       0     0     r     1     1     ? :     ? :     -;// Output retains the
                                                       // current state if both
                                                       // J and K are 0.
       0     1     r     ?     1     ? :     ? :     0;// Clocked J & K.
       1     0     r     1     ?     ? :     ? :     1;
       1     1     r     1     ?     ? :     0 :     1;// Clocked toggle.
       1     1     r     ?     1     ? :     1 :     0;

       1     x     r     1     1     ? :     0 :     1;// toggle previous one
       x     1     r     1     1     ? :     1 :     0;// toggle previous one
       ?     1     r     x     1     ? :     1 :     0;//pessimism
       1     ?     r     1     x     ? :     0 :     1;

       ?     0     r     1     ?     ? :     1 :     1;// conflict statements
       0     ?     r     ?     1     ? :     0 :     0;// conflict statement

       ?     ?     f     ?     ?     ? :     ? :     -;// Output state is
						       // insensitive to the
						       // falling edge of
						       // the clock.
       0     0  (x1)     1     1     ? :     ? :     -;// possible clocked JK
       0     ?  (x1)     ?     1     ? :     0 :     0;
       ?     0  (x1)     1     ?     ? :     1 :     1;

       0     0  (?x)     1     1     ? :     ? :     -;
       0     ?  (?x)     ?     1     ? :     0 :     0;
       ?     0  (?x)     1     ?     ? :     1 :     1;

       0     ?     ?     x     1     ? :     0 :     0;//pessimism
       ?     0     ?     1     x     ? :     1 :     1;//pessimism

       // deal with clk "x"
       *     ?     b     1     1     ? :     ? :     -;
       *     0     x     1     1     ? :     1 :     1;
       ?     *     b     1     1     ? :     ? :     -;
       0     *     x     1     1     ? :     0 :     0;
						       
       ?     ?     ?     0     ?     ? :     ? :     0;// Clear.
       ?     ?     ?     1     0     ? :     ? :     1;// Set.

       ?     ?  (?0)     1     1     ? :     ? :     -;// ignore falling clock.

       // deal with clk "x"
       ?     ?     b  (?1)     1     ? :     ? :     -;
       0     0     x  (?1)     1     ? :     ? :     -;
       0     ?     x  (?1)     1     ? :     0 :     0;
       ?     ?     b     1  (?1)     ? :     ? :     -;
       0     0     x     1  (?1)     ? :     ? :     -;
       ?     0     x     1  (?1)     ? :     1 :     1;

       ?     ?     ?     ?     ?     * :     ? :     x;
   endtable
`endprotect
endprimitive

