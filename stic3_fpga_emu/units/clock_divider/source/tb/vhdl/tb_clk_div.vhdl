--small testbench for the clock divider

LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity tb_clk_div is
end entity tb_clk_div;




architecture a  of tb_clk_div is


	component clock_divider is
	GENERIC (Nd : Integer := 50);
	PORT(
			i_clk 		  : IN  STD_LOGIC;
			o_div_clk_delay : out std_logic;
			o_div_clk     : OUT STD_LOGIC);
	end component clock_divider;

	signal o_clk,o_clk_del : std_logic;	
	signal clk : std_logic := '0';

begin

	clk <= not clk after 3.125 ns;

	dut: clock_divider
	generic map(Nd => 10)
	port map(
				i_clk => clk,
				o_div_clk_delay =>  o_clk_del,
				o_div_clk => o_clk
			);

	sim : process
	begin
		wait;
	end process sim;

end architecture a ;