
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity test_jkfn is
	end entity test_jkfn;

architecture a  of test_jkfn is

	component JKFN 
		port(Q : out std_logic;
			 RB : in std_logic;
			 SB : in std_logic;
			 J : in std_logic;
			 K : in std_logic;
			 CK: in std_logic);
	end component ;


	signal q	: std_logic;
	signal clk : std_logic:='0';
	signal j,k: std_logic;
	

begin

	clk <= not clk after 10 ns;

	dut: JKFN
	port map(
		Q => q,
		RB => '1',
		SB => '1',
		J => j,
		K => k,
		CK => clk
	);

	sim : process
	begin
		j <= '1';
		k <= '1';
		wait until falling_edge(clk);
		wait until falling_edge(clk);
		j <= '1';
		k <= '1';
		wait until falling_edge(clk);
		wait until falling_edge(clk);
		j <= '1';
		k <= '1';
		wait;
	end process sim;

end architecture a ;	
