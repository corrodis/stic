Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity tb_stic3_emu is
end tb_stic3_emu;




architecture a of tb_stic3_emu is


	component stic3_emu_top is
		port(
				--INPUT CLOCK SIGNAL 160 MHz
				i_sys_clk : in std_logic;
				--RESET SIGNAL FROM THE DAQ
				i_rst : in std_logic;

				o_serial_txd : out std_logic;


				--UART INTERCONNECTION
	--			o_uart_tx : out std_logic;
				i_uart_rx : in std_logic
			);
	end component stic3_emu_top;

	component s_data is --{{{
	port(
		data : in std_logic_vector(7 downto 0);
		clk : in std_logic;
		rst_n: in std_logic;
		send : in std_logic;
		d_ready : out std_logic;
		TX_BIT : out std_logic
	);
	end component s_data; --}}}



	signal s_clk : std_logic := '0';
	signal s_rst : std_logic;
	signal s_serial_txd : std_logic;

	--UART SIGNALS
	signal s_send : std_logic;
	signal s_dready : std_logic;
	signal s_uart_rx : std_logic;
	signal s_rst_n : std_logic;

	signal s_uartdata : std_logic_vector(7 downto 0);


begin

	s_clk <= not s_clk after 6.25 ns;	--160 MHz system clock

	uut : stic3_emu_top
	port map(
				i_sys_clk => s_clk,
				i_rst => s_rst,
				o_serial_txd => s_serial_txd,
				i_uart_rx => s_uart_rx
			);


	u_uart_send : s_data
	port map(
				data => s_uartdata,
				clk => s_clk,
				rst_n => s_rst_n,
				send => s_send,
				d_ready => s_dready,
				TX_BIT => s_uart_rx
			);



	stim : process
	begin
		s_rst <= '1';
		s_rst_n <= '0';
		s_send <= '0';

		wait for 400 ns;
		s_rst <= '0';
		s_rst_n <= '1';

		s_uartdata <= X"03";
		wait until rising_edge(s_clk);
		wait until rising_edge(s_clk);
		wait until rising_edge(s_clk);
		s_send <= '1';
		wait until s_dready = '1';
		s_send <= '0';
		s_rst_n <= '0';
		wait until rising_edge(s_clk);
		s_rst_n <= '1';


		wait for 10 us;
		s_uartdata <= X"01";
		wait until rising_edge(s_clk);
		wait until rising_edge(s_clk);
		wait until rising_edge(s_clk);
		s_send <= '1';
		wait until s_dready = '1';
		s_send <= '0';
		wait until rising_edge(s_clk);


		wait;

	end process;

end;

