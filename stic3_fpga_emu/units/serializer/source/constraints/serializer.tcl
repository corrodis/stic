create_clock -name "i_clk" -period 6.25 -waveform { 3.125 6.25  }  { i_clk }
create_clock -name "i_clk_s_d" -period 62.5 -waveform { 31.25 62.5  }  { i_clk_s_d }
