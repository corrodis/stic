--=============================================================
--
--serializer.vhdl
--ENCODING AND TRANSMISSION OF THE FRAMEDATA OVER A SERIAL LINE
--
--
--Version Date: 07.06.2011
--
--Author: Tobias Harion
--
--
--	|HEADER IDENTIFIER|FRAMECOUNTER|DATA...|TRAIL IDENTIFIER|NUMBER OF HITS|
--
--=============================================================




library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;


entity serializer is		--{{{
	generic(N_bits : Integer := 36);
	port (

			i_clk		: in std_logic;					--SYSTEM CLOCK
			i_rst		: in std_logic;
			i_clk_s_d	: in std_logic;



			--SIGNALS FROM AND TO THE FRAME GENERATOR
			i_ser_data		: in std_logic_vector(9 downto 0);

			--DATA LINK OUTPUT SIGNAL
			o_serial_TXD		: out std_logic

		 );
	end entity serializer; --}}}



architecture multiplexed of serializer is	--{{{1

	--SIGNAL DECLARATIONS		--{{{2

	signal trans_data : std_logic_vector(9 downto 0);		--REGISTER FOR THE DATA TO BE TRANSMITTED
	signal shift_reg : std_logic_vector(9 downto 0);		--SHIFT REGISTER IN THE NEW CLOCK DOMAIN

	signal trans_data_sampled : std_logic_vector(9 downto 0);

	--}}}


begin



	sampe_enc : process (i_clk_s_d,i_rst,i_ser_data)		--{{{2
		--SAMPLE THE ENCODED INPUT FOR THE DATA TRANSMISSION WITH THE FASTER CLOCK
	begin
		if falling_edge(i_clk_s_d) then
			if i_rst = '1' then
				trans_data <= (others => '0');
			else
				trans_data <= i_ser_data;
			end if;
		end if;
	end process sampe_enc; --}}}

	--sample the data for the clock domain crossing
	buff_transdata: process(i_clk)
	begin
		if rising_edge(i_clk) then
			trans_data_sampled <= trans_data;
		end if;
	end process;


	transmit_shift: process(i_clk)	--{{{2
		variable bit_cnt : integer range 0 to 9;
	begin
		if rising_edge(i_clk) then
			if i_rst = '1' then --SYNCHRONOUS RESET
				bit_cnt := 0;
				shift_reg <= (others => '0');
			else

				--PUT THE NEW DATA IN THE REGISTER OR SHIFT IT
				if bit_cnt = 1 then
					shift_reg <= trans_data_sampled;
				else
					shift_reg <= shift_reg(0) & shift_reg(shift_reg'high downto 1);
				end if;

				--COUNT UP AND ROLL OVER IF NEEDED
				if bit_cnt = 9 then
					bit_cnt := 0;
				else
					bit_cnt := bit_cnt+1;
				end if;

			end if;
		end if;
	end process; --}}}


	o_serial_TXD <= shift_reg(0);


end architecture multiplexed;		--}}}
