--=============================================================
--
--tb_fg_ds.vhdl
--testbench for frame generation in combination with the fifo buffer
--
--Version Date: 08.05.2011
--
--Author: Tobias Harion
--
--	sys_clk
--           .----------.         .----------.         .----------.         .----------.
--  _________|          |_________|          |_________|          |_________|          |





--=============================================================

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.std_logic_arith.all;


entity tb_serializer is	--{{{
	generic(N_bits : Integer := 36);
end entity tb_serializer;	--}}}


architecture a  of tb_serializer is	--{{{1

component data_storing is		--{{{2
	port (

			--SYSTEM CLOCK 160MHz
			i_sys_clk	: in std_logic;

			i_rst		: in std_logic;

			--DATA FROM THE TDC/QDC STIC CHANNELS
			i_ch0_data 	: in std_logic_vector(31 downto 0);
			i_ch1_data 	: in std_logic_vector(31 downto 0);
			i_ch2_data 	: in std_logic_vector(31 downto 0);
			i_ch3_data 	: in std_logic_vector(31 downto 0);
			i_ch4_data	: in std_logic_vector(31 downto 0);
			i_ch5_data	: in std_logic_vector(31 downto 0);
			i_ch6_data	: in std_logic_vector(31 downto 0);
			i_ch7_data	: in std_logic_vector(31 downto 0);
			i_ch8_data	: in std_logic_vector(31 downto 0);
			i_ch9_data	: in std_logic_vector(31 downto 0);
			i_ch10_data	: in std_logic_vector(31 downto 0);
			i_ch11_data	: in std_logic_vector(31 downto 0);

			--DATA READY SIGNALS OF THE STIC CHANNELS
			i_ch_dready	: in std_logic_vector(11 downto 0);
			--MASKING OF CHANNELS
			i_channel_mask : in std_logic_vector(11 downto 0);
			--RESET SIGNAL TO THE STIC CHANNELS
			o_rst_channel : out std_logic_vector (11 downto 0);


		 	--EXTERNAL SIGNALS TO AND FROM THE FIFO
			i_fifo_rden	: in std_logic;
			o_fifo_data	: out std_logic_vector(35 downto 0);
			o_fifo_full	: out std_logic;
			o_fifo_count	: out std_logic_vector(5 downto 0);
			o_fifo_empty : out std_logic
		 );
end component data_storing; --}}}

	component frame_generator is	--{{{2
		generic(N_bits : Integer := 36);
		port (
				--SYSTEM SIGNALS
				i_clk				: in std_logic;
				i_rst				: in std_logic;									--RESET SIGNAL
				i_start_trans		: in std_logic;									--SIGNAL FROM DAQ TO START TRANSMISSION

				--FIFO SIGNALS
				o_fifo_rd_en		: out std_logic;								--READ ENABLE TO THE FIFO
				i_fifo_count		: in std_logic_vector(5 downto 0);				--NUMBER OF STORED HITS IN THE FIFO
				i_fifo_empty		: in std_logic;									--FIFO EMPTY SIGNAL
				i_fifo_data			: in std_logic_vector(N_bits - 1 downto 0);		--THE DATA OUTPUT FROM THE FIFO

				--CONFIGURATION SIGNALS
				i_gen_idle_sig		: in std_logic;


				-- SERIALIZER SIGNALS
				o_ser_data			: out std_logic_vector(9 downto 0);		--DATA TO THE SERIALIZER
				o_clk_s_d			: out std_logic


			 );
		end component frame_generator; --}}}

component serializer is		--{{{
	generic(N_bits : Integer := 36);
	port (

			i_clk		: in std_logic;					--SYSTEM CLOCK
			i_rst		: in std_logic;
			i_clk_s_d		: in std_logic;


			--SIGNALS FROM AND TO THE FRAME GENERATOR
			i_ser_data			: in std_logic_vector(9 downto 0);

			--DATA LINK OUTPUT SIGNAL
			o_serial_TXD		: out std_logic

		 );
	end component serializer; --}}}


--SIGNAL DECLARATIONS FRAMEGEN {{{2
			--SYSTEM SIGNALS
			signal start_trans		:  std_logic;									--SIGNAL FROM DAQ TO START TRANSMISSION

			-- SERIALIZER SIGNALS
			signal ser_data			:  std_logic_vector(9 downto 0);		--DATA TO THE SERIALIZER
			signal clk_d			: std_logic;							--DELAYED BYTE CLOCK
---}}}

--SIGNAL DECLARATIONS DATASTORING --{{{2
	signal clk : std_logic := '0';
	

	signal i_ch0_data 	:std_logic_vector(31 downto 0);
	signal i_ch1_data 	: std_logic_vector(31 downto 0);
	signal i_ch2_data 	: std_logic_vector(31 downto 0);
	signal i_ch3_data 	: std_logic_vector(31 downto 0);
	signal i_ch4_data	: std_logic_vector(31 downto 0);
	signal i_ch5_data	: std_logic_vector(31 downto 0);
	signal i_ch6_data	: std_logic_vector(31 downto 0);
	signal i_ch7_data	: std_logic_vector(31 downto 0);
	signal i_ch8_data	: std_logic_vector(31 downto 0);
	signal i_ch9_data	: std_logic_vector(31 downto 0);
	signal i_ch10_data	: std_logic_vector(31 downto 0);
	signal i_ch11_data	: std_logic_vector(31 downto 0);

	--DATA READY SIGNALS OF THE STIC CHANNELS
	signal i_ch_dready	: std_logic_vector(11 downto 0);
	--MASKING OF CHANNELS
	signal i_channel_mask: std_logic_vector(11 downto 0);
	--RESET SIGNAL TO THE STIC CHANNELS
	signal o_rst_channel : std_logic_vector (11 downto 0);


	--EXTERNAL SIGNALS TO AND FROM THE FIFO
	signal i_fifo_rden	:  std_logic;
	signal o_fifo_data	:  std_logic_vector(35 downto 0);
	signal o_fifo_full : std_logic;
	signal o_fifo_empty : std_logic;
	signal o_fifo_count : std_logic_vector(5 downto 0);

	signal i_rst	: std_logic;

	---}}}

--LOCALLY USED SIGNALS SO SIMULATE THE SERIALIZER --{{{2
	signal serial_TXD : std_logic; 
	--}}}

begin

--160MHz system clock
clk <= not clk after 3.125 ns;

dut1: data_storing	--{{{2
port map(
			--SYSTEM CLOCK 160MHz
			i_sys_clk => clk,
			

			i_rst => i_rst,

			--DATA FROM THE TDC/QDC STIC CHANNELS
			i_ch0_data => 	i_ch0_data,
			i_ch1_data => 	i_ch1_data,
			i_ch2_data => 	i_ch2_data,
			i_ch3_data => 	i_ch3_data,
			i_ch4_data => 	i_ch4_data,
			i_ch5_data => 	i_ch5_data,
			i_ch6_data => 	i_ch6_data,
			i_ch7_data => 	i_ch7_data,
			i_ch8_data => 	i_ch8_data,
			i_ch9_data => 	i_ch9_data,
			i_ch10_data => 	i_ch10_data,
			i_ch11_data => 	i_ch11_data,

			--DATA READY SIGNALS OF THE STIC CHANNELS
			i_ch_dready => 	i_ch_dready,
			--MASKING OF CHANNELS
			i_channel_mask =>  i_channel_mask,
			--RESET SIGNAL TO THE STIC CHANNELS
			o_rst_channel =>  o_rst_channel,
 

		 	--EXTERNAL SIGNALS TO AND FROM THE FIFO
			i_fifo_rden => 	i_fifo_rden,
			o_fifo_data => 	o_fifo_data,
			o_fifo_full => o_fifo_full,
			o_fifo_empty => o_fifo_empty,
			o_fifo_count => o_fifo_count

		);		--}}}

dut2: frame_generator	--{{{2
port map(
				--SYSTEM SIGNALS
				i_clk			=> 	clk,
				i_rst			=> 	i_rst,
				i_start_trans	=> 	start_trans,

				--FIFO SIGNALS
				o_fifo_rd_en	=> 	i_fifo_rden,
				i_fifo_count	=> 	o_fifo_count,
--				i_fifo_full		=> 	fifo_full,
				i_fifo_empty	=> 	o_fifo_empty,
				i_fifo_data		=> 	o_fifo_data,

				--CONFIGURATION SIGNALS
				i_gen_idle_sig	=> '0',	




				-- SERIALIZER SIGNALS
				o_ser_data		=> 	ser_data,
				o_clk_s_d		=>  clk_d
			
		);				--}}}

dut_ser: serializer		--{{{2
	port map(
				i_clk => clk,
				i_clk_s_d => clk_d,
				i_rst => i_rst,
				i_ser_data => ser_data,
				o_serial_TXD => serial_TXD
			);			--}}}



sim : process	--{{{2
begin
	i_ch0_data <= 	(others => '0');
	i_ch1_data <= conv_std_logic_vector(11111,32);	
	i_ch2_data <= conv_std_logic_vector(22222,32);	
	i_ch3_data <= conv_std_logic_vector(33333,32);	
	i_ch4_data <= conv_std_logic_vector(44444,32);	
	i_ch5_data <= conv_std_logic_vector(55555,32);	
	i_ch6_data <= 	(others => '0');
	i_ch7_data <= 	(others => '0');
	i_ch8_data <= 	(others => '0');
	i_ch9_data <= 	(others => '0');
	i_ch10_data <= 	(others => '0');
	i_ch11_data <= 	(others => '0');
	i_ch_dready <= (others => '0');
	i_channel_mask <= (others => '0');
	i_rst <= '1';
	wait for 50 ns;
	i_rst <= '0';
--	for i in 0 to 200 loop
		wait until rising_edge(clk);
		i_ch_dready <= (1=> '1',2=> '1',5 => '1' ,others=> '0');
		wait until o_rst_channel(1) = '1';
		i_ch_dready <= (2=> '1',5=> '1',others => '0');
		wait until o_rst_channel(2) = '1';
		i_ch_dready <= (3=> '1', 4=> '1', 5=> '1',others => '0');
		wait until o_rst_channel(3) = '1';
		i_ch_dready <= ( 4=> '1', 5=> '1',others => '0');
		wait until o_rst_channel(4) = '1';
		i_ch_dready <= ( 5=> '1',others => '0');
		wait until o_rst_channel(5) = '1';
		i_ch_dready <= (others => '0');
		wait until falling_edge(clk);
		wait for 300 ns;
		wait;
--	end loop;
--	wait until falling_edge(clk);
--	wait until falling_edge(clk);
--	i_rst <= '0';




end process sim;	--}}}

sim_fg : process --{{{2
begin
	start_trans <= '0';
	wait for 500 ns;	
	wait until rising_edge(clk);
	start_trans <= '1';
	wait for 50 ns;
	start_trans <= '0';
	wait for 2.5 us;
	start_trans <= '1';
	wait for 70 ns;
	start_trans <= '0';
	wait;

end process sim_fg; --}}}


end architecture a ;	--}}}
