--=============================================================
--
--tb_fg_ds.vhdl
--testbench for frame generation in combination with the fifo buffer
--
--Version Date: 08.05.2011
--
--Author: Tobias Harion
--
--	sys_clk
--           .----------.         .----------.         .----------.         .----------.
--  _________|          |_________|          |_________|          |_________|          |





--=============================================================

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.std_logic_arith.all;


entity tb_serializer is	--{{{
	generic(N_bits : Integer := 36);
end entity tb_serializer;	--}}}


architecture a  of tb_serializer is	--{{{1

component serializer is		--{{{
	generic(N_bits : Integer := 36);
	port (

			i_clk		: in std_logic;					--SYSTEM CLOCK
			i_rst		: in std_logic;


			--SIGNALS FROM AND TO THE FRAME GENERATOR
			i_byte			: in std_logic_vector(7 downto 0);
			i_serial_start	: in std_logic;
			i_k_signal 		: in std_logic;
			o_ack			: out std_logic;

			--DATA LINK OUTPUT SIGNAL

			o_serial_TXD		: out std_logic

		 );
	end component serializer; --}}}


--LOCALLY USED SIGNALS SO SIMULATE THE SERIALIZER --{{{2
	signal serial_TXD : std_logic; 
	signal clk : std_logic :='0';
	signal rst : std_logic;
	signal byte : std_logic_vector(7 downto 0);
	signal k : std_logic;
	signal ack : std_logic;
	signal serial_start : std_logic;
	--}}}

begin

--160MHz system clock
clk <= not clk after 3.125 ns;

dut_ser: serializer		--{{{2
	port map(
				i_clk => clk,
				i_rst => rst,
				i_byte => byte,
				i_serial_start => serial_start,
				i_k_signal => k,
				o_ack => ack,
				o_serial_TXD => serial_TXD
			);			--}}}



sim : process	--{{{2
begin

	rst <= '1';
	byte<=  (others => '0');
	serial_start <= '0';
	k <= '0';
	wait until rising_edge(clk);
	rst <= '0';
	wait until rising_edge(clk);
	wait until rising_edge(clk);
	wait until rising_edge(clk);
	wait until rising_edge(clk);
	wait until rising_edge(clk);
	wait until rising_edge(clk);
	

	wait;


end process sim;	--}}}



end architecture a ;	--}}}
