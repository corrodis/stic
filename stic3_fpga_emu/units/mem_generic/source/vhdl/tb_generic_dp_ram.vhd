Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity tb_generic_dp_ram is
end tb_generic_dp_ram;




architecture a of tb_generic_dp_ram is


	component generic_dp_ram is	--{{{
		generic(
				C_NUM_WORDS : INTEGER := 1024;
				C_ADDR_WIDTH : INTEGER := 12;
				C_DATA_WIDTH : INTEGER := 32
			);
		port(
				clka : in std_logic;										--WRITE CLOCK
				clkb : in std_logic;										--READ CLOCK
				addr_a : in std_logic_vector(C_ADDR_WIDTH-1 downto 0);		--ADDRESS ON PORT A
				addr_b : in std_logic_vector(C_ADDR_WIDTH-1 downto 0);		--ADDRESS ON PORT B

				din_a : in std_logic_vector(C_DATA_WIDTH-1 downto 0);		--INPUT DATA FOR WRITING
				dout_a : out std_logic_vector(C_DATA_WIDTH-1 downto 0);		--OUTPUT DATA ON PORT A
				dout_b : out std_logic_vector(C_DATA_WIDTH-1 downto 0);		--OUTPUT DATA FOR READING
				we : in std_logic											--WRITE ENABLE
			);
	end component;	--}}}


	constant ADDR_WIDTH : integer := 12;
	constant NUM_WORDS : integer := 1024;
	constant DATA_WIDTH : integer := 56;

	signal clka : std_logic := '0';										--WRITE CLOCK
	signal clkb : std_logic := '0';										--READ CLOCK
	signal addr_a : std_logic_vector(ADDR_WIDTH-1 downto 0);		--READ ADDRESS ON PORT A
	signal addr_b : std_logic_vector(ADDR_WIDTH-1 downto 0);		--READ ADDRESS ON PORT B

	signal din_a : std_logic_vector(DATA_WIDTH-1 downto 0);		--INPUT DATA FOR WRITING
	signal dout_a :std_logic_vector(DATA_WIDTH-1 downto 0);		--OUTPUT DATA ON PORT A
	signal dout_b :std_logic_vector(DATA_WIDTH-1 downto 0);		--OUTPUT DATA FOR READING
	signal we : std_logic;											--WRITE ENABLE


	constant clka_period : time := 10 ns;
	constant clkb_period : time := 14 ns;


begin

	--CREATE THE CLOCK SIGNALS WITH THE DEFINED PERIOD
	clka <= not clka after clka_period/2;
	clkb <= not clkb after clkb_period/2;



	dut : generic_dp_ram
	generic map(
					C_NUM_WORDS => NUM_WORDS,
					C_ADDR_WIDTH => ADDR_WIDTH,
					C_DATA_WIDTH => DATA_WIDTH
			   )
	port map(
				clka => clka,
				clkb => clkb,
				addr_a => addr_a,
				addr_b => addr_b,

				din_a => din_a,
				dout_a => dout_a,
				dout_b => dout_b,
				we => we
			);


	write_a: process
	begin
		addr_a <= (others => '0');
		we <= '0';
		din_a <= (others => '0');

		wait for 100 ns;
		wait until rising_edge(clka);

		--FILL THE MEMORY WITH DATA
		for i in 0 to NUM_WORDS-1 loop
			
			we <= '1';
			addr_a <= std_logic_vector(to_unsigned(i,ADDR_WIDTH));
			din_a <= std_logic_vector(to_unsigned(i,DATA_WIDTH));
			wait until rising_edge(clka);
			we <= '0';
			wait until rising_edge(clka);

		end loop;

	end process;


	read_b: process
	begin
		addr_b <= (others => '0');
		wait for 1 us;
		wait until rising_edge(clkb);

		for i in 0 to NUM_WORDS-1 loop
			addr_b <= std_logic_vector(to_unsigned(i,ADDR_WIDTH));
			wait until rising_edge(clkb);
			wait until rising_edge(clkb);
		end loop;

	end process;



end;
