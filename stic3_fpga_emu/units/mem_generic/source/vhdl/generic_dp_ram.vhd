-- GENERIC DUAL PORT MEMORY BLOCK WITH SYNCHRONOUS READ
-- DATA WIDTH CAN BE SUPPLIED USING THE GENERIC PARAMETER
--
-- SEQUENCE TO WRITE DATA D1 TO THE MEMORY AT ADDRESS R1
--
--           ___________        ___________
--CLK _______|          |_______|          |_________
--WEN        ___________________
--    _______|                  |________
--    _______ __________________ ________________
--DIN _______X_______D1_________X______D2________
--     ______ __________________ ________________
--ADDR ______X_______R1_________X______R2________
--     ______ __________________ ________________
--DOUT ______X_______XX_________X______D1________


Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity generic_dp_ram is
	generic(
			C_NUM_WORDS : INTEGER := 1024;
			C_ADDR_WIDTH : INTEGER := 12;
			C_DATA_WIDTH : INTEGER := 32
		);
	port(
			clka : in std_logic;										--WRITE CLOCK
			clkb : in std_logic;										--READ CLOCK
			addr_a : in std_logic_vector(C_ADDR_WIDTH-1 downto 0);		--ADDRESS ON PORT A
			addr_b : in std_logic_vector(C_ADDR_WIDTH-1 downto 0);		--ADDRESS ON PORT B

			din_a : in std_logic_vector(C_DATA_WIDTH-1 downto 0);		--INPUT DATA FOR WRITING
			dout_a : out std_logic_vector(C_DATA_WIDTH-1 downto 0);		--OUTPUT DATA ON PORT A
			dout_b : out std_logic_vector(C_DATA_WIDTH-1 downto 0);		--OUTPUT DATA FOR READING
			we : in std_logic											--WRITE ENABLE
	    );
end generic_dp_ram;



--IMPLEMENTATION ACCORDING TO THE XST USER GUIDE
architecture xst of generic_dp_ram is


	type MEM_RAM is array(C_NUM_WORDS-1 downto 0) of std_logic_vector(C_DATA_WIDTH-1 downto 0);
	signal ram : MEM_RAM;

	--THE WORDS ARE 
	signal mem_waddra, mem_raddra, mem_raddrb : std_logic_vector(C_ADDR_WIDTH-1 downto 0);			--READ AND WRITE ADDRESS OF THE WORD ADDRESSABLE MEMORY


begin



	write_proc: process(clka)
	begin
		if rising_edge(clka) then
			if we = '1' then
				ram(conv_integer(addr_a)) <= din_a;	--WRITE THE DATA TO THE MEMORY IF WRITE_ENABLE IS '1'
			end if;
			mem_raddra <= addr_a;				--TAKE OVER THE READ ADDRESS OF PORT A
		end if;
	end process;


	read_proc : process (clkb)	--SAMPLE THE BYTE READ ADDRESS WITH THE RISING EDGE OF THE DUAL CLOCK
	begin
		if rising_edge(clkb) then
			mem_raddrb <= addr_b;				--TAKE OVER THE READ ADDRESS OF PORT B
		end if;
	end process read_proc;

	dout_b <= ram(conv_integer(mem_raddrb));
	dout_a <= ram(conv_integer(mem_raddra));

end;

