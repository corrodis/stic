Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity generic_dp_fifo is
	generic(
			C_DATA_WIDTH : integer := 32;
			C_ADDR_WIDTH : integer := 6;
			C_NUM_WORDS : integer := 64
		);
	port(
			i_rst : in std_logic;		--RESET SIGNAL
			clka : in std_logic;	--WRITE CLOCK SIGNAL
			clkb : in std_logic;	--READ CLOCK SIGNAL
			din : in std_logic_vector(C_DATA_WIDTH-1 downto 0);	--INPUT DATA
			we_a : in std_logic;		--WRITE ENABLE
			re_b : in std_logic;		--READ ENABLE
			dout : out std_logic_vector(C_DATA_WIDTH-1 downto 0);	--OUTPUT DATA
			full : out std_logic;		--FIFO FULL SIGNAL
			empty : out std_logic		--FIFO EMPTY SIGNAL
	    );
end generic_dp_fifo;




architecture xst of generic_dp_fifo is


	component generic_dp_ram is	--{{{
		generic(
				C_NUM_WORDS : INTEGER := 1024;
				C_ADDR_WIDTH : INTEGER := 12;
				C_DATA_WIDTH : INTEGER := 32
			);
		port(
				clka : in std_logic;										--WRITE CLOCK
				clkb : in std_logic;										--READ CLOCK
				addr_a : in std_logic_vector(C_ADDR_WIDTH-1 downto 0);		--ADDRESS ON PORT A
				addr_b : in std_logic_vector(C_ADDR_WIDTH-1 downto 0);		--ADDRESS ON PORT B

				din_a : in std_logic_vector(C_DATA_WIDTH-1 downto 0);		--INPUT DATA FOR WRITING
				dout_a : out std_logic_vector(C_DATA_WIDTH-1 downto 0);		--OUTPUT DATA ON PORT A
				dout_b : out std_logic_vector(C_DATA_WIDTH-1 downto 0);		--OUTPUT DATA FOR READING
				we : in std_logic											--WRITE ENABLE
			);
	end component;	--}}}


	--WRITE ADDRESS ON PORT A OF THE DUAL PORT MEMORY
	signal waddr_a, n_waddr_a : std_logic_vector(C_ADDR_WIDTH - 1 downto 0);
	--READ ADDRESS ON PORT B OF THE DUAL PORT MEMORY
	signal raddr_b, n_raddr_b : std_logic_vector(C_ADDR_WIDTH - 1 downto 0);

	--PREFETCH THE NEXT READ DATA ADDRESS TO PROVIDE THE DATA ONE CLOCK CYCLE AFTER THE READ ENABLE
	signal s_prefetch_addr, n_prefetch_addr : std_logic_vector(C_ADDR_WIDTH - 1 downto 0);
	signal s_prefetch_data : std_logic_vector(C_DATA_WIDTH - 1 downto 0);


	signal s_full, s_empty : std_logic;

	--MEMORY WRITE ENABLE, ONLY ACTIVE IF FIFO NOT FULL
	signal s_mem_wen : std_logic;

begin


	u_dpram_block : generic_dp_ram	--{{{
	generic map(
					C_NUM_WORDS => C_NUM_WORDS,
					C_ADDR_WIDTH => C_ADDR_WIDTH,
					C_DATA_WIDTH => C_DATA_WIDTH
			   )
	port map(
				clka => clka,
				clkb => clkb,
				addr_a => waddr_a,
				addr_b => raddr_b,

				din_a => din,
				dout_a => open,
				dout_b => s_prefetch_data,
				we => s_mem_wen
			);
	--}}}

	--ASSIGN THE INTERNAL SIGNALS TO THEIR CORRESPONDING OUTPUTS
	full <= s_full;
	empty <= s_empty;


	--MEMORY WRITE ENABLE ONLY ACTIVE IF THE FIFO IS NOT FULL
	s_mem_wen <= we_a and not s_full;






	--====CALCULATE THE CORRECT NEXT ADDRESSES AND THE STATE OF THE FIFO MEMORY
	p_calc_address : process (waddr_a,raddr_b)
	begin
		--CALCULATE THE NEXT READ AND WRITE ADDRESS FOR THE MEMORY
		if waddr_a = std_logic_vector(to_unsigned(C_NUM_WORDS-1,C_ADDR_WIDTH)) then
			n_waddr_a <= (others => '0');
		else
			n_waddr_a <= std_logic_vector(unsigned(waddr_a)+1);
		end if;

		if raddr_b = std_logic_vector(to_unsigned(C_NUM_WORDS-1,C_ADDR_WIDTH)) then
			n_raddr_b <= (others => '0');
		else
			n_raddr_b <= std_logic_vector(unsigned(raddr_b)+1);
		end if;


		--CHECK THE STATE OF THE FIFO MEMORY
		s_full <= '0';
		s_empty <= '0';
		if unsigned(waddr_a) = unsigned(raddr_b) - 1 then
			s_full <= '1';
		end if;

		if unsigned(raddr_b) = unsigned(waddr_a) then
			s_empty <= '1';
		end if;

	end process p_calc_address;
	--============================================


	--=====ASSIGN THE NEW WRITE ADDRESS IF THE MEMORY WRITE IS ENABLED
	p_write : process (clka)
	begin
		if rising_edge(clka) then
			if i_rst = '1' then
				waddr_a <= (others => '0');
			else
				if s_mem_wen = '1' then
					waddr_a <= n_waddr_a;
				end if;
			end if;
		end if;
	end process p_write;
	--============================================


	--=====ASSIGN THE NEW READ ADDRESS IF THE FIFO IS NOT EMPTY
	p_read : process (clkb)
	begin
		if rising_edge(clkb) then
			if i_rst = '1' then
				raddr_b <= (others => '0');
			else
				if re_b ='1' and s_empty = '0' then
					raddr_b <= n_raddr_b;
					dout <= s_prefetch_data;
				end if;
			end if;
		end if;
	end process p_read;
	--============================================




end;

