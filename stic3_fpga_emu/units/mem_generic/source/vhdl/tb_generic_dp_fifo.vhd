Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity tb_generic_dp_fifo is
end tb_generic_dp_fifo;




architecture a of tb_generic_dp_fifo is

	component generic_dp_fifo is	--{{{
		generic(
				C_DATA_WIDTH : integer := 32;
				C_ADDR_WIDTH : integer := 6;
				C_NUM_WORDS : integer := 64
			);
		port(
				i_rst : in std_logic;		--RESET SIGNAL
				clka : in std_logic;	--WRITE CLOCK SIGNAL
				clkb : in std_logic;	--READ CLOCK SIGNAL
				din : in std_logic_vector(C_DATA_WIDTH-1 downto 0);	--INPUT DATA
				we_a : in std_logic;		--WRITE ENABLE
				re_b : in std_logic;		--READ ENABLE
				dout : out std_logic_vector(C_DATA_WIDTH-1 downto 0);	--OUTPUT DATA
				full : out std_logic;		--FIFO FULL SIGNAL
				empty : out std_logic		--FIFO EMPTY SIGNAL
			);
	end component; --}}}


	constant C_DATA_WIDTH : integer := 32;
	constant C_ADDR_WIDTH : integer := 6;
	constant C_NUM_WORDS : integer := 64;

	signal i_rst : std_logic;		--RESET SIGNAL
	signal clka : std_logic := '0';	--WRITE CLOCK SIGNAL
	signal clkb : std_logic := '0';	--READ CLOCK SIGNAL
	signal din : std_logic_vector(C_DATA_WIDTH-1 downto 0);	--INPUT DATA
	signal we_a : std_logic;		--WRITE ENABLE
	signal re_b : std_logic;		--READ ENABLE
	signal dout : std_logic_vector(C_DATA_WIDTH-1 downto 0);	--OUTPUT DATA
	signal full : std_logic;		--FIFO FULL SIGNAL
	signal empty : std_logic;		--FIFO EMPTY SIGNAL

	constant clka_period : time := 10 ns;
	constant clkb_period : time := 14 ns;

begin

	--CREATE THE CLOCK SIGNALS WITH THE DEFINED PERIOD
	clka <= not clka after clka_period/2;
	clkb <= not clkb after clkb_period/2;




	dut : generic_dp_fifo --{{{
		generic map(
				C_DATA_WIDTH => C_DATA_WIDTH,
				C_ADDR_WIDTH => C_ADDR_WIDTH,
				C_NUM_WORDS => C_NUM_WORDS
			)
		port map(
				i_rst => i_rst,
				clka => clka,
				clkb => clkb,
				din => din,
				we_a => we_a,
				re_b => re_b,
				dout => dout,
				full => full,
				empty => empty
			); --}}}


	write_a: process
	begin
		we_a <= '0';
		din <= (others => '0');
		i_rst <= '1';

		wait for 100 ns;
		i_rst <= '0';
		wait until rising_edge(clka);

		--FILL THE MEMORY WITH DATA
		for i in 0 to C_NUM_WORDS+1 loop

			we_a <= '1';
			din <= std_logic_vector(to_unsigned(i,C_DATA_WIDTH));
			wait until rising_edge(clka);
			we_a <= '0';
			wait until rising_edge(clka);

		end loop;

		wait;

	end process;


	read_b: process
	begin
		re_b <= '0';
		wait for 10 us;
		wait until rising_edge(clkb);

		for i in 0 to C_NUM_WORDS-1 loop
			wait until rising_edge(clkb);
			re_b <= '1';
		end loop;

		wait;

	end process;



end;

