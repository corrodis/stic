--	Send a byte over the serial bus to the pc
--
--
Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;

entity s_data is --{{{
port(
	data : in std_logic_vector(7 downto 0);
	clk : in std_logic;
	rst_n: in std_logic;
	send : in std_logic;
	d_ready : out std_logic;
	TX_BIT : out std_logic
);
end s_data;
--}}}

architecture a of s_data is 

------- SIGNAL DEFINITIONS --{{{

signal d_i     : std_logic_vector(7 downto 0);
signal eot     : std_logic; 


type s_state is (S0,S1,S2,S3,S4,S5,S6,S7,S8,S9);	--define the states of the statemachine
signal present_st, next_st : s_state;

signal start : std_logic := '0';
signal sample : std_logic := '0';


component clock_div is
generic (Nd : Integer := 50);
PORT(
        clk, en : IN  STD_LOGIC;
        sample     : OUT STD_LOGIC);
end component;


--}}}


------- BEHAVIOUR OF ARCHITECTURE --{{{

begin

d_ready <= eot;	-- show when the transmission has finished
d_i <= data; -- use internal data



--DIVIDE THE INPUT CLOCK TO THE 115200 Hz FOR THE TRANSMISSION
g_sam1: clock_div 
generic map(Nd => 1389)
port map(
	clk => clk,
	en => start,
	sample => sample
);



process(clk,eot,rst_n)
begin
	
	if rising_edge(clk) then
		if(rst_n = '0' or eot = '1') then	--RESET or DATA Ready so do not send anything now
			start <= '0';
		elsif send = '1' then
			start <= '1';
		end if;
		
	end if;
end process;


process(sample,rst_n,next_st,eot)
begin
	if rst_n = '0' then
		present_st <= S0;
		next_st <= S0;
		TX_BIT <= '1';			-- Set dataline to high
		eot <= '0';
	elsif rising_edge(sample) then
		case present_st is

		when S0 => if start = '1' then
			   	next_st <= S1;
				eot <= '0';
				TX_BIT <= '0'; --STARTBIT
			   else
				next_st <= S0;
			   end if;

		when S1 => next_st <= S2;
			   eot <= '0';
			   TX_BIT <= d_i(0);
		when S2 => next_st <= S3;
			   eot <= '0';
			   TX_BIT <= d_i(1);
		when S3 => next_st <= S4;
			   eot <= '0';
			   TX_BIT <= d_i(2);
		when S4 => next_st <= S5;
			   eot <= '0';
			   TX_BIT <= d_i(3);
		when S5 => next_st <= S6;
			   eot <= '0';
		 	   TX_BIT <= d_i(4);
		when S6 => next_st <= S7;
			   eot <= '0';
			   TX_BIT <= d_i(5);
		when S7 => next_st <= S8;
			   eot <= '0';
			   TX_BIT <= d_i(6);
		when S8 => next_st <= S9;
			   eot <= '0';
			   TX_BIT <= d_i(7);
		when S9 => next_st <= S0;
			   TX_BIT <= '1';
			   eot <= '1';
		
		when others => next_st <= S0;
			  eot <= '0';
		end case;	

	end if;
	present_st <= next_st;
end process;


end; --}}}

