Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity stic3_emu_top is
	port(
			--INPUT CLOCK SIGNAL 160 MHz
			i_sys_clk : in std_logic;
			--RESET SIGNAL FROM THE DAQ
			i_rst : in std_logic;

			o_serial_txd : out std_logic;


			--UART INTERCONNECTION
--			o_uart_tx : out std_logic;
			i_uart_rx : in std_logic
	    );
end stic3_emu_top;




architecture behaviour of stic3_emu_top is


component frame_generator is	--{{{
		generic(N_bits : Integer := 70);
		port (
				--SYSTEM SIGNALS
				i_clk				: in std_logic;
				i_rst				: in std_logic;									--RESET SIGNAL
				i_rst_slow			: in std_logic;
				i_start_trans		: in std_logic;									--SIGNAL FROM DAQ TO START TRANSMISSION

				--FIFO SIGNALS
				o_fifo_rd_en		: out std_logic;								--READ ENABLE TO THE FIFO
				i_fifo_count		: in std_logic_vector(6 downto 0);				--NUMBER OF STORED HITS IN THE FIFO
				i_fifo_empty		: in std_logic;									--FIFO EMPTY SIGNAL
				i_fifo_data			: in std_logic_vector(N_bits - 1 downto 0);		--THE DATA OUTPUT FROM THE FIFO

				--CONFIGURATION SIGNALS
				i_gen_idle_sig		: in std_logic;


				-- SERIALIZER SIGNALS
				o_ser_data			: out std_logic_vector(9 downto 0);		--DATA TO THE SERIALIZER
				o_clk_s_d			: out std_logic


			 );
end component frame_generator; --}}}

component serializer is		--{{{
	generic(N_bits : Integer := 36);
	port (

			i_clk		: in std_logic;					--SYSTEM CLOCK
			i_rst		: in std_logic;
			i_clk_s_d		: in std_logic;

			--SIGNALS FROM AND TO THE FRAME GENERATOR
			i_ser_data			: in std_logic_vector(9 downto 0);

			--DATA LINK OUTPUT SIGNAL
			o_serial_TXD		: out std_logic
		 );
	end component serializer; --}}}

	component eval_cmd is
		port(
				i_sys_clk : in std_logic;
				i_rst : in std_logic;

				--FIFO EMULATION SIGNALS
				o_fifo_data : out std_logic_vector(47 downto 0);
				o_fifo_empty : out std_logic;
				o_fifo_count : out std_logic_vector(6 downto 0);

				o_start_trans : out std_logic;
				o_gen_idle : out std_logic;

				--UART INTERFACE SIGNALS
				i_uart_rx : in std_logic
			);
	end component eval_cmd;


	--DP FIFO IF WE WANT TO IMPLEMENT A MORE REALISTIC VERSION OF THE STIC3 SERIALIZER EMULATION
component generic_dp_fifo is --{{{
	generic(
			C_DATA_WIDTH : integer := 32;
			C_ADDR_WIDTH : integer := 6;
			C_NUM_WORDS : integer := 64
		);
	port(
			i_rst : in std_logic;		--RESET SIGNAL
			clka : in std_logic;	--WRITE CLOCK SIGNAL
			clkb : in std_logic;	--READ CLOCK SIGNAL
			din : in std_logic_vector(C_DATA_WIDTH-1 downto 0);	--INPUT DATA
			we_a : in std_logic;		--WRITE ENABLE
			re_b : in std_logic;		--READ ENABLE
			dout : out std_logic_vector(C_DATA_WIDTH-1 downto 0);	--OUTPUT DATA
			full : out std_logic;		--FIFO FULL SIGNAL
			empty : out std_logic		--FIFO EMPTY SIGNAL
	    );
end component generic_dp_fifo; --}}}





	--FRAME GENERATOR SIGNALS
	signal s_start_trans : std_logic;	--Init transmission controlled via serial interface

	--ENCODED DATA FOR THE TRANSMISSION
	signal s_ser_data : std_logic_vector(9 downto 0);

	signal s_clock_s_d : std_logic;		--slow clock delayed
	signal s_gen_idle_sig : std_logic;	--generate the idle signal if this is '1'

	--FIFO BUFFER SIGNALS
--	signal s_fifo_data_in : std_logic_vector(47 downto 0);
	signal s_fifo_data_out : std_logic_vector(47 downto 0);
	signal s_fifo_count : std_logic_vector(6 downto 0);	--signal provided by the serial interface since the dp_generic_fifo has no counter
	signal s_fifo_empty : std_logic;

	--WRITE AND READ ENABLE
--	signal s_fifo_we : std_logic;
--	signal s_fifo_re : std_logic;


begin


		unit_frame_gen: frame_generator --{{{
		generic map(N_bits => 48)
		port map(
			--SYSTEM SIGNALS
			i_clk			=> 	i_sys_clk,
			i_rst			=> 	i_rst,
			i_rst_slow		=>  i_rst,    -- tmp signal
			i_start_trans	=> 	s_start_trans,

			--FIFO SIGNALS  Level 2
			o_fifo_rd_en	=> 	open,	--NOT NEEDED WITH EMULATED FIFO
			i_fifo_count	=> 	s_fifo_count,
			i_fifo_empty	=> 	s_fifo_empty,
			i_fifo_data		=> 	s_fifo_data_out,

			--CONFIGURATION SIGNALS
			i_gen_idle_sig	=> s_gen_idle_sig,

			-- SERIALIZER SIGNALS
			o_ser_data		=>  s_ser_data,
			o_clk_s_d		=>  s_clock_s_d

		); --}}}


--		Memory_L2 : generic_dp_fifo --{{{
--		generic map(C_NUM_WORDS => 128, C_DATA_WIDTH => 48, C_ADDR_WIDTH => 7)
--		PORT MAP (
--			i_clk => i_sys_clk,
--			i_rst => i_rst,
--			i_DI => s_fifo_data_in,
--			i_wen => s_fifo_we,
--			i_ren => s_fifo_re,
--			o_DO => s_fifo_data_out,
--			o_full => s_fifo_full,
--			o_empty => s_fifo_empty
--		); --}}}




		unit_serializer : serializer --{{{
		port map(
					i_clk => i_sys_clk,
					i_rst => i_rst,
					i_clk_s_d => s_clock_s_d,

					--SIGNALS FROM AND TO THE FRAME GENERATOR
					i_ser_data => s_ser_data,

					--DATA LINK OUTPUT SIGNAL
					o_serial_TXD => o_serial_txd
				); --}}}


	unit_uartcontrol : eval_cmd
		port map(
				i_sys_clk => i_sys_clk,
				i_rst => i_rst,

				--FIFO EMULATION SIGNALS
				o_fifo_data => s_fifo_data_out,
				o_fifo_empty => s_fifo_empty,
				o_fifo_count => s_fifo_count,
				o_start_trans => s_start_trans,
				o_gen_idle => s_gen_idle_sig,


				--UART INTERFACE SIGNALS
				i_uart_rx => i_uart_rx
			);



end;

