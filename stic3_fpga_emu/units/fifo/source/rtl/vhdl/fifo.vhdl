--=============================================================
--
--fifo.vhdl
--
-- FIFO BUFFER USING 64x36 DUAL PORT SRAM REGISTER
--
--Version Date: 02.06.2011
--
--Author: Tobias Harion
--
--	sys_clk
--           .----------.         .----------.         .----------.         .----------.
--  _________|          |_________|          |_________|          |_________|          |





--=============================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.VITAL_Primitives.all;
use IEEE.VITAL_Timing.all;
use std.textio.all;
use IEEE.std_logic_textio.all;
use IEEE.std_logic_arith.all;

entity fifo is	--{{{
	generic( N_words : Integer := 64;	--NUMBER OF WORDS TO STORE IN THE FIFO
			 ADDR_WIDTH : Integer := 6;	--ADDRESS WIDTH OF THE COUNTER, ALLOWS TO IMPLEMENT TWO FIFO DEPTHS
		   	 N_bits : Integer := 71		--WIDTH OF THE WORDS TO BE STORED
		   );
	port (

				--DATA SIGNALS

				i_DI : in std_logic_vector(N_bits - 1 downto 0);
				o_DO : out std_logic_vector(N_bits - 1 downto 0);

				--CONTROL SIGNALS

				i_clk : in std_logic;		--CLOCK SIGNAL
				i_wen : in std_logic;		--WRITE ENABLE BIT: WRITES DATA TO THE CURRENT POSITION AND INCREMENTS THE WRITE POINTER
				i_ren : in std_logic;		--READ ENABLE BIT: INCREMENTS THE READ POINTER WITH THE CLK SIGNAL
				i_rst : in std_logic;		--RESET BIT: RESETS ONLY THE POINTERS SINCE THERE IS NO RESET FOR THE SRAM

				
				o_count : out std_logic_vector(ADDR_WIDTH -1 downto 0);	--NUMBER OF HITS STORED IN THE BUFFER
				o_empty : out std_logic;
				o_full : out std_logic
			
		 );
	end entity fifo; --}}}




architecture quick of fifo is	--{{{1

-- COMPONENT: SRAM	{{{
component SZ180_128X48X1CM2 is
   port(
      A0                            :   IN   std_logic;
      A1                            :   IN   std_logic;
      A2                            :   IN   std_logic;
      A3                            :   IN   std_logic;
      A4                            :   IN   std_logic;
      A5                            :   IN   std_logic;
      A6                            :   IN   std_logic;
      B0                            :   IN   std_logic;
      B1                            :   IN   std_logic;
      B2                            :   IN   std_logic;
      B3                            :   IN   std_logic;
      B4                            :   IN   std_logic;
      B5                            :   IN   std_logic;
      B6                            :   IN   std_logic;
      DO0                        :   OUT   std_logic;
      DO1                        :   OUT   std_logic;
      DO2                        :   OUT   std_logic;
      DO3                        :   OUT   std_logic;
      DO4                        :   OUT   std_logic;
      DO5                        :   OUT   std_logic;
      DO6                        :   OUT   std_logic;
      DO7                        :   OUT   std_logic;
      DO8                        :   OUT   std_logic;
      DO9                        :   OUT   std_logic;
      DO10                        :   OUT   std_logic;
      DO11                        :   OUT   std_logic;
      DO12                        :   OUT   std_logic;
      DO13                        :   OUT   std_logic;
      DO14                        :   OUT   std_logic;
      DO15                        :   OUT   std_logic;
      DO16                        :   OUT   std_logic;
      DO17                        :   OUT   std_logic;
      DO18                        :   OUT   std_logic;
      DO19                        :   OUT   std_logic;
      DO20                        :   OUT   std_logic;
      DO21                        :   OUT   std_logic;
      DO22                        :   OUT   std_logic;
      DO23                        :   OUT   std_logic;
      DO24                        :   OUT   std_logic;
      DO25                        :   OUT   std_logic;
      DO26                        :   OUT   std_logic;
      DO27                        :   OUT   std_logic;
      DO28                        :   OUT   std_logic;
      DO29                        :   OUT   std_logic;
      DO30                        :   OUT   std_logic;
      DO31                        :   OUT   std_logic;
      DO32                        :   OUT   std_logic;
      DO33                        :   OUT   std_logic;
      DO34                        :   OUT   std_logic;
      DO35                        :   OUT   std_logic;
      DO36                        :   OUT   std_logic;
      DO37                        :   OUT   std_logic;
      DO38                        :   OUT   std_logic;
      DO39                        :   OUT   std_logic;
      DO40                        :   OUT   std_logic;
      DO41                        :   OUT   std_logic;
      DO42                        :   OUT   std_logic;
      DO43                        :   OUT   std_logic;
      DO44                        :   OUT   std_logic;
      DO45                        :   OUT   std_logic;
      DO46                        :   OUT   std_logic;
      DO47                        :   OUT   std_logic;
      DI0                        :   IN   std_logic;
      DI1                        :   IN   std_logic;
      DI2                        :   IN   std_logic;
      DI3                        :   IN   std_logic;
      DI4                        :   IN   std_logic;
      DI5                        :   IN   std_logic;
      DI6                        :   IN   std_logic;
      DI7                        :   IN   std_logic;
      DI8                        :   IN   std_logic;
      DI9                        :   IN   std_logic;
      DI10                        :   IN   std_logic;
      DI11                        :   IN   std_logic;
      DI12                        :   IN   std_logic;
      DI13                        :   IN   std_logic;
      DI14                        :   IN   std_logic;
      DI15                        :   IN   std_logic;
      DI16                        :   IN   std_logic;
      DI17                        :   IN   std_logic;
      DI18                        :   IN   std_logic;
      DI19                        :   IN   std_logic;
      DI20                        :   IN   std_logic;
      DI21                        :   IN   std_logic;
      DI22                        :   IN   std_logic;
      DI23                        :   IN   std_logic;
      DI24                        :   IN   std_logic;
      DI25                        :   IN   std_logic;
      DI26                        :   IN   std_logic;
      DI27                        :   IN   std_logic;
      DI28                        :   IN   std_logic;
      DI29                        :   IN   std_logic;
      DI30                        :   IN   std_logic;
      DI31                        :   IN   std_logic;
      DI32                        :   IN   std_logic;
      DI33                        :   IN   std_logic;
      DI34                        :   IN   std_logic;
      DI35                        :   IN   std_logic;
      DI36                        :   IN   std_logic;
      DI37                        :   IN   std_logic;
      DI38                        :   IN   std_logic;
      DI39                        :   IN   std_logic;
      DI40                        :   IN   std_logic;
      DI41                        :   IN   std_logic;
      DI42                        :   IN   std_logic;
      DI43                        :   IN   std_logic;
      DI44                        :   IN   std_logic;
      DI45                        :   IN   std_logic;
      DI46                        :   IN   std_logic;
      DI47                        :   IN   std_logic;
      WEB                           :   IN   std_logic;
      CKA                            :   IN   std_logic;
      CKB                            :   IN   std_logic;
      CSAN                            :   IN   std_logic;
      CSBN                            :   IN   std_logic
      );
end component SZ180_128X48X1CM2;

-- }}}

-- COMPONENT: SRAM	{{{
component SZ180_64X78X1CM2 is
   port(
      A0                            :   IN   std_logic;
      A1                            :   IN   std_logic;
      A2                            :   IN   std_logic;
      A3                            :   IN   std_logic;
      A4                            :   IN   std_logic;
      A5                            :   IN   std_logic;
      B0                            :   IN   std_logic;
      B1                            :   IN   std_logic;
      B2                            :   IN   std_logic;
      B3                            :   IN   std_logic;
      B4                            :   IN   std_logic;
      B5                            :   IN   std_logic;
      DO0                        :   OUT   std_logic;
      DO1                        :   OUT   std_logic;
      DO2                        :   OUT   std_logic;
      DO3                        :   OUT   std_logic;
      DO4                        :   OUT   std_logic;
      DO5                        :   OUT   std_logic;
      DO6                        :   OUT   std_logic;
      DO7                        :   OUT   std_logic;
      DO8                        :   OUT   std_logic;
      DO9                        :   OUT   std_logic;
      DO10                        :   OUT   std_logic;
      DO11                        :   OUT   std_logic;
      DO12                        :   OUT   std_logic;
      DO13                        :   OUT   std_logic;
      DO14                        :   OUT   std_logic;
      DO15                        :   OUT   std_logic;
      DO16                        :   OUT   std_logic;
      DO17                        :   OUT   std_logic;
      DO18                        :   OUT   std_logic;
      DO19                        :   OUT   std_logic;
      DO20                        :   OUT   std_logic;
      DO21                        :   OUT   std_logic;
      DO22                        :   OUT   std_logic;
      DO23                        :   OUT   std_logic;
      DO24                        :   OUT   std_logic;
      DO25                        :   OUT   std_logic;
      DO26                        :   OUT   std_logic;
      DO27                        :   OUT   std_logic;
      DO28                        :   OUT   std_logic;
      DO29                        :   OUT   std_logic;
      DO30                        :   OUT   std_logic;
      DO31                        :   OUT   std_logic;
      DO32                        :   OUT   std_logic;
      DO33                        :   OUT   std_logic;
      DO34                        :   OUT   std_logic;
      DO35                        :   OUT   std_logic;
      DO36                        :   OUT   std_logic;
      DO37                        :   OUT   std_logic;
      DO38                        :   OUT   std_logic;
      DO39                        :   OUT   std_logic;
      DO40                        :   OUT   std_logic;
      DO41                        :   OUT   std_logic;
      DO42                        :   OUT   std_logic;
      DO43                        :   OUT   std_logic;
      DO44                        :   OUT   std_logic;
      DO45                        :   OUT   std_logic;
      DO46                        :   OUT   std_logic;
      DO47                        :   OUT   std_logic;
      DO48                        :   OUT   std_logic;
      DO49                        :   OUT   std_logic;
      DO50                        :   OUT   std_logic;
      DO51                        :   OUT   std_logic;
      DO52                        :   OUT   std_logic;
      DO53                        :   OUT   std_logic;
      DO54                        :   OUT   std_logic;
      DO55                        :   OUT   std_logic;
      DO56                        :   OUT   std_logic;
      DO57                        :   OUT   std_logic;
      DO58                        :   OUT   std_logic;
      DO59                        :   OUT   std_logic;
      DO60                        :   OUT   std_logic;
      DO61                        :   OUT   std_logic;
      DO62                        :   OUT   std_logic;
      DO63                        :   OUT   std_logic;
      DO64                        :   OUT   std_logic;
      DO65                        :   OUT   std_logic;
      DO66                        :   OUT   std_logic;
      DO67                        :   OUT   std_logic;
      DO68                        :   OUT   std_logic;
      DO69                        :   OUT   std_logic;
      DO70                        :   OUT   std_logic;
      DO71                        :   OUT   std_logic;
      DO72                        :   OUT   std_logic;
      DO73                        :   OUT   std_logic;
      DO74                        :   OUT   std_logic;
      DO75                        :   OUT   std_logic;
      DO76                        :   OUT   std_logic;
      DO77                        :   OUT   std_logic;
      DI0                        :   IN   std_logic;
      DI1                        :   IN   std_logic;
      DI2                        :   IN   std_logic;
      DI3                        :   IN   std_logic;
      DI4                        :   IN   std_logic;
      DI5                        :   IN   std_logic;
      DI6                        :   IN   std_logic;
      DI7                        :   IN   std_logic;
      DI8                        :   IN   std_logic;
      DI9                        :   IN   std_logic;
      DI10                        :   IN   std_logic;
      DI11                        :   IN   std_logic;
      DI12                        :   IN   std_logic;
      DI13                        :   IN   std_logic;
      DI14                        :   IN   std_logic;
      DI15                        :   IN   std_logic;
      DI16                        :   IN   std_logic;
      DI17                        :   IN   std_logic;
      DI18                        :   IN   std_logic;
      DI19                        :   IN   std_logic;
      DI20                        :   IN   std_logic;
      DI21                        :   IN   std_logic;
      DI22                        :   IN   std_logic;
      DI23                        :   IN   std_logic;
      DI24                        :   IN   std_logic;
      DI25                        :   IN   std_logic;
      DI26                        :   IN   std_logic;
      DI27                        :   IN   std_logic;
      DI28                        :   IN   std_logic;
      DI29                        :   IN   std_logic;
      DI30                        :   IN   std_logic;
      DI31                        :   IN   std_logic;
      DI32                        :   IN   std_logic;
      DI33                        :   IN   std_logic;
      DI34                        :   IN   std_logic;
      DI35                        :   IN   std_logic;
      DI36                        :   IN   std_logic;
      DI37                        :   IN   std_logic;
      DI38                        :   IN   std_logic;
      DI39                        :   IN   std_logic;
      DI40                        :   IN   std_logic;
      DI41                        :   IN   std_logic;
      DI42                        :   IN   std_logic;
      DI43                        :   IN   std_logic;
      DI44                        :   IN   std_logic;
      DI45                        :   IN   std_logic;
      DI46                        :   IN   std_logic;
      DI47                        :   IN   std_logic;
      DI48                        :   IN   std_logic;
      DI49                        :   IN   std_logic;
      DI50                        :   IN   std_logic;
      DI51                        :   IN   std_logic;
      DI52                        :   IN   std_logic;
      DI53                        :   IN   std_logic;
      DI54                        :   IN   std_logic;
      DI55                        :   IN   std_logic;
      DI56                        :   IN   std_logic;
      DI57                        :   IN   std_logic;
      DI58                        :   IN   std_logic;
      DI59                        :   IN   std_logic;
      DI60                        :   IN   std_logic;
      DI61                        :   IN   std_logic;
      DI62                        :   IN   std_logic;
      DI63                        :   IN   std_logic;
      DI64                        :   IN   std_logic;
      DI65                        :   IN   std_logic;
      DI66                        :   IN   std_logic;
      DI67                        :   IN   std_logic;
      DI68                        :   IN   std_logic;
      DI69                        :   IN   std_logic;
      DI70                        :   IN   std_logic;
      DI71                        :   IN   std_logic;
      DI72                        :   IN   std_logic;
      DI73                        :   IN   std_logic;
      DI74                        :   IN   std_logic;
      DI75                        :   IN   std_logic;
      DI76                        :   IN   std_logic;
      DI77                        :   IN   std_logic;
      WEB                           :   IN   std_logic;
      CKA                            :   IN   std_logic;
      CKB                            :   IN   std_logic;
      CSAN                            :   IN   std_logic;
      CSBN                            :   IN   std_logic
      );
end component SZ180_64X78X1CM2;

-- }}}

signal A,B : std_logic_vector(ADDR_WIDTH-1 downto 0);		--A: READ ADDRESS, B: WRITE ADDRESS
signal next_A, next_B : std_logic_vector(ADDR_WIDTH-1 downto 0);
signal DO,DI : std_logic_vector(N_bits - 1 downto 0);	--DATA INPUT AND DATA OUTPUT OF THE SRAM
signal WEB, CSAN, CSBN : std_logic;				--WRITE ENABLE AND CHIP SELECT SIGNALS
signal CKB : std_logic;							--THE CLOCK SIGNALS FOR THE SRAM
signal fifo_empty : std_logic;
signal fifo_full : std_logic;


begin


--ASSIGN THE EXTERNAL INPUT SIGNALS TO THEIR COUNTERPART
	CKB <= i_clk;
	o_DO <= DO;

--=======SRAM INSTANTIATION =======---
	L2_sram: if N_words = 128 generate
--	{{{
	sram: SZ180_128X48X1CM2
	port map(			  A0	=>  A(0),
						  A1	=>  A(1),
						  A2	=>  A(2),
						  A3	=>  A(3),
						  A4	=>  A(4),
						  A5	=>  A(5),
						  A6	=>  A(6),
						  B0	=>  B(0),
						  B1	=>  B(1),
						  B2	=>  B(2),
						  B3	=>  B(3),
						  B4	=>  B(4),
						  B5	=>  B(5),
						  B6	=>  B(6),
						  DO0	=>  DO(0),
						  DO1	=>  DO(1),
						  DO2	=>  DO(2),
						  DO3	=>  DO(3),
						  DO4	=>  DO(4),
						  DO5	=>  DO(5),
						  DO6	=>  DO(6),
						  DO7	=>  DO(7),
						  DO8	=>  DO(8),
						  DO9	=>  DO(9),
						  DO10	=>  DO(10),
						  DO11	=>  DO(11),
						  DO12	=>  DO(12),
						  DO13	=>  DO(13),
						  DO14	=>  DO(14),
						  DO15	=>  DO(15),
						  DO16	=>  DO(16),
						  DO17	=>  DO(17),
						  DO18	=>  DO(18),
						  DO19	=>  DO(19),
						  DO20	=>  DO(20),
						  DO21	=>  DO(21),
						  DO22	=>  DO(22),
						  DO23	=>  DO(23),
						  DO24	=>  DO(24),
						  DO25	=>  DO(25),
						  DO26	=>  DO(26),
						  DO27	=>  DO(27),
						  DO28	=>  DO(28),
						  DO29	=>  DO(29),
						  DO30	=>  DO(30),
						  DO31	=>  DO(31),
						  DO32	=>  DO(32),
						  DO33	=>  DO(33),
						  DO34	=>  DO(34),
						  DO35	=>  DO(35),
						DO36=> DO(36),
						DO37=> DO(37),
						DO38=> DO(38),
						DO39=> DO(39),
						DO40=> DO(40),
						DO41=> DO(41),
						DO42=> DO(42),
						DO43=> DO(43),
						DO44=> DO(44),
						DO45=> DO(45),
						DO46=> DO(46),
						DO47=> DO(47),


						  DI0	=>  DI(0),
						  DI1	=>  DI(1),
						  DI2	=>  DI(2),
						  DI3	=>  DI(3),
						  DI4	=>  DI(4),
						  DI5	=>  DI(5),
						  DI6	=>  DI(6),
						  DI7	=>  DI(7),
						  DI8	=>  DI(8),
						  DI9	=>  DI(9),
						  DI10	=>  DI(10),
						  DI11	=>  DI(11),
						  DI12	=>  DI(12),
						  DI13	=>  DI(13),
						  DI14	=>  DI(14),
						  DI15	=>  DI(15),
						  DI16	=>  DI(16),
						  DI17	=>  DI(17),
						  DI18	=>  DI(18),
						  DI19	=>  DI(19),
						  DI20	=>  DI(20),
						  DI21	=>  DI(21),
						  DI22	=>  DI(22),
						  DI23	=>  DI(23),
						  DI24	=>  DI(24),
						  DI25	=>  DI(25),
						  DI26	=>  DI(26),
						  DI27	=>  DI(27),
						  DI28	=>  DI(28),
						  DI29	=>  DI(29),
						  DI30	=>  DI(30),
						  DI31	=>  DI(31),
						  DI32	=>  DI(32),
						  DI33	=>  DI(33),
						  DI34	=>  DI(34),
						  DI35	=>  DI(35),
							DI36 => DI(36),
							DI37=> DI(37),
							DI38=> DI(38),
							DI39=> DI(39),
							DI40=> DI(40),
							DI41=> DI(41),
							DI42=> DI(42),
							DI43=> DI(43),
							DI44=> DI(44),
							DI45=> DI(45),
							DI46=> DI(46),
							DI47=> DI(47),

						  WEB	=>  WEB,
						  CKA	=>  CKB,
						  CKB	=>	CKB,
						  CSAN	=> CSAN,
						  CSBN	=>  CSBN

				);
--	}}}
	end generate;
	L1_sram: if N_words = 64 generate
	--	{{{2
		sram: SZ180_64X78X1CM2
		port map(			  A0	=>  A(0),
							  A1	=>  A(1),
							  A2	=>  A(2),
							  A3	=>  A(3),
							  A4	=>  A(4),
							  A5	=>  A(5),
							  B0	=>  B(0),
							  B1	=>  B(1),
							  B2	=>  B(2),
							  B3	=>  B(3),
							  B4	=>  B(4),
							  B5	=>  B(5),
							  DO0	=>  DO(0),
							  DO1	=>  DO(1),
							  DO2	=>  DO(2),
							  DO3	=>  DO(3),
							  DO4	=>  DO(4),
							  DO5	=>  DO(5),
							  DO6	=>  DO(6),
							  DO7	=>  DO(7),
							  DO8	=>  DO(8),
							  DO9	=>  DO(9),
							  DO10	=>  DO(10),
							  DO11	=>  DO(11),
							  DO12	=>  DO(12),
							  DO13	=>  DO(13),
							  DO14	=>  DO(14),
							  DO15	=>  DO(15),
							  DO16	=>  DO(16),
							  DO17	=>  DO(17),
							  DO18	=>  DO(18),
							  DO19	=>  DO(19),
							  DO20	=>  DO(20),
							  DO21	=>  DO(21),
							  DO22	=>  DO(22),
							  DO23	=>  DO(23),
							  DO24	=>  DO(24),
							  DO25	=>  DO(25),
							  DO26	=>  DO(26),
							  DO27	=>  DO(27),
							  DO28	=>  DO(28),
							  DO29	=>  DO(29),
							  DO30	=>  DO(30),
							  DO31	=>  DO(31),
							  DO32	=>  DO(32),
							  DO33	=>  DO(33),
							  DO34	=>  DO(34),
							  DO35	=>  DO(35),
							DO36=> DO(36),
							DO37=> DO(37),
							DO38=> DO(38),
							DO39=> DO(39),
							DO40=> DO(40),
							DO41=> DO(41),
							DO42=> DO(42),
							DO43=> DO(43),
							DO44=> DO(44),
							DO45=> DO(45),
							DO46=> DO(46),
							DO47=> DO(47),
							DO48=> DO(48),
							DO49=> DO(49),
							DO50=> DO(50),
							DO51=> DO(51),
							DO52=> DO(52),
							DO53=> DO(53),
							DO54=> DO(54),
							DO55=> DO(55),
							DO56=> DO(56),
							DO57=> DO(57),
							DO58=> DO(58),
							DO59=> DO(59),
							DO60=> DO(60),
							DO61=> DO(61),
							DO62=> DO(62),
							DO63=> DO(63),
							DO64=> DO(64),
							DO65=> DO(65),
							DO66=> DO(66),
							DO67=> DO(67),
							DO68=> DO(68),
							DO69=> DO(69),
							DO70=> DO(70),
							DO71=> DO(71),
							DO72=> DO(72),
							DO73=> DO(73),
							DO74=> DO(74),
							DO75=> DO(75),
							DO76=> DO(76),
							DO77=> DO(77),


							  DI0	=>  DI(0),
							  DI1	=>  DI(1),
							  DI2	=>  DI(2),
							  DI3	=>  DI(3),
							  DI4	=>  DI(4),
							  DI5	=>  DI(5),
							  DI6	=>  DI(6),
							  DI7	=>  DI(7),
							  DI8	=>  DI(8),
							  DI9	=>  DI(9),
							  DI10	=>  DI(10),
							  DI11	=>  DI(11),
							  DI12	=>  DI(12),
							  DI13	=>  DI(13),
							  DI14	=>  DI(14),
							  DI15	=>  DI(15),
							  DI16	=>  DI(16),
							  DI17	=>  DI(17),
							  DI18	=>  DI(18),
							  DI19	=>  DI(19),
							  DI20	=>  DI(20),
							  DI21	=>  DI(21),
							  DI22	=>  DI(22),
							  DI23	=>  DI(23),
							  DI24	=>  DI(24),
							  DI25	=>  DI(25),
							  DI26	=>  DI(26),
							  DI27	=>  DI(27),
							  DI28	=>  DI(28),
							  DI29	=>  DI(29),
							  DI30	=>  DI(30),
							  DI31	=>  DI(31),
							  DI32	=>  DI(32),
							  DI33	=>  DI(33),
							  DI34	=>  DI(34),
							  DI35	=>  DI(35),
								DI36 => DI(36),
								DI37=> DI(37),
								DI38=> DI(38),
								DI39=> DI(39),
								DI40=> DI(40),
								DI41=> DI(41),
								DI42=> DI(42),
								DI43=> DI(43),
								DI44=> DI(44),
								DI45=> DI(45),
								DI46=> DI(46),
								DI47=> DI(47),
								DI48=> DI(48),
								DI49=> DI(49),
								DI50=> DI(50),
								DI51=> DI(51),
								DI52=> DI(52),
								DI53=> DI(53),
								DI54=> DI(54),
								DI55=> DI(55),
								DI56=> DI(56),
								DI57=> DI(57),
								DI58=> DI(58),
								DI59=> DI(59),
								DI60=> DI(60),
								DI61=> DI(61),
								DI62=> DI(62),
								DI63=> DI(63),
								DI64=> DI(64),
								DI65=> DI(65),
								DI66=> DI(66),
								DI67=> DI(67),
								DI68=> DI(68),
								DI69=> DI(69),
								DI70=> DI(70),
								DI71=> DI(71),
								DI72=> DI(72),
								DI73=> DI(73),
								DI74=> DI(74),
								DI75=> DI(75),
								DI76=> DI(76),
								DI77=> DI(77),

							  WEB	=>  WEB,
							  CKA	=>  CKB,
							  CKB	=>	CKB,
							  CSAN	=> CSAN,
							  CSBN	=>  CSBN

					);


	--	}}}
	end generate;

		new_address	 : process (A,B)			--Calculate the next addresses	--{{{2
		begin

				if A = conv_std_logic_vector(N_words - 1,ADDR_WIDTH) then
					next_A <= (others => '0');
				else
					next_A <= conv_std_logic_vector(conv_integer(unsigned(A))+1,ADDR_WIDTH);
				end if;

				if B = conv_std_logic_vector(N_words - 1,ADDR_WIDTH) then
					next_B <= (others => '0');
				else
					next_B <= conv_std_logic_vector(conv_integer(unsigned(B))+1,ADDR_WIDTH);
				end if;

		end process new_address;	--}}}





		write_enable : process (CKB,i_wen,fifo_full,i_rst)	--{{{2
		begin
			if rising_edge(CKB) then
				if i_rst = '1' then
					WEB <= '1';
				else
					DI <=  i_DI; --SINCE THE WEB IS DELAYED SO HAVE TO BE THE DATA SIGNALS

					if i_wen = '1' and fifo_full = '0' then
						WEB <= '0';
					else
						WEB <= '1';
					end if;
				end if;
			end if;
		end process write_enable;		--}}}


			CSAN <= '0';	--ALWAYS ENABLE THE CHIPS
			CSBN <= '0';




		pointer	 : process (CKB,i_wen,i_ren,i_rst,next_A,next_B)			--{{{2
			variable store_count : integer range 0 to N_words -1;	--KEEP TRACK OF HOW MANY REGISTERS HAVE VALID DATA
		begin
				if i_rst = '1' then				--SYNC RESET
					store_count := 0;
					A <= (others => '0');
					B <= (others => '0');	--START AT ADDRESS 1

				elsif rising_edge(CKB) then

						--WRITE CYCLE
						if store_count /= N_words - 1 and i_wen = '1' then	--ONLY WRITE IF THE FIFO IS NOT FULL AND WEN IS SELECTED
							B <= next_B;
							store_count := store_count + 1;
						end if;

						--READ CYCLE

						if store_count /= 0 and i_ren = '1' then			--IS THE BUFFER EMPTY
							A <= next_A;
							store_count := store_count - 1;
						end if;

				end if;	--end if rising_edge(CKB)

				fifo_full <= '0';
				fifo_empty <= '0';
				--GENERATE THE FULL AND EMPTY FLAGS
				if store_count = 0 then
					fifo_full <= '0';
					fifo_empty <= '1';
				elsif store_count = N_words-1 then
					fifo_full <= '1';
					fifo_empty <= '0';
--				else
				end if;
				o_count <= conv_std_logic_vector(store_count,ADDR_WIDTH);			--WRITE OUT THE CURRENT NUMBER OF HITS

		end process pointer;		--}}}


		o_full <= fifo_full;
		o_empty <= fifo_empty;

	
	
end architecture quick;	--}}}
