onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix unsigned /fifo_tb/DO
add wave -noupdate -radix unsigned /fifo_tb/DI
add wave -noupdate /fifo_tb/WE
add wave -noupdate /fifo_tb/RE
add wave -noupdate /fifo_tb/rst
add wave -noupdate /fifo_tb/empty
add wave -noupdate /fifo_tb/full
add wave -noupdate -radix unsigned /fifo_tb/counts
add wave -noupdate /fifo_tb/CKB
add wave -noupdate -divider {dut signal}
add wave -noupdate -radix unsigned /fifo_tb/dut/i_DI
add wave -noupdate -radix unsigned /fifo_tb/dut/DI
add wave -noupdate -radix hexadecimal /fifo_tb/dut/o_DO
add wave -noupdate /fifo_tb/dut/i_wen
add wave -noupdate /fifo_tb/dut/i_ren
add wave -noupdate /fifo_tb/dut/i_rst
add wave -noupdate /fifo_tb/dut/o_empty
add wave -noupdate /fifo_tb/dut/o_full
add wave -noupdate -radix unsigned /fifo_tb/dut/A
add wave -noupdate -radix unsigned /fifo_tb/dut/B
add wave -noupdate -radix unsigned /fifo_tb/dut/DO
add wave -noupdate /fifo_tb/dut/WEB
add wave -noupdate /fifo_tb/dut/CSAN
add wave -noupdate /fifo_tb/dut/CSBN
add wave -noupdate /fifo_tb/dut/CKB
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {275000 ps} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {234741 ps} {461717 ps}
