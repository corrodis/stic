--=============================================================
--
--data_storing.vhdl
--ACQUISITION OF DATA FROM THE STIC CHANNELS AND STORING
--INTO A FIFO BUFFER
--
--Version Date: 30.05.2011
--
--Author: Tobias Harion
--



--	sys_clk
--           .----------.         .----------.         .----------.         .----------.
--  _________|          |_________|          |_________|          |_________|          |





--=============================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.VITAL_Primitives.all;
use IEEE.VITAL_Timing.all;
use std.textio.all;
use IEEE.std_logic_textio.all;


entity fifo_tb is
end entity fifo_tb;


architecture a of fifo_tb is




	component fifo is	--{{{
	generic( N_words : Integer := 64;	--NUMBER OF WORDS TO STORE IN THE FIFO
			 ADDR_WIDTH : Integer := 6;	--ADDRESS WIDTH OF THE COUNTER, ALLOWS TO IMPLEMENT TWO FIFO DEPTHS
		   	 N_bits : Integer := 71		--WIDTH OF THE WORDS TO BE STORED
		   );
	port (

				--DATA SIGNALS

				i_DI : in std_logic_vector(N_bits - 1 downto 0);
				o_DO : out std_logic_vector(N_bits - 1 downto 0);

				--CONTROL SIGNALS

				i_clk : in std_logic;		--CLOCK SIGNAL
				i_wen : in std_logic;		--WRITE ENABLE BIT: WRITES DATA TO THE CURRENT POSITION AND INCREMENTS THE WRITE POINTER
				i_ren : in std_logic;		--READ ENABLE BIT: INCREMENTS THE READ POINTER WITH THE CLK SIGNAL
				i_rst : in std_logic;		--RESET BIT: RESETS ONLY THE POINTERS SINCE THERE IS NO RESET FOR THE SRAM

				
				o_count : out std_logic_vector(ADDR_WIDTH -1 downto 0);	--NUMBER OF HITS STORED IN THE BUFFER
				o_empty : out std_logic;
				o_full : out std_logic
			
		 );
	end component; --}}}



signal DO,DI : std_logic_vector(77 downto 0);
signal WE, RE, rst, empty, full: std_logic;
signal CKB : std_logic := '1';
signal counts : std_logic_vector(5 downto 0);

begin


	CKB <= not CKB after 3125 ps;


dut: fifo
generic map(N_words => 64, ADDR_WIDTH => 6, N_bits => 78)
port map(
			i_DI => DI,
			o_DO => DO,
			i_clk => CKB,
			i_wen => WE,
			i_ren => RE,
			i_rst => rst,
			o_count => counts,
			o_empty => empty,
			o_full => full
		);





		write: process

		begin
			DI <= (others => '0');
			WE <= '0';
			RE <= '0';
			rst <= '1';			--RESET THE POINTERS ETC.
			wait for 10 ns;

			wait until rising_edge(CKB);
			rst <= '0';
			wait until rising_edge(CKB);
			for i in 15 to 30 loop
				DI <= conv_std_logic_vector(i,78);
				WE <= '1';
				wait until rising_edge(CKB);
			end loop;
			WE <= '0';
			wait for 50 ns;
			wait until rising_edge(CKB);
			RE <= '1';
			wait until rising_edge(CKB);
			RE <= '0';
			wait for 50 ns;
			RE<= '1';

			wait until empty = '1';
			RE <= '0';
			wait;

		end process;

end a;
