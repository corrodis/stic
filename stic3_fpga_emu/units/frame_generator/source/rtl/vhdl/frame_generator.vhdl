--=============================================================
--
--frame_generator.vhdl
--ASSEMBLY OF THE FIFO DATA INTO A FRAME AND TRANSMISSION TO THE SERIALIZER
--THE KOMMA SIGNALS ARE USED TO IDENTIFY HEADER AND TRAILER DATA
--
--Version Date: 06.06.2011
--
--Author: Tobias Harion
--
--
--	|HEADER IDENTIFIER|FRAMECOUNTER|DATA...|TRAIL IDENTIFIER|NUMBER OF HITS|
--
--=============================================================




library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;


entity frame_generator is	--{{{
	generic(N_bits : Integer := 48);
	port (
			--SYSTEM SIGNALS
			i_clk				: in std_logic;
			i_rst				: in std_logic;									--RESET SIGNAL
			i_rst_slow			: in std_logic;									--SYNC RESET
			i_start_trans		: in std_logic;									--SIGNAL FROM DAQ TO START TRANSMISSION

			--FIFO SIGNALS
			o_fifo_rd_en		: out std_logic;								--READ ENABLE TO THE FIFO
			i_fifo_count		: in std_logic_vector(6 downto 0);				--NUMBER OF STORED HITS IN THE FIFO
			i_fifo_empty		: in std_logic;									--FIFO EMPTY SIGNAL
			i_fifo_data			: in std_logic_vector(N_bits - 1 downto 0);		--THE DATA OUTPUT FROM THE FIFO


			--CONFIGURATION SIGNALS
			i_gen_idle_sig		: in std_logic;

			-- SERIALIZER SIGNALS
			o_ser_data			: out std_logic_vector(9 downto 0);				--BYTE TO THE SERIALIZER
			o_clk_s_d			: out std_logic


		 );
	end entity frame_generator; --}}}


architecture bit_patterns of frame_generator is --{{{1


	component clock_divider is --{{{2
	generic (Nd : Integer := 50);
	port(
			i_clk 		  : IN  STD_LOGIC;	--SYSTEM CLOCK
--			i_rst		  : IN std_logic;	--RESET SIGNAL
			o_div_clk_delay : out std_logic; --DIVIDED AND DELAYED CLOCK SIGNAL
			o_div_clk     : OUT STD_LOGIC	--DIVIDED CLOCK
		);
	end component clock_divider; --}}}

component encoder_module  is			--{{{2
	port (
				i_clk_s	: in std_logic;		--SLOW CLOCK
				i_rst	: in std_logic;		--SYNC RESET
				i_byte	: in std_logic_vector(7 downto 0);
				i_k		: in std_logic;
				o_enc_ready : out std_logic;
				o_enc_data : out std_logic_vector(9 downto 0)
		 );
	end component encoder_module ;		--}}}

----  SIGNAL DEFINITIONS	--{{{2

	--USE THE KOMMA SIGNALS FROM THE 8b/10b ENCODING AS IDENTIFIERS FOR HEADER AND TRAILER
	CONSTANT head_id : std_logic_vector(7 downto 0) := "00011100"; --K28.0	10 BIT: 1100001011
	CONSTANT trail_id : std_logic_vector(7 downto 0) := "10011100"; --K28.4
	CONSTANT komma_sync : std_logic_vector(7 downto 0) := "10111100"; --K28.5


	signal frame_count,next_frame_count : std_logic_vector(7 downto 0);		--FRAME AND EVENT COUNT AND THE CORRESPONDING SIGNALS OF THE STATEMACHINE
	--EVENT_NUMBER: Number of events currently in the frame buffer
	--EVENT_COUNT : Counter how many events have been transmitted
	signal event_number,next_event_number,event_count,next_event_count : std_logic_vector(6 downto 0);

	signal fifo_data_extended : std_logic_vector(47 downto 0); 		--THIS IS AN EXTENDED SIGNAL TO FILL THE 9th BYTE
	signal k_signal	: std_logic;

--	signal next_o_fifo_rd_en 	:std_logic;
	signal buf_getfifo1, buf_getfifo2 : std_logic;

	signal next_k_signal	: std_logic;

	signal initiate_transmission : std_logic;	--SET TO 1 WITH THE RISING EDGE OF THE i_start_trans SIGNAL
	signal reset_init_trans	: std_logic;		--RESET THE INIT_TRANSMISSION SIGNAL


	signal clk_s_del, clk_s : std_logic;	--BYTE CLOCK SIGNALS FOR THE ENCODER

	--STATEMACHINE STATES
	type fg_states is (	S_IDLE,				--IDLE STATE: TRANSMIT NOTHING OR AN IDLE SIGNAL
						S_RESET0,			--RESET STATES FOR THE ENCODER AND OTHER PARTS
						S_HEADER,			--TRANSMIT THE HEADER ID, GO TO S_FETCH AFTERWARDS
						S_FRAMECOUNT,
						S_BYTE0,
						S_BYTE1,
						S_BYTE2,
						S_BYTE3,
						S_BYTE4,
						S_BYTE5,
--						S_BYTE6,
--						S_BYTE7,
--						S_BYTE8,
						S_TRAILER,			--SEND THE TRAILER ID CONTINUE TO S_EVENTCOUNT
						S_EVENTCOUNT		--SEND THE NUMBER OF EVENTS IN THE FRAME AND RETURN TO S_IDLE
					);

	signal present_state, next_state : fg_states;


	type fifo_fetch_states is (FETCH_IDLE, FETCH_LOAD);		--THE FIFO HAS TO BE FETCHED WITH THE 160MHz Clock
	signal fetch_ps, fetch_ns : fifo_fetch_states;

	--THE SIGNALS FOR GENERATING THE RIGHT RISING EDGES FOR THE ENCODER
	signal byte, next_byte : std_logic_vector(7 downto 0);
	signal enc_rst,next_enc_rst : std_logic;

	signal enc_data : std_logic_vector(9 downto 0);
	signal enc_ready : std_logic;
	signal next_get_fifo, get_fifo : std_logic;		--INDICATE THAT THE FIFO NEEDS TO BE FETCHED

--}}}


begin

	fifo_data_extended (N_bits -1 downto 0) <= i_fifo_data;
	fifo_data_extended (47 downto N_bits) <= (others => '0');  -- ? don't needed
	o_ser_data <= enc_data;
	o_clk_s_d <= clk_s_del;			--OUTPUT THE DELAYED CLOCK FOR THE FRAME GENERATOR


	cdiv: clock_divider --{{{2
	generic map(Nd => 10)
	port map(
				i_clk => i_clk,
--				i_rst => i_rst,
				o_div_clk_delay =>  clk_s_del,
				o_div_clk => clk_s
			); --}}}


	enc_mod: encoder_module --{{{2
	port map(
				i_clk_s => clk_s,
				i_rst => enc_rst,
				i_byte => byte,
				i_k => k_signal,
				o_enc_data => enc_data,
				o_enc_ready => enc_ready
			); --}}}


----SYNCHRONOUS PART OF THE STATEMACHINE --{{{2
	frame_gen : process (clk_s_del )

	begin
				--SYNCHRONOUS PART
		if i_rst_slow = '1' then						--ASYNC RESET OF THE FSM
				present_state <= S_IDLE;
				--ALSO RESET ALL THE SIGNAL REGISTERS
				frame_count <= (others => '0');
				event_count <= (others => '0');
				k_signal <= '1';					--IN THE RESET STATE KEEP THE KOMMA SYNC SIGNAL
				byte <= komma_sync;
				enc_rst <= '1';
				get_fifo <= '0';

		elsif rising_edge(clk_s_del) then						--rising_edge because the fifo unit is sensitive to the falling edge
				present_state <= next_state;			--JUST GOT TO THE NEXT STATE AND TAKE THE NEW SIGNAL VALUES
				--ASSIGN THE NEW SIGNAL VALUES
				enc_rst <=  next_enc_rst;
				event_count <= next_event_count;
				frame_count <= next_frame_count;
				event_number <= next_event_number;
				k_signal <= next_k_signal;
				byte <= next_byte;
				get_fifo <= next_get_fifo;
		end if;

	end process frame_gen; --}}}
--

	signal_gen : process(	present_state,initiate_transmission,i_fifo_empty,i_fifo_count,frame_count,event_count,	--{{{2
				fifo_data_extended, byte,i_gen_idle_sig,enc_ready,event_number,k_signal)
	begin
			--GENERATE THE NEXT STATE AND THE OUTPUT SIGNALS
		next_state <= present_state;							--EVERYTHING FOR THE USUAL STATE
		next_frame_count <= frame_count;
		next_event_count <= event_count;

		next_k_signal <= k_signal;								--NORMALLY ITS NOT A CONTROL SIGNAL
		next_get_fifo <= '0';
		next_byte <= byte;
		next_enc_rst <= '0';
		next_event_number <= event_number;

		reset_init_trans <= '0';						--NORMALLY DONT RESET THE INIT TRANSMISSION SIGNAL

--
--
		case present_state is


			when S_IDLE =>								--WAIT FOR THE SIGNAL TO START A TRANSMISSION	{{{3

				next_byte <= komma_sync;				--WHILE IDLE KEEP THE SYNC SIGNAL AT THE ENCODER
				next_k_signal <= '1';

				next_event_number <= i_fifo_count;			--COPY THE EVENT NUMBER AND RESET THE EVENT COUNTER

				if i_gen_idle_sig = '1' then			--IF THE IDLE SIGNAL SHOULD BE GENERATED DO IT
					next_enc_rst <= '0';
				else
					next_enc_rst <= '1';				--IF THERE IS NO IDLE SIGNAL KEEP THE ENCODER IN RESET
				end if;


				if initiate_transmission = '1' then
					next_event_count <= i_fifo_count;

					if i_gen_idle_sig = '1' then		--IF THE ENCODER NEEDS THE RESET WAIT FOR IT TO BECOME READY
						next_state <= S_HEADER;
					else
						next_state <= S_RESET0;
					end if;

					if i_fifo_empty = '0' then
						next_get_fifo <= '1';
					end if;
				end if;

														--}}}


			when S_RESET0 =>		--WAIT FOR THE ENCODER TO BECOME READY {{{3
				next_enc_rst <= '0';
				next_k_signal <= '1';
				next_byte <= komma_sync;
				if enc_ready = '1' then
					next_state <= S_HEADER;
				end if;				--}}}


			when S_HEADER =>							--TRANSMIT THE HEADER	--{{{3
				--ASSIGN THE HEADER DATA

				next_k_signal <= '1';					--ONLY IF THE NEXT STATE IS S_FETCH THE K_SIGNALS GETS SET TO '0'
				next_byte <= head_id;
				reset_init_trans <= '1';				--WE HAVE STARTED THE TRANSMISSION OF THE HEADER WE CAN NOW RESET THE INITIATE_TRANSMISSION SIGNAL
				next_state <= S_FRAMECOUNT;				--ALWAYS APPEND THE FRAMECOUNT

													--}}}


			when S_FRAMECOUNT => 	--TRANSMIT THE CURRENT FRAMECOUNT{{{3
				next_k_signal <= '0';
				next_byte <= frame_count;

				if event_number /= "0000000" then				--IF THE FIFO IS EMPTY DONT SEND ANY DATA
					next_state <= S_BYTE0;
				else
					next_state <= S_TRAILER;
				end if;

										--}}}


			when S_BYTE0 =>			--TRANSMIT THE BYTES ONE AFTER THE OTHER		--{{{3
				next_byte <= fifo_data_extended(7 downto 0);
				next_state <= S_BYTE1;

			when S_BYTE1 =>
				next_byte <= fifo_data_extended(15 downto 8);
				next_state <= S_BYTE2;

			when S_BYTE2 =>
				next_byte <= fifo_data_extended(23 downto 16);
				next_state <= S_BYTE3;

			when S_BYTE3 =>
				next_byte <= fifo_data_extended(31 downto 24);
				next_state <= S_BYTE4;

			when S_BYTE4 =>
				next_byte <= fifo_data_extended(39 downto 32);
				next_state <= S_BYTE5;

			when S_BYTE5 =>
				next_byte <= fifo_data_extended(47 downto 40);

				if conv_integer(unsigned(event_count)) = 1 then	--ARE WE AT THE LAST ONE?
					next_state <= S_TRAILER;						--THEN DONT FETCH ANY MORE AND GO TO THE TRAILER
				else
					next_get_fifo <= '1';
					next_event_count <= conv_std_logic_vector(unsigned(event_count) - 1,7);
					next_state <= S_BYTE0;
				end if;

				--}}}


			when S_TRAILER => 			--TRANSMIT THE TRAILER ID		--{{{3

				next_frame_count <= conv_std_logic_vector(unsigned(frame_count) +1,8);	--INCREMENT THE FRAME COUNTER
				next_state <= S_EVENTCOUNT;

				next_k_signal <= '1';
				next_byte <= trail_id;	--TRAILER CONSISTS OF ID AND EVENT COUNTER	--}}}


			when S_EVENTCOUNT => 		--TRANSMIT THE EVENT COUNT IN THE CURRENT FRAME --{{{3

				next_byte <= '0' & event_number;		--THIS IS WHY WE STORED THE EVENT NUMBER. . .
				next_k_signal <= '0';
				next_state <= S_IDLE;	--}}}


			when others => null;
		end case;

	end process signal_gen; --}}}


	sample_start_trans : process (i_start_trans,reset_init_trans,i_rst)--STORE THE I_START_TRANS SIGNALS FOR THE SLOWER CLOCK --{{{2
	begin
		if (i_rst or reset_init_trans) = '1' then
			initiate_transmission <= '0';
		elsif rising_edge(i_start_trans) then
			initiate_transmission <= '1';
		end if;

	end process sample_start_trans; --}}}



gen_fifo_ren: process(i_clk)
begin

	if rising_edge(i_clk) then
		buf_getfifo1 <= get_fifo;
		buf_getfifo2 <= buf_getfifo1;
		o_fifo_rd_en <= buf_getfifo1 and not buf_getfifo2;
	end if;
end process;



end architecture bit_patterns ;		--}}}
