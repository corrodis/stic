onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -height 18 -group {testbench signals} /tb_fg_ds/N_bits
add wave -noupdate -height 18 -group {testbench signals} /tb_fg_ds/start_trans
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch0_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch1_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch2_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch3_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch4_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch5_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch6_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch7_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch8_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch9_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch10_data
add wave -noupdate -height 18 -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch11_data
add wave -noupdate -height 18 -group {testbench signals} -radix binary /tb_fg_ds/i_ch_dready
add wave -noupdate -height 18 -group {testbench signals} /tb_fg_ds/i_channel_mask
add wave -noupdate -height 18 -group {testbench signals} /tb_fg_ds/o_rst_channel
add wave -noupdate -height 18 -group {testbench signals} /tb_fg_ds/i_fifo_rden
add wave -noupdate -height 18 -group {testbench signals} -radix unsigned /tb_fg_ds/o_fifo_data
add wave -noupdate -height 18 -group {testbench signals} /tb_fg_ds/o_fifo_empty
add wave -noupdate -height 18 -group {testbench signals} -radix unsigned /tb_fg_ds/o_fifo_count
add wave -noupdate -height 18 -group {testbench signals} /tb_fg_ds/i_rst
add wave -noupdate -height 18 -group {testbench signals} /tb_fg_ds/clk
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch0_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch1_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch2_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch3_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch4_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch5_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch6_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch7_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch8_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch9_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch10_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch11_data
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/i_ch_dready
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/clk
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/i_channel_mask
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/o_rst_channel
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/o_fifo_data
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/o_fifo_count
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/mux_out_extended
add wave -noupdate -height 18 -group {data_storing signals} -radix unsigned /tb_fg_ds/dut1/mux_out_extended_s
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/fifo_data_out
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/any_dready_masked
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/rst_channel_int
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/fifo_count
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/fifo_full
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/fifo_empty
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/write_en
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/i_clk
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/i_rst
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/i_start_trans
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/o_fifo_rd_en
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/i_fifo_count
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/i_fifo_empty
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/i_fifo_data
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/i_gen_idle_sig
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/o_ser_data
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/frame_count
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_frame_count
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/event_number
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/event_count
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_event_count
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/fifo_data_extended
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/k_signal
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_o_serial_start
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_o_fifo_rd_en
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_k_signal
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/clk_s_del
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/clk_s
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/present_state
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_state
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/fetch_ps
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/fetch_ns
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/byte
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_byte
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/enc_rst
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_enc_rst
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/enc_data
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/enc_ready
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_get_fifo
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/get_fifo
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/N_words
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/N_bits
add wave -noupdate -height 18 -group {fifo signals} -radix unsigned /tb_fg_ds/dut1/memory/i_DI
add wave -noupdate -height 18 -group {fifo signals} -radix unsigned /tb_fg_ds/dut1/memory/o_DO
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/i_clk
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/i_wen
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/i_ren
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/i_rst
add wave -noupdate -height 18 -group {fifo signals} -radix unsigned /tb_fg_ds/dut1/memory/o_count
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/o_empty
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/o_full
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/A
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/B
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/DO
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/DI
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/WEB
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/CSAN
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/CSBN
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/CKB
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/i_clk_s
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/i_rst
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/i_byte
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/i_k
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/o_enc_ready
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/o_enc_data
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/enc_rst
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/next_enc_rst
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/next_o_enc_ready
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/present_state
add wave -noupdate -height 18 -expand -group {encoder module} /tb_fg_ds/dut2/enc_mod/next_state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {236936 ps} 0} {{Cursor 3} {1378125 ps} 0} {{Cursor 4} {20952482 ps} 0} {{Cursor 4} {7526265 ps} 0} {{Cursor 11} {2993494 ps} 0}
configure wave -namecolwidth 277
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {369121 ps}
