onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/N_bits
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/start_trans
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/ser_ack
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/serial_start
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch0_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch1_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch2_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch3_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch4_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch5_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch6_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch7_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch8_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch9_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch10_data
add wave -noupdate -height 18 -expand -group {testbench signals} -height 18 -group {tb channel data} -radix unsigned /tb_fg_ds/i_ch11_data
add wave -noupdate -height 18 -expand -group {testbench signals} -radix binary /tb_fg_ds/i_ch_dready
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/i_channel_mask
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/o_rst_channel
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/i_fifo_rden
add wave -noupdate -height 18 -expand -group {testbench signals} -radix unsigned /tb_fg_ds/o_fifo_data
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/o_fifo_empty
add wave -noupdate -height 18 -expand -group {testbench signals} -radix unsigned /tb_fg_ds/o_fifo_count
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/i_rst
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/ser_byte
add wave -noupdate -height 18 -expand -group {testbench signals} /tb_fg_ds/clk
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch0_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch1_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch2_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch3_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch4_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch5_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch6_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch7_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch8_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch9_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch10_data
add wave -noupdate -height 18 -group {data_storing signals} -height 18 -group {external channel input} /tb_fg_ds/dut1/i_ch11_data
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/i_ch_dready
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/clk
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/i_channel_mask
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/o_rst_channel
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/o_fifo_data
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/o_fifo_count
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/mux_out_extended
add wave -noupdate -height 18 -group {data_storing signals} -radix unsigned /tb_fg_ds/dut1/mux_out_extended_s
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/fifo_data_out
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/any_dready_masked
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/rst_channel_int
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/fifo_count
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/fifo_full
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/fifo_empty
add wave -noupdate -height 18 -group {data_storing signals} /tb_fg_ds/dut1/write_en
add wave -noupdate -height 18 -expand -group {frame generator signals} -radix unsigned /tb_fg_ds/dut2/i_fifo_count
add wave -noupdate -height 18 -expand -group {frame generator signals} -radix unsigned /tb_fg_ds/dut2/i_fifo_data
add wave -noupdate -height 18 -expand -group {frame generator signals} -radix unsigned /tb_fg_ds/dut2/frame_count
add wave -noupdate -height 18 -expand -group {frame generator signals} -radix unsigned /tb_fg_ds/dut2/event_count
add wave -noupdate -height 18 -expand -group {frame generator signals} -radix unsigned /tb_fg_ds/dut2/next_event_count
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/present_state
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_state
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/o_ser_byte
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/dut2/next_o_ser_byte
add wave -noupdate -height 18 -expand -group {frame generator signals} /tb_fg_ds/clk
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/N_words
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/N_bits
add wave -noupdate -height 18 -group {fifo signals} -radix unsigned /tb_fg_ds/dut1/memory/i_DI
add wave -noupdate -height 18 -group {fifo signals} -radix unsigned /tb_fg_ds/dut1/memory/o_DO
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/i_clk
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/i_wen
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/i_ren
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/i_rst
add wave -noupdate -height 18 -group {fifo signals} -radix unsigned /tb_fg_ds/dut1/memory/o_count
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/o_empty
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/o_full
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/A
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/B
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/DO
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/DI
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/WEB
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/CSAN
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/CSBN
add wave -noupdate -height 18 -group {fifo signals} /tb_fg_ds/dut1/memory/CKB
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {21875 ps} 0} {{Cursor 3} {357960 ps} 0} {{Cursor 4} {20952482 ps} 0} {{Cursor 4} {4137648 ps} 0} {{Cursor 11} {512847 ps} 0}
configure wave -namecolwidth 277
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {516066 ps} {575759 ps}
