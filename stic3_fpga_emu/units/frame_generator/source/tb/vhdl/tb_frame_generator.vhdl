library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.VITAL_Primitives.all;
use IEEE.VITAL_Timing.all;
use std.textio.all;
use IEEE.std_logic_textio.all;
use IEEE.std_logic_arith.all;



entity tb_frame_generator is
	generic(N_bits : Integer := 36);		--GENERIC WORD WIDTH
end entity tb_frame_generator;



architecture a  of tb_frame_generator is --{{{1

	component frame_generator is	--{{{2
		generic(N_bits : Integer := 36);
		port (
				--SYSTEM SIGNALS
				i_clk				: in std_logic;
				i_rst				: in std_logic;									--RESET SIGNAL
				i_start_trans		: in std_logic;									--SIGNAL FROM DAQ TO START TRANSMISSION

				--FIFO SIGNALS
				o_fifo_rd_en		: out std_logic;								--READ ENABLE TO THE FIFO
				i_fifo_count		: in std_logic_vector(5 downto 0);				--NUMBER OF STORED HITS IN THE FIFO
--				i_fifo_full			: in std_logic;									--FIFO FULL SIGNAL
				i_fifo_empty		: in std_logic;									--FIFO EMPTY SIGNAL
				i_fifo_data			: in std_logic_vector(N_bits - 1 downto 0);		--THE DATA OUTPUT FROM THE FIFO

				-- SERIALIZER SIGNALS
				i_ser_ack			: in  std_logic;								--REQUEST FOR THE NEXT SIGNAL
				o_k_signal			: out std_logic;								--KOMMA SIGNAL TO IDENTIFY HEADER AND TRAILER
				o_ser_data			: out std_logic_vector(N_bits - 1 downto 0)		--DATA TO THE SERIALIZER


			 );
		end component frame_generator; --}}}



--SIGNAL DECLARATIONS 										{{{2
			--SYSTEM SIGNALS
			signal clk				:  std_logic := '0';
			signal rst				:  std_logic;									--RESET SIGNAL
			signal start_trans		:  std_logic;									--SIGNAL FROM DAQ TO START TRANSMISSION

			--FIFO SIGNALS
			signal fifo_rd_en		:  std_logic;								--READ ENABLE TO THE FIFO
			signal fifo_count		:  std_logic_vector(5 downto 0);				--NUMBER OF STORED HITS IN THE FIFO
--			signal fifo_full		:  std_logic;									--FIFO FULL SIGNAL
			signal fifo_empty		:  std_logic;									--FIFO EMPTY SIGNAL
			signal fifo_data		:  std_logic_vector(N_bits - 1 downto 0);		--THE DATA OUTPUT FROM THE FIFO

			-- SERIALIZER SIGNALS
			signal ser_ack			:  std_logic;								--REQUEST FOR THE NEXT SIGNAL
			signal k_signal			:  std_logic;								--KOMMA SIGNAL TO IDENTIFY HEADER AND TRAILER
			signal ser_data			:  std_logic_vector(N_bits - 1 downto 0);		--DATA TO THE SERIALIZER
---}}}

begin



dut: frame_generator	--{{{2
port map(
				--SYSTEM SIGNALS
				i_clk			=> 	clk,
				i_rst			=> 	rst,
				i_start_trans	=> 	start_trans,

				--FIFO SIGNALS
				o_fifo_rd_en	=> 	fifo_rd_en,
				i_fifo_count	=> 	fifo_count,
--				i_fifo_full		=> 	fifo_full,
				i_fifo_empty	=> 	fifo_empty,
				i_fifo_data		=> 	fifo_data,

				-- SERIALIZER SIGNALS
				i_ser_ack		=> 	ser_ack,
				o_k_signal		=> 	k_signal,
				o_ser_data		=> 	ser_data
			
		);				--}}}

clk <=  not clk after 3.125 ns;

sim : process
begin
	ser_ack <= '0';
	start_trans <= '0';
	rst <= '1';
	fifo_count <= conv_std_logic_vector(4,6);
--	fifo_full <= '0';
	fifo_empty <=  '0';
	fifo_data <= conv_std_logic_vector(157,N_bits);
	
	wait until falling_edge(clk);
	wait until rising_edge(clk);
	rst <= '0';
	wait for 10 ns;
	wait until rising_edge(clk);
	start_trans <= '1';
	wait until rising_edge(clk);
	start_trans <= '0';
	wait for 26 ns;
	wait until rising_edge(clk);
	for i in 0 to 4 loop
		ser_ack <= '1';
		wait until rising_edge(clk);
		ser_ack <= '0';
		wait for 27 ns;
		wait until rising_edge(clk);
	end loop;
	wait until k_signal = '1';

	wait for 10 ns;
	wait until rising_edge(clk);
	ser_ack <= '1';
	wait until rising_edge(clk);
	ser_ack <= '0';
	wait;
	
end process sim;

end architecture a ; --}}}
