Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity diff_trigger_1V5 is
	port(
			i_clk : in std_logic; --SYSTEM CLOCK
			o_trigger_clk : out std_logic;
			o_trigger_clk_n : out std_logic
	    );
end diff_trigger_1V5;




architecture a of diff_trigger_1V5 is


	signal counter : std_logic_vector(15 downto 0);
	signal s_trigger : std_logic;


begin

	o_trigger_clk <= s_trigger;
	o_trigger_clk_n <= not s_trigger;

	cnt_down : process (i_clk)
	begin
		if rising_edge(i_clk) then

			counter <= std_logic_vector(unsigned(counter)-1);
			if counter = X"0810" then
				s_trigger <= '1';
			end if;
			if counter = X"0808" then
				s_trigger <= '0';
			end if;
			if counter = X"0800" then
				s_trigger <= '1';
			end if;
			if counter = X"0000" then
				s_trigger <= '0';
				counter <= X"FFFF";
			end if;
		end if;

	end process cnt_down;

end;

