--	PACKAGE CONTAINING HELPER FUNCTIONS AND TYPES FOR THE STIC3 ASIC
--
--	Author: Tobias
--  Date: Fri Jul 26 15:04:06 CEST 2013
--
--

Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.MATH_REAL.all;

package stic2_vhdl_package is


	constant N_CHANNELS : INTEGER := 64; --THE NUMBER OF CHANNELS USED IN THE ASIC
	constant N_GROUPS : INTEGER := 4;
	constant N_CONF_BITS : INTEGER := 4657;
	constant N_CHAN_PER_GROUP : INTEGER := N_CHANNELS/N_GROUPS;

	--using log is not a good idea...
--	constant GROUP_NUM_WIDTH : INTEGER := INTEGER(FLOOR(LOG(2, N_GROUPS)));
	constant GROUP_NUM_WIDTH : INTEGER := 2;


	--SPI CONFIGURATION CONSTANTS USED IN THE FUNCTIONS
	constant ch_pattern_offset : INTEGER :=19;
	constant ch_pattern_len : INTEGER :=69;	--ANALOG CHANNEL BITS + 1 MASK BIT

	constant ch_tdc_config_len : INTEGER := 168;
	--BITPATTERN LEN OF THE MONITOR CONFIGURATIONS
	constant ch_monitor_config_len : INTEGER := 54;



	--STiC TDC DATA OF ONE EVENT RECORDED BY THE HIT COLLECTOR


	type type_data_L1 is
		record
			channel : std_logic_vector(5 downto 0);	--CHANNEL NUMBER
			TCCM : std_logic_vector(14 downto 0); --TIME CCM VALUE
			TCCS : std_logic_vector(14 downto 0); --TIME CCM VALUE
			T_Fine : std_logic_vector(4 downto 0); --FINECOUNTER VALUE
			T_BadHit : std_logic;					--BAD HIT FLAG
			ECCM : std_logic_vector(14 downto 0);	--ENERGY MEASUREMENT SIGNAL
			ECCS : std_logic_vector(14 downto 0);	--ENERGY MEASUREMENT SIGNAL
			E_Fine : std_logic_vector(4 downto 0);
			E_BadHit : std_logic;
		end record;


	type type_data_L2 is
		record
			channel : std_logic_vector(5 downto 0);	--CHANNEL NUMBER
			TCC : std_logic_vector(14 downto 0); --TIME CCM VALUE
			T_Fine : std_logic_vector(4 downto 0); --FINECOUNTER VALUE
			T_BadHit : std_logic;					--BAD HIT FLAG
			ECC : std_logic_vector(14 downto 0);	--ENERGY MEASUREMENT SIGNAL
			E_Fine : std_logic_vector(4 downto 0);
			E_BadHit : std_logic;
		end record;

	type type_tdc_data is	--USED ONLY FROM THE TDC CHANNEL TO THE CH_HIT_RECEIVER
		record
			CCM : std_logic_vector(14 downto 0);
			CCS : std_logic_vector(14 downto 0);
			Fine : std_logic_vector(15 downto 0);
		end record;



	type type_tdc_encoded is	--AS USED IN THE REST OF THE DIGITAL PART --{{{
		record
			Fine : std_logic_vector(4 downto 0);	--INDEX (4 downto 0)
			CCS : std_logic_vector(14 downto 0);	--INDEX (19 downto 5)
			CCM : std_logic_vector(14 downto 0);	--INDEX (34 downto 20)
			BadHit : std_logic;						--INDEX (35)
		end record;	--}}}

	--DATA ARRAY CONTAINING ALL TDC DATA
	type tdc_data_array is array(N_CHANNELS - 1 downto 0) of type_tdc_data;

	--DATA ARRAY FOR THE LEVEL 1 GROUPS
	type tdc_group_data is array(N_CHAN_PER_GROUP - 1 downto 0) of type_tdc_data;

	--DATA ARRAY OF THE EVENTS IN THE GROUPS
	type t_event_array is array (0 to N_CHAN_PER_GROUP -1) of std_logic_vector(71 downto 0);

	--HALF OF THE EVENTS FOR PIPELINING THE MULTIPLEXING
	type t_event_halfarray is array (0 to N_CHAN_PER_GROUP/2 -1) of std_logic_vector(71 downto 0);


	--FIFO BUFFERS FROM L1 TO L2
	type t_l1_data_array is array (N_GROUPS-1 downto 0) of std_logic_vector(77 downto 0);


	function vector_to_tdc(vec : std_logic_vector(35 downto 0)) return type_tdc_encoded;
	function tdc_to_vector(data : type_tdc_data) return std_logic_vector;
	function vector_to_tdc(vec : std_logic_vector(45 downto 0)) return type_tdc_data;
	function vector_to_L2(vec : std_logic_vector(47 downto 0)) return type_data_L2;

	function to_vector(data : type_data_L2) return std_logic_vector;

	function to_vector(data : type_data_L1) return std_logic_vector;

	function to_L1_data(vec : std_logic_vector(77 downto 0)) return type_data_L1;

	function tdc_to_group(group_number : integer; data : tdc_data_array) return tdc_group_data;



	--CAUTION!!! THE BITS IN THE ANALOG PART ARE USUALLY DEFINED FROM LSB TO MSB
	type dacs_2bit is array (N_CHANNELS - 1 downto 0) of std_logic_vector(0 to 1);
	type dacs_3bit is array (N_CHANNELS - 1 downto 0) of std_logic_vector(0 to 2);
	type dacs_4bit is array (N_CHANNELS - 1 downto 0) of std_logic_vector(0 to 3);
	type dacs_5bit is array (N_CHANNELS - 1 downto 0) of std_logic_vector(0 to 4);
	type dacs_6bit is array (N_CHANNELS - 1 downto 0) of std_logic_vector(0 to 5);
	type dacs_7bit is array (N_CHANNELS - 1 downto 0) of std_logic_vector(0 to 6);
	type dacs_8bit is array (N_CHANNELS - 1 downto 0) of std_logic_vector(0 to 7);
	type dacs_9bit is array (N_CHANNELS - 1 downto 0) of std_logic_vector(0 to 8);
	type dacs_10bit is array (N_CHANNELS - 1 downto 0) of std_logic_vector(0 to 9);



	--THE SPI CONFIGURATION CONTAINED IN ONE RECORD
	type stic3_spi_config is --{{{
		record
	--==============DIGITAL OUTPUT PINS========

			gen_idle_signal	: std_logic;
			recv_rec_all : std_logic;

			ms_limits : std_logic_vector(9 downto 0);
			ms_debug : std_logic;
			ms_switch_sel : std_logic;

			pll1_SetCoarse : std_logic;
			pll2_SetCoarse : std_logic;

			pll1_EnVCOMonitor : std_logic;
			pll2_EnVCOMonitor : std_logic;

			disable_coarse		: std_logic; --NOT CONNECTED IN THE TDC

			channel_mask		: std_logic_vector(0 to N_CHANNELS-1); --TODO: CHECK IF THIS IS NEEDED FOR ANALOG PART
	--==============ANALOG PART OUTPUT PINS====================		--{{{
			--ANALOG CHANNEL PINS
			Anode_flag : std_logic_vector(N_CHANNELS-1 downto 0);
			Cathode_flag : std_logic_vector(N_CHANNELS-1 downto 0);
			S_switch : std_logic_vector(N_CHANNELS-1 downto 0);
			SorD : std_logic_vector(N_CHANNELS-1 downto 0);
			SorD_not : std_logic_vector(N_CHANNELS-1 downto 0);
			edge  : std_logic_vector(N_CHANNELS-1 downto 0);
			edge_cml  : std_logic_vector(N_CHANNELS-1 downto 0);
			DAC_cmlscale  : std_logic_vector(N_CHANNELS-1 downto 0);

			comp_spi : dacs_2bit;

			DAC_SiPM : dacs_7bit;
			DAC_Tthresh : dacs_9bit;
			DAC_ampcom : dacs_8bit;
			DAC_inputbias : dacs_7bit;
			DAC_Ethresh : dacs_8bit;
			DAC_pole : dacs_7bit;

			DAC_cml : dacs_4bit;
			DAC_delay : dacs_2bit;

			amon_ctrl : dacs_3bit;
			dmon_ena : std_logic_vector(N_CHANNELS-1 downto 0);
			dmon_sw : std_logic_vector(N_CHANNELS-1 downto 0);
			tdctest : std_logic_vector(N_CHANNELS-1 downto 0);


			--}}}

			dac_tdc_bias_left : std_logic_vector(0 to 71);
			dac_tdc_bias_right : std_logic_vector(0 to 71);

			dac_tdc_latchbias_left : std_logic_vector(11 downto 0);
			dac_tdc_latchbias_right : std_logic_vector(11 downto 0);

			--ANALOG MONITOR PINS
			amon_left_en : std_logic;
			amon_right_en : std_logic;
			amon_left_DAC : std_logic_vector(0 to 7);
			amon_right_DAC : std_logic_vector(0 to 7);

			dig_mon1_en_right : std_logic;
			dig_mon1_DAC_right : std_logic_vector(0 to 7);

			dig_mon1_en_left : std_logic;
			dig_mon1_DAC_left : std_logic_vector(0 to 7);

			dig_mon2_en_right : std_logic;
			dig_mon2_DAC_right : std_logic_vector(0 to 7);

			dig_mon2_en_left : std_logic;
			dig_mon2_DAC_left : std_logic_vector(0 to 7);

			end record; --}}}



	--FUNCTIONS TO CONVERT THE CONFIGURATION TO A LOGIC VECTOR AND VICE VERSA
	function config_to_vector (config : stic3_spi_config) return std_logic_vector;
	function vector_to_config (config_vector : std_logic_vector(0 to N_CONF_BITS - 1)) return stic3_spi_config;




	--FUNCTIONS TO CREATE (AND DECODE) THERMETRIC CODE
	function to_therm (value : integer) return std_logic_vector;
--	function to_integer (code : std_logic_vector(15 downto 0) return integer;

end;	--package



package body stic2_vhdl_package is

	--SPI CONFIG FUNCTIONS
	function vector_to_config (config_vector : std_logic_vector(0 to N_CONF_BITS-1) ) return stic3_spi_config is	--{{{

		variable current_offset : INTEGER;
		variable config : stic3_spi_config;

	begin
			current_offset := 0;

			config.gen_idle_signal := config_vector(current_offset);
			current_offset := current_offset+1;

			for i in 0 to 9 loop
				config.ms_limits(i) := config_vector(i+current_offset);
			end loop;
			current_offset := current_offset+10;


			config.ms_switch_sel := config_vector(current_offset);
			current_offset := current_offset+1;

			config.ms_debug := config_vector(current_offset);
			current_offset := current_offset+1;

			config.recv_rec_all := config_vector(current_offset);
			current_offset := current_offset+1;


			config.pll1_SetCoarse := config_vector(current_offset);
			current_offset := current_offset+1;
			config.pll2_SetCoarse := config_vector(current_offset);
			current_offset := current_offset+1;

			config.pll1_EnVCOMonitor := config_vector(current_offset);
			current_offset := current_offset+1;
			config.pll2_EnVCOMonitor := config_vector(current_offset);
			current_offset := current_offset+1;

			config.disable_coarse := config_vector(current_offset);



			for i in 0 to N_CHANNELS -1 loop

				--There is no longer the configuration in the middle of the chip for global bias
				current_offset := i*ch_pattern_len+ch_pattern_offset;

				config.Anode_flag(i) := config_vector(current_offset);
				current_offset := current_offset+1;

				config.Cathode_flag(i) := config_vector(current_offset);
				current_offset := current_offset+1;

				config.S_switch(i) := config_vector(current_offset);
				current_offset := current_offset+1;

				config.SorD(i) := config_vector(current_offset);
				current_offset := current_offset+1;

				config.SorD_not(i) := config_vector(current_offset);
				current_offset := current_offset+1;

				config.edge(i) := config_vector(current_offset);
				current_offset := current_offset+1;

				config.edge_cml(i) := config_vector(current_offset);
				current_offset := current_offset+1;

				config.DAC_cmlscale(i) := config_vector(current_offset);
				current_offset := current_offset+1;

				config.dmon_ena(i) := config_vector(current_offset);
				current_offset := current_offset+1;

				config.dmon_sw(i) := config_vector(current_offset);
				current_offset := current_offset+1;

				config.tdctest(i) := config_vector(current_offset);
				current_offset := current_offset+1;



				config.amon_ctrl(i) := config_vector( current_offset to current_offset+2);
				current_offset := current_offset+3;

				config.comp_spi(i) := config_vector( current_offset to current_offset+1);
				current_offset := current_offset+2;

				config.DAC_SiPM(i) := config_vector( current_offset to current_offset+6);
				current_offset := current_offset+7;

				config.DAC_Tthresh(i) := config_vector( current_offset to current_offset+8);
				current_offset := current_offset+9;

				config.DAC_ampcom(i) := config_vector( current_offset to current_offset+7);
				current_offset := current_offset+8;

				config.DAC_inputbias(i) := config_vector( current_offset to current_offset+6);
				current_offset := current_offset+7;

				config.DAC_Ethresh(i) := config_vector( current_offset to current_offset+7);
				current_offset := current_offset+8;

				config.DAC_pole(i) := config_vector( current_offset to current_offset+6);
				current_offset := current_offset+7;

				config.DAC_cml(i) := config_vector( current_offset to current_offset+3);
				current_offset := current_offset+4;

				config.DAC_delay(i) := config_vector( current_offset to current_offset+1);
				current_offset := current_offset+2;

				config.channel_mask(i) := config_vector(current_offset);
				current_offset := current_offset+1;

			end loop;


			--TDC BIAS CONFIGURATION
			config.dac_tdc_bias_left := config_vector(current_offset to current_offset+71);
			current_offset := current_offset + 72;

			config.dac_tdc_bias_right := config_vector(current_offset to current_offset+71);
			current_offset := current_offset + 72;

			--INVERT THIS BECAUSE THE 12 BIT DAC IS DEFINED WITH DOWNTO
			for i in 0 to 11 loop
				config.dac_tdc_latchbias_left(i) := config_vector(i+current_offset);
			end loop;
			current_offset := current_offset + 12;

			for i in 0 to 11 loop
				config.dac_tdc_latchbias_right(i) := config_vector(i+current_offset);
			end loop;
			current_offset := current_offset + 12;


			config.amon_left_en := config_vector(current_offset);
			current_offset := current_offset + 1;

			config.amon_left_DAC := config_vector(current_offset to current_offset+7);
			current_offset := current_offset + 8;

			config.amon_right_en := config_vector(current_offset);
			current_offset := current_offset + 1;

			config.amon_right_DAC := config_vector(current_offset to current_offset+7);
			current_offset := current_offset + 8;


			config.dig_mon1_en_left := config_vector(current_offset);
			current_offset := current_offset + 1;

			config.dig_mon1_DAC_left := config_vector(current_offset to current_offset+7);
			current_offset := current_offset + 8;

			config.dig_mon1_en_right := config_vector(current_offset);
			current_offset := current_offset + 1;

			config.dig_mon1_DAC_right := config_vector(current_offset to current_offset+7);
			current_offset := current_offset + 8;


			config.dig_mon2_en_left := config_vector(current_offset);
			current_offset := current_offset + 1;

			config.dig_mon2_DAC_left := config_vector(current_offset to current_offset+7);
			current_offset := current_offset + 8;

			config.dig_mon2_en_right := config_vector(current_offset);
			current_offset := current_offset + 1;

			config.dig_mon2_DAC_right := config_vector(current_offset to current_offset+7);
			current_offset := current_offset + 8;





			return config;

	end function; --}}}

	function config_to_vector (config : stic3_spi_config) return std_logic_vector is --{{{

		variable current_offset : INTEGER;
		variable config_vector : std_logic_vector(0 to N_CONF_BITS - 1);

	begin
			current_offset := 0;

			config_vector(current_offset) := config.gen_idle_signal;
			current_offset := current_offset+1;

			for i in 0 to 9 loop
				config_vector(i+current_offset) := config.ms_limits(i);
			end loop;
			current_offset := current_offset+10;


			config_vector(current_offset) := config.ms_switch_sel;
			current_offset := current_offset+1;

			config_vector(current_offset) := config.ms_debug;
			current_offset := current_offset+1;

			config_vector(current_offset) := config.recv_rec_all;
			current_offset := current_offset+1;


			config_vector(current_offset) := config.pll1_SetCoarse;
			current_offset := current_offset+1;
			config_vector(current_offset) := config.pll2_SetCoarse;
			current_offset := current_offset+1;

			config_vector(current_offset) := config.pll1_EnVCOMonitor;
			current_offset := current_offset+1;
			config_vector(current_offset) := config.pll2_EnVCOMonitor;
			current_offset := current_offset+1;

			config_vector(current_offset) := config.disable_coarse;



			for i in 0 to N_CHANNELS -1 loop

				--There is no longer the configuration in the middle of the chip for global bias
				current_offset := i*ch_pattern_len+ch_pattern_offset;

				config_vector(current_offset) := config.Anode_flag(i);
				current_offset := current_offset+1;

				config_vector(current_offset) := config.Cathode_flag(i);
				current_offset := current_offset+1;

				config_vector(current_offset) := config.S_switch(i);
				current_offset := current_offset+1;

				config_vector(current_offset) := config.SorD(i);
				current_offset := current_offset+1;

				config_vector(current_offset) := config.SorD_not(i);
				current_offset := current_offset+1;

				config_vector(current_offset) := config.edge(i);
				current_offset := current_offset+1;

				config_vector(current_offset) := config.edge_cml(i);
				current_offset := current_offset+1;

				config_vector(current_offset) := config.DAC_cmlscale(i);
				current_offset := current_offset+1;


				config_vector(current_offset) := config.dmon_ena(i);
				current_offset := current_offset+1;

				config_vector(current_offset) := config.dmon_sw(i);
				current_offset := current_offset+1;

				config_vector(current_offset) := config.tdctest(i);
				current_offset := current_offset+1;


				config_vector( current_offset to current_offset+2) := config.amon_ctrl(i);
				current_offset := current_offset+3;



				config_vector( current_offset to current_offset+1) := config.comp_spi(i);
				current_offset := current_offset+2;

				config_vector( current_offset to current_offset+6) := config.DAC_SiPM(i);
				current_offset := current_offset+7;

				config_vector( current_offset to current_offset+8) := config.DAC_Tthresh(i);
				current_offset := current_offset+9;

				config_vector( current_offset to current_offset+7) := config.DAC_ampcom(i);
				current_offset := current_offset+8;

				config_vector( current_offset to current_offset+6) := config.DAC_inputbias(i);
				current_offset := current_offset+7;

				config_vector( current_offset to current_offset+7) := config.DAC_Ethresh(i);
				current_offset := current_offset+8;

				config_vector( current_offset to current_offset+6) := config.DAC_pole(i);
				current_offset := current_offset+7;

				config_vector( current_offset to current_offset+3) := config.DAC_cml(i);
				current_offset := current_offset+4;

				config_vector( current_offset to current_offset+1) := config.DAC_delay(i);
				current_offset := current_offset+2;

				config_vector(current_offset) := config.channel_mask(i);
				current_offset := current_offset+1;

			end loop;


			--TDC BIAS CONFIGURATION
			config_vector(current_offset to current_offset+71) := config.dac_tdc_bias_left;
			current_offset := current_offset + 72;

			config_vector(current_offset to current_offset+71) := config.dac_tdc_bias_right;
			current_offset := current_offset + 72;

			--INVERT THIS BECAUSE THE 12 BIT DAC IS DEFINED WITH DOWNTO
			for i in 0 to 11 loop
				config_vector(i+current_offset) := config.dac_tdc_latchbias_left(i);
			end loop;
			current_offset := current_offset + 12;

			for i in 0 to 11 loop
				config_vector(i+current_offset) := config.dac_tdc_latchbias_right(i);
			end loop;
			current_offset := current_offset + 12;


			config_vector(current_offset) := config.amon_left_en;
			current_offset := current_offset + 1;

			config_vector(current_offset to current_offset+7) := config.amon_left_DAC;
			current_offset := current_offset + 8;

			config_vector(current_offset) := config.amon_right_en;
			current_offset := current_offset + 1;

			config_vector(current_offset to current_offset+7) := config.amon_right_DAC;
			current_offset := current_offset + 8;


			config_vector(current_offset) := config.dig_mon1_en_left;
			current_offset := current_offset + 1;

			config_vector(current_offset to current_offset+7) := config.dig_mon1_DAC_left;
			current_offset := current_offset + 8;

			config_vector(current_offset) := config.dig_mon1_en_right;
			current_offset := current_offset + 1;

			config_vector(current_offset to current_offset+7) := config.dig_mon1_DAC_right;
			current_offset := current_offset + 8;


			config_vector(current_offset) := config.dig_mon2_en_left;
			current_offset := current_offset + 1;

			config_vector(current_offset to current_offset+7) := config.dig_mon2_DAC_left;
			current_offset := current_offset + 8;

			config_vector(current_offset) := config.dig_mon2_en_right;
			current_offset := current_offset + 1;

			config_vector(current_offset to current_offset+7) := config.dig_mon2_DAC_right;
			current_offset := current_offset + 8;


			return config_vector;

	end function; --}}}


	--TDC DATA CONVERSION FUNCTIONS
	function tdc_to_vector(data : type_tdc_data) return std_logic_vector is --{{{
		variable vec : std_logic_vector(45 downto 0);
	begin
		vec(15 downto 0) := data.Fine;
		vec(30 downto 16) := data.CCS;
		vec(45 downto 31) := data.CCM;
--		vec(36) := data.Eflag;
		return vec;
	end; --}}}

	function vector_to_tdc(vec : std_logic_vector(45 downto 0)) return type_tdc_data is --{{{
		variable data : type_tdc_data;
	begin
		data.Fine := vec(15 downto 0);
		data.CCS := vec(30 downto 16);
		data.CCM := vec(45 downto 31);
--		data.Eflag := vec(36);
		return data;
	end;	--}}}

	function vector_to_tdc(vec : std_logic_vector(35 downto 0)) return type_tdc_encoded is --{{{
		variable data : type_tdc_encoded;
	begin
		data.Fine := vec(4 downto 0);
		data.CCS := vec(19 downto 5);
		data.CCM := vec(34 downto 20);
		data.BadHit := vec(35);
		return data;
	end function;		--}}}


        function vector_to_L2(vec : std_logic_vector(47 downto 0)) return type_data_L2 is --{{{
                variable data : type_data_L2;
        begin
		data.channel := vec(47 downto 42);
		data.T_BadHit := vec(41);
		data.TCC := vec(40 downto 26);
		data.T_Fine := vec(25 downto 21);
		data.E_BadHit := vec(20);
		data.ECC := vec(19 downto 5);
		data.E_Fine := vec(4 downto 0);
                return data;
        end;    --}}}

        function to_vector(data : type_data_L2) return std_logic_vector is --{{{
			variable vec : std_logic_vector(47 downto 0);
        begin
			vec(47 downto 42) := data.channel;
			vec(41) := data.T_BadHit;
			vec(40 downto 26) := data.TCC;
			vec(25 downto 21) := data.T_Fine;
			vec(20) := data.E_BadHit;
			vec(19 downto 5) := data.ECC;
			vec(4 downto 0) := data.E_Fine;
			return vec;
        end;    --}}}

        function to_L1_data(vec : std_logic_vector(77 downto 0)) return type_data_L1 is --{{{
                variable data : type_data_L1;
        begin

			data.channel := vec(77 downto 72);

			data.T_BadHit := vec(71);
			data.TCCM := vec(70 downto 56);
			data.TCCS := vec(55 downto 41);
			data.T_Fine := vec(40 downto 36);

			data.E_BadHit := vec(35);
			data.ECCM := vec(34 downto 20);
			data.ECCS := vec(19 downto 5);
			data.E_Fine := vec(4 downto 0);
			return data;
        end;    --}}}

        function to_vector(data : type_data_L1) return std_logic_vector is --{{{
			variable vec : std_logic_vector(77 downto 0);
        begin
			vec(77 downto 72) := data.channel;

			vec(71) := data.T_BadHit;
			vec(70 downto 56) := data.TCCM;
			vec(55 downto 41) := data.TCCS;
			vec(40 downto 36) := data.T_Fine;

			vec(35) := data.E_BadHit;
			vec(34 downto 20) := data.ECCM;
			vec(19 downto 5) := data.ECCS;
			vec(4 downto 0) := data.E_Fine;
			return vec;
        end;    --}}}



	function tdc_to_group(group_number : integer; data : tdc_data_array) return tdc_group_data is --{{{
		variable group_data : tdc_group_data;
	begin

		for i in 0 to N_CHAN_PER_GROUP-1 loop
			group_data(i) := data(group_number*N_CHAN_PER_GROUP+i);
		end loop;

		return group_data;
	end;	--}}}



	--FUNCTIONS TO GENERATE THERMOMETRIC CODE
	function to_therm (value : integer) return std_logic_vector is --{{{
		variable vector : std_logic_vector(15 downto 0);
	begin

		if value < 16 then
		   for i in 0 to 15 loop
			   if i < value then
				   vector(i) := '1';
			   else
				   vector(i) := '0';
			   end if;
		   end loop;
	   else
		   for i in 0 to 15 loop
			   if i+16 < value then
				   vector(i) := '0';
			   else
				   vector(i) := '1';
			   end if;
		   end loop;
	   end if;

	   return vector;

	end;	--}}}



end package body;
