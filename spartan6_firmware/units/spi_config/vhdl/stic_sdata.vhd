--	Entity for sending configuration data to the stic chip
--
--
--
--
--
--


Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;

entity stic_sdata is
generic(
	C_CLK_DIVIDE : integer := 10				--CLOCK DIVISION FACTOR FOR THE SYSTEM TO SPI CLOCK DIVISION
);
port(
	data : in std_logic_vector(31 downto 0);	--configuration data
	i_dlength : in std_logic_vector(4 downto 0);	--length of the configuration data
	clk : in std_logic;				--100MHz clk from the AXI Bus
	start: in std_logic;				--start signal. transmission will start when this signal gets high
	RX_BIT: in std_logic;				--input from the end of the daisy chain of the stic chip
	busy : out std_logic;				--transmisstion in progress signal. NEGATIVE LOGIC
	TX_Bit : out std_logic;				--output to the stic daisy chain
	chain_clk : out std_logic;			--clock signal for the daisy chain
	data_old : out std_logic_vector(31 downto 0)	--received configuration data

);
end stic_sdata;


architecture a of stic_sdata is


------- SIGNAL DEFINITIONS

signal d_i     : std_logic_vector(31 downto 0);
signal d_old     : std_logic_vector(31 downto 0);
signal eot     : std_logic;


type s_state is (S_TRANSMIT,S_INIT,S_STOP);	--define the states of the statemachine
signal present_st, next_st : s_state;

signal clk1kHz : std_logic := '0';
signal en_sample : std_logic;
--signal err : std_logic := '0';
signal enable_chain : std_logic;


signal counter : std_logic_vector(4 downto 0);


component clk_div is
generic (Nd : Integer := 50);
PORT(
        clk, en : IN  STD_LOGIC;
        sample     : OUT STD_LOGIC);
end component;



------- BEHAVIOUR OF ARCHITECTURE

begin




chain_clk <= not clk1kHz and enable_chain;
TX_Bit <= d_i(31);			--transmit always the actual MSB
busy <=  not eot;
data_old <= d_old;

g_sam1: clk_div
generic map(Nd => C_CLK_DIVIDE)	--THIS WILL BE 1 MHz :)
port map(
	clk => clk,
	en => en_sample,
	sample => clk1kHz
);



gen_mclk: process(clk)
begin
	if rising_edge(clk) then
		if start = '0' or eot = '1' then
			en_sample <= '0';
		elsif start = '1' then
			en_sample <= '1';
		end if;
	end if;
end process;



sm: process(clk1kHz,next_st,start, i_dlength)

begin
	present_st <= next_st;
	if start = '0' then	--async clear with the start signal
		enable_chain <= '0';
		next_st <= S_INIT;
		present_st <= S_INIT;
		d_i <= (others => '0');
		eot <= '0';			--The transmission has not yet begun, so EOT is 0
		counter <= conv_std_logic_vector(unsigned(i_dlength),5);

	elsif rising_edge(clk1kHz) then
		case present_st is

			when S_INIT =>	--start is '1' => enable the chain get the first bit from the data line and set d_i
				enable_chain	<= '1';
				d_i				<= data;
				d_old			<= d_old(d_old'high - 1 downto 0) & RX_BIT;
				next_st			<= S_TRANSMIT;

			when S_STOP =>
				eot <= '1';

			when S_TRANSMIT =>
				eot <= '0';
				d_i	<= d_i(d_i'high - 1 downto 0) & d_i(d_i'high);	--d_i(31) is transmitted -> MSB, LSB last
				counter <= conv_std_logic_vector(unsigned(counter) - 1,5);					--decrease the counter
				if counter = "00000" then 					--are we done yet?
					eot <= '1';					--transmission is finished
					next_st <= S_STOP;				--got to the stop state and wait for start='0'
					enable_chain <= '0';				--stop the spi clock
				else
					d_old <= d_old(d_old'high - 1 downto 0) & RX_BIT;	--sample the dataline
				end if;

			when others => next_st <= present_st;

		end case;
	end if;
end process;


end;
