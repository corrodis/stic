--VHDL ENTITY HANDLING THE TRANSFER OF 32 OR LESS BITS TO A CONNECTED SPI CLIENT
--
--THE DATA IS SEND WITH THE MSB FIRST AND READ BACK IN THE SAME WAY
--SIGNAL i_num_bits : NUMBER OF BITS TO TRANSMIT FROM THE 32 BIT VECTOR AT I_DATA


Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity spi_config_top is
	generic(
			C_NUM_CLIENTS	: integer := 1;				--NUMBER OF CLIENTS CONNECTED TO THE SPI INTERFACE
			C_CLK_DIVIDE : integer := 10;				--CLOCK DIVISION FACTOR FOR THE SYSTEM TO SPI CLOCK DIVISION
			C_HANDLEID_WIDTH : integer := 1				--WIDTH OF THE HANDLEID FOR MULTIPLEXING OF THE SIGNALS
		);
	port(
			--SYSTEM SIGNALS
			i_clk : in std_logic;						--SYSTEM CLOCK SIGNAL (100 MHz DESIGN)
		--	i_rst : in std_logic;						--RESET SIGNAL FOR THE SPI COMPONENT

			--DATA FROM OR TO THE USB INTERFACE
			i_data : in std_logic_vector(31 downto 0);	--THE CONFIGURATION DATA TO TRANSMIT
			o_return_data : out std_logic_vector(31 downto 0);


			--CONTROL SIGNALS FOR THE SPI TRANSMISSION
			i_num_bits : in std_logic_vector(4 downto 0);					--HOW MANY BITS FROM THIS CONFIGURATION DATA TO SEND
			i_handleid : in std_logic_vector(C_HANDLEID_WIDTH-1 downto 0);	--SELECTION OF THE SPI CLIENT
			i_start : in std_logic;											--START THE SPI TRANSMISSION
			i_chipselect : in std_logic;									--WHEN TO SELECT THE CHIP
			o_busy : out std_logic;											--END OF TRANSMISSION




			--SPI INTERCONNECTION
			o_mdo							: out std_logic; 	--COMMON OUTPUT TO ALL CONNECTED SPI CLIENTS
			o_sclk							: out std_logic;	--COMMON CLOCK TO ALL CONNECTED SPI CLIENTS
			i_mdi							: in std_logic_vector(C_NUM_CLIENTS-1 downto 0);	--INDIVIDUAL SPI SLAVE INPUTS
			o_cs							: out std_logic_vector(C_NUM_CLIENTS-1 downto 0)	--INDIVIDUAL CHIP SELECT SIGNALS

	    );
end spi_config_top;




architecture multiple_client of spi_config_top is


	component stic_sdata is
	generic(
		C_CLK_DIVIDE : integer := 10				--CLOCK DIVISION FACTOR FOR THE SYSTEM TO SPI CLOCK DIVISION
	);
	port(
		data : in std_logic_vector(31 downto 0);	--configuration data
		i_dlength : in std_logic_vector(4 downto 0);	--length of the configuration data
		clk : in std_logic;					--160MHz system clk
		start: in std_logic;				--start signal. transmission will start when this signal gets high
		RX_BIT: in std_logic;				--input from the end of the daisy chain of the stic chip
		busy : out std_logic;				--transmisstion in progress signal. '0' when the current transmission has finished, else '1'!
		TX_BIT : out std_logic;				--output to the stic daisy chain
		chain_clk : out std_logic;			--clock signal for the daisy chain
		data_old : out std_logic_vector(31 downto 0)	--received configuration data

	);
	end component;



	--SIGNALS REQUIRED FOR THE MULTIPLEXING OF CLIENTS
	signal s_txbit_local : std_logic;
	signal s_mdi_muxed : std_logic;


begin

	process(i_handleid, i_mdi,i_chipselect)
		variable v_id : natural;
	begin
		v_id := conv_integer(unsigned(i_handleid));
		--ASSIGN THE CORRECT CHIPSELECT SIGNAL
		o_cs <= (others => '1');	--CHIP SELECT IS ACTIVE LOW
		o_cs(v_id) <= i_chipselect;
		--ASSIGN THE CORRECT INPUT DATA LINE
		s_mdi_muxed <= i_mdi(v_id);
	end process;




	u_spi_send : stic_sdata
	generic map(
				C_CLK_DIVIDE => C_CLK_DIVIDE
			   )
	port map(
				data => i_data,
				i_dlength => i_num_bits,
				clk => i_clk,
				start => i_start,
				RX_BIT => s_mdi_muxed,
				busy => o_busy,
				TX_BIT => o_mdo,
				chain_clk => o_sclk,
				data_old => o_return_data
			);


end;

