Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

use work.stic2_daq_communication.all;

entity tb_daq_control is
end tb_daq_control;




architecture a of tb_daq_control is


	component daq_control is	--{{{
	port(
			i_sys_clk						: in std_logic; 	--100MHZ USER CLOCK FROM THE BOARD

			led2							: out std_logic;	--GREEN LED ON THE SPARTAN BOARD

			--SYSTEM CONTROL PORTS
			o_pll_enable 					: out std_logic;	--ENABLE OR DISABLE THE 640MHz OSCILLATOR


			--TRIGGER TEST PORTS
			o_test_trigger_p				: out std_logic;
			o_test_trigger_n				: out std_logic;


			--DESERIALIZER PORTS

			i_stic_txd_p					: in std_logic_vector(N_SPI_CLIENTS -1 downto 0);		--SERIAL DATA TRANSMISSION LINES P TERMINAL
			i_stic_txd_n					: in std_logic_vector(N_SPI_CLIENTS -1 downto 0);		--SERIAL DATA TRANSMISSION LINES N TERMINAL

			o_stic_clk_160M_p				: out std_logic;	--160 MHZ DIFFERENTIAL SYSTEM CLOCK FOR THE ASIC
			o_stic_clk_160M_n				: out std_logic;

			o_stic_rst						: out std_logic_vector(3 downto 0);	--STIC RESET SIGNALS


			i_debug_mdi						: in std_logic_vector(0 downto 0);		--DEBUG SPI MASTER DATA IN SIGNAL
			o_debug_cs						: out std_logic_vector(0 downto 0);		--DEBUG SPI CHIP SELECT SIGNAL

			--SPI PORTS TO THE CLIENTS
			o_spi_clk						: out std_logic;
			o_spi_mdo						: out std_logic;
			i_spi_mdi						: in std_logic_vector(N_SPI_CLIENTS -1 downto 0);
			o_spi_cs						: out std_logic_vector(N_SPI_CLIENTS -1 downto 0);



			--CYPRESS USB COMPONENT PORTS
			usb_reset_n						: out std_logic;	--RESET OF THE CYPRESS USB CORE

			usb_sloe 						: out std_logic;	--USB_FIFO OUTPUT ENABLE
			usb_slwr 						: out std_logic;	--USB_FIFO WRITE ENABLE
			usb_slrd 						: out std_logic;	--USB_FIFO READ ENABLE
			usb_flaga 						: in std_logic; --EP2 DATA FLAG	--CONFIGURED AS IN ENDPOINT	IF '1' HAS DATA
			usb_flagb 						: in std_logic; --EP4 DATA FLAG
			usb_flagc 						: in std_logic; --EP6 DATA FLAG
			usb_flagd 						: in std_logic; --EP8 DATA FLAG	--CONFIGURED AS OUT ENDPOINT IF '1' IS AVAILABLE
			fifo_addr 						: out std_logic_vector(1 downto 0);	--SELECT FIFO ENDPOINT BUFFER : DEFAULT 00 ONLY 10 WHEN WRITING
			fifo_data_io					: inout std_logic_vector(7 downto 0);	--TRISTATE FIFO DATA TO WRITE OR READ

			usb_fclk						: out std_logic;	--USB FIFO CLOCK (48 MHz)
			usb_clk							: out std_logic		--CYPRESS EXTERNAL CLOCK (24 MHz)


	    );
	end component daq_control; --}}}



	component stic3_emu_top is
		port(
				--INPUT CLOCK SIGNAL 160 MHz
				i_sys_clk : in std_logic;
				--RESET SIGNAL FROM THE DAQ
				i_rst : in std_logic;

				o_serial_txd : out std_logic;


				--UART INTERCONNECTION
	--			o_uart_tx : out std_logic;
				i_uart_rx : in std_logic
			);
	end component stic3_emu_top;

	component s_data is --{{{
	port(
		data : in std_logic_vector(7 downto 0);
		clk : in std_logic;
		rst_n: in std_logic;
		send : in std_logic;
		d_ready : out std_logic;
		TX_BIT : out std_logic
	);
	end component s_data; --}}}



	signal s_clk_emu : std_logic := '0';

	signal s_clk : std_logic := '0';
	signal s_rst : std_logic;
	signal s_serial_txd : std_logic;

	--UART SIGNALS
	signal s_send : std_logic;
	signal s_dready : std_logic;
	signal s_uart_rx : std_logic;
	signal s_rst_n : std_logic;

	signal s_uartdata : std_logic_vector(7 downto 0);

	--DAQ SIGNALS

	signal s_led2 : std_logic;
	signal stic_txd_p : std_logic_vector(N_SPI_CLIENTS -1 downto 0);
	signal stic_txd_n : std_logic_vector(N_SPI_CLIENTS -1 downto 0);
	signal stic_clk_160M : std_logic;

	signal s_fifo_data_io : std_logic_vector(7 downto 0);

begin

	s_clk <= not s_clk after 10.0 ns;	--100 MHz system clock
	s_clk_emu <= not s_clk_emu after 6.25 ns;

	stic_txd_p <= "000" & s_serial_txd;
	stic_txd_n <= "111" & not s_serial_txd;


	uut_daq: daq_control 	--{{{
	port map(
			i_sys_clk => s_clk,
			led2 => s_led2,

			--SYSTEM CONTROL PORTS
			o_pll_enable => open,


			--TRIGGER TEST PORTS
			o_test_trigger_p => open,
			o_test_trigger_n => open,


			--DESERIALIZER PORTS
			i_stic_txd_p => stic_txd_p,
			i_stic_txd_n => stic_txd_n,

			o_stic_clk_160M_p => stic_clk_160M,
			o_stic_clk_160M_n => open,

			o_stic_rst => open,


			i_debug_mdi	=> "0",
			o_debug_cs => open,

			--SPI PORTS TO THE CLIENTS
			o_spi_clk => open,
			o_spi_mdo => open,
			i_spi_mdi => "0000",
			o_spi_cs =>  open,



			--CYPRESS USB COMPONENT PORTS
			usb_reset_n	 => open,

			usb_sloe => open,
			usb_slwr => open,
			usb_slrd  => open,
			usb_flaga => '0',
			usb_flagb => '0',
			usb_flagc => '0',
			usb_flagd => '1',
			fifo_addr => open,
			fifo_data_io => s_fifo_data_io,

			usb_fclk => open,
			usb_clk	=> open


	    ); --}}}




	uut_emu : stic3_emu_top
	port map(
				i_sys_clk => s_clk_emu,
				i_rst => s_rst,
				o_serial_txd => s_serial_txd,
				i_uart_rx => s_uart_rx
			);


	u_uart_send : s_data
	port map(
				data => s_uartdata,
				clk => s_clk_emu,
				rst_n => s_rst_n,
				send => s_send,
				d_ready => s_dready,
				TX_BIT => s_uart_rx
			);



	stim : process
	begin
		s_rst <= '1';
		s_rst_n <= '0';
		s_send <= '0';

		wait for 1 us;
		s_rst <= '0';
		s_rst_n <= '1';

		s_uartdata <= X"03";
		wait until rising_edge(s_clk);
		wait until rising_edge(s_clk);
		wait until rising_edge(s_clk);
		s_send <= '1';
		wait until s_dready = '1';
		s_send <= '0';
		s_rst_n <= '0';
		wait until rising_edge(s_clk);
		s_rst_n <= '1';


		wait for 10 us;
		s_uartdata <= X"04";
		wait until rising_edge(s_clk);
		wait until rising_edge(s_clk);
		wait until rising_edge(s_clk);
		s_send <= '1';
		wait until s_dready = '1';
		s_send <= '0';
		wait until rising_edge(s_clk);


		wait;

	end process;

end;

