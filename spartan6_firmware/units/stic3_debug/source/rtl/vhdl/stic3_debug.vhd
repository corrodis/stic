Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

use work.debug_vhdl_package.all;

entity stic3_debug is
	port(
			i_clk : in std_logic;
			i_rst : in std_logic;

			i_debug_start : in std_logic;		--NEW USB PACKET READY START DEBUG TRANSMISSION
			o_debug_done : out std_logic;		--DEBUG TRANSMISSION FINISHED RETURNED DATA IN THE USB TRANSMISSION MEMORY

			--USB INTERFACE SIGNALS
			--READING FROM USB
			o_usb_rd_memaddr : out std_logic_vector(11 downto 0);
			i_usb_rd_data : in std_logic_vector(31 downto 0);

			--WRITING TO USB
			o_usb_wr_memaddr : out std_logic_vector(11 downto 0);	--WRITE ADDRESS
			o_usb_wr_data : in std_logic_vector(31 downto 0);		--WRITE DATA
			o_usb_wr_en : out std_logic;							--MEMORY WRITE ENABLE

			--DEBUG INTERFACE SPI SIGNALS
			i_dbg_mdi : in std_logic;
			o_dbg_mdo : out std_logic;
			o_dbg_cs : out std_logic;
			o_dbg_sclk : out std_logic
	    );
end stic3_debug;



architecture standalone_serial of stic3_debug is

	component spi_dbg_iface is --{{{
		port(
				i_data : in std_logic_vector(87 downto 0);	--DEBUG DATA
				i_clk : in std_logic;					--SYSTEM CLK
				i_start: in std_logic;				--START SIGNAL. TRANSMISSION WILL START WHEN THIS SIGNAL GETS HIGH
				i_RX_BIT: in std_logic;				--INPUT FROM THE END OF THE DAISY CHAIN OF THE STIC CHIP
				o_busy : out std_logic;				--TRANSMISSTION IN PROGRESS SIGNAL. NEGATIVE LOGIC
				o_TX_Bit : out std_logic;				--OUTPUT TO THE STIC DAISY CHAIN
				o_chain_clk : out std_logic;			--CLOCK SIGNAL FOR THE DAISY CHAIN
				o_data_old : out std_logic_vector(87 downto 0)	--RECEIVED CONFIGURATION DATA
			);
	end component; --}}}

	signal s_dbg_cmd : type_debug_msg;

	--rs232 required internal signals
	signal s_tx_data : std_logic_vector(87 downto 0);	--DATA TO TRANSMIT BACK TO THE UART
	signal s_rx_data : std_logic_vector(87 downto 0);	--DATA FROM THE UART INTERFACE


	--spi_dbg require internal signals
	signal s_spi_start : std_logic;
	signal s_spi_busy : std_logic;
	signal s_spi_clk : std_logic;

	type FSM_DBG_STATES is (FS_IDLE, FS_CHECK_CMD, FS_WRITE_SPI, FS_WAIT_SPI, FS_FETCH_USB, FS_WRITE_USB);
	signal p_dbg_state, n_dbg_state : FSM_DBG_STATES;


	signal s_dbg_clk, n_dbg_clk : std_logic;

	signal s_en_clk, n_en_clk : std_logic;

begin


	s_rx_data <= s_usb_data(95 downto 0);	--ASSIGN THE PORTION OF THE USB DATA CONTAINING THE DEBUG DATA

	s_dbg_cmd <= vector_to_debugmsg(s_rx_data);	--CONVERT TO THE RECEIVED MESSAGE TO A DEBUG RECORD

	u_spi_iface: spi_dbg_iface --{{{
		port map(
				i_data => s_rx_data,
				i_clk => i_clk,
				i_start => s_spi_start,
				i_RX_BIT => i_dbg_mdi,
				o_busy => s_spi_busy,
				o_TX_Bit => o_dbg_mdo,
				o_chain_clk => s_spi_clk,
				o_data_old  => s_tx_data
			); --}}}


		--COMBINATORIAL PART WITH MOORE OUTPUT DEFINITIONS
		dbg_comb : process (p_dbg_state, s_rdata_ready,s_dbg_cmd,s_spi_busy,s_uart_sdone,s_stic_clk) --{{{
		begin

			n_dbg_state <= p_dbg_state;
			n_dbg_clk <= '0';
			n_stic_clk <= s_stic_clk;
			s_spi_start <= '0';
			s_uart_start <= '0';
			s_rdata_ack <= '0';
			o_dbg_cs <= '1';
			n_en_clk <= s_en_clk;

			case p_dbg_state is
				when FS_IDLE =>
					if s_rdata_ready = '1' then
						n_dbg_state <= FS_CHECK_CMD;
					end if;

				when FS_CHECK_CMD =>
					s_rdata_ack <= '1';
					n_dbg_state <= FS_WRITE_SPI; --DEFAULT NEXT STATE

					if s_dbg_cmd.cmd = CMD_STEP_SPI then	--ONLY OVERWRITE IF THE COMMAND IS FOR STEPPING OF A CLOCK
						n_dbg_state <= FS_STEP_SPI;
					end if;
					if s_dbg_cmd.cmd = CMD_STEP_SYSCLK then	--ONLY OVERWRITE IF THE COMMAND IS FOR STEPPING OF A CLOCK
						n_dbg_state <= FS_STEP_STIC;
					end if;
					if s_dbg_cmd.cmd = CMD_EN_SYS_CLOCK then	--ONLY OVERWRITE IF THE COMMAND IS FOR STEPPING OF A CLOCK
						n_dbg_state <= FS_IDLE;
						n_en_clk <= '1';
					end if;
					if s_dbg_cmd.cmd = CMD_DIS_SYS_CLOCK then	--ONLY OVERWRITE IF THE COMMAND IS FOR STEPPING OF A CLOCK
						n_dbg_state <= FS_IDLE;
						n_en_clk <= '0';
					end if;

				when FS_STEP_STIC =>
					n_stic_clk <= not s_stic_clk;
					n_dbg_state <= FS_IDLE;

				when FS_STEP_SPI =>
					n_dbg_clk <= '1';
					n_dbg_state <= FS_IDLE;

				when FS_WRITE_SPI =>
					o_dbg_cs <= '0';
					s_spi_start <= '1';
					n_dbg_state <= FS_WAIT_SPI;

				when FS_WAIT_SPI =>
					o_dbg_cs <= '0';
					s_spi_start <= '1';
					if s_spi_busy = '0' then
						n_dbg_state <= FS_REPLY_UART;
					end if;

				when FS_REPLY_UART =>
					s_uart_start <= '1';
					n_dbg_state <= FS_WAIT_UART;

				when FS_WAIT_UART =>
					s_uart_start <= '1';
					if s_uart_sdone = '1' then
						n_dbg_state <= FS_IDLE;
					end if;


				when others => null;
			end case;
		end process dbg_comb; --}}}

		dbg_syn : process (i_clk) --{{{
		begin
			if rising_edge(i_clk) then
				if i_rst = '1' then
					p_dbg_state <= FS_IDLE;
					s_stic_clk <= '0';
					s_en_clk <= '0';
				else
					p_dbg_state <= n_dbg_state;
					s_dbg_clk <= n_dbg_clk;
					s_stic_clk <= n_stic_clk;
					s_en_clk <= n_en_clk;
				end if;
			end if;
		end process dbg_syn; --}}}



		o_dbg_sclk <= s_dbg_clk or s_spi_clk;
		o_stic_clk <= s_stic_clk when s_en_clk = '0' else i_clk;


end;
