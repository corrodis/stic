Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity tb_stic3_debug is
end tb_stic3_debug;


architecture behaviour of tb_stic3_debug is


	component stic3_debug is --{{{
		port(
				i_clk : in std_logic;
				i_rst : in std_logic;

				--UART INTERFACE PINS
				i_uart_rx : in std_logic;
				o_uart_tx : out std_logic;

				--CLOCK TO THE STIC ASIC FOR STEPPING
				o_stic_clk : out std_logic;

				i_dbg_mdi : in std_logic;
				o_dbg_mdo : out std_logic;
				o_dbg_cs : out std_logic;
				o_dbg_sclk : out std_logic
			);
	end component; --}}}


	component s_data is --{{{
	port(
		data : in std_logic_vector(7 downto 0);
		clk : in std_logic;
		rst_n: in std_logic;
		send : in std_logic;
		d_ready : out std_logic;
		TX_BIT : out std_logic
	);
	end component; --}}}

		signal s_clk : std_logic := '0';
		constant c_clk_period : time := 10 ns;
		signal s_rst : std_logic;

		--UART INTERFACE PINS
		signal s_uart_rx : std_logic;
		signal s_uart_tx : std_logic;

		--CLOCK TO THE STIC ASIC FOR STEPPING
		signal s_stic_clk : std_logic;

		signal s_dbg_mdi : std_logic;
		signal s_dbg_mdo : std_logic;
		signal s_dbg_cs : std_logic;
		signal s_dbg_sclk : std_logic;


		signal spi_slave_shreg : std_logic_vector(87 downto 0);



		--UART SEND SIGNALS
		signal s_serial_data : std_logic_vector(7 downto 0);
		signal s_send_uart : std_logic;
		signal s_send_ready : std_logic;
		signal s_rst_n : std_logic;	--RESET SIGNAL FOR THE UART SENDER

		signal s_cmd_data : std_logic_vector(87 downto 0);


begin

	--CLOCK SIMULATION
	s_clk <= not s_clk after c_clk_period;

	uut: stic3_debug --{{{
		port map(
				i_clk => s_clk,
				i_rst => s_rst,

				--UART INTERFACE PINS
				i_uart_rx => s_uart_rx,
				o_uart_tx => s_uart_tx,

				--CLOCK TO THE STIC ASIC FOR STEPPING
				o_stic_clk => s_stic_clk,

				i_dbg_mdi => s_dbg_mdi,
				o_dbg_mdo => s_dbg_mdo,
				o_dbg_cs => s_dbg_cs,
				o_dbg_sclk => s_dbg_sclk
			); --}}}


		s_dbg_mdi <= spi_slave_shreg(spi_slave_shreg'high);
		sim_shreg : process (s_dbg_sclk)
		begin
			if rising_edge(s_dbg_sclk) then
				spi_slave_shreg <= spi_slave_shreg(spi_slave_shreg'high-1 downto 0) & s_dbg_mdo;
			end if;
		end process sim_shreg;


		serial_send : s_data	--WRITING TO THE SERIAL INTERFACE FOR DEBUGGING PURPOSES
		port map(
				data => s_serial_data,
				clk => s_clk,
				rst_n => s_rst_n,
				send => s_send_uart,
				d_ready => s_send_ready,
				TX_BIT => s_uart_rx
				);


		stim : process
			variable v_byte_count : integer;
		begin
			s_rst <= '1';
			s_rst_n <= '0';
			s_cmd_data <= X"100a090807060504030201";
			wait for 100 ns;
			wait until rising_edge(s_clk);
			s_rst <= '0';
			s_rst_n <= '1';


			v_byte_count := 0;
			while (v_byte_count < 11) loop

				s_serial_data <= s_cmd_data((v_byte_count+1)*8-1 downto v_byte_count*8);
				wait for 10 us;
				wait until rising_edge(s_clk);
				s_send_uart <= '1';
				wait until s_send_ready = '1';
				s_send_uart <= '0';
				wait until rising_edge(s_clk);
				s_rst_n <= '0';
				wait until rising_edge(s_clk);
				s_rst_n <= '1';

				v_byte_count := v_byte_count+1;

			end loop;

			wait for 300 us;
			s_cmd_data <= X"110a090807060504030201";
			--SECOND TIME READBACK
			v_byte_count := 0;
			while (v_byte_count < 11) loop

				s_serial_data <= s_cmd_data((v_byte_count+1)*8-1 downto v_byte_count*8);
				wait for 10 us;
				wait until rising_edge(s_clk);
				s_send_uart <= '1';
				wait until s_send_ready = '1';
				s_send_uart <= '0';
				wait until rising_edge(s_clk);
				s_rst_n <= '0';
				wait until rising_edge(s_clk);
				s_rst_n <= '1';

				v_byte_count := v_byte_count+1;

			end loop;

			wait;
		end process stim;


end;
