Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

use work.stic2_daq_communication.all;

entity usb_trans is
	port(
			USB_Clk48 : in std_logic;			--CLOCK FOR THE LOGIC
			--USB_Clk24 : in std_logic;			--CLOCK FOR THE CYPRESS CHIP	--DIRECTLY SUPPLIED FROM THE AXI INTERFACE TO THE OUTPUT PIN
			reset : in std_logic;				--AXI RESET SIGNAL

			--USB SLAVE FIFO SIGNALS
			usb_sloe : out std_logic;	--USB_FIFO OUTPUT ENABLE
			usb_slwr : out std_logic;	--USB_FIFO WRITE ENABLE
			usb_slrd : out std_logic;	--USB_FIFO READ ENABLE
			usb_flaga : in std_logic; --EP2 DATA FLAG	--CONFIGURED AS IN ENDPOINT	IF '1' HAS DATA
			usb_flagb : in std_logic; --EP4 DATA FLAG
			usb_flagc : in std_logic; --EP6 DATA FLAG
			usb_flagd : in std_logic; --EP8 DATA FLAG	--CONFIGURED AS OUT ENDPOINT IF '1' IS AVAILABLE
			fifo_addr : out std_logic_vector(1 downto 0);	--SELECT FIFO ENDPOINT BUFFER : DEFAULT 00 ONLY 10 WHEN WRITING
			fifo_data_in : in std_logic_vector(7 downto 0);	--FIFO DATA TO WRITE OR READ
			fifo_data_out : out std_logic_vector(7 downto 0);
			fifo_data_T : out std_logic;					--FIFO TRISTATE SIGNAL	IF '1' DIRECTION = IN

			usb_reset_n : out std_logic;

			--USB RX SIGNALS	(FORM USB TO AXI)

			usb_write_en : out std_logic;		--USB MEMORY WRITE ENABLE
			usb_write_data : out std_logic_vector(31 downto 0);	--PACKET DATA FOR STORING IN THE MEMORY
			usb_write_addr : out std_logic_vector(11 downto 0);	--WRITE ADDRESS FOR THE USB WRITE DATA
			usb_packet_ready : out std_logic;					--INTERRUPT SIGNAL INDICATING A NEW PACKET IN THE MEMORY

			i_usb_packet_header : in std_logic_vector(31 downto 0);	--THE PACKET HEADER CURRENTLY STORED IN THE RCV MEMORY

			--USB TX SIGNALS	(FROM AXI TO USB)

			usb_send_done : out std_logic;							--SIGNAL TO INDICATE THAT THE SENDING HAS BEEN COMPLETED
			usb_read_addr : out std_logic_vector(11 downto 0);		--USB MEMORY READ ADDRESS
			usb_read_data : in std_logic_vector(31 downto 0);		--DATA FROM THE MEMORY TO THE USB
			usb_start_trans : in std_logic;							--SIGNAL TO START THE TRANSMISSION
			usb_addr_offset : in std_logic_vector(11 downto 0);		--MEMORY ADDRESS OF THE PACKET
			usb_num_packets : in std_logic_vector(1 downto 0)		--NUMBER OF PACKETS TO BE TRANSMITTED
		);
end usb_trans;




architecture imp of usb_trans is



	--USB STATE MACHINE DESCRIPTION:
	--IDLE WAITS FOR DATA FROM THE AXI OR USB BUS AND GOES TO THE READ OR WRITE STATE RESPECTIVELY
	--IN WRITE STATE THE DATA IS FETCHED FROM THE MEMORY STARTING AT THE ADDRESS GIVEN IN ADDR_OFFSET AND NUM_PACKETS ARE WRITTEN TO THE FIFO
	--IN READ STATE THE FIFO DATA IS FETCHED AND TRANSFERED TO THE AXI MEMORY 2 STARTING AT ADDRESS 0X0 (THIS FIRST VERSION WILL ONLY SUPPORT ONE PACKET)


	type usb_states is (S_RESET, S_IDLE, S_USB_READ, S_MEM_WRITE, S_MEM_READ, S_USB_WRITE, S_CHECK_WRITE, S_TIMEOUT,S_CHECK_LEN);	--STATES OF THE USB STATEMACHINE
	signal usb_ps, usb_ns : usb_states;

	
	signal s_fifo_data : std_logic_vector(7 downto 0);	--REQUIRED FOR THE TRISTATE
	signal s_usb_slwr : std_logic;


	signal count_bytes,next_count_bytes : std_logic_vector(11 downto 0);	--NUMBER OF BYTES TO READ/WRITE


	--SIGNALS USED IN THE FSM FOR THE ACCESS TO THE DUAL PORT SRAM
	signal word, next_word : std_logic_vector(31 downto 0);			--FF'S STORING THE CURRENT WORD THAT IS BEING TRANSMITTED OR RECEIVED
	signal n_usb_raddr, n_usb_waddr	: std_logic_vector(11 downto 0);	--THE NEXT CALCULATED USB READ ADDRESS
	signal s_usb_raddr, s_usb_waddr : std_logic_vector(11 downto 0);	--USB READ AND WRITE ADDRESS NEEDED FOR READBACK;
	signal n_usb_we, s_usb_we: std_logic;									--USB WRITE ENABLE SIGNALS

	signal b_count : std_logic_vector(1 downto 0);					--INTERMEDIATE SIGNAL FOR COUNTING THE 4 BYTES OF A 32 BIT WORD

	signal n_usb_packet_ready : std_logic;							--sampled packet ready signal to avoid IRQ glitches

	signal n_packet_count, s_packet_count : std_logic;

--	signal s_usb_start_trans_shreg : std_logic_vector(1 downto 0);
	signal s_usb_start_trans : std_logic;


	signal s_usb_timeout, n_usb_timeout : std_logic_vector(11 downto 0);

	signal s_rcv_header : t_usb_header;

begin

	--MAKE CLOCK DOMAIN CROSSING SAFE
	edge_det : process(USB_Clk48)
	begin
		if rising_edge(USB_Clk48) then
			s_usb_start_trans <= usb_start_trans;	--shift is through
		end if;
	end process;
--	s_usb_start_trans <= s_usb_start_trans_shreg(0) and (not s_usb_start_trans_shreg(1));


	s_rcv_header <= to_usb_header(i_usb_packet_header);

	fifo_data_out <= s_fifo_data; -- when s_usb_slwr = '0' else (others => 'Z'); --XPS WANTS TO DO ITS OWN TRISTATE...
	fifo_data_T <= s_usb_slwr;		--THE TRISTATE SIGNAL IS ACTIVE HIGH
	usb_slwr <= s_usb_slwr;


	b_count <= not count_bytes(1 downto 0);		--THE INTER-WORD BYTE COUNT HOW MANY BYTES OF THE CURRENT WORD HAVE BEEN TRANSFERED

	usb_read_addr <= s_usb_raddr;		--ASSIGN THE READ AND WRITE ADDRESSES TO THE OUTPUT PORTS
	usb_write_addr <= s_usb_waddr;

	usb_write_en <= s_usb_we;
	usb_write_data <= word;


	--COMBINATORIAL PART{{{
	usb_comb: process(usb_ps, usb_flaga, usb_flagd, word, fifo_data_in, count_bytes, s_usb_start_trans, b_count, s_usb_we, s_usb_timeout, s_rcv_header,
						s_usb_raddr, s_usb_waddr, usb_addr_offset, usb_num_packets, usb_read_data,s_packet_count	--TO MAKE XST HAPPY WE INCLUDE ALL SIGNALS
					)
	begin

		--DEFAULT VALUES
		--KEEP THE PREVIOUS VALUES IF NOTHING ELSE CHANGED
		usb_ns <= usb_ps;
		next_count_bytes <= count_bytes;
		n_usb_raddr <= s_usb_raddr;
		n_usb_waddr <= s_usb_waddr;
		next_word <= word;
		n_packet_count <= s_packet_count;

		n_usb_timeout <= s_usb_timeout;


		n_usb_we <= '0';			--SET THE DEFAULT MEMORY WRITE ENABLE TO '0'
		fifo_addr <= "00";
		usb_slrd <= '1';
		usb_sloe <= '1';
		s_usb_slwr <= '1';			--BY DEFAULT DON'T WRITE!!! THE CYPRESS USB SIGNALS ARE ACTIVE LOW!!
		s_fifo_data <= (others => '0');


		usb_send_done <= '0';			--SENT IS NOT DONE BY DEFAULT
		n_usb_packet_ready <= '0';
		usb_reset_n <= '1';			--NO RESET BY DEFAULT


		case usb_ps is
			when S_RESET =>
				s_fifo_data <= (others => '0');
				n_packet_count <= '0';
				s_usb_slwr <= '1';		--DISABLE THE SLAVE WRITE COMMAND
				usb_slrd <= '1';		--DISABLE THE SLAVE_READ COMMAND
				usb_sloe <= '0';		--BY DEFAULT ENABLE THE SLAVE OUTPUT
				fifo_addr <= "00";		--SET TO THE IN ENDPOINT
				next_count_bytes <= X"1FF";
				n_usb_timeout <= (others => '1');
				next_word <= (others => '0');
				usb_reset_n <= '0';		--ALSO RESET THE USB CONTROLLER

				--NEXT STATE CALCULATION
				usb_ns <= S_IDLE;

			when S_IDLE =>

				n_usb_raddr <= usb_addr_offset;
				n_usb_waddr <= (others => '0');

				next_count_bytes <= X"1FF";
				n_packet_count <= '0';
				fifo_addr <= "00";
				usb_sloe <= '0';

				n_usb_timeout <= (others => '1');	--KEEP THE TIMEOUT RESET

				if s_usb_start_trans = '1' then
					usb_ns <= S_CHECK_WRITE;
				end if;

				if usb_flaga = '1' then
					usb_ns <= S_USB_READ;
				end if;


			when S_CHECK_WRITE =>

				-- Simon: coming from top level  0
				n_usb_raddr <= usb_addr_offset;			--USE THE SUPPLIED ADDRESS OFFSET AS START ADDRESS
				-- Simon: usb_num_packets = "01" -> 1023
				next_count_bytes <= '0' & usb_num_packets & "111111111";	--USE THE NUMBER OF PACKETS TO WRITE TO ADD TO THE COUNT BYTES

				usb_sloe <= '1';			--DISABLE THE SLAVE OUTPUT
				fifo_addr <= "10";			--OUT ENDPOINT 6
				if usb_flagd = '1' then		--THE BUFFER IS NOT BUSY, WE CAN WRITE
					usb_ns <= S_USB_WRITE;		--GO TO THE WRITE STATE
				else
					if usb_flaga = '1' then --DON'T GET INTO A LOCKED STATE WHILE WAITING FOR THE READY OF THE WRITE
						usb_ns <= S_IDLE;
					end if;
				end if;

			when S_USB_WRITE => 

				case b_count is		--SELECT THE CORRECT BYTE FROM THE 32 BIT INPUT WORD
					when "00" =>
						s_fifo_data <= usb_read_data(7 downto 0);
					when "01" =>
						s_fifo_data <= usb_read_data(15 downto 8);
					when "10" =>
						s_fifo_data <= usb_read_data(23 downto 16);
						n_usb_raddr <= s_usb_raddr + 4;		--INCREMENT THE BY ADDRESS TO ACCESS THE NEXT WORD (BECOMES VALID IN THE NEXT TO NEXT CLOCK CYCLE)
					when "11" =>
						s_fifo_data <= usb_read_data(31 downto 24);
					when others => null;
				end case;

				fifo_addr <= "10";
				s_usb_slwr <= '0';			--ENABLE THE SLAVE WRITE
				usb_sloe <= '1';			--DISABLE THE USB SLAVE OUTPUT


				if usb_flagd = '1' and count_bytes /= X"000" then		--WHILE THE BUFFER IS NOT FULL AND 0 HAS NOT BEEN REACHED
					next_count_bytes <= count_bytes - 1;
				end if;


				n_usb_timeout <= (others => '1');	--RESET THE TIMEOUT WHEN THE FLAGD IS NOT SET

				if usb_flagd = '0' and count_bytes /= X"000" then	--THE BUFFER BECAME FULL BEFORE WE FINISHED WRITING! RESET AFTER TIMEOUT
					n_usb_timeout <= s_usb_timeout - 1;
				end if;

				if s_usb_timeout = X"000" then	--TIMEOUT COUNTER HAS REACHED THE FINAL VALUE
					usb_ns <= S_TIMEOUT;
				end if;

				if count_bytes = X"000" then
					usb_ns <= S_IDLE;			--DIRECTLY GO OVER TO THE IDLE STATE, OTHERWISE WE GET A LOOP IN THE CLOCK DOMAINS
					usb_send_done <= '1';		--INDICATE THAT SENDING THE DATA HAS BEEN COMPLETED
--					s_usb_slwr <= '0';			--NOT SURE IF IT HAS TO BE SET AT THIS POINT OR NOT
				end if;

			when S_TIMEOUT =>
				usb_ns <= S_IDLE;
				usb_send_done <= '1';			--INDICATE THAT THE SENDING IS FINISHED ALTHOUGH IT HAS BEEN ABORTED, TO ENSURE THAT THE GLOBAL FSM KEEPS RUNNING


			when S_USB_READ =>
				usb_sloe <= '0';
				usb_slrd <= '0';

				next_count_bytes <= count_bytes - 1;
				next_word <= fifo_data_in & word(word'high downto 8);		--SHIFT THE BYTES THROUGH

				if b_count = "11" then		--SET THE NEXT USB WRITE ENABLE AT THE WHEN THE FOURTH BYTE IS ASSIGNED
					n_usb_we <= '1';
				end if;

				if s_usb_we = '1' then		--CALCULATE THE NEXT WRITE ADDRESS WHEN THE WRITE ENABLE IS HIGH
					n_usb_waddr <= s_usb_waddr + 4;
				end if;

				if count_bytes = X"000" then
--					n_usb_we <= '0';		--DONT DISABLE THE WRITE ENABLE ON THE MEMORY BLOCK
					usb_ns <= S_CHECK_LEN;
				end if;

			when S_CHECK_LEN =>

				next_count_bytes <= X"1FF";
				fifo_addr <= "00";
				usb_sloe <= '0';

				n_usb_waddr <= X"200";	--WRITE AT THE NEXT PACKET ADDRESS

				if s_rcv_header.len(9) = '1' and s_packet_count='0' then


					if usb_flaga = '1' then	--WAIT FOR THE FLAGA TO GET ACTIVE AGAIN THEN READ THE SECOND PACKET
						n_packet_count <= '1';	--REMEMBER THAT WE ALREADY ARE READING THE SECOND PACKET
						usb_ns <= S_USB_READ;
					end if;

				--TIMEOUT WAITING FOR THE SECOND PACKET
					n_usb_timeout <= s_usb_timeout - 1;
					if s_usb_timeout = X"000" then
						usb_ns <= S_RESET;	--MAKE SURE THAT WE CLEAR EVERYTHING SINCE THERE WAS A TIMEOUT
					end if;


				else
					usb_ns <= S_IDLE;
					n_usb_packet_ready <= '1';
				end if;


			when others =>
				usb_ns <= S_RESET;
		end case;

	end process;	--}}}



	usb_seq: process(USB_Clk48, reset)	--SEQUENTIAL PART{{{
	begin
		if reset = '1' then	--ASYNCRONOUS RESET SINCE AXI AND USB CLOCK ARE NOT SYNC
			usb_ps <= S_RESET;

		elsif falling_edge(USB_Clk48) then

			usb_packet_ready <= n_usb_packet_ready;
			usb_ps <= usb_ns;
			count_bytes <= next_count_bytes;

			word <= next_word;		--ASSIGN THE NEXT WORD THAT IS TO BE TRANSMITTED
			s_usb_we <= n_usb_we;
			s_usb_raddr <= n_usb_raddr;
			s_usb_waddr <= n_usb_waddr;

			s_packet_count <= n_packet_count;


			s_usb_timeout <= n_usb_timeout;

		end if;

	end process;	--}}}

end;
