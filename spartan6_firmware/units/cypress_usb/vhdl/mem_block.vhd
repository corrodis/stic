
-- SEQUENCE TO WRITE DATA D1 TO THE MEMORY AT ADDRESS R1

--           ___________        ___________
--CLK _______|          |_______|          |_________
--WEN        ___________________
--    _______|                  |________
--    _______ __________________ ________________
--DIN _______X_______D1_________X______D2________
--     ______ __________________ ________________
--ADDR ______X_______R1_________X______R2________
--


Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity mem_block is
	port(
			clka : in std_logic;							--WRITE CLOCK
			clkb : in std_logic;							--READ CLOCK
			waddr_a : in std_logic_vector(11 downto 0);		--WRITE ADDRESS ON PORT A
			raddr_a : in std_logic_vector(11 downto 0);		--READ ADDRESS ON PORT A
			raddr_b : in std_logic_vector(11 downto 0);		--READ ADDRESS ON PORT B
			din_a : in std_logic_vector(31 downto 0);		--INPUT DATA FOR WRITING
			dout_a : out std_logic_vector(31 downto 0);		--OUTPUT DATA ON PORT A
			dout_b : out std_logic_vector(31 downto 0);		--OUTPUT DATA FOR READING
			we : in std_logic								--WRITE ENABLE
	    );
end mem_block;




architecture a of mem_block is



	type MEM_RAM is array(1023 downto 0) of std_logic_vector(31 downto 0);
	signal ram : MEM_RAM;

	signal mem_waddra, mem_raddra, mem_raddrb : std_logic_vector(9 downto 0);			--READ AND WRITE ADDRESS OF THE BYTE ADDRESSABLE MEMORY

begin



	mem_waddra <= waddr_a(11 downto 2);						--ASSIGN THE BYTE ADDRESS



	write_proc: process(clka)
	begin
		if rising_edge(clka) then
			if we = '1' then
				ram(conv_integer(mem_waddra)) <= din_a;	--WRITE THE DATA TO THE MEMORY IF WRITE_ENABLE IS '1'
			end if;
			mem_raddra <= raddr_a(11 downto 2);				--TAKE OVER THE READ ADDRESS OF PORT A
		end if;
	end process;


	read_proc : process (clkb)	--SAMPLE THE BYTE READ ADDRESS WITH THE RISING EDGE OF THE DUAL CLOCK
	begin
		if rising_edge(clkb) then
			mem_raddrb <= raddr_b(11 downto 2);				--TAKE OVER THE READ ADDRESS OF PORT B
		end if;
	end process read_proc;

	dout_b <= ram(conv_integer(mem_raddrb));
	dout_a <= ram(conv_integer(mem_raddra));


end;

