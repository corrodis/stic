Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;


--FOR THE ODDR INSTANCES
Library UNISIM;
use UNISIM.vcomponents.all;

entity cypress_usb_top is
	port(
			i_clk_160M						: in std_logic;		--A 160 MHZ CLOCK TO GENERATE THE 48 AND 24 MHZ
			i_rst							: in std_logic;		--RESET SIGNAL OF THE USB CORE


			--USB SLAVE FIFO SIGNALS CONNECTED TO THE EXTERNAL PORTS
			usb_reset_n						: out std_logic;	--RESET OF THE CYPRESS USB CORE

			usb_sloe 						: out std_logic;	--USB_FIFO OUTPUT ENABLE
			usb_slwr 						: out std_logic;	--USB_FIFO WRITE ENABLE
			usb_slrd 						: out std_logic;	--USB_FIFO READ ENABLE
			usb_flaga 						: in std_logic; --EP2 DATA FLAG	--CONFIGURED AS IN ENDPOINT	IF '1' HAS DATA
			usb_flagb 						: in std_logic; --EP4 DATA FLAG
			usb_flagc 						: in std_logic; --EP6 DATA FLAG
			usb_flagd 						: in std_logic; --EP8 DATA FLAG	--CONFIGURED AS OUT ENDPOINT IF '1' IS AVAILABLE
			fifo_addr 						: out std_logic_vector(1 downto 0);	--SELECT FIFO ENDPOINT BUFFER : DEFAULT 00 ONLY 10 WHEN WRITING
			fifo_data_io					: inout std_logic_vector(7 downto 0);	--TRISTATE FIFO DATA TO WRITE OR READ

			usb_fclk						: out std_logic;	--USB FIFO CLOCK (48 MHz)
			usb_clk							: out std_logic;	--CYPRESS EXTERNAL CLOCK (24 MHz)


			--CONNECION FROM THE CONTROL LOGIC TO THE USB_TRANS ENTITY
			i_usb_start_trans 				: in std_logic;							--SIGNAL TO START THE TRANSMISSION
			i_usb_addr_offset 				: in std_logic_vector(11 downto 0);		--MEMORY START ADDRESS OF THE PACKET
			i_usb_num_packets 				: in std_logic_vector(1 downto 0);		--NUMBER OF PACKETS TO BE TRANSMITTED
			o_usb_packet_ready				: out std_logic;		--INTERRUPT SIGNAL INDICATING A NEW PACKET, ACTIVE FOR ONE CLOCK CYCLE
			o_usb_send_done					: out std_logic;		--INDICATING THAT THE SENDING IS FINISHED

			--CONNECTION FROM THE CONTROL LOGIC TO THE SEND BUFFER
			i_memsend_data					: in std_logic_vector(31 downto 0);	--32 BIT DATA VECTOR TO WRITE IN THE SEND BUFFER
			i_memsend_addr					: in std_logic_vector(11 downto 0);	--THE BIT ADDRESS FOR WRITING THE DATA, THE LAST 2 BITS WILL BE CROPPED TO GET THE BYTE ADDRESS
			i_memsend_wen					: in std_logic;

			--CONNECTION FROM THE CONTROL LOGIC TO THE READ BUFFER
			i_memread_addr					: in std_logic_vector(11 downto 0);
			o_memread_data					: out std_logic_vector(31 downto 0)

	    );
end cypress_usb_top;




architecture behaviour of cypress_usb_top is


	component mem_block is	--{{{
		port(
				clka : in std_logic;							--WRITE CLOCK
				clkb : in std_logic;							--READ CLOCK
				waddr_a : in std_logic_vector(11 downto 0);		--WRITE ADDRESS ON PORT A
				raddr_a : in std_logic_vector(11 downto 0);		--READ ADDRESS ON PORT A
				raddr_b : in std_logic_vector(11 downto 0);		--READ ADDRESS ON PORT B
				din_a : in std_logic_vector(31 downto 0);		--INPUT DATA FOR WRITING
				dout_a : out std_logic_vector(31 downto 0);		--OUTPUT DATA ON PORT A
				dout_b : out std_logic_vector(31 downto 0);		--OUTPUT DATA FOR READING
				we : in std_logic								--WRITE ENABLE
			);
	end component;	--}}}

	component dcm_clockgen is	--{{{
	port(
			CLK_IN1		:	in std_logic;
			CLK_OUT1	:	out std_logic;
			RESET		:	in std_logic
--			LOCKED		:	out std_logic
		);
	end component; --}}}

	component pll_filter is	--{{{
	port(
			CLK_IN1		:	in std_logic;
			CLK_OUT1	:	out std_logic
		);
	end component; --}}}

	component usb_trans is	--{{{
		port
		(
			USB_Clk48 : in std_logic;			--CLOCK FOR THE LOGIC
			--USB_Clk24 : in std_logic;			--CLOCK FOR THE CYPRESS CHIP	--DIRECTLY SUPPLIED FROM THE AXI INTERFACE TO THE OUTPUT PIN
			reset : in std_logic;				--AXI RESET SIGNAL

			--USB SLAVE FIFO SIGNALS
			usb_sloe : out std_logic;	--USB_FIFO OUTPUT ENABLE
			usb_slwr : out std_logic;	--USB_FIFO WRITE ENABLE
			usb_slrd : out std_logic;	--USB_FIFO READ ENABLE
			usb_flaga : in std_logic; --EP2 DATA FLAG	--CONFIGURED AS IN ENDPOINT	IF '1' HAS DATA
			usb_flagb : in std_logic; --EP4 DATA FLAG
			usb_flagc : in std_logic; --EP6 DATA FLAG
			usb_flagd : in std_logic; --EP8 DATA FLAG	--CONFIGURED AS OUT ENDPOINT IF '1' IS AVAILABLE
			fifo_addr : out std_logic_vector(1 downto 0);	--SELECT FIFO ENDPOINT BUFFER : DEFAULT 00 ONLY 10 WHEN WRITING
			fifo_data_in : in std_logic_vector(7 downto 0);	--FIFO DATA TO WRITE OR READ
			fifo_data_out : out std_logic_vector(7 downto 0);
			fifo_data_T : out std_logic;

			usb_reset_n : out std_logic;

			--USB RX SIGNALS	(FORM USB TO AXI)

			usb_write_en : out std_logic;		--USB MEMORY WRITE ENABLE
			usb_write_data : out std_logic_vector(31 downto 0);	--PACKET DATA FOR STORING IN THE MEMORY
			usb_write_addr : out std_logic_vector(11 downto 0);	--WRITE ADDRESS FOR THE USB WRITE DATA
			usb_packet_ready : out std_logic;					--INTERRUPT SIGNAL INDICATING A NEW PACKET IN THE MEMORY

			i_usb_packet_header : in std_logic_vector(31 downto 0);	--THE PACKET HEADER CURRENTLY STORED IN THE RCV MEMORY

			--USB TX SIGNALS	(FROM AXI TO USB)

			usb_send_done : out std_logic;							--SIGNAL TO INDICATE THAT THE SENDING HAS BEEN COMPLETED
			usb_read_addr : out std_logic_vector(11 downto 0);		--USB MEMORY READ ADDRESS
			usb_read_data : in std_logic_vector(31 downto 0);		--DATA FROM THE MEMORY TO THE USB
			usb_start_trans : in std_logic;							--SIGNAL TO START THE TRANSMISSION
			usb_addr_offset : in std_logic_vector(11 downto 0);		--MEMORY ADDRESS OF THE PACKET
			usb_num_packets : in std_logic_vector(1 downto 0)		--NUMBER OF PACKETS TO BE TRANSMITTED

		);
	end component;	--}}}


	--GENERATED CLOCK SIGNALS
	signal s_clk_48M : std_logic;
	signal s_clk_24M : std_logic := '0';


	--SEND BUFFER SIGNALS
	signal write_enable : std_logic;


	--TRISTATE BUFFER DESCRIPTION TRISTATE AT TOPLEVEL BECAUSE IT WAS NEEDED FOR THE MICROBLAZE
	signal s_fifo_data_in, s_fifo_data_out : std_logic_vector(7 downto 0);
	signal s_fifo_data_T : std_logic; --IF FIFO_DATA_T = '0' THEN OUTPUT IS DRIVEN BY THE FPGA ELSE THE CYPRESS IC DRIVES THE BUSLINES



	--USB RX SIGNALS (FROM USB TO READ BUFFER)
	signal s_usb_write_en : std_logic;						--USB MEMORY WRITE ENABLE FROM USB_TRANS
	signal s_usb_write_data : std_logic_vector(31 downto 0);	--PACKET DATA FOR STORING IN THE MEMORY FROM USB_TRANS
	signal s_usb_write_addr : std_logic_vector(11 downto 0);	--WRITE ADDRESS FOR THE USB WRITE DATA FROM USB_TRANS
	signal s_usb_packet_ready : std_logic;					--INTERRUPT SIGNAL INDICATING A NEW PACKET IN THE MEMORY FROM USB_TRANS

	signal s_usb_rcv_header : std_logic_vector(31 downto 0);	--HEADER OF A RECEIVED PACKET TO CHECK IF MORE THAN ONE PACKET IS EXPECTED

	--USB TX SIGNALS	(FROM SEND BUFFER TO USB)
	signal s_usb_read_addr : std_logic_vector(11 downto 0);		--USB MEMORY READ ADDRESS CREATE BY USB_TRANS ENTITY
	signal s_usb_read_data : std_logic_vector(31 downto 0);		--DATA FROM THE MEMORY TO THE USB_TRANS ENTITY



	signal s_clk_24M_inv : std_logic;
	signal s_clk_48M_inv : std_logic;

begin


	--INSTANTIATION OF THE ODDR CLOCK BUFFERING TO STOP ERRORS FROM THE MAP PROCESS	{{{

	--DECLARE INVERTED CLOCK SIGNALS TO AVOID WARNINGS IN ODDR INSTANTIATIONS
	s_clk_24M_inv <= not s_clk_24M;
	s_clk_48M_inv <= not s_clk_48M;

	ODDR2_usb_clk : ODDR2
	port map(
				Q => usb_clk,
				C0 => s_clk_24M,
				C1 => s_clk_24M_inv,
				D0 => '1',
				D1 => '0'
			);

	ODDR2_usb_fclk : ODDR2
	port map(
				Q => usb_fclk,
				C0 => s_clk_48M,
				C1 => s_clk_48M_inv,
				D0 => '1',
				D1 => '0'
			);

	--}}}


	o_usb_packet_ready <= s_usb_packet_ready;

	--TRISTATE SIGNALS TO THE USB FIFO MEMORY
	s_fifo_data_in <= fifo_data_io;
	fifo_data_io <= s_fifo_data_out when s_fifo_data_T = '0' else (others => 'Z');



	--GENERATE THE 48 AND 24 MHZ CLOCK SIGNALS --{{{

	i_dcm_usbclock : dcm_clockgen
	port map(
				CLK_IN1	=> i_clk_160M,
				CLK_OUT1 => s_clk_48M,
				RESET => i_rst
			);


	gen_24MHz: pll_filter
	port map(
			 CLK_IN1 => s_clk_48M,
			 CLK_OUT1 => s_clk_24M
		 );

	--}}}

	i_usb: usb_trans  --{{{
		port map
		(
			USB_Clk48 => s_clk_48M,
			reset => i_rst,

			--CYPRESS USB SLAVE FIFO SIGNALS
			usb_sloe => usb_sloe,
			usb_slwr => usb_slwr,
			usb_slrd => usb_slrd,
			usb_flaga => usb_flaga,
			usb_flagb => usb_flagb,
			usb_flagc => usb_flagc,
			usb_flagd => usb_flagd,
			fifo_addr => fifo_addr,
			fifo_data_in => s_fifo_data_in,
			fifo_data_out => s_fifo_data_out,
			fifo_data_T => s_fifo_data_T,

			usb_reset_n => usb_reset_n,

			--USB RX SIGNALS	(FORM USB TO MEMORY)

			usb_write_en => s_usb_write_en,
			usb_write_data => s_usb_write_data,
			usb_write_addr => s_usb_write_addr,
			usb_packet_ready => s_usb_packet_ready,

			i_usb_packet_header => s_usb_rcv_header,

			--USB TX SIGNALS	(FROM MEMORY TO USB)

			usb_send_done => o_usb_send_done,
			usb_read_addr => s_usb_read_addr,
			usb_read_data => s_usb_read_data,
			usb_start_trans => i_usb_start_trans,
			usb_addr_offset => i_usb_addr_offset,
			usb_num_packets => i_usb_num_packets
		); --}}}


	--MEMORY BLOCKS FOR THE COMMUNICATION --{{{

	--USB READ BUFFER
	i_mem_usbrcv : mem_block
		port map(
				clka => s_clk_48M,							--WRITE CLOCK
				clkb => i_clk_160M,							--READ CLOCK
				waddr_a => s_usb_write_addr,				--WRITE ADDRESS ON PORT A
				raddr_a => (others => '0'),					--READ ADDRESS ON PORT A, DISABLED SINCE THE USB ONLY WRITES AND READING HAPPENS ON PORT B
				raddr_b => i_memread_addr, 					--READ ADDRESS ON PORT B
				din_a => s_usb_write_data,					--INPUT DATA FOR WRITING
				dout_a => s_usb_rcv_header,					--OUTPUT DATA ON PORT A, TIED TO THE ADDRESS OF THE PACKET HEADER
				dout_b => o_memread_data,					--OUTPUT DATA FOR READING
				we => s_usb_write_en						--WRITE ENABLE
			);


	--USB SEND BUFFER
	i_mem_usbsend : mem_block
	port map(
				clka => i_clk_160M,							--WRITE CLOCK
				clkb => s_clk_48M,							--READ CLOCK
				waddr_a => i_memsend_addr,					--WRITE ADDRESS ON PORT A
				raddr_a => (others => '0'),					--READ ADDRESS ON PORT A, DISABLED SINCE THE USB ONLY WRITES AND READING HAPPENS ON PORT B
				raddr_b => s_usb_read_addr, 				--READ ADDRESS ON PORT B
				din_a => i_memsend_data,					--INPUT DATA FOR WRITING
				dout_a => open,								--OUTPUT DATA ON PORT A, DISABLED SINCE THERE IS NO READ FROM THE USB
				dout_b => s_usb_read_data,					--OUTPUT DATA FOR READING
				we => i_memsend_wen							--WRITE ENABLE
			);



	--}}}
end;
