--LOOPBACK ENTITY FOR TESTING THE COMMUNICATION WITH THE CYPRESS USB IC ON THE FPGA BOARD


Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity usb_loopback is
	port(

			--SYSTEM SIGNALS
			i_sys_clk : in std_logic; 	--100MHz User Clock


			--CYPRESS USB COMPONENT PORTS
			usb_reset_n						: out std_logic;	--RESET OF THE CYPRESS USB CORE

			usb_sloe 						: out std_logic;	--USB_FIFO OUTPUT ENABLE
			usb_slwr 						: out std_logic;	--USB_FIFO WRITE ENABLE
			usb_slrd 						: out std_logic;	--USB_FIFO READ ENABLE
			usb_flaga 						: in std_logic; --EP2 DATA FLAG	--CONFIGURED AS IN ENDPOINT	IF '1' HAS DATA
			usb_flagb 						: in std_logic; --EP4 DATA FLAG
			usb_flagc 						: in std_logic; --EP6 DATA FLAG
			usb_flagd 						: in std_logic; --EP8 DATA FLAG	--CONFIGURED AS OUT ENDPOINT IF '1' IS AVAILABLE
			fifo_addr 						: out std_logic_vector(1 downto 0);	--SELECT FIFO ENDPOINT BUFFER : DEFAULT 00 ONLY 10 WHEN WRITING
			fifo_data_io					: inout std_logic_vector(7 downto 0);	--TRISTATE FIFO DATA TO WRITE OR READ

			usb_fclk						: out std_logic;	--USB FIFO CLOCK (48 MHz)
			usb_clk							: out std_logic		--CYPRESS EXTERNAL CLOCK (24 MHz)
	    );
end usb_loopback;




architecture test of usb_loopback is


	--CLOCK SIGNALS
	signal s_clk_160M : std_logic;
	signal s_reset : std_logic;


	component dcm_160M is	--{{{
	port(
			CLK_IN1		:	in std_logic;
			CLK_OUT1	:	out std_logic;
			RESET		:	in std_logic
--			LOCKED		:	out std_logic
		);
	end component; --}}}


	component cypress_usb_top is --{{{
		port(
				i_clk_160M						: in std_logic;		--A 160 MHZ CLOCK TO GENERATE THE 48 AND 24 MHZ
				i_rst							: in std_logic;		--RESET SIGNAL OF THE USB CORE


				--USB SLAVE FIFO SIGNALS CONNECTED TO THE EXTERNAL PORTS
				usb_reset_n						: out std_logic;	--RESET OF THE CYPRESS USB CORE

				usb_sloe 						: out std_logic;	--USB_FIFO OUTPUT ENABLE
				usb_slwr 						: out std_logic;	--USB_FIFO WRITE ENABLE
				usb_slrd 						: out std_logic;	--USB_FIFO READ ENABLE
				usb_flaga 						: in std_logic; --EP2 DATA FLAG	--CONFIGURED AS IN ENDPOINT	IF '1' HAS DATA
				usb_flagb 						: in std_logic; --EP4 DATA FLAG
				usb_flagc 						: in std_logic; --EP6 DATA FLAG
				usb_flagd 						: in std_logic; --EP8 DATA FLAG	--CONFIGURED AS OUT ENDPOINT IF '1' IS AVAILABLE
				fifo_addr 						: out std_logic_vector(1 downto 0);	--SELECT FIFO ENDPOINT BUFFER : DEFAULT 00 ONLY 10 WHEN WRITING
				fifo_data_io					: inout std_logic_vector(7 downto 0);	--TRISTATE FIFO DATA TO WRITE OR READ

				usb_fclk						: out std_logic;	--USB FIFO CLOCK (48 MHz)
				usb_clk							: out std_logic;	--CYPRESS EXTERNAL CLOCK (24 MHz)


				--CONNECTION FROM THE CONTROL LOGIC TO THE USB_TRANS ENTITY
				i_usb_start_trans 				: in std_logic;							--SIGNAL TO START THE TRANSMISSION
				i_usb_addr_offset 				: in std_logic_vector(11 downto 0);		--MEMORY START ADDRESS OF THE PACKET
				i_usb_num_packets 				: in std_logic_vector(1 downto 0);		--NUMBER OF PACKETS TO BE TRANSMITTED
				o_usb_packet_ready				: out std_logic;		--INTERRUPT SIGNAL INDICATING A NEW PACKET, ACTIVE FOR ONE CLOCK CYCLE
				o_usb_send_done					: out std_logic;		--INDICATING THAT THE SENDING IS FINISHED

				--CONNECTION FROM THE CONTROL LOGIC TO THE SEND BUFFER
				i_memsend_data					: in std_logic_vector(31 downto 0);	--32 BIT DATA VECTOR TO WRITE IN THE SEND BUFFER
				i_memsend_addr					: in std_logic_vector(11 downto 0);	--THE BIT ADDRESS FOR WRITING THE DATA, THE LAST 2 BITS WILL BE CROPPED TO GET THE BYTE ADDRESS
				i_memsend_wen					: in std_logic;

				--CONNECTION FROM THE CONTROL LOGIC TO THE READ BUFFER
				i_memread_addr					: in std_logic_vector(11 downto 0);
				o_memread_data					: out std_logic_vector(31 downto 0)
			);
	end component cypress_usb_top; --}}}

	signal s_tmp_data : std_logic_vector(31 downto 0);	--FF's storing the current read 32 bit vector of the USB packet

	type FSM_STATES is (S_IDLE,S_WRITE_FIRST,S_WRITE,S_SEND,S_RST);
	signal p_state, n_state : FSM_STATES;


	signal s_usb_send_done : std_logic;

	--FSM FLIPFLOP SIGNALS
	signal s_memsend_addr,n_memsend_addr : std_logic_vector(11 downto 0);
	signal s_memsend_wen,n_memsend_wen : std_logic;
	signal s_memread_addr,n_memread_addr : std_logic_vector(11 downto 0);
	signal s_usb_packet_ready : std_logic;
	signal s_usb_start_trans,n_usb_start_trans : std_logic;


	signal s_usb_addr_offset : std_logic_vector(11 downto 0);
	signal s_usb_num_packets : std_logic_vector(1 downto 0);


	signal s_word_cnt,n_word_cnt : std_logic_vector(6 downto 0);	--VARIABLE COUNTING THE NUMBER OF WORDS WRITTEN TO THE SEND BUFFER
begin


	s_usb_addr_offset <= (others => '0'); --DEFAULT: NO ADDRESS OFFSET NOT USED HERE
	s_usb_num_packets <= (others => '0'); --DEFAULT: ONLY ONE PACKET

	--CREATE THE 160 MHZ CLOCK --{{{
	i_dcm_160 : dcm_160M
	port map(
				CLK_IN1	=> i_sys_clk,
				CLK_OUT1 => s_clk_160M,
				RESET => '0'
			); --}}}


	--USB CONTROLLER INSTANCE
	i_cypress_usb_top : cypress_usb_top --{{{
	port map(
				i_clk_160M => s_clk_160M,
				i_rst => s_reset,


				--USB SLAVE FIFO SIGNALS CONNECTED TO THE EXTERNAL PORTS
				usb_reset_n => usb_reset_n,

				usb_sloe  => usb_sloe,
				usb_slwr  => usb_slwr,
				usb_slrd  => usb_slrd,
				usb_flaga  => usb_flaga,
				usb_flagb  => usb_flagb,
				usb_flagc  => usb_flagc,
				usb_flagd  => usb_flagd,
				fifo_addr  => fifo_addr,
				fifo_data_io => fifo_data_io,

				usb_fclk => usb_fclk,
				usb_clk => usb_clk,


				--CONNECTION FROM THE CONTROL LOGIC TO THE USB_TRANS ENTITY
				i_usb_start_trans  => s_usb_start_trans,
				i_usb_addr_offset  => s_usb_addr_offset,
				i_usb_num_packets  => s_usb_num_packets,
				o_usb_packet_ready => s_usb_packet_ready,
				o_usb_send_done => s_usb_send_done,

				--CONNECTION FROM THE CONTROL LOGIC TO THE SEND BUFFER
				i_memsend_data => s_tmp_data,		--LOOPBACK CONNECTION
				i_memsend_addr => s_memsend_addr,
				i_memsend_wen => s_memsend_wen,

				--CONNECTION FROM THE CONTROL LOGIC TO THE READ BUFFER
				i_memread_addr => s_memread_addr,
				o_memread_data => s_tmp_data		--LOOPBACK CONNECTION

			); --}}}


	comb_fsm : process (p_state, s_usb_packet_ready, s_memsend_addr,s_memread_addr,s_word_cnt,s_usb_send_done)
	begin
		n_memsend_addr <= s_memsend_addr;
		n_memread_addr <= s_memread_addr;

		n_word_cnt <= s_word_cnt;

		n_memsend_wen <= '0';
		n_usb_start_trans <= '0';

		n_state <= p_state;

		s_reset <= '0';

		case p_state is

			when S_RST =>
				s_reset <= '1';
				n_state <= S_IDLE;

			when S_IDLE =>
				n_memsend_addr <= (others => '0');
				n_memread_addr <= (others => '0');
				n_word_cnt <= (others => '0');

				if s_usb_packet_ready = '1' then
					n_state <= S_WRITE_FIRST;
				end if;

			when S_WRITE_FIRST =>			--WRITE FIRST WITHOUT INCREMENTING THE ADDRESS
				n_state <= S_WRITE;
				n_memsend_wen <= '1';
				n_memread_addr <= std_logic_vector(unsigned(s_memread_addr)+4); --INCREMENT THE READ ADDRESS, THE READ VALUE WILL BE VALID IN THE NEXT CLOCK CYCLE


			when S_WRITE =>
				n_memsend_wen <= '1';
				n_memsend_addr <= std_logic_vector(unsigned(s_memsend_addr)+4);
				n_memread_addr <= std_logic_vector(unsigned(s_memread_addr)+4);	--KEEP THE READ ADDRESS AHEAD OF THE WRITE ADDRESS

				n_word_cnt <= std_logic_vector(unsigned(s_word_cnt)+1);
				if s_word_cnt = "1111111" then	--128 words have been written, start the transmission
					n_memsend_wen <= '0';		--ALREADY STOP HERE TO AVOID WRITING THE LAST VALUE TWICE
					n_state <= S_SEND;
				end if;

			when S_SEND =>
				n_usb_start_trans <= '1';
				if s_usb_send_done = '1' then
					n_state <= S_IDLE;
				end if;


			when others => n_state <= S_RST; --DEFAULT : GO TO RESET
		end case;
	end process comb_fsm;



	syn_fsm : process (s_clk_160M)
	begin
		if rising_edge(s_clk_160M) then
			p_state <= n_state;
			s_word_cnt <= n_word_cnt;
			s_memsend_addr <= n_memsend_addr;
			s_memsend_wen <= n_memsend_wen;
			s_memread_addr <= n_memread_addr;
			s_usb_start_trans <= n_usb_start_trans;
		end if;
	end process syn_fsm;

end;
