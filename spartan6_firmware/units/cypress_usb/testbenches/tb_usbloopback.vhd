  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

  ENTITY tb_usbloopback IS
  END tb_usbloopback;

  ARCHITECTURE behavior OF tb_usbloopback IS

  -- Component Declaration
	component usb_loopback is
		port(

				--SYSTEM SIGNALS
				i_sys_clk : in std_logic; 	--100MHz User Clock


				--CYPRESS USB COMPONENT PORTS
				usb_reset_n						: out std_logic;	--RESET OF THE CYPRESS USB CORE

				usb_sloe 						: out std_logic;	--USB_FIFO OUTPUT ENABLE
				usb_slwr 						: out std_logic;	--USB_FIFO WRITE ENABLE
				usb_slrd 						: out std_logic;	--USB_FIFO READ ENABLE
				usb_flaga 						: in std_logic; --EP2 DATA FLAG	--CONFIGURED AS IN ENDPOINT	IF '1' HAS DATA
				usb_flagb 						: in std_logic; --EP4 DATA FLAG
				usb_flagc 						: in std_logic; --EP6 DATA FLAG
				usb_flagd 						: in std_logic; --EP8 DATA FLAG	--CONFIGURED AS OUT ENDPOINT IF '1' IS AVAILABLE
				fifo_addr 						: out std_logic_vector(1 downto 0);	--SELECT FIFO ENDPOINT BUFFER : DEFAULT 00 ONLY 10 WHEN WRITING
				fifo_data_io					: inout std_logic_vector(7 downto 0);	--TRISTATE FIFO DATA TO WRITE OR READ

				usb_fclk						: out std_logic;	--USB FIFO CLOCK (48 MHz)
				usb_clk							: out std_logic		--CYPRESS EXTERNAL CLOCK (24 MHz)
			);
	end component;




				--SYSTEM SIGNALS
				signal i_sys_clk :  std_logic:='0'; 	--100MHz User Clock


				--CYPRESS USB COMPONENT PORTS
				signal usb_reset_n						:  std_logic;	--RESET OF THE CYPRESS USB CORE

				signal usb_sloe 			:  std_logic;	--USB_FIFO OUTPUT ENABLE
				signal usb_slwr 			:  std_logic;	--USB_FIFO WRITE ENABLE
				signal usb_slrd 			:  std_logic;	--USB_FIFO READ ENABLE
				signal usb_flaga 			:  std_logic; --EP2 DATA FLAG	--CONFIGURED AS IN ENDPOINT	IF '1' HAS DATA
				signal usb_flagb 			:  std_logic; --EP4 DATA FLAG
				signal usb_flagc 			:  std_logic; --EP6 DATA FLAG
				signal usb_flagd 			:  std_logic; --EP8 DATA FLAG	--CONFIGURED AS OUT ENDPOINT IF '1' IS AVAILABLE
				signal fifo_addr 			:  std_logic_vector(1 downto 0);	--SELECT FIFO ENDPOINT BUFFER : DEFAULT 00 ONLY 10 WHEN WRITING
				signal fifo_data_io		:  std_logic_vector(7 downto 0);	--TRISTATE FIFO DATA TO WRITE OR READ

				signal usb_fclk						:  std_logic;	--USB FIFO CLOCK (48 MHz)
				signal usb_clk							:  std_logic;		--CYPRESS EXTERNAL CLOCK (24 MHz)


				constant clock_per : time := 10 ns;

  BEGIN


	  def_clock : process
	  begin
		  i_sys_clk <= '0';
		  wait for clock_per/2;
		  i_sys_clk <= '1';
		  wait for clock_per/2;

	  end process;


  -- Component Instantiation

	  --TRISTATE FOR THE FIFO DATA
	  fifo_data_io <= X"AA" when usb_sloe = '0' else (others => 'Z');


		uut : usb_loopback
			port map(

					--SYSTEM SIGNALS
					i_sys_clk => i_sys_clk,


					--CYPRESS USB COMPONENT PORTS
					usb_reset_n					 => usb_reset_n,

					usb_sloe 					 => usb_sloe,
					usb_slwr 					 => usb_slwr,
					usb_slrd 					 => usb_slrd,
					usb_flaga 					 => usb_flaga,
					usb_flagb 					 => usb_flagb,
					usb_flagc 					 => usb_flagc,
					usb_flagd 					 => usb_flagd,
					fifo_addr 					 => fifo_addr,
					fifo_data_io				 => fifo_data_io,

					usb_fclk					 => usb_fclk,
					usb_clk						 => usb_clk
				);





  --  Test Bench Statements
     tb : PROCESS
     BEGIN
		 usb_flaga <= '0';
		 usb_flagd <= '1';
        wait for 200 ns; -- wait until global set/reset completes

		usb_flaga <= '1';
		wait for 200 ns;
		usb_flaga <= '0';



        -- Add user defined stimulus here

        wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench

  END;
