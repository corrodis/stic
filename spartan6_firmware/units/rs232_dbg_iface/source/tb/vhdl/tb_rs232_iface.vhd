Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity tb_rs232_iface is
end tb_rs232_iface;




architecture a of tb_rs232_iface is


	component rs232_dbg_iface is
		port(
				i_clk : in std_logic;	--SYSTEM CLOCK
				i_rst : in std_logic;	--SYSTEM RESET ACTIVE HIGH
				i_uart_rx : in std_logic;	--UART RX SIGNAL
				o_uart_tx : out std_logic;	--UART TX SIGNAL

				i_tx_data : in std_logic_vector(87 downto 0); --INPUT DATA TO SEND TO THE UART
				o_rx_data : out std_logic_vector(87 downto 0);	--RECEIVED DATA FROM THE UART

				i_start_trans : in std_logic;	--SIGNAL TO START THE DATA TRANMISSION
				o_send_done : out std_logic;	--DATA TRANSMISSION FINISHED

				i_rdata_ack : in std_logic;		--ACK THAT DATA HAS BEEN RECEIVED BY THE DEBUG FSM
				o_rdata_ready : out std_logic	--NEW REASSEMBLED DATA FROM THE UART IS READY
			);
	end component;

	component s_data is --{{{
	port(
		data : in std_logic_vector(7 downto 0);
		clk : in std_logic;
		rst_n: in std_logic;
		send : in std_logic;
		d_ready : out std_logic;
		TX_BIT : out std_logic
	);
	end component; --}}}



	signal s_clk : std_logic := '0';
	signal clk_period : time := 10 ns;

	signal s_rst : std_logic;
	signal s_rst_n : std_logic;
	signal s_uart_tx : std_logic;
	signal s_uart_rx : std_logic;
	signal s_tx_data : std_logic_vector(87 downto 0);
	signal s_rx_data : std_logic_vector(87 downto 0);
	signal s_start_trans : std_logic;
	signal s_send_done : std_logic;
	signal s_rdata_ready : std_logic;
	signal s_rdata_ack : std_logic;


	--UART S_DATA SIGNALS
	signal s_serial_data : std_logic_vector(7 downto 0);
	signal s_send_uart : std_logic;
	signal s_send_ready : std_logic;

begin


	serial_send : s_data	--WRITING TO THE SERIAL INTERFACE FOR DEBUGGING PURPOSES
	port map(
				data => s_serial_data,
				clk => s_clk,
				rst_n => s_rst_n,
				send => s_send_uart,
				d_ready => s_send_ready,
				TX_BIT => s_uart_rx
				);


	uut: rs232_dbg_iface
		port map(
				i_clk => s_clk,
				i_rst => s_rst,

				i_uart_rx => s_uart_rx,
				o_uart_tx => s_uart_tx,

				i_tx_data => s_tx_data,
				o_rx_data => s_rx_data,

				i_start_trans => s_start_trans,
				o_send_done => s_send_done,

				i_rdata_ack => s_rdata_ack,
				o_rdata_ready => s_rdata_ready
			);



		s_clk <= not s_clk after clk_period;


		stim : process
		begin
			s_rst_n <= '0';
			s_rst <= '1';
			s_send_uart <= '0';
			s_rdata_ack <= '0';
			s_tx_data <= X"0102030405060708090a0b";

			wait for 100 ns;
			s_rst_n <= '1';
			s_rst <= '0';
			wait for 900 ns; 


			s_serial_data <= X"01";
			wait for 10 us;
			wait until rising_edge(s_clk);
			s_send_uart <= '1';
			wait until s_send_ready = '1';
			s_send_uart <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '1';

			s_serial_data <= X"02";
			wait for 10 us;
			wait until rising_edge(s_clk);
			s_send_uart <= '1';
			wait until s_send_ready = '1';
			s_send_uart <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '1';

			s_serial_data <= X"03";
			wait for 10 us;
			wait until rising_edge(s_clk);
			s_send_uart <= '1';
			wait until s_send_ready = '1';
			s_send_uart <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '1';

			s_serial_data <= X"04";
			wait for 10 us;
			wait until rising_edge(s_clk);
			s_send_uart <= '1';
			wait until s_send_ready = '1';
			s_send_uart <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '1';

			s_serial_data <= X"05";
			wait for 10 us;
			wait until rising_edge(s_clk);
			s_send_uart <= '1';
			wait until s_send_ready = '1';
			s_send_uart <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '1';
			
			s_serial_data <= X"06";
			wait for 10 us;
			wait until rising_edge(s_clk);
			s_send_uart <= '1';
			wait until s_send_ready = '1';
			s_send_uart <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '1';

			s_serial_data <= X"07";
			wait for 10 us;
			wait until rising_edge(s_clk);
			s_send_uart <= '1';
			wait until s_send_ready = '1';
			s_send_uart <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '1';

			s_serial_data <= X"08";
			wait for 10 us;
			wait until rising_edge(s_clk);
			s_send_uart <= '1';
			wait until s_send_ready = '1';
			s_send_uart <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '1';

			s_serial_data <= X"09";
			wait for 10 us;
			wait until rising_edge(s_clk);
			s_send_uart <= '1';
			wait until s_send_ready = '1';
			s_send_uart <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '1';

			s_serial_data <= X"0a";
			wait for 10 us;
			wait until rising_edge(s_clk);
			s_send_uart <= '1';
			wait until s_send_ready = '1';
			s_send_uart <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '0';
			wait until rising_edge(s_clk);
			s_rst_n <= '1';

			wait until s_rdata_ready = '1';
			wait until rising_edge(s_clk);
			s_rdata_ack <= '1';
			wait until rising_edge(s_clk);
			s_rdata_ack <= '0';


			wait for 50 ns;
			wait until rising_edge(s_clk);

			s_start_trans <= '1';
			wait until s_send_done = '1';
			wait until rising_edge(s_clk);
			s_start_trans <= '0';

			wait;

		end process stim;



end;
