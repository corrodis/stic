--SPI CONTROL UNIT FOR SENDING BIT PATTERNS OF ARBITRARY LENGTH THROUGH 32 BIT WORDS

Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.STD_LOGIC_UNSIGNED.all;


entity spi_control is
		generic(
				C_NUM_WORDS : integer := 146;				--THE TOTAL NUMBER OF WORDS TO READ FROM THE MEMORY
				C_FIRST_MSB : integer := 16;				--THE MSB OF THE FIRST WORD TO WRITE
				C_NUM_CLIENTS	: integer := 1;				--NUMBER OF CLIENTS CONNECTED TO THE SPI INTERFACE
				C_CLK_DIVIDE : integer := 10;				--CLOCK DIVISION FACTOR FOR THE SYSTEM TO SPI CLOCK DIVISION
				C_HANDLEID_WIDTH : integer := 1				--WIDTH OF THE HANDLEID FOR MULTIPLEXING OF THE SIGNALS
			);
	port(
				--SYSTEM SIGNALS
				i_clk : in std_logic;						--SYSTEM CLOCK SIGNAL (100 MHz DESIGN)
				i_rst : in std_logic;						--RESET SIGNAL FOR THE SPI COMPONENT


				--CONTROL SIGNALS FROM THE DAQ CONTROL
				i_handleid 	: in std_logic_vector(C_HANDLEID_WIDTH-1 downto 0);	--THE HANDLE ID OF THE CHIP WE WANT TO CONFIGURE
				i_start_trans 	: in std_logic;						--START TRANSMISSION SIGNAL FROM THE DAQ CONTROL
				o_eot 			: out std_logic;					--END OF TRANSMISSION SIGNAL

				--DATA FROM OR TO THE USB INTERFACE
				o_read_addr	: out std_logic_vector(11 downto 0);	--THE MEMORY READ ADDRESS
				i_conf_data	: in std_logic_vector(31 downto 0);	--THE CONFIGURATION DATA FROM THE USB READ MEMORY

				o_return_data 	: out std_logic_vector(31 downto 0);	--THE OLD CONFIGURATION THAT WILL BE WRITTEN TO THE RETURN BUFFER (USB SEND MEMORY)
				o_write_addr 	: out std_logic_vector(11 downto 0);	--THE WRITE ADDRESS FOR THE RETURNED SPI DATA
				o_write_en 		: out std_logic;						--THE WRITE ENABLE FOR THE MEMORY



				--SPI INTERCONNECTION SIGNALS
				o_mdo							: out std_logic; 	--COMMON OUTPUT TO ALL CONNECTED SPI CLIENTS
				o_sclk							: out std_logic;	--COMMON CLOCK TO ALL CONNECTED SPI CLIENTS
				i_mdi							: in std_logic_vector(C_NUM_CLIENTS-1 downto 0);	--INDIVIDUAL SPI SLAVE INPUTS
				o_cs							: out std_logic_vector(C_NUM_CLIENTS-1 downto 0)	--INDIVIDUAL CHIP SELECT SIGNALS
	    );
end spi_control;




architecture behaviour of spi_control is



	component spi_config_top is	--{{{
		generic(
				C_NUM_CLIENTS	: integer := 1;				--NUMBER OF CLIENTS CONNECTED TO THE SPI INTERFACE
				C_CLK_DIVIDE : integer := 10;				--CLOCK DIVISION FACTOR FOR THE SYSTEM TO SPI CLOCK DIVISION
				C_HANDLEID_WIDTH : integer := 1				--WIDTH OF THE HANDLEID FOR MULTIPLEXING OF THE SIGNALS
			);
		port(
				--SYSTEM SIGNALS
				i_clk : in std_logic;						--SYSTEM CLOCK SIGNAL (100 MHz DESIGN)
--				i_rst : in std_logic;						--RESET SIGNAL FOR THE SPI COMPONENT

				--DATA FROM OR TO THE USB INTERFACE
				i_data : in std_logic_vector(31 downto 0);	--THE CONFIGURATION DATA TO TRANSMIT
				o_return_data : out std_logic_vector(31 downto 0);


				--CONTROL SIGNALS FOR THE SPI TRANSMISSION
				i_num_bits : in std_logic_vector(4 downto 0);					--HOW MANY BITS FROM THIS CONFIGURATION DATA TO SEND
				i_handleid : in std_logic_vector(C_HANDLEID_WIDTH-1 downto 0);	--SELECTION OF THE SPI CLIENT
				i_start : in std_logic;											--START THE SPI TRANSMISSION
				i_chipselect : in std_logic;									--WHEN TO SELECT THE CHIP
				o_busy : out std_logic;											--END OF TRANSMISSION


				--SPI INTERCONNECTION
				o_mdo							: out std_logic; 	--COMMON OUTPUT TO ALL CONNECTED SPI CLIENTS
				o_sclk							: out std_logic;	--COMMON CLOCK TO ALL CONNECTED SPI CLIENTS
				i_mdi							: in std_logic_vector(C_NUM_CLIENTS-1 downto 0);	--INDIVIDUAL SPI SLAVE INPUTS
				o_cs							: out std_logic_vector(C_NUM_CLIENTS-1 downto 0)	--INDIVIDUAL CHIP SELECT SIGNALS

			);
	end component;	--}}}



	type fsm_state is (FS_IDLE, FS_START_TRANS, FS_WAIT_READ, FS_SPI_PREPARE_DATA, FS_SPI_SEND, FS_SPI_WRITEBACK, FS_SPI_GETNEXT, FS_EOT);
	signal p_state, n_state : fsm_state;


	--MEMORY INTERFACE SIGNALS
	signal s_write_addr, n_write_addr : std_logic_vector(11 downto 0);
	signal s_write_en, n_write_en : std_logic;

	signal s_write_data : std_logic_vector(31 downto 0);

	signal s_read_addr, n_read_addr : std_logic_vector(11 downto 0);


	--SPI INTERFACE SIGNALS
	signal s_num_bits, n_num_bits : std_logic_vector(4 downto 0);
	signal s_spi_start, n_spi_start : std_logic;

	signal s_word_cnt, n_word_cnt : std_logic_vector(8 downto 0);

	signal s_spi_data_in, n_spi_data_in : std_logic_vector(31 downto 0);	--DATA TO THE SPI INTERFACE
	signal s_spi_cs, n_spi_cs : std_logic;		--SPI CHIP SELECT SIGNAL



	signal s_spi_data_out : std_logic_vector(31 downto 0);					--DATA FROM THE SPI INTERFACE
	signal s_spi_busy : std_logic;





begin

	--OUTPUT MEM CONNECTIONS
	o_read_addr <= s_read_addr;			--CURRENT READ ADDRESS FOR THE USB READ BUFFER
	o_write_addr <= s_write_addr;		--CURRENT WRITE ADDRESS FOR THE USB SEND BUFFER
	o_write_en <= s_write_en;			--WRITE ENABLE FOR THE USB SEND BUFFER
	o_return_data <= s_spi_data_out;	--THE RETURNED DATA FROM THE SPI FOR WRITING TO THE USB MEMORY



	u_spi_config : spi_config_top --{{{
		generic map(
				C_NUM_CLIENTS =>  C_NUM_CLIENTS,				--NUMBER OF CLIENTS CONNECTED TO THE SPI INTERFACE
				C_CLK_DIVIDE => C_CLK_DIVIDE,
				C_HANDLEID_WIDTH => C_HANDLEID_WIDTH
			)
		port map(
				--SYSTEM SIGNALS
				i_clk => i_clk,
		--		i_rst => i_rst,

				--DATA FROM OR TO THE USB INTERFACE
				i_data => s_spi_data_in,
				o_return_data => s_spi_data_out,


				--CONTROL SIGNALS FOR THE SPI TRANSMISSION
				i_num_bits => s_num_bits,
				i_handleid => i_handleid,
				i_start => s_spi_start,
				i_chipselect => s_spi_cs,
				o_busy => s_spi_busy,




				--SPI INTERCONNECTION
				o_mdo => o_mdo,
				o_sclk => o_sclk,
				i_mdi => i_mdi,
				o_cs => o_cs

			);
		--}}}


	fsm_comb : process(p_state, i_start_trans, s_num_bits, s_spi_data_in, s_read_addr,s_spi_start,s_write_addr, s_word_cnt,
						i_conf_data, s_spi_busy
		)	--{{{
		CONSTANT C_ZERO_FILL : std_logic_vector(30 - C_FIRST_MSB downto 0) := (others => '0');
	begin


		--DEFAULT VALUES FOR THE FF'S
		--FSM N_STATE DEFAULT
		n_state <= p_state;
		
		n_word_cnt <= s_word_cnt;

		n_num_bits <= s_num_bits;
		n_spi_data_in <= s_spi_data_in;

		n_spi_start <= '0';
		n_spi_cs <= '1';
		n_read_addr <= s_read_addr;
		n_write_addr <= s_write_addr;
		n_write_en <= '0';

		o_eot <= '0';

		case p_state is

			when FS_IDLE =>

				if i_start_trans = '1' then
					n_state <= FS_START_TRANS;
				end if;

			when FS_START_TRANS =>
				n_read_addr <= std_logic_vector(to_unsigned(C_NUM_WORDS*4,12));			--THE BYTE ADDRESS OF THE FIRST CONFIGURATION WORD
				n_word_cnt <= "000000001";	--THE WORD COUNTER IS SET TO 1 SINCE THE HEADER IS ALREADY WRITTEN BY THE DAQ_CONTROL
				n_spi_cs <= '0';
				n_state <= FS_WAIT_READ;


			when FS_WAIT_READ =>		--WAIT FOR THE FIRST WORD TO BE READ FROM THE MEMORY
				n_read_addr <= std_logic_vector(to_unsigned(C_NUM_WORDS*4,12));			--THE BYTE ADDRESS OF THE FIRST CONFIGURATION WORD
				n_word_cnt <= "000000001";	--THE WORD COUNTER IS SET TO 1 SINCE THE HEADER IS ALREADY WRITTEN BY THE DAQ_CONTROL
				n_spi_cs <= '0';
				n_state <= FS_SPI_PREPARE_DATA;


			when FS_SPI_PREPARE_DATA =>

				n_spi_cs <= '0';
				if s_word_cnt = "000000001" then
					n_num_bits <= std_logic_vector(to_unsigned(C_FIRST_MSB,5));			--ASSIGN THE NUMBER OF BITS TO TRANSMIT
					n_spi_data_in <= i_conf_data(C_FIRST_MSB downto 0) & C_ZERO_FILL; --ASSIGN THE FIRST BITSHIFTED WORD
				else
					n_spi_data_in <= i_conf_data;						--TRANSMISSION OF A FULL 32 BIT WORD
					n_num_bits <= "11111";
				end if;

				n_write_addr <= s_read_addr;		--WRITE THE RETURNED SPI DATA AT THE SAME POSITION IN THE USB BUFFER

				n_state <= FS_SPI_SEND;


			when FS_SPI_SEND => 		--SEND THE 32BIT WORD TO THE CHIP
				n_spi_cs <= '0';
				n_spi_start <= '1';
				if s_spi_busy = '0' then
					n_state <= FS_SPI_WRITEBACK;
				end if;
				n_write_addr <= s_read_addr;		--WRITE THE RETURNED SPI DATA AT THE SAME POSITION IN THE USB BUFFER


			when FS_SPI_WRITEBACK =>
				n_spi_cs <= '0';
				n_write_en <= '1';

				n_state <= FS_SPI_GETNEXT;

				n_read_addr <= std_logic_vector(unsigned(s_read_addr)-4); --DECREMENT THE READ ADDRESS
				n_write_addr <= s_read_addr;		--WRITE THE RETURNED SPI DATA AT THE SAME POSITION IN THE USB BUFFER

			when FS_SPI_GETNEXT =>
				n_spi_cs <= '0';

				n_word_cnt <= std_logic_vector(unsigned(s_word_cnt)+1);	--INCREMENT THE WORD COUNTER

				if s_word_cnt = std_logic_vector(to_unsigned(C_NUM_WORDS,9)) then --CHECK IF ALL WORDS HAVE BEEN SENT
					n_state <= FS_EOT;
				else
					n_state <= FS_SPI_PREPARE_DATA;
				end if;


			when FS_EOT =>			--RETURN TO THE IDLE STATE WHEN THE START TRANSMISSION SIGNAL IS RELEASED
				o_eot <= '1';
				if i_start_trans = '0' then
					n_state <= FS_IDLE;
				end if;

			when others => n_state <= FS_IDLE;
		end case;

	end process;	--}}}





	fsm_syn : process (i_clk,i_rst)
	begin
		if i_rst = '1' then
			p_state <= FS_IDLE;
		elsif rising_edge(i_clk) then

			p_state <= n_state;
			s_word_cnt <= n_word_cnt;
			

			--SPI SIGNALS
			s_spi_cs <= n_spi_cs;
			s_spi_start <= n_spi_start;
			s_spi_data_in <= n_spi_data_in;

			s_write_addr <= n_write_addr;
			s_read_addr <= n_read_addr;
			s_write_en <= n_write_en;

			--BIT COUNTER
			s_num_bits <= n_num_bits;
		end if;

	end process fsm_syn;

end;
