Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity clock_recovery is
	port(
			i_serial_data : in std_logic;

			i_clk0 : in std_logic;		--TWO PHASESHIFTED CLOCKS USED TO SAMPLE THE DATA IN 4 TIME DOMAINS
			i_clk90 : in std_logic;

			o_syn_data : out std_logic_vector(1 downto 0);	--THE DATA SAMPLED AT THE INPUT BY THE CORRECT CLOCK EDGE SYNCHRONOUS TO I_CLK0
			o_dv : out std_logic_vector(1 downto 0)
	    );
end clock_recovery;




architecture a of clock_recovery is


	--THE DATA SAMPLED IN THE INDIVIDUAL TIME DOMAINS
	signal s_data0 : std_logic_vector(3 downto 0);	
	signal s_data90 : std_logic_vector(3 downto 0);
	signal s_data180 : std_logic_vector(3 downto 0);
	signal s_data270 : std_logic_vector(3 downto 0);

	--THE SIGNALS INDICATING BETWEEN WHICH DOMAINS A TRANSITION OCCURED
	signal s_transitionsP, s_transitionsN : std_logic_vector(3 downto 0);
	signal s_mux_value : std_logic_vector(3 downto 0);

	--SIGNALS ARE AS DESCRIBED IN THE XILINX XAPP224 NOTE
	signal s_use0 : std_logic;
	signal s_use1 : std_logic;
	signal s_use2 : std_logic;
	signal s_use3 : std_logic;

	signal s_use00 : std_logic;
	signal s_use11 : std_logic;
	signal s_use22 : std_logic;
	signal s_use33 : std_logic;

	signal s_d0 : std_logic_vector(1 downto 0);
	signal s_d1 : std_logic_vector(1 downto 0);
	signal s_d2 : std_logic_vector(1 downto 0);
	signal s_d3 : std_logic_vector(1 downto 0);


	signal s_dvalid : std_logic_vector(1 downto 0);


	signal pipe_ce0 : std_logic;


	attribute keep : string;
	attribute keep of s_data0 : signal is "true";
	attribute keep of s_data90 : signal is "true";
	attribute keep of s_data180 : signal is "true";
	attribute keep of s_data270 : signal is "true";


begin


		--======SAMPLING TIMEDOMAIN 0=====--	{{{

	process(i_clk0)
	begin
		if rising_edge(i_clk0) then
			s_data0 <= s_data0(2 downto 0) & i_serial_data;
		end if;
	end process;

	--}}}

		--======SAMPLING TIMEDOMAIN 1=====-- {{{
	process(i_clk0, i_clk90)
	begin

		if rising_edge(i_clk90) then
			s_data90(0) <= i_serial_data;
		end if;

		if rising_edge(i_clk0) then
			s_data90(3 downto 1) <= s_data90(2 downto 0);
		end if;
	end process;--}}}

		--======SAMPLING TIMEDOMAIN 2=====-- {{{
	process(i_clk0, i_clk90)
	begin
		if falling_edge(i_clk0) then
			s_data180(0) <= i_serial_data;
		end if;

		if rising_edge(i_clk90) then
			s_data180(1) <= s_data180(0);
		end if;

		if rising_edge(i_clk0) then
			s_data180(3 downto 2) <= s_data180(2 downto 1);
		end if;
	end process;--}}}

		--======SAMPLING TIMEDOMAIN 3=====-- {{{
	process(i_clk0, i_clk90)
	begin
		if falling_edge(i_clk90) then
			s_data270(0) <= i_serial_data;
		end if;

		if falling_edge(i_clk0) then
			s_data270(1) <= s_data270(0);
		end if;

		if rising_edge(i_clk90) then
			s_data270(2) <= s_data270(1);
		end if;

		if rising_edge(i_clk0) then
			s_data270(3) <= s_data270(2);
		end if;
	end process;--}}}


	--=====TRANSITION FINDING=====--
	p_findtrans: process(i_clk0)
	begin
		if rising_edge(i_clk0) then
			s_transitionsP(0) <= (s_data0(3) xor s_data0(2)) and not s_data0(2);		--TRANSISION IN TIMEDOMAIN 0 -> select domain 2 for data sampling
			s_transitionsP(1) <= (s_data90(3) xor s_data90(2)) and not s_data90(2);		--TRANSISION IN TIMEDOMAIN 1 -> select domain 3 for data sampling
			s_transitionsP(2) <= (s_data180(3) xor s_data180(2)) and not s_data180(2);	--TRANSISION IN TIMEDOMAIN 2 -> select domain 0 for data sampling
			s_transitionsP(3) <= (s_data270(3) xor s_data270(2)) and not s_data270(2);	--TRANSISION IN TIMEDOMAIN 3 -> select domain 1 for data sampling

			s_transitionsN(0) <= (s_data0(3) xor s_data0(2)) and s_data0(2);		--TRANSISION IN TIMEDOMAIN 0 -> select domain 2 for data sampling
			s_transitionsN(1) <= (s_data90(3) xor s_data90(2)) and s_data90(2);		--TRANSISION IN TIMEDOMAIN 1 -> select domain 3 for data sampling
			s_transitionsN(2) <= (s_data180(3) xor s_data180(2)) and s_data180(2);	--TRANSISION IN TIMEDOMAIN 2 -> select domain 0 for data sampling
			s_transitionsN(3) <= (s_data270(3) xor s_data270(2)) and s_data270(2);	--TRANSISION IN TIMEDOMAIN 3 -> select domain 1 for data sampling
		end if;
	end process;


	--=====STORE NEW MUX SELECT VALUE=====--
	p_storemux: process(i_clk0)
	begin
		if rising_edge(i_clk0) then
			s_use0 <= '0';
			s_use1 <= '0';
			s_use2 <= '0';
			s_use3 <= '0';

			if s_transitionsP = "1111" or s_transitionsN="1111" then
				s_use2 <= '1';
			end if;

			if s_transitionsP = "0001" or s_transitionsN="0001" then
				s_use3 <= '1';
			end if;

			if s_transitionsP = "0011" or s_transitionsN="0011" then
				s_use0 <= '1';
			end if;

			if s_transitionsP = "0111" or s_transitionsN="0111" then
				s_use1 <= '1';
			end if;

			
			if (s_use0 = '1' or s_use1 = '1' or s_use2 = '1' or s_use3 = '1') then
				pipe_ce0 <= '1';
				s_use00 <= s_use0;
				s_use11 <= s_use1;
				s_use22 <= s_use2;
				s_use33 <= s_use3;
			end if;

			if pipe_ce0 = '1' then
				o_syn_data <= s_d0 or s_d1 or s_d2 or s_d3;
			end if;
			
			if s_use33 = '1' and s_use0 = '1' then 
				s_dvalid <= "00" ;
			elsif s_use00 = '1' and s_use3 = '1' then
				s_dvalid <= "11" ;
			else 
				s_dvalid <= "01" ;
			end if ;

		end if;
	end process;



	o_dv <= s_dvalid;

	s_d0(0) <= s_data0(3) and s_use00;
	s_d0(1) <= s_data0(3) and s_use33;

	s_d1(0) <= s_data90(3) and s_use11;
	s_d1(1) <= '0';

	s_d2(0) <= s_data180(3) and s_use22;
	s_d2(1) <= '0';

	s_d3(0) <= s_data270(3) and s_use33;
	s_d3(1) <= s_data270(3) and s_use00;


end;

