Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity tb_clock_recovery is
end tb_clock_recovery;




architecture a of tb_clock_recovery is


	component clock_recovery is	--{{{
		port(
				i_serial_data : in std_logic;

				i_clk0 : in std_logic;		--TWO PHASESHIFTED CLOCKS USED TO SAMPLE THE DATA IN 4 TIME DOMAINS
				i_clk90 : in std_logic;

				o_syn_data : out std_logic_vector(1 downto 0); 	--THE DATA SAMPLED AT THE INPUT BY THE CORRECT CLOCK EDGE SYNCHRONOUS TO I_CLK0
				o_dv : out std_logic_vector(1 downto 0)
			);
	end component;	--}}}



	signal serial_data : std_logic;
	signal clk0 : std_logic;
	signal clk90 : std_logic;
	signal s_syn_data : std_logic_vector(1 downto 0);
	signal s_dv : std_logic_vector(1 downto 0);


	signal clk_data : std_logic := '0';


	constant clk_period : time := 10 ns;
	constant data_period : time := 9.4 ns;
	constant w_time : time := 8.1 ns;

begin


	clk_data <= not clk_data after data_period/2;


	uut: clock_recovery
	port map(
				i_serial_data => serial_data,
				i_clk0 => clk0,
				i_clk90 => clk90,
				o_syn_data => s_syn_data,
				o_dv => s_dv
			);


	clk_gen: process
	begin
		clk0 <= '0';
		clk90 <= '1';
		wait for clk_period/4;
		clk90 <= '0';
		wait for clk_period/4;
		clk0 <= '1';
		wait for clk_period/4;
		clk90 <= '1';
		wait for clk_period/4;
	end process;






	p_dstream : process
	begin

		serial_data <= '0';
		wait until rising_edge(clk_data);
		serial_data <= '1';
		wait until rising_edge(clk_data);
		serial_data <= '0';
		wait until rising_edge(clk_data);
		serial_data <= '1';
		wait until rising_edge(clk_data);

		loop
			serial_data <= '0';
			wait until rising_edge(clk_data);
			serial_data <= '0';
			wait until rising_edge(clk_data);
			serial_data <= '1';
			wait until rising_edge(clk_data);
			serial_data <= '0';
			wait until rising_edge(clk_data);
			serial_data <= '1';
			wait until rising_edge(clk_data);
			serial_data <= '1';
			wait until rising_edge(clk_data);
		end loop;

	end process;



end;

