Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity tb_spi_dbg_iface is
end tb_spi_dbg_iface;




architecture a of tb_spi_dbg_iface is

	component spi_dbg_iface is
		port(
				i_data : in std_logic_vector(87 downto 0);	--DEBUG DATA
				i_clk : in std_logic;					--SYSTEM CLK
				i_start: in std_logic;				--START SIGNAL. TRANSMISSION WILL START WHEN THIS SIGNAL GETS HIGH
				i_RX_BIT: in std_logic;				--INPUT FROM THE END OF THE DAISY CHAIN OF THE STIC CHIP
				o_busy : out std_logic;				--TRANSMISSTION IN PROGRESS SIGNAL. NEGATIVE LOGIC
				o_TX_Bit : out std_logic;				--OUTPUT TO THE STIC DAISY CHAIN
				o_chain_clk : out std_logic;			--CLOCK SIGNAL FOR THE DAISY CHAIN
				o_data_old : out std_logic_vector(87 downto 0)	--RECEIVED CONFIGURATION DATA
			);
	end component;


	signal s_data : std_logic_vector(87 downto 0);
	signal s_clk : std_logic := '0';
	signal c_clk_period : time := 10 ns;
	signal s_start : std_logic;
	signal s_rx_bit : std_logic;
	signal s_busy : std_logic;
	signal s_tx_bit : std_logic;
	signal s_chain_clk : std_logic;
	signal s_data_old : std_logic_vector(87 downto 0);

begin


	uut: spi_dbg_iface
	port map(
				i_data => s_data,
				i_clk => s_clk,
				i_start => s_start,
				i_RX_BIT => s_rx_bit,
				o_busy => s_busy,
				o_TX_Bit => s_tx_bit,
				o_chain_clk => s_chain_clk,
				o_data_old => s_data_old
			);


	s_clk <= not s_clk after c_clk_period;

	stim : process
	begin
		s_data <= X"0102030405060708090a0b";
		s_start <= '0';
		s_rx_bit <= '1';
		wait for 100 ns;
		wait until rising_edge(s_clk);
		s_start <= '1';
		wait until rising_edge(s_clk);
		wait until rising_edge(s_clk);
		wait until s_busy = '0';
		wait until rising_edge(s_clk);
		s_start <= '0';
		wait;


	end process stim;

end;
