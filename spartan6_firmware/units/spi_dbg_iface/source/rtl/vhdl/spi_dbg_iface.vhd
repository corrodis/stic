Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity spi_dbg_iface is
	port(
			i_data : in std_logic_vector(87 downto 0);	--DEBUG DATA
			i_clk : in std_logic;					--SYSTEM CLK
			i_start: in std_logic;				--START SIGNAL. TRANSMISSION WILL START WHEN THIS SIGNAL GETS HIGH
			i_RX_BIT: in std_logic;				--INPUT FROM THE END OF THE DAISY CHAIN OF THE STIC CHIP
			o_busy : out std_logic;				--TRANSMISSTION IN PROGRESS SIGNAL. NEGATIVE LOGIC
			o_TX_Bit : out std_logic;				--OUTPUT TO THE STIC DAISY CHAIN
			o_chain_clk : out std_logic;			--CLOCK SIGNAL FOR THE DAISY CHAIN
			o_data_old : out std_logic_vector(87 downto 0)	--RECEIVED CONFIGURATION DATA
	    );
end spi_dbg_iface;




architecture a of spi_dbg_iface is

------- SIGNAL DEFINITIONS
signal d_i,n_d_i     : std_logic_vector(87 downto 0);
signal d_old,n_d_old     : std_logic_vector(87 downto 0);
signal eot,n_eot     : std_logic;


type s_state is (S_TRANSMIT,S_INIT,S_STOP);	--define the states of the statemachine
signal present_st, next_st : s_state;

signal clk1kHz : std_logic := '0';
signal en_sample : std_logic;
signal enable_chain,n_enable_chain : std_logic;


signal counter,n_counter : std_logic_vector(6 downto 0);


component clock_div is
generic (Nd : Integer := 50);
PORT(
        clk, en : IN  STD_LOGIC;
        sample     : OUT STD_LOGIC);
end component;



------- BEHAVIOUR OF ARCHITECTURE

begin




o_chain_clk <= not clk1kHz and enable_chain;
o_TX_Bit <= d_i(d_i'high);			--transmit always the actual MSB
o_busy <=  not eot;
o_data_old <= d_old;

g_sam1: clock_div
generic map(Nd => 100)	--DIVIDE THE CLOCK DOWN
port map(
	clk => i_clk,
	en => en_sample,
	sample => clk1kHz
);



gen_mclk: process(i_clk)
begin
	if rising_edge(i_clk) then
		if i_start = '0' or eot = '1' then
			en_sample <= '0';
		elsif i_start = '1' then
			en_sample <= '1';
		end if;
	end if;
end process;





	comb : process (present_st, counter,enable_chain,d_old, d_i, i_data,i_RX_BIT)
	begin

		next_st <= present_st;
		n_eot <= '0';
		n_d_i <= d_i;
		n_d_old <= d_old;
		n_enable_chain <= enable_chain;
		n_counter <= counter;

		case present_st is

			when S_INIT =>	--start is '1' => enable the chain get the first bit from the data line and set d_i
				n_enable_chain	<= '1';
				n_d_i				<= i_data;
				n_d_old			<= d_old(d_old'high - 1 downto 0) & i_RX_BIT;
				next_st			<= S_TRANSMIT;

			when S_STOP =>
				n_eot <= '1';

			when S_TRANSMIT =>
				n_d_i	<= d_i(d_i'high - 1 downto 0) & d_i(d_i'high);	--d_i(31) is transmitted -> MSB, LSB last
				n_counter <= conv_std_logic_vector(unsigned(counter) - 1,7);					--decrease the counter
				if counter = "0000000" then 					--are we done yet?
					n_eot <= '1';					--transmission is finished
					next_st <= S_STOP;				--got to the stop state and wait for start='0'
					n_enable_chain <= '0';				--stop the spi clock
				else
					n_d_old <= d_old(d_old'high - 1 downto 0) & i_RX_BIT;	--sample the dataline
				end if;

			when others => NULL;

		end case;

	end process comb;




syn: process(clk1kHz,i_start)

begin
	if i_start = '0' then	--async clear with the start signal
		enable_chain <= '0';
		present_st <= S_INIT;
		d_i <= (others => '0');
		eot <= '0';			--The transmission has not yet begun, so EOT is 0
		counter <= conv_std_logic_vector(87,7);	--ALWAYS TRANSMIT 80 BITS

	elsif rising_edge(clk1kHz) then

		present_st <= next_st;
		enable_chain <= n_enable_chain;
		counter <= n_counter;
		d_old <= n_d_old;
		d_i <= n_d_i;
		eot <= n_eot;

	end if;
end process syn;

end;
