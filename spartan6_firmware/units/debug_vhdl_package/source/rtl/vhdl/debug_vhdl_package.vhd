--	PACKAGE CONTAINING HELPER FUNCTIONS AND TYPES FOR THE STIC3 ASIC
--
--	Author: Tobias
--  Date: Sun Jul  7 11:19:50 CEST 2013
--
--

Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.MATH_REAL.all;

use work.stic2_vhdl_package.all;

package debug_vhdl_package is


	--DEBUGGING CONSTANTS USED IN THE DESIGN
	CONSTANT C_MUX_MONWIDTH : INTEGER :=16;	--THE NUMBER OF USED MONITORING SIGNALS
	CONSTANT C_MUX_SELWIDTH : INTEGER :=4;	--THE REQUIRED SEL BIT WIDTH TO MULTIPLEX THE MONITOR SIGS


	--DEBUGGING COMMANDS: MONITORING CONTROL
	CONSTANT CMD_SET_MONITOR1 : std_logic_vector(7 downto 0) := X"01";	--SET THE MONITOR VALUE FOR MUX 1
	CONSTANT CMD_SET_MONITOR2 : std_logic_vector(7 downto 0) := X"03";	--SET THE MONITOR VALUE FOR MUX 2

	CONSTANT CMD_EN_MONITOR1 : std_logic_vector(7 downto 0) := X"02";
	CONSTANT CMD_EN_MONITOR2 : std_logic_vector(7 downto 0) := X"04";

	CONSTANT CMD_DIS_MONITOR : std_logic_vector(7 downto 0) := X"00";

	--DEBUGGING COMMANDS: DATA INJECTION
	CONSTANT CMD_WRITE_L1 : std_logic_vector(7 downto 0) := X"10";	--WRITE EVENT DATA TO A LEVEL 1 BUFFER
	CONSTANT CMD_WRITE_L2 : std_logic_vector(7 downto 0) := X"20";	--WRITE EVENT DATA TO THE LEVEL 2 BUFFER

	--DEBUGGING COMMANDS: DATA READING
	--THE GROUP NUMBER SO READ BACK IS GIVEN IN THE PAYLOAD
	CONSTANT CMD_READ_DSTORING_IN : std_logic_vector(7 downto 0) := X"30"; --"00110000" READ THE DATA TO THE L1 STORING
	CONSTANT CMD_READ_L1_FIFO_IN : std_logic_vector(7 downto 0) := X"40"; --"01000000"; READ THE DATA TO THE L1 FIFO
	CONSTANT CMD_READ_L1_FIFO_OUT : std_logic_vector(7 downto 0) := X"50"; --"01010000"; READ THE DATA TO THE GROUP SELECT
	CONSTANT CMD_READ_GROUP_SEL_OUT : std_logic_vector(7 downto 0) := X"60"; --"01100000"; READ THE DATA TO THE MS SELECT
	CONSTANT CMD_READ_MS_SEL_OUT : std_logic_vector(7 downto 0) := X"70";		--"01110000"; READ THE DATA TO THE L2 FIFO
	CONSTANT CMD_READ_L2_FIFO_OUT : std_logic_vector(7 downto 0) := X"80";		--"01110000"; READ THE DATA FROM THE L2 FIFO



	CONSTANT CMD_DIS_DATA_FLOW_MON : std_logic_vector(7 downto 0) := X"F0"; --"11110001"; SWITCH OFF THE DATA FLOW MONITOR
	CONSTANT CMD_EN_DATA_FLOW_MON : std_logic_vector(7 downto 0) := X"F1"; --"11110001"; SWITCH ON THE DATA FLOW MONITOR



	--======================DEBUGGING RECORDS AND FUNCTIONS====================
	--DEBUG RECORD CONTAINING THE DEBUG SIGNALS
	type type_monitor_sigs is --{{{
		record
			init_trans : std_logic;
			--BYTE CLOCK AND DATA READY
			byte_clk : std_logic;
			L1_any_dready : std_logic_vector(3 downto 0);

			--FIFO SIGNALS
			L1_fifo_empty : std_logic_vector(3 downto 0);
			L1_fifo_full : std_logic_vector(3 downto 0);
			L2_fifo_empty : std_logic;
			L2_fifo_full : std_logic;
		end record;	--}}}

	--PACKET STRUCTURE OF THE DEBUGGING COMMUNICATION
	type type_debug_msg is
		record
			cmd : std_logic_vector(7 downto 0);	--COMMAND ID THE PAYLOAD
			data : std_logic_vector(79 downto 0); --PAYLOAD DATA CAN TAKE A FULL TDC EVENT OF 78 BITS
		end record;


	--RECORD OF THE DATA FLOW MONITORS
	type t_dflow_array is array(N_GROUPS-1 downto 0) of std_logic_vector(79 downto 0); --ARRAY TO REDUCE THE SIGNAL NUMBER
	type t_dflow_dstoring is array (N_GROUPS -1 downto 0) of std_logic_vector(78 downto 0);	--TO CONNECT THE DATA STORING FLOW MONITOR PINS WITHOUT WARNINGS

	type type_data_flow_sigs is
		record
--			hit_receiver : t_dflow_array;
			data_storing_to_L1 : t_dflow_array; --any_dready and write_en and mux_out_extended_s
			L1_to_GroupSelect : t_dflow_array;	--fifo_empty and read_en and fifo_data_out
			GroupSelect_to_MS : std_logic_vector(79 downto 0);	--group_select_dready and ms_busy and group_select_out 
			MS_to_L2 : std_logic_vector(48 downto 0);			--write_en + ms_to_L2
			L2_fifo_to_fgen : std_logic_vector(48 downto 0);		--read_en_L2+fifo_data_out_L2
		end record;

	function vector_to_debugmsg(config_vector : std_logic_vector(87 downto 0)) return type_debug_msg;
	function debugmsg_to_vector(msg : type_debug_msg) return std_logic_vector;

	function monitors_to_vector(monitors : type_monitor_sigs) return std_logic_vector;
	--====================END DEBUGGING SIGNALS AND FUNCTIONS

end;	--package



package body debug_vhdl_package is

		function vector_to_debugmsg (config_vector : std_logic_vector(87 downto 0)) return type_debug_msg is --{{{
                variable msg : type_debug_msg;
        begin

				msg.cmd := config_vector(87 downto 80);	--COMMAND ID THE PAYLOAD
				msg.data := config_vector(79 downto 0); --PAYLOAD DATA CAN TAKE A FULL TDC EVENT OF 78 BITS

                return msg;
        end;    --}}}

		function debugmsg_to_vector (msg : type_debug_msg) return std_logic_vector is --{{{
			variable vect : std_logic_vector(87 downto 0);
		begin

			vect(87 downto 80) := msg.cmd;
			vect(79 downto 0) := msg.data;

			return vect;
		end;--}}}




		function monitors_to_vector (monitors : type_monitor_sigs) return std_logic_vector is --{{{
			variable m_vect : std_logic_vector(C_MUX_MONWIDTH-1 downto 0);
		begin
			m_vect(0) := monitors.byte_clk;
			m_vect(4 downto 1) := monitors.L1_any_dready;
			m_vect(8 downto 5) := monitors.L1_fifo_empty;
			m_vect(12 downto 9) := monitors.L1_fifo_full;
			m_vect(13) := monitors.L2_fifo_empty;
			m_vect(14) := monitors.L2_fifo_full;
			m_vect(15) := monitors.init_trans;

			return m_vect;
		end;	--}}}



end package body;
