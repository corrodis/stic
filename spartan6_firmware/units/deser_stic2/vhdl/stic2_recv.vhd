--STIC2 DATA RECEIVER
--ENTITY CONTAINING THE DESERIALIZER, FRAME DECOMPOSING AND BUFFERING OF THE DECODED EVENTS


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;



entity stic2_recv is
	port (
				--DEBUGGING OUTPUTS
			o_byte_clk : out std_logic;
			o_sync_found : out std_logic;

			i_start_val : in std_logic_vector(3 downto 0);	--THE RESET VALUE OF THE BYTE CLOCK COUNTER
			i_rst		: in std_logic;		--THE COMMON RESET SIGNAL TO SYNC THE DECODER AND RECEIVER
			--STIC2 DIGITAL INPUTS
			i_stic2_txd : in std_logic;		--THE LVDS SERIAL INPUT FROM STIC2
			i_stic2_clk : in std_logic;		--THE CLOCK SIGNAL OF THE 160 MHz CLOCK
			i_stic2_clk90 : in std_logic;	--A 90 DEGREE PHASESHIFTED COPY OF THE STIC CLOCK USED IN THE DATA RECOVERY

			--MICROBLAZE INPUT
			i_rd_en		: in std_logic;
			i_rd_clk	: in std_logic;
			o_fifo_full : out std_logic;
			o_fifo_empty : out std_logic;
			o_fifo_count : out std_logic_vector(6 downto 0);	--NUMBER OF EVENTS STORED IN THE FIFO
			o_fifo_data : out std_logic_vector(55 downto 0) --FIFO DATA TO BE READ BY THE MICROBLAZE

		 );
end entity stic2_recv;




architecture first_try of stic2_recv is


--SIGNAL AND COMPONENT DECLARATIONS --{{{1
	signal byte_data : std_logic_vector(7 downto 0);
	signal kontrol_bit : std_logic;
	signal byte_clk : std_logic;
	signal event_ready : std_logic;
	signal event_data : std_logic_vector(55 downto 0);

	signal s_syn_data : std_logic;	--RECOVERED SERIAL DATA SYNCHRONOUS TO THE STIC2 CLOCK



	component clock_recovery is	--{{{
		port(
				i_serial_data : in std_logic;

				i_clk0 : in std_logic;		--TWO PHASESHIFTED CLOCKS USED TO SAMPLE THE DATA IN 4 TIME DOMAINS
				i_clk90 : in std_logic;

				o_syn_data : out std_logic 	--THE DATA SAMPLED AT THE INPUT BY THE CORRECT CLOCK EDGE SYNCHRONOUS TO I_CLK0
			);
	end component;	--}}}


component frame_decomp is --{{{2
	port (
			i_k_signal 		: in std_logic;
			i_byte_data 	: in std_logic_vector(7 downto 0);
			i_byte_clk 		: in std_logic;
			i_rst 			: in std_logic;
			o_event_data 	: out std_logic_vector(55 downto 0);
			o_event_ready 	: out std_logic

		 );
	end component frame_decomp; --}}}

	
component deserializer is --{{{2
	port (
			i_ser_clk		: in std_logic;
			i_start_val		: in std_logic_vector(3 downto 0);
			i_rst			: in std_logic;
			i_serial_RXD	: in std_logic;
			o_byte_clock 	: out std_logic;
			o_byte_data		: out std_logic_vector(7 downto 0);
			o_sync_found	: out std_logic;			--DEBUG SIGNAL SHOWING WHEN A SYNC PACKET WAS FOUND
			o_kontrol		: out std_logic
		 );
	end component deserializer;
	 --}}}


component event_storing is --{{{2
	port (
			--SIGNALS FROM THE FRAME DECOMPOSER
			i_sys_clk : in std_logic;
			i_rst : std_logic;
			i_event_data : in std_logic_vector(55 downto 0);
			i_event_ready : in std_logic;
			--SIGNAL FROM THE MICROPROCESSOR
			i_rd_en : in std_logic;
			i_rd_clk : in std_logic;
			o_fifo_dout : out std_logic_vector(55 downto 0);
			o_fifo_empty : out std_logic;
			o_fifo_count : out std_logic_vector(6 downto 0);	--NUMBER OF EVENTS STORED IN THE FIFO
			o_fifo_full : out std_logic
		 );
end component event_storing; --}}}

--}}}



begin

--BEHAVORIAL DESCRIPTION --{{{

o_byte_clk <= byte_clk;

--	u_clock_recover: clock_recovery --{{{
--	port map(
--				i_serial_data => i_stic2_txd,
--				i_clk0 => i_stic2_clk,
--				i_clk90 => i_stic2_clk90,
--				o_syn_data => s_syn_data
--			);	--}}}


u_deser: deserializer --DESERIALIZER UNIT DECODING THE 8B10B bitstream{{{2
port map(
			o_sync_found => o_sync_found,
			i_start_val => i_start_val,
			i_rst => i_rst,
			i_ser_clk => i_stic2_clk,
			i_serial_RXD => i_stic2_txd,
			o_byte_clock => byte_clk,
			o_byte_data => byte_data,
			o_kontrol => kontrol_bit
		);
		--}}}


u_unframe: frame_decomp --{{{2
	port map(
			i_k_signal => kontrol_bit,
			i_byte_data => byte_data,
			i_byte_clk => byte_clk,
			i_rst => i_rst,
			o_event_data => event_data,
			o_event_ready => event_ready

		 );
	--}}}


u_fifo_storing: event_storing --{{{2
	port map(
			--SIGNALS FROM THE FRAME DECOMPOSER
			i_sys_clk		=> i_stic2_clk,
			i_rst			=> i_rst,
			i_event_data 	=> event_data,
			i_event_ready	=> event_ready,
			--SIGNAL FROM THE MICROPROCESSOR
			i_rd_en			=> i_rd_en,
			i_rd_clk		=> i_rd_clk,
			o_fifo_dout		=> o_fifo_data,
			o_fifo_empty	=> o_fifo_empty,
			o_fifo_count	=> o_fifo_count,
			o_fifo_full		=> o_fifo_full
		 );

	--}}}

end architecture first_try;	--}}}
