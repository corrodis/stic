--STIC2 FRAME DECOMPOSITION
--
-- LOOK FOR FRAME HEADERS EXTRACT THE EVENTS AND CHECK FOR CONSITENCY IN THE EVEN NUMBERS


LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.std_logic_arith.all;

entity frame_decomp is
	port (
			i_k_signal : in std_logic;
			i_byte_data : in std_logic_vector(7 downto 0);
			i_byte_clk : in std_logic;
			i_rst : in std_logic;
			o_event_data : out std_logic_vector(55 downto 0);
			o_event_ready : out std_logic

		 );
	end entity frame_decomp;



architecture with_frame_cnt of frame_decomp is
	CONSTANT head_id : std_logic_vector(7 downto 0) := "00011100"; --K28.0
	CONSTANT trail_id : std_logic_vector(7 downto 0) := "10011100"; --K28.4
	CONSTANT komma_sync : std_logic_vector(7 downto 0) := "10111100"; --K28.5


	type decomp_states is (	S_WAIT_HEADER,
							S_FRAME_NUM,
							S_GET_EVENTS,
							S_EVENT_COUNT
						);

	signal present_state, next_state : decomp_states;


	signal data_pipe : std_logic_vector(55 downto 0); --A PIPE CONTAINING THE LAST 10 DECODED BYTES
--	signal event_count : std_logic_vector(7 downto 0); --THE NUMBER OF EVENTS IN THE CURRENT FRAME
	signal next_event_data_count, event_data_count : std_logic_vector(3 downto 0);
	signal next_event_ready : std_logic;
	signal current_byte : std_logic_vector(7 downto 0); --THE CURRENTLY SAMPLED BYTE DATA
	signal frame_number, next_frame_number : std_logic_vector(7 downto 0); --THE FRAME NUMBER OF THE CURRENT TRANSMISSION
	signal no_event : std_logic;

begin

	o_event_data <= frame_number & data_pipe(55 downto 8);
	current_byte <= data_pipe(55 downto 48);




	sample_bytes : process(i_byte_clk)  --SAMPLE THE BYTE FROM THE DECODER {{{
	--WE NEED TO SAMPLE THE INCOMING BYTES SINCE IT WILL CHANGE WITH THE FALLING EDGE OF THE BYTE CLOCK
	begin
		if rising_edge(i_byte_clk) then
			data_pipe <= i_byte_data & data_pipe(55 downto 8);	 --PIPELINE CONTAINING THE LAST 10 bits
		end if;
	end process sample_bytes; --}}}




	fsm_comb : process (present_state, current_byte, i_k_signal, frame_number, event_data_count) --THE TRANSITION PART OF THE FSM{{{
	--NOT REALLY A MOORE FSM SINCE THE DATA IS ALSO ASSIGNED TO THE EVENT DATA ETC
	begin
		next_state <= present_state;
		next_event_data_count <= (others => '-');
		next_frame_number <= frame_number;
		next_event_ready <= '0';

		case present_state is

			when S_WAIT_HEADER =>				--WAIT UNTIL A FRAME HEADER IS FOUND
				if current_byte = head_id and i_k_signal = '1' then
					next_state <= S_FRAME_NUM;
				end if;

			when S_FRAME_NUM =>					--SAVE THE FRAME NUMBER FROM THE CURRENT DATA
				next_frame_number <= current_byte;
				next_event_data_count <= conv_std_logic_vector(5,4);
				next_state <= S_GET_EVENTS;

			when S_GET_EVENTS =>				--GET THE EVENT DATA OR TRAILER ID; GENERATE DATA READY IF EVENT DATA WAS RECEIVED
				if current_byte = trail_id and i_k_signal = '1' then
					next_state <= S_EVENT_COUNT;
				else
					if unsigned(event_data_count) = 0 then		--6 BYTES HAVE BEEN RECEIVED -> EVENT READY
						next_event_ready <= '1';
						next_event_data_count <= conv_std_logic_vector(5,4);
					else
						next_event_data_count <= conv_std_logic_vector(unsigned(event_data_count) - 1, 4); --ONE MORE EVENT BYTE
						next_event_ready <= '0';
					end if;
				end if;


		when S_EVENT_COUNT =>
--				event_count <= current_byte; --EVENT COUNT IS AT THE MOMENT NOT USED
				next_state <= S_WAIT_HEADER;

			when others =>
				next_state <= S_WAIT_HEADER;

		end case;
	end process fsm_comb;
	--}}}

	fsm_sync : process (i_byte_clk) --THE SYNCHRONOUS CHANGE OF THE STATE {{{
	begin
		if falling_edge(i_byte_clk) then
			if i_rst = '1' then
				present_state <= S_WAIT_HEADER;
				frame_number <= (others => '0');
			else
				present_state <= next_state;
				frame_number <= next_frame_number;
				event_data_count <= next_event_data_count;
				o_event_ready <= next_event_ready;
			end if;
		end if;

	end process fsm_sync; --}}}


end architecture with_frame_cnt;
