Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;

entity reset_gen is
	port(
			i_chan_rst : in std_logic;
			i_chip_rst : in std_logic;
			i_clk : in std_logic;
			o_stic_rst : out std_logic

	    );
end reset_gen;




architecture di_flipflop of reset_gen is

signal shift_reg : std_logic_vector(1 downto 0);
signal channel_reset : std_logic;


begin


	shifting : process (i_clk)		--sample the chan reset with two flip flops to generate the reset only for one clock period
	begin
		if rising_edge(i_clk) then
			shift_reg <= shift_reg(0) & i_chan_rst;
		end if;
	end process shifting;


	channel_reset <= not shift_reg(1) and shift_reg(0);	--channel_reset only active for one clk period

	o_stic_rst <= channel_reset or i_chip_rst;	--stic_rst active for one channel_reset period or with chip reset


end;

