LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.std_logic_arith.all;

entity deserializer is
	port (
			o_sync_found	: out std_logic;		--DEBUG OUTPUT TO INDICATE A SYNC WAS FOUND
			i_start_val		: in std_logic_vector(3 downto 0);	--RESET VALUE FOR THE BCLOCK GENERATOR
			i_ser_clk		: in std_logic;			--THE SERIAL 160MHZ CLOCK TO OR FROM THE CHIP
			i_rst			: in std_logic;			--GLOBAL RESET SIGNAL
			i_serial_RXD	: in std_logic;			--THE 8B10B SERIAL DATA STREAM
			o_byte_clock 	: out std_logic;		--THE RECOVERED BYTE CLOCK
			o_byte_data		: out std_logic_vector(7 downto 0);	--THE DECODED BYTE DATA
			o_kontrol		: out std_logic			--THE INDICATION WHETHER THE FOUND SIGNAL WAS A KONTROL SIGNAL
		 );
	end entity deserializer;


architecture a  of deserializer is


component dec_8b10b is	--{{{
    port(
		RESET : in std_logic ;	-- Global asynchronous reset (AH) 
		RBYTECLK : in std_logic ;	-- Master synchronous receive byte clock
		AI, BI, CI, DI, EI, II : in std_logic ;
		FI, GI, HI, JI : in std_logic ; -- Encoded input (LS..MS)		
		KO : out std_logic ;	-- Control (K) character indicator (AH)
		HO, GO, FO, EO, DO, CO, BO, AO : out std_logic 	-- Decoded out (MS..LS)
	    );
end component dec_8b10b; --}}}


component bclock_gen is	--{{{
	generic(Nd : Integer := 10);
PORT(
		i_clk 			: IN  STD_LOGIC;
		i_start_val		: in std_logic_vector(3 downto 0);
		i_rst			: in std_logic;
        o_div_clk		: OUT STD_LOGIC
	);
end component bclock_gen;	--}}}



	signal fast_clk : std_logic := '0';
	signal byte_data : std_logic_vector(7 downto 0);
	signal slow_clock : std_logic;
	signal local_data : std_logic_vector(9 downto 0);
	signal local_sampled_data : std_logic_vector(9 downto 0);
	signal rst : std_logic;
	signal s_o_kontrol : std_logic;
	signal sync_found : std_logic; 

	signal no_sync_since_reset : std_logic;
				--HEADER ID 10 BIT 1100001011
	CONSTANT komma_sync : std_logic_vector(9 downto 0) := "0101111100"; --K28.5


	signal err_found : std_logic;
	signal reset_dec : std_logic;

begin

	--RESET THE DECODER IF AN ERRONEOUS CODE WAS FOUND OR IF A BUS RESET OCCURED
	reset_dec <= err_found or i_rst;

	o_sync_found <= sync_found;

	o_kontrol <= s_o_kontrol;
	o_byte_data <= byte_data;
	o_byte_clock <= slow_clock;	--AT SOME POINT I SHOULD GIVE ALL SIGNALS UNIFORM NAMES... 

decoder: dec_8b10b	--{{{
port map(
	RESET => no_sync_since_reset,
	RBYTECLK => slow_clock,
	AI=> local_sampled_data(0),
	BI=> local_sampled_data(1),
	CI=> local_sampled_data(2),
	DI=> local_sampled_data(3),
	EI=> local_sampled_data(4),
	II=> local_sampled_data(5),
	FI=> local_sampled_data(6),
	GI=> local_sampled_data(7),
	HI=> local_sampled_data(8),
	JI=> local_sampled_data(9),
	KO => s_o_kontrol,
	AO => byte_data(0),
	BO => byte_data(1),
	CO => byte_data(2),
	DO => byte_data(3),
	EO => byte_data(4),
	FO => byte_data(5),
	GO => byte_data(6),
	HO => byte_data(7)
); --}}}

byte_clk: bclock_gen --{{{
port map(
		i_clk => i_ser_clk,
		i_start_val => i_start_val,
		i_rst => sync_found,
        o_div_clk => slow_clock
		); --}}}


	--SYNCHRONIZING PROCESSES CAPTURING THE SYNC SIGNAL AND CONTROL SIGNALS

	sample_data : process (i_ser_clk) --SAMPLE THE INCOMING SIGNAL {{{
	begin
		if falling_edge(i_ser_clk) then
			local_data <= i_serial_RXD & local_data(local_data'high downto 1);
		end if;
	end process ; --}}}

	--IF THE LINK IS DEAD OR BROKEN WE HAVE TO RESET THE DECODER --{{{
	check_err : process(i_ser_clk)
	begin
		if rising_edge(i_ser_clk) then
			--6 SUBSEQUENT 1 OR 0 SHOULD NEVER OCCUR
			if (local_data(9 downto 3) = "0000000" or local_data(9 downto 3) = "1111111") then
				err_found <= '1';
			else
				err_found <= '0';
			end if;
		end if;
	end process; --}}}

	--CREATE A SIGNAL THAT INDICATES WHETHER THE CURRENTLY SAMPLED DATA IS A SYNCRONIZATION SIGNAL	0101111100
	find_syn : process (i_ser_clk) --{{{
	begin
		if rising_edge(i_ser_clk) then
			if (local_data(9 downto 0) = "1010000011" or local_data(9 downto 0) = "0101111100") then
				sync_found <= '1';
			else
				sync_found <= '0';
			end if;
		end if;
	end process find_syn; --}}}


	sample_input : process (slow_clock) --SAMPLE THE 10 BIT INPUT STREAM WITH THE SYNCRONIZED BYTE CLOCK {{{
	begin
		if rising_edge(slow_clock) then	
			local_sampled_data <= local_data;
		end if;
	end process sample_input;
--}}}


reset_encoder : process(i_ser_clk) --The 8b10b decoder needs to be reset together with the chip. Until we find a sync signal. {{{
begin
	if rising_edge(i_ser_clk) then
		if reset_dec = '1' then
			no_sync_since_reset <= '1';
		elsif sync_found = '1' then
			no_sync_since_reset <= '0';
		end if;
	end if;
end process; --}}}





end architecture a ;	
