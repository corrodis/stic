Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;

Library UNISIM;
use UNISIM.VCOMPONENTS.all;

entity dcm_chipscope is
	port(
			CLK_IN1		:	in std_logic;
			CLK_OUT1	:	out std_logic;
			RESET		:	in std_logic
--			LOCKED		:	out std_logic;
	    );
end dcm_chipscope;




architecture manual of dcm_chipscope is

	--Input clock coming from PLL does not need to be buffered
--	signal clkin1 : std_logic; --buffer input clock
	signal clkfb : std_logic;	--feedback clock for the dcm
	signal clk0 : std_logic;	--clock0 output for pll lock
	signal clkfx : std_logic;	--synthesized clock frequency
--	signal locked_internal : std_logic;
--	signal status_internal : std_logic_vector(7 downto 0);

begin

--	--input signal buffering {{{
--	clkin_buf : IBUFG
--	port map(
--				I => CLK_IN1,
--				O => clkin1
--			);
--	--}}}


	--DCM INSTANCE {{{
	dcm_inst : DCM_SP
	generic map(
					CLKDV_DIVIDE		=> 2.000,
					CLKFX_DIVIDE		=> 4,
					CLKFX_MULTIPLY		=> 12,
					CLKIN_DIVIDE_BY_2	=> FALSE,
					CLKIN_PERIOD		=> 10.0,
					CLKOUT_PHASE_SHIFT	=> "NONE",
					CLK_FEEDBACK		=> "1X",
					DESKEW_ADJUST		=> "SYSTEM_SYNCHRONOUS",
					PHASE_SHIFT			=> 0,
					STARTUP_WAIT		=> FALSE
			   )
	port map(
					CLKIN                 => CLK_IN1,
					CLKFB                 => clkfb,
					-- Output clocks
					CLK0                  => clk0,
					CLK90                 => open,
					CLK180                => open,
					CLK270                => open,
					CLK2X                 => open,
					CLK2X180              => open,
					CLKFX                 => clkfx,
					CLKFX180              => open,
					CLKDV                 => open,
					-- Ports for dynamic phase shift
					PSCLK                 => '0',
					PSEN                  => '0',
					PSINCDEC              => '0',
					PSDONE                => open,
					-- Other control and status signals
					LOCKED                => open,
					STATUS                => open,
					RST                   => RESET,
					-- Unused pin, tie low
					DSSEN                 => '0'
			);
--}}}

--	LOCKED <= locked_internal;

	--output clock buffering

	clk_feedback : BUFG
	port map(
				I => clk0,
				O => clkfb
			);

	clkout1_buf: BUFG
	port map(
				I => clkfx,
				O => CLK_OUT1
			);



end;
