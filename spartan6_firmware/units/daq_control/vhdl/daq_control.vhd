Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

Library UNISIM;
use UNISIM.vcomponents.all;


use work.stic2_daq_communication.all;

entity daq_control is	--{{{
	port(
			i_sys_clk						: in std_logic; 	--100MHZ USER CLOCK FROM THE BOARD

			led2							: out std_logic;	--GREEN LED ON THE SPARTAN BOARD

			--SYSTEM CONTROL PORTS
			o_pll_enable 					: out std_logic;	--ENABLE OR DISABLE THE 640MHz OSCILLATOR


			--TRIGGER TEST PORTS
			o_test_trigger_p				: out std_logic;
			o_test_trigger_n				: out std_logic;


			--DESERIALIZER PORTS

			i_stic_txd_p					: in std_logic_vector(N_SPI_CLIENTS -1 downto 0);		--SERIAL DATA TRANSMISSION LINES P TERMINAL
			i_stic_txd_n					: in std_logic_vector(N_SPI_CLIENTS -1 downto 0);		--SERIAL DATA TRANSMISSION LINES N TERMINAL

			o_stic_clk_160M_p				: out std_logic;	--160 MHZ DIFFERENTIAL SYSTEM CLOCK FOR THE ASIC
			o_stic_clk_160M_n				: out std_logic;

			o_stic_rst						: out std_logic_vector(3 downto 0);	--STIC RESET SIGNALS


			i_debug_mdi						: in std_logic_vector(0 downto 0);		--DEBUG SPI MASTER DATA IN SIGNAL
			o_debug_cs						: out std_logic_vector(0 downto 0);		--DEBUG SPI CHIP SELECT SIGNAL

			--SPI PORTS TO THE CLIENTS
			o_spi_clk						: out std_logic;
			o_spi_mdo						: out std_logic;
			i_spi_mdi						: in std_logic_vector(N_SPI_CLIENTS/2 -1 downto 0);	--TWO CHIPS PER FEBA WITH COMMON SDO PORT!!!
			o_spi_cs						: out std_logic_vector(N_SPI_CLIENTS -1 downto 0);



			--CYPRESS USB COMPONENT PORTS
			usb_reset_n						: out std_logic;	--RESET OF THE CYPRESS USB CORE

			usb_sloe 						: out std_logic;	--USB_FIFO OUTPUT ENABLE
			usb_slwr 						: out std_logic;	--USB_FIFO WRITE ENABLE
			usb_slrd 						: out std_logic;	--USB_FIFO READ ENABLE
			usb_flaga 						: in std_logic; --EP2 DATA FLAG	--CONFIGURED AS IN ENDPOINT	IF '1' HAS DATA
			usb_flagb 						: in std_logic; --EP4 DATA FLAG
			usb_flagc 						: in std_logic; --EP6 DATA FLAG
			usb_flagd 						: in std_logic; --EP8 DATA FLAG	--CONFIGURED AS OUT ENDPOINT IF '1' IS AVAILABLE
			fifo_addr 						: out std_logic_vector(1 downto 0);	--SELECT FIFO ENDPOINT BUFFER : DEFAULT 00 ONLY 10 WHEN WRITING
			fifo_data_io					: inout std_logic_vector(7 downto 0);	--TRISTATE FIFO DATA TO WRITE OR READ

			usb_fclk						: out std_logic;	--USB FIFO CLOCK (48 MHz)
			usb_clk							: out std_logic		--CYPRESS EXTERNAL CLOCK (24 MHz)


	    );
end daq_control; --}}}




architecture large_fsm of daq_control is

	component dcm_160M is	--{{{
	port(
			CLK_IN1		:	in std_logic;
			CLK_OUT1	:	out std_logic;
			RESET		:	in std_logic
--			LOCKED		:	out std_logic
		);
	end component; --}}}

	component dcm_chipscope is	--{{{
	port(
			CLK_IN1		:	in std_logic;
			CLK_OUT1	:	out std_logic;
			RESET		:	in std_logic
--			LOCKED		:	out std_logic
		);
	end component; --}}}

	component dcm_deser is	--{{{
	port(
			CLK_IN1		:	in std_logic;
			CLK_OUT1	:	out std_logic;
			CLK_OUT2	:	out std_logic;
			RESET		:	in std_logic
--			LOCKED		:	out std_logic
		);
	end component; --}}}

	component spi_control is	--{{{
			generic(
					C_NUM_WORDS : integer := 146;				--THE TOTAL NUMBER OF WORDS TO READ FROM THE MEMORY
					C_FIRST_MSB : integer := 16;				--THE MSB OF THE FIRST WORD TO WRITE
					C_CLK_DIVIDE : integer := 10;				--CLOCK DIVISION FACTOR FOR THE SYSTEM TO SPI CLOCK DIVISION
					C_NUM_CLIENTS	: integer := 1;				--NUMBER OF CLIENTS CONNECTED TO THE SPI INTERFACE
					C_HANDLEID_WIDTH : integer := 1				--WIDTH OF THE HANDLEID FOR MULTIPLEXING OF THE SIGNALS
				);
		port(
					--SYSTEM SIGNALS
					i_clk : in std_logic;						--SYSTEM CLOCK SIGNAL (100 MHz DESIGN)
					i_rst : in std_logic;						--RESET SIGNAL FOR THE SPI COMPONENT


					--CONTROL SIGNALS FROM THE DAQ CONTROL
					i_handleid 	: in std_logic_vector(C_HANDLEID_WIDTH-1 downto 0);	--THE HANDLE ID OF THE CHIP WE WANT TO CONFIGURE
					i_start_trans 	: in std_logic;						--START TRANSMISSION SIGNAL FROM THE DAQ CONTROL
					o_eot 			: out std_logic;					--END OF TRANSMISSION SIGNAL

					--DATA FROM OR TO THE USB INTERFACE
					o_read_addr	: out std_logic_vector(11 downto 0);	--THE MEMORY READ ADDRESS
					i_conf_data	: in std_logic_vector(31 downto 0);	--THE CONFIGURATION DATA FROM THE USB READ MEMORY

					o_return_data 	: out std_logic_vector(31 downto 0);	--THE OLD CONFIGURATION THAT WILL BE WRITTEN TO THE RETURN BUFFER (USB SEND MEMORY)
					o_write_addr 	: out std_logic_vector(11 downto 0);	--THE WRITE ADDRESS FOR THE RETURNED SPI DATA
					o_write_en 		: out std_logic;						--THE WRITE ENABLE FOR THE MEMORY



					--SPI INTERCONNECTION SIGNALS
					o_mdo							: out std_logic; 	--COMMON OUTPUT TO ALL CONNECTED SPI CLIENTS
					o_sclk							: out std_logic;	--COMMON CLOCK TO ALL CONNECTED SPI CLIENTS
					i_mdi							: in std_logic_vector(C_NUM_CLIENTS-1 downto 0);	--INDIVIDUAL SPI SLAVE INPUTS
					o_cs							: out std_logic_vector(C_NUM_CLIENTS-1 downto 0)	--INDIVIDUAL CHIP SELECT SIGNALS
			);
	end component;	--}}}

	component cypress_usb_top is --{{{
		port(
				i_clk_160M						: in std_logic;		--A 160 MHZ CLOCK TO GENERATE THE 48 AND 24 MHZ
				i_rst							: in std_logic;		--RESET SIGNAL OF THE USB CORE


				--USB SLAVE FIFO SIGNALS CONNECTED TO THE EXTERNAL PORTS
				usb_reset_n						: out std_logic;	--RESET OF THE CYPRESS USB CORE

				usb_sloe 						: out std_logic;	--USB_FIFO OUTPUT ENABLE
				usb_slwr 						: out std_logic;	--USB_FIFO WRITE ENABLE
				usb_slrd 						: out std_logic;	--USB_FIFO READ ENABLE
				usb_flaga 						: in std_logic; --EP2 DATA FLAG	--CONFIGURED AS IN ENDPOINT	IF '1' HAS DATA
				usb_flagb 						: in std_logic; --EP4 DATA FLAG
				usb_flagc 						: in std_logic; --EP6 DATA FLAG
				usb_flagd 						: in std_logic; --EP8 DATA FLAG	--CONFIGURED AS OUT ENDPOINT IF '1' IS AVAILABLE
				fifo_addr 						: out std_logic_vector(1 downto 0);	--SELECT FIFO ENDPOINT BUFFER : DEFAULT 00 ONLY 10 WHEN WRITING
				fifo_data_io					: inout std_logic_vector(7 downto 0);	--TRISTATE FIFO DATA TO WRITE OR READ

				usb_fclk						: out std_logic;	--USB FIFO CLOCK (48 MHz)
				usb_clk							: out std_logic;	--CYPRESS EXTERNAL CLOCK (24 MHz)


				--CONNECTION FROM THE CONTROL LOGIC TO THE USB_TRANS ENTITY
				i_usb_start_trans 				: in std_logic;							--SIGNAL TO START THE TRANSMISSION
				i_usb_addr_offset 				: in std_logic_vector(11 downto 0);		--MEMORY START ADDRESS OF THE PACKET
				i_usb_num_packets 				: in std_logic_vector(1 downto 0);		--NUMBER OF PACKETS TO BE TRANSMITTED
				o_usb_packet_ready				: out std_logic;		--INTERRUPT SIGNAL INDICATING A NEW PACKET, ACTIVE FOR ONE CLOCK CYCLE
				o_usb_send_done					: out std_logic;		--INDICATING THAT THE SENDING IS FINISHED

				--CONNECTION FROM THE CONTROL LOGIC TO THE SEND BUFFER
				i_memsend_data					: in std_logic_vector(31 downto 0);	--32 BIT DATA VECTOR TO WRITE IN THE SEND BUFFER
				i_memsend_addr					: in std_logic_vector(11 downto 0);	--THE BIT ADDRESS FOR WRITING THE DATA, THE LAST 2 BITS WILL BE CROPPED TO GET THE BYTE ADDRESS
				i_memsend_wen					: in std_logic;

				--CONNECTION FROM THE CONTROL LOGIC TO THE READ BUFFER
				i_memread_addr					: in std_logic_vector(11 downto 0);
				o_memread_data					: out std_logic_vector(31 downto 0)
			);
	end component cypress_usb_top; --}}}

	component stic2_recv is	--{{{
		port (
					--DEBUGGING OUTPUTS
				o_byte_clk : out std_logic;
				o_sync_found : out std_logic;

				i_start_val : in std_logic_vector(3 downto 0);	--THE RESET VALUE OF THE BYTE CLOCK COUNTER
				i_rst		: in std_logic;		--THE COMMON RESET SIGNAL TO SYNC THE DECODER AND RECEIVER
				--STIC2 DIGITAL INPUTS
				i_stic2_txd : in std_logic;		--THE LVDS SERIAL INPUT FROM STIC2
				i_stic2_clk : in std_logic;		--THE CLOCK SIGNAL OF THE 160 MHz CLOCK
				i_stic2_clk90 : in std_logic;	--A 90 DEGREE PHASESHIFTED COPY OF THE STIC CLOCK USED IN THE DATA RECOVERY

				--MICROBLAZE INPUT
				i_rd_en		: in std_logic;
				i_rd_clk	: in std_logic;
				o_fifo_full : out std_logic;
				o_fifo_empty : out std_logic;
				o_fifo_count : out std_logic_vector(6 downto 0);
				o_fifo_data : out std_logic_vector(55 downto 0) --FIFO DATA TO BE READ BY THE MICROBLAZE

			 );
	end component;	--}}}

	component clk_div is --{{{
	generic (Nd : Integer := 50);
	PORT(
			clk, en : IN  STD_LOGIC;
			sample     : OUT STD_LOGIC);
	end component; --}}}

	component diff_trigger_1V5 is --{{{
		port(
				i_clk : in std_logic; --SYSTEM CLOCK
				o_trigger_clk : out std_logic;
				o_trigger_clk_n : out std_logic
			);
	end component; --}}}


	--SIGNAL DECLARATIONS	--{{{

	--SYSTEM SIGNALS
	signal s_clk_160M : std_logic;
	signal s_clk_160M_inv : std_logic;
	signal s_clk90_160M : std_logic;
	signal s_clk_100M : std_logic;



	-- STATEMACHINE SIGNALS
	type FSM_CONTROL_STATES is (FS_IDLE, FS_RESET, FS_CHECK_HEADER,
								FS_SPI_CONFIG_HEADER, FS_SPI_CONFIG_SEND, --SPI CONFIGURATION STATES
								FS_SPI_DEBUG_HEADER, FS_SPI_DEBUG_SEND, FS_STEP_DEBUG, --SPI DEBUG STATES
								FS_SENDUSB, FS_GETUSB, --USB CONTROL STATES
								FS_DESER_SEL_CHIP, FS_DESER_READ_FIFO, FS_DESER_WRITEUSB_1, FS_DESER_WRITEUSB_2,
								FS_DESER_WRITEUSB_3, FS_CREATE_HEADER, FS_DESER_WRITE_HEADER, FS_DESER_WAIT_CYCLE, FS_DESER_FIFO_DELAY,
								FS_CHIP_RESET, FS_HOLD_RESET,
								FS_LOOPBACK_FIRST, FS_LOOPBACK	--DEBUG STATES
							);

	signal p_state, n_state, s_return_state,n_return_state : FSM_CONTROL_STATES;



	--ALSO HAST TO BE IMPLEMENTED AS FF'S TO WRITE THE HEADER BACK IF REQUIRED
	signal s_uheader, n_uheader : t_usb_header;




	--SIGNALS REQUIRED FOR THE USB COMMUNICATION
	signal s_usb_send_done,s_usb_send_done_direct : std_logic;
	signal s_reset,n_reset : std_logic;


	--FSM FLIPFLOP SIGNALS
	signal s_memsend_addr,n_memsend_addr : std_logic_vector(11 downto 0);
	signal s_memsend_wen,n_memsend_wen : std_logic;
	signal s_memread_addr,n_memread_addr : std_logic_vector(11 downto 0);

	signal s_memsend_data, n_memsend_data : std_logic_vector(31 downto 0);
	signal s_memread_data : std_logic_vector(31 downto 0);

	signal s_usb_packet_ready : std_logic;			--SIGNAL INDICATING THAT THE USB HAS A NEW PACKET, ACTIVE FOR ONE 48MHZ CLOCK CYCLE

	signal s_usb_needs_read : std_logic;			--SIGNAL INDICATING THET THERE ARE UNREAD USB DATA, ACTIVE UNTIL USB IS READ
	signal s_usb_read_ack, n_usb_read_ack : std_logic;				--RESET THE USB NEEDS READ SIGNAL

	signal s_usb_start_trans,n_usb_start_trans : std_logic;


	signal s_usb_addr_offset : std_logic_vector(11 downto 0);
	signal s_usb_num_packets, n_usb_num_packets : std_logic_vector(1 downto 0);


	signal s_word_cnt,n_word_cnt : std_logic_vector(8 downto 0);	--VARIABLE COUNTING THE NUMBER OF WORDS WRITTEN TO THE SEND BUFFER


	signal s_delay_count, n_delay_count : std_logic_vector(7 downto 0);

	signal s_read_count, n_read_count : std_logic_vector(6 downto 0);	--HOW MANY EVENTS TO READ FROM THE FIFO




	--MULTIPLEXED USB SIGNALS
	signal s_usb_wr_data : std_logic_vector(31 downto 0);
	signal s_usb_wr_addr : std_logic_vector(11 downto 0);
	signal s_usb_wr_en : std_logic;
	signal s_usb_rd_addr : std_logic_vector(11 downto 0);


	--SPI DEBUG UNIT SIGNALS
	signal s_debug_write_data : std_logic_vector(31 downto 0);				--RETURNED DATA VECTOR FROM THE DEBUG TRANSMISSION
	signal s_debug_write_addr : std_logic_vector(11 downto 0);				--WRITE ADDRESS FROM THE DEBUG CONFIGURATION UNIT
	signal s_debug_write_en : std_logic;									--WRITE ENABLE FROM THE DEBUG CONFIGURATION UNIT

	signal s_debug_readaddr : std_logic_vector(11 downto 0);				--READ ADDRESS OF THE DEBUG CONFIGURATION UNIT

	signal s_debug_eot : std_logic;											--DEBUG UNIT END OF TRANSMISSION SIGNAL
	signal s_debug_starttrans, n_debug_starttrans : std_logic;				--DEBUG UNIT START TRANSMISSION SIGNAL

	signal s_debug_step : std_logic;											--INDIVIDUAL CLOCK SIGNAL FOR THE DEBUG FSM (SPI CLOCK SIGNAL)
	signal s_en_debug_clk,n_en_debug_clk : std_logic;						--ENABLE FOR THE DEBUG CLOCK SIGNAL
	signal s_debug_clk_cnt, n_debug_clk_cnt : std_logic_vector(7 downto 0);	--TIMEOUT COUNTER FOR THE DEBUG CLOCK STEPPING


	--SPI CONFIGURATION UNIT SIGNALS
	signal s_spi_write_data : std_logic_vector(31 downto 0);				--RETURNED DATA VECTOR FROM THE SPI TRANSMISSION
	signal s_spi_write_addr : std_logic_vector(11 downto 0);				--WRITE ADDRESS FROM THE SPI CONFIGURATION UNIT
	signal s_spi_write_en : std_logic;										--WRITE ENABLE FROM THE SPI CONFIGURATION UNIT

	signal s_spi_readaddr : std_logic_vector(11 downto 0);					--READ ADDRESS OF THE SPI CONFIGURATION UNIT


	--DEBUG UNIT SPI SIGNALS
	signal s_debug_mdo : std_logic;											--SPI CONFIGURATION MDO SIGNAL
	signal s_debug_sclk : std_logic;											--SPI CONFIGURATION SPI CLOCK

	--SPI FSM CONTROL SIGNALS
	signal s_spi_mdo : std_logic;											--SPI CONFIGURATION MDO SIGNAL
	signal s_spi_sclk : std_logic;											--SPI CONFIGURATION SPI CLOCK
	signal s_spi_eot : std_logic;											--SPI END OF TRANSMISSION SIGNAL
	signal s_spi_starttrans, n_spi_starttrans : std_logic;					--START THE TRANSMISSION OF THE NEXT CONFIGURATION

	signal s_spi_mdi : std_logic_vector(N_SPI_CLIENTS-1 downto 0);			--ASSIGN THE CORRECT MDI SIGNALS TO THE SPI CONTROL CLIENT




	--CHIP MULTIPLEXING SIGNALS
	signal s_handle_id : std_logic_vector(1 downto 0);						--REDUCE THE 8 BIT VECTOR FROM THE HEADER TO THE REQUIRED VALUE

	signal s_mux_spi, n_mux_spi : std_logic_vector(1 downto 0);				--MUX THE MEMORY AND SPI SIGNALS BETWEEN LOCAL FSM (00), SPI FSM(01) AND DEBUG FSM (11)



	--DESERIALIZER SIGNALS
	type t_deser_fifo_data is array (N_SPI_CLIENTS-1 downto 0) of std_logic_vector(55 downto 0);
	signal s_deser_data_out : t_deser_fifo_data;

	type t_deser_fifo_count is array (N_SPI_CLIENTS-1 downto 0) of std_logic_vector(6 downto 0);
	signal s_deser_fifo_count : t_deser_fifo_count;

	signal s_deser_fifo_empty : std_logic_vector(N_SPI_CLIENTS-1 downto 0);

	signal s_deser_ren, n_deser_ren : std_logic_vector(N_SPI_CLIENTS -1 downto 0);
	signal s_deser_startval, n_deser_startval : std_logic_vector(3 downto 0);

	signal s_deser_enable, n_deser_enable : std_logic;


	signal s_deser_header, n_deser_header : t_usb_header;		--HEADER OF THE DAQ DATA PACKET	ALSO CONTAINS THE CHIP SELECTION
	signal s_deser_all_empty : std_logic;

	signal s_deser_sel_ID :std_logic_vector(1 downto 0);


	signal s_deser_mask, n_deser_mask : std_logic_vector(N_SPI_CLIENTS-1 downto 0);	--MASK PATTERN FOR DISABLING THE INDIVIDUAL DESERIALIZERS
	signal s_deser_resets : std_logic_vector(N_SPI_CLIENTS-1 downto 0);



	signal s_deser_word1, s_deser_word2 : std_logic_vector(31 downto 0);
	signal n_deser_word1, n_deser_word2 : std_logic_vector(31 downto 0);

	signal s_stic2_txd : std_logic_vector(N_SPI_CLIENTS-1 downto 0);
	signal s_clk_160M_oddr : std_logic;							--ODDR BUFFERED SIGNAL FOR THE SYSTEM CLOCK


	signal s_chip_reset, n_chip_reset : std_logic;

	--CHIPSCOPE SIGNALS
	constant DEBUG : integer := 0;
	signal s_clk_chipscope : std_logic;

	--}}}



begin




	--FAST SAMPLING CLOCK FOR CHIPSCOPE IF DEBUG IS ENABLED {{{

	gen_chipscope: if DEBUG = 1 generate
		u_chipscope_dcm : dcm_chipscope
		port map(
					CLK_IN1	=> s_clk_100M,
					CLK_OUT1 => s_clk_chipscope,
					RESET => '0'
				);
	end generate;

	--}}}

	--FILTER THE 100MHZ SYSTEM CLOCK --{{{
	i_dcm_100 : dcm_160M
	port map(
				CLK_IN1	=> i_sys_clk,
				CLK_OUT1 => s_clk_100M,
				RESET => '0'
			); --}}}

	--SAMPLING CLOCK FOR THE DESERIALIZERS {{{

		u_deser_dcm : dcm_deser
		port map(
					CLK_IN1	=> s_clk_100M,
					CLK_OUT1 => s_clk_160M,
					CLK_OUT2 => s_clk90_160M,
					RESET => '0'
				);

	--}}}

	--DESERIALIZER DIFFERENTIAL IO	--{{{

	u_clk_out : OBUFDS
	generic map(iostandard => "LVDS_33")
	port map(
				O => o_stic_clk_160M_p,
				OB => o_stic_clk_160M_n,
				I => s_clk_160M_oddr
			);



	gen_input_bufds : for i in 0 to N_SPI_CLIENTS-1 generate
		u_lvdstxd_in : IBUFDS
		generic map(iostandard => "LVDS_33")
		port map(
					O => s_stic2_txd(i),
					I => i_stic_txd_p(i),
					IB => i_stic_txd_n(i)
				);
	end generate;


	s_clk_160M_inv <= not s_clk_160M;	--INVERSION TO REMOVE WARNINGS
	ODDR2_stic_clk : ODDR2
	port map(
				Q => s_clk_160M_oddr,
				C0 => s_clk_160M,
				C1 => s_clk_160M_inv,
				D0 => '1',
				D1 => '0'
			);



	--}}}


	--REMEMBER THE LAST USB READ SIGNAL
	p_usb_packets_in : process(s_clk_100M, s_usb_read_ack)--{{{
	begin
		if s_usb_read_ack = '1' then
			s_usb_needs_read <= '0';
		elsif rising_edge(s_clk_100M) then
			if s_usb_packet_ready = '1' then
				s_usb_needs_read <= '1';
			end if;
		end if;
	end process;	--}}}


	o_stic_rst <= (others => s_chip_reset);
	o_pll_enable <= '1';



	--HARDCODED SINCE THERE ARE ONLY 4 CHIPS
	s_deser_all_empty <= s_deser_fifo_empty(0) and s_deser_fifo_empty(1) and s_deser_fifo_empty(2) and s_deser_fifo_empty(3);

--	priority_sel : process(s_deser_fifo_empty)	--{{{
--	begin
--		s_deser_sel_ID <= (others => '0');
--
--		if s_deser_fifo_empty(3) = '0' then
--			s_deser_sel_ID <= "11";
--		end if;
--		if s_deser_fifo_empty(2) = '0' then
--			s_deser_sel_ID <= "10";
--		end if;
--		if s_deser_fifo_empty(1) = '0' then
--			s_deser_sel_ID <= "01";
--		end if;
--		if s_deser_fifo_empty(0) = '0' then
--			s_deser_sel_ID <= "00";
--		end if;
--	end process;--}}}


	--MASK THE INDIVIDUAL DESERIALIZER BY KEEPING THE RESET SIGNAL HIGH
	gen_deser_reset: for i in 0 to N_SPI_CLIENTS-1 generate
		s_deser_resets(i) <= s_reset or s_deser_mask(i);
	end generate;

	s_usb_addr_offset <= (others => '0'); --DEFAULT: NO ADDRESS OFFSET NOT USED HERE


	s_handle_id <= s_uheader.handleID(1 downto 0);


	led2 <= s_deser_fifo_empty(0);	--THAT SHOULD BE THE CORRECT PORT


	--DIFFERENTIAL SIGNAL TRIGGER FOR TEST INPUT TO STIC3 --{{{
	u_dtrigger_1V5 : diff_trigger_1V5
	port map(
				i_clk => s_clk_100M,
				o_trigger_clk => o_test_trigger_p,
				o_trigger_clk_n => o_test_trigger_n
			); --}}}

	u_cypress_usb_top : cypress_usb_top --{{{
	port map(
				i_clk_160M => s_clk_100M,
				i_rst => '0',


				--USB SLAVE FIFO SIGNALS CONNECTED TO THE EXTERNAL PORTS
				usb_reset_n => usb_reset_n,

				usb_sloe  => usb_sloe,
				usb_slwr  => usb_slwr,
				usb_slrd  => usb_slrd,
				usb_flaga  => usb_flaga,
				usb_flagb  => usb_flagb,
				usb_flagc  => usb_flagc,
				usb_flagd  => usb_flagd,
				fifo_addr  => fifo_addr,
				fifo_data_io => fifo_data_io,

				usb_fclk => usb_fclk,
				usb_clk => usb_clk,


				--CONNECTION FROM THE CONTROL LOGIC TO THE USB_TRANS ENTITY
				i_usb_start_trans  => s_usb_start_trans,
				i_usb_addr_offset  => s_usb_addr_offset,
				i_usb_num_packets  => s_usb_num_packets,
				o_usb_packet_ready => s_usb_packet_ready,
				o_usb_send_done => s_usb_send_done_direct,

				--CONNECTION FROM THE CONTROL LOGIC TO THE SEND BUFFER
				i_memsend_data => s_usb_wr_data,
				i_memsend_addr => s_usb_wr_addr,
				i_memsend_wen => s_usb_wr_en,

				--CONNECTION FROM THE CONTROL LOGIC TO THE READ BUFFER
				i_memread_addr => s_usb_rd_addr,
				o_memread_data => s_memread_data

			); --}}}


		gen_spi_mdi_connection: for i in 0 to N_SPI_CLIENTS/2-1 generate
			s_spi_mdi(i*2) <= i_spi_mdi(i);
			s_spi_mdi(i*2+1) <= i_spi_mdi(i);
		end generate;

		u_spi_config : spi_control	--{{{
		generic map(
					C_NUM_WORDS => 146,				--THE TOTAL NUMBER OF WORDS TO READ FROM THE MEMORY
					C_FIRST_MSB => 16,				--THE MSB OF THE FIRST WORD TO WRITE
					C_CLK_DIVIDE => 30,
					C_NUM_CLIENTS => N_SPI_CLIENTS,				--NUMBER OF CLIENTS CONNECTED TO THE SPI INTERFACE
					C_HANDLEID_WIDTH => 2				--WIDTH OF THE HANDLEID FOR MULTIPLEXING OF THE SIGNALS
				)
		port map(
					--SYSTEM SIGNALS
					i_clk => s_clk_100M,
					i_rst => s_reset,


					--CONTROL SIGNALS FROM THE DAQ CONTROL
					i_handleid 	=> s_handle_id,
					i_start_trans => s_spi_starttrans,
					o_eot => s_spi_eot,

					--DATA FROM OR TO THE USB INTERFACE
					o_read_addr	=> s_spi_readaddr,
					i_conf_data	=> s_memread_data,

					o_return_data => s_spi_write_data,
					o_write_addr => s_spi_write_addr,
					o_write_en => s_spi_write_en,


					--SPI INTERCONNECTION SIGNALS
					o_mdo => s_spi_mdo,
					o_sclk => s_spi_sclk,
					i_mdi => s_spi_mdi,
					o_cs => o_spi_cs
			);--}}}

		u_spi_debug : spi_control	--{{{
		generic map(
					C_NUM_WORDS => 3,				--THE TOTAL NUMBER OF WORDS TO READ FROM THE MEMORY
					C_FIRST_MSB => 23,				--THE MSB OF THE FIRST WORD TO WRITE
					C_CLK_DIVIDE => 20,				--DIVIDE ONLY BY 4 FOR THE DEBUGGING MODULE (CONSTRAINED IN STIC3 TO 50 MHz)
					C_NUM_CLIENTS => 1,				--NUMBER OF CLIENTS CONNECTED TO THE SPI INTERFACE
					C_HANDLEID_WIDTH => 1				--WIDTH OF THE HANDLEID FOR MULTIPLEXING OF THE SIGNALS
				)
		port map(
					--SYSTEM SIGNALS
					i_clk => s_clk_100M,
					i_rst => s_reset,


					--CONTROL SIGNALS FROM THE DAQ CONTROL
					i_handleid 	=> (others => '0'),
					i_start_trans => s_debug_starttrans,
					o_eot => s_debug_eot,

					--DATA FROM OR TO THE USB INTERFACE
					o_read_addr	=> s_debug_readaddr,
					i_conf_data	=> s_memread_data,

					o_return_data => s_debug_write_data,
					o_write_addr => s_debug_write_addr,
					o_write_en => s_debug_write_en,


					--SPI INTERCONNECTION SIGNALS
					o_mdo => s_debug_mdo,
					o_sclk => s_debug_sclk,
					i_mdi => i_debug_mdi,
					o_cs => o_debug_cs
			);--}}}

	gen_receivers : for i in 0 to 3 generate --{{{
		u_rcv : stic2_recv
			port map(
						--DEBUGGING OUTPUTS	UNCONNECTED AT THE MOMENT
					o_byte_clk => open,
					o_sync_found => open,

					i_start_val => s_deser_startval,
					i_rst => s_deser_resets(i),
					--STIC2 DIGITAL INPUTS
					i_stic2_txd => s_stic2_txd(i),
					i_stic2_clk => s_clk_160M,
					i_stic2_clk90 => s_clk90_160M,

					--CONTROL LOGIC INPUT
					i_rd_en	=> s_deser_ren(i),
					i_rd_clk => s_clk_100M,
					o_fifo_full => open,					--ONLY REQUIRED FOR DEBUGGING AND ADVANCED CONTROL LOGICS
					o_fifo_count => s_deser_fifo_count(i),
					o_fifo_empty => s_deser_fifo_empty(i),
					o_fifo_data => s_deser_data_out(i)
				 );

	end generate;


--		u_rcv_0 : stic2_recv
--			port map(
--						--DEBUGGING OUTPUTS	UNCONNECTED AT THE MOMENT
--					o_byte_clk => o_byte_clk,
--					o_sync_found => open,
--
--					i_start_val => s_deser_startval,
--					i_rst => s_deser_resets(0),
--					--STIC2 DIGITAL INPUTS
--					i_stic2_txd => s_stic2_txd(0),
--					i_stic2_clk => s_clk_160M,
--					i_stic2_clk90 => s_clk90_160M,
--
--					--CONTROL LOGIC INPUT
--					i_rd_en	=> s_deser_ren(0),
--					i_rd_clk => s_clk_100M,
--					o_fifo_full => open,					--ONLY REQUIRED FOR DEBUGGING AND ADVANCED CONTROL LOGICS
--					o_fifo_empty => s_deser_fifo_empty(0),
--					o_fifo_data => s_deser_data_out(0)
--				 );

	--}}}




	--COMBINATORIAL PART OF THE DAQ FSM --{{{
	comb_fsm : process(p_state, s_return_state,
					s_memread_addr, s_memsend_addr, s_memsend_addr,s_memsend_data, s_memread_data,			--USB SEND/RCV BUFFER SIGNALS
					s_uheader, s_usb_needs_read,s_usb_send_done,s_word_cnt,s_delay_count, s_usb_num_packets,	--USB CONTROL SIGNALS
					s_deser_enable, s_deser_header, s_deser_startval, s_deser_all_empty,	--DESERIALIZER SIGNALS
					s_deser_mask, s_deser_ren, s_deser_data_out,s_deser_word1,
					s_deser_word2, s_deser_fifo_empty,
					s_spi_eot, s_usb_read_ack,s_mux_spi,s_debug_eot,s_debug_clk_cnt							--SPI CONFIGURATION SIGNALS
				)

				variable v_deser_data_vector : std_logic_vector(55 downto 0);
	begin


		--DEFAULT VALUES TO AVOID LATCHES --{{{
		n_state <= p_state;
		n_return_state <= s_return_state;	--THE STORED RETURN STATE FOR WAITING ONE CLOCK CYCLE DURING DESERIALIZER READOUT

		n_chip_reset <= '0';
		n_delay_count <= s_delay_count;



		--SPI SIGNALS
		n_mux_spi <= "00";			--BY DEFAULT SELECT THE SIGNALS FROM THE LOCAL FSM
		n_spi_starttrans <= '0';	--ONLY '1' DURING THE FS_SEND_SPI STATE WHERE THIS VALUE WILL BE OVERWRITTE
		n_debug_starttrans <= '0';

		n_en_debug_clk <= '0';
		n_debug_clk_cnt <= s_debug_clk_cnt;


		--DESERIALIZER SIGNALS
		n_deser_mask <= s_deser_mask;
		n_deser_enable <= s_deser_enable;

		n_deser_header <= s_deser_header;
		n_deser_ren <= (others => '0');					--ONLY READING DURING ONE STATE

		--n_deser_startval <= s_deser_startval;
		n_deser_startval <= X"8";

		--USB MEMORY SIGNALS
		n_memread_addr <= s_memread_addr;

		n_memsend_addr <= s_memsend_addr;
		n_memsend_data <= s_memsend_data;

		n_usb_read_ack <= s_usb_read_ack;

		n_memsend_wen <= '0';

		n_word_cnt <= s_word_cnt;
		n_usb_start_trans <= '0';


		n_read_count <= s_read_count;

		n_uheader <= s_uheader;
		n_usb_num_packets <= s_usb_num_packets;


		--DESERIALIZER STANDARD SIGNALS	--{{{
		v_deser_data_vector := s_deser_data_out(to_integer(unsigned(s_deser_header.handleID)));

		n_deser_word1 <= v_deser_data_vector(31 downto 0);	--ATTACH THE CORRECT DATA TO THE MEMORY
		n_deser_word2 <= X"00" & v_deser_data_vector(55 downto 32);	--ATTACH THE CORRECT DATA TO THE MEMORY
--		n_deser_word3 <= X"0000" & v_deser_data_vector(79 downto 64);	--ATTACH THE CORRECT DATA TO THE MEMORY
		--}}}


		n_reset <= '0';	--ONLY RESET WHEN THE STATE IS FS_RESET

		--}}}

		case p_state is


			when FS_RESET =>
				n_reset <= '1';
				n_state <= FS_IDLE;
				n_usb_read_ack <= '0';
				n_usb_num_packets <= "01";

			when FS_IDLE =>

				n_usb_num_packets <= "01";
				n_usb_read_ack <= '0';
				n_memread_addr <= (others => '0');
				n_memsend_addr <= (others => '0');
				n_word_cnt <= (others => '0');


				--THE HEADER SHOULD BE WRITTEN AT ADDRESS 0
				n_uheader <= to_usb_header(s_memread_data);

--				if s_deser_all_empty = '0' and s_deser_enable = '1' then
--					n_state <= FS_DESER_SEL_CHIP;
--				end if;

				if s_usb_needs_read = '1' then	--THE PACKET TRANSFER TO THE READBUFFER IS COMPLETED -> CHECK THE HEADER
					n_state <= FS_CHECK_HEADER;
				end if;

			when FS_CHECK_HEADER =>	--{{{
				n_usb_read_ack <= '1';

				n_memread_addr <= std_logic_vector(unsigned(s_memread_addr)+4); --INCREMENT THE READ ADDRESS, THE READ VALUE WILL BE VALID IN THE NEXT (!) CLOCK CYCLE

				n_state <= FS_IDLE;	--DEFAULT: RETURN TO IDLE

				case s_uheader.cmd is
					when X"63" => 		--THE COMMAND IS 'c' PUT THE HEADER BACK TO THE SEND BUFFER AND START TRANSFERING THE SPI CONFIGURATION
						n_state <= FS_SPI_CONFIG_HEADER;

					when X"44" => 		--THE COMMAND IS 'D' PUT THE HEADER BACK TO THE SEND BUFFER AND START TRANSFERING THE SPI DEBUG DATA 
						n_state <= FS_SPI_DEBUG_HEADER;

					when X"65" => 		--THE COMMAND IS 'e' ENABLE THE DATA TRANSMISSION TO THE DAQ PC
						n_deser_enable <= '1';

					when X"66" => 		--THE COMMAND IS 'f' FETCH THE DATA FROM THE CHIP SPECIFIED IN HANDLE ID
						n_state <= FS_DESER_SEL_CHIP;

					when X"45" => 		--THE COMMAND IS 'E' DISABLE THE DATA TRANSMISSION TO THE DAQ PC
						n_deser_enable <= '0';

					when X"72" => 		--THE COMMAND IS 'ESC', RESET THE DAQ
						n_state <= FS_CHIP_RESET;

					when X"4D" => 		--THE COMMAND IS 'M', A NEW MASK FOR THE DESERIALIZER IS IN THE HANDLE ID OF THE HEADER
						n_deser_mask <= s_uheader.handleID(3 downto 0);

					when X"1B" =>
						n_state <= FS_RESET;


					--DEBUGGING
					when X"4C" => 		--THE COMMAND IS 'L' SEND THE BUFFER BACK TO THE PC FOR TESTING THE USB CONNECTION
						n_usb_num_packets <= "01"; --WRITE 2 PACKETS
						n_state <= FS_LOOPBACK_FIRST;

					when others => NULL;

				end case;

				--}}}


			when FS_SPI_CONFIG_HEADER =>		--WRITE THE SPI HEADER BACK TO THE BUFFER BEFORE STARTING THE SPI WRITING

				n_memsend_data <= to_word(s_uheader);
				n_memsend_wen <= '1';

				n_state <= FS_SPI_CONFIG_SEND;					--MAYBE WE HAVE TO ADD ANOTHER READ CYCLE HERE TO ENSURE THE USB DATA IS VALID

			when FS_SPI_DEBUG_HEADER =>		--WRITE THE SPI HEADER BACK TO THE BUFFER BEFORE STARTING THE SPI WRITING
				n_memsend_data <= to_word(s_uheader);
				n_memsend_wen <= '1';

				n_state <= FS_SPI_DEBUG_SEND;					--MAYBE WE HAVE TO ADD ANOTHER READ CYCLE HER TO ENSURE THE USB DATA IS VALID


			--SPI SENDING AND READING OF A CONFIGURATION	--{{{

			when FS_SPI_CONFIG_SEND =>

				n_spi_starttrans <= '1';
				n_mux_spi <= "01";			--SET THE MULTIPLEXER FOR THE SPI CLOCK AND MDO TO THE CONFIGURATION UNIT
				n_usb_num_packets <= "01";

				if s_spi_eot = '1' then
					n_state <= FS_SENDUSB;
				end if;



				--}}}

			--SPI SENDING AND READING OF DEBUG INFORMATION	--{{{

			when FS_SPI_DEBUG_SEND =>

				n_debug_clk_cnt <= "10000000";
				n_debug_starttrans <= '1';
				n_mux_spi <= "10";			--SET THE MULTIPLEXER FOR THE SPI CLOCK AND MDO TO THE CONFIGURATION UNIT
				n_usb_num_packets <= "01";

				if s_debug_eot = '1' then
					n_state <= FS_STEP_DEBUG;	--STEP THE DEBUG CLOCK A BIT 
				end if;

			when FS_STEP_DEBUG =>
				n_en_debug_clk <= '1';
				n_debug_clk_cnt <= std_logic_vector(unsigned(s_debug_clk_cnt) -1);
				if s_debug_clk_cnt = "0000000" then
					n_state <= FS_SENDUSB;
				end if;




				--}}}

				--RESETTING THE CHIP	{{{
			when FS_CHIP_RESET =>
				n_chip_reset <= '1';
				n_delay_count <= (others => '0');
				n_state <= FS_HOLD_RESET;


			when FS_HOLD_RESET =>
				n_chip_reset <= '1';
				n_delay_count <= std_logic_vector(unsigned(s_delay_count)+1);	--INCREMENT THE WORD COUNTER
				if s_delay_count = X"ff" then	--128 words have been written, start the transmission
					n_state <= FS_IDLE;
				end if;

				--}}}

				--DESERIALIZER READOUT AND DATA TRANSFER PRIORITY SELECTED AT THE MOMENT	{{{


			when FS_DESER_SEL_CHIP =>

				--ALREADY STORE THE ID OF THE SELECTED CHIP AND PREPARE THE HEADER
				n_deser_header.handleID <= s_uheader.handleID;
				n_deser_header.cmd <= X"64";
				n_deser_header.packet_id <= s_uheader.packet_id; --RETURN THE PACKET ID FOR TRANSMISSION IDENTIFICATION
				n_deser_header.len <= s_deser_fifo_count(to_integer(unsigned(s_uheader.handleID))) & "000";	--LEN OF THE PAYLOAD ONLY
				n_word_cnt <= std_logic_vector(unsigned(s_word_cnt) + 1);				--HEADER COUNTS AS +1
				n_read_count <= s_deser_fifo_count(to_integer(unsigned(s_uheader.handleID)));					--HOW MANY EVENTS TO READ

				n_state <= FS_CREATE_HEADER;



			when FS_DESER_READ_FIFO =>

				n_deser_ren <= (others => '0');		--DESELECT ALL FIFOS
				n_deser_ren(to_integer(unsigned(s_deser_header.handleID))) <= '1';		--READ THE SELECTED FIFO
				n_read_count <= std_logic_vector(unsigned(s_read_count)-1);

				n_state <= FS_DESER_WAIT_CYCLE;
				n_return_state <= FS_DESER_FIFO_DELAY;


			when FS_DESER_FIFO_DELAY =>											--WAIT FOR ONE MORE CLOCK CYCLE TO GET THE FIFO DATA TO s_deser_word1
				n_state <= FS_DESER_WRITEUSB_1;

			when FS_DESER_WRITEUSB_1 =>
				n_memsend_data <= s_deser_word1;	--ATTACH THE CORRECT DATA TO THE MEMORY
				n_memsend_addr <= std_logic_vector(unsigned(s_memsend_addr)+4);
				n_memsend_wen <= '1';

				n_word_cnt <= std_logic_vector(unsigned(s_word_cnt)+2);	--ALREADY COUNT BOTH WORDS THAT WILL BE WRITTEN

				n_state <= FS_DESER_WRITEUSB_2;


			when FS_DESER_WRITEUSB_2 =>
				n_memsend_data <= s_deser_word2;	--ATTACH THE CORRECT DATA TO THE MEMORY
				n_memsend_addr <= std_logic_vector(unsigned(s_memsend_addr)+4);


				n_memsend_wen <= '1';
--				n_word_cnt <= std_logic_vector(unsigned(s_word_cnt)+1); --THIS IS ALREADY ADDED TO STATE WRITEUSB_1


				-- SIMON here its specified to send 126 events!
				-- s_read_count: --HOW MANY EVENTS TO READ (NUMBER OF EVENTS STORED IN THE FIFO), counts down
				if (unsigned(s_word_cnt) = 126) or (s_deser_fifo_empty(to_integer(unsigned(s_deser_header.handleID))) = '1') or s_read_count="0000000" then
					n_state <= FS_SENDUSB;
				else
					n_state <= FS_DESER_READ_FIFO;
				end if;


			when FS_CREATE_HEADER =>
				n_memsend_data <= to_word(s_deser_header);	--PASS THE DESERIALIZER HEADER TO THE MEMSEND BUFFER
				n_memsend_addr <= (others => '0');
				n_state <= FS_DESER_WRITE_HEADER;

			when FS_DESER_WRITE_HEADER =>
				n_memsend_data <= to_word(s_deser_header);	--PASS THE DESERIALIZER HEADER TO THE MEMSEND BUFFER
				n_memsend_addr <= (others => '0');
				n_memsend_wen <= '1';

				if s_deser_fifo_empty(to_integer(unsigned(s_deser_header.handleID))) = '1' then
					n_state <= FS_SENDUSB;
				else
					n_state <= FS_DESER_READ_FIFO;
				end if;



			when FS_DESER_WAIT_CYCLE =>		--GO TO THE RETURN STATE IN THE NEXT CLOCK CYCLE
				n_state <= s_return_state;

				--}}}



			when FS_SENDUSB =>				--TRANSMIT THE USB PACKET AND RETURN WHEN FINISHED
				n_usb_start_trans <= '1';
				if s_usb_send_done = '1' then
					n_state <= FS_IDLE;
				end if;



			--LOOPBACK FOR DEBUGGING THE USB COMMUNICATION
			when FS_LOOPBACK_FIRST =>			--WRITE FIRST WITHOUT INCREMENTING THE ADDRESS
				n_state <= FS_LOOPBACK;
				n_memsend_wen <= '1';
				n_memread_addr <= std_logic_vector(unsigned(s_memread_addr)+4); --INCREMENT THE READ ADDRESS, THE READ VALUE WILL BE VALID IN THE NEXT CLOCK CYCLE
				n_memsend_data <= s_memread_data;			--LOOPBACK FOR THE DATA

			when FS_LOOPBACK =>
				n_chip_reset <= '1';
				n_memsend_wen <= '1';
				n_memsend_addr <= std_logic_vector(unsigned(s_memsend_addr)+4);
				n_memread_addr <= std_logic_vector(unsigned(s_memread_addr)+4);	--KEEP THE READ ADDRESS AHEAD OF THE WRITE ADDRESS

				n_memsend_data <= s_memread_data;			--LOOPBACK FOR THE DATA

				n_word_cnt <= std_logic_vector(unsigned(s_word_cnt)+1);
				if s_word_cnt = "001111111" then	--256 words have been written, start the transmission
					n_memsend_wen <= '0';		--ALREADY STOP HERE TO AVOID WRITING THE LAST VALUE TWICE
					n_state <= FS_SENDUSB;
				end if;




			when others => n_state <= FS_RESET;
		end case;


	end process;
	--}}}

	--SYNCHRONOUS PART OF THE STATE MACHINE {{{
	sync_fsm : process(s_clk_100M)
	begin
		if rising_edge(s_clk_100M) then
			p_state <= n_state;
			s_return_state <= n_return_state;

			--USB SEND/RCV BUFFER SIGNALS
			s_memsend_addr <= n_memsend_addr;
			s_memsend_data <= n_memsend_data;
			s_memsend_wen <= n_memsend_wen;

			s_memread_addr <= n_memread_addr;

			s_word_cnt <= n_word_cnt;

			s_read_count <= n_read_count;

			s_usb_start_trans <= n_usb_start_trans;
			s_usb_send_done <= s_usb_send_done_direct;

			s_uheader <= n_uheader;
			s_usb_num_packets <= n_usb_num_packets;

			s_usb_read_ack <= n_usb_read_ack;


			--SPI CONTROL SIGNALS
			s_spi_starttrans <= n_spi_starttrans;
			s_debug_starttrans <= n_debug_starttrans;
			s_mux_spi <= n_mux_spi;

			s_en_debug_clk <= n_en_debug_clk;
			s_debug_clk_cnt <= n_debug_clk_cnt;


			--DESERIALIZER CONTROL SIGNALS
			s_deser_enable <= n_deser_enable;
			s_deser_mask <= n_deser_mask;
			s_deser_header <= n_deser_header;
			s_deser_ren <= n_deser_ren;
			s_deser_startval <= n_deser_startval;

			s_deser_word1 <= n_deser_word1;
			s_deser_word2 <= n_deser_word2;
	--		s_deser_word3 <= n_deser_word3;



			s_chip_reset <= n_chip_reset;
			s_delay_count <= n_delay_count;

			s_reset <= n_reset;
		end if;

	end process;	--}}}



	--MULTIPLEXING OF THE MEMORY AND SPI SIGNALS BETWEEN THE INDIVIDUAL MODULES	--{{{

	--MULTIPLEXING OF THE SPI BUS SIGNALS
	o_spi_clk <= s_spi_sclk when s_mux_spi = "01" else
				 s_debug_sclk when s_mux_spi = "10" else
				 s_debug_step;
	o_spi_mdo <= s_spi_mdo when s_mux_spi = "01" else
				 s_debug_mdo when s_mux_spi = "10" else
				 '0';




	--USB READ AND WRITE ADDRESS AND WRITE ENABLE
	s_usb_wr_addr <= s_spi_write_addr when s_mux_spi = "01" else
					 s_debug_write_addr when s_mux_spi = "10" else
					 s_memsend_addr when s_mux_spi = "00" else
					 (others => '0');

	s_usb_wr_data<=	 s_spi_write_data when s_mux_spi = "01" else
					 s_debug_write_data when s_mux_spi = "10" else
					 s_memsend_data when s_mux_spi = "00" else
					 (others => '0');

	s_usb_wr_en <= 	s_spi_write_en when s_mux_spi = "01" else
					s_debug_write_en when s_mux_spi = "10" else
					s_memsend_wen when s_mux_spi = "00" else
					'0';

	s_usb_rd_addr <= s_spi_readaddr when s_mux_spi = "01" else
					 s_debug_readaddr when s_mux_spi = "10" else
					 s_memread_addr when s_mux_spi = "00" else
					 (others => '0');


	--}}}


--DEBUG SPI CLOCK REQUIRED FOR THE STATEMACHINE
--THIS CLOCK DIVIDER PROVIDES AN ADDITIONAL SPI CLOCK THAT CAN BE STEPPED INDEPENDENTLY FROM THE SPI UNIT, ALLOWING TO RUN THE DEBUG FSM
	g_sam1: clk_div --{{{
	generic map(Nd => 20)
	port map(
		clk => s_clk_100M,
		en => s_en_debug_clk,
		sample => s_debug_step 
	);
	--}}}

end;
