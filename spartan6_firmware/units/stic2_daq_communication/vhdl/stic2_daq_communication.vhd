Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;


package stic2_daq_communication is


	--NUMBER OF SPI CLIENTS CONNECTED TO THE DESIGN
	constant N_SPI_CLIENTS : integer := 4;


	--ONLY THE HEADER IS DEFINED THE PAYLOAD WILL BE HANDLED ACCORDING TO THE DESTINATION
	type t_usb_header is
		record
			cmd : std_logic_vector(7 downto 0);	--COMMAND WORD
			handleID : std_logic_vector(7 downto 0);	--THE HANDLE ID IDENTIFYING AN ASIC
			packet_id : std_logic_vector(5 downto 0);	--THE ID OF THE PACKET ALLOWING TO IDENTIFY RESPONSES TO A QUERY
			len : std_logic_vector(9 downto 0);			--THE LEN OF THE PACKET	UP TO 1024 BYTES (2 USB PACKETS)
		end record;



	function to_usb_header(word : std_logic_vector(31 downto 0)) return t_usb_header;
	function to_word(head : t_usb_header) return std_logic_vector;

	function swap_endian(word : std_logic_vector(31 downto 0)) return std_logic_vector;

end;	--package






package body stic2_daq_communication is

	function to_usb_header(word : std_logic_vector(31 downto 0)) return t_usb_header is
		variable u_header : t_usb_header;
	begin
		u_header.cmd := word(7 downto 0);
		u_header.handleID := word(15 downto 8);
		u_header.packet_id := word(21 downto 16);
		u_header.len := word(31 downto 22);

		return u_header;
	end;


	function to_word(head : t_usb_header) return std_logic_vector is
		variable word : std_logic_vector(31 downto 0);
	begin
		word(7 downto 0) := head.cmd;
		word(15 downto 8) := head.handleID;
		word(21 downto 16) := head.packet_id;
		word(31 downto 22) := head.len;

		return word;
	end;


	function swap_endian(word : std_logic_vector(31 downto 0)) return std_logic_vector is
		variable swapped : std_logic_vector(31 downto 0);
	begin

		swapped(7 downto 0) := word(31 downto 24);
		swapped(15 downto 8) := word(23 downto 16);
		swapped(23 downto 16) := word(15 downto 8);
		swapped(31 downto 24) := word(7 downto 0);

		return swapped;



	end;


end package body;
