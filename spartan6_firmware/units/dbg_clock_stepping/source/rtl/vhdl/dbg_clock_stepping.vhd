--UNIT GENERATING A SINGLE RISING EDGE 
--MAYBE NOT NEEDED SINCE THIS CAN ALSO BE DONE IN A STATE OF A FSM

Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity dbg_clock_stepping is
	port(
			i_clk : std_logic;
			i_step : std_logic;
			o_stic_clk : std_logic
	    );
end dbg_clock_stepping;




architecture behaviour of dbg_clock_stepping is

	signal shift_reg : std_logic_vector(1 downto 0);

begin

	shift : process (i_clk)
	begin
		if rising_edge(i_clk) then
			shift_reg <= shift_reg(0) & i_step;
		end if;
	end process shift;

	o_stic_clk <= (not shift_reg(1)) and shift_reg(0);

end;
