#!/bin/bash

#IP: arp 
# 53/tcp    open  domain
# 49152/tcp open  unknown
# 49154/tcp open  unknown
IP=10.42.0.48
LIMIT=30

echo "switch power on"
./wemo.sh $IP ON
while [[ 1 -eq 1 ]]; do 
	date | tee -a ./temperature_trace.log
	TMP=$(./temp_read)
	echo $TMP | tee -a ./temperature_trace.log;
	if [ $(bc <<< "$TMP > $LIMIT") == 1 ]; then
	    if [ "$(./wemo.sh $IP GETSTATE)" == "OFF" ]; then
		echo "power off"
	    else
	    	echo "switch power off!"
	    	./wemo.sh $IP OFF
	    fi
		
	fi
	sleep 5
done;
