#ifndef __TDS18S20_H__
#define __TDS18S20_H__


#include<termios.h>
#include<fcntl.h>

#include<stdio.h>
#include<string.h>
#include<fcntl.h>
#include<errno.h>
#include<unistd.h>
#include<termios.h>
#include<stdlib.h>
#include<iostream>



class TDS18S20 {

private:

	int fd;
	//device status&configuration
	int status;
	struct termios old_tio,options;
	fd_set readfs;

	int isReady;


	/**read data from the interface.
	returns:
		-2 if reading failed,
		0  else.
	*/
	int read_reply(int len, char* reply);

	int send_cmd(int len, char* data);

	int read_scratchpad();


public:


	//Constructor and Destructor
	
	
	TDS18S20(const char* dev_name);
	~TDS18S20();

	unsigned char scratchpad[10];

	double read_temperature();

};




#endif
