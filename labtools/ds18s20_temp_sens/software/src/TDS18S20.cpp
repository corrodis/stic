#include "TDS18S20.h"
#include <unistd.h>
#include <stdio.h>


//#define DEBUG

TDS18S20::TDS18S20(const char *devname)
{
    char deviceName[255];


    if (!devname){

      strcpy(deviceName,"/dev/ttyUSB0");
      printf("if open\n");
    }
    else 
	strcpy(deviceName,devname);


    //try to open device

#ifdef DEBUG
    printf("Trying to open device \"%s\"\n",deviceName);
#endif
    fd = open(deviceName,O_RDWR | O_NOCTTY,0) ;
    if(fd<0)
    {
        perror("Open");
        return;
    }
    isReady=1;

//    fcntl(fd, F_SETFL,0);//FNDELAY);

    tcgetattr(fd,&old_tio);
    tcgetattr(fd,&options);

    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);

    options.c_cflag |= (CLOCAL | CREAD);

    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;
    options.c_cflag &= ~CRTSCTS;

    options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON | IXOFF);
    options.c_oflag &= ~(OCRNL | ONLCR | ONLRET | ONOCR | OFILL | OLCUC | OPOST);
    options.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

    tcsetattr(fd,TCSAFLUSH,&options);


    FD_ZERO(&readfs);
    FD_SET(fd,&readfs);
    
#ifdef DEBUG
    printf("Device %s opened\n",deviceName);
#endif

    return;
}

TDS18S20::~TDS18S20()
{
	close(fd);
}


int TDS18S20::send_cmd( int len, char* data)
{
	int status;
	status=write(fd,data,len);
	if(status==-1)
	{
		printf("Error while sending command\n");
		return -2;
	}

	return status;

}


int TDS18S20::read_reply( int len, char* reply)
{
	int status;
	status=read(fd,reply,len);
	if (status < len)
	{
		printf("Error reading all bytes from Device. Status=%d\n",status);
		return -2;
		perror("Read:");
	}
	return status;

}


int TDS18S20::read_scratchpad()
{

	char reply[2];
	char cmd[2];
	

	int i;
	for(i=0;i<10;i++)
	{
		//SELECT ROM
		cmd[0]=0x72;
		cmd[1]=0x00;
		send_cmd(2,cmd);
		usleep(20000);
#ifdef DEBUG
		printf("Reading UART reply\n");
#endif
		read_reply(2,reply);
#ifdef DEBUG
		printf("UART replied\n");
#endif
		
		if( (0xFF & (short)reply[0]) == 0xDA ){
		  scratchpad[i]=reply[1];
		}
		else printf("Incorrect reply: %02X\n",0xff&reply[0]);

		usleep(10000);
	}

	return 0;
}

//VERY SIMPLE READ OF ONLY ONE TEMPERATURE SENSOR!!
double TDS18S20::read_temperature()
{
#ifdef DEBUG
	printf("Reading temperature\n");
#endif
	char cmd[2];

	double temp_c, highprecision;

	if (isReady == 0){
	       	printf("Error: the serial interface is not open!\n");
		return -274;
	};

#ifdef DEBUG
	printf("1-wire reset\n");
#endif
	cmd[0]='i';	//initialisation
	send_cmd(2,cmd);
	usleep(10000);
	
	//SELECT ROM
	cmd[0]='w';
	cmd[1]=0xCC;
	send_cmd(2,cmd);
	usleep(10000);

	cmd[0]='w';
	cmd[1]=0x44;
	send_cmd(2,cmd);
	usleep(800000);

#ifdef DEBUG
	printf("1-wire reset\n");
#endif
	cmd[0]='i';	//initialisation
	send_cmd(2,cmd);
	usleep(10000);

	//SELECT ROM
	cmd[0]='w';
	cmd[1]=0xCC;
	send_cmd(2,cmd);
	usleep(10000);

	cmd[0]='w';
	cmd[1]=0xBE;
	send_cmd(2,cmd);
	usleep(10000);

#ifdef DEBUG
	printf("Reading Scratchpad\n");
#endif

	read_scratchpad();
#ifdef DEBUG
	printf("Scratchpad returned: ");
	for(int j=0;j<10;j++) printf("%02X ",scratchpad[j]);
	printf("\n");
#endif


	//CALCULATE TEMPERATURE (calculation taken from digitemp code)

	if( (0xff& scratchpad[1]) ==0)
	{
		temp_c = (int) scratchpad[0] >> 1;

	} else {
		temp_c = -1 * (int) (0x100-scratchpad[0]) >> 1;
	}
#ifdef DEBUG
	printf("Low precision temperature: %f\n", temp_c);
#endif
	temp_c -= 0.25;
	highprecision = (int)scratchpad[7]-(int)scratchpad[6];
	highprecision = highprecision/(int)scratchpad[7];

	temp_c = temp_c + highprecision;

	return temp_c;

}
