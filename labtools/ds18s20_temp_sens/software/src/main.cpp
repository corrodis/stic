#include "TDS18S20.h"


int main()
{
	double temperature;
	TDS18S20 *temp_sens = new TDS18S20("/dev/ttyUSB0");

	temperature = temp_sens->read_temperature();

	printf("%f\n", temperature);
	return 0;

}
