Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity onewire_rw is
	port(

				i_sys_clk : in std_logic;		--1MHz clock
				i_rst : in std_logic;			--system reset signal

				--control signals

				i_ow_reset : in std_logic;			--initiate a one wire reset with presence detection
				o_presence_detect : out std_logic;	--one wire presence detected after reset


				i_start : in std_logic;			--start the read or write on the bus
				o_busy : out std_logic;			--transmission finished

				i_write_nread : in std_logic;	--write, not read : 1 to write a bit on the 1-wire bus, 0 to read a bit from the bus

				i_write_bit : in std_logic;		--bit to write on the bus
				o_read_bit : out std_logic;		--read bit on the bus

				--onewire bus signal
				io_ow_bus : inout std_logic		--bus signal

	    );
end onewire_rw;




architecture simple of onewire_rw is

	signal s_ow_oe : std_logic;	--TRISTATE output enable signal
	signal n_ow_oe : std_logic;	--TRISTATE output enable signal

	signal s_ow_bus_read : std_logic;		--1-wire bus input signal
	signal n_ow_bus_read : std_logic;		--1-wire bus input signal

	signal s_ow_input : std_logic;

	--THESE ARE NOT NEEDED SINCE THE MASTER ONLY CHOOSES TO PULL THE BUS LOW OR RELEASE IT
--	signal s_ow_bus_write : std_logic;	--1-wire bus output signal
--	signal n_ow_bus_write : std_logic;	--1-wire bus output signal next state


	signal s_time_count : std_logic_vector(8 downto 0);
	signal n_time_count : std_logic_vector(8 downto 0);


	signal s_presence_detect : std_logic;
	signal n_presence_detect : std_logic;

	--STATE MACHINE STATES
	type fsm_states is (FS_IDLE, FS_OW_RESET, FS_OW_PRESENCE, FS_PULL_LOW, FS_WRITE_0, FS_WRITE_1, FS_INIT_READ, FS_READ_SAMPLE, FS_WAIT_64);
	signal p_state, n_state : fsm_states;

begin


	io_ow_bus <= '0' when s_ow_oe = '1' else 'Z';

	s_ow_input <= io_ow_bus when s_ow_oe = '0' else '1';
	o_presence_detect <= s_presence_detect;

	o_read_bit <= s_ow_bus_read;


	comb_fsm : process (p_state, i_write_bit, i_write_nread, i_start, io_ow_bus, s_time_count, i_ow_reset,s_ow_bus_read, s_presence_detect,s_ow_oe, s_ow_input) --{{{
	begin
		--DEFAULT ASSIGNMENTS
		n_state <= p_state;
		n_ow_bus_read <= s_ow_bus_read;

		o_busy <= '1';		--ALWAYS BUSY EXCEPT WHEN IDLE
		n_time_count <= s_time_count;
		n_presence_detect <= s_presence_detect;

		n_ow_oe <= s_ow_oe;

		case p_state is


			when FS_IDLE =>
				o_busy <= '0'; --ONLY IN THIS STATE WE ARE NOT BUSY

				n_ow_oe <= '0';

				n_time_count <= (others => '0');
				if i_start = '1' then
					n_state <= FS_PULL_LOW;
				end if;

				if i_ow_reset = '1' then
					n_state <= FS_OW_RESET;
				end if;



			when FS_PULL_LOW =>
				n_time_count <= std_logic_vector(unsigned(s_time_count)+1);
				n_ow_oe <= '1';

				--INITIATE A READ SLOT AND SAMPLE THE VALUE FROM THE 1-WIRE SLAVE
				if i_write_nread = '0' then
					n_state <= FS_READ_SAMPLE;
				end if;

				--GO TO THE APPROPRIATE WRITE STATE
				if i_write_nread = '1' and i_write_bit = '0' then
					n_state <= FS_WRITE_0;
				end if;
				if i_write_nread = '1' and i_write_bit = '1' then
					n_state <= FS_WRITE_1;
				end if;

				--WRITING A BIT TO THE SLAVE --{{{
			when FS_WRITE_0 =>
				n_time_count <= std_logic_vector(unsigned(s_time_count)+1);

				if s_time_count = "00111110" then
					n_ow_oe <= '0';
				end if;
				if s_time_count = "00111111" then
					n_ow_oe <= '0';
					n_state <= FS_IDLE;
				end if;

			when FS_WRITE_1 =>
				n_time_count <= std_logic_vector(unsigned(s_time_count)+1);

				--RELEASE THE BUS AFTER 4 us
				if s_time_count = "000000100" then
					n_ow_oe <= '0';
				end if;

				if s_time_count = "00111111" then
					n_state <= FS_IDLE;
				end if;
				--}}}


				--READ A BIT FROM THE SLAVE --{{{
			when FS_READ_SAMPLE =>
				--RELEASE THE BUS AGAIN
				n_ow_oe <= '0';
				n_time_count <= std_logic_vector(unsigned(s_time_count)+1);

				--SAMPLE THE 1-WIRE BUS
				if s_time_count = "000000111" then
					n_ow_bus_read <= s_ow_input;
				end if;

				if s_time_count = "00111111" then
					n_state <= FS_IDLE;
				end if;
				--}}}


			when FS_OW_RESET =>
				n_ow_oe <= '1';
				n_time_count <= std_logic_vector(unsigned(s_time_count)+1);

				if s_time_count = "111111111" then
					n_state <= FS_WAIT_64;
					n_time_count <= (others => '0');
				end if;

			--WAIT FOR 30 US TO DETECT THE PRESENCE PULSE
			when FS_WAIT_64 =>
				n_ow_oe <= '0';
				n_time_count <= std_logic_vector(unsigned(s_time_count)+1);
				if s_time_count = "001000000" then
					n_state <= FS_OW_PRESENCE;
				end if;


			when FS_OW_PRESENCE =>
				n_ow_oe <= '0';
				n_presence_detect <= not io_ow_bus;

				n_state <= FS_IDLE;

			when others => null;
		end case;

	end process comb_fsm; --}}}




	fsm_sync : process (i_sys_clk)
	begin
		if rising_edge(i_sys_clk) then

			if i_rst = '1' then
				p_state <= FS_IDLE;
				s_presence_detect <= '0';
				s_time_count <= (others => '0');
				s_ow_oe <= '0';
			else

				p_state <= n_state;
				s_time_count <= n_time_count;
				s_presence_detect <= n_presence_detect;
				s_ow_oe <= n_ow_oe;
				s_ow_bus_read <= n_ow_bus_read;

			end if;

		end if;
	end process fsm_sync;

end;
