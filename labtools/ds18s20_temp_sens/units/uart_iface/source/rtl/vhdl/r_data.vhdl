Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;

entity r_data is
port(
	data : out std_logic_vector(7 downto 0);
	clk : in std_logic;
	rst_n: in std_logic;
	d_ready : out std_logic;
	RX_BIT : in std_logic
);
end r_data;


architecture a of r_data is 


------- SIGNAL DEFINITIONS

signal d_i     : std_logic_vector(8 downto 0); --8 bits + stop bit
signal eot     : std_logic; 
signal bit_counter : std_logic_vector(3 downto 0);
signal next_bit_counter : std_logic_vector(3 downto 0);


type r_state is (AWAIT,RCV,CHECK_END,STOP);	--define the states of the statemachine
signal present_st, next_st : r_state;

signal start : std_logic := '0';
signal sample : std_logic := '0';


component clock_div is
generic (Nd : Integer := 50);
PORT(
        clk, en : IN  STD_LOGIC;
        sample     : OUT STD_LOGIC);
end component;



------- BEHAVIOUR OF ARCHITECTURE

begin

d_ready <= eot;
data <= d_i(7 downto 0);


g_sam1: clock_div --{{{
generic map(Nd => 868)
port map(
	clk => clk,
	en => start,
	sample => sample
); --}}}

process(sample,rst_n) --{{{
begin
	if rst_n = '0' then
		d_i <= (others => '0');
		next_bit_counter <= "1010";
	elsif rising_edge(sample) then
		d_i <= RX_BIT & d_i(d_i'high downto 1);
		next_bit_counter <= conv_std_logic_vector(conv_integer(unsigned(bit_counter))-1,4);
	end if;
end process; --}}}

process(present_st, RX_BIT, bit_counter,d_i) --{{{
begin
	eot <= '0';
	start <= '0';
	next_st <= present_st;
	case present_st is
		when AWAIT => 
			eot <= '0';
			if RX_BIT = '0' then
				next_st <= RCV;
			else
				next_st <= AWAIT;
			end if;
		when RCV => 
			start <= '1';
			if bit_counter = "0001" then
				next_st <= CHECK_END;
			end if;

		when CHECK_END =>
			start <= '1';
			if bit_counter = "0000" and d_i(d_i'high)='1' then
				next_st <= STOP;
			end if;

		when STOP => 
			eot <= '1';
		when others => 
			eot <= '0';
			start <= '0';
			next_st <= AWAIT;
	end case;
end process; --}}}

process(clk,rst_n) --{{{
begin
	if rst_n = '0' then
		present_st <= AWAIT;
		bit_counter <= "1001";
	elsif rising_edge(clk) then
		present_st <= next_st;
		bit_counter <= next_bit_counter;
	end if;
end process; --}}}

end;
