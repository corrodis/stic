--	Send a byte over the serial bus to the pc
--
--
Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity s_data is --{{{
port(
	data : in std_logic_vector(7 downto 0);
	clk : in std_logic;
	rst_n: in std_logic;
	send : in std_logic;
	d_ready : out std_logic;
	TX_BIT : out std_logic
);
end s_data;
--}}}

architecture a of s_data is

------- SIGNAL DEFINITIONS --{{{

signal s_di, n_di     : std_logic_vector(7 downto 0);
signal busy     : std_logic;


type fsm_state is (FS_IDLE, FS_NEXT_DATA, FS_WAIT, FS_STARTBIT, FS_STOPBIT);	--define the states of the statemachine
signal p_state, n_state : fsm_state;

--TIME COUNTERS
signal s_timecount : std_logic_vector(9 downto 0);
signal n_timecount : std_logic_vector(9 downto 0);

signal s_bitcount : std_logic_vector(4 downto 0);
signal n_bitcount : std_logic_vector(4 downto 0);

--FSM SIGNAL FOR THE UART OUTPUT BIT CURRENTLY ON THE LINE
signal n_tx_bit : std_logic;
signal s_tx_bit : std_logic;

--THE TIME TO GET THE CORRECT BAUD RATE
--867 with 100 MHz
constant s_hold_time : std_logic_vector(9 downto 0) := "1101100100";

--}}}


------- BEHAVIOUR OF ARCHITECTURE --{{{

begin

	--ASSIGN THE OUTPUT
	TX_BIT <= s_tx_bit;
	d_ready <= busy;

	p_comb : process (p_state, s_di, s_timecount, send, data, s_tx_bit, s_bitcount)
	begin

		--DEFAULT SIGNAL ASSIGNMENTS
		n_state <= p_state;
		n_di <= s_di;
		n_timecount <= s_timecount;
		busy <= '1';
		n_tx_bit <= '1';	--KEEP IT HIGH AS DEFAULT
		n_bitcount <= s_bitcount;


		case p_state is
			when FS_IDLE =>
				busy <= '0';
				n_timecount <= (others => '0');
				n_bitcount <= (others => '0');
				--Get the data from the input
				n_di <= data;

				if send  = '1' then
					n_state <= FS_STARTBIT;
				end if;

			when FS_STARTBIT =>	--PULL THE LINE LOW FOR A SHORT TIME
				n_tx_bit <= '0';
				n_state <= FS_WAIT;

			when FS_WAIT =>
				n_timecount <= std_logic_vector(unsigned(s_timecount)+1);
				--KEEP THE CURRENT BIT
				n_tx_bit <= s_tx_bit;
				if s_timecount = s_hold_time then
					n_state <= FS_NEXT_DATA;
				end if;

			when FS_NEXT_DATA =>
				n_tx_bit <= s_di(0);

				n_timecount <= (others => '0');
				n_bitcount <= std_logic_vector(unsigned(s_bitcount)+1);

				--SHIFT THE BITS THROUGH
				n_di <=  s_di(0) & s_di(7 downto 1);

				n_state <= FS_WAIT;

				if s_bitcount = "1000" then
					n_state <= FS_STOPBIT;
				end if;

			when FS_STOPBIT =>
				n_timecount <= std_logic_vector(unsigned(s_timecount)+1);
				n_tx_bit <= '1';
				--KEEP THE CURRENT BIT

				if s_timecount = "1111111111" then
					n_state <= FS_IDLE;
				end if;

			when others => n_state <= FS_IDLE;
		end case;


	end process p_comb;


	p_sync : process (clk)
	begin
		if rising_edge(clk) then
			if rst_n = '0' then
				p_state <= FS_IDLE;
			else
				p_state <= n_state;
				s_bitcount <= n_bitcount;
				s_timecount <= n_timecount;
				s_di <= n_di;

				s_tx_bit <= n_tx_bit;

			end if;
		end if;
	end process p_sync;


end; --}}}

