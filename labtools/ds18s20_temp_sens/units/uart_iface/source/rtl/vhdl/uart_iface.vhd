-- Entity handling the serial communication between the PC and the
-- debugging FSM
--
-- Reassambles the incoming 8 bit serial data to a debugging command packet
-- Transmits the returned 80 bit debug data in 10 8 bit packets


Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity uart_iface is
	port(
			i_clk : in std_logic;	--SYSTEM CLOCK
			i_rst : in std_logic;	--SYSTEM RESET ACTIVE HIGH
			i_uart_rx : in std_logic;	--UART RX SIGNAL
			o_uart_tx : out std_logic;	--UART TX SIGNAL

			i_tx_data : in std_logic_vector(15 downto 0); --INPUT DATA TO SEND TO THE UART
			o_rx_data : out std_logic_vector(15 downto 0);	--RECEIVED DATA FROM THE UART

			i_start_trans : in std_logic;	--SIGNAL TO START THE DATA TRANMISSION
			o_send_done : out std_logic;	--DATA TRANSMISSION FINISHED

			i_rdata_ack : in std_logic;		--ACK THAT DATA HAS BEEN RECEIVED BY THE DEBUG FSM
			o_rdata_ready : out std_logic	--NEW REASSEMBLED DATA FROM THE UART IS READY
	    );
end uart_iface;




architecture behaviour of uart_iface is


	component s_data is --{{{
	port(
		data : in std_logic_vector(7 downto 0);
		clk : in std_logic;
		rst_n: in std_logic;
		send : in std_logic;
		d_ready : out std_logic;
		TX_BIT : out std_logic
	);
	end component; --}}}

	component r_data is --{{{
	port(
		data : out std_logic_vector(7 downto 0);
		clk : in std_logic;
		rst_n: in std_logic;
		d_ready : out std_logic;
		RX_BIT : in std_logic
	);
	end component; --}}}



	type read_states is (FS_IDLE, FS_WAIT_RX,FS_DELAY, FS_ASSEMBLE, FS_COMPLETE);
	signal p_state_read, n_state_read : read_states;

	type send_states is (FS_IDLE, FS_FRAGMENT_SEND, FS_DELAY, FS_WAIT_SEND, FS_COMPLETE);
	signal p_state_send, n_state_send : send_states;




	--UART READ SIGNALS

	--COMPONENT SIGNALS
	signal s_rd_ready : std_logic;
	signal s_rd_data : std_logic_vector(7 downto 0);

	--REQUIRED FSM SIGNALS
	signal s_rd_wcnt, n_rd_wcnt : std_logic_vector(3 downto 0);	--COUNTER FOR THE NUMBER OF WORDS READ FROM UART
	signal s_read_vector,n_read_vector : std_logic_vector(15 downto 0);	--FF's for the reassambled read debug data
	signal s_rd_rst_n, n_rd_rst_n : std_logic;	--FF's for the read reset signal
	signal n_rdata_ready, s_rdata_ready : std_logic; --FF's for the vector data ready signal


	--UART SEND SIGNALS

	signal s_wr_wcnt, n_wr_wcnt : std_logic_vector(3 downto 0);	--COUNTER FOR THE NUMBER OF WORDS SEND TO UART
	signal s_wr_data, n_wr_data : std_logic_vector(7 downto 0);	--THE BYTE DATA TO SEND TO THE PC
	signal s_wr_start, n_wr_start : std_logic;					--START OF THE DATA TRANSMISSION TO THE UART
	signal s_wr_ready : std_logic;								--TRANSMISSION FINISHED SIGNAL
	signal s_wr_rst_n, n_wr_rst_n : std_logic;					--RESET SIGNAL FOR THE DATA TRANSMISSION
	signal s_wr_complete, n_wr_complete : std_logic;			--SIGNAL INDICATING THE END OF A DEBUG TRANSMISSION OVER UART

begin


	u_rd_data : r_data --{{{
	port map(
			data => s_rd_data,
			clk => i_clk,
			rst_n => s_rd_rst_n,
			d_ready => s_rd_ready,
			RX_BIT => i_uart_rx
			); --}}}

	u_wr_data : s_data --{{{
	port map(
			data => s_wr_data,
			clk => i_clk,
			rst_n => s_wr_rst_n,
			send => s_wr_start,
			d_ready => s_wr_ready,
			TX_BIT => o_uart_tx
			); --}}}



	read_comb : process (p_state_read, i_rdata_ack, s_rd_ready, s_rd_data, s_rd_wcnt, s_read_vector) --{{{
	begin
		--DEFAULT VALUES FOR THE READ FSM
		n_rd_rst_n <= '1';
		n_rdata_ready <= '0';
		n_read_vector <= s_read_vector;
		n_rd_wcnt <= s_rd_wcnt;
		n_state_read <= p_state_read;

		case p_state_read is
			when FS_IDLE =>
				n_rd_wcnt <= X"2";
				if s_rd_ready = '1' then
					n_state_read <= FS_ASSEMBLE;	--GO TO THE NEXT STATE FOR DATA ASSEMBLY
				end if;

			when FS_ASSEMBLE =>
				n_read_vector <= s_rd_data & s_read_vector(15 downto 8); 	--ASSEMBLE THE NEW DATA IN THE VECTOR
				n_rd_wcnt <= conv_std_logic_vector(conv_integer(unsigned(s_rd_wcnt))-1,4);	--DECREMENT THE WORD COUNTER
				n_rd_rst_n <= '0';		--RESET THE UART RECEIVER
				n_state_read <= FS_DELAY;	--GO BACK TO THE WAIT STATE

			when FS_DELAY =>
				n_state_read <= FS_WAIT_RX;


			when FS_WAIT_RX =>
				if s_rd_ready = '1' then	--WAIT FOR THE NEXT BYTE
					n_state_read <= FS_ASSEMBLE;
				end if;
				if s_rd_wcnt = X"0" then
					n_state_read <= FS_COMPLETE;	--AFTER 10 BYTES THE VECTOR IS COMPLETE
				end if;


			when FS_COMPLETE =>
				n_rdata_ready <= '1';
				if i_rdata_ack = '1' then	--WAIT FOR THE DEBUG FSM TO ASSERT THE ACK SIGNAL
					n_state_read <= FS_IDLE;
				end if;

			when others => null;
		end case;
	end process read_comb;	--}}}

	read_syn : process (i_clk) --{{{
	begin
		if rising_edge(i_clk) then
			if i_rst = '1' then --SYNCHRONOUS RESET
				p_state_read <= FS_IDLE;
				s_rd_rst_n <= '0';
			else

				p_state_read <= n_state_read;
				s_read_vector <= n_read_vector;
				s_rd_rst_n <= n_rd_rst_n;
				s_rd_wcnt <= n_rd_wcnt;
				s_rdata_ready <= n_rdata_ready;

			end if;	--(i_rst='1')

		end if;	--(rising_edge(i_clk))

	end process read_syn; --}}}


	--OUTPUT VALUES READER
	o_rdata_ready <= s_rdata_ready;
	o_rx_data <= s_read_vector;




	send_comb : process (p_state_send,s_wr_ready,i_start_trans,i_tx_data,s_wr_wcnt,s_wr_data) --{{{
		variable v_curr_byte : integer;
	begin

		--DEFAULT VALUES
		n_state_send <= p_state_send;

		n_wr_start <= '0';
		n_wr_wcnt <= s_wr_wcnt;
		n_wr_data <= s_wr_data;
		n_wr_rst_n <= '1';
		n_wr_complete <= '0';

		v_curr_byte := conv_integer(unsigned(s_wr_wcnt));

		case p_state_send is
			when FS_IDLE =>
				n_wr_wcnt <= X"0"; --TRANSMIT 10 WORDS
				if i_start_trans = '1' then	--WAIT UNTIL THERE IS A START SIGNAL FROM THE DEBUG FSM
					n_state_send <= FS_FRAGMENT_SEND;
				end if;

			when FS_FRAGMENT_SEND =>
				n_wr_rst_n <= '0';	--RESET THE SENDER BEFORE SENDING
				n_wr_data <= i_tx_data((v_curr_byte+1)*8-1 downto v_curr_byte*8);	--ASSIGN THE CORRECT PORTION OF THE DEBUG MESSAGE
				n_wr_wcnt <= conv_std_logic_vector(conv_integer(unsigned(s_wr_wcnt))+1,4);	--INCREMENT THE WORD COUNTER
				n_state_send <= FS_WAIT_SEND;	--GO TO THE SEND STATE

			when FS_WAIT_SEND =>
				n_wr_start <= '1';
				if s_wr_ready = '1' then
					if s_wr_wcnt = X"2" then
						n_state_send <= FS_COMPLETE;
					else
						n_state_send <= FS_DELAY;
					end if;
				end if;

			when FS_DELAY =>
				if s_wr_ready = '0' then
					n_state_send <= FS_FRAGMENT_SEND;
				end if;

			when FS_COMPLETE =>
				n_wr_complete <= '1';
				if i_start_trans = '0' then
					n_state_send <= FS_IDLE;
				end if;

			when others => null;
		end case;
	end process send_comb; --}}}

	send_syn : process (i_clk) --{{{
	begin
		if rising_edge(i_clk) then
			if i_rst = '1' then
				s_wr_rst_n <= '0';
				p_state_send <= FS_IDLE;
			else
				s_wr_wcnt <= n_wr_wcnt;
				s_wr_data <= n_wr_data;
				s_wr_start <= n_wr_start;
				s_wr_rst_n <= n_wr_rst_n;
				s_wr_complete <= n_wr_complete;
				p_state_send <= n_state_send;
			end if;
		end if;
	end process send_syn;

	--OUTPUT SIGNALS
	o_send_done <= s_wr_complete; --}}}

end;
