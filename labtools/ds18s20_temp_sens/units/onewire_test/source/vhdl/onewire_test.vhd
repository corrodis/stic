Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity onewire_test is
	port(
			i_clk : in std_logic;


			--USED AS RESET
			GPIO_DIP1 : in std_logic;
			--USED TO INITIATE A PRESENCE DETECTION
			GPIO_DIP2 : in std_logic;
--			GPIO_DIP3 : in std_logic;
--			GPIO_DIP4 : in std_logic;
			
			GPIO_LED1 : out std_logic;
			GPIO_LED2 : out std_logic;

			--TRISTATE ONEWIRE BUS
			PMOD1_P4 : inout std_logic
	
	
	    );
end onewire_test;




architecture p_detect of onewire_test is



	component clk_div is
	generic (Nd : Integer := 50);
	port(
			clk, en : IN  STD_LOGIC;
			sample     : OUT STD_LOGIC);
	end component;


	component ow_byteiface is --{{{
		port(

					i_sys_clk : in std_logic;		--1MHz clock
					i_rst : in std_logic;			--system reset signal

					--control signals

					i_ow_reset : in std_logic;			--initiate a one wire reset with presence detection
					o_presence_detect : out std_logic;	--one wire presence detected after reset


					i_start : in std_logic;			--start the read or write on the bus
					o_busy : out std_logic;			--transmission finished

					i_write_nread : in std_logic;	--write, not read : 1 to write a bit on the 1-wire bus, 0 to read a bit from the bus

					i_write_byte : in std_logic_vector(7 downto 0);		--bit to write on the bus
					o_read_byte : out std_logic_vector(7 downto 0);		--read bit on the bus

					--onewire bus signal
					io_ow_bus : inout std_logic		--bus signal

			);
	end component; --}}}



	signal s_clk1M : std_logic;

	signal s_ow_reset : std_logic;
	signal s_ow_write_nread : std_logic;
	signal s_ow_start : std_logic;
	signal s_ow_writebyte : std_logic_vector(7 downto 0);
	signal s_ow_readbyte : std_logic_vector(7 downto 0);
	signal s_ow_busy : std_logic;


	type FS_STATE_OWTEST is (FS_IDLE, FS_OW_RESET, FS_RETURN_SWITCH);
	signal p_state, n_state : FS_STATE_OWTEST;

begin


	GPIO_LED2 <= s_ow_busy;


	uut : ow_byteiface --{{{
		port map(

					i_sys_clk => i_clk,		--1MHz clock
					i_rst => GPIO_DIP1,

					--control signals

					i_ow_reset => s_ow_reset,
					o_presence_detect => GPIO_LED1,


					i_start => s_ow_start,
					o_busy => s_ow_busy,

					i_write_nread => s_ow_write_nread,

					i_write_byte => s_ow_writebyte,
					o_read_byte => s_ow_readbyte,

					--onewire bus signal
					io_ow_bus => PMOD1_P4

			); --}}}

		comb_ow_test : process (p_state,GPIO_DIP2,s_ow_busy)
		begin

			n_state <= p_state;

			--DEFAULT SIGNAL VALUES
			s_ow_writebyte <= (others => '0');
			s_ow_write_nread <= '0';
			s_ow_start <= '0';
			s_ow_reset <= '0';

			case p_state is
				when FS_IDLE =>
					if GPIO_DIP2 = '1' then
						n_state <= FS_OW_RESET;
					end if;
					
				when FS_OW_RESET =>
					s_ow_reset <= '1';
					if s_ow_busy = '1' then
						n_state <= FS_RETURN_SWITCH;
					end if;

				when FS_RETURN_SWITCH =>
					if GPIO_DIP2 = '0' and s_ow_busy = '0' then
						n_state <= FS_IDLE;
					end if;


				when others => null;
			end case;

		end process comb_ow_test;



		sync_ow_test : process (i_clk)
		begin
			if rising_edge(i_clk) then
				if GPIO_DIP1 = '1' then
					p_state <= FS_IDLE;
				else
					p_state <= n_state;
				end if;
			end if;
		end process sync_ow_test;


end;
