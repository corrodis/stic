Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity ow_byteiface is
	port(
				i_sys_clk : in std_logic;		--1MHz clock
				i_rst : in std_logic;			--system reset signal

				--control signals

				i_ow_reset : in std_logic;			--initiate a one wire reset with presence detection
				o_presence_detect : out std_logic;	--one wire presence detected after reset


				i_start : in std_logic;			--start the read or write on the bus
				o_busy : out std_logic;			--transmission finished

				i_write_nread : in std_logic;	--write, not read : 1 to write a bit on the 1-wire bus, 0 to read a bit from the bus

				i_write_byte : in std_logic_vector(7 downto 0);		--bit to write on the bus
				o_read_byte : out std_logic_vector(7 downto 0);		--read bit on the bus

				--onewire bus signal
				io_ow_bus : inout std_logic		--bus signal
	    );
end ow_byteiface;




architecture a of ow_byteiface is


	component clk_div is --{{{
	generic (Nd : Integer := 50);
	port(
			clk, en : IN  STD_LOGIC;
			sample     : OUT STD_LOGIC);
	end component; --}}}




	component onewire_rw is --{{{
		port(

					i_sys_clk : in std_logic;		--1MHz clock
					i_rst : in std_logic;			--system reset signal

					--control signals

					i_ow_reset : in std_logic;			--initiate a one wire reset with presence detection
					o_presence_detect : out std_logic;	--one wire presence detected after reset


					i_start : in std_logic;			--start the read or write on the bus
					o_busy : out std_logic;			--transmission finished

					i_write_nread : in std_logic;	--write, not read : 1 to write a bit on the 1-wire bus, 0 to read a bit from the bus

					i_write_bit : in std_logic;		--bit to write on the bus
					o_read_bit : out std_logic;		--read bit on the bus

					--onewire bus signal
					io_ow_bus : inout std_logic		--bus signal

			);
	end component; --}}}


	signal s_clk1M : std_logic;	--1 MHz CLOCK SIGNAL FOR THE 1 WIRE BUS TIMING

	signal s_ow_start, n_ow_start : std_logic;
	signal s_ow_busy : std_logic;

	signal s_wbyte, n_wbyte : std_logic_vector(7 downto 0);

	signal s_ow_readbit : std_logic;

	signal s_ow_rbyte, n_ow_rbyte : std_logic_vector(7 downto 0);

	signal s_ow_reset : std_logic;

	signal s_bit_count, n_bit_count : std_logic_vector(3 downto 0);

	type FS_STATES is (FS_IDLE, FS_WRITE_BIT, FS_WAIT_WRITE, FS_READ_BIT, FS_WAIT_READ, FS_OW_RESET, FS_WAIT_OW);
	signal p_state, n_state : FS_STATES;


begin


	o_read_byte <= s_ow_rbyte;


	u_clk_div : clk_div
	generic map(Nd => 100)
	port map(
				clk => i_sys_clk,
				en => '1',
				sample => s_clk1M
			);


	u_ow_bittrans : onewire_rw --{{{
		port map(

					i_sys_clk => s_clk1M,		--1MHz clock
					i_rst => i_rst,

					--control signals

					i_ow_reset => s_ow_reset,
					o_presence_detect => o_presence_detect,


					i_start => s_ow_start,
					o_busy => s_ow_busy,

					i_write_nread => i_write_nread,

					i_write_bit => s_wbyte(0),
					o_read_bit => s_ow_readbit,

					--onewire bus signal
					io_ow_bus => io_ow_bus 

			); --}}}


		p_fsm_comb : process (p_state, i_write_nread, i_start, i_write_byte, s_ow_rbyte, s_bit_count, s_wbyte, s_ow_busy,s_ow_readbit, i_ow_reset) --{{{
		begin

			--DEFAULT SIGNAL ASSIGNMENTS
			n_state <= p_state;
			n_ow_start <= '0';	--BY DEFAULT SHOULD NOT START A READ OR WRITE. . . 
			n_ow_rbyte <= s_ow_rbyte;
			n_bit_count <= s_bit_count;
			n_wbyte <= s_wbyte;

			o_busy <= '1';

			s_ow_reset <= '0';

			case p_state is
				when FS_IDLE =>
					n_wbyte <= i_write_byte;	--ALREADY ASSIGN THE CORRECT BIT FOR TRANSMISSION
					o_busy <= '0';
					n_bit_count <= (others => '0');

					if i_start = '1' and i_write_nread = '1' then
						n_state <= FS_WRITE_BIT;
					end if;

					if i_start = '1' and i_write_nread = '0' then
						n_state <= FS_READ_BIT;
					end if;

					if i_ow_reset = '1' then
						n_state <=  FS_OW_RESET;
					end if;


				when FS_OW_RESET =>
					s_ow_reset <= '1';
					if s_ow_busy = '1' then
						n_state <= FS_WAIT_OW;
					end if;

				when FS_WAIT_OW =>
					if s_ow_busy = '0' then
						n_state <= FS_IDLE;
					end if;


				--READ A BYTE OVER THE 1-WIRE BUS	--{{{
				when FS_READ_BIT  =>
					n_ow_start <= '1';
					if s_ow_busy = '1' then	--NOW READING THE NEXT BIT
						n_bit_count <= std_logic_vector(unsigned(s_bit_count)+1);
						n_state <= FS_WAIT_READ;
					end if;

					if s_bit_count = "1000" then
						n_state <= FS_IDLE;
					end if;




				when FS_WAIT_READ =>
					if s_ow_busy = '0' then 	--READING THE BIT IS FINISHED
						n_ow_rbyte <=  s_ow_readbit & s_ow_rbyte(7 downto 1);	--SHIFT THE NEW BIT THROUGH
						n_state <= FS_READ_BIT;
					end if;

					 --}}}


				--WRITE A BYTE OVER THE 1-WIRE BUS	--{{{
				when FS_WRITE_BIT  =>
					n_ow_start <= '1';
					if s_ow_busy = '1' then	--NOW WRITING THE NEXT BIT
						n_bit_count <= std_logic_vector(unsigned(s_bit_count)+1);
						n_state <= FS_WAIT_WRITE;
					end if;

					if s_bit_count = "1000" then	--ALL TRANSMITTED
						n_state <= FS_IDLE;
					end if;




				when FS_WAIT_WRITE =>
					if s_ow_busy = '0' then 	--READING THE BIT IS FINISHED
						n_wbyte <= s_wbyte(0) & s_wbyte(s_wbyte'high downto 1);
						n_state <= FS_WRITE_BIT;
					end if;

					 --}}}


				when others => null;
			end case;


		end process p_fsm_comb; --}}}



		p_fsm_syn : process (i_sys_clk)
		begin
			if rising_edge(i_sys_clk) then
				if i_rst = '1' then
					p_state <= FS_IDLE;
					s_ow_start <= '0';
					s_bit_count <= (others => '0');
				else
					
					p_state <= n_state;
					s_ow_start <= n_ow_start;
					s_bit_count <= n_bit_count;
					s_wbyte <= n_wbyte;
					s_ow_rbyte <= n_ow_rbyte;
				end if;
			end if;
			
		end process p_fsm_syn;



end;

