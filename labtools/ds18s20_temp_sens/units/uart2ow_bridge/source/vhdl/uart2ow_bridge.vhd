Library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity uart2ow_bridge is
	port(
			i_clk : in std_logic;	--SYSTEM CLOCK 40 MHZ
			i_reset : in std_logic;	--SYSTEM RESET SIGNAL

			--USED TO INITIATE A PRESENCE DETECTION
			
			GPIO_LED1 : out std_logic;
			GPIO_LED2 : out std_logic;


			--UART INTERFACE PINS
			i_uart_rx : in std_logic;
			o_uart_tx : out std_logic;

			--TRISTATE ONEWIRE BUS
			io_ow_data_line : inout std_logic
	    );
end uart2ow_bridge;




architecture a of uart2ow_bridge is


	component uart_iface is --{{{
		port(
			i_clk : in std_logic;	--SYSTEM CLOCK
			i_rst : in std_logic;	--SYSTEM RESET ACTIVE HIGH
			i_uart_rx : in std_logic;	--UART RX SIGNAL
			o_uart_tx : out std_logic;	--UART TX SIGNAL

			i_tx_data : in std_logic_vector(15 downto 0); --INPUT DATA TO SEND TO THE UART
			o_rx_data : out std_logic_vector(15 downto 0);	--RECEIVED DATA FROM THE UART

			i_start_trans : in std_logic;	--SIGNAL TO START THE DATA TRANMISSION
			o_send_done : out std_logic;	--DATA TRANSMISSION FINISHED

			i_rdata_ack : in std_logic;		--ACK THAT DATA HAS BEEN RECEIVED BY THE DEBUG FSM
			o_rdata_ready : out std_logic	--NEW REASSEMBLED DATA FROM THE UART IS READY
	    );
	end component; --}}}


	component ow_byteiface is --{{{
		port(

					i_sys_clk : in std_logic;		--1MHz clock
					i_rst : in std_logic;			--system reset signal

					--control signals

					i_ow_reset : in std_logic;			--initiate a one wire reset with presence detection
					o_presence_detect : out std_logic;	--one wire presence detected after reset


					i_start : in std_logic;			--start the read or write on the bus
					o_busy : out std_logic;			--transmission finished

					i_write_nread : in std_logic;	--write, not read : 1 to write a bit on the 1-wire bus, 0 to read a bit from the bus

					i_write_byte : in std_logic_vector(7 downto 0);		--byte to write on the bus
					o_read_byte : out std_logic_vector(7 downto 0);		--read byte on the bus

					--onewire bus signal
					io_ow_bus : inout std_logic		--bus signal

			);
	end component; --}}}


	--UART SIGNALS
	--RX SIGNALS
	signal s_uart_rdata : std_logic_vector(15 downto 0);
	signal s_uart_rdata_ready : std_logic;
	signal s_uart_rdata_ack : std_logic;

	--THE COMMAND PART OF THE UART PACKET
	signal s_uart_cmd : std_logic_vector(7 downto 0);

	--TX SIGNALS
	signal s_uart_tx_done : std_logic;
	signal s_uart_sdata : std_logic_vector(15 downto 0);
	signal s_uart_start_trans : std_logic;

	--ONE WIRE BUS SIGNALS
	signal s_ow_reset : std_logic;
	signal s_ow_write_nread, n_ow_write_nread : std_logic;
	signal s_ow_start : std_logic;
	signal s_ow_wbyte : std_logic_vector(7 downto 0);
	signal n_ow_wbyte : std_logic_vector(7 downto 0);
	signal s_ow_rbyte : std_logic_vector(7 downto 0);
	signal s_ow_busy : std_logic;



	type FS_STATES is (FS_IDLE, FS_EVAL_CMD, FS_WRITE_OW, FS_WAIT_OW, FS_READ_OW, FS_WAIT_READ, FS_WRITE_UART, FS_OW_RESET, FS_READ_ACK);

	signal p_state, n_state : FS_STATES;


begin

	GPIO_LED1 <= s_ow_rbyte(0);

	s_ow_wbyte <= s_uart_rdata(15 downto 8);	--ASSIGN THE PAYLOAD OF THE UART DATA TO THE OW WRITE BYTE
	s_uart_cmd <= s_uart_rdata(7 downto 0);		--EXTRACT THE COMMAND PART OF THE UART DATA

	--DEFAULT REPLY
	s_uart_sdata <= s_ow_rbyte & X"DA";

	u_ow_iface: ow_byteiface --{{{
		port map(

					i_sys_clk => i_clk,
					i_rst => i_reset,

					--control signals

					i_ow_reset => s_ow_reset,
					o_presence_detect => GPIO_LED2,


					i_start => s_ow_start,
					o_busy => s_ow_busy,

					i_write_nread => s_ow_write_nread,

					i_write_byte => s_ow_wbyte,
					o_read_byte => s_ow_rbyte,

					--onewire bus signal
					io_ow_bus => io_ow_data_line

			); --}}}

	u_uart_iface : uart_iface --{{{
	port map(
		i_clk => i_clk,
		i_rst => i_reset,
		i_uart_rx => i_uart_rx,
		o_uart_tx => o_uart_tx,

		i_tx_data => s_uart_sdata,
		o_rx_data => s_uart_rdata,

		i_start_trans => s_uart_start_trans,
		o_send_done => s_uart_tx_done,

		i_rdata_ack => s_uart_rdata_ack,
		o_rdata_ready => s_uart_rdata_ready
	);
	--}}}


	p_fsm_comb : process (p_state, s_uart_rdata_ready, s_uart_tx_done, s_uart_rdata, s_ow_busy, s_ow_write_nread, s_uart_cmd)	--{{{
	begin

		--DEFAULT SIGNAL ASSIGNMENTS
		n_state <= p_state;

		--UART DEFAULT SINALS
		s_uart_start_trans <= '0';
		s_uart_rdata_ack <= '0';

		--1-WIRE DEFAULT SIGNALS
		s_ow_reset <= '0';
		s_ow_start <= '0';
		n_ow_write_nread <= s_ow_write_nread;

		case p_state is
			when FS_IDLE =>
				if s_uart_rdata_ready = '1' then
					n_state <= FS_EVAL_CMD;
				end if;

			when FS_EVAL_CMD => --{{{
				if s_uart_cmd = X"69" then	--i
					n_state <= FS_OW_RESET;
				end if;	

				if s_uart_cmd = X"72" then	--r
					n_ow_write_nread <= '0';
					n_state <= FS_READ_OW;
				end if;	

				if s_uart_cmd = X"77" then	--w
					n_ow_write_nread <= '1';
					n_state <= FS_WRITE_OW;
				end if;	
				--}}}


			--WRITE NEXT BYTE TO THE OW BUS 
			when FS_WRITE_OW =>
				n_ow_write_nread <= '1';
				s_ow_start <= '1';

				if s_ow_busy = '1' then
					n_state <= FS_WAIT_OW;
				end if;
				

			when FS_READ_OW =>
				n_ow_write_nread <= '0';
				s_ow_start <= '1';
				if s_ow_busy = '1' then
					n_state <= FS_WAIT_READ;
				end if;

			when FS_WAIT_READ =>
				if s_ow_busy = '0' then
					n_state <=  FS_WRITE_UART;
				end if;


			when FS_WRITE_UART =>
				s_uart_rdata_ack <= '1';	--ACK THE PREVIOUS RDATA FROM THE UART
				s_uart_start_trans <= '1';
				if s_uart_tx_done = '1' then
					n_state <= FS_IDLE;
				end if;



			when FS_OW_RESET =>
				s_ow_reset <= '1';
				if s_ow_busy = '1' then
					n_state <= FS_WAIT_OW;
				end if;



			when FS_WAIT_OW =>
				if s_ow_busy = '0' then
					n_state <= FS_READ_ACK;
				end if;

			when FS_READ_ACK =>
				s_uart_rdata_ack <= '1';
				if s_uart_rdata_ready = '0' then
					n_state <= FS_IDLE;
				end if;



			when others => null;
		end case;

	end process p_fsm_comb; --}}}



	p_fsm_sync : process (i_clk)
	begin
		if rising_edge(i_clk) then
			if i_reset = '1' then
				p_state <= FS_IDLE;
			else
				p_state <= n_state;

				--1-WIRE FF REGISTERS
				s_ow_write_nread <= n_ow_write_nread;

			end if;
		end if;

	end process p_fsm_sync;


end;
