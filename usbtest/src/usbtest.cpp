/*
 * Small tool to test the usb
 *
 */
 
#include <libusb.h>
#include <iostream>

using std::cout;
using std::endl;

int main(int argc, char **argv) {

	//---------- INIT ---------------
	// Init needed variables
	libusb_context *ctx = NULL; 			// libusb context
	struct libusb_device_handle *devh = NULL;	// usb device handle for libusb
	struct libusb_device * dev = NULL;
	unsigned int vid = 0x04b4;			// Vendor ID of the USB device we want to open
	unsigned int pid = 0x1003;			// Product ID of the USB device we want to open
	unsigned int timeout = 2000;			// Timeout in ms for read and write operations (500ms)
	// Init libusb
	int r;	
	r = libusb_init(NULL);				
	if(r<0)
		cout << "libusb_init failed:" << r << endl;
	else
		cout << "libusub_init" << endl;
	// Set Log Level to debug
	cout << "set debug level" << endl;
	libusb_set_debug(NULL,4);
	
	cout << "open device" << endl;
	devh = libusb_open_device_with_vid_pid(NULL, vid, pid);
	cout << "dev handle: " << devh << endl;
	dev = libusb_get_device(devh);
	//cout << "dev: " << *dev << endl;

	cout << "claim interface" << endl;
	r = libusb_claim_interface(devh, 0);		
	if(r<0)
		cout << "libusb_claim_interface failed: " << r << endl;
	
	cout << "bus number: " << std::dec << (int)libusb_get_bus_number(dev) << endl;

	cout << "port number: " << (int)libusb_get_port_number(dev) << endl;

	uint8_t port_numbers[7];
	int no = 0;
	no = libusb_get_port_numbers(dev, port_numbers, 7);
	cout << "port numbers (" << no << "): ";
	for(int i = 0; i < no; ++i)
		cout << (int)port_numbers[i] << " ";
	cout << endl;

	cout << "partent handle id: " << libusb_get_parent(dev) << endl;

	cout << "bus address (hex): " << std::hex << (int)libusb_get_device_address(dev) << endl;

	cout << "speed: " << std::dec << libusb_get_device_speed(dev);
	cout << " (0: unknown, 1:low(1.5MBit/s, 2:full(12Mbit/s), 3:high(480MBit/s), 4:super(5000MBit/s))" << endl;

	
	cout << "wMaxPacketSize (-5:LIBUSB_ERROR_NOT_FOUND(endpoint doesn't exist))" << std::dec << endl;
	for(int ep = 0; ep < 16; ep++) {
		cout << "\t" << ep << "IN: " << libusb_get_max_packet_size(dev, 0x80 | ep) << endl;
		cout << "\t" << ep << "OUT: " << libusb_get_max_packet_size(dev, 0x00 | ep) << endl;
	
	}
	
	cout << "Max packet size in 1 microframe: " << endl;
	for(int ep = 0; ep < 16; ep++) {
		cout << "\t" << ep << "IN: " << libusb_get_max_iso_packet_size(dev, 0x80 | ep) << endl;
		cout << "\t" << ep << "OUT: " << libusb_get_max_iso_packet_size(dev, 0x00 | ep) << endl;
	
	}

	int config;
	if(!libusb_get_configuration(devh, &config))
		cout << "bConfigurationValue: " << config << endl;
	else
		cout << "libusb_get_configuration failed" << endl;

	struct libusb_device_descriptor * desc = new libusb_device_descriptor();
	cout << "get device descriptor" << endl;
	if(libusb_get_device_descriptor(dev, desc))
		cout << "libusb_get_device_descriptor failed" << endl;
	cout << "\tbLength (size of descriptor): " 	    << (int)desc->bLength << endl;
	cout << "\tType (LIBUSB_DT_DEVICE): " 		    << (int)desc->bDescriptorType << endl;
	cout << "\tbcdUSB: " 			<< std::hex << (int)desc->bcdUSB << endl;
	cout << "\tClass: " 			<< std::dec << (int)desc->bDeviceClass << endl;  	
	cout << "\tSubClass: " 			<< std::dec << (int)desc->bDeviceSubClass << endl;  	
	cout << "\tDeviceProtocol: " 		<< std::dec << (int)desc->bDeviceProtocol << endl; 	
	cout << "\tMaxPacketSize0 (ep0): " 	<< std::dec << (int)desc->bMaxPacketSize0 << endl;  	
	cout << "\tidVendor: " 			<< std::hex << (int)desc->idVendor << endl;  	
	cout << "\tidProduct: " 		<< std::hex << (int)desc->idProduct << endl;  	
	cout << "\tbcdDevice: " 		<< std::hex << (int)desc->bcdDevice << endl;  	
	cout << "\tiManufacturer: " 		<< std::hex << (int)desc->iManufacturer << endl;  	
	cout << "\tiSerialNumber: " 		<< std::dec << (int)desc->iSerialNumber << endl;  	
	cout << "\tbNumConfigurations: " 	<< std::hex << (int)desc->bNumConfigurations << endl;  	

	struct libusb_config_descriptor * conf = new libusb_config_descriptor();
	for(int i = 0; i < desc->bNumConfigurations; ++i) {
		cout << "Get Configuration no " << i << endl;
		if(!libusb_get_config_descriptor(dev, i, &conf) != 0)
			cout << "libusb_get_config_descriptor failed" << endl;
	}

	
	if(libusb_get_active_config_descriptor(dev, &conf) != 0)
		cout << "libusb_get_ative_config_descriptor failed" << endl;
	cout << "\tbLength: "				<< std::dec << (int)conf->bLength << endl;
	cout << "\tbDescriptorType: "			<< std::dec << (int)conf->bDescriptorType << endl;
	cout << "\twTotalLength (max data returned): "	<< std::dec << (int)conf->wTotalLength << endl;
	cout << "\tbNumInterfaces: "			<< std::dec << (int)conf->bNumInterfaces << endl;
	cout << "\tbConfigurationValue: "		<< std::dec << (int)conf->bConfigurationValue << endl;
	cout << "\tiConfiguration: "			<< std::dec << (int)conf->iConfiguration << endl;
	cout << "\tbmAttributes: 0x"			<< std::hex << (int)conf->bmAttributes << endl;
	cout << "\tMaxPower: "				<< std::dec << (int)conf->MaxPower << endl;
	cout << "\textra: "				<< std::dec;
	for(int j = 0; j < conf->extra_length; j++)
		cout  << conf->extra[j];
	cout << endl;
	

	//unsigned short ep_read;		//!< the endpoint number for reading
	//unsigned short ep_write;	//!< the endpoint number for writing

	//bool device_open;		//!< indicator if a device is open
	return 0;
}
