void PlotsTreeOccupancy(char filein[80]="EventSelection.root",
			char savecanvdir[80]="./CanvasDir"){ 
  TFile *f = new TFile(filein); 
  TTree *tree = (TTree*)f->Get("EventTree"); 

  TH2F *h_map0 = new TH2F("h_map_asic0","h_map_asic0",16,0.5,16.5,4,0.5,4.5); 
  TH2F *h_map1 = new TH2F("h_map_asic1","h_map_asic1",16,0.5,16.5,4,0.5,4.5);
  
  TCanvas *c = new TCanvas("cOccupancy","cOccupancy",900,900); 
  c->Divide(1,4);
  TLine *l[4]; 
  for (int i=0;i<4;i++) { 
    l[i]= new TLine(4.5+i*4,0.5,4.5+i*4,4.5);  
    l[i]->SetLineWidth(2); 
  }
  
  // chan0 in ASIC0 
  c->cd(1); 
  EventTree->Draw("Y[0]:X[0]>>h(16,0.5,16.5,4,0.5,4.5)","TovT[0]*(Nhits==2 && Chan[0]<64)","box");
  for (int i=0;i<4;i++){ l[i]->Draw("same"); }

  // chan0 in ASIC1
  c->cd(2); 
  EventTree->Draw("Y[0]:X[0]>>h2(16,0.5,16.5,4,0.5,4.5)","TovT[0]*(Nhits==2 && Chan[0]>63)","box");
  for (int i=0;i<4;i++){ l[i]->Draw("same"); }

  // chan1 in ASIC 0 
  c->cd(3); 
  EventTree->Draw("Y[1]:X[1]>>h3(16,0.5,16.5,4,0.5,4.5)","TovT[1]*(Nhits==2 && Chan[1]<64)","box");
  for (int i=0;i<4;i++){ l[i]->Draw("same"); }

  // chan1 in ASIC 1 
  c->cd(4); 
  EventTree->Draw("Y[1]:X[1]>>h4(16,0.5,16.5,4,0.5,4.5)","TovT[1]*(Nhits==2 && Chan[1]>63)","box");
  for (int i=0;i<4;i++){ l[i]->Draw("same"); }
  
  char csave[80]; 
  sprintf(csave,"%s%s",savecanvdir,"/cOccupancy.root"); 
  c->Print(csave); 
  
}
