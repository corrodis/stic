void GetRateSingle(Int_t RunID=1,  		       
		   Double_t run_duration=10){
  char totfile[80]; 
  sprintf(totfile,"./Run%d/_ToThistos.root",RunID); 
  TFile *f = new TFile(totfile); 
  TH1F *h_all = f->Get("h_chan");// Nr hits in total
  h_all->Draw();
  Int_t xx=h_all->GetBinContent(108);
  
  if (RunID==32 || RunID==33) { 
    Int_t xx=(h_all->GetBinContent(81)+
	      h_all->GetBinContent(82)+
	      h_all->GetBinContent(83)+
	      h_all->GetBinContent(84)+
	      h_all->GetBinContent(85)+
	      h_all->GetBinContent(87)+
	      h_all->GetBinContent(88)+
	      h_all->GetBinContent(89)+
	      h_all->GetBinContent(90)+
	      h_all->GetBinContent(91)+
	      h_all->GetBinContent(92)+
	      h_all->GetBinContent(93)+
	      h_all->GetBinContent(94)+
	      h_all->GetBinContent(95)+
	      h_all->GetBinContent(96))/15.;
    
  }
    
  
  h_all->Fit("pol0","","q",16,32); 
  
  char totcuts[80]; 
  sprintf(totcuts,"./Run%d/_ToTcuts.txt",RunID); 
  ifstream fin; 
  fin.open(totcuts); 
  Double_t line , chann , tot , sigma ; 
  while (fin) { 
    fin >> line >> chann >> tot >> sigma ; 
  }
  fin.close(); 
  
  cout <<RunID << " " <<  xx/run_duration << " " << pol0->GetParameter(0)/run_duration << " " << tot << endl; 


}
