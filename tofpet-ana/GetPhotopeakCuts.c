#include "ReadSingleVariables.h" 
#include <algorithm>  
#include <iostream> 
void GetPhotopeakCuts(char filein[80]="ToT_histos.root", 
		      Int_t MinNrEntries=100,
              Double_t PeakParameter = 0.1,
		      char fileout[80]="ToT_Cuts.txt",
		      char savecanvdir[80]="./CanvasDir"){
  
  TFile *f = new TFile(filein); 
  
  TCanvas *cToT[8];     
  char cname[80]; 
  for (int k=0; k<8; k++) { 
    sprintf(cname,"cToT_Asic%d_Slot%d",k/4,k%4); 
    cToT[k] = new TCanvas(cname,cname,1000,900);
    cToT[k]->Divide(4,4); 
  }

    
  TF1 *fit_gaus = new TF1("fit_gaus","gaus",-10000,10000); 
  Double_t ChannID[NChannels];
  Double_t CutPP[NChannels]; 
  Double_t CutPPWnd[NChannels]; 

  ofstream fout; 
  fout.open(fileout); 

  char tit[80];
  char result[80]; 
  Int_t i=0;
  TBox *l[NChannels]; 
  
  for (int k=0; k<NChannels; k++) { 
    // 
    sprintf(tit,"ToT Channel%d",k); 
    TH1F *h=f->Get(tit); 
    
    // non empty histograms 
    if (h->GetEntries()>MinNrEntries) { 
      // -----
      cToT[k/16]->cd(k%16+1); 
      h->Draw(); 
      i++; 
      // -----
    
      TSpectrum *s = new TSpectrum(10);
      Int_t nfound = s->Search(h,1,"new",PeakParameter); //Par = 0.01 for high volt - 0.1 for conventional peaks
      Float_t *pp  = s->GetPositionX();

      Int_t MaxPeak=*std::max_element(pp,pp+nfound);
      h->Fit(fit_gaus,"q","",MaxPeak-20,MaxPeak+20); 
      h->Fit(fit_gaus,"q","",fit_gaus->GetParameter(1)-0.5*fit_gaus->GetParameter(2),fit_gaus->GetParameter(1)+1.5*fit_gaus->GetParameter(2));
    
      ChannID[i] = k; 
      CutPP[i] = fit_gaus->GetParameter(1);
      CutPPWnd[i] = 1.0*fit_gaus->GetParameter(2);

      l[k] = new TBox(CutPP[i]-CutPPWnd[i],0,CutPP[i]+CutPPWnd[i],h->GetMaximum());
      l[k]->SetFillStyle(0);
      l[k]->Draw("same"); 
      
      sprintf(result,"%5d %5d %8.2f %7.2f \n", i, ChannID[i], CutPP[i], CutPPWnd[i]) ;
      fout << result; 
    }
  }

  TCanvas *cCutsRes= new TCanvas("cCuts","cCuts",900,400);
  Double_t hmin=100;
  Double_t hmax=250; 
  cCutsRes->DrawFrame(-0.5,hmin,127.5,hmax); 
  cCutsRes->SetGrid(); 

  TGraphErrors *g = new TGraphErrors(i+1,ChannID,CutPP,0,CutPPWnd); 
  g->SetTitle("Photopeak cuts; ChannelID ; Photopeak mean +/- sigma [ns]"); 
  g->SetMarkerStyle(2); 
  g->Draw("P same"); 
  
  TLine *line[8]; 
  for (int i=0;i<9;i++) { 
    line[i]= new TLine(-0.5+i*16,hmin,-0.5+i*16,hmax);  
    line[i]->SetLineColor(2); 
    line[i]->Draw("same"); 
  }


  fout.close(); 
  
  char csave[80]; 
  for (int k=0;k<8;k++) { 
    sprintf(cname,cToT[k]->GetName()); 
    sprintf(csave,"%s%s%s%s",savecanvdir,"/",cname,".root"); 
    cToT[k]->Print(csave); 	
  }
  sprintf(csave,"%s%s",savecanvdir,"/cPhotopeakCuts.root"); 
  cCutsRes->Print(csave); 
  
  
}
