#include "ReadSingleVariables.h" 
#include <iostream> 

void BuildEventTreThrCut(char filename[120]="../data/first/na_default_new_s.root", Double_t Threshold=0, char fout[80]="EventSelectionThrCut.root"){
 
 
  // ----------------------------------------
  // INPUT TREE 
  TFile *f = new TFile(filename); 
  TTree *lmData = (TTree*)f->Get("lmData"); 
  // Branches 
  lmData->SetBranchAddress("time",&time);
  lmData->SetBranchAddress("channel",&channel);
  lmData->SetBranchAddress("tot",&tot);
 

  // ----------------------------------------
  // OUTPUT EVENTS TREE 
  TFile *fevt = new TFile(fout,"RECREATE");
  Int_t    EvtID; 
  Int_t    Nhits; 
  Int_t    NhitsPP; 
  Int_t    PPevt;  
  Double_t Coinc_ns; 
  Long64_t Time[MaxEvtSize] ;
  Int_t    Chan[MaxEvtSize] ; 
  Int_t    X[MaxEvtSize] ; 
  Int_t    Y[MaxEvtSize] ; 
  Float_t  TovT[MaxEvtSize] ;
  Int_t    PPcut[MaxEvtSize];// 1 = at photopeak  
  Int_t    IndxPP[2];
  //Int_t    ChanPP[2];
  //Long64_t TimePP[2];

  TTree *EventTree = new TTree("EventTree","Event Tree");
  EventTree->Branch("EvtID",   &EvtID,"EvtID/I");
  EventTree->Branch("Coinc_ns",&Coinc_ns,"Coinc_ns/D");
  EventTree->Branch("Nhits",   &Nhits,"Nhits/I");
  EventTree->Branch("NhitsPP", &NhitsPP,"NhitsPP/I");
  EventTree->Branch("Time",    &Time,"Time[Nhits]/L");
  EventTree->Branch("Chan",    &Chan,"Chan[Nhits]/I");
  EventTree->Branch("X",       &X,"X[Nhits]/I");
  EventTree->Branch("Y",       &Y,"Y[Nhits]/I");
  EventTree->Branch("TovT",    &TovT,"TovT[Nhits]/F");
  EventTree->Branch("PPcut",   &PPcut,"PPcut[Nhits]/I");
  EventTree->Branch("PPevt",   &PPevt,"PPevt/I");
  EventTree->Branch("IndxPP",    &IndxPP,"IndxPP[2]/I");
  //EventTree->Branch("ChanPP",    &ChanPP,"ChanPP[2]/I");
  //EventTree->Branch("TimePP",    &TimePP,"TimePP[2]/L");
  Coinc_ns =  CoincWnd/100.;
  // ----------------------------------------
  
  // --------------------------------------------------  
  /*
  // * * *  R E A D    P H O T O C U T    I N F O * * * 

  Double_t PPpeak[NChannels];
  Double_t PPwidh[NChannels]; 
  // loose - global cut 
  for (int k=0;k<128;k++) { 
    PPpeak[k]=0;
    PPwidh[k]=0;
  }
  Int_t indx,i;
  Double_t a,b; 
  ifstream fin; 
  fin.open(totcuts);
  while(fin) { 
    fin >> indx >> i >> a >> b ; 
    PPpeak[i] = a ; 
    PPwidh[i] = N_SIGMA*b ; 
    //cout << i << " " << PPpeak[i] <<  " " << PPwidh[i] << endl; 
  }
  fin.close(); 
  */

  // ----------------------------------------------  
  // * * *  R E A D    I N P U T    T R E E   * * * 
  cout << " NSingles = " << lmData->GetEntries() << endl;  
  Int_t MaxSizeReached = 0;			
  Int_t firsthittaken  = 0; 
  for (int i=0;i<lmData->GetEntries(); i++) { 
  //for (int i=0; i< 100000; i++) { 
    lmData->GetEntry(i); 

   
    // -------------------------------------------------------  
    // * * *  W R I T E   O U T   E V E N T    T R E E   * * * 
    // build events -in coincidence windows 

    // ONLY EVTS AT PHOTOPEAK ENTER THE TREE ! 
    //if (i < 100 ) cout << i << " " << tot << " " << PPpeak[channel] << endl; 
    // if (abs(tot-PPpeak[channel])< PPwidh[channel] ) { 
    if (tot> Threshold) { 
    
      if (firsthittaken == 0) { 
	// fist hit 
	Double_t tstart = time; 
	Nhits = 1; 
	EvtID = 0;
	
	Time[Nhits-1]=time;  
	Chan[Nhits-1]=channel; 
	TovT[Nhits-1]=tot; 
	X[Nhits-1]=xmap[channel%64]; 	
	Y[Nhits-1]=ymap[channel%64]; 	

	/*
	if (abs(tot-PPpeak[channel]) < PPwidh[channel]) {
	  PPcut[Nhits-1] = 1;
	  NhitsPP = 1; } 
	else { 
	  PPcut[Nhits-1] = 0; 
	  NhitsPP = 0; }
	*/ 

	firsthittaken = 1; 
      }
      //
      else {
	//if ((time - tstart) < CoincWnd) {
	if ( time>=tstart &&  (time - tstart) < CoincWnd) {
	  Nhits++; 
	  //protection against too large evts 
	  /*
	    if (MaxSizeReached == 0) Nhits++;
	    else Nhits=1; 
	    
	    if (Nhits == MaxEvtSize) { 
	    tstart = time; 
	    MaxSizeReached = 1; }
	    else { MaxSizeReached = 0;}
	  */
	}
	else {
	  //here the event is complete - write to tree 

	  // - - - - - - - -
	  EventTree->Fill(); 
	  // - - - - - - - - 
	  
	  // new event 
	  tstart = time; 
	  Nhits = 1; 
	  EvtID ++; 
	  NhitsPP = 0; 
	}
	
	Time[Nhits-1]=time; 
	Chan[Nhits-1]=channel; 
	TovT[Nhits-1]=tot; 
	X[Nhits-1]=xmap[channel%64]; 	
	Y[Nhits-1]=ymap[channel%64]; 	
	
	//cout << channel <<  " " << channel%64 << " " << xmap[channel%64] << " "  << Chan[Nhits-1] << " " << X[Nhits-1] << " " <<Y[Nhits-1] << endl; 
	
	/*
	if (abs(tot-PPpeak[channel]) < PPwidh[channel]) { 
	  PPcut[Nhits-1] = 1;
	  NhitsPP++; 
	}
	else {
	  PPcut[Nhits-1] = 0;
	}
     	*/

      }
    }
  }

  EventTree->Write();
  cout << " NEvents  = " <<  EventTree->GetEntries() << endl; 
  fevt->Close(); 

}
