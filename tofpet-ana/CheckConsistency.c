#include "OperationalVariables.h"

Bool_t CheckConsistency(){
  Int_t N_0=0; 
  Int_t N_1=0; 
  for (int k=0; k<4; k++) { 
    if (Slot0[k]!=0) {N_0++;} 
    if (Slot1[k]!=0) {N_1++;}     
  }
  
  if (N_0 == Narray0 && N_1 == Narray1) return 1; 
  else return 0; 
}
