#include "GetPairID.c" 
void DrawCRT(char fileCRT[80]="CRT_histos.root", Int_t Ch1stASIC0= 0, Int_t Ch1stASIC1=96) { 
  //
  TFile *f = new TFile(fileCRT); 
  
  TH1F *hh[16][16]; 
  Int_t channel0, channel1; 
 
  TCanvas *c_CRT[16];
  char ctit[80],hname[80]; 

  for (int ic=0 ; ic<16; ic++ ) { 

    channel0 = Ch1stASIC0 + ic; 
    sprintf(ctit,"CRTfit_ch%dASIC0",channel0); 
    c_CRT[ic] = new TCanvas(ctit,ctit,1000,900);    
    c_CRT[ic]->Divide(4,4); 
    
    for (int j=0 ; j<16; j++) {    
      c_CRT[ic]->cd(j+1); 
      channel1 = Ch1stASIC1 + j; 
      
      sprintf(hname,"Pair_%d_%d",channel0,channel1);
      hh[ic][j]=(TH1F*)f->Get(hname); 
      hh[ic][j]->Draw();
    }
  }
}
