void GetCountingsZh(Int_t RunID=17, Double_t time=100) {
  //
  char dire[80]="./09Dec14_500M/";
  Int_t min0=16; 
  Int_t max0=32; 
  Int_t min1=96; 
  Int_t max1=112; 
  
  char totfile[80]; 
  sprintf(totfile,"%s/Run%d/_ToThistos.root",dire,RunID); 
  char filein[80]; 
  sprintf(filein,"%s/Run%d/_EvtTree.root",dire,RunID); 
  
  TCanvas *c_count = new TCanvas("cCountings","cCountings",1000,900); 
  c_count->Divide(3,3);
  
  // All hits counting 
  TFile *f = new TFile(totfile); 
  TH1F *h_all = f->Get("h_chan");// Nr hits in total
  TH1F *h2 = h_all->Clone();  //h2->Rebin(16); // nr hits per array
  TH1F *h3 = h_all->Clone();  //h3->Rebin(64); // nr hits per ASIC
  c_count->cd(1); h_all->Draw(); 
  c_count->cd(2); h2->Draw(); 
  c_count->cd(3); h3->Draw(); 
  h_all->SetTitle("Channel Occupancy"); 
  h2->Fit("pol0","","",min0,max0);
  Double_t asic0 = pol0->GetParameter(0); 
  h3->Fit("pol0","","",min1,max1); 
  Double_t asic1 = pol0->GetParameter(0); 
  //f->Close(); 

  // Hits at photopeak 
  TFile *fevt = new TFile(filein); 
  TTree *tree = (TTree*)fevt->Get("EventTree"); 
  c_count->cd(4); tree->Draw("Chan>>h_chanPP(128,0,128)"); // Nr hits at photopeak  
  TH1F *h5 = h_chanPP->Clone();
  TH1F *h6 = h_chanPP->Clone();
  c_count->cd(5); h5->Draw(); 
  c_count->cd(6); h6->Draw(); 
  h_chanPP->SetTitle("Channel Occupancy - PP"); 
  h5->Fit("pol0","","",min0,max0);
  Double_t asic0PP = pol0->GetParameter(0); 
  h6->Fit("pol0","","",min1,max1); 
  Double_t asic1PP = pol0->GetParameter(0); 

  // Hits at photopeak and in coincidence 
  c_count->cd(7); tree->Draw("Chan>>h_chanPPCoinc(128,0,128)","Nhits==2"); //Nr hits in coincidence 
  TH1F *h8 = h_chanPPCoinc->Clone();
  TH1F *h9 = h_chanPPCoinc->Clone();
  c_count->cd(8); h8->Draw(); 
  c_count->cd(9); h9->Draw(); 
  h_chanPPCoinc->SetTitle("Channel Occupancy -PP/ in coinc");
  h8->Fit("pol0","","",min0,max0);
  Double_t asic0PPcoinc = pol0->GetParameter(0); 
  h9->Fit("pol0","","",min1,max1); 
  Double_t asic1PPcoinc = pol0->GetParameter(0); 
	  
  char result[80]; 
  sprintf(result, "%d %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f \n", 
	  RunID , asic0/time, asic1/time, asic0PP/time, asic1PP/time, asic0PPcoinc/time, asic1PPcoinc/time);  
  cout << result << endl;
	  
}
