#include "ReadSingleVariables.h" 
#include <iostream> 
void WriteSinglesFile(char filename[80]="ExampleSingles.root",
		      char Singlefile[80]="SinglesFile.txt"){

  TFile *f = new TFile(filename); 
  TTree *lmData = (TTree*)f->Get("lmData");
  // Branches 
  lmData->SetBranchAddress("time",&time);
  lmData->SetBranchAddress("channel",&channel);
  lmData->SetBranchAddress("tot",&tot);

  ofstream fout; 
  fout.open(Singlefile); 

  char result[80]; 
  Int_t mod,xtal;
  Int_t X,Y; 
  
  for (int i=0; i<lmData->GetEntries(); i++) { 
    lmData->GetEntry(i);
    X=xmap[channel%64]; 	
    Y=ymap[channel%64]; 
	
    if (channel<64) { 
      mod = (X-1)/4; 
      xtal= ((X-1)%4)*4 + (Y-1);
    }
    else { 
      mod = 7-(X-1)/4; 
      xtal= (3-(X-1)%4)*4+ (Y-1); 
    }

    sprintf(result,
	    "%5d %5d %5d %7.2f %8ld\n", 
	    i, mod, xtal, tot, time);
    
    fout << result ;
    if (i%100000==0) cout << result; 
    
  }
  fout.close(); 
    
}
