Int_t GetIndexArray(Int_t *array, Int_t size, Int_t element) { 
  Int_t index = -1; 
  for (int k=0; k<size; k++) { 
    if (array[k]==element) index = k; 
  }
  return index; 
}
