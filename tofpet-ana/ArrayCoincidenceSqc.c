#include "GetToThistos_simple.c" 
#include "GetPhotopeakCuts.c" 
#include "GetGlobalHistos.c"
#include "BuildEventTreeSelection.c"
#include "GetCountings.c"
#include "GetCRTmatrix3.c"
#include "PlotsTreeMultiplicity.c"
#include "PlotsTreeOccupancy.c"
#include "PlotsTree2HitsSelectionNew.c"
#include "DrawCRT.c"

//#include "ReadSingleVariables.h" 


Double_t ArrayCoincidenceSqc(char filein[120]="/data/tofpet/HILGER_4x4/BiasScan/root/Vscan_Vb664_s.root", 
			     char fileout_path[180]="/home/mri-pet/Desktop/ana/Xtals/ARRAY_HILGER4x4/BiasScan/Vscan_Vb664", 
			     char canvas_dir[180]="/home/mri-pet/Desktop/ana/Xtals/ARRAY_HILGER4x4/BiasScan/",
			     Double_t PeakParameter = 0.05,  
			     Int_t iToT = 1, 
			     Int_t iCRT = 0,
			     Int_t iPlot = 0,
			     Int_t Ch0=-1, Int_t Ch1=-1){   

 /*
  sprintf(filein,"/home/mri-pet/Documents/TOFPET/ekit-sw-2/data/Xtals/FULL_OCCU/root/Test4plus1_66V4_s.root"); 
  sprintf(fileout_path,"/home/mri-pet/Desktop/ana/Xtals/FULL_OCCU/Test4plus1_66V4"); 
  sprintf(canvas_dir  ,"/home/mri-pet/Desktop/ana/Xtals/FULL_OCCU/"); 
  PeakParameter = 0.05;   
  */

  gErrorIgnoreLevel = kWarning;
  Double_t CRT = -999.; 
   
  char tothistos[80]; 
  sprintf(tothistos,"%s%s",fileout_path,"_ToThistos.root");
  char filetotcuts[80]; 
  sprintf(filetotcuts,"%s%s",fileout_path,"_ToTcuts.txt"); 
  char tree_evtout[80]; 
  sprintf(tree_evtout,"%s%s",fileout_path,"_EvtTree.root");
  char crthistos[80]; 
  sprintf(crthistos,"%s%s",fileout_path,"_CRThistos.root");

  cout << " * * * * * * * * * * * * * * * * * * * * * * * * * " << endl; 
  cout << " Processing " << filein << endl;  
  cout << " Files output will be " << fileout_path << "*" << endl;  
  cout << " * * * * * * * * * * * * * * * * * * * * * * * * * " << endl; 

  Int_t MinNrEntries=100;   // Entries in ToT plot, to be able to perform fit at photopeak
  Double_t Nsigmas=1.;      // Nr of sigmas around photopeak to build cut on events 
  Int_t MinStatPair = 100;  // Minimum stat in TimeDiff plot to perform CRT fit

  //Int_t Nsingles; 
  //Int_t Npairs[4]; 

  /*
  Int_t iToT = 1; 
  Int_t iCRT = 0; 
  Int_t iPlot = 0; 
  */
  if (iToT == 1) { 
    // read tree => "ToT_histos.root"  
    GetToThistos_simple(filein,tothistos); 
    cout << " ... Produced  " << tothistos << endl;    
    
    // photopeak cuts (from ToT histos) 
    GetPhotopeakCuts(tothistos,MinNrEntries,PeakParameter,filetotcuts, canvas_dir); 
    cout << " ... Defined PP cuts. Produced  " << filetotcuts << endl;  
 
    // global histograms
    GetGlobalHistos(tothistos,canvas_dir);    
  }
  

  if (iCRT == 1 ) { 
    // build coincidence events  
    BuildEventTreeSelection(filein,tree_evtout,filetotcuts,Nsigmas); 
    cout << " ... Produced  " << tree_evtout << endl;  
      
    // counts 
    GetCountings(tothistos,tree_evtout,canvas_dir);
    
    // crt results
    CRT = GetCRTmatrix3(tree_evtout, MinStatPair, crthistos, Ch0, Ch1, canvas_dir); 
  }


  if (iPlot == 1) { 
    //-- plot results 
    PlotsTreeMultiplicity(tree_evtout, canvas_dir); 
    PlotsTreeOccupancy(tree_evtout, canvas_dir); 
    PlotsTree2HitsSelectionNew(tree_evtout, canvas_dir);  
    
    Int_t Ch1st_ASIC0 = 16;   //  0 - 16 - 32 - 48
    Int_t Ch1st_ASIC1 = 96;  // 64 - 80 - 96 - 112
    //DrawCRT(crthistos,Ch1st_ASIC0,Ch1st_ASIC1); 
    //for (int i = 0 ; i<64; i=i+16){DrawCRT(crthistos,i,96); }
    
  }
  return CRT; 
 
}
