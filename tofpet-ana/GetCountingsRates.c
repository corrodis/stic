void GetCountingsRates(char dire[80]="./09Dec14_500M/",
		       Int_t RunID=5,  		       
		       Double_t run_duration=100) {
  
  char totfile[80]; 
  sprintf(totfile,"%s/Run%d/_ToThistos.root",dire,RunID); 
  char filein[80]; 
  sprintf(filein,"%s/Run%d/_EvtTree.root",dire,RunID); 


  TCanvas *c_count = new TCanvas("cCountings","cCountings",500,500); 
  c_count->Divide(2,2);
  
  // All hits counting 
  TFile *f = new TFile(totfile); 
  TH1F *h_all = f->Get("h_chan");// Nr hits in total
  c_count->cd(1); 
  h_all->Draw(); 

  // Hits at photopeak 
  TFile *fevt = new TFile(filein); 
  TTree *tree = (TTree*)fevt->Get("EventTree"); 
  c_count->cd(2); 
  tree->Draw("Chan>>h_chanPP(128,0,128)"); // Nr hits at photopeak  

  // Hits at photopeak and in coincidence 
  c_count->cd(3); 
  tree->Draw("Chan>>h_chanPPCoinc(128,0,128)","Nhits==2"); //Nr hits in coincidence 


  Double_t Rate[2], RatePP[2], RatePPcoinc[2]; 
  Double_t min[2], max[2]; 
  for (int k=0; k<2; k++) {  
    Rate[k]=0; 
    RatePP[k]=0;
    RatePPcoinc[k]=0;
    if (k==0) { 
      min[k]=0;
      max[k]=64; 
    }
    else {
      min[k]=64;
      max[k]=150; 
    }
  }
  
  
  for (int k=0; k<2; k++) {  
    h_all->Fit("pol0","","q",min[k],max[k]); 
    Rate[k]=pol0->GetParameter(0)/run_duration; 
    
    h_chanPP->Fit("pol0","","q",min[k],max[k]); 
    RatePP[k]=pol0->GetParameter(0)/run_duration; 
    
    h_chanPPCoinc->Fit("pol0","","q",min[k],max[k]); 
    RatePPcoinc[k]=pol0->GetParameter(0)/run_duration; 
  }
  char result[80]; 
  sprintf(result, "%d %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f \n", 
	  RunID , Rate[0], RatePP[0], RatePPcoinc[0],  Rate[1], RatePP[1], RatePPcoinc[1]) ;  
  cout << result << endl;
  
}
