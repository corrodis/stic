import ROOT
f = ROOT.TFile('scan_1-28-18-_s_sort.root')
t = f.Get("lmData")

h1 = ROOT.TH1I("dT","dT",40,-1000,1000)
h2 = ROOT.TH1I("dT2","dT2",40,-1000,1000)
h3 = ROOT.TH2I("h3","h3",500,0,500,500,0,500)
h4 = ROOT.TH2I("h4","h4",500,0,500,500,0,500)
h5 = ROOT.TH1I("h5","h5",100,-50,50)



for i in range(t.GetEntries()):
    #if( i%100000 == 0):
    #    print "read: "+str(100.0*i/t.GetEntries())+"%"
    t.GetEntry(i)
    t0 = t.time
    ch0 = t.channel
    e0 = t.tot
    rawId0 = t.rawId
    t.GetEntry(i+1)
    t1 = t.time
    ch1 = t.channel
    e1 = t.tot
    rawId1 = t.rawId
    #if (t1-t0)==50 and ch0 in [30, 48] and ch1 in [30, 48] and ch0 != ch1: 
    #    print t1-t0, ch0, ch1
    #    n = n + 1
    if ch0 in [30, 48] and ch1 in [30, 48] and ch0 != ch1:
        h1.Fill(t1-t0)
        if (t1-t0)==0:
            h3.Fill(e0,e1)
        
    if ch0 in [30, 48] and ch1 in [30, 48] and ch0 == ch1:
        h2.Fill(t1-t0)
        if (t1-t0)==0:
            h4.Fill(e0,e1)
            h5.Fill(e0-e1)
            if(e0 == e1):
                print rawId0, rawId1
            

h1.GetXaxis().SetTitle("deltaT [ps]");
h1.SetTitle("dT ch0 != ch1");
h2.GetXaxis().SetTitle("deltaT [ps]");
h2.SetTitle("dT ch0 == ch1");

h3.GetXaxis().SetTitle("ToT ch0");
h3.GetYaxis().SetTitle("ToT ch0");
h3.SetTitle("energy distribution: ch0 != ch1");

h4.GetXaxis().SetTitle("ToT ch0");
h4.GetYaxis().SetTitle("ToT ch0");
h4.SetTitle("energy distribution: ch0 == ch1");

