#include "ReadSingleVariables.h" 
void GetToThistos_simple(char filename[130] = "scan_1-28-18-_s_sort.root", char fileout[80]="ToT_histos.root"){

  // ----------------------------------------
  // INPUT TREE 
  TFile *f = new TFile(filename); 
  TTree *lmData = (TTree*)f->Get("lmData");
  // Branches 
  lmData->SetBranchAddress("time",&time);
  lmData->SetBranchAddress("channel",&channel);
  lmData->SetBranchAddress("tot",&tot);
  lmData->SetBranchAddress("tqT",&tqT);
  lmData->SetBranchAddress("tqE",&tqE);
  // ----------------------------------------
  
  // ----------------------------------------  
  // HISTOGRAMS DECLARATION 
  char tit[80]; 
  TH1F *hToT[128]; 
  for (int k=0;k<128;k++) {    
    sprintf(tit,"ToT Channel%d",k); 
    hToT[k]=new TH1F(tit,tit,150,-100,700);
  }
  TH1F *h_tot1 = new TH1F("h_tot1","ToT0;ToT chann[0,63];nr counts"  ,1200,-200,1000);
  TH1F *h_tot2 = new TH1F("h_tot2","ToT1;ToT chann[64,127];nr counts",1200,-200,1000);
  TH1F *h_chan = new TH1F("h_chan","Channel;ch;nr counts",128,0,128); 
  TH2F *h_map1 = new TH2F("h_map_asic0","h_map_asic0",16,0.5,16.5,4,0.5,4.5); 
  TH2F *h_map2 = new TH2F("h_map_asic1","h_map_asic1",16,0.5,16.5,4,0.5,4.5);
  // ----------------------------------------


  Int_t XX,YY; 
   
  // ----------------------------------------------  
  // * * *  R E A D    I N P U T    T R E E   * * * 
  for (int i=0;i<lmData->GetEntries(); i++) { 
    lmData->GetEntry(i); 
    
    XX=xmap[channel%64]; 	
    YY=ymap[channel%64];
    
    // general control histograms
    h_chan->Fill(channel); 
    if (channel<64) {
      h_tot1->Fill(tot);
      h_map1->Fill(XX,YY);       
    } 
    else {
      h_tot2->Fill(tot);
      h_map2->Fill(XX,YY);       
    } 
    hToT[channel]->Fill(tot); 
  }

  // -------------------------------------------------------  
  // * * *  W R I T E    H I S T O S  * * * 
  
  TFile *fout = new TFile(fileout,"RECREATE"); 
  for (int k=0;k<128;k++) {    
    hToT[k]->Write();
  }
  h_tot1->Write();
  h_tot2->Write();
  h_chan->Write();
  h_map1->Write();
  h_map2->Write();
 
  fout->Close(); 



}
