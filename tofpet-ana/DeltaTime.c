#include "ReadSingleVariables.h" 
void DeltaTime(char filename[130] = "/home/mri-pet/Documents/TOFPET/ekit-sw-2/data/Xtals/AGILE_8x8/root/XtalMatrx_259_258_66V9_short_s.root"){
   // ----------------------------------------
  // INPUT TREE 
  TFile *f = new TFile(filename); 
  TTree *lmData = (TTree*)f->Get("lmData");
  // Branches 
  lmData->SetBranchAddress("time",&time);

  Long64_t time0=0;
  Long64_t deltatime=0;

  TH1F *h = new TH1F("h","DeltaTime",6000,-10000000,50000000); 

  for (int i=0;i<lmData->GetEntries(); i++) { 
    lmData->GetEntry(i); 
    deltatime = time - time0; 
    time0 = time; 
    if (i>0) h->Fill(deltatime); 
    if (i<100) cout << i << " " << time << " "<< deltatime << endl; 
  }
  TCanvas *c = new TCanvas("c","C",700,700); 
  c->SetLogy();
  h->Draw();
      
}
