#include <iostream> 
#include "OperationalVariables.h" 

void PlotsTree2HitsSelectionNew(char filein[80]="EventSelection.root",
				char savecanvdir[80]="./CanvasDir"){ 
  TFile *f = new TFile(filein); 
  TTree *tree = (TTree*)f->Get("EventTree"); 
  
  gStyle->SetPalette(1); 
  // 2 hits events 
  
  TCanvas *c3 = new TCanvas("c2hits","c2hits",1000,900); 
  c3->Divide(2,2);
  
  c3->cd(1); 
  tree->Draw("TovT[0]:TovT[1]>>h(250,-50,400,250,-50,400)","Nhits==2","col");
    
  c3->cd(2); 
  tree->Draw("Chan[0]:Chan[1]>>h2(128,0,128,128,0,128)","Nhits==2","col");
 
  TCut DiffAsic="((Chan[0]>63 && Chan[1]<64)||(Chan[1]>63 && Chan[0]<64))";
  TCut SameAsic="((Chan[0]>63 && Chan[1]>63)||(Chan[0]<64 && Chan[1]<64))";
  TCut DiffAsic10= "Chan[0]>63 && Chan[1]<64"; //Good2
  TCut DiffAsic01= "Chan[1]>63 && Chan[0]<64"; //Good1
  TCut SameAsic00= "Chan[0]<64 && Chan[1]<64"; //Bad1
  TCut SameAsic11= "Chan[0]>63 && Chan[1]>63"; //Bad2

  c3->cd(3); 
  tree->Draw("Chan[0]:Chan[1]>>h3(128,0,128,128,0,128)","Nhits==2" && DiffAsic,"col"); 
 
  c3->cd(4); 
  tree->Draw("Chan[0]:Chan[1]>>h4(128,0,128,128,0,128)","Nhits==2" && SameAsic,"col"); 

  // ------------------- 
  TCanvas *c4 = new TCanvas("c2hitsBAD_GOOD","c2hitsBAD_GOOD",1000,900); 
  c4->Divide(2,2);
  
  c4->cd(1); 
  tree->Draw("Chan[0]:Chan[1]>>hk1(128,0,128,128,0,128)","Nhits==2" && DiffAsic01,"col"); 
 
  c4->cd(2); 
  tree->Draw("Chan[0]:Chan[1]>>hk2(128,0,128,128,0,128)","Nhits==2" && DiffAsic10,"col"); 

  c4->cd(3); 
  tree->Draw("Chan[0]:Chan[1]>>hk3(128,0,128,128,0,128)","Nhits==2" && SameAsic00,"col"); 
  
  c4->cd(4); 
  tree->Draw("Chan[0]:Chan[1]>>hk4(128,0,128,128,0,128)","Nhits==2" && SameAsic11,"col"); 
 
  // ------------------- 
  TCanvas *c5 = new TCanvas("c2hitsBAD_GOOD_Stat","c2hitsBAD_GOOD_Stat",1000,900); 
  TH1F *hstatG1 = new TH1F("hstatG1","hstatG1",16384,0,16384);
  TH1F *hstatG2 = new TH1F("hstatG2","hstatG2",16384,0,16384);
  TH1F *hstatB1 = new TH1F("hstatB1","hstatB1",16384,0,16384);
  TH1F *hstatB2 = new TH1F("hstatB2","hstatB2",16384,0,16384);
  
  c5->Divide(2,2);
  for (int i=1;i<129;i++) { 
    for (int j=1;j<129;j++) { 
      hstatG1->Fill(i+128*j,hk1->GetBinContent(i,j)); 
      hstatG2->Fill(i+128*j,hk2->GetBinContent(i,j)); 
      hstatB1->Fill(i+128*j,hk3->GetBinContent(i,j)); 
      hstatB2->Fill(i+128*j,hk4->GetBinContent(i,j)); 
    }
  }
  
  c5->cd(1); hstatG1 ->Draw(); hstatG1->SetLineColor(1); 
  c5->cd(2); hstatG2 ->Draw(); hstatG2->SetLineColor(4); 
  c5->cd(3); hstatB1 ->Draw(); hstatB1->SetLineColor(2); 
  c5->cd(4); hstatB2 ->Draw(); hstatB2->SetLineColor(3); 

  char csave[80];						
  sprintf(csave,"%s%s",savecanvdir,"/cTwoHitsCorr.root"); 
  c3->Print(csave); 
  sprintf(csave,"%s%s",savecanvdir,"/cTwoHitsStatPairs.root"); 
  c5->Print(csave); 
  sprintf(csave,"%s%s",savecanvdir,"/cTwoHitsStatPairs2D.root"); 
  c4->Print(csave); 
  

  //-----------------------
  TCanvas *cstat = new TCanvas("cstat","cstat",1000,400); 
  
  cstat->DrawFrame(0,0,16384,2000); 
  hstatG1 ->Draw("same"); 
  hstatG2 ->Draw("same"); 
  hstatB1 ->Draw("same"); 
  hstatB2 ->Draw("same"); 

  /*
  TLegend *le = new TLegend(0.7, 0.8, 0.9, 0.9); 
  le->AddEntry(hstatG1,"ASIC01 (good1)");  //,"l"); 
  le->AddEntry(hstatG1,"ASIC10 (good2)");  //,"l"); 
  le->AddEntry(hstatG1,"ASIC00 (bad1)");   //,"l"); 
  le->AddEntry(hstatG1,"ASIC11 (bad2)");   //,"l"); 
  le->Draw();
  */
  
}
