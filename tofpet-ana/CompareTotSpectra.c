void CompareTotSpectra(Int_t Chan=107, const Int_t nt =10){ 
  //const Int_t nt =10; 
  Int_t Run0=17; 
  char hname_chan[180]; 

  TH1F *h_chan[nt]; 
  TFile *f[nt]; 
  TTree *T[nt]; 
  TLegend *l92 = new TLegend(0.68,0.68,1,1); 
  l92->SetFillColor(0); 
  
  char filename[180][nt];
  for (int k=0;k<nt;k++) { 
    sprintf(filename[k],"/data/tofpet/HighRateTest/Setup_4_1/root/Run%d_s.root",Run0+k); 
    
    f[k] = new TFile(filename[k]); 
    T[k]= (TTree*)f[k]->Get("lmData"); 
    char cut1[80];
    sprintf(cut1,"channel==%d",Chan); 
    sprintf(hname_chan,"Channel%d_Run%d",Chan,Run0+k); 
    //
    T[k]->Draw("tot>>h1(1000,-300,700)",cut1);
    h_chan[k] = (TH1F*)h1->Clone(hname_chan);
    h_chan[k]->SetLineColor(k+1); 
    
    l92->AddEntry(h_chan[k],hname_chan); 
  }

  TCanvas *c = new TCanvas("c","c",800,780); 
  c->cd(1)->SetLogy();
  h_chan[0]->Draw("hist"); 
  for (int k=1;k<nt;k++) {
    if ((Run0+k)!=25) {  
      h_chan[k]->Draw("same hist");
    }
  }
  l92->Draw(); 
}
