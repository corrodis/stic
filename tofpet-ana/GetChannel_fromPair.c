#include "OperationalVariables.h"
#include "CheckConsistency.c" 
#include "ChannelID.c" 
Int_t GetChannel_fromPair(Int_t ipair=10, Int_t iASIC=0){
  
  if (!CheckConsistency()) { 
    cout << "  -- ERROR -- Mismatch in the 'OperationalVariables.h' File " << endl; 
    cout << "  -- Macro aborted. " << endl; 
    break; 
  }

  const Int_t Nchanns_ASIC0 = Narray0*16; 
  const Int_t Nchanns_ASIC1 = Narray1*16; 
  Int_t chASIC0[Nchanns_ASIC0],chASIC1[Nchanns_ASIC1];
  ChannelID(0,chASIC0);   
  ChannelID(1,chASIC1);

  Int_t Channel;

  for (int i=0;i<Nchanns_ASIC0;i++) { 
    for (int j=0;j<Nchanns_ASIC1;j++) {
      Int_t PairID = i*Nchanns_ASIC1+j ;
      //
      if (PairID==ipair) { 
	//cout << i << " " << j << " " << PairID << endl; 	
	if (iASIC==0) return chASIC0[i]; 
	if (iASIC==1) return chASIC1[j]; 
      }
    }
  }
  

  //return Channel;
  
}
