// ----------------------------------------
// Variables
//
//Double_t CoincWnd = 10*1.0e+06;
//Double_t CoincWnd = 100000; //100ns
//Double_t CoincWnd = 10000; //10ns
//Double_t CoincWnd = 1000; //1ns
Double_t CoincWnd = 10e3; // in ns


const Int_t MaxEvtNumber = 5000000;
const Int_t MaxEvtSize = 40;
const Int_t NChannels = 128;

// ----------------------------------------
// Declaration of leaf types
Float_t         step1;
Float_t         step2;
Long64_t        time;
UShort_t        channel;
Float_t         tot;
UShort_t        tac;
Double_t        channelIdleTime;
Double_t        tacIdleTime;
Float_t         tqT;
Float_t         tqE;
UShort_t        rawtCoarse;
UShort_t        rawtFine;
UShort_t        raweCoarse;
UShort_t        raweFine;

//------------------------------------------
// --- M A P  ---
Int_t xmap[64]={1,1,2,2,3,3,4,4,2,4,3,1,1,2,4,3,5,5,6,6,5,5,6,7,6,8,7,7,8,8,7,8,9,10,9,9,10,10,9,11,10,11,12,12,11,11,12,12,14,13,13,15,16,16,14,15,13,13,14,14,15,15,16,16};
Int_t ymap[64]={3,4,4,3,3,4,4,3,2,2,2,2,1,1,1,1,3,4,4,3,2,1,1,2,2,1,1,3,2,4,4,3,3,4,4,2,3,1,1,2,2,1,1,2,3,4,4,3,1,1,2,1,1,2,2,2,3,4,4,3,4,3,4,3};

