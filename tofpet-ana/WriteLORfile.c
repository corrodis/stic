#include "ReadSingleVariables.h" 
#include <iostream> 
void WriteLORfile(char filename[80]="EventSelection.root",
		  char LORfile[80]="LOR.txt"){
  
  TFile *f = new TFile(filename); 
  TTree *ETree = (TTree*)f->Get("EventTree"); 
  
  // Branches 
  Int_t    EvtID; 
  Int_t    Nhits; 
  Float_t  TovT[MaxEvtSize] ;
  Int_t    Chan[MaxEvtSize] ; 
  Int_t    X[MaxEvtSize] ; 
  Int_t    Y[MaxEvtSize] ; 
  ETree->SetBranchAddress("EvtID",&EvtID);
  ETree->SetBranchAddress("Nhits",&Nhits);
  ETree->SetBranchAddress("TovT",&TovT);
  ETree->SetBranchAddress("Chan", &Chan);
  ETree->SetBranchAddress("X", &X);
  ETree->SetBranchAddress("Y", &Y);
  
  ofstream fout; 
  fout.open(LORfile); 
  
  
  char result[80]; 
  Float_t tot[2]; 
  Int_t mod[2],xtal[2]; 
  for (int k=0; k<2;k++) {
    mod[k]=-1;
    xtal[k]=-1;
    tot[k]=-1;
  }
  for (int i=0; i<EventTree->GetEntries(); i++) { 
    ETree->GetEntry(i);
    
    if (Nhits==2) { 
      // module 
      if (Chan[0]<64) mod[0] = (X[0]-1)/4; 
      if (Chan[1]<64) mod[1] = (X[1]-1)/4; 
      if (Chan[0]>63) mod[0] = 7-(X[0]-1)/4; 
      if (Chan[1]>63) mod[1] = 7-(X[1]-1)/4; 
      
      // crystal 
      if (Chan[0]<64) xtal[0] = ((X[0]-1)%4)*4 + (Y[0]-1);
      if (Chan[1]<64) xtal[1] = ((X[1]-1)%4)*4 + (Y[1]-1);
      if (Chan[0]>63) xtal[0] = (3-(X[0]-1)%4)*4+(Y[0]-1); 
      if (Chan[1]>63) xtal[1] = (3-(X[1]-1)%4)*4+(Y[1]-1); 

      if (EvtID<1000) { 
	sprintf(result,
		"%5d %5d %5d %5d %5d %5d %5d %5d %5d %7.2f %5d %5d %7.2f", 
		EvtID, Chan[0], Chan[1], X[0], Y[0], X[1], Y[1], mod[0], xtal[0], TovT[0],  mod[1], xtal[1], TovT[1]) ;  
	cout << result << endl; 
      }

      sprintf(result,
	      "%5d %5d %5d %7.2f %5d %5d %7.2f \n", 
	      EvtID, mod[0], xtal[0], TovT[0],  mod[1], xtal[1], TovT[1]) ;  
   
      fout << result ; 
    }
    
  }
  fout.close(); 
}
