void GetRatesRun(Int_t RunID=1,Int_t RunFull=0,   		       
		 Double_t run_duration=10) {

  char totfile[80]; 
  if (RunFull==0) { 
    sprintf(totfile,"./Run%d/_ToThistos.root",RunID); 
    cout << " * * * * Run"<< RunID<< "* * * *"<<endl; 
  }
  if (RunFull==1) { 
    sprintf(totfile,"./Full%d/_ToThistos.root",RunID); 
    cout << " * * * * FullLoad"<< RunID<< "* * * *"<<endl; 
  }
  
  //
  TFile *f = new TFile(totfile); 
  TH1F *h_all = f->Get("h_chan");// Nr hits in total

  Double_t Rate[2];
  Double_t min[2], max[2]; 
  for (int k=0; k<2; k++) {  
    Rate[k]=0; 
    if (k==0) { 
      min[k]=16;
      max[k]=32; 
    }
    else {
      min[k]=96;
      max[k]=112; 
    }
  }

  for (int k=0; k<2; k++) {  
    h_all->Fit("pol0","","q",min[k],max[k]); 
    Rate[k]=pol0->GetParameter(0)/run_duration; 
  }
  h_all->Draw(); 


  Double_t aveall0;
  Int_t single1; 
  if (RunFull==1) {
    h_all->Fit("pol0","","q",0.,64.);
    aveall0 = pol0->GetParameter(0)/run_duration; 
    single1 = (h_all->GetBinContent(93))/run_duration;
  }
  else { 
    aveall0 = 0; 
    single1 = 0; 
  }
  
  char result[80]; 
  sprintf(result, "%d %d %5.2f %5.2f %5.2f %5.2f\n", 
	  RunID , RunFull, Rate[0], Rate[1], aveall0, single1) ;  
  cout << result << endl;
  
  
  h_all->Draw(); 
  
}
