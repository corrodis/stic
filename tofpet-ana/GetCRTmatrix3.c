#include "ReadSingleVariables.h" 
#include "OperationalVariables.h"
#include "ChannelID.c" 
#include "CheckConsistency.c"
#include "GetIndexArray.c" 

Double_t GetCRTmatrix3(char filein[80]="EventSelection.root",
               Int_t MinStatCRThisto = 10,
		       char fileout[80]="CRT_histos.root",
               Int_t Ch0=-1, Int_t Ch1=-1,
		       char savecanvdir[120]="./CanvasDir",
               Double_t FitCRTminstat=1) {

  if (!CheckConsistency()) { 
    cout << "  -- ERROR -- Mismatch in the 'OperationalVariables.h' File " << endl; 
    cout << "  -- Macro aborted. " << endl; 
    break; 
  }

  // - - - - - - - - - - - - - - - - - -  
  Double_t CRT = -999.; 
  // - - - - - - - - - - - - - - - - - -  
  char hname[80]; 
  char htit[80]; 
   
  const Int_t Nchanns_ASIC0 = Narray0*16; 
  const Int_t Nchanns_ASIC1 = Narray1*16; 

  Int_t chASIC0[Nchanns_ASIC0],chASIC1[Nchanns_ASIC1];
  ChannelID(0,chASIC0);   
  ChannelID(1,chASIC1); 
  
  TH1F *hpair[Nchanns_ASIC0][Nchanns_ASIC1]; 
  for (int i=0;i<Nchanns_ASIC0;i++) { 
    for (int j=0;j<Nchanns_ASIC1;j++) { 
      sprintf(hname,"Pair_%d_%d",chASIC0[i],chASIC1[j]);  
      sprintf(htit,"CRT Pair_%d_%d;Time chASIC0 - chASIC1 [ps]",chASIC0[i],chASIC1[j]);  
      hpair[i][j]=new TH1F(hname,htit,201,-5000,5000);
      //hpair[i][j]=new TH1F(hname,htit,50000,-500000000,500000000);
      //cout << i << " " << j << " " << hname << endl; 
    }
  }
  
  TFile *f = new TFile(filein); 
  TTree *ETree = (TTree*)f->Get("EventTree");
 
  // Branches 
  Int_t    EvtID; 
  Int_t    Nhits; 
  Long64_t Time[MaxEvtSize] ;
  Int_t    Chan[MaxEvtSize] ; 
  ETree->SetBranchAddress("EvtID",&EvtID);
  ETree->SetBranchAddress("Nhits",&Nhits);
  ETree->SetBranchAddress("Time", &Time);
  ETree->SetBranchAddress("Chan", &Chan);
  
  
  Int_t iASIC0,jASIC1; 
  Long64_t Dtime=0; //Dtime = TimeASIC0 - TimeASIC1 
  
  //cout << EventTree->GetEntries() << endl; 
  for (int i=0; i<EventTree->GetEntries(); i++) { 
    ETree->GetEntry(i);
    
    if (Nhits==2) { 
        if(Chan[0] < Chan[1])
            Dtime = Time[0]-Time[1];
        else
            Dtime = Time[1]-Time[0];
        hpair[0][0]->Fill(Dtime);
     /*if (Chan[0]<64 && Chan[1]>63)  { //ch0 in ASIC0; ch1 in ASIC1
	iASIC0 = GetIndexArray(chASIC0,Nchanns_ASIC0,Chan[0]); //0...63
	jASIC1 = GetIndexArray(chASIC1,Nchanns_ASIC1,Chan[1]); //0...63 	
	Dtime = Time[0]-Time[1]; //TimeASIC0 - TimeASIC1 
      }
      else if (Chan[0]>63 && Chan[1]<64)  { //ch0 in ASIC1; ch1 in ASIC0 
	iASIC0 = GetIndexArray(chASIC0,Nchanns_ASIC0,Chan[1]);
	jASIC1 = GetIndexArray(chASIC1,Nchanns_ASIC1,Chan[0]);  
	Dtime = Time[1]-Time[0]; //TimeASIC0 - TimeASIC1 
      }
      else if (Chan[0]<63 && Chan[1]<64)  { //ch0 in ASIC0; ch1 in ASIC0
    iASIC0 = GetIndexArray(chASIC0,Nchanns_ASIC0,Chan[0]);
    jASIC1 = GetIndexArray(chASIC0,Nchanns_ASIC0,Chan[1]);
    if(Chan[0] < Chan[1])
        Dtime = Time[0]-Time[1];
    else
        Dtime = Time[1]-Time[0];
      }
      else { 
	iASIC0 = -99;
	jASIC1 = -99;
      }
      //
      if (iASIC0>=0 && jASIC1>=0){ 
    hpair[0][0]->Fill(Dtime);
      }*/
    }
  }

  // ---- 

  // -------- 
  if (Ch0==-1 && Ch1==-1 && false) { // Analysis on all channels

    Double_t mean[Nchanns_ASIC0*Nchanns_ASIC1];
    Double_t sigma[Nchanns_ASIC0*Nchanns_ASIC1];
    Double_t stat[Nchanns_ASIC0*Nchanns_ASIC1]; 
    Double_t ID[Nchanns_ASIC0*Nchanns_ASIC1]; 

    Int_t k=0; 
   
    //Fit all histos 
    TCanvas *c_dummy = new TCanvas("c_dummy","c_dummy",400,400); 
    for (int i=0 ; i<Nchanns_ASIC0; i++ ) { 
      for (int j=0 ; j<Nchanns_ASIC1; j++) {
	c_dummy->cd(); 
	hpair[i][j]->Draw(); 
	Double_t mea = hpair[i][j]->GetMean(); 
	Double_t rms = hpair[i][j]->GetRMS(); 
	if (hpair[i][j]->GetEntries()>0) hpair[i][j]->Fit("gaus","q","",mea-1.1*rms,mea+1.1*rms); 
	
	if (hpair[i][j]->GetEntries()>MinStatCRThisto) { 
	  Int_t PairID = i*Nchanns_ASIC1+j ;
	  ID[k] = PairID; 
	  stat[k] = hpair[i][j]->GetEntries(); 
	  mean[k] = gaus->GetParameter(1); 
	  sigma[k]= gaus->GetParameter(2);
	  //mean[k] = hpair[i][j]->GetMean();
	  //sigma[k]= hpair[i][j]->GetRMS();
	  k++; 
	}
      }
    }

    char csave[80]; 
    char ctit[80]; 
    char tit[80]; 

  
    // ---------------------------------------------    
    cout << "NPairs with Statistics > Minstat(="<<MinStatCRThisto <<") = " << k << endl; 
    TCanvas *c_CRTres = new TCanvas("CRTres","CRTres",1200,900); 
    c_CRTres->Divide(2,2); 
    
    const Int_t Ng=4; 
    TGraph *gsummary[Ng]; 
    Int_t Size = k; 
  
    gsummary[0] = new TGraph(Size,ID,stat);
    gsummary[1] = new TGraph(Size,ID,mean);
    gsummary[2] = new TGraph(Size,ID,sigma);
    gsummary[3] = new TGraph(Size,stat,sigma);
    char title[Ng][80] = {"CRT statistics", 
			  "DeltaTime mean value", 
			  "CRT sigma [ps]",
			  "CRT VS stat"};  
    
    Double_t Amax[3]; 
    Double_t Amin[3];
    Amax[0]=stat[TMath::LocMax(Size,stat)]; 
    Amin[0]=0; //stat[TMath::LocMin(Size,stat)]; 
    Amax[1]=mean[TMath::LocMax(Size,mean)]; 
    Amin[1]=mean[TMath::LocMin(Size,mean)]; 
    Amax[2]=sigma[TMath::LocMax(Size,sigma)]; 
    Amin[2]=0;//sigma[TMath::LocMin(Size,sigma)]; 
    
    //cout << Amax[0] << " " << Amax[1] << " " << Amax[2] << endl; 
    //cout << Amin[0] << " " << Amin[1] << " " << Amin[2] << endl; 
    
    TLine *l[3][16]; 
    
    for (int ic=0 ; ic<3; ic++ ) { 
      gsummary[ic]->SetMarkerStyle(20);     
      
      TH1F *hframe = c_CRTres->cd(ic+1)->DrawFrame(0.,0.9*Amin[ic],Nchanns_ASIC0*Nchanns_ASIC1,1.1*Amax[ic]);
      c_CRTres->cd(ic+1)->SetGrid();
      hframe->Draw(); 
      sprintf(ctit,"%s;PairID = chASIC0*Nchanns_ASIC1 + chASIC1",title[ic]); 
      hframe->SetTitle(ctit); 
      
      gsummary[ic]->Draw("P same");
      
      
      for (int k=0;k<16;k++) { 
	l[ic][k] = new TLine(k*16,0.9*Amin[ic],k*16,1.1*Amax[ic]); 
	//l[ic][k]->Draw("same");
      } 
    }
  
    c_CRTres->cd(4); 
    gsummary[3]->SetMarkerStyle(20);
    sprintf(ctit,"CRT vs Stat ; Statistics ; CRT sigma [ps]");  
    gsummary[3]->SetTitle(ctit); 
    gsummary[3]->Draw("AP");
    
    sprintf(csave,"%s%s%s",savecanvdir, "/", "CRTsummary.root" ); 
    c_CRTres->Print(csave); 
  
    gsummary[3]->Fit("pol0","","",FitCRTminstat,100000); 
    CRT = pol0->GetParameter(0); 
    
  }//if (Ch0==-1 && Ch1==-1) {

  else { // Analysis for ONE DEFINED PAIR ONLY
    char cname[80];
    sprintf(cname,"CRT_Channs_%d_%d",Ch0,Ch1); 
    TCanvas *c_pair = new TCanvas(cname,cname,700,500); 

    Int_t i0 = 0; //GetIndexArray(chASIC0,Nchanns_ASIC0,Ch0); //0...63
    Int_t j1 = 0; //GetIndexArray(chASIC0,Nchanns_ASIC0,Ch1); //0...63

    hpair[i0][j1]->Draw(); 
    Double_t mea = hpair[i0][j1]->GetMean(); 
    Double_t rms = hpair[i0][j1]->GetRMS(); 
    hpair[i0][j1]->Fit("gaus","q","",mea-1.1*rms,mea+1.1*rms); 
    hpair[i0][j1]->Draw(); 
    CRT = gaus->GetParameter(2); 
    cout << "CRT: " << CRT << endl;
    cout << "integral: " << hpair[i0][j1]->Integral(hpair[i0][j1]->GetXaxis()->FindBin(gaus->GetParameter(1) - 3 * gaus->GetParameter(2)), hpair[i0][j1]->GetXaxis()->FindBin(gaus->GetParameter(1) + 3 * gaus->GetParameter(2))) << endl;
  }
  
  // ----- 
  // write out file CRT histos 
  TFile *hout = new TFile(fileout,"RECREATE"); 
  
  for (int i=0 ; i<Nchanns_ASIC0; i++ ) { 
    for (int j=0 ; j<Nchanns_ASIC1; j++) {
      hpair[i][j]->Write(); 
    }
  }
  hout->Close(); 
  // ---- 
 

  return CRT; 

}
