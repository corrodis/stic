void GetCountings(char totfile[80]="ToT_histos.root",
		  char filein[80]="EventSelection.root",
		  char savecanvdir[80]="./CanvasDir"){ 

  TCanvas *c_count = new TCanvas("cCountings","cCountings",1000,900); 
  c_count->Divide(3,3);
  
  // All hits counting 
  TFile *f = new TFile(totfile); 
  TH1F *h_all = f->Get("h_chan");// Nr hits in total
  TH1F *h2 = h_all->Clone();  h2->Rebin(16); // nr hits per array
  TH1F *h3 = h_all->Clone();  h3->Rebin(64); // nr hits per ASIC
  c_count->cd(1); h_all->Draw(); 
  c_count->cd(2); h2->Draw(); 
  c_count->cd(3); h3->Draw(); 
  h_all->SetTitle("Channel Occupancy"); 
  h2->SetTitle("Array Occupancy"); 
  h3->SetTitle("ASIC Occupancy"); 
  //f->Close(); 

  // Hits at photopeak 
  TFile *fevt = new TFile(filein); 
  TTree *tree = (TTree*)fevt->Get("EventTree"); 
  c_count->cd(4); tree->Draw("Chan>>h_chanPP(128,0,128)"); // Nr hits at photopeak  
  c_count->cd(5); tree->Draw("Chan>>h5(8,0,128)"); 
  c_count->cd(6); tree->Draw("Chan>>h6(2,0,128)"); 
  h_chanPP->SetTitle("Channel Occupancy - PP"); 
  h5->SetTitle("Array Occupancy - PP"); 
  h6->SetTitle("ASIC Occupancy - PP "); 
  
  // Hits at photopeak and in coincidence 
  c_count->cd(7); tree->Draw("Chan>>h_chanPPCoinc(128,0,128)","Nhits==2"); //Nr hits in coincidence 
  c_count->cd(8); tree->Draw("Chan>>h8(8,0,128)","Nhits==2"); 
  c_count->cd(9); tree->Draw("Chan>>h9(2,0,128)","Nhits==2"); 
  h_chanPPCoinc->SetTitle("Channel Occupancy -PP/ in coinc");
  h8->SetTitle("Array Occupancy -PP/ in coinc"); 
  h9->SetTitle("ASIC Occupancy -PP/ in coinc"); 

  char csave[80]; 
  sprintf(csave,"%s%s",savecanvdir,"/cCountings.root"); 
  c_count->Print(csave); 
}
