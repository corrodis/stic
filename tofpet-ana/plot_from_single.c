void plot_from_single(char file[180]="../data/HighRateTest/LowActivity/root/interactive_4_73MBq_1+1_Vm0p3_Toffset-14_E5_1000sec_s.root"){ 
  
  TFile *f = new TFile(file); 
  
  TTree *t = f->Get("lmData"); 
  
  TCanvas *c = new TCanvas("c","cc",600,800); 
  c->Divide(2,2); 
  c->cd(1); 
  t->Draw("channel") ; 

  c->cd(3); 
  t->Draw("tot>>h(1000,-200,800)","channel<64") ; 

  c->cd(4); 
  t->Draw("tot>>h2(1000,-200,800)","channel>63") ; 

}
