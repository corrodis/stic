#include "OperationalVariables.h"
#include "ChannelID.c"
#include "GetIndexArray.c"
#include "CheckConsistency.c" 

Int_t GetPairID(Int_t chID_ASIC0, Int_t chID_ASIC1){
  
  if (!CheckConsistency()) { 
    cout << "  -- ERROR -- Mismatch in the 'OperationalVariables.h' File " << endl; 
    cout << "  -- Macro aborted. " << endl; 
    break; 
  }

  const Int_t Nchanns_ASIC0 = Narray0*16; 
  const Int_t Nchanns_ASIC1 = Narray1*16; 
  Int_t ChanID_0[Nchanns_ASIC0]; 
  Int_t ChanID_1[Nchanns_ASIC1]; 		
  // 
  ChannelID(0,ChanID_0);   
  ChannelID(1,ChanID_1);
  
  Int_t i = GetIndexArray(ChanID_0,Nchanns_ASIC0,chID_ASIC0); 
  Int_t j = GetIndexArray(ChanID_1,Nchanns_ASIC1,chID_ASIC1); 
  Int_t pair = -1; 
  if (i>=0 && j>=0) pair = i*Nchanns_ASIC1+j; 
  return pair; 
}
