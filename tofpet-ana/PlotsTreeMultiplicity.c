void PlotsTreeMultiplicity(char filein[80]="EventSelection.root",
			   char savecanvdir[80]="./CanvasDir"){ 

  TFile *f = new TFile(filein); 
  TTree *tree = (TTree*)f->Get("EventTree"); 
  
  // Hits / Multiplicity 
 
  TCanvas *c = new TCanvas("cEventTree","cEventTree",1000,900); 
  c->Divide(2,2);
  
  c->cd(1)->SetLogy(); 
  tree->Draw("Nhits>>h1(30,0,30)"); 

  c->cd(2)->SetLogy(); 
  tree->Draw("Nhits>>h2(15,0,15)","Nhits==2"); 

  c->cd(3); tree->Draw("TovT[0]>>h3(500,-50,400)","Nhits==1"); 
  
  char csave[80]; 
  sprintf(csave,"%s%s",savecanvdir,"/cMultiplicity.root"); 
  c->Print(csave); 
  
  
  char printout[80]; 
  sprintf(printout,"1 hit = %d\n", h1->GetBinContent(2)); cout << printout; 
  sprintf(printout,"2 hits = %d\n",h1->GetBinContent(3)); cout << printout; 
} 
