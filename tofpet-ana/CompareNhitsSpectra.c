void CompareNhitsSpectra(const Int_t nt =21){ 
  
  char title[180]; 
  
  TH1F *hist[nt]; 
  TFile *f[nt]; 
  TTree *T[nt]; 
  TLegend *leg = new TLegend(0.68,0.68,1,1); 
  leg->SetFillColor(0); 
  
  Int_t i=0; 
  char filename[180][nt];
  // for NhitsPP - 9/12
  //for (int k=1;k<nt;k=k+2) { 
  //if (k!=0 && k!=2 && k!=10 && k!=13 && k!=16 && k!=19) {  
  for (int k=0;k<nt;k++) { 
    if (k==17 || k==7 || k==4 || k==20) { 
      //sprintf(filename[k],"./09Dec14_500M/Run%d/_EvtTree.root",k);
      sprintf(filename[k],"./09Dec14_500M/Run%d/_EvtTreeThr0.root",k);
      f[k] = new TFile(filename[k]); 
      T[k]= (TTree*)f[k]->Get("EventTree"); 
      char cut1[80];
      
      sprintf(title,"Nhits_Run%d",k); 
      //
      T[k]->Draw("Nhits>>h1(30,0,30)");
      hist[i] = (TH1F*)h1->Clone(title);
      hist[i]->SetLineColor(i+1); 
      
      leg->AddEntry(hist[i],title); 
      i++;
    }
  }

  TCanvas *c = new TCanvas("c","c",800,780); 
  c->cd(1)->SetLogy();
  hist[0]->Draw("hist"); 
  for (int k=1;k<i;k++) {
    hist[k]->Draw("same hist");
  }
  leg->Draw(); 
}
