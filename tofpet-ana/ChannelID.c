#include "OperationalVariables.h"
Double_t ChannelID(Double_t AsicId, Int_t *chanId) {
  // -------------------------------------------------------------
  // assign the ID of the channels in ASIC0 (e.g 16... 31,48...63) 
  // and in ASIC1 (e.g. 96...111)  depending on the occupied slots 
  // -------------------------------------------------------------
  Int_t Slot[4]; 
  Int_t Occu[64]; 
  for (int k=0;k<4;k++) { 
    if (AsicId == 0) Slot[k]=Slot0[k]; 
    if (AsicId == 1) Slot[k]=Slot1[k]; 
    for (int i=0;i<16;i++) { 
      Occu[k*16+i]= Slot[k]; 
      //cout << i << " " << k << " " << k*16+i << " " << Occu[k*16+i] << endl; 
    }
  }
  Int_t Channel0; 
  if (AsicId == 0) Channel0=0; 
  if (AsicId == 1) Channel0=64; 
  Int_t ich=0; 
  for (int i=0; i<64; i++) { 
    //cout << i << " " << Occu[i] << " " << ich << endl; 
    if (Occu[i]!=0) {
      chanId[ich]=i+Channel0; 
      ich++;
    }
  }
  return *chanId; 
}
