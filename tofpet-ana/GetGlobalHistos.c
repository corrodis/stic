#include "ReadSingleVariables.h" 
#include <algorithm>  
#include <iostream> 
void GetGlobalHistos(char filein[80]="ToT_histos.root",
		     char savecanvdir[80]="./CanvasDir"){

  TFile *f = new TFile(filein); 
  
  TCanvas *cGlob = new TCanvas("cGlobal","cGlobal",1000,900);
  TCanvas *cOccu = new TCanvas("cOccuAllHits","cOccuAllHits",1000,600);

  TLine *l[4]; 
  for (int i=0;i<4;i++) { 
    l[i]= new TLine(4.5+i*4,0.5,4.5+i*4,4.5);  
    l[i]->SetLineWidth(2); 
  }

  cGlob->Divide(3,2); 
  TH1F *h1 = f->Get("h_tot1");  
  TH1F *h2 = f->Get("h_tot2");  
  TH1F *h3 = f->Get("h_chan");            // nr hits per channel
  TH1F *h4 = h3->Clone();  h4->Rebin(16); // nr hits per array
  TH1F *h5 = h3->Clone();  h5->Rebin(64); // nr hits per ASIC
  
  cGlob->cd(1); h3->Draw(); h3->SetTitle("Channel Occupancy"); 
  h3->Fit("pol0");
  cGlob->cd(2); h4->Draw(); h4->SetTitle("Array Occupancy"); 
  cGlob->cd(3); h5->Draw(); h5->SetTitle("ASIC Occupancy"); 
  cGlob->cd(4)->SetLogy(); h1->Draw(); 
  cGlob->cd(5)->SetLogy(); h2->Draw(); 


  cOccu->Divide(1,2); 
  TH1F *hm1 = f->Get("h_map_asic0");  
  TH1F *hm2 = f->Get("h_map_asic1");  
  cOccu->cd(1); hm1->Draw("box");  for (int i=0;i<4;i++){ l[i]->Draw("same"); }
  cOccu->cd(2); hm2->Draw("box");  for (int i=0;i<4;i++){ l[i]->Draw("same"); }
  
  char csave[80]; 
  sprintf(csave,"%s%s%s",savecanvdir,"/","cGlobal.root"); 
  cGlob->Print(csave); 
  sprintf(csave,"%s%s%s",savecanvdir,"/","cOccupancyAllHits.root"); 
  cOccu->Print(csave); 
  
  

}
