#coding: utf-8
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize
import scipy.odr as odr

files=[ #"lisbon_run1_68V_sept11_chip1.dat",
    "lisbon_run2_68V_sept11_chip1.dat",     
    "lisbon_run3_68V_april14_sept11_chip1.dat",
    "lisbon_run4_68V_april14_sept11_chip1.dat",
    "lisbon_run5_68V_jan13_april14_sept11_chip1.dat",
    "lisbon_run6_68V_jan13_april14_sept11_chip1.dat",
    "lisbon_run7_68V_jan13_chip1.dat",
        "lisbon_run8_68V_jan13_chip1.dat"    
        #"lisbon_run9_68V_jan13_chip1.dat"
]


def checkRates(fname):
    rates = np.loadtxt(fname[0:-4]+"_rates.dat")
    plt.errorbar(rates[:,0],rates[:,1],rates[:,2],fmt='x',label="globale time")
    plt.errorbar(rates[:,0],rates[:,5],rates[:,6],fmt='x',label="timestamps")
    plt.xlabel("channel")
    plt.ylabel("rate [Hz]")
    plt.legend()
    plt.grid()
    plt.savefig("plots/"+fname[0:-4]+"_rates.png")
    plt.clf()
    plt.xlabel("channel")
    plt.plot(rates[:,0],rates[:,3]/rates[:,4],'x',label="globale time")
    plt.plot(rates[:,0],rates[:,7]/rates[:,8],'x',label="timestamps")
    plt.legend()
    plt.ylabel("chi2/nfd")
    plt.grid()
    plt.savefig("plots/"+fname[0:-4]+"_rates_chi2.png")
    plt.clf()
    #plt.hist(rates[:,3]/rates[:,4],label="globale time")
    #plt.hist(rates[:,7]/rates[:,8],label="timestamps")
    #plt.legend()
    #plt.xlabel("chi2/ndf")
    #plt.savefig("plots/"+fname[0:-4]+"_rates_chi2hist.png")
    #plt.clf()


def plotCTRvsrate(fname,cut=5000):
    ctrs = np.loadtxt(fname)
    rates = np.loadtxt(fname[0:-4]+"_rates.dat")
    ctr = ctrs[ctrs[:,9]>cut,:]
    meanRate = []
    maxRate = []
    for chs in ctr[:,0:2]:
        r1 = rates[rates[:,0]==chs[0],:]
        r2 = rates[rates[:,0]==chs[1],:]
        #meanRate.append([(r1[0][1]+r2[0][1])/2, (r1[0][2]+r2[0][2])/2])
        meanRate.append([(r1[0][5]+r2[0][5])/2, (r1[0][6]+r2[0][6])/2])
        if(r1[0][1] > r2[0][1]):
            #maxRate.append([r1[0][1], r1[0][2]])
            maxRate.append([r1[0][5], r1[0][6]])
        else:
            #maxRate.append([r2[0][1], r2[0][2]])
            maxRate.append([r2[0][5], r2[0][6]])
    meanRate = np.array(meanRate)
    maxRate = np.array(maxRate)
    #print meanRate, ctr
    if(len(ctr)>0):
        plt.errorbar(meanRate[:,0],ctr[:,4],ctr[:,5],meanRate[:,1],fmt='.')
        plt.xlabel("(mean) rate [Hz]")
        plt.ylabel("ctr [ps]")
        plt.ylim([0, 500])
        plt.title(fname[7:11]+", Lisbon Matrix, "+fname[16:-10]+" Source (stat>"+str(cut)+")")
        #plt.show()
        plt.grid()
        plt.savefig("plots/"+fname[0:-4]+"_ctrVsRate.png")
        plt.clf()
        plt.errorbar(maxRate[:,0],ctr[:,4],ctr[:,5],maxRate[:,1],fmt='.')
        plt.xlabel("(max) rate [Hz]")
        plt.ylabel("ctr [ps]")
        plt.ylim([0, 500])
        plt.title(fname[7:11]+", Lisbon Matrix, "+fname[16:-10]+" Source (stat>"+str(cut)+")")
        #plt.show()
        plt.grid()
        plt.savefig("plots/"+fname[0:-4]+"_ctrVsRate_max.png")
        plt.clf()
        plt.hist(ctr[:,4],bins=[150,155,160,165,170,175,180,185,190,195,200,205,210,215,220,225,230,235,240])
        plt.xlabel("ctr simga [ps]")
        plt.title(fname[7:11]+", Lisbon Matrix, "+fname[16:-10]+" Source (stat>"+str(cut)+")")
        #plt.show()
        plt.grid()
        plt.savefig("plots/"+fname[0:-4]+"_ctr_hist.png")
        plt.clf()



def plotCTR(fname):
    ctr = np.loadtxt(fname)
    plt.errorbar(ctr[:,9],ctr[:,4],ctr[:,5],fmt='x')
    plt.grid()
    plt.xlabel("# coinc")
    plt.ylabel("sigma CTR [ps]")
    plt.ylim([0,500])
    plt.title(fname[7:11]+", Lisbon Matrix, "+fname[16:-10]+" Source")
    #plt.show()
    plt.savefig("plots/"+fname[0:-3]+"png")
    plt.clf()

def plotMean(fname):
    ctr = np.loadtxt(fname)
    plt.errorbar(ctr[:,9],ctr[:,2],ctr[:,3],fmt='x')
    plt.grid()
    plt.xlabel("# coinc")
    plt.ylabel("shift [ps]")
    plt.ylim([-2000,2000])
    plt.title(fname[7:11]+", Lisbon Matrix, "+fname[16:-10]+" Source")
    #plt.show()
    plt.savefig("plots/"+fname[0:-4]+"_shift.png")
    plt.clf()

def plotStat(fname):
    ctr = np.loadtxt(fname)
    plt.plot(ctr[:,8],'x')
    plt.ylabel("number of coinc")
    #plt.show()
    plt.clf()    
    plt.savefig("plots/"+fname[0:-4]+"_statCoinc.png")
    plt.plot(ctr[:,9],'x')
    plt.ylabel("number of coinc in peak")
    #plt.show()
    plt.clf()
    plt.savefig("plots/"+fname[0:-4]+"_statPeak.png")
    plt.plot(ctr[:,9]/ctr[:,8],'x')
    plt.ylabel("ratio peak/coinc")
    #plt.show()
    plt.savefig("plots/"+fname[0:-4]+"statRatio.png")
    plt.clf()

def plotAll(files,cut=5000,ratecut=5000):
    linfitfunc = lambda p, x: p[0] + x*p[1]
    errfunc = lambda p, x, y: linfitfunc(p, x) - y
    p0 = [200, 0]
    
    linear = odr.Model(linfitfunc)
    
    ctrall = np.array([])
    ctrAll = np.array([])
    ctrErrAll = np.array([])
    meanRateAll = np.array([])
    maxRateAll = np.array([])
    meanRateErrAll = np.array([])
    maxRateErrAll = np.array([])
    for fname in files:
        ctr = np.loadtxt(fname)
        plt.errorbar(ctr[:,9],ctr[:,4],ctr[:,5],fmt='x',label=fname[16:-10])
        if len(ctr[ctr[:,9]>cut,:])>0:
            #print ctr[ctr[:,9]>cut,:]
            ctrall = np.append(ctrall, ctr[ctr[:,9]>cut,4])
            #ctrErrAll = np.append(ctrErrAll, ctr[ctr[:,9]>cut,5])
        plt.xlabel("# coinc")
        plt.ylabel("sigma CTR [ps]")
        plt.ylim([0,500])
        plt.title("Overview")
    plt.legend()
    plt.grid()
    #plt.show()
    plt.savefig("plots/overview.png")
    plt.clf()

    plt.hist(ctrall, bins=60)
    plt.xlabel("ctr sigma [ps]")
    plt.title("CTR Overview")
    plt.grid()
    plt.savefig("plots/overview_hist.png")
    plt.clf()
    
    for fname in files:
            ctrs = np.loadtxt(fname)
            rates = np.loadtxt(fname[0:-4]+"_rates.dat")
            ctr = ctrs[ctrs[:,9]>cut,:]
            meanRate = []
            maxRate = []
            for chs in ctr[:,0:2]:
                r1 = rates[rates[:,0]==chs[0],:]
                r2 = rates[rates[:,0]==chs[1],:]
                #meanRate.append([(r1[0][1]+r2[0][1])/2, (r1[0][2]+r2[0][2])/2])
                
                meanRate.append([(r1[0][5]+r2[0][5])/2, (r1[0][6]+r2[0][6])/2])
                if(r1[0][5] > r2[0][5]):
                    #maxRate.append([r1[0][1], r1[0][2]])
                    maxRate.append([r1[0][5], r1[0][6]])
                else:
                    #maxRate.append([r2[0][1], r2[0][2]])
                    maxRate.append([r2[0][5], r2[0][6]])
                #print r1[0][5], r2[0][5], (r1[0][5]+r2[0][5])/2, meanRate[len(meanRate)-1][0], maxRate[len(meanRate)-1][0]
                #if maxRate[len(meanRate)-1][0] < meanRate[len(meanRate)-1][0] :
                #    print "ooooops"
            meanRate = np.array(meanRate)
            maxRate = np.array(maxRate)
            
            sel = meanRate[:,1] < ratecut
            if len(ctr)>0:
                plt.errorbar(meanRate[sel,0]/1000,ctr[sel,4],ctr[sel,5],meanRate[sel,1]/1000,fmt='.',label=fname[16:-10])
                #print meanRateAll
                meanRateAll = np.append(meanRateAll, meanRate[sel,0])
                meanRateErrAll = np.append(meanRateErrAll, meanRate[sel,1])
                maxRateAll  = np.append(maxRateAll, maxRate[sel,0])
                maxRateErrAll  = np.append(maxRateErrAll, maxRate[sel,1])
                ctrAll = np.append(ctrAll, ctr[sel,4])
                ctrErrAll = np.append(ctrErrAll, ctr[sel,5])
    plt.xlabel("(mean) channel rate [kHz]")
    plt.ylabel("ctr sigma [ps]")
    plt.ylim([100, 400])
    plt.title("CTR vs Channel Rate")
    plt.grid()
    #plt.show()
    plt.legend()
    mydata = odr.RealData(meanRateAll, ctrAll,meanRateErrAll, ctrErrAll)
    myodr = odr.ODR(mydata, linear, beta0=[200,0])
    myout = myodr.run()
    p1 = myout.beta
    p1Error = myout.sd_beta
    #print p1[0]
    #p1, success = scipy.optimize.leastsq(errfunc, p0, args=(meanRateAll, ctrall))
    #m, b = np.polyfit(meanRateAll, ctrall, 1)
    meanRateAll.sort()
    #plt.plot(np.array([0,60000]), linfitfunc(p1, np.array([0,60000])), '--k')
    plt.fill_between(np.array([0,60000])/1000, linfitfunc(p1-p1Error, np.array([0,60000])), linfitfunc(p1 + p1Error, np.array([0,60000])), facecolor='grey', alpha=0.3)
    print p1, p1Error
    plt.xlim([0,60])
    plt.figtext(0.13,0.7, "ctr = {:0.0f}".format(p1[0])+"({:0.0f}) ps + r*".format(p1Error[0])+"{:0.2f}".format(p1[1]*1000)+" ({:0.2f}) ps/kHz".format(p1Error[1]*1000))
    plt.savefig("plots/overview_ctrVsRate_"+str(cut)+".png")
    plt.clf()



    for fname in files:
        ctrs = np.loadtxt(fname)
        rates = np.loadtxt(fname[0:-4]+"_rates.dat")
        ctr = ctrs[ctrs[:,9]>cut,:]
        meanRate = []
        maxRate = []
        for chs in ctr[:,0:2]:
            r1 = rates[rates[:,0]==chs[0],:] 
            r2 = rates[rates[:,0]==chs[1],:] 
            #meanRate.append([(r1[0][1]+r2[0][1])/2, (r1[0][2]+r2[0][2])/2]) 
            meanRate.append([(r1[0][5]+r2[0][5])/2, (r1[0][6]+r2[0][6])/2]) 
            if(r1[0][5] > r2[0][5]): 
                #maxRate.append([r1[0][1], r1[0][2]]) 
                maxRate.append([r1[0][5], r1[0][6]]) 
            else: 
                #maxRate.append([r2[0][1], r2[0][2]]) 
                maxRate.append([r2[0][5], r2[0][6]]) 

        meanRate = np.array(meanRate)
        maxRate = np.array(maxRate)
        sel = meanRate[:,1] < ratecut
        if len(ctr)>0:
            plt.errorbar(maxRate[sel,0]/1000,ctr[sel,4],ctr[sel,5],maxRate[sel,1]/1000,fmt='.',label=fname[16:-10])
    plt.xlabel("(max) channel rate [kHz]")
    plt.ylabel("ctr simga [ps]")
    plt.ylim([100, 400])
    plt.title("CTR vs Channel Rate")
    plt.grid()
    plt.legend()
    mydata = odr.RealData(maxRateAll, ctrAll,maxRateErrAll, ctrErrAll)
    myodr = odr.ODR(mydata, linear, beta0=[200,0])
    myout = myodr.run()
    p2 = myout.beta
    p2Error = myout.sd_beta
    #plt.show()
    #m, b = np.polyfit(maxRateAll, ctrall, 1)
    maxRateAll.sort()
    #plt.plot(maxRateAll, m*maxRateAll+b, '-k')
    #print m, b
    print p2
    #plt.plot(np.array([0,60000]), linfitfunc(p2, np.array([0,60000])), '--k')
    plt.fill_between(np.array([0,60000])/1000, linfitfunc(p2-p2Error, np.array([0,60000])), linfitfunc(p2 + p2Error, np.array([0,55000])), facecolor='grey', alpha=0.3)
    print p2, p2Error
    plt.xlim([0,60])
    plt.figtext(0.13,0.7, "ctr = {:0.0f}".format(p2[0])+"({:0.0f}) ps + r*".format(p2Error[0])+"{:0.2f}".format(p2[1]*1000)+" ({:0.2f}) ps/kHz".format(p2Error[1]*1000))
    plt.savefig("plots/overview_ctrVsRate_max_"+str(cut)+".png")
    plt.clf()

def plotCTRvsChiprate(files,cut=5000):
    linfitfunc = lambda p, x: p[0] + x*p[1]
    
    linear = odr.Model(linfitfunc)
    allCtr = np.array([])
    allCtrErr = np.array([])
    allChipRate = np.array([])
    allChipRateErr = np.array([])
    allMeanChipRate = np.array([])
    allMeanChipRateErr = np.array([])
    rates = np.loadtxt("chip_rates.dat")
    for fname in files:
            ctrs = np.loadtxt(fname)
            ctr = ctrs[ctrs[:,9]>cut,:]
            rate = rates[rates[:,0]==int(fname[10:11]),:][0]
            rateVec = np.array([rate[1] for i in range(len(ctr[:,5]))])
            rateErrVec = np.array([rate[2] for i in range(len(ctr[:,5]))])
            if len(ctr)>0:
                plt.errorbar(rateVec/1000,ctr[:,4],ctr[:,5],rateErrVec/1000,fmt='.',label=fname[16:-10])
                allCtr = np.append(allCtr, np.mean(ctr[:,4]))
                allCtrErr = np.append(allCtrErr, np.std(ctr[:,4]))
                allChipRate = np.append(allChipRate, rate[5])
                allChipRateErr = np.append(allChipRateErr, rate[6])
                allMeanChipRate = np.append(allMeanChipRate, rate[1])
                allMeanChipRateErr = np.append(allMeanChipRateErr, rate[2])
                #print meanRateAll
    plt.xlabel("mean chip rate k[Hz]")
    plt.ylabel("ctr sigma [ps]")
    plt.ylim([100, 400])
    plt.title("CTR vs Mean Chip Rate")
    plt.grid()
    #plt.show()
    plt.legend()
    mydata = odr.RealData(allMeanChipRate, allCtr, allMeanChipRateErr, allCtrErr)
    myodr = odr.ODR(mydata, linear, beta0=[200,0])
    myout = myodr.run()
    p1 = myout.beta
    p1Error = myout.sd_beta
    #print p1[0]
    #p1, success = scipy.optimize.leastsq(errfunc, p0, args=(meanRateAll, ctrall))
    #m, b = np.polyfit(meanRateAll, ctrall, 1)
    #meanRateAll.sort()
    #plt.plot(np.array([0,60000]), linfitfunc(p1, np.array([0,60000])), '--k')
    plt.fill_between(np.array([0,60000])/1000, linfitfunc(p1-p1Error, np.array([0,60000])), linfitfunc(p1 + p1Error, np.array([0,55000])), facecolor='grey', alpha=0.3)
    #print p1, p1Error
    plt.figtext(0.13,0.7, "ctr = {:0.0f}".format(p1[0])+"({:0.0f}) ps + r*".format(p1Error[0])+"{:0.2f}".format(p1[1]*1000)+" ({:0.2f}) ps/kHz".format(p1Error[1]*1000))
    plt.xlim([0,60])
    plt.savefig("plots/overview_ctrVsMeanChipRate_"+str(cut)+".png")
    plt.clf()



    for fname in files:
            ctrs = np.loadtxt(fname)
            ctr = ctrs[ctrs[:,9]>cut,:]
            rate = rates[rates[:,0]==int(fname[10:11]),:][0]
            rateVec = np.array([rate[5] for i in range(len(ctr[:,5]))])
            rateErrVec = np.array([rate[6] for i in range(len(ctr[:,5]))])
            if len(ctr)>0:
                plt.errorbar(rateVec/1000,ctr[:,4],ctr[:,5],rateErrVec/1000,fmt='.',label=fname[16:-10])
                #print meanRateAll
                plt.xlabel("chip rate [kHz]")
    plt.ylabel("ctr sigma [ps]")
    plt.ylim([100, 400])
    plt.title("CTR vs Chip Rate")
    plt.grid()
    #plt.show()
    plt.legend()
    mydata = odr.RealData(allChipRate, allCtr, allChipRateErr, allCtrErr)
    myodr = odr.ODR(mydata, linear, beta0=[200,0])
    myout = myodr.run()
    p2 = myout.beta
    p2Error = myout.sd_beta
    #print p1[0]
    #p1, success = scipy.optimize.leastsq(errfunc, p0, args=(meanRateAll, ctrall))
    #m, b = np.polyfit(meanRateAll, ctrall, 1)
    #meanRateAll.sort()
    #plt.plot(np.array([0,60000]), linfitfunc(p1, np.array([0,60000])), '--k')
    plt.fill_between(np.array([0,700000])/1000, linfitfunc(p2-p2Error, np.array([0,700000])), linfitfunc(p2 + p2Error, np.array([0,700000])), facecolor='grey', alpha=0.3)
    #print p1, p1Error
    plt.xlim([0,700])
    plt.figtext(0.13,0.7, "ctr = {:0.0f}".format(p2[0])+"({:0.0f}) ps + r*".format(p2Error[0])+"{:0.2f}".format(p2[1]*1000)+" ({:0.2f}) ps/kHz".format(p2Error[1]*1000))
    plt.savefig("plots/overview_ctrVsChipRate_"+str(cut)+".png")
    plt.clf()


for f in files:
    checkRates(f)
    plotCTR(f)
    plotMean(f)
    plotStat(f)
    plotCTRvsrate(f,2000)  

for cut in [0, 500, 1000, 1500, 2000, 2500, 3000, 4000, 4500]:
    plotCTRvsChiprate(files,cut)  
    plotAll(files,cut,2000)
