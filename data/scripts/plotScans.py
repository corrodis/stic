import ROOT
import sys
import os

ch = sys.argv[1]

if len(sys.argv)>2:
	scale = int(sys.argv[2])
else:
	scale = 0 


fit_file = ROOT.TFile("Volt68_2/rates.root")
fit_gr = fit_file.Get("fit_ch"+str(ch)+"_scale"+str(scale))
fit_gr.GetYaxis().SetRangeUser(0,2500)
fit_gr.GetXaxis().SetRangeUser(0,32)
nois_file = ROOT.TFile("Volt0/rates.root")
nois_gr = nois_file.Get("ch"+str(ch)+"_scale"+str(scale))
nois_gr.SetLineColor(ROOT.kRed)
nois_gr.SetLineWidth(3)


if not os.path.exists("plots"):
    os.makedirs("plots")

c = ROOT.TCanvas("c1")
fit_gr.Draw("ALP")
nois_gr.Draw("SAME")
#if not os.path.isfile(fname):
#	os.mkdir("plots")

c.SaveAs("plots/ch"+str(ch)+"_s"+str(scale)+".pdf")
c.SaveAs("plots/ch"+str(ch)+"_s"+str(scale)+".root")

fit_file.Close()
nois_file.Close()
